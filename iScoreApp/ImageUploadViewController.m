//
//  ImageUploadViewController.m
//  iScoreApp
//
//  Created by My Mac on 11/05/20.
//  Copyright © 2020 ADMIN-Khushal. All rights reserved.
//

#import "ImageUploadViewController.h"
#import "UploadImageCollectionViewCell.h"
#import <Photos/Photos.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "ApplicationConst.h"
#import <AFNetworking/AFNetworking.h>

@import OpalImagePicker;

@interface ImageUploadViewController ()<UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout, UIImagePickerControllerDelegate, UINavigationControllerDelegate,OpalImagePickerControllerDelegate>
{
    NSMutableArray *items;
   // OpalImagePickerController *conttrl;
   
}
@end

@implementation ImageUploadViewController
@synthesize conttrl;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    items = [[NSMutableArray alloc] init];
    
    _View1 = false;
}

- (void)viewWillAppear:(BOOL)animated
{
    if (!_View1) {
        [items insertObject:[UIImage imageNamed:@"postad"] atIndex:0];
    }
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return items.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
     UploadImageCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    
    if (indexPath.item != 0) {
        cell.Btn_Close.hidden = false;
                  // cell.Btn_Close.addTarget(self, action: #selector(PostADSecondViewController.connected), for: .touchUpInside)
        cell.Btn_Close.tag = indexPath.row;
          [cell.Btn_Close addTarget:self action:@selector(connected:) forControlEvents:UIControlEventTouchUpInside];
                   //cell.Btn_Close.backgroundColor = GlobalColor.SelectedColor
        cell.Img_Vw.image = self->items[indexPath.row];
               }
               else{
                   cell.Btn_Close.hidden = true;
                   cell.Img_Vw.image = self->items[indexPath.row];
                  // cell.Img_Vw.changeImageViewImageColor(color: UIColor.white)
               }
               
    cell.Img_Vw.layer.cornerRadius = cell.Img_Vw.frame.size.height/2;
    cell.Img_Vw.layer.masksToBounds = true;
    cell.Btn_Close.layer.cornerRadius = cell.Btn_Close.frame.size.height/2;
    cell.Btn_Close.layer.masksToBounds = true;
          
               
    //self.viewDidLayoutSubviews();
    
   // [self viewDidLayoutSubviews];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    float cellWidth = screenWidth / 3.0; //Replace the divisor with the column count requirement. Make sure to have it in float.
    CGSize size = CGSizeMake(cellWidth, cellWidth);

    return size;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"You selected cell:%ld", indexPath.item);
    if (21 - self->items.count != 0) {

                if (indexPath.item == 0) {
                                            
                    //   print("Imageview Clicked")
                    NSLog(@"Imageview Clicked");
                                                           
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Alert" message:@"Select Media Type" preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *camera = [UIAlertAction actionWithTitle:@"Take Photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                //button click event
                        _View1 = true;
                            NSLog(@"CHOOSE IMAGE!!!");
                                UIImagePickerController *picker = [[UIImagePickerController alloc] init];
                                picker.delegate = self;
                                picker.allowsEditing = YES;
                                picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                                [self presentViewController:picker animated:YES completion:NULL];
                            
                                }];
                    
                    UIAlertAction *gallery = [UIAlertAction actionWithTitle:@"Choose from Gallery" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    //button click event
                     conttrl = [[OpalImagePickerController alloc] init];
                                _View1 = true;
                        conttrl.imagePickerDelegate  = self;
                        conttrl.maximumSelectionsAllowed = 21 - items.count;
                        [self presentViewController:conttrl animated:YES completion:NULL];
                                }];
                    
                    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
                    [alert addAction:cancel];
                    [alert addAction:camera];
                     [alert addAction:gallery];
                    [self presentViewController:alert animated:YES completion:nil];
                }
            
               
}
    else
    {
        // showToast(uiview: self, msg: "You can select only 10 images")
       
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Alert" message:@"You can select only 20 images" preferredStyle:UIAlertControllerStyleAlert];
                          
         UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
         [alert addAction:cancel];
         [self presentViewController:alert animated:YES completion:nil];
    }
}

-(void)connected:(UIButton *)sender{
    
    int buttonTag = (int)sender.tag;;
              
    //  items.remove(at: buttonTag)
    [items removeObjectAtIndex:buttonTag];
    [_Vw_Collection reloadData];
    //  self.viewDidLayoutSubviews()
   // [self viewDidLayoutSubviews];
    
}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
     _View1 = true;
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
   
    
//    NSURL *imageURL = [info valueForKey:UIImagePickerControllerPHAsset];
//    PHAsset *phAsset = [[PHAsset fetchAssetsWithALAssetURLs:@[imageURL] options:nil] lastObject];
//    NSString *imageName = [phAsset valueForKey:@"filename"];
    
    UIImage *photo = [info valueForKey:UIImagePickerControllerEditedImage];
    
    NSLog(@"Picked image: width: %f x height: %f", photo.size.width, photo.size.height);
    

    [items addObject:photo];
    NSLog(@"%@", items);

               if (items.count != 0) {
                   [_Vw_Collection reloadData];
               }
    
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
  
    
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}



- (IBAction)back_click:(UIButton *)sender {
     [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)upload_click:(UIButton *)sender {
    if (items.count > 1) {
        
        if ([[_dictdata valueForKey:@"Type"]isEqualToString:@"upload"])
        {
             [self uploadImages];
        }
        else
        {
            [self uploadImagesgenerate];
        }
       
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                      message:@"please select images"
                                      delegate:self
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil];
        
                  [alert show];
    }
}

-(void)imagePickerDidCancel:(OpalImagePickerController *)picker
{
    
}

-(void)imagePicker:(OpalImagePickerController *)picker didFinishPickingAssets:(NSArray<PHAsset *> *)assets
{
     _View1 = true;
    
    self.requestOptions = [[PHImageRequestOptions alloc] init];
    self.requestOptions.resizeMode   = PHImageRequestOptionsResizeModeFast;
    self.requestOptions.deliveryMode = PHImageRequestOptionsDeliveryModeHighQualityFormat;

    // this one is key
    self.requestOptions.synchronous = YES;

    for(PHAsset *asset in assets) {
        // This autorelease pool seems good (a1)
      //  @autoreleasepool {
         //   NSLog(@"started requesting image %i", i);
        if (asset.mediaType == PHAssetMediaTypeImage) {
              [[PHImageManager defaultManager] requestImageForAsset:asset targetSize:PHImageManagerMaximumSize contentMode:PHImageContentModeAspectFit options:_requestOptions resultHandler:^(UIImage *image, NSDictionary *info) {
                              [items addObject:image];
            //                dispatch_async(dispatch_get_main_queue(), ^{
            //                    //you can add autorelease pool here as well (a2)
            //
            //                });
                        }];
        }
          
           // i++;
      //  } // a1 ends here
    }
    
        
    NSLog(@"items:%@", items);
    
            if (items.count != 0) {
                [_Vw_Collection reloadData];
            }
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

//-(void)uploadImages{
// [APP_DELEGATE showLoadingView:@""];
//// image.finalImage - is image itself
//// totalCount - Total number of images need to upload on server
//    if (items.count > 1) {
//        [items removeObjectAtIndex:0];
//    }
//
//NSDictionary *parameters = @{@"StudID":[[NSUserDefaults standardUserDefaults] valueForKeyPath:@"ParseData.StudentID"], @"PaperID" : [_dictdata valueForKey:@"PaperID"]};
//
//NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:[NSString stringWithFormat:@"%@submit_upload_paper_student_anwser",ICAPIURL] parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
//    int i=0;
//    for(UIImage *eachImagePath in items)
//    {
//
//        NSData *imageData = UIImageJPEGRepresentation(eachImagePath,0.5);
//        [formData appendPartWithFileData:imageData name:[NSString stringWithFormat:@"image%d",i ] fileName:[NSString stringWithFormat:@"image%d.jpg",i ] mimeType:@"image/jpeg"];
//
////        NSURL *fileURL = [[NSURL alloc] initFileURLWithPath:eachImagePath];
////        [formData appendPartWithFileURL:fileURL name:@"image" fileName:[NSString stringWithFormat:@"file%d.jpg",i ] mimeType:@"image/jpeg" error:nil];
//        i++;
//    }
//} error:nil];
//    // "multipart/form­data"    @"multipart/form-data"
//    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
//    [request setTimeoutInterval:300];
//   // [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
//
//AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
//
//NSURLSessionUploadTask *uploadTask;
//uploadTask = [manager
//              uploadTaskWithStreamedRequest:request
//              progress:nil
//              completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
//                  if (error) {
//                      NSLog(@"Error: %@", error);
//                      UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
//                                                                         message:[responseObject valueForKey:@"message"]
//                                                                         delegate:self
//                                                             cancelButtonTitle:@"OK"
//                                                             otherButtonTitles:nil];
//
//                                                     [alert show];
//                  } else {
//                      NSLog(@"%@ %@", response, responseObject);
//                      UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
//                                                    message:[responseObject valueForKey:@"message"]
//                                                    delegate:self
//                                        cancelButtonTitle:@"OK"
//                                        otherButtonTitles:nil];
//
//                                [alert show];
//                      [self.navigationController popViewControllerAnimated:NO];
//                  }
//     [APP_DELEGATE hideLoadingView];
//              }];
//
//[uploadTask resume];
//}


-(void)uploadImages{
 [APP_DELEGATE showLoadingView:@""];
// image.finalImage - is image itself
// totalCount - Total number of images need to upload on server
    if (items.count > 1) {
        [items removeObjectAtIndex:0];
    }
    
NSDictionary *parameters = @{@"StudID":[[NSUserDefaults standardUserDefaults] valueForKeyPath:@"ParseData.StudentID"], @"PaperID" : [_dictdata valueForKey:@"PaperID"]};

AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html", @"image/jpg",  @"image/jpeg", @"image/gif", @"image/png", @"application/pdf",@"application/json", nil];

[manager POST:[NSString stringWithFormat:@"%@submit_upload_paper_student_anwser",ICAPIURL] parameters:[parameters copy] constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
     int i=0;
        for(UIImage *eachImagePath in items)
        {
    
            NSData *imageData = UIImageJPEGRepresentation(eachImagePath,0.5);
            [formData appendPartWithFileData:imageData name:@"image" fileName:[NSString stringWithFormat:@"image%d.jpg",i ] mimeType:@"image/jpeg"];
    
    //        NSURL *fileURL = [[NSURL alloc] initFileURLWithPath:eachImagePath];
    //        [formData appendPartWithFileURL:fileURL name:@"image" fileName:[NSString stringWithFormat:@"file%d.jpg",i ] mimeType:@"image/jpeg" error:nil];
            i++;
        }
    //add img data one by one
//    NSData *imageData = UIImageJPEGRepresentation([_dictdata valueForKey:@"studimg"],0.0);
//    //NSData *imageData = UIImagePNGRepresentation([_dictdata valueForKey:@"studimg"]);
//    [formData appendPartWithFileData:imageData name:[NSString stringWithFormat:@"image"] fileName:[NSString stringWithFormat:@"image.jpg"] mimeType:@"image/jpeg"];
    
} success:^(AFHTTPRequestOperation *operation, id dicsResponse) {
    NSLog(@"Success: %@", dicsResponse);
    
     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                        message:[dicsResponse valueForKey:@"message"]
                                                        delegate:self
                                            cancelButtonTitle:@"OK"
                                            otherButtonTitles:nil];
    
                                    [alert show];
                          [self.navigationController popViewControllerAnimated:NO];
    
    
    
    [APP_DELEGATE hideLoadingView];
}
      failure:^(AFHTTPRequestOperation *operation, NSError *error) {
          NSLog(@"Error: %@", error);
          [APP_DELEGATE hideLoadingView];
      }];
}

-(void)uploadImagesgenerate{
 [APP_DELEGATE showLoadingView:@""];
// image.finalImage - is image itself
// totalCount - Total number of images need to upload on server
    if (items.count > 1) {
        [items removeObjectAtIndex:0];
    }
    
NSDictionary *parameters = @{@"StudID":[[NSUserDefaults standardUserDefaults] valueForKeyPath:@"ParseData.StudentID"], @"PaperID" : [_dictdata valueForKey:@"PaperID"]};

AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html", @"image/jpg",  @"image/jpeg", @"image/gif", @"image/png", @"application/pdf",@"application/json", nil];

[manager POST:[NSString stringWithFormat:@"%@submit_student_anwser",ICAPIURL] parameters:[parameters copy] constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
     int i=0;
        for(UIImage *eachImagePath in items)
        {
    
            NSData *imageData = UIImageJPEGRepresentation(eachImagePath,0.5);
            [formData appendPartWithFileData:imageData name:@"image" fileName:[NSString stringWithFormat:@"image%d.jpg",i ] mimeType:@"image/jpeg"];
    
    //        NSURL *fileURL = [[NSURL alloc] initFileURLWithPath:eachImagePath];
    //        [formData appendPartWithFileURL:fileURL name:@"image" fileName:[NSString stringWithFormat:@"file%d.jpg",i ] mimeType:@"image/jpeg" error:nil];
            i++;
        }
    //add img data one by one
//    NSData *imageData = UIImageJPEGRepresentation([_dictdata valueForKey:@"studimg"],0.0);
//    //NSData *imageData = UIImagePNGRepresentation([_dictdata valueForKey:@"studimg"]);
//    [formData appendPartWithFileData:imageData name:[NSString stringWithFormat:@"image"] fileName:[NSString stringWithFormat:@"image.jpg"] mimeType:@"image/jpeg"];
    
} success:^(AFHTTPRequestOperation *operation, id dicsResponse) {
    NSLog(@"Success: %@", dicsResponse);
    
     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                        message:[dicsResponse valueForKey:@"message"]
                                                        delegate:self
                                            cancelButtonTitle:@"OK"
                                            otherButtonTitles:nil];
    
                                    [alert show];
                          [self.navigationController popViewControllerAnimated:NO];
    
    
    
    [APP_DELEGATE hideLoadingView];
}
      failure:^(AFHTTPRequestOperation *operation, NSError *error) {
          NSLog(@"Error: %@", error);
          [APP_DELEGATE hideLoadingView];
      }];
}



@end
