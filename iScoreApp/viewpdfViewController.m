//
//  viewpdfViewController.m
//  iScoreApp
//
//  Created by My Mac on 09/05/20.
//  Copyright © 2020 ADMIN-Khushal. All rights reserved.
//

#import "viewpdfViewController.h"
#import <WebKit/WebKit.h>
#import "ImageUploadViewController.h"

@interface viewpdfViewController ()
{
    NSTimer *timer;
}
@property (strong, nonatomic) IBOutlet WKWebView *webvw;

@end

@implementation viewpdfViewController

- (void)viewDidDisappear:(BOOL)animated
{
    [timer invalidate];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if ([[_dictdata valueForKey:@"Type"]isEqualToString:@"upload"])
           {
               [_webvw setBackgroundColor:[UIColor whiteColor]];
                NSURL *nsurl=[NSURL URLWithString:[_dictdata valueForKey:@"QuestionPaperFile"]];
                NSURLRequest *nsrequest=[NSURLRequest requestWithURL:nsurl];
                [_webvw loadRequest:nsrequest];
           }
           else
           {
                [_webvw setBackgroundColor:[UIColor whiteColor]];
                [_webvw loadHTMLString:_htmlstring baseURL:nil];
           }
    
    
    
    
   timer = [NSTimer scheduledTimerWithTimeInterval:1.0f
    target:self selector:@selector(methodB:) userInfo:nil repeats:YES];
}
- (void) methodB:(NSTimer *)timer
{
//Do calculations.
    
    NSDate *today = [NSDate date]; // it will give you current date
              NSDateFormatter *dateFormatte = [[NSDateFormatter alloc] init];
               [dateFormatte setDateFormat:@"yyyy-MM-dd hh:mm a"];
               
    NSDate *endTime = [dateFormatte dateFromString:[_dictdata valueForKey:@"EndTime"]];
               
               NSComparisonResult result,result1;
               //has three possible values: NSOrderedSame,NSOrderedDescending, NSOrderedAscending

                 result1 = [today compare:endTime];
               // comparing two dates

        if  (result1 == NSOrderedAscending)
               {
                   NSLog(@"today is less");
               
               }
               else if(result1 == NSOrderedDescending)
               {
                   //cell.btnplay.enabled = false;
                   NSLog(@"newDate is less");
                   [timer invalidate];
                                      UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Alert" message:@"You have completed your time limit for your test. \n All the best." preferredStyle:UIAlertControllerStyleAlert];

                                      UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                              //button click event
                                          
                                          [self.navigationController popViewControllerAnimated:YES];
                                                          }];
                   //                   UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
                   //                   [alert addAction:cancel];
                                      [alert addAction:ok];
                                      [self presentViewController:alert animated:YES completion:nil];

               }
               else
               {
                   NSLog(@"Both dates are same");
               }
    
}

- (IBAction)back_click:(UIButton *)sender {
     [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)upload_click:(UIButton *)sender {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Alert" message:@"Are You Sure You want to upload paper?" preferredStyle:UIAlertControllerStyleAlert];

                                         UIAlertAction *ok = [UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                                 //button click event
                                              UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
                                            ImageUploadViewController * next = [storyboard instantiateViewControllerWithIdentifier:@"ImageUploadViewController"];
                                            next.dictdata = _dictdata;
                                            [self.navigationController pushViewController:next animated:YES];
                                             
                                                             }];
                                         UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleCancel handler:nil];
                                         [alert addAction:cancel];
                                         [alert addAction:ok];
                                         [self presentViewController:alert animated:YES completion:nil];
}
@end
