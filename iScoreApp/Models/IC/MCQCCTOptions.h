//
//  MCQCCTOptions.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 9/18/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <objc/runtime.h>

@interface MCQCCTOptions : NSObject<NSCoding>

@property (retain , nonatomic) NSString *MCQOPtionID;
@property (retain , nonatomic) NSString *Options;
@property (retain , nonatomic) NSString *isCorrect;


- (MCQCCTOptions*) parseObjectsWithData:(NSDictionary *)mDict;
- (void)getValues:(NSCoder *)aCoder;

@end
