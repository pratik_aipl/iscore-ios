//
//  ICTestModel.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 9/20/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <objc/runtime.h>

@interface ICTestModel : NSObject<NSCoding>

@property (retain , nonatomic) NSString *Answer;
@property (retain , nonatomic) NSString *ClassQuestionPaperDetailID;
@property (retain , nonatomic) NSString *ExamTypePatternDetailID;
@property (retain , nonatomic) NSString *isPassage;
@property (retain , nonatomic) NSString *isQuestion;
@property (retain , nonatomic) NSString *MQuestionID;
@property (retain , nonatomic) NSString *PageNo;
@property (retain , nonatomic) NSString *Question;
@property (retain , nonatomic) NSString *QuestionNo;
@property (retain , nonatomic) NSString *QuestionTypeText;
@property (retain , nonatomic) NSString *SubQuestionNo;



- (ICTestModel*) parseObjectsWithData:(NSDictionary *)mDict;
- (void)getValues:(NSCoder *)aCoder;
@end
