//
//  MCQCCTModel.m
//  iScoreApp
//
//  Created by ADMIN-Khushal on 9/17/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "MCQCCTModel.h"

@implementation MCQCCTModel

-(MCQCCTModel*) parseObjectWithFMResultSet :(NSDictionary *)mDict
{
    
    if (self == [super init]) {
        unsigned int outCount, i;
        objc_property_t *properties = class_copyPropertyList([self class], &outCount);
        for (i = 0; i < outCount; i++) {
            objc_property_t property = properties[i];
            
            @try {
                NSString *p = [NSString stringWithFormat:@"%s", property_getName(property)];
                
                
                if(([mDict valueForKey:p] == NULL || [mDict valueForKey:p] == nil || [[mDict valueForKey:p] isEqual:@""]) ){
                    [self setValue:@"" forKey:p];
                } else {
                    [self setValue:[mDict valueForKey:p]  forKey:p];
                }
            }@catch (NSException *exception) {
                NSLog(@"%@", exception.reason);
            }
            @finally {
                NSLog(@"Char at index %d cannot be found", index);
            }
        }
        free(properties);
    }
    return self;
}


- (void)getValues:(NSCoder *)aCoder
{
    unsigned int outCount, i;
    objc_property_t *properties = class_copyPropertyList([self class], &outCount);
    for (i = 0; i < outCount; i++) {
        objc_property_t property = properties[i];
        NSString *p = [NSString stringWithFormat:@"%s", property_getName(property)];
        [aCoder encodeObject:[self valueForKey:p] forKey:p];
    }
    free(properties);
}




@end
