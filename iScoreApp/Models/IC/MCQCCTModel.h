//
//  MCQCCTModel.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 9/17/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//
#import <Foundation/Foundation.h>
#import <objc/runtime.h>
#import "MCQCCTOptions.h"

@interface MCQCCTModel : NSObject<NSCoding>

//// add your properties
//// add your properties
@property (retain , nonatomic) NSString *AnswerID;
@property (retain , nonatomic) NSString *ClassMCQTestDTLID;
@property (retain , nonatomic) NSString *IsAttempt;
@property (retain , nonatomic) NSString *MasterorCustomized;
@property (retain , nonatomic) NSString *Question;
@property (retain , nonatomic) NSString *QuestionID;//selectedAns
@property (retain , nonatomic) NSString *selectedAns;
@property (retain , nonatomic) NSString *Level;
@property (retain , nonatomic) NSString *IsRight;

@property (retain , nonatomic) NSMutableArray<MCQCCTOptions *> *optionsArray;


- (MCQCCTModel*) parseObjectWithFMResultSet:(NSDictionary *)mDict;
- (void)getValues:(NSCoder *)aCoder;

@end


