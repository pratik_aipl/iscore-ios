//
//  LevelModel.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 5/7/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <objc/runtime.h>

@interface LevelModel : NSObject<NSCoding>

//// add your properties
@property (retain , nonatomic) NSString *Total_attempt_level;
@property (retain , nonatomic) NSString *Right_Answer_level;
@property (retain , nonatomic) NSString *accuracy;



+(LevelModel*) parseObjectWithFMResultSet:(NSDictionary *)mDict;
- (void)getValues:(NSCoder *)aCoder;


@end
