//
//  LevelAccuracy.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 5/8/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <objc/runtime.h>

@interface LevelAccuracy : NSObject<NSCoding>

//// add your properties
@property (retain , nonatomic) NSString *accuracy;

+(LevelAccuracy*) parseObjectWithFMResultSet:(NSDictionary *)mDict;
- (void)getValues:(NSCoder *)aCoder;

@end
