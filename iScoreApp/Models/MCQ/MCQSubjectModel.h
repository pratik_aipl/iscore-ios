//
//  MCQSubjectModel.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 4/20/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <objc/runtime.h>

@interface MCQSubjectModel : NSObject <NSCoding>

//// add your properties
@property (retain , nonatomic) NSString *SubjectName;
@property (retain , nonatomic) NSString *SubjectIcon;
@property (retain , nonatomic) NSString *SubjectID;
@property (retain , nonatomic) NSString *BoardID;
@property (retain , nonatomic) NSString *MediumID;
@property (retain , nonatomic) NSString *StandardID;
@property (retain , nonatomic) NSString *CreatedBy;
@property (retain , nonatomic) NSString *CreatedOn;
@property (retain , nonatomic) NSString *ModifiedBy;
@property (retain , nonatomic) NSString *ModifiedOn;
@property (retain , nonatomic) NSString *SubjectOrder;
@property (retain , nonatomic) NSString *isMCQ;
@property (retain , nonatomic) NSString *isGeneratePaper;
@property (retain , nonatomic) NSString *accuracy1;
@property (retain , nonatomic) NSString *Last_Date;
@property (retain , nonatomic) NSString *TotalSubjectTest ;
/// only change above

- (NSString *) className;


+ (MCQSubjectModel*) parseObjectWithFMResultSet:(NSDictionary *)mDict;
- (void)getValues:(NSCoder *)aCoder;

@end
