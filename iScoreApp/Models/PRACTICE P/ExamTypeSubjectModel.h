//
//  ExamTypeSubjectModel.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 5/31/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <objc/runtime.h>

@interface ExamTypeSubjectModel : NSObject<NSCoding>

@property (retain , nonatomic) NSString *ExamTypeID;

@property (retain , nonatomic) NSString *ExamTypeName;
@property (retain , nonatomic) NSString *PaperType;
@property (retain , nonatomic) NSString *BoardID;
@property (retain , nonatomic) NSString *MediumID;
@property (retain , nonatomic) NSString *StanderdID;
@property (retain , nonatomic) NSString *TotalMarks;
@property (retain , nonatomic) NSString *Duration;
@property (retain , nonatomic) NSString *instruction;
@property (retain , nonatomic) NSString *CreatedBy;
@property (retain , nonatomic) NSString *CreatedOn;
@property (retain , nonatomic) NSString *ModifiedBy;
@property (retain , nonatomic) NSString *ModifiedOn;
@property (retain , nonatomic) NSString *MainInstruction;

@property (retain , nonatomic) NSString *ExamTypePatternID;




@end
