//
//  SetPaperModel.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 5/29/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <objc/runtime.h>

@interface SetPaperModel : NSObject<NSCoding>

@property (retain , nonatomic) NSString *TotalQuestion;
@property (retain , nonatomic) NSString *QuestionTypeID;
@property (retain , nonatomic) NSString *QuestionType;
@property (retain , nonatomic) NSString *Marks;

@property (nonatomic) BOOL Checked;
@property (nonatomic, strong) NSString *Txttoask;
@property (nonatomic, strong) NSString *Txttoans;




- (NSString *) className;


+ (SetPaperModel*) parseObjectWithFMResultSet:(NSDictionary *)mDict;
- (void)getValues:(NSCoder *)aCoder;


@end
