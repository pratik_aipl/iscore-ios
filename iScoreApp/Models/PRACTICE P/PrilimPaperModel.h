//
//  PrilimPaperModel.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 6/12/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <objc/runtime.h>

@interface PrilimPaperModel : NSObject<NSCoding>

/////////////
@property (retain , nonatomic) NSString *MQuestionID;
@property (retain , nonatomic) NSString *Question;
@property (retain , nonatomic) NSString *ImageURL;
@property (retain , nonatomic) NSString *Answer;
@property (retain , nonatomic) NSString *BoardID;
@property (retain , nonatomic) NSString *MediumID;
@property (retain , nonatomic) NSString *StanderdID;
@property (retain , nonatomic) NSString *ClassID;
@property (retain , nonatomic) NSString *subjectID;
@property (retain , nonatomic) NSString *ChapterID;
@property (retain , nonatomic) NSString *QuestionTypeID;
@property (retain , nonatomic) NSString *isBoard;
@property (retain , nonatomic) NSString *ExerciseNo;
@property (retain , nonatomic) NSString *QuestionNo;
@property (retain , nonatomic) NSString *Question_Year;
@property (retain , nonatomic) NSString *is_demo;
@property (retain , nonatomic) NSString *Solution;
@property (retain , nonatomic) NSString *is_hide;
@property (retain , nonatomic) NSString *old_question_id;
@property (retain , nonatomic) NSString *isTextual;
@property (retain , nonatomic) NSString *CreatedBy;
@property (retain , nonatomic) NSString *CreatedOn;
@property (retain , nonatomic) NSString *ModifiedBy;
@property (retain , nonatomic) NSString *ModifiedOn;

////////////
@property (retain , nonatomic) NSString *ExamTypePatternDetailID;
@property (retain , nonatomic) NSString *ExamTypePatternID;
@property (retain , nonatomic) NSString *ExamTypeID;
//@property (retain , nonatomic) NSString *QuestionTypeID;
@property (retain , nonatomic) NSString *QuestionNO;
@property (retain , nonatomic) NSString *SubQuestionNO;
@property (retain , nonatomic) NSString *QuestionTypeText;
@property (retain , nonatomic) NSString *QuestionMarks;
@property (retain , nonatomic) NSString *NoOfQuestion;
@property (retain , nonatomic) NSString *DisplayOrder;
@property (retain , nonatomic) NSString *isQuestion;
@property (retain , nonatomic) NSString *isQuestionVisible;
//@property (retain , nonatomic) NSString *ChapterID;

@property (retain , nonatomic) NSString *PageNo;


@end
