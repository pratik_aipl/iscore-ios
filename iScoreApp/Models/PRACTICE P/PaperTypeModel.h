//
//  PaperTypeModel.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 5/28/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <objc/runtime.h>

@interface PaperTypeModel : NSObject <NSCoding>

@property (retain , nonatomic) NSString *PaperTypeID;
@property (retain , nonatomic) NSString *PaperTypeName;
@property (retain , nonatomic) NSString *TotalTest;
@property (retain , nonatomic) NSString *last_attempt;


- (NSString *) className;

+ (PaperTypeModel*) parseObjectWithFMResultSet:(NSDictionary *)mDict;
- (void)getValues:(NSCoder *)aCoder;

@end
