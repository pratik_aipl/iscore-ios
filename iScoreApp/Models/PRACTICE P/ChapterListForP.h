//
//  ChapterListForP.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 5/29/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <objc/runtime.h>

@interface ChapterListForP : NSObject<NSCoding>


//// add your properties
@property (retain , nonatomic) NSString *SubjectName;
@property (retain , nonatomic) NSString *SubjectIcon;

@property (retain , nonatomic) NSString *ChapterID;
@property (retain , nonatomic) NSString *ParentChapterID;
@property (retain , nonatomic) NSString *ChapterNumber;
@property (retain , nonatomic) NSString *BoardID;
@property (retain , nonatomic) NSString *MediumID;
@property (retain , nonatomic) NSString *StandardID;
@property (retain , nonatomic) NSString *SubjectID;
@property (retain , nonatomic) NSString *ChapterName;
@property (retain , nonatomic) NSString *isMCQ;
@property (retain , nonatomic) NSString *CreatedBy;
@property (retain , nonatomic) NSString *CreatedOn;
@property (retain , nonatomic) NSString *ModifiedBy;
@property (retain , nonatomic) NSString *ModifiedOn;
@property (retain , nonatomic) NSString *isGeneratePaper ;

@property (retain , nonatomic) NSString *DisplayOrder ;
@property (retain , nonatomic) NSString *WeightageMarks ;
@property (retain , nonatomic) NSString *isMandatory ;
@property (retain , nonatomic) NSString *isLibrary ;
@property (retain , nonatomic) NSString *isDemo ;

/// only change above

- (NSString *) className;
-(NSObject*) parseObjectWithFMResultSet:(NSDictionary *)mDict;
- (void)getValues:(NSCoder *)aCoder;


@end
