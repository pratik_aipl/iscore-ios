//
//  ETPDetailsModel.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 6/5/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <objc/runtime.h>

@interface ETPDetailsModel : NSObject<NSCoding>


@property (retain , nonatomic) NSString *ExamTypePatternDetailID;
@property (retain , nonatomic) NSString *ExamTypePatternID;
@property (retain , nonatomic) NSString *ExamTypeID;
@property (retain , nonatomic) NSString *QuestionTypeID;
@property (retain , nonatomic) NSString *QuestionNO;
@property (retain , nonatomic) NSString *SubQuestionNO;
@property (retain , nonatomic) NSString *QuestionTypeText;
@property (retain , nonatomic) NSString *QuestionMarks;
@property (retain , nonatomic) NSString *NoOfQuestion;
@property (retain , nonatomic) NSString *DisplayOrder;
@property (retain , nonatomic) NSString *isQuestion;
@property (retain , nonatomic) NSString *isQuestionVisible;
@property (retain , nonatomic) NSString *ChapterID;
@property (retain , nonatomic) NSString *CreatedBy;
@property (retain , nonatomic) NSString *CreatedOn;
@property (retain , nonatomic) NSString *ModifiedBy;
@property (retain , nonatomic) NSString *ModifiedOn;

@property (retain , nonatomic) NSString *PageNo;

@end
