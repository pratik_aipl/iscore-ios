//
//  SetPaperQuestionModel.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 6/26/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <objc/runtime.h>

@interface SetPaperQuestionModel : NSObject<NSCoding>

@property (retain , nonatomic) NSString *StudentSetPaperDetailID;
@property (retain , nonatomic) NSString *isPassage;
@property (retain , nonatomic) NSString *Question;
@property (retain , nonatomic) NSString *Answer;
@property (retain , nonatomic) NSString *MQuestionID;
@property (retain , nonatomic) NSString *QuestionType;
@property (retain , nonatomic) NSString *QuestionTypeID;
@property (retain , nonatomic) NSString *TotalAsk;
@property (retain , nonatomic) NSString *ToAnswer;
@property (retain , nonatomic) NSString *TotalMark;



- (NSString *) className;


+ (SetPaperQuestionModel*) parseObjectWithFMResultSet:(NSDictionary *)mDict;
- (void)getValues:(NSCoder *)aCoder;

@end
