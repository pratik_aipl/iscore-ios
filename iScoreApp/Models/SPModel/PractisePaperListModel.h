//
//  PractisePaperListModel.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 7/25/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <objc/runtime.h>

@interface PractisePaperListModel : NSObject<NSCoding>

@property (retain , nonatomic) NSString *StudentQuestionPaperID;
@property (retain , nonatomic) NSString *SubjectName;
@property (retain , nonatomic) NSString *CreatedOn;
@property (retain , nonatomic) NSString *ExamTypeName;
@property (retain , nonatomic) NSString *Duration;
@property (retain , nonatomic) NSString *TotalMarks;
@property (retain , nonatomic) NSString *ExamTypePatternID;
@property (retain , nonatomic) NSString *PaperTypeName;
@property (retain , nonatomic) NSString *PaperTypeID;
@property (retain , nonatomic) NSString *SetPaperMarks;
@property (retain , nonatomic) NSString *SetPaperDuration;



- (NSString *) className;

+ (PractisePaperListModel*) parseObjectWithFMResultSet:(NSDictionary *)mDict;
- (void)getValues:(NSCoder *)aCoder;


@end
