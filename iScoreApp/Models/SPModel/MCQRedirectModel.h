//
//  MCQRedirectModel.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 7/28/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <objc/runtime.h>

@interface MCQRedirectModel : NSObject<NSCoding>

@property (retain , nonatomic) NSString *StudentMCQTestDTLID;
@property (retain , nonatomic) NSString *StudentMCQTestHDRID;
@property (retain , nonatomic) NSString *QuestionID;
@property (retain , nonatomic) NSString *AnswerID;
@property (retain , nonatomic) NSString *IsAttempt;
@property (retain , nonatomic) NSString *srNo;
@property (retain , nonatomic) NSString *CreatedBy;
@property (retain , nonatomic) NSString *CreatedOn;
@property (retain , nonatomic) NSString *ModifiedOn;
@property (retain , nonatomic) NSString *ModifiedBy;

//-----------------

/*
@property (retain , nonatomic) NSString *MCQQuestionID;
@property (retain , nonatomic) NSString *Solution;
@property (retain , nonatomic) NSString *Question;
@property (retain , nonatomic) NSString *ImageURL;
@property (retain , nonatomic) NSString *BoardID;
@property (retain , nonatomic) NSString *PageNumberID;
@property (retain , nonatomic) NSString *is_demo;
@property (retain , nonatomic) NSString *MediumID;
@property (retain , nonatomic) NSString *ClassID;
@property (retain , nonatomic) NSString *StanderdID;
@property (retain , nonatomic) NSString *SubjectID;
@property (retain , nonatomic) NSString *ChapterID;
@property (retain , nonatomic) NSString *QuestionTypeID;
@property (retain , nonatomic) NSString *QuestionLevelID;
@property (retain , nonatomic) NSString *Duration;
*/


- (NSString *) className;

+ (MCQRedirectModel*) parseObjectWithFMResultSet:(NSDictionary *)mDict;
- (void)getValues:(NSCoder *)aCoder;



@end
