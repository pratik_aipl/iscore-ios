//
//  MCQFilterModel.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 7/24/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <objc/runtime.h>

@interface MCQFilterModel : NSObject<NSCoding>

@property (retain , nonatomic) NSString *SubjectID;
@property (retain , nonatomic) NSString *SubjectName;
@property (retain , nonatomic) NSString *accuracy1;
@property (retain , nonatomic) NSString *Total_Right;
@property (retain , nonatomic) NSString *Total_que;
@property (retain , nonatomic) NSString *StudentMCQTestHDRID;
@property (retain , nonatomic) NSString *CreatedOn;


- (NSString *) className;

+ (MCQFilterModel*) parseObjectWithFMResultSet:(NSDictionary *)mDict;
- (void)getValues:(NSCoder *)aCoder;

@end

