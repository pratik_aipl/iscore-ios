//
//  AllSubjectModel.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 7/24/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <objc/runtime.h>

@interface AllSubjectModel : NSObject <NSCoding>

@property (retain , nonatomic) NSString *SubjectID;
@property (retain , nonatomic) NSString *SubjectName;
@property (retain , nonatomic) NSString *BoardID;
@property (retain , nonatomic) NSString *MediumID;
@property (retain , nonatomic) NSString *StandardID;
@property (retain , nonatomic) NSString *SubjectIcon;
@property (retain , nonatomic) NSString *Price;
@property (retain , nonatomic) NSString *SubjectOrder;
@property (retain , nonatomic) NSString *isGeneratePaper;
@property (retain , nonatomic) NSString *isMCQ;
@property (retain , nonatomic) NSString *isWEIGHTAGE;
@property (retain , nonatomic) NSString *instruction;
@property (retain , nonatomic) NSString *isTexual;
@property (retain , nonatomic) NSString *isSimilar;




- (NSString *) className;

+ (AllSubjectModel*) parseObjectWithFMResultSet:(NSDictionary *)mDict;
- (void)getValues:(NSCoder *)aCoder;

@end
