//
//  ZookiModel.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 9/14/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//
#import <Foundation/Foundation.h>
#import <objc/runtime.h>

@interface ZookiModel : NSObject<NSCoding>

//// add your properties
@property (retain , nonatomic) NSString *image;
@property (retain , nonatomic) NSString *Desc;
@property (retain , nonatomic) NSString *ZookiID;
@property (retain , nonatomic) NSString *CreatedOn;
@property (retain , nonatomic) NSString *Title;

/// only change above

- (NSString *) className;


+ (ZookiModel*) parseObjectWithFMResultSet:(NSDictionary *)mDict;
- (void)getValues:(NSCoder *)aCoder;

@end

