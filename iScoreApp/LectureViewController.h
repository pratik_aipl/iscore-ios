//
//  LectureViewController.h
//  iScoreApp
//
//  Created by My Mac on 10/04/20.
//  Copyright © 2020 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FSCalendar.h"
NS_ASSUME_NONNULL_BEGIN

@interface LectureViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet FSCalendar *calender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *calendarHeightConstraint;
@property (strong, nonatomic) IBOutlet UILabel *lblnodata;
- (IBAction)back_click:(UIButton *)sender;
@end

NS_ASSUME_NONNULL_END
