//
//  SSMedium.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 8/27/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AFNetworking/AFNetworking.h>

@interface SSMedium : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    NSMutableArray* marrSelect;
    NSMutableArray* cells_Array;
    
    int selectedIndex;
    int status;
    
    NSMutableArray* mboard;
    NSMutableArray* mboardID;
    
}

@property (retain , nonatomic)NSMutableDictionary *dictdata;

//Outlet
@property (weak, nonatomic) IBOutlet UITableView *tbl_medium;

@property (strong, nonatomic) IBOutlet UIButton *btn_arr_priv;
@property (strong, nonatomic) IBOutlet UIButton *btn_arr_next;

//Action
- (IBAction)btn_ARR_NEXT_A:(id)sender;
- (IBAction)btn_ARR_PRIV_A:(id)sender;



@end
