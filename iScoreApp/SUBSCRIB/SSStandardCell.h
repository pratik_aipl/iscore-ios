//
//  SSStandardCell.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 8/27/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SSStandardCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UIView *vw_std;
@property (weak, nonatomic) IBOutlet UILabel *lbl_stdnm;
@property (weak, nonatomic) IBOutlet UILabel *lbl_price;
@property (weak, nonatomic) IBOutlet UILabel *lbl_exp;

@end
