//
//  SSMediumCell.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 8/27/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SSMediumCell : UITableViewCell


@property (strong, nonatomic) IBOutlet UILabel *lbl_name;
@property (strong, nonatomic) IBOutlet UIButton *btn_checkBtn;

@end
