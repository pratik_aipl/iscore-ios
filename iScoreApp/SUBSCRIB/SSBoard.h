//
//  SSBoard.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 8/23/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AFNetworking/AFNetworking.h>

@interface SSBoard : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
    
    NSMutableArray* marrSelect;
    NSMutableArray* cells_Array;
    
    int selectedIndex;
    int status;
    
    NSMutableArray* board;
    NSMutableArray* boardID;
}

@property (retain , nonatomic)NSMutableDictionary *dictdata;
@property (weak, nonatomic) IBOutlet UIButton *btn_arr_priv;
@property (weak, nonatomic) IBOutlet UIButton *btn_arr_next;


@property (weak, nonatomic) IBOutlet UITableView *tbl_board;


- (IBAction)btn_NEXT:(id)sender;
- (IBAction)btn_PRIV:(id)sender;


@end
