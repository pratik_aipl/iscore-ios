//
//  SSBoardCell.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 8/23/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SSBoardCell : UITableViewCell




@property (weak, nonatomic) IBOutlet UILabel *txt_title;
@property (weak, nonatomic) IBOutlet UIButton *btn_check;

- (IBAction)btn_SELECTOP:(id)sender;

@end
