//
//  SSBoard.m
//  iScoreApp
//
//  Created by ADMIN-Khushal on 8/23/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "SSBoard.h"
#import "SSBoardCell.h"
#import "ApplicationConst.h"
#import "SSMedium.h"



@interface SSBoard ()

@end

@implementation SSBoard

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _btn_arr_priv.layer.cornerRadius=_btn_arr_priv.frame.size.height/2;
    _btn_arr_next.layer.cornerRadius=_btn_arr_next.frame.size.height/2;
    
    marrSelect = [[NSMutableArray alloc] initWithObjects:@"0", nil];

    board=[[NSMutableArray alloc]init];
    
    [self CallSelectBoard];
    //[self CallChangeSyncDate];
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark TableViewDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return board.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Cell";
    SSBoardCell *cell =
    [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc]
                initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:cellIdentifier owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    cell.txt_title.text=[NSString stringWithFormat:@"%@",[board objectAtIndex:indexPath.row]];
    cell.btn_check.userInteractionEnabled = NO;
    [cell.btn_check setSelected:false];
    if ([[marrSelect objectAtIndex:indexPath.section] isEqualToString:[NSString stringWithFormat:@"%ld",(long)indexPath.row]])
    {
        [cell.btn_check setSelected:true];
    }
    [cells_Array addObject:cell];
    return cell;
    
}


- (void)checkBoxClicked:(id)sender event:(id)event
{
    NSSet *touches = [event allTouches];
    UITouch *touch = [touches anyObject];
    CGPoint currentTouchPosition = [touch locationInView:_tbl_board]; //here tv is TableView Object
    NSIndexPath *indexPath1 = [_tbl_board indexPathForRowAtPoint: currentTouchPosition];
    
    NSLog(@"value of indePath.section %ld ,indexPath.row %ld",(long)indexPath1.section,(long)indexPath1.row);
    
    UIButton *button=(UIButton *) sender;
    NSIndexPath *indexpath = [NSIndexPath indexPathForRow:indexPath1.row inSection:indexPath1.section];
    SSBoardCell *tappedCell = (SSBoardCell*)[_tbl_board cellForRowAtIndexPath:indexpath];
    
   /*
    if( [[tappedCell.btn_checkBtn imageForState:UIControlStateNormal] isEqual:[UIImage imageNamed:@"unselect.png"]])
    {
        [tappedCell.btn_checkBtn setImage:[UIImage imageNamed:@"radio-unselect.png"] forState:UIControlStateNormal];
    }
    else
    {
        [tappedCell.btn_checkBtn setImage:[UIImage imageNamed:@"radio-select.png"] forState:UIControlStateSelected];
    }*/
    
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath* )indexPath
{
    
    SSBoardCell *cellView = [_tbl_board cellForRowAtIndexPath:indexPath];

    [marrSelect replaceObjectAtIndex:indexPath.section withObject:[NSString stringWithFormat:@"%ld",(long)indexPath.row]];
    
    selectedIndex = (int)indexPath.row;
    NSLog(@"%d",selectedIndex);
    
    
    
    [tableView reloadData];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50.0f;
}


- (IBAction)btn_NEXT:(id)sender {
    
    NSString * value = [NSString stringWithFormat:@"%@",[board objectAtIndex:selectedIndex]];
    NSLog( @"%@",value);
    _dictdata=[[NSMutableDictionary alloc]init];
    
    [_dictdata setValue:value forKey:@"BoardName"];
    [_dictdata setValue:[boardID objectAtIndex:selectedIndex] forKey:@"BoardID"];
    
    [self CallSelectMedium];
    
    
}

/////////////////////////NEXT PAGE API////////
-(void)CallSelectMedium
{
    [APP_DELEGATE showLoadingView:@""];
    
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    [AddPost setValue:[_dictdata valueForKey:@"BoardID"] forKey:@"board_id"];
    //[AddPost setValue:@"1" forKey:@"board_id"];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    [manager GET:[NSString stringWithFormat:@"%@getMediums",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject){
        NSLog(@"JSON: %@", responseObject);
        status=[[responseObject valueForKey:@"status"] intValue];
        
       if (status==1) {
            SSMedium * next = [self.storyboard instantiateViewControllerWithIdentifier:@"SSMedium"];
            next.dictdata=_dictdata;
            [self.navigationController pushViewController:next animated:NO];
        }
        
        [APP_DELEGATE hideLoadingView];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [APP_DELEGATE hideLoadingView];
        NSLog(@"Error: %@", error);
    }];
}

- (IBAction)btn_PRIV:(id)sender {
     [self.navigationController popViewControllerAnimated:NO];
}


-(void)CallSelectBoard
{
    [APP_DELEGATE showLoadingView:@""];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    [manager POST:[NSString stringWithFormat:@"%@getBoards",BaseURLAPI] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        if ([[responseObject valueForKey:@"status"] intValue]==1) {
            responseObject =[responseObject valueForKey:@"data"];
            NSLog(@"-- %@",responseObject);
            board=[[NSMutableArray alloc]init];
            boardID=[[NSMutableArray alloc]init];
            for (int i=0; i<[responseObject count]; i++) {
                [board addObject:[[responseObject objectAtIndex:i]valueForKey:@"BoardFullName"]];
                [boardID addObject:[[responseObject objectAtIndex:i]valueForKey:@"BoardID"]];
                
            }
            [_tbl_board reloadData];
            [APP_DELEGATE hideLoadingView];
        }
        
        [APP_DELEGATE hideLoadingView];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [APP_DELEGATE hideLoadingView];
        NSLog(@"Error: %@", error);
    }];
    
}


@end
