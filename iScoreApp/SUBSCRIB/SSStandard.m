//
//  SSStandard.m
//  iScoreApp
//
//  Created by ADMIN-Khushal on 8/27/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "SSStandard.h"
#import "SSStandardCell.h"
#import "ApplicationConst.h"



@interface SSStandard ()

@end

@implementation SSStandard

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    arrStd=[[NSMutableArray alloc]init];
    
    for (int i=0; i<4; i++) {
        [arrStd addObject:@"0"];
    }
    
    _btn_arr_priv.layer.cornerRadius=_btn_arr_priv.frame.size.height/2;
    _btn_arr_next.layer.cornerRadius=_btn_arr_next.frame.size.height/2;
    
   // [self CallSelectSTD];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _dictdata.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Cell";
    SSStandardCell *cell =
    [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc]
                initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:cellIdentifier owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
     [self setShadow:cell.vw_std];
    
    if ([[arrStd objectAtIndex:indexPath.row] intValue] == 1) {
        cell.vw_std.layer.borderColor=[UIColor redColor].CGColor;
        cell.vw_std.layer.borderWidth=0.8;
    }
    else
    {
        cell.vw_std.layer.borderColor=[UIColor clearColor].CGColor;
        cell.vw_std.layer.borderWidth=0;
    }
    
    ///////////////
    cell.lbl_stdnm.text=[NSString stringWithFormat:@"%@ Standard",[_dictdata valueForKey:@"StandardName"][indexPath.row]];
    cell.lbl_price.text=[NSString stringWithFormat:@"%@",[_dictdata valueForKey:@"Price"][indexPath.row]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Update the cell's appearance somewhere here
    [_tbl_std deselectRowAtIndexPath:indexPath animated:NO];
    
    
    for (int i=0; i<4; i++) {
        if (i == indexPath.row) {
            [arrStd replaceObjectAtIndex:i withObject:@"1"];
        }
        else
        {
            [arrStd replaceObjectAtIndex:i withObject:@"0"];
        }
    }
    
    [_tbl_std reloadData];
    
}


-(void)setShadow :(UIView*)txtV
{
    
    txtV.layer.shadowRadius  = 1.5f;
    txtV.layer.shadowColor   = [UIColor lightGrayColor].CGColor;
    txtV.layer.shadowOffset  = CGSizeMake(0.0f, 0.0f);
    txtV.layer.shadowOpacity = 0.9f;
    txtV.layer.masksToBounds = NO;
    
    UIEdgeInsets shadowInsets     = UIEdgeInsetsMake(0, 0, -1.5f, 0);
    UIBezierPath *shadowPath      = [UIBezierPath bezierPathWithRect:UIEdgeInsetsInsetRect(txtV.bounds, shadowInsets)];
    txtV.layer.shadowPath    = shadowPath.CGPath;
}




@end
