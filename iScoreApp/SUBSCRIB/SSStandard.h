//
//  SSStandard.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 8/27/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AFNetworking/AFNetworking.h>

@interface SSStandard : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    NSMutableArray *arrStd;
}


@property (retain , nonatomic)NSMutableDictionary *dictdata;


//Outlet
@property (weak, nonatomic) IBOutlet UITableView *tbl_std;

@property (strong, nonatomic) IBOutlet UIButton *btn_arr_priv;
@property (strong, nonatomic) IBOutlet UIButton *btn_arr_next;

//Action
- (IBAction)btn_ARR_NEXT_A:(id)sender;
- (IBAction)btn_ARR_PRIV_A:(id)sender;



@end
