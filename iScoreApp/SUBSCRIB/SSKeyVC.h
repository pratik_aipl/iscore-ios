//
//  SSKeyVC.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 8/28/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SSKeyVC : UIViewController
{
    NSMutableDictionary *dictdata,*ParseData;
}


//Outlet
@property (strong, nonatomic) IBOutlet UIView *vw_activkey;
@property (strong, nonatomic) IBOutlet UIImageView *img_key_icon;
@property (strong, nonatomic) IBOutlet UIButton *btn_next_b;
@property (strong, nonatomic) IBOutlet UIView *vw_mob_icon;

@property (weak, nonatomic) IBOutlet UITextField *txt_key;


- (IBAction)btn_NEXT_A:(id)sender;
- (IBAction)btn_BACK:(id)sender;



@end
