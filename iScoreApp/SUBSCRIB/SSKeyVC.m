//
//  SSKeyVC.m
//  iScoreApp
//
//  Created by ADMIN-Khushal on 8/28/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "SSKeyVC.h"
#import "UIColor+CL.h"
#import "ApplicationConst.h"
#import <AFNetworking/AFNetworking.h>
#import "DownloadVC.h"




@interface SSKeyVC ()

@end

@implementation SSKeyVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _vw_activkey.layer.borderWidth=1.0;
    _vw_activkey.layer.borderColor=[UIColor colorWithHex:0x0A385A].CGColor;
    _vw_activkey.layer.cornerRadius=5;
    
    _btn_next_b.layer.cornerRadius=3;
    
    
    
    //Mobile Number
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.vw_mob_icon.bounds byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerBottomLeft) cornerRadii:CGSizeMake(5.0, 5.0)];
    
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.view.bounds;
    maskLayer.path  = maskPath.CGPath;
    self.vw_mob_icon.layer.mask = maskLayer;
    
    [_txt_key addTarget:self action:@selector(textFieldDidChange1:) forControlEvents:UIControlEventEditingChanged];
    
    
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)CallActiveKey
{
    
    [APP_DELEGATE showLoadingView:@""];
    
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    [AddPost setValue:_txt_key.text forKey:@"app_key"];
    [AddPost setValue:[[NSUserDefaults standardUserDefaults]valueForKeyPath:@"ParseData.StudentID"] forKey:@"StudID"];
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    [manager GET:[NSString stringWithFormat:@"%@change_class_key?",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject){
        NSLog(@"JSON: %@", responseObject);
        
        [APP_DELEGATE hideLoadingView];
        
        if ([[responseObject valueForKey:@"status"] intValue]==1) {
            
            [[NSUserDefaults standardUserDefaults]setObject:responseObject forKey:@"TEMP"];
            
            [self DataParsing];
            
            
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                            message:[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"message"]]
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }
        
        [APP_DELEGATE hideLoadingView];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];
    }];
    
}



-(void)DataParsing
{
    
    dictdata=[[NSMutableDictionary alloc]init];
    dictdata=[[NSUserDefaults standardUserDefaults] valueForKey:@"TEMP"];
    
    
    if (dictdata.count>0) {
        dictdata=[dictdata valueForKey:@"data"];
        
        /*ParseData=[[NSMutableDictionary alloc]init];
        [ParseData setDictionary:[[dictdata valueForKey:@"KeyData"] objectAtIndex:0]];
        NSLog(@"temp dict %@",ParseData);
        [ParseData setValue:[dictdata valueForKey:@"EmailID"] forKey:@"EmailID"];
        [ParseData setValue:[dictdata valueForKey:@"FirstName"] forKey:@"FirstName"];
        [ParseData setValue:[dictdata valueForKey:@"FlowFlag"] forKey:@"FlowFlag"];
        [ParseData setValue:[dictdata valueForKey:@"IMEINo"] forKey:@"IMEINo"];
        [ParseData setValue:[dictdata valueForKey:@"LastName"] forKey:@"LastName"];
        [ParseData setValue:[dictdata valueForKey:@"MOTP"] forKey:@"MOTP"];
        [ParseData setValue:[dictdata valueForKey:@"ProfileImage"] forKey:@"ProfileImage"];
        [ParseData setValue:[dictdata valueForKey:@"RegMobNo"] forKey:@"RegMobNo"];
        [ParseData setValue:[dictdata valueForKey:@"SchoolName"] forKey:@"SchoolName"];
        [ParseData setValue:[dictdata valueForKey:@"StudentCode"] forKey:@"StudentCode"];
        [ParseData setValue:[dictdata valueForKey:@"StudentID"] forKey:@"StudentID"];
        
        NSLog(@"Parse dtata %@",ParseData);
        
        [[NSUserDefaults standardUserDefaults] setObject:ParseData forKey:@"ParseData"];
        [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:@"DataDownloadStatus"];
        */
        
        DownloadVC * next = [self.storyboard instantiateViewControllerWithIdentifier:@"DownloadVC"];
        next.dictdata=dictdata;
        [self.navigationController pushViewController:next animated:NO];
        
    }
    
}



- (IBAction)btn_NEXT_A:(id)sender {
    
    [self CallActiveKey];
    
}

- (IBAction)btn_BACK:(id)sender {
    
     [self.navigationController popViewControllerAnimated:NO];
}

-(void)textFieldDidChange1 :(UITextField *)theTextField
{
    
    
    if ([theTextField.text length]<=12) {
        NSLog(@"ok");
        _txt_key.text =[NSString stringWithFormat:@"%ld",[theTextField.text integerValue]];
        
        
    }
    else
    {
        NSString *message = [NSString stringWithFormat:@"Should not more than 12 digit"];
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil
                                                                       message:message
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:nil];
        
        int duration = 2; // duration in seconds
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [alert dismissViewControllerAnimated:YES completion:nil];
        });
        
        NSLog(@"NOT OK");
        NSString *lastString=[_txt_key.text substringToIndex:[_txt_key.text length] - 1];;
        [_txt_key setText:lastString];
    }
    
}


@end
