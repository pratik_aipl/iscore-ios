//
//  DownloadVC.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 3/1/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DownloadVC : UIViewController
{
    NSMutableDictionary *ParseData;
    int CallFlag;
    int progVal;
    int totrecord;
    
    BOOL isInternet;
    
    
    

}

@property (retain,nonatomic )NSMutableDictionary *dictdata;


//Outlet
@property (strong, nonatomic) IBOutlet UIProgressView *pro_download;
@property (strong, nonatomic) IBOutlet UILabel *lbl_pers;

@property (strong, nonatomic) IBOutlet UIView *view_indicator;
@property (strong, nonatomic) IBOutlet UILabel *lbl_ass_picture;





//Action
- (IBAction)btn_SKIP_EXTRA:(id)sender;
- (void) internetDisconnectAlert;


@end
