//
//  LoginVC.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 2/23/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TextFieldValidator.h"
//#import <AFNetworking/AFNetworking.h>

@interface LoginVC : UIViewController<UITextFieldDelegate>
{
    BOOL isInternet;
}


@property (retain , nonatomic)NSMutableDictionary *dictdata;
//Outlets
@property (strong, nonatomic) IBOutlet UITextField *txt_mobile;

@property (strong, nonatomic) IBOutlet UIView *vw_mobile_verify;
@property (strong, nonatomic) IBOutlet UIImageView *img_mobile_icon;
@property (strong, nonatomic) IBOutlet UIButton *btn_login;
@property (strong, nonatomic) IBOutlet UIView *vw_mob_icon;

@property (strong, nonatomic) IBOutlet UIView *vw_activkey;
@property (strong, nonatomic) IBOutlet UIView *vwuserid;
@property (strong, nonatomic) IBOutlet UIImageView *img_key_icon;
@property (strong, nonatomic) IBOutlet UIButton *btn_next_b;

@property (strong, nonatomic) IBOutlet TextFieldValidator *txt_akey;

//Action
- (IBAction)btn_LOGIN_A:(id)sender;








//Action
- (IBAction)btn_BACK:(id)sender;


@end
