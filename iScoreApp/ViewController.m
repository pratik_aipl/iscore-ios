//
//  ViewController.m
//  iScoreApp
//
//  Created by ADMIN-Khushal on 2/23/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "ViewController.h"
#import "LoginVC.h"
#import "SignupActivationKeyVC.h"
#import "OTPVC.h"
#import "SSubVC.h"
#import "DownloadVC.h"
#import "HomeVC.h"
#import "SelectBoardVC.h"



@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.navigationController.navigationBarHidden=YES;
    _btn_login.layer.cornerRadius=3;
    _btn_signup.layer.cornerRadius=3;
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)btn_LOGIN_ACTION:(id)sender {
    
    LoginVC * next = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
    [self.navigationController pushViewController:next animated:YES];
    
  /*  DownloadVC * next = [self.storyboard instantiateViewControllerWithIdentifier:@"DownloadVC"];
    [self.navigationController pushViewController:next animated:YES];*/
    
    /*SelectBoardVC * next = [self.storyboard instantiateViewControllerWithIdentifier:@"SelectBoardVC"];
    next.dictdata=_dictdata;
    [self.navigationController pushViewController:next animated:YES];
     */
    
}

- (IBAction)btn_SIGNUP_ACTION:(id)sender {
    SignupActivationKeyVC * next = [self.storyboard instantiateViewControllerWithIdentifier:@"SignupActivationKeyVC"];
    [self.navigationController pushViewController:next animated:YES];
    
   /* SSubVC *viewController=[[SSubVC alloc]initWithNibName:@"SSubVC" bundle:nil];
    [self.navigationController pushViewController:viewController animated:YES];*/
    
  /*  ReadyPVC *viewController=[[ReadyPVC alloc]initWithNibName:@"ReadyPVC" bundle:nil];
    [self.navigationController pushViewController:viewController animated:YES];*/
    
    
}
@end
