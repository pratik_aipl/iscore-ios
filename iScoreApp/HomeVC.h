//
//  HomeVC.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 3/1/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "Utils.h"

#import "MyDBQueries.h"
#import "MyDBResultParser.h"


@interface HomeVC : UIViewController<UICollectionViewDataSource,UICollectionViewDelegate>
{
    
    
    MyDBQueries *myDbQueris;
    MyDBResultParser *myDBParser;
    NSMutableArray *arrZooki ;
    NSString *filePath;
    
    UIView *picker;
    
    //Utils *util;
    
    NSMutableDictionary* ParseData;
    
    UILabel *ProfileLastSync;
    
    CGFloat difference1,difference2;
}


@property (retain ,nonatomic )NSString *shareApp;

//Outlet
@property (strong, nonatomic) IBOutlet UIButton *btn_menu;
@property (strong, nonatomic) IBOutlet UIImageView *img_user_prof;

@property (strong, nonatomic) IBOutlet UILabel *user_nm;
@property (strong, nonatomic) IBOutlet UILabel *std_id;
@property (strong, nonatomic) IBOutlet UILabel *lbl_board;
@property (strong, nonatomic) IBOutlet UILabel *lbl_medium;
@property (strong, nonatomic) IBOutlet UILabel *lbl_class;
@property (weak, nonatomic) IBOutlet UIView *vw_allmenu;
@property (weak, nonatomic) IBOutlet UIScrollView *scroll_main;
@property (weak, nonatomic) IBOutlet UIView *vw_zooki;
@property (weak, nonatomic) IBOutlet UIView *vw_1;






- (IBAction)menu_click:(UIButton *)sender;
//Acction
- (IBAction)btn_MENU_A:(id)sender;
- (IBAction)btn_MCQTEST:(id)sender;
- (IBAction)btn_PRACTISE:(id)sender;
- (IBAction)btn_REVISE:(id)sender;
- (IBAction)btn_MYREPORT:(id)sender;
- (IBAction)btn_INSTITUTE:(id)sender;
- (IBAction)btn_SEARCHPAPER:(id)sender;







@end
