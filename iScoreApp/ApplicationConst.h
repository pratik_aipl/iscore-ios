//
//  ApplicationConst.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 4/6/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#ifndef ApplicationConst_h
#define ApplicationConst_h

#import "AppDelegate.h"

//#define BaseURLAPI @"https://test.escore.parshvaa.com/web_services/version_39/web_services/"

//#define BaseURLAPI @"https://test.pemiscore.parshvaa.com/web_services/version_40/web_services/"
#define BaseURLAPI @"https://staff.parshvaa.com/web_services/version_58/web_services/"

//#define ICAPIURL @"https://test.pemiscore.parshvaa.com/web_services/version_58/evaluator_web_services/"
#define ICAPIURL @"https://staff.parshvaa.com/web_services/version_58/evaluator_web_services/"

#define VIEW_PAPER_EVALUTER_URL @"https://evaluater.parshvaa.com/class-admin/web-services-paper/"

#define APP_DELEGATE ((AppDelegate*)[[UIApplication sharedApplication] delegate])



//------------------- LIVE SERVER--------------------------------------
/*
#define BASIC_URL = "https://staff.parshvaa.com/web_services/";
#define BASIC_VIEW_PAPER_EVA_URL = "https://evaluater.parshvaa.com/";
#define URL_VERSION = "version_56/";
*/

//------------------- PEMP SERVER--------------------------------------

/*#define BASIC_URL = "https://test.pemiscore.parshvaa.com/web_services/";
 #define BASIC_VIEW_PAPER_EVA_URL = "https://test.pemevaluater.parshvaa.com/";
 #define URL_VERSION = "version_39/";*/

//------------------- TEST SERVER--------------------------------------
// #define BASIC_URL = "https://test.escore.parshvaa.com/web_services/";
// #define BASIC_VIEW_PAPER_EVA_URL = "https://test.evaluator.parshvaa.com/";
// #define URL_VERSION = "version_39/";

#define URL = Constant.BASIC_URL + Constant.URL_VERSION + "web_services/";

#define IURL = Constant.BASIC_URL + Constant.URL_VERSION + "evaluator_web_services/";


#endif /* ApplicationConst_h */
