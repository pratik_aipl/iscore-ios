//
//  LectureViewController.m
//  iScoreApp
//
//  Created by My Mac on 10/04/20.
//  Copyright © 2020 ADMIN-Khushal. All rights reserved.
//

#import "LectureViewController.h"
#import "LectureCell.h"
#import <AFNetworking/AFNetworking.h>
#import "ApplicationConst.h"
#import <QuartzCore/QuartzCore.h>
NS_ASSUME_NONNULL_BEGIN

@interface LectureViewController ()<UITableViewDelegate,UITableViewDataSource,FSCalendarDataSource,FSCalendarDelegate,UIGestureRecognizerDelegate>
{
        NSMutableArray *arrlist,*arrdates,*arrplanerdata;
     void * _KVOContext;
    NSString *month,*selecteddate;
}
@property (strong, nonatomic) NSDateFormatter *dateFormatter;
@property (strong, nonatomic) UIPanGestureRecognizer *scopeGesture;
@end

NS_ASSUME_NONNULL_END

@implementation LectureViewController

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        self.dateFormatter = [[NSDateFormatter alloc] init];
        self.dateFormatter.dateFormat = @"yyyy/MM/dd";
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self.calender action:@selector(handleScopeGesture:)];
      panGesture.delegate = self;
      panGesture.minimumNumberOfTouches = 1;
      panGesture.maximumNumberOfTouches = 2;
      [self.view addGestureRecognizer:panGesture];
      self.scopeGesture = panGesture;
      
      // While the scope gesture begin, the pan gesture of tableView should cancel.
      [self.tableView.panGestureRecognizer requireGestureRecognizerToFail:panGesture];
      
    [self.calender addObserver:self forKeyPath:@"scope" options:NSKeyValueObservingOptionNew|NSKeyValueObservingOptionOld context:_KVOContext];
    self.calender.placeholderType = FSCalendarPlaceholderTypeNone;
    self.calender.scope = FSCalendarScopeMonth;
      
    [self.calender selectDate:[NSDate date] scrollToDate:YES];
      
      // For UITest
    self.calender.accessibilityIdentifier = @"calendar";
      
     NSDateFormatter *df = [[NSDateFormatter alloc] init];

     [df setDateFormat:@"MM/yyyy"];
     month = [df stringFromDate:[NSDate date]];
    [self CallLesserPlannerDates];
    _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;

}

- (void)dealloc
{
    [self.calender removeObserver:self forKeyPath:@"scope" context:_KVOContext];
    NSLog(@"%s",__FUNCTION__);
}

#pragma mark - KVO

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context
{
    if (context == _KVOContext) {
        FSCalendarScope oldScope = [change[NSKeyValueChangeOldKey] unsignedIntegerValue];
        FSCalendarScope newScope = [change[NSKeyValueChangeNewKey] unsignedIntegerValue];
        NSLog(@"From %@ to %@",(oldScope==FSCalendarScopeWeek?@"week":@"month"),(newScope==FSCalendarScopeWeek?@"week":@"month"));
    } else {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

#pragma mark - <UIGestureRecognizerDelegate>

// Whether scope gesture should begin
- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    BOOL shouldBegin = self.tableView.contentOffset.y <= -self.tableView.contentInset.top;
    if (shouldBegin) {
        CGPoint velocity = [self.scopeGesture velocityInView:self.view];
        switch (self.calender.scope) {
            case FSCalendarScopeMonth:
                return velocity.y < 0;
            case FSCalendarScopeWeek:
                return velocity.y > 0;
        }
    }
    return shouldBegin;
}

#pragma mark - <FSCalendarDelegate>

- (void)calendar:(FSCalendar *)calendar boundingRectWillChange:(CGRect)bounds animated:(BOOL)animated
{
//    NSLog(@"%@",(calendar.scope==FSCalendarScopeWeek?@"week":@"month"));
    _calendarHeightConstraint.constant = CGRectGetHeight(bounds);
    [self.view layoutIfNeeded];
}

- (void)calendar:(FSCalendar *)calendar didSelectDate:(NSDate *)date atMonthPosition:(FSCalendarMonthPosition)monthPosition
{
    NSLog(@"did select date %@",[self.dateFormatter stringFromDate:date]);
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];

        [df setDateFormat:@"dd/MM/yyyy"];
        selecteddate = [df stringFromDate:date];
    [self CallLesserPlannerData];
    
    NSMutableArray *selectedDates = [NSMutableArray arrayWithCapacity:calendar.selectedDates.count];
    [calendar.selectedDates enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [selectedDates addObject:[self.dateFormatter stringFromDate:obj]];
    }];
    NSLog(@"selected dates is %@",selectedDates);
    if (monthPosition == FSCalendarMonthPositionNext || monthPosition == FSCalendarMonthPositionPrevious) {
        [calendar setCurrentPage:date animated:YES];
    }
}

- (void)calendarCurrentPageDidChange:(FSCalendar *)calendar
{
    NSLog(@"%s %@", __FUNCTION__, [self.dateFormatter stringFromDate:calendar.currentPage]);
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];

       [df setDateFormat:@"MM/yyyy"];
       month = [df stringFromDate:calendar.currentPage];
      [self CallLesserPlannerDates];
}

- (NSInteger)calendar:(FSCalendar *)calendar numberOfEventsForDate:(NSDate*)date
{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];

        [df setDateFormat:@"yyyy-MM-dd"];
    NSString *dateString = [df stringFromDate:date];

    if ([[arrdates valueForKey:@"Date"] containsObject:dateString]){
        return 1;
    }
    else{
        NSLog(@"........Events List........");
    }
    return 0;
}

#pragma mark - <TableviewDelegate>

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrplanerdata.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"LectureCell";
    LectureCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
  
    
    if (cell == nil) {
        cell = [[LectureCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    cell.vwmain.layer.borderColor = [UIColor blackColor].CGColor;
    cell.vwmain.layer.borderWidth = 2.0f;
    
    NSDictionary *dict = [arrplanerdata objectAtIndex:indexPath.row];
    
    cell.lblteachername.text = [dict valueForKey:@"TeacherName"];
    cell.lblboardname.text = [dict valueForKey:@"BoardName"];
    cell.lblmediumname.text = [dict valueForKey:@"MediumName"];
    cell.lblstandardname.text = [dict valueForKey:@"StandardName"];
    cell.lblsubjectname.text = [dict valueForKey:@"SubjectName"];
    cell.lbltitlename.text = [dict valueForKey:@"Title"];
    cell.lblcreated.text = [dict valueForKey:@"PaperDate"];
    
//    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
//    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss a"];
//    // or @"yyyy-MM-dd hh:mm:ss a" if you prefer the time with AM/PM
//    NSLog(@"%@",[dateFormatter stringFromDate:[NSDate date]]);
    
    NSString *myString = [dict valueForKey:@"startTime"];

    NSDateFormatter *dateFormatter=[NSDateFormatter new];
    [dateFormatter setDateFormat:@"dd/MM/yyyy hh:mm:ss a"];
    NSDate *date=[dateFormatter dateFromString:myString];

    NSDateFormatter *dfTime = [NSDateFormatter new];
    [dfTime setDateFormat:@"hh:mm a"];
    NSString *time=[dfTime stringFromDate:date];
    
    cell.lblstarttime.text = time;
    
    cell.lblduration.text =  [self formatTimeFromSeconds:[[dict valueForKey:@"Duration"]intValue]*60];
   
    NSString *IsLive = [dict valueForKey:@"IsLive"];
    
    if ([IsLive isEqualToString:@"1"]) {
        cell.btnplay.hidden = false;
        cell.btnwidth.constant = 30;
    }
    else
    {
        cell.btnplay.hidden = true;
        cell.btnwidth.constant = 0;
    }
    
    cell.btnplay.tag = indexPath.row;
   [cell.btnplay addTarget:self action:@selector(yourButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    

    NSDate *today = [NSDate date]; // it will give you current date
   NSDateFormatter *dateFormatte = [[NSDateFormatter alloc] init];
    [dateFormatte setDateFormat:@"dd/MM/yyyy hh:mm:ss a"];
    
    NSDate *startTime = [dateFormatte dateFromString:[dict valueForKey:@"startTime"]];// your date
    NSDate *endTime = [dateFormatte dateFromString:[dict valueForKey:@"endTime"]];
    
    NSComparisonResult result,result1;
    //has three possible values: NSOrderedSame,NSOrderedDescending, NSOrderedAscending

    result = [today compare:startTime];
      result1 = [today compare:endTime];
    // comparing two dates

    if ((result==NSOrderedDescending) && (result1 == NSOrderedAscending))
    {
        cell.userInteractionEnabled = true;
        NSLog(@"today is less");
    cell.btnplay.alpha = 1.0f;
    [UIView animateWithDuration:1.0
      delay:0.5
      options:UIViewAnimationOptionCurveEaseInOut |
              UIViewAnimationOptionRepeat |
              UIViewAnimationOptionAutoreverse |
              UIViewAnimationOptionAllowUserInteraction
      animations:^{
       cell.btnplay.alpha = 0.1f;
       //  cell.btnplay.userInteractionEnabled = true;
    }
    completion:^(BOOL finished){
    // Do nothing
    }];
    }
//    else if(result==NSOrderedDescending)
//    {
//        //cell.btnplay.enabled = false;
//        NSLog(@"newDate is less");
//    }
    else
        NSLog(@"Both dates are same");
 //   cell.btnplay.userInteractionEnabled = false;
    //etc.
    return cell;
}

-(void)yourButtonClicked:(UIButton*)sender
{
     NSDictionary *dict = [arrplanerdata objectAtIndex:sender.tag];
    if ([[dict valueForKey:@"LiveClassID"] isEqualToString:@"0"]) {
        return;
    }
    
    NSDate *today = [NSDate date]; // it will give you current date
     NSDateFormatter *dateFormatte = [[NSDateFormatter alloc] init];
      [dateFormatte setDateFormat:@"dd/MM/yyyy hh:mm:ss a"];
      
      NSDate *startTime = [dateFormatte dateFromString:[dict valueForKey:@"startTime"]];// your date
      NSDate *endTime = [dateFormatte dateFromString:[dict valueForKey:@"endTime"]];
      
      NSComparisonResult result,result1;
      //has three possible values: NSOrderedSame,NSOrderedDescending, NSOrderedAscending

      result = [today compare:startTime];
        result1 = [today compare:endTime];
       if ((result==NSOrderedDescending) && (result1 == NSOrderedAscending))
        {
          [APP_DELEGATE showLoadingView:@""];

               //Post Method Request
               NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
              
            NSMutableDictionary *   ParseData=[[NSMutableDictionary alloc]init];
         ParseData=[[NSUserDefaults standardUserDefaults] valueForKey:@"ParseData"];
               
               [AddPost setValue: [[NSUserDefaults standardUserDefaults] valueForKeyPath:@"ParseData.StudentID"] forKey:@"userId"];
              [AddPost setValue: [NSString stringWithFormat:@"%@ %@",[ParseData  valueForKey:@"FirstName"],[ParseData valueForKey:@"LastName"]] forKey:@"userName"];
            [AddPost setValue:[dict valueForKey:@"LiveClassID"] forKey:@"class_id"];
             [AddPost setValue:@"0" forKey:@"isTeacher"];
             [AddPost setValue:[dict valueForKey:@"ClassName"] forKey:@"lessonName"];
             [AddPost setValue:[dict valueForKey:@"ClassName"] forKey:@"courseName"];
               NSLog(@"StudID %@",AddPost);
               
               AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
               manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
               //%@get_student_class_dtl?",BaseURLAPI
               
               [manager POST:[NSString stringWithFormat:@"https://api.braincert.com/v2/getclasslaunch?apikey=yOYjUZ9bX4LkuWthDlHg"] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject){
                   NSLog(@"JSON: %@", responseObject);
                   
                            NSString *pURL = [responseObject valueForKey:@"encryptedlaunchurl"];

                              if( [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:pURL]])
                                  [[UIApplication sharedApplication] openURL:[NSURL URLWithString:pURL]];
                   
        //           if ([[responseObject valueForKey:@"status"] intValue]==1) {
        //
        //
        //
        //           }
        //           else
        //           {
        //               UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
        //                                                               message:[responseObject valueForKey:@"error"]
        //                                                              delegate:self
        //                                                     cancelButtonTitle:@"OK"
        //                                                     otherButtonTitles:nil];
        //               [alert show];
        //           }
                   
                   
                   [APP_DELEGATE hideLoadingView];
                   
               } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                   NSLog(@"Error: %@", error);
                   [APP_DELEGATE hideLoadingView];
                   UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                                   message:@"Can not load data."
                                                                  delegate:self
                                                         cancelButtonTitle:@"OK"
                                                         otherButtonTitles:nil];
                   [alert show];
                   
                   [self.navigationController popViewControllerAnimated:NO];
                   
               }];
        }

        else
        {
           NSLog(@"Both dates are same");
            dispatch_async(dispatch_get_global_queue(0, 0), ^{
             //load your data here.
             dispatch_async(dispatch_get_main_queue(), ^{
                            //update UI in main thread.
                 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                           message:@"Lecture is Ended"
                          delegate:self
                 cancelButtonTitle:@"OK"
                 otherButtonTitles:nil];
                        });
            });
            
        }
            
   
  
    
}

-(NSString *)formatTimeFromSeconds:(int)numberOfSeconds
{

    int seconds = numberOfSeconds % 60;
    int minutes = (numberOfSeconds / 60) % 60;
    int hours = numberOfSeconds / 3600;

    //we have >=1 hour => example : 3h:25m
    if (hours) {
        return [NSString stringWithFormat:@"%dh:%02dm", hours, minutes];
    }
    //we have 0 hours and >=1 minutes => example : 3m:25s
    if (minutes) {
        return [NSString stringWithFormat:@"%dm:%02ds", minutes, seconds];
    }
    //we have only seconds example : 25s
    return [NSString stringWithFormat:@"%ds", seconds];
}


#pragma mark - <API Call>

-(void)CallLesserPlannerDates
{
    [APP_DELEGATE showLoadingView:@""];

    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
   
    
    [AddPost setValue: [[NSUserDefaults standardUserDefaults] valueForKeyPath:@"ParseData.StudentID"] forKey:@"StudID"];
    [AddPost setValue:month forKey:@"Month"];
    NSLog(@"StudID %@",AddPost);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    //%@get_student_class_dtl?",BaseURLAPI
    
    [manager POST:[NSString stringWithFormat:@"%@lesson_planner_dates",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject){
        NSLog(@"JSON: %@", responseObject);
        
        arrdates = [[NSMutableArray alloc] init];
        
      
        
        if ([[responseObject valueForKey:@"status"] intValue]==1) {
            
            arrdates = [responseObject valueForKey:@"Dates"];
            [self.calender reloadData ];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                            message:[responseObject valueForKey:@"message"]
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }
        
        
        [APP_DELEGATE hideLoadingView];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                        message:@"Can not load data."
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        
        [self.navigationController popViewControllerAnimated:NO];
        
    }];
    
}


-(void)CallLesserPlannerData
{
    [APP_DELEGATE showLoadingView:@""];

    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
   
    
    [AddPost setValue: [[NSUserDefaults standardUserDefaults] valueForKeyPath:@"ParseData.StudentID"] forKey:@"StudID"];
    [AddPost setValue:selecteddate forKey:@"Date"];
    NSLog(@"StudID %@",AddPost);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    //%@get_student_class_dtl?",BaseURLAPI
    
    [manager POST:[NSString stringWithFormat:@"%@lesson_planner_data",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject){
        NSLog(@"JSON: %@", responseObject);
        
        arrplanerdata = [[NSMutableArray alloc] init];
        
      
        
        if ([[responseObject valueForKey:@"status"] intValue]==1) {
            
            arrplanerdata = [responseObject valueForKey:@"Data"];
            [self.tableView reloadData ];
            self.lblnodata.hidden = YES;
             self.tableView.hidden = NO;
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                            message:[responseObject valueForKey:@"message"]
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            
            self.lblnodata.hidden = NO;
                    self.tableView.hidden = YES;
        }
        
        [APP_DELEGATE hideLoadingView];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                        message:@"Can not load data."
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        
        [self.navigationController popViewControllerAnimated:NO];
        
    }];
    
}




- (IBAction)back_click:(UIButton *)sender {
     [self.navigationController popViewControllerAnimated:YES];
}
@end
