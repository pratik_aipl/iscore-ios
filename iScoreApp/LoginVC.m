//
//  LoginVC.m
//  iScoreApp
//
//  Created by ADMIN-Khushal on 2/23/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "LoginVC.h"
#import "UIColor+CL.h"
#import "OTPVC.h"
#import "ApplicationConst.h"
#import "Reachability.h"
#import "FreeRegisterVC.h"
#import <AFNetworking/AFNetworking.h>

#define REGEX_PHONE_DEFAULT @"[0-9]"

@interface LoginVC ()

-(void)reachabilityChanged:(NSNotification*)note;

@property(strong) Reachability * googleReach;
@property(strong) Reachability * localWiFiReach;
@property(strong) Reachability * internetConnectionReach;

@end

@implementation LoginVC

- (void)viewDidLoad {
    [super viewDidLoad];
     //[self setupAlerts];
    self.navigationController.navigationBarHidden=YES;
    // Do any additional setup after loading the view.
    
    _vw_activkey.layer.borderWidth=1.0;
       _vw_activkey.layer.borderColor=[UIColor colorWithHex:0x0A385A].CGColor;
       _vw_activkey.layer.cornerRadius=5;
       
       _vwuserid.layer.borderWidth=1.0;
          _vwuserid.layer.borderColor=[UIColor colorWithHex:0x0A385A].CGColor;
          _vwuserid.layer.cornerRadius=5;
      
       _btn_next_b.layer.cornerRadius=3;
    
    _vw_mobile_verify.layer.borderWidth=1.0;
    _vw_mobile_verify.layer.borderColor=[UIColor colorWithHex:0x0A385A].CGColor;
    _vw_mobile_verify.layer.cornerRadius=3;
    _btn_login.layer.cornerRadius=3;
    
    //Mobile Number
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.vw_mob_icon.bounds byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerBottomLeft) cornerRadii:CGSizeMake(5.0, 5.0)];
    
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.view.bounds;
    maskLayer.path  = maskPath.CGPath;
    self.vw_mob_icon.layer.mask = maskLayer;
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reachabilityChanged:)
                                                 name:kReachabilityChangedNotification
                                               object:nil];
    
    
    __weak __block typeof(self) weakself = self;
    
    [_txt_mobile addTarget:self action:@selector(textFieldDidChange1:) forControlEvents:UIControlEventEditingChanged];
    
    //[self setDoneButton:_txt_mobile];
    
    //////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////
    //
    // create a Reachability object for www.google.com
    
    self.googleReach = [Reachability reachabilityWithHostname:@"www.google.com"];
    
    self.googleReach.reachableBlock = ^(Reachability * reachability)
    {
        NSString * temp = [NSString stringWithFormat:@"GOOGLE Block Says Reachable(%@)", reachability.currentReachabilityString];
        NSLog(@"%@", temp);
        
        // to update UI components from a block callback
        // you need to dipatch this to the main thread
        // this uses NSOperationQueue mainQueue
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            //weakself.blockLabel.text = temp;
            //weakself.blockLabel.textColor = [UIColor blackColor];
            isInternet=YES;
        }];
    };
    
    self.googleReach.unreachableBlock = ^(Reachability * reachability)
    {
        NSString * temp = [NSString stringWithFormat:@"GOOGLE Block Says Unreachable(%@)", reachability.currentReachabilityString];
        NSLog(@"%@", temp);
        
        // to update UI components from a block callback
        // you need to dipatch this to the main thread
        // this one uses dispatch_async they do the same thing (as above)
        dispatch_async(dispatch_get_main_queue(), ^{
            //weakself.blockLabel.text = temp;
            //weakself.blockLabel.textColor = [UIColor redColor];
            isInternet=NO;
        });
    };
    
    
    [self.googleReach startNotifier];
    
    
}
#pragma Rechablity
-(void)reachabilityChanged:(NSNotification*)note
{
    Reachability * reach = [note object];
    
    if(reach == self.googleReach)
    {
        if([reach isReachable])
        {
            NSString * temp = [NSString stringWithFormat:@"GOOGLE Notification Says Reachable(%@)", reach.currentReachabilityString];
            NSLog(@"%@", temp);
            
            //[self CheckInternet];
            isInternet=YES;
            
        }
        else
        {
            NSString * temp = [NSString stringWithFormat:@"GOOGLE Notification Says Unreachable(%@)", reach.currentReachabilityString];
            NSLog(@"%@", temp);
            isInternet=NO;
           // [self CheckInternet];
            
        }
    }
}

-(void)textFieldDidChange1 :(UITextField *)theTextField
{
   
    
    if ([theTextField.text length]<=10) {
        NSLog(@"ok");
        _txt_mobile.text =[NSString stringWithFormat:@"%ld",[theTextField.text integerValue]];
        
        
    }
    else
    {
        NSString *message = [NSString stringWithFormat:@"Should not more than 10 digit"];
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil
                                                                       message:message
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:nil];
        
        int duration = 2; // duration in seconds
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [alert dismissViewControllerAnimated:YES completion:nil];
        });
        
        NSLog(@"NOT OK");
        NSString *lastString=[_txt_mobile.text substringToIndex:[_txt_mobile.text length] - 1];;
        [_txt_mobile setText:lastString];
    }
    
}


-(void)CallLogin
{
    [APP_DELEGATE showLoadingView:@""];
    
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    [AddPost setValue:_txt_mobile.text forKey:@"register_mobile"];
    [AddPost setValue:@"222312125" forKey:@"imei_number"];
    [AddPost setValue:@"" forKey:@"device_id"];
    [AddPost setValue:@"iPhone" forKey:@"device_type"];
    [AddPost setValue:@"b752b39e-9606-4149-ae0c-e396cf5aee" forKey:@"player_id"];
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    [manager GET:[NSString stringWithFormat:@"%@login?",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject){
        NSLog(@"JSON: %@", responseObject);
        
        
        
        
        if ([[responseObject valueForKey:@"status"] intValue]==1) {
            
            if ([[responseObject valueForKey:@"data"] count]>0) {
                _dictdata=[[NSMutableDictionary alloc]init];
                [_dictdata setDictionary:[responseObject valueForKey:@"data"]];
                [_dictdata setValue:[responseObject valueForKey:@"otp"] forKey:@"MOTP"];
                
                [_dictdata setValue:@"LOGIN" forKey:@"FlowFlag"];
                
                OTPVC * next = [self.storyboard instantiateViewControllerWithIdentifier:@"OTPVC"];
                next.dictdata=_dictdata;
                [self.navigationController pushViewController:next animated:YES];
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                                message:@"Some Error."
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
            }
            
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                            message:@"Mobile Number Not Exist, Please Signup First"
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }
        [APP_DELEGATE hideLoadingView];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                        message:@"some things wrong"
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        [APP_DELEGATE hideLoadingView];
    }];

}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*
-(void)setupAlerts{
    
    
    [_txt_mobile addRegx:REGEX_PHONE_DEFAULT  withMsg:@"Enter valid mobile number"];
    
}*/
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}

- (IBAction)btn_BACK:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btn_LOGIN_A:(id)sender {
    
    [self.view endEditing:YES];
    
    if ([_txt_mobile.text length]<=10) {
        
        if (isInternet) {
            [self CallLogin];
        }
        else
        {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Alert" message:@"Internet is not available." preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            
                // Enter code here
            }];
            [alert addAction:defaultAction];
            
            // Present action where needed
            [self presentViewController:alert animated:YES completion:nil];
        }
        
        
    }
    
   
}

- (void)setDoneButton :(UITextField *)txtfield
{
    
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    numberToolbar.barStyle = UIBarStyleDefault;
    numberToolbar.items =@[[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelNumberPad:txtfield:)],
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           [[UIBarButtonItem alloc]initWithTitle:@"Apply" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad:txtfield:)]];
    
    
    [numberToolbar sizeToFit];
    txtfield.inputAccessoryView = numberToolbar;
}

-(void)cancelNumberPad :(UITextField*)txtfield : (id) sender
{
    [txtfield resignFirstResponder];
    txtfield.text = @"";
}

-(void)doneWithNumberPad :(UITextField*)txtfield : (id) sender
{
    NSString *numberFromTheKeyboard = txtfield.text;
    [txtfield resignFirstResponder];
}

-(void)CallActiveKey
{
    
    [APP_DELEGATE showLoadingView:@""];
    
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    [AddPost setValue:_txt_akey.text forKey:@"app_key"];
    [AddPost setValue:@"222312125" forKey:@"imei_number"];
    [AddPost setValue:@"" forKey:@"device_id"];
    [AddPost setValue:@"iPhone" forKey:@"device_type"];
   
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    [manager GET:[NSString stringWithFormat:@"%@check_key?",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject){
        NSLog(@"JSON: %@", responseObject);
        
        if ([[responseObject valueForKey:@"status"] intValue]==1) {
            
            //_dictdata=[responseObject valueForKey:@"data"];
            
            [_dictdata setDictionary:[responseObject valueForKey:@"data"]];
            
            
            
            FreeRegisterVC * next = [self.storyboard instantiateViewControllerWithIdentifier:@"FreeRegisterVC"];
            next.dictdata=_dictdata;
            [self.navigationController pushViewController:next animated:YES];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                            message:@"Please Enter valid password"
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }
        [APP_DELEGATE hideLoadingView];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];
    }];

}

- (IBAction)btn_NEXT:(id)sender {
    [self.view endEditing:YES];
    if ([_txt_akey validate]) {
        if (isInternet) {
             [self CallActiveKey];
        }
        else
        {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Alert" message:@"Internet is not available." preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                
                // Enter code here
            }];
            [alert addAction:defaultAction];
            
            // Present action where needed
            [self presentViewController:alert animated:YES completion:nil];
        }
       
    }
}

@end
