//
//  TVCDispChapters.h
//  McqApp
//
//  Created by My Mac on 3/8/17.
//  Copyright © 2017 My Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TVCDispChapters1 : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgArrow;
@property (strong, nonatomic) IBOutlet UIImageView *img_cpt_select;
@property (strong, nonatomic) IBOutlet UIButton *btn_sel_allchptr;

@property (weak, nonatomic) IBOutlet UIImageView *imgSelection;

@property (weak, nonatomic) IBOutlet UILabel *lblChepterName;
@property (weak, nonatomic) IBOutlet UILabel *lblSubChapterName;
@property (strong, nonatomic) IBOutlet UIProgressView *pro_cell_chapt;
@property (strong, nonatomic) IBOutlet UILabel *lbl_acu;

@property (strong, nonatomic) IBOutlet UIView *vw_chptnm_view;


@end

