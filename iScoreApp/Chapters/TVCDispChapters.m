//
//  TVCDispChapters.m
//  McqApp
//
//  Created by My Mac on 3/8/17.
//  Copyright © 2017 My Mac. All rights reserved.
//

#import "TVCDispChapters.h"

@implementation TVCDispChapters

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    _vw_chptnm_view.layer.shadowRadius  = 2.5f;
    _vw_chptnm_view.layer.shadowColor   = [UIColor colorWithRed:176.f/255.f green:199.f/255.f blue:226.f/255.f alpha:1.f].CGColor;
    _vw_chptnm_view.layer.shadowOffset  = CGSizeMake(0.0f, 0.0f);
    _vw_chptnm_view.layer.shadowOpacity = 0.7f;
    _vw_chptnm_view.layer.masksToBounds = NO;
    
    UIEdgeInsets shadowInsets     = UIEdgeInsetsMake(0, 0, -1.5f, 0);
    UIBezierPath *shadowPath      = [UIBezierPath bezierPathWithRect:UIEdgeInsetsInsetRect(_vw_chptnm_view.bounds, shadowInsets)];
    _vw_chptnm_view.layer.shadowPath    = shadowPath.CGPath;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
