//
//  Chapters.m
//  iScoreApp
//
//  Created by ADMIN-Khushal on 4/24/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "Chapters.h"
#import "MyModel.h"
#import "MyDBQueries.h"
#import "ChapterListModel.h"
#import "UIColor+CL.h"
#import "Chapters.h"
#import "SelectLevelVC.h"
#import "SubjectVC.h"




@interface Chapters ()

@end

NSMutableArray * marrParentChapters;
NSMutableArray * marrSubChapters;
NSString * chapterId;

@implementation Chapters

bool selectAllSub = false;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    _img_top_header.alpha=0.0;
    _tblDispChapters.tableHeaderView = _vw_headerv;
    
    selectAllSub = false;
    
    _marrHideShowSection =[[NSMutableArray alloc]init];
    _marrSelectionSection =[[NSMutableArray alloc]init];
    _marrAllChapter =[[NSMutableArray alloc]init];
    myDbQueris =[[MyDBQueries alloc]init];
    myDBParser=[[MyDBResultParser alloc]init];
    
    NSLog(@"Subject ID %@",_subjectId);
    
    [self CallChapterList];
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _tblDispChapters.frame.size.width, 80)];
    [_tblDispChapters setTableFooterView:view];
    
    
    for (int i = 0; i < marrParentChapters.count; i++) {
        [_marrHideShowSection addObject:@"0"];
        [_marrAllChapter addObject:@"0"];
        
        NSMutableArray * marrSubSelection = [[NSMutableArray alloc] init];
    
        for (int j = 0; j < [[marrSubChapters objectAtIndex:i]count]; j++) {
            [marrSubSelection addObject:@"0"];
        }
        [_marrSelectionSection addObject:marrSubSelection];
        }
    
    
    
    
    
    UIBezierPath *maskPath;
    maskPath = [UIBezierPath bezierPathWithRoundedRect:_vw_hideview.bounds byRoundingCorners:(UIRectCornerBottomRight | UIRectCornerTopRight) cornerRadii:CGSizeMake(10.0, 10.0)];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.path = maskPath.CGPath;
    _vw_hideview.layer.mask = maskLayer;
    
    _lbl_subjectnm.text=[NSString stringWithFormat:@"%@",_subjectName];
    //_lbl_accuracy.text=[NSString stringWithFormat:@"Accuracy %@%%",_subjectAccu];
    _lbl_tot_test.text=[NSString stringWithFormat:@"Total Test %@",_totalTest];
    
    UIImage *theImage=[UIImage imageWithContentsOfFile:_subjectIcon];
    _img_sub_icon.image=theImage;
    
    ////////PROGRESSBAR
    float f;
    if (_subjectAccu==[NSNull null])
    {
        _lbl_accuracy.text=@"Accuracy: 0%";
        f=0.0;
    }
    else
    {
        _lbl_accuracy.text=[NSString stringWithFormat:@"Accuracy: %@%%",_subjectAccu];
        f=[_subjectAccu floatValue];
    }
    
    [_pro_cptr_accu.layer setCornerRadius:_pro_cptr_accu.frame.size.height/2];
    _pro_cptr_accu.clipsToBounds = true;
    [_pro_cptr_accu setTransform:CGAffineTransformMakeScale(1.0f, 3.0f)];
    
    _pro_cptr_accu.progress =  f/100.0;
    _pro_cptr_accu.trackTintColor = [UIColor colorWithHex:0xDDDDDD];
    _pro_cptr_accu.progressTintColor = [UIColor colorWithHex:0xEF9430];
    
    
    
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    isScrollingStart=YES;
    NSLog(@"scrollViewDidScroll  %f , %f",scrollView.contentOffset.x,scrollView.contentOffset.y);
    
    
    if (scrollView.contentOffset.y<=124) {
        _img_top_header.alpha=scrollView.contentOffset.y/124;
    }
    else
    {
        _img_top_header.alpha=1.0;
    }
    
    
    
}

#pragma mark - UITableView Method

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    if (tableView == _tblDispChapters){
        return _marrHideShowSection.count;
    }else{
        return 1;
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == _tblDispChapters){
        if (_isFromRevisionary) {
            
            if ([[_marrHideShowSection objectAtIndex:section] boolValue])
                return 1+[[marrSubChapters objectAtIndex:section]count];
            else
                return 1;
            
        }else if(_isFromGenerate){
            
            if ([[_marrHideShowSection objectAtIndex:section] boolValue])
                return 1+[[marrSubChapters objectAtIndex:section]count];
            else
                return 1;
            
        }
        else{
            
            if ([[_marrHideShowSection objectAtIndex:section] boolValue])
                return 1+[[marrSubChapters objectAtIndex:section]count];
            else
                return 1;
            
        }
    }else{
        
        return _marrQuestiontype.count;
        
    }
    
    
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (tableView == _tblDispChapters) {
        
        if (indexPath.row==0) {
             return 60;
        }
        else
        {
             return 50;
        }
        
       
        
    }else{
        /*QuestionTypeTVC *cell = [tableView dequeueReusableCellWithIdentifier:@"CellQuesType"];
        cell.lblQuesType.text = @"0";
        
        return [self getLabelHeight:cell.lblQuesType];
         */
        return 50;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    if (tableView == _tblDispChapters) {
        
        if(indexPath.row == 0){
            
            TVCDispChapters *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
            
            //Accuracy
            int acu;
            if ([[marrParentChapters objectAtIndex:indexPath.section]valueForKey:@"acuracy"]==[NSNull null] ) {
                 NSLog(@"%@",[[marrParentChapters objectAtIndex:indexPath.section]valueForKey:@"acuracy"]);
                acu=0;
            }
            else
            {
                acu=[[[marrParentChapters objectAtIndex:indexPath.section]valueForKey:@"acuracy"] intValue];
                NSLog(@"%d",acu);
            }
            
            cell.lbl_acu.text=[NSString stringWithFormat:@"Accuracy %d%%",acu];
            [cell.pro_cell_chapt.layer setCornerRadius:cell.pro_cell_chapt.frame.size.height/2];
            cell.pro_cell_chapt.clipsToBounds = true;
            [cell.pro_cell_chapt setTransform:CGAffineTransformMakeScale(1.0f, 2.0f)];
            
            cell.pro_cell_chapt.progress =  acu/100.0;
            cell.pro_cell_chapt.trackTintColor = [UIColor colorWithHex:0xDDDDDD];
            cell.pro_cell_chapt.progressTintColor = [UIColor colorWithHex:0xEF9430];
            
            
            cell.lblChepterName.text = [[marrParentChapters objectAtIndex:indexPath.section]valueForKey:@"ChapterName"];
            [cell.btn_sel_allchptr setTag:indexPath.section];
            [cell.btn_sel_allchptr addTarget:self action:@selector(checkBoxClicked:)forControlEvents:UIControlEventTouchUpInside];
           
            if ([[_marrHideShowSection objectAtIndex:indexPath.section] boolValue]){
                [cell.imgArrow setImage:[UIImage imageNamed:@"up-arrow1"]];
            }else{
                [cell.imgArrow setImage:[UIImage imageNamed:@"down-arrow1"]];
                
            }
            
            if ([[_marrAllChapter objectAtIndex:indexPath.section] boolValue]){
                [cell.img_cpt_select setImage:[UIImage imageNamed:@"checkbox-selected1"]];
            }else{
                [cell.img_cpt_select setImage:[UIImage imageNamed:@"checkbox-unselected1"]];
            }
            return cell;
        }else{
            TVCDispChapters *cell = [tableView dequeueReusableCellWithIdentifier:@"sub_Cell" forIndexPath:indexPath];
            cell.lblSubChapterName.text = [[[marrSubChapters objectAtIndex:indexPath.section] objectAtIndex:indexPath.row-1]valueForKey:@"ChapterName"];
            
            if ([[[_marrSelectionSection objectAtIndex:indexPath.section] objectAtIndex:indexPath.row-1] boolValue])
            {
                [cell.imgSelection setImage:[UIImage imageNamed:@"checkbox-selected1"]];
            }else{
                [cell.imgSelection setImage:[UIImage imageNamed:@"checkbox-unselected1"]];
            }
            return cell;
        }
    }else{
        
        QuestionTypeTVC *cell1 = [tableView dequeueReusableCellWithIdentifier:@"CellQuesType" forIndexPath:indexPath];
        
        if ([_marrSelectedQuestionypeID containsObject:[[_marrQuestiontype objectAtIndex:indexPath.row] valueForKey:@"QuestionTypeID"]]){
            cell1.imgSelection.image = [UIImage imageNamed:@"checkbox-selected1"];
        }else{
            cell1.imgSelection.image = [UIImage imageNamed:@"checkbox-unselected1"];
        }
        cell1.lblQuesType.text=@"1";
        return cell1;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    if (tableView == _tblDispChapters) {
        
        if(indexPath.row == 0){
            if([[_marrHideShowSection objectAtIndex:indexPath.section] isEqualToString:@"0"]){
                [_marrHideShowSection replaceObjectAtIndex:indexPath.section withObject:@"1"];
            }else{
                [_marrHideShowSection replaceObjectAtIndex:indexPath.section withObject:@"0"];
            }
            
        }
        else{
            if([[[_marrSelectionSection objectAtIndex:indexPath.section] objectAtIndex:indexPath.row-1] isEqualToString:@"0"]){
                [[_marrSelectionSection objectAtIndex:indexPath.section] replaceObjectAtIndex:indexPath.row-1 withObject:@"1"];
            }else{
                [[_marrSelectionSection objectAtIndex:indexPath.section] replaceObjectAtIndex:indexPath.row-1 withObject:@"0"];
            }
            [self CheckSelectAllORNot:indexPath.section];
        }
        [self.tblDispChapters reloadData];
        
    }else{
        
        
        if ([_marrSelectedQuestionypeID containsObject:[[_marrQuestiontype objectAtIndex:indexPath.row] valueForKey:@"QuestionTypeID"]]) {
            [_marrSelectedQuestionypeID removeObject:[[_marrQuestiontype objectAtIndex:indexPath.row] valueForKey:@"QuestionTypeID"]];
            
        }
        else{
            [_marrSelectedQuestionypeID addObject:[[_marrQuestiontype objectAtIndex:indexPath.row]  valueForKey:@"QuestionTypeID"]];
        }
        [self.tblQuesType reloadData];
    }
}


- (void)checkBoxClicked:(id)sender
{
    UIButton *btn=sender;
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[btn tag]  inSection:0];
    
    
    if([[_marrAllChapter objectAtIndex:indexPath.row] isEqualToString:@"0"]){
        [_marrAllChapter replaceObjectAtIndex:indexPath.row withObject:@"1"];
    }else{
        [_marrAllChapter replaceObjectAtIndex:indexPath.row withObject:@"0"];
    }
    
    for(int j = 0 ; j < [[_marrSelectionSection objectAtIndex:indexPath.row] count] ; j++){
        if([[_marrAllChapter objectAtIndex:indexPath.row] isEqualToString:@"0"]){
            [[_marrSelectionSection objectAtIndex:indexPath.row] replaceObjectAtIndex:j withObject:@"0"];
        }else{
            [[_marrSelectionSection objectAtIndex:indexPath.row] replaceObjectAtIndex:j withObject:@"1"];
        }
    }
    
    [self CheckParent];
    [_tblDispChapters reloadData];
    
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

- (CGFloat)getLabelHeight:(UILabel*)label {
    
    CGSize constraint = CGSizeMake(label.frame.size.width, CGFLOAT_MAX);
    CGSize size;
    NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
    CGSize boundingBox = [label.text boundingRectWithSize:constraint
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                               attributes:@{NSFontAttributeName:label.font}
                                                  context:context].size;
    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    return size.height + 18;
}

-(void)CheckParent
{
    int index=0;
    
    for (int i=0; i<[_marrAllChapter count]; i++) {
        if ([[_marrAllChapter objectAtIndex:i] intValue]==1) {
            index++;
        }
    }
    if ([_marrAllChapter count]==index) {
        [_imgSelectAll setImage:[UIImage imageNamed:@"checkbox-selected1"]];
        selectAllSub=true;
    }
    else
    {
        [_imgSelectAll setImage:[UIImage imageNamed:@"checkbox-unselected1"]];
        selectAllSub=false;
    }
}

-(void)CheckSelectAllORNot :(NSInteger)intval
{
    int index=0;
    for(int j = 0 ; j < [[_marrSelectionSection objectAtIndex:intval] count] ; j++){
        if([[[_marrSelectionSection objectAtIndex:intval] objectAtIndex:j] intValue]==1)
        {
            index++;
        }
        
        if (index==[[_marrSelectionSection objectAtIndex:intval] count]) {
            [_marrAllChapter replaceObjectAtIndex:intval withObject:@"1"];
        }
        else
        {
            [_marrAllChapter replaceObjectAtIndex:intval withObject:@"0"];
        }
    }
    [self CheckParent];
    [_tblDispChapters reloadData];
}


-(void)select_unselectAllSubject{
    
    for (int i = 0; i < _marrSelectionSection.count; i++) {
        if (selectAllSub) {
            [_marrAllChapter replaceObjectAtIndex:i withObject:@"1"];
        }
        else
        {
            [_marrAllChapter replaceObjectAtIndex:i withObject:@"0"];
        }
        for(int j = 0 ; j < [[_marrSelectionSection objectAtIndex:i] count] ; j++){
            if (selectAllSub) {
                [[_marrSelectionSection objectAtIndex:i] replaceObjectAtIndex:j withObject:@"1"];
            }
            else{
                [[_marrSelectionSection objectAtIndex:i] replaceObjectAtIndex:j withObject:@"0"];
            }
        }
    }
    [_tblDispChapters reloadData];
}

- (IBAction)btnSelectAllSubject:(id)sender {
    
    if (selectAllSub) {
        selectAllSub =false;
        [_imgSelectAll setImage:[UIImage imageNamed:@"checkbox-unselected1"]];
        [self select_unselectAllSubject];
    }else{
        selectAllSub = true;
        [_imgSelectAll setImage:[UIImage imageNamed:@"checkbox-selected1"]];
        [self select_unselectAllSubject];
    }
}

-(void)CallChapterList
{
    FMResultSet *rs = [MyModel selectQuery:[myDbQueris getChapterList:_subjectId]];
    chapterArray = [[NSMutableArray alloc] init];
    
    while ([rs next]) {
        ChapterListModel *SubModel = [[ChapterListModel alloc] init];
        [chapterArray  addObject:[myDBParser parseDBResult:rs :SubModel]];
    }
    marrParentChapters = [NSMutableArray arrayWithArray:chapterArray];
    [self CallSubChapterData];
}

-(void)CallSubChapterData{
    
    NSMutableArray * marrTempSubChapters = [[NSMutableArray alloc]init];
    for (int i = 0; i < marrParentChapters.count; i++) {
        NSUInteger count = [MyModel getCount:[myDbQueris getSubChapterCount:[[marrParentChapters objectAtIndex:i]valueForKey:@"ChapterID"]]];
        
        NSMutableArray * mSubChapterArray = [[NSMutableArray alloc]init];
        
        if (count == 0) {
            [mSubChapterArray addObject:[marrParentChapters objectAtIndex:i]];
            [marrTempSubChapters addObject:mSubChapterArray];
        
        } else{
            FMResultSet *rs = [MyModel selectQuery:[myDbQueris getSubChapter:[[marrParentChapters objectAtIndex:i] valueForKey:@"ChapterID"]]];
            
            while ([rs next]) {
                ChapterListModel *SubModel = [[ChapterListModel alloc] init];
                [mSubChapterArray  addObject:[myDBParser parseDBResult:rs :SubModel]];
            }
            [marrTempSubChapters addObject:mSubChapterArray];
        }
    }
    marrSubChapters = [NSMutableArray arrayWithArray:marrTempSubChapters];
}

- (IBAction)btn_BACK:(id)sender {
   
    [self.navigationController popViewControllerAnimated:YES];
   // SubjectVC * next = [self.storyboard instantiateViewControllerWithIdentifier:@"SubjectVC"];
   // [self.navigationController pushViewController:next animated:YES];
}

- (IBAction)btn_NEXT:(id)sender {
    _marrSelectedChapters = [[NSMutableArray alloc]init];
    
    for (int i = 0; i < _marrSelectionSection.count; i++) {
        
        for(int j = 0 ; j < [[_marrSelectionSection objectAtIndex:i] count] ; j++){
            
            if ([[[_marrSelectionSection objectAtIndex:i]objectAtIndex:j] isEqual:@"1"]) {
                
                [_marrSelectedChapters addObject:[[[marrSubChapters objectAtIndex:i] objectAtIndex:j] valueForKey:@"ChapterID"]];
            }
        }
    }
    NSLog(@"---%@",_marrSelectedChapters);
    
    if (_marrSelectedChapters.count>0) {
        NSString *strChapterlist=[self convertToCommaSeparatedFromArray:_marrSelectedChapters];
        SelectLevelVC *viewController=[[SelectLevelVC alloc]initWithNibName:@"SelectLevelVC" bundle:nil];
        viewController.chapterList=strChapterlist;
        viewController.SubjectID=_subjectId;
        viewController.subjectName=_subjectName;
        viewController.subjectIcon=_subjectIcon;
        [self.navigationController pushViewController:viewController animated:YES];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                        message:@"Please Select Chapter."
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
    
    
    
   
}

-(NSString *)convertToCommaSeparatedFromArray:(NSArray*)array{
    return [array componentsJoinedByString:@","];
}

@end

