//
//  Chapters.m
//  iScoreApp
//
//  Created by ADMIN-Khushal on 4/24/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "ChaptersVC.h"
#import "MyModel.h"
#import "MyDBQueries.h"
#import "ChapterListModel.h"
#import "UIColor+CL.h"
#import "ChaptersVC.h"
#import "SelectLevelVC.h"
#import "SubjectVC.h"
#import "ChapterListForP.h"
#import "SetPaperVC.h"
#import "PDFView.h"
#import "SelectQueTypeVC.h"





@interface ChaptersVC ()

@end

NSMutableArray * marrparentchapters1;
NSMutableArray * marrSubChapters1;
NSString * chapterId1;

@implementation ChaptersVC

bool selectAllSub1 = false;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    _img_top_header.alpha=0.0;
    _tblDispChapters.tableHeaderView = _vw_headerv;
    
    selectAllSub1 = false;
    
    _marrHideShowSection =[[NSMutableArray alloc]init];
    _marrSelectionSection =[[NSMutableArray alloc]init];
    _marrAllChapter =[[NSMutableArray alloc]init];
    myDbQueris =[[MyDBQueries alloc]init];
    myDBParser=[[MyDBResultParser alloc]init];
    
    NSLog(@"Subject ID %@",_SubjectID);
    
    
    if ([_isFrom isEqualToString:@"Revise"]) {
        _lbl_tot_test.hidden=YES;
    }
    
    [self CallChapterList];
    
    for (int i = 0; i < marrparentchapters1.count; i++) {
        [_marrHideShowSection addObject:@"0"];
        [_marrAllChapter addObject:@"0"];
        
        NSMutableArray * marrSubSelection = [[NSMutableArray alloc] init];
        
        for (int j = 0; j < [[marrSubChapters1 objectAtIndex:i]count]; j++) {
            [marrSubSelection addObject:@"0"];
        }
        [_marrSelectionSection addObject:marrSubSelection];
    }
    
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _tblDispChapters.frame.size.width, 80)];
    [_tblDispChapters setTableFooterView:view];
    
    
    UIBezierPath *maskPath;
    maskPath = [UIBezierPath bezierPathWithRoundedRect:_vw_hideview.bounds byRoundingCorners:(UIRectCornerBottomRight | UIRectCornerTopRight) cornerRadii:CGSizeMake(10.0, 10.0)];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.path = maskPath.CGPath;
    _vw_hideview.layer.mask = maskLayer;
    
    _lbl_subjectnm.text=[NSString stringWithFormat:@"%@",_subjectName];
    //_lbl_accuracy.text=[NSString stringWithFormat:@"Accuracy %@%%",_subjectAccu];
    _lbl_tot_test.text=[NSString stringWithFormat:@"Total Test %@",_totalTest];
    
    UIImage *theImage=[UIImage imageWithContentsOfFile:_subjectIcon];
    _img_sub_icon.image=theImage;
    
   
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    isScrollingStart=YES;
    NSLog(@"scrollViewDidScroll  %f , %f",scrollView.contentOffset.x,scrollView.contentOffset.y);
    
    
    if (scrollView.contentOffset.y<=124) {
        _img_top_header.alpha=scrollView.contentOffset.y/124;
    }
    else
    {
        _img_top_header.alpha=1.0;
    }
    
    
    
}

#pragma mark - UITableView Method

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    if (tableView == _tblDispChapters){
        return _marrHideShowSection.count;
    }else{
        return 1;
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == _tblDispChapters){
        if (_isFromRevisionary) {
            
            if ([[_marrHideShowSection objectAtIndex:section] boolValue])
                return 1+[[marrSubChapters1 objectAtIndex:section]count];
            else
                return 1;
            
        }else if(_isFromGenerate){
            
            if ([[_marrHideShowSection objectAtIndex:section] boolValue])
                return 1+[[marrSubChapters1 objectAtIndex:section]count];
            else
                return 1;
            
        }
        else{
            
            if ([[_marrHideShowSection objectAtIndex:section] boolValue])
                return 1+[[marrSubChapters1 objectAtIndex:section]count];
            else
                return 1;
            
        }
    }else{
        
        return _marrQuestiontype.count;
        
    }
    
    
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (tableView == _tblDispChapters) {
        
        return 50;
        
    }else{
        /*QuestionTypeTVC1 *cell = [tableView dequeueReusableCellWithIdentifier:@"CellQuesType"];
         cell.lblQuesType.text = @"0";
         
         return [self getLabelHeight:cell.lblQuesType];
         */
        return 50;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    if (tableView == _tblDispChapters) {
        
        if(indexPath.row == 0){
            
            TVCDispChapters1 *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
            
          
            cell.lblChepterName.text = [[marrparentchapters1 objectAtIndex:indexPath.section]valueForKey:@"ChapterName"];
            [cell.btn_sel_allchptr setTag:indexPath.section];
            [cell.btn_sel_allchptr addTarget:self action:@selector(checkBoxClicked:)forControlEvents:UIControlEventTouchUpInside];
            
            if ([[_marrHideShowSection objectAtIndex:indexPath.section] boolValue]){
                [cell.imgArrow setImage:[UIImage imageNamed:@"up-arrow1"]];
            }else{
                [cell.imgArrow setImage:[UIImage imageNamed:@"down-arrow1"]];
                
            }
            
            if ([[_marrAllChapter objectAtIndex:indexPath.section] boolValue]){
                [cell.img_cpt_select setImage:[UIImage imageNamed:@"checkbox-selected1"]];
            }else{
                [cell.img_cpt_select setImage:[UIImage imageNamed:@"checkbox-unselected1"]];
            }
            return cell;
        }else{
            TVCDispChapters1 *cell = [tableView dequeueReusableCellWithIdentifier:@"sub_Cell" forIndexPath:indexPath];
            cell.lblSubChapterName.text = [[[marrSubChapters1 objectAtIndex:indexPath.section] objectAtIndex:indexPath.row-1]valueForKey:@"ChapterName"];
            
            if ([[[_marrSelectionSection objectAtIndex:indexPath.section] objectAtIndex:indexPath.row-1] boolValue])
            {
                [cell.imgSelection setImage:[UIImage imageNamed:@"checkbox-selected1"]];
            }else{
                [cell.imgSelection setImage:[UIImage imageNamed:@"checkbox-unselected1"]];
            }
            return cell;
        }
    }else{
        
        QuestionTypeTVC1 *cell1 = [tableView dequeueReusableCellWithIdentifier:@"CellQuesType" forIndexPath:indexPath];
        
        if ([_marrSelectedQuestionypeID containsObject:[[_marrQuestiontype objectAtIndex:indexPath.row] valueForKey:@"QuestionTypeID"]]){
            cell1.imgSelection.image = [UIImage imageNamed:@"checkbox-selected1"];
        }else{
            cell1.imgSelection.image = [UIImage imageNamed:@"checkbox-unselected1"];
        }
        cell1.lblQuesType.text=@"1";
        return cell1;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    if (tableView == _tblDispChapters) {
        
        if(indexPath.row == 0){
            if([[_marrHideShowSection objectAtIndex:indexPath.section] isEqualToString:@"0"]){
                [_marrHideShowSection replaceObjectAtIndex:indexPath.section withObject:@"1"];
            }else{
                [_marrHideShowSection replaceObjectAtIndex:indexPath.section withObject:@"0"];
            }
            
        }
        else{
            if([[[_marrSelectionSection objectAtIndex:indexPath.section] objectAtIndex:indexPath.row-1] isEqualToString:@"0"]){
                [[_marrSelectionSection objectAtIndex:indexPath.section] replaceObjectAtIndex:indexPath.row-1 withObject:@"1"];
            }else{
                [[_marrSelectionSection objectAtIndex:indexPath.section] replaceObjectAtIndex:indexPath.row-1 withObject:@"0"];
            }
            [self CheckSelectAllORNot:indexPath.section];
        }
        [self.tblDispChapters reloadData];
        
    }else{
        
        
        if ([_marrSelectedQuestionypeID containsObject:[[_marrQuestiontype objectAtIndex:indexPath.row] valueForKey:@"QuestionTypeID"]]) {
            [_marrSelectedQuestionypeID removeObject:[[_marrQuestiontype objectAtIndex:indexPath.row] valueForKey:@"QuestionTypeID"]];
            
        }
        else{
            [_marrSelectedQuestionypeID addObject:[[_marrQuestiontype objectAtIndex:indexPath.row]  valueForKey:@"QuestionTypeID"]];
        }
        [self.tblQuesType reloadData];
    }
}


- (void)checkBoxClicked:(id)sender
{
    UIButton *btn=sender;
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[btn tag]  inSection:0];
    
    
    if([[_marrAllChapter objectAtIndex:indexPath.row] isEqualToString:@"0"]){
        [_marrAllChapter replaceObjectAtIndex:indexPath.row withObject:@"1"];
    }else{
        [_marrAllChapter replaceObjectAtIndex:indexPath.row withObject:@"0"];
    }
    
    for(int j = 0 ; j < [[_marrSelectionSection objectAtIndex:indexPath.row] count] ; j++){
        if([[_marrAllChapter objectAtIndex:indexPath.row] isEqualToString:@"0"]){
            [[_marrSelectionSection objectAtIndex:indexPath.row] replaceObjectAtIndex:j withObject:@"0"];
        }else{
            [[_marrSelectionSection objectAtIndex:indexPath.row] replaceObjectAtIndex:j withObject:@"1"];
        }
    }
    
    [self CheckParent];
    [_tblDispChapters reloadData];
    
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

- (CGFloat)getLabelHeight:(UILabel*)label {
    
    CGSize constraint = CGSizeMake(label.frame.size.width, CGFLOAT_MAX);
    CGSize size;
    NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
    CGSize boundingBox = [label.text boundingRectWithSize:constraint
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                               attributes:@{NSFontAttributeName:label.font}
                                                  context:context].size;
    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    return size.height + 18;
}

-(void)CheckParent
{
    int index=0;
    
    for (int i=0; i<[_marrAllChapter count]; i++) {
        if ([[_marrAllChapter objectAtIndex:i] intValue]==1) {
            index++;
        }
    }
    if ([_marrAllChapter count]==index) {
        [_imgSelectAll setImage:[UIImage imageNamed:@"checkbox-selected1"]];
        selectAllSub1=true;
    }
    else
    {
        [_imgSelectAll setImage:[UIImage imageNamed:@"checkbox-unselected1"]];
        selectAllSub1=false;
    }
}

-(void)CheckSelectAllORNot :(NSInteger)intval
{
    int index=0;
    for(int j = 0 ; j < [[_marrSelectionSection objectAtIndex:intval] count] ; j++){
        if([[[_marrSelectionSection objectAtIndex:intval] objectAtIndex:j] intValue]==1)
        {
            index++;
        }
        
        if (index==[[_marrSelectionSection objectAtIndex:intval] count]) {
            [_marrAllChapter replaceObjectAtIndex:intval withObject:@"1"];
        }
        else
        {
            [_marrAllChapter replaceObjectAtIndex:intval withObject:@"0"];
        }
    }
    [self CheckParent];
    [_tblDispChapters reloadData];
}


-(void)select_unselectAllSub1ject{
    
    for (int i = 0; i < _marrSelectionSection.count; i++) {
        if (selectAllSub1) {
            [_marrAllChapter replaceObjectAtIndex:i withObject:@"1"];
        }
        else
        {
            [_marrAllChapter replaceObjectAtIndex:i withObject:@"0"];
        }
        for(int j = 0 ; j < [[_marrSelectionSection objectAtIndex:i] count] ; j++){
            if (selectAllSub1) {
                [[_marrSelectionSection objectAtIndex:i] replaceObjectAtIndex:j withObject:@"1"];
            }
            else{
                [[_marrSelectionSection objectAtIndex:i] replaceObjectAtIndex:j withObject:@"0"];
            }
        }
    }
    [_tblDispChapters reloadData];
}

- (IBAction)btnselectAllSubject:(id)sender {
    
    if (selectAllSub1) {
        selectAllSub1 =false;
        [_imgSelectAll setImage:[UIImage imageNamed:@"checkbox-unselected1"]];
        [self select_unselectAllSub1ject];
    }else{
        selectAllSub1 = true;
        [_imgSelectAll setImage:[UIImage imageNamed:@"checkbox-selected1"]];
        [self select_unselectAllSub1ject];
    }
}

-(void)CallChapterList
{
    FMResultSet *rs = [MyModel selectQuery:[myDbQueris getChapterListForP:_SubjectID]];
    chapterArray = [[NSMutableArray alloc] init];
    
    while ([rs next]) {
        ChapterListForP *SubModel = [[ChapterListForP alloc] init];
        [chapterArray  addObject:[myDBParser parseDBResult:rs :SubModel]];
    }
    marrparentchapters1 = [NSMutableArray arrayWithArray:chapterArray];
    [self CallSubChapterData];
}

-(void)CallSubChapterData{
    
    NSMutableArray * marrTempSubChapters = [[NSMutableArray alloc]init];
    for (int i = 0; i < marrparentchapters1.count; i++) {
        NSUInteger count = [MyModel getCount:[myDbQueris getSubChapterCountForP:[[marrparentchapters1 objectAtIndex:i]valueForKey:@"ChapterID"]]];
        
        NSMutableArray * mSubChapterArray = [[NSMutableArray alloc]init];
        
        if (count == 0) {
            [mSubChapterArray addObject:[marrparentchapters1 objectAtIndex:i]];
            [marrTempSubChapters addObject:mSubChapterArray];
            
        } else{
            FMResultSet *rs = [MyModel selectQuery:[myDbQueris getSubChapterForP:[[marrparentchapters1 objectAtIndex:i] valueForKey:@"ChapterID"]]];
            
            while ([rs next]) {
                ChapterListForP *SubModel = [[ChapterListForP alloc] init];
                [mSubChapterArray  addObject:[myDBParser parseDBResult:rs :SubModel]];
            }
            [marrTempSubChapters addObject:mSubChapterArray];
        }
    }
    marrSubChapters1 = [NSMutableArray arrayWithArray:marrTempSubChapters];
}

- (IBAction)btn_BACK:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
    // SubjectVC * next = [self.storyboard instantiateViewControllerWithIdentifier:@"SubjectVC"];
    // [self.navigationController pushViewController:next animated:YES];
}

- (IBAction)btn_NEXT:(id)sender {
    _marrSelectedChapters = [[NSMutableArray alloc]init];
    
    for (int i = 0; i < _marrSelectionSection.count; i++) {
        
        for(int j = 0 ; j < [[_marrSelectionSection objectAtIndex:i] count] ; j++){
            
            if ([[[_marrSelectionSection objectAtIndex:i]objectAtIndex:j] isEqual:@"1"]) {
                
                [_marrSelectedChapters addObject:[[[marrSubChapters1 objectAtIndex:i] objectAtIndex:j] valueForKey:@"ChapterID"]];
            }
        }
    }
    NSLog(@"---%@",_marrSelectedChapters);
    
    if (_marrSelectedChapters.count>0) {
        NSString *strChapterlist=[self convertToCommaSeparatedFromArray:_marrSelectedChapters];
        
        if ([_isFrom isEqualToString:@"3"]) {
            SetPaperVC *next=[[SetPaperVC alloc]initWithNibName:@"SetPaperVC" bundle:nil];
            next.chapterList=strChapterlist;
            next.SubjectID=_SubjectID;
            next.isFrom=_isFrom;
            [self.navigationController pushViewController:next animated:YES];
        }
        else if ([_isFrom isEqualToString:@"2"])
        {
            PDFView *next=[[PDFView alloc]initWithNibName:@"PDFView" bundle:nil];
            next.isFrom=_isFrom;
            next.examTypeID=_examTypeID;
            next.subjectID=_SubjectID;
            next.arrChapterIDs=_marrSelectedChapters;
            [self.navigationController pushViewController:next animated:YES];
        }
        else if ([_isFrom isEqualToString:@"Revise"]) {
            
            NSString *strChapterlist=[self convertToCommaSeparatedFromArray:_marrSelectedChapters];
            
            SelectQueTypeVC * next = [self.storyboard instantiateViewControllerWithIdentifier:@"SelectQueTypeVC"];
            next.isFrom=_isFrom;
            next.SubjectID=_SubjectID;
            next.subjectName=_subjectName;
            next.strChapterlist=strChapterlist;
            [self.navigationController pushViewController:next animated:YES];
            
        }
        
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                        message:@"Please Select Chapter."
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
    
    
    
}

-(NSString *)convertToCommaSeparatedFromArray:(NSArray*)array{
    return [array componentsJoinedByString:@","];
}

@end


