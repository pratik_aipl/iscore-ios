//
//  Chapters.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 4/24/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QuestionTypeTVC.h"
#import "TVCDispChapters.h"
#import "MyDBQueries.h"
#import "MyDBResultParser.h"


@interface Chapters : UIViewController
{
    MyDBQueries *myDbQueris;
    MyDBResultParser *myDBParser;
    NSMutableArray *chapterArray;
    
    BOOL isScrollingStart;
}
//---------------
@property (retain , nonatomic)NSString *subjectName;
@property (retain , nonatomic)NSString *subjectAccu;
@property (retain , nonatomic)NSString *totalTest;
@property (retain , nonatomic)NSString *subjectIcon;


//---------------
@property(retain,nonatomic)NSMutableArray * marrHideShowSection;
@property(retain,nonatomic)NSMutableArray * marrAllChapter;
@property(retain,nonatomic)NSMutableArray * marrSelectionSection;
@property(retain,nonatomic)NSMutableArray * marrSelectedChapters;

@property (nonatomic ,retain) NSMutableArray * marrQuestiontype;
@property (nonatomic ,retain) NSMutableArray * marrSelectedQuestionypeID;

@property(nonatomic,assign)NSString * subjectId;
@property(nonatomic,assign)NSString * PaperTypeId;
@property(nonatomic,assign)NSString * ExamTypeId;
@property(nonatomic,assign)Boolean isFromRevisionary;
@property(nonatomic,assign)Boolean isFromGenerate;


@property (weak, nonatomic) IBOutlet UITableView *tblDispChapters;
@property (weak, nonatomic) IBOutlet UITableView *tblQuesType;


@property (weak, nonatomic) IBOutlet UILabel *lblUserName;
@property (weak, nonatomic) IBOutlet UIImageView *imgSelectAll;
@property (weak, nonatomic) IBOutlet UILabel *lblGoNext;

@property (weak, nonatomic) IBOutlet UIView *VBGQuesType;

@property (strong, nonatomic) IBOutlet UIView *vw_headerv;
@property (strong, nonatomic) IBOutlet UIImageView *img_top_header;

@property (strong, nonatomic) IBOutlet UILabel *lbl_subjectnm;
@property (strong, nonatomic) IBOutlet UIView *vw_hideview;
@property (strong, nonatomic) IBOutlet UILabel *lbl_tot_test;
@property (strong, nonatomic) IBOutlet UILabel *lbl_accuracy;
@property (strong, nonatomic) IBOutlet UIProgressView *pro_cptr_accu;
@property (strong, nonatomic) IBOutlet UIImageView *img_sub_icon;


- (IBAction)btnSelectAllSubject:(id)sender;

- (IBAction)btn_BACK:(id)sender;
- (IBAction)btn_NEXT:(id)sender;


@end
