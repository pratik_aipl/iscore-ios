//
//  SubjectCell.m
//  iScoreApp
//
//  Created by ADMIN-Khushal on 5/8/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "SubjectCell.h"

@implementation SubjectCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    [self setShadow:_vw_main];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setShadow :(UIView*)vview
{
    vview.layer.shadowRadius  = 2.5f;
    vview.layer.shadowColor   = [UIColor colorWithRed:176.f/255.f green:199.f/255.f blue:226.f/255.f alpha:1.f].CGColor;
    vview.layer.shadowOffset  = CGSizeMake(0.0f, 0.0f);
    vview.layer.shadowOpacity = 0.7f;
    vview.layer.masksToBounds = NO;
    
    UIEdgeInsets shadowInsets     = UIEdgeInsetsMake(0, 0, -1.5f, 0);
    UIBezierPath *shadowPath      = [UIBezierPath bezierPathWithRect:UIEdgeInsetsInsetRect(vview.bounds, shadowInsets)];
    vview.layer.shadowPath    = shadowPath.CGPath;
}


@end
