//
//  SubjectVC.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 5/8/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyDBQueries.h"
#import "MyDBResultParser.h"
#import "QuestionTypeTVC.h"
#import "TVCDispChapters.h"

@interface SubjectVC : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    MyDBQueries *myDbQueris;
    MyDBResultParser *myDBParser;
    NSMutableArray *subjectArray ;
    NSString *filePath;
}

@property (retain , nonatomic)NSString *BoardID;
@property (retain , nonatomic)NSString *MediumID;
@property (retain , nonatomic)NSString *StandardID;

//Outlet
@property (strong, nonatomic) IBOutlet UITableView *tbl_sub;


//Actioon
- (IBAction)btn_BACK:(id)sender;

@end
