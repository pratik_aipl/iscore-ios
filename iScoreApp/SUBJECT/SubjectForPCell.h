//
//  SubjectCell.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 5/8/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SubjectForPCell : UITableViewCell

//Outlet
@property (strong, nonatomic) IBOutlet UIImageView *img_subject;
@property (strong, nonatomic) IBOutlet UILabel *lbl_subjectnm;
@property (strong, nonatomic) IBOutlet UILabel *lbl_accurecy;
@property (strong, nonatomic) IBOutlet UILabel *lbl_tot_test;
@property (strong, nonatomic) IBOutlet UILabel *lbl_dt;
@property (strong, nonatomic) IBOutlet UIProgressView *pro_subject;
@property (strong, nonatomic) IBOutlet UILabel *lbl_dt1;
@property (weak, nonatomic) IBOutlet UIView *vw_main;



@end

