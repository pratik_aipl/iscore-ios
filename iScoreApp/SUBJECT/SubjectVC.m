//
//  SubjectVC.m
//  iScoreApp
//
//  Created by ADMIN-Khushal on 5/8/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "SubjectVC.h"
#import "MyModel.h"
#import "MyDBQueries.h"
#import "MCQSubjectModel.h"
#import "UIColor+CL.h"
#import "Chapters.h"
#import "MyModel.h"
#import "MyDBQueries.h"
#import "ChapterListModel.h"
#import "UIColor+CL.h"
#import "SubjectCell.h"
#import "ApplicationConst.h"
#import "HomeVC.h"
#import "SetGoalVC.h"


@interface SubjectVC ()

@end

@implementation SubjectVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    myDbQueris =[[MyDBQueries alloc]init];
    myDBParser=[[MyDBResultParser alloc]init];
    filePath = [MyModel getFilePath];
    
    FMResultSet *rs = [MyModel selectQuery:[myDbQueris getMCQSubjectsQuery:_StandardID]];
    subjectArray = [[NSMutableArray alloc] init];
    while ([rs next]) {
        
        MCQSubjectModel *SubModel = [[MCQSubjectModel alloc] init];
        [subjectArray  addObject:[myDBParser parseDBResult:rs :SubModel]];
        NSLog(@"--%@",subjectArray);
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return subjectArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MCQSubjectModel *mySubject = [subjectArray objectAtIndex:indexPath.row];
    
    static NSString *simpleTableIdentifier = @"Cell";
    SubjectCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil) {
        cell = [[SubjectCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    cell.lbl_subjectnm.text=[NSString stringWithFormat:@"%@",mySubject.SubjectName];
    cell.lbl_tot_test.text=[NSString stringWithFormat:@"%@",mySubject.TotalSubjectTest];
    
    
    float f;
    if (mySubject.accuracy1==[NSNull null])
    {
        cell.lbl_accurecy.text=@"Accuracy:0";
        f=0.0;
    }
    else
    {
        cell.lbl_accurecy.text=[NSString stringWithFormat:@"Accuracy:%@",mySubject.accuracy1];
        f=[mySubject.accuracy1 floatValue];
    }
    
    
    if (mySubject.Last_Date==[NSNull null])
    {
        if (![cell.lbl_dt.text isEqualToString:@"N/A"]) {
           // cell.lbl_dt1.frame = CGRectMake(cell.lbl_dt1.frame.origin.x+40, cell.lbl_dt1.frame.origin.y, cell.lbl_dt1.frame.size.width, cell.lbl_dt1.frame.size.height);
            
            CGRect border = cell.lbl_dt1.frame;
            border.origin.x =210;
            cell.lbl_dt1.frame = border;
            
            
        }
        
        cell.lbl_dt.text=@"N/A";
    }
    else
    {
        cell.lbl_dt.text=[NSString stringWithFormat:@"%@",mySubject.Last_Date];
        cell.lbl_dt.text=[NSString stringWithFormat:@"%@",[mySubject.Last_Date substringToIndex:[mySubject.Last_Date length]-9]];
        CGRect border = cell.lbl_dt1.frame;
        border.origin.x =179;
        cell.lbl_dt1.frame = border;
    }
    
    
    NSString* imagepath = [NSString stringWithFormat:@"%@/subject_icon/%@",filePath,[mySubject.SubjectIcon stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
    
    NSLog(@"Subject Icon Path :: %@",[mySubject.SubjectIcon stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]);
    
    NSFileManager *fileManager = [[NSFileManager alloc] init];
    
    UIImage *theImage=[UIImage imageWithContentsOfFile:imagepath];
    cell.img_subject.image=theImage;
    
    ////////PROGRESSBAR
    
    [cell.pro_subject.layer setCornerRadius:cell.pro_subject.frame.size.height/2];
    cell.pro_subject.clipsToBounds = true;
    [cell.pro_subject setTransform:CGAffineTransformMakeScale(1.0f, 3.0f)];
    
    cell.pro_subject.progress =  f/100.0;
    cell.pro_subject.trackTintColor = [UIColor colorWithHex:0xDDDDDD];
    cell.pro_subject.progressTintColor = [UIColor colorWithHex:0x0A385A];
    
    
    //etc.
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
   
    [APP_DELEGATE showLoadingView:@""];
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0ul);
    dispatch_async(queue, ^{
        dispatch_sync(dispatch_get_main_queue(), ^{
            UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
            if (cell.selected) {
                [tableView deselectRowAtIndexPath:indexPath animated:YES];
            }
            
            MCQSubjectModel *mySubject = [subjectArray objectAtIndex:indexPath.row];
            NSString* imagepath = [NSString stringWithFormat:@"%@/subject_icon/%@",filePath,[mySubject.SubjectIcon stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
            NSLog(@"imagePath  %@",imagepath);
            Chapters * next = [self.storyboard instantiateViewControllerWithIdentifier:@"Chapters"];
            next.subjectId=mySubject.SubjectID;
            next.subjectName=mySubject.SubjectName;
            next.subjectAccu=mySubject.accuracy1;
            next.totalTest=mySubject.TotalSubjectTest;
            next.subjectIcon=imagepath;
            [APP_DELEGATE hideLoadingView];
            [self.navigationController pushViewController:next animated:YES];
           
        });
    });
   
}

-(void)CallLevels
{
   
    
}




- (IBAction)btn_BACK:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
    
    //HomeVC * next = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeVC"];
    //[self.navigationController pushViewController:next animated:NO];

}


@end
