//
//  LegalDetailsVC.m
//  iScoreApp
//
//  Created by ADMIN-Khushal on 8/9/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "LegalDetailsVC.h"

@interface LegalDetailsVC ()

@end

@implementation LegalDetailsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _lbl_title.text=[NSString stringWithFormat:@"%@",_title_to];
    
    
    
    if ([_title_to isEqualToString:@"Privacy Policy"]) {
        NSString *htmlFile = [[NSBundle mainBundle] pathForResource:@"privacy-policy" ofType:@"html"];
        NSString* htmlString = [NSString stringWithContentsOfFile:htmlFile encoding:NSUTF8StringEncoding error:nil];
        [_web_legal loadHTMLString:htmlString baseURL: [[NSBundle mainBundle] bundleURL]];
    }
    else if ([_title_to isEqualToString:@"Terms & Condition"]) {
        NSString *htmlFile = [[NSBundle mainBundle] pathForResource:@"Terms&Conditions" ofType:@"html"];
        NSString* htmlString = [NSString stringWithContentsOfFile:htmlFile encoding:NSUTF8StringEncoding error:nil];
        [_web_legal loadHTMLString:htmlString baseURL: [[NSBundle mainBundle] bundleURL]];
    }
    else if ([_title_to isEqualToString:@"Refund & Cancellation"]) {
        NSString *htmlFile = [[NSBundle mainBundle] pathForResource:@"Refund&Cancellation" ofType:@"html"];
        NSString* htmlString = [NSString stringWithContentsOfFile:htmlFile encoding:NSUTF8StringEncoding error:nil];
        [_web_legal loadHTMLString:htmlString baseURL: [[NSBundle mainBundle] bundleURL]];
    }
    else if ([_title_to isEqualToString:@"Licenses& Copyrights"])
    {
        NSString *htmlFile = [[NSBundle mainBundle] pathForResource:@"Licenses&Copyrights" ofType:@"html"];
        NSString* htmlString = [NSString stringWithContentsOfFile:htmlFile encoding:NSUTF8StringEncoding error:nil];
        [_web_legal loadHTMLString:htmlString baseURL: [[NSBundle mainBundle] bundleURL]];
    }
    
    
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btn_BACK:(id)sender {
    [self.navigationController popViewControllerAnimated:NO];
}
@end
