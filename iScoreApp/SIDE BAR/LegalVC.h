//
//  LegalVC.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 8/9/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LegalVC : UIViewController



//Outlet
@property (weak, nonatomic) IBOutlet UIButton *btn_pp;
@property (weak, nonatomic) IBOutlet UIButton *btn_tc;
@property (weak, nonatomic) IBOutlet UIButton *btn_rc;
@property (weak, nonatomic) IBOutlet UIButton *btn_lc;




//Action
- (IBAction)btn_PP_A:(id)sender;
- (IBAction)btn_TC_A:(id)sender;
- (IBAction)btn_RC_A:(id)sender;
- (IBAction)btn_LC_A:(id)sender;

- (IBAction)btn_BACK:(id)sender;



@end
