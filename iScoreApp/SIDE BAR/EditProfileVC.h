//
//  EditProfileVC.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 8/10/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//


    

#import <UIKit/UIKit.h>
#import "HttpWrapper.h"


@interface EditProfileVC : UIViewController<HttpWrapperDelegate>
{
    NSMutableDictionary *ParseData;
    HttpWrapper *httpEditProfile;
    UIImageView *img_view;
    UIImagePickerController *ipc;
}


//OUTLET
@property (weak, nonatomic) IBOutlet UIScrollView *scroollvw_main;
@property (weak, nonatomic) IBOutlet UIView *vw_1;
@property (weak, nonatomic) IBOutlet UIView *vw_2;
@property (weak, nonatomic) IBOutlet UIButton *btn_update_prof;


@property (weak, nonatomic) IBOutlet UITextField *txt_goal;
@property (weak, nonatomic) IBOutlet UITextField *txt_pyear;
@property (weak, nonatomic) IBOutlet UITextField *txt_fnm;
@property (weak, nonatomic) IBOutlet UITextField *txt_lnm;
@property (weak, nonatomic) IBOutlet UITextField *txt_email;
@property (weak, nonatomic) IBOutlet UITextField *txt_regmob;
@property (weak, nonatomic) IBOutlet UIImageView *img_user;



//ACTION
- (IBAction)btn_UPDATEPROF:(id)sender;
- (IBAction)btn_BACK:(id)sender;
- (IBAction)btn_CHOOSE_IMG:(id)sender;




@end
