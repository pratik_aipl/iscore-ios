//
//  NotificationCollectionVC.m
//  iScoreApp
//
//  Created by ADMIN-Khushal on 8/11/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "NotificationCollectionVC.h"
#import "NotificationCollCell.h"
#import "MyModel.h"
#import "ZookiModel.h"
#import "ZookiCell.h"

#import "ZookiWebView.h"



@interface NotificationCollectionVC ()

@end

@implementation NotificationCollectionVC

static NSString * const reuseIdentifier = @"Cell";

- (void)viewDidLoad {
    [super viewDidLoad];
    _coll_main.showsHorizontalScrollIndicator = NO;
    _coll_main.showsVerticalScrollIndicator = NO;
    
    
    [self CallGetZooki];
    
}

-(void)CallGetZooki
{
    myDbQueris =[[MyDBQueries alloc]init];
    myDBParser=[[MyDBResultParser alloc]init];
    filePath = [MyModel getFilePath];
    
    FMResultSet *rs = [MyModel selectQuery:[myDbQueris getZooki]];
    arrZooki = [[NSMutableArray alloc] init];
    while ([rs next]) {
        
        ZookiModel *SubModel = [[ZookiModel alloc] init];
        [arrZooki  addObject:[myDBParser parseDBResult:rs :SubModel]];
        NSLog(@"--%@",arrZooki);
    }
    
}


- (NSIndexPath *)curIndexPath {
    NSArray *indexPaths = [_coll_main indexPathsForVisibleItems];
    NSIndexPath *curIndexPath = nil;
    NSInteger curzIndex = 0;
    for (NSIndexPath *path in indexPaths.objectEnumerator) {
        UICollectionViewLayoutAttributes *attributes = [_coll_main layoutAttributesForItemAtIndexPath:path];
        if (!curIndexPath) {
            curIndexPath = path;
            curzIndex = attributes.zIndex;
            continue;
        }
        if (attributes.zIndex > curzIndex) {
            curIndexPath = path;
            curzIndex = attributes.zIndex;
        }
    }
    return curIndexPath;
}

- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSIndexPath *curIndexPath = [self curIndexPath];
    if (indexPath.row == curIndexPath.row) {
        return YES;
    }
    
    [_coll_main scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionNone animated:YES];
    
    
    return NO;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"click %ld", indexPath.row);
    
    if(indexPath.row == arrZooki.count)
    {
        ZookiWebView * next = [self.storyboard instantiateViewControllerWithIdentifier:@"ZookiWebView"];
        [self.navigationController pushViewController:next animated:YES];
        
    }
    
    
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    CGFloat leftInset = (self.view.bounds.size.width - 375) / 2;    // CELL_WIDTH is the width of your cell
    return UIEdgeInsetsMake(0, leftInset, 0, leftInset);
}




#pragma mark <UICollectionViewDataSource>
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return arrZooki.count+1;
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
   
    
    NotificationCollCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    
    
    if(indexPath.row == arrZooki.count)
    {
        
        cell.vw_page.hidden=YES;
        cell.img_page.hidden=YES;
        
        cell.btn_vw_more.hidden=NO;
        
       
    }
    else
    {
        cell.btn_vw_more.hidden=YES;
        
         ZookiModel *myzooki = [arrZooki objectAtIndex:indexPath.row];
        
        cell.vw_page.hidden=NO;
        cell.img_page.hidden=NO;
        
        cell.lbl_page_title.text=[NSString stringWithFormat:@"%@",myzooki.Title];
        cell.lbl_page_dt.text=[NSString stringWithFormat:@"%@",myzooki.CreatedOn];
        cell.txtv_details.text=[NSString stringWithFormat:@"%@",myzooki.Desc];
        
        NSString *strdt=[NSString stringWithFormat:@"%@",myzooki.CreatedOn];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSData *Date = [dateFormatter dateFromString:strdt];
        
        [dateFormatter setDateFormat:@"dd-MM-yyyy"];
        NSString *fDate=[dateFormatter stringFromDate:Date];
        
        NSLog(@"%@",fDate);
        cell.lbl_page_dt.text=[NSString stringWithFormat:@"%@",fDate];
        
        
        
        NSString* imagepath = [NSString stringWithFormat:@"%@/zooki/%@",filePath,[myzooki.image stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
        
        NSLog(@"Subject Icon Path :: %@",[myzooki.image stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]);
        
        NSFileManager *fileManager = [[NSFileManager alloc] init];
        
        UIImage *theImage=[UIImage imageWithContentsOfFile:imagepath];
        cell.img_page.image=theImage;
        
        
        ///////SET HEIGHT PAGE/////
        
        UILabel *fromLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 285, 0)];
        fromLabel.text=cell.txtv_details.text;
        fromLabel.font = [UIFont fontWithName:@"roboto" size:12];
        CGFloat f=[self getLabelHeight:fromLabel];
        
        CGRect frame = cell.txtv_details.frame;
        frame.size.height = f+70;
        cell.txtv_details.frame = frame;
        
        CGRect frame1 = cell.vw_page.frame;
        frame1.size.height = f+135;
        cell.vw_page.frame = frame1;
        
        cell.scroll_page.delegate= self;
        [cell.scroll_page setShowsHorizontalScrollIndicator:NO];
        [cell.scroll_page setShowsVerticalScrollIndicator:NO];
        cell.scroll_page.scrollEnabled= YES;
        cell.scroll_page.userInteractionEnabled= YES;
        cell.scroll_page.contentSize= CGSizeMake(cell.scroll_page.frame.size.width, cell.vw_page.frame.size.height+135);
        
        
        _lbl_tpage.text=[NSString stringWithFormat:@"/%lu",(unsigned long)arrZooki.count];
        
       
    }
    
     return cell;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
   /* CGFloat pageWidth = _coll_main.frame.size.width;
    float currentPage = _coll_main.contentOffset.x / pageWidth;
    
    if (0.0f != fmodf(currentPage, 1.0f))
    {
        pageControl.currentPage = currentPage + 1;
    }
    else
    {
        pageControl.currentPage = currentPage;
    }
    
    NSLog(@"Page Number : %ld", (long)pageControl.currentPage);
    */
    
    CGFloat pageWidth = scrollView.frame.size.width;
    int page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    NSLog(@"Current page -> %d",page);
    
    
    _lbl_cpage.text=[NSString stringWithFormat:@"%d",page+1];
    
    
}

- (CGFloat)getLabelHeight:(UILabel*)label
{
    CGSize constraint = CGSizeMake(label.frame.size.width, CGFLOAT_MAX);
    CGSize size;
    
    NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
    CGSize boundingBox = [label.text boundingRectWithSize:constraint
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                               attributes:@{NSFontAttributeName:label.font}
                                                  context:context].size;
    
    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    
    return size.height;
}

- (IBAction)btn_CANCEL:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:NO];
    
}
@end

