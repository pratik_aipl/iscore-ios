//
//  HJCarouselViewCell.h
//  HJCarouselDemo
//
//  Created by haijiao on 15/8/20.
//  Copyright (c) 2015年 olinone. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HJCarouselViewCell : UICollectionViewCell


@property (weak, nonatomic) IBOutlet UIScrollView *scoll_vw;

@property (weak, nonatomic) IBOutlet UIImageView *img_view;
@property (weak, nonatomic) IBOutlet UIView *vw_1;

@property (weak, nonatomic) IBOutlet UILabel *lbl_title;
@property (weak, nonatomic) IBOutlet UILabel *lbl_dt;
@property (weak, nonatomic) IBOutlet UITextView *txt_text;



@end
