//
//  NotificationCell.m
//  iScoreApp
//
//  Created by ADMIN-Khushal on 8/11/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "NotificationCell.h"

@implementation NotificationCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    [self setShadow:_ve_cell];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setShadow :(UIView*)txtV
{
    
    txtV.layer.shadowRadius  = 1.5f;
    txtV.layer.shadowColor   = [UIColor lightGrayColor].CGColor;
    txtV.layer.shadowOffset  = CGSizeMake(0.0f, 0.0f);
    txtV.layer.shadowOpacity = 0.9f;
    txtV.layer.masksToBounds = NO;
    
    UIEdgeInsets shadowInsets     = UIEdgeInsetsMake(0, 0, -1.5f, 0);
    UIBezierPath *shadowPath      = [UIBezierPath bezierPathWithRect:UIEdgeInsetsInsetRect(txtV.bounds, shadowInsets)];
    txtV.layer.shadowPath    = shadowPath.CGPath;
}




@end
