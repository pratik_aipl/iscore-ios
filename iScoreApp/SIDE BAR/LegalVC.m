//
//  LegalVC.m
//  iScoreApp
//
//  Created by ADMIN-Khushal on 8/9/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "LegalVC.h"
#import "LegalDetailsVC.h"
#import "HomeVC.h"



@interface LegalVC ()

@end

@implementation LegalVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden=YES;
    
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btn_PP_A:(id)sender {
    LegalDetailsVC * next = [self.storyboard instantiateViewControllerWithIdentifier:@"LegalDetailsVC"];
    next.title_to=_btn_pp.titleLabel.text;
    
    [self.navigationController pushViewController:next animated:YES];
    
}

- (IBAction)btn_TC_A:(id)sender {
    LegalDetailsVC * next = [self.storyboard instantiateViewControllerWithIdentifier:@"LegalDetailsVC"];
    next.title_to=_btn_tc.titleLabel.text;
    
    [self.navigationController pushViewController:next animated:YES];
}

- (IBAction)btn_RC_A:(id)sender {
    LegalDetailsVC * next = [self.storyboard instantiateViewControllerWithIdentifier:@"LegalDetailsVC"];
    next.title_to=_btn_rc.titleLabel.text;
    
    [self.navigationController pushViewController:next animated:YES];
}

- (IBAction)btn_LC_A:(id)sender {
    LegalDetailsVC * next = [self.storyboard instantiateViewControllerWithIdentifier:@"LegalDetailsVC"];
    next.title_to=_btn_lc.titleLabel.text;
    
    [self.navigationController pushViewController:next animated:YES];
}

- (IBAction)btn_BACK:(id)sender {
    HomeVC * next = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeVC"];
    [self.navigationController pushViewController:next animated:NO];
}
@end
