//
//  EditProfileVC.m
//  iScoreApp
//
//  Created by ADMIN-Khushal on 8/10/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "EditProfileVC.h"
#import <AFNetworking/AFNetworking.h>
#import "ApplicationConst.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "UIColor+CL.h"


@interface EditProfileVC ()

@end

@implementation EditProfileVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _scroollvw_main.delegate= self;
    [_scroollvw_main setShowsHorizontalScrollIndicator:NO];
    [_scroollvw_main setShowsVerticalScrollIndicator:NO];
    _scroollvw_main.scrollEnabled= YES;
    _scroollvw_main.userInteractionEnabled= YES;
    _scroollvw_main.contentSize= CGSizeMake(_scroollvw_main.frame.size.width ,725);
    
    
    [self setShadow:_vw_1];
    [self setShadow:_vw_2];
    
    _btn_update_prof.layer.cornerRadius=5;
    
    //TEXT
    [self setTextFieldProperty:_txt_goal];
    [self setTextFieldProperty:_txt_pyear];
    [self setTextFieldProperty:_txt_fnm];
    [self setTextFieldProperty:_txt_lnm];
    [self setTextFieldProperty:_txt_email];
    [self setTextFieldProperty:_txt_regmob];
    
    
    
    
    ParseData=[[NSMutableDictionary alloc]initWithDictionary:[[NSUserDefaults standardUserDefaults] valueForKey:@"ParseData"]];
    //ParseData=[[NSUserDefaults standardUserDefaults] valueForKey:@"ParseData"];
    
    NSLog(@"ParseData ==%@",ParseData);
    
    _txt_fnm.text=[NSString stringWithFormat:@"%@",[ParseData valueForKey:@"FirstName"]];
    _txt_lnm.text=[NSString stringWithFormat:@"%@",[ParseData valueForKey:@"LastName"]];
    _txt_email.text=[NSString stringWithFormat:@"%@",[ParseData valueForKey:@"EmailID"]];
    _txt_regmob.text=[NSString stringWithFormat:@"%@",[ParseData valueForKey:@"RegMobNo"]];
    

    _txt_pyear.text=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"PYear"]];
    _txt_goal.text=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"CYear"]];
    
    
    ////////////////////
    _txt_goal.userInteractionEnabled=YES;
    [_txt_goal becomeFirstResponder];
    _txt_goal.layer.borderColor=[UIColor colorWithHex:0x888888].CGColor;
    _txt_goal.layer.borderWidth=1;
    _txt_goal.layer.cornerRadius=4;
    
    
    _txt_goal.textColor=[UIColor colorWithHex:0x454545];
    [_txt_goal addTarget:self action:@selector(textFieldDidChange1:) forControlEvents:UIControlEventEditingChanged];
    
    _txt_pyear.userInteractionEnabled=YES;
    _txt_pyear.textColor=[UIColor colorWithHex:0x454545];
    _txt_pyear.layer.borderColor=[UIColor colorWithHex:0x888888].CGColor;
    _txt_pyear.layer.borderWidth=1;
    _txt_pyear.layer.cornerRadius=4;
    [_txt_pyear addTarget:self action:@selector(textFieldDidChange2:) forControlEvents:UIControlEventEditingChanged];
    
    
    /////
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard:)];
    [_scroollvw_main addGestureRecognizer:gestureRecognizer];
    _scroollvw_main.userInteractionEnabled = YES;
    gestureRecognizer.cancelsTouchesInView = NO;
   
    
}

- (void) hideKeyboard: (UITapGestureRecognizer *)recognizer
{
    //Code to handle the gesture
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)textFieldDidChange1 :(UITextField *)theTextField
{
    
    
    if ([theTextField.text integerValue]<=100) {
        NSLog(@"ok");
        _txt_goal.text =[NSString stringWithFormat:@"%ld",[theTextField.text integerValue]];
        
        
    }
    else
    {
        NSString *message = [NSString stringWithFormat:@"Should not more than 100"];
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil
                                                                       message:message
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:nil];
        
        int duration = 2; // duration in seconds
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [alert dismissViewControllerAnimated:YES completion:nil];
        });
        
        NSLog(@"NOT OK");
        NSString *lastString=[_txt_goal.text substringToIndex:[_txt_goal.text length] - 1];;
        [_txt_goal setText:lastString];
    }
    
    
}

-(void)textFieldDidChange2 :(UITextField *)theTextField
{
    
    
    if ([theTextField.text integerValue]<=100) {
        NSLog(@"ok");
        _txt_pyear.text =[NSString stringWithFormat:@"%ld",[theTextField.text integerValue]];
        
        
    }
    else
    {
        NSString *message = [NSString stringWithFormat:@"Should not more than 100"];
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil
                                                                       message:message
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:nil];
        
        int duration = 2; // duration in seconds
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [alert dismissViewControllerAnimated:YES completion:nil];
        });
        
        NSLog(@"NOT OK");
        NSString *lastString=[_txt_pyear.text substringToIndex:[_txt_pyear.text length] - 1];;
        [_txt_pyear setText:lastString];
    }
    
    
}



- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}

-(void)setShadow :(UIView*)view
{
    view.layer.shadowRadius  = 2.2f;
    view.layer.shadowColor   = [UIColor colorWithRed:176.f/255.f green:199.f/255.f blue:226.f/255.f alpha:1.f].CGColor;
    view.layer.shadowOffset  = CGSizeMake(0.0f, 0.0f);
    view.layer.shadowOpacity = 0.7f;
    view.layer.masksToBounds = NO;
    
    UIEdgeInsets shadowInsets     = UIEdgeInsetsMake(0, 0, -1.5f, 0);
    UIBezierPath *shadowPath      = [UIBezierPath bezierPathWithRect:UIEdgeInsetsInsetRect(view.bounds, shadowInsets)];
    view.layer.shadowPath    = shadowPath.CGPath;
}

-(void)viewWillAppear:(BOOL)animated
{
    img_view=[[UIImageView alloc]init];
    img_view.image=_img_user.image;
}

-(void)setTextFieldProperty :(UITextField*)txtF
{
    //CGRect someRect = CGRectMake(txtF.layer.frame.origin.x, txtF.layer.frame.origin.y, txtF.layer.frame.size.width, 80);
    //txtF = [[UITextField alloc]initWithFrame:someRect];
    txtF.layer.cornerRadius = 5;
    txtF.layer.borderWidth = 1;
    txtF.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    txtF.layer.sublayerTransform = CATransform3DMakeTranslation(12, 0, 0);
}




- (IBAction)btn_UPDATEPROF:(id)sender {
    
    [self CallEditProfile];
    
}

- (IBAction)btn_BACK:(id)sender {
    
    [self.navigationController popViewControllerAnimated:NO];
    
}

- (IBAction)btn_CHOOSE_IMG:(id)sender {
    ipc= [[UIImagePickerController alloc] init];
    ipc.delegate = self;
    ipc.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    ipc.allowsEditing = YES;
    [self presentViewController:ipc animated:YES completion:nil];
}


#pragma mark - GetAllPosts
-(void)CallEditProfile
{
   
    [APP_DELEGATE showLoadingView:@"Please Wait..."];
    
    
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    
    [AddPost setValue:@"UpdateProfile" forKey:@"action"];
    [AddPost setValue:_txt_fnm.text forKey:@"Firstname"];
    [AddPost setValue:_txt_lnm.text forKey:@"Lastname"];
    [AddPost setValue:_txt_goal.text forKey:@"SetTarget"];
    [AddPost setValue:_txt_pyear.text forKey:@"PreviousYearScore"];
    [AddPost setValue:[ParseData valueForKey:@"StudentID"] forKey:@"StudID"];
    [AddPost setValue:@"123456" forKey:@"player_id"];
    
    //UIImage *image = [UIImage imageNamed:@"tour_board"];
    //NSData *imageData = UIImagePNGRepresentation(img_view.image);
    NSData *imageData = UIImageJPEGRepresentation(img_view.image, 1);
    
    NSString *queryStringss = [NSString stringWithFormat:@"%@datasync?",BaseURLAPI];
    queryStringss = [queryStringss stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    [manager POST:queryStringss parameters:AddPost constructingBodyWithBlock:^(id<AFMultipartFormData> formData)
     {
         [formData appendPartWithFileData:imageData name:@"image" fileName:@"tour_board.jpg" mimeType:[self contentTypeForImageData:imageData]];
         
     }
          success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
       
         NSLog(@"responseObject %@",responseObject);
         
         NSLog(@"Success: %@ ***** %@", operation.responseString, responseObject);
         
         if ([[responseObject valueForKey:@"status"] intValue]==1) {
             [ParseData setValue:_txt_fnm.text forKey:@"FirstName"];
             [ParseData setValue:_txt_lnm.text forKey:@"LastName"];
             //[ParseData setValue:_img_user.image forKey:@"ProfileImage"];
             
             
             [[NSUserDefaults standardUserDefaults]setObject:ParseData forKey:@"ParseData"];
             
             [[NSUserDefaults standardUserDefaults] setValue:_txt_goal.text forKey:@"CYear"];
             [[NSUserDefaults standardUserDefaults] setValue:_txt_pyear.text forKey:@"PYear"];
             //[[NSUserDefaults standardUserDefaults] setObject:img_view.image forKey:@"IMG_USER_PROF"];
             
             
             [APP_DELEGATE hideLoadingView];
             [self.navigationController popViewControllerAnimated:NO];
             
             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"UPDATE"
                                                             message:@"Update Profile Successfully" delegate:self cancelButtonTitle:nil
                                                   otherButtonTitles:@"OK", nil];
             
             [alert show];

         }
         
         
     }
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         NSLog(@"Error: %@ ***** %@", operation.responseString, error);
     }];
}


-(NSString *)contentTypeForImageData:(NSData *)data {
    uint8_t c;
    [data getBytes:&c length:1];
    
    switch (c) {
        case 0xFF:
            return @"image/jpeg";
        case 0x89:
            return @"image/png";
        case 0x47:
            return @"image/gif";
        case 0x49:
        case 0x4D:
            return @"image/tiff";
    }
    return nil;
}



#pragma mark - ImagePickerController Delegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
   
    UIImage* originalImage = nil;
    originalImage = [info objectForKey:UIImagePickerControllerEditedImage];
    if(originalImage==nil)
    {
        originalImage = [info objectForKey:UIImagePickerControllerEditedImage];
    }
    if(originalImage==nil)
    {
        originalImage = [info objectForKey:UIImagePickerControllerCropRect];
    }
    
    img_view.image = originalImage;
    [ipc dismissViewControllerAnimated:YES completion:nil];
    _img_user.image=originalImage;
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
}




@end
