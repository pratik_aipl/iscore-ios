//
//  NotificationCollectionVC.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 8/11/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyDBQueries.h"
#import "MyDBResultParser.h"



@interface NotificationCollectionVC : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource>
{
    MyDBQueries *myDbQueris;
    MyDBResultParser *myDBParser;
    NSMutableArray *arrZooki ;
    NSString *filePath;
}


@property (weak, nonatomic) IBOutlet UICollectionView *coll_main;
@property (strong, nonatomic) IBOutlet UIView *vw_main;
@property (weak, nonatomic) IBOutlet UIView *vw_grad;


@property (weak, nonatomic) IBOutlet UILabel *lbl_cpage;
@property (weak, nonatomic) IBOutlet UILabel *lbl_tpage;



- (IBAction)btn_CANCEL:(id)sender;

@end
