//
//  NotificationCollCell.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 8/11/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationCollCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *img_page;
@property (weak, nonatomic) IBOutlet UIView *vw_page;
@property (weak, nonatomic) IBOutlet UILabel *lbl_page_title;
@property (weak, nonatomic) IBOutlet UILabel *lbl_page_dt;
@property (weak, nonatomic) IBOutlet UITextView *txtv_details;

@property (weak, nonatomic) IBOutlet UIScrollView *scroll_page;

@property (weak, nonatomic) IBOutlet UIButton *btn_vw_more;



@end
