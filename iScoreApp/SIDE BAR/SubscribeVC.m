//
//  SubscribeVC.m
//  iScoreApp
//
//  Created by ADMIN-Khushal on 8/9/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "SubscribeVC.h"
#import "HomeVC.h"
#import "SSBoard.h"
#import "SSKeyVC.h"



@interface SubscribeVC ()

@end

@implementation SubscribeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
     self.navigationController.navigationBarHidden=YES;
    
    [self setShadow:_vw_view1];
    [self setShadow:_vw_view2];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)setShadow :(UIView*)view
{
    view.layer.shadowRadius  = 2.2f;
    view.layer.shadowColor   = [UIColor colorWithRed:176.f/255.f green:199.f/255.f blue:226.f/255.f alpha:1.f].CGColor;
    view.layer.shadowOffset  = CGSizeMake(0.0f, 0.0f);
    view.layer.shadowOpacity = 0.7f;
    view.layer.masksToBounds = NO;
    
    UIEdgeInsets shadowInsets     = UIEdgeInsetsMake(0, 0, -1.5f, 0);
    UIBezierPath *shadowPath      = [UIBezierPath bezierPathWithRect:UIEdgeInsetsInsetRect(view.bounds, shadowInsets)];
    view.layer.shadowPath    = shadowPath.CGPath;
}


- (IBAction)btn_BACK:(id)sender {
    
    HomeVC * next = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeVC"];
    [self.navigationController pushViewController:next animated:NO];
    
}

- (IBAction)btn_PURC_KEY:(id)sender {

    SSBoard * next = [self.storyboard instantiateViewControllerWithIdentifier:@"SSBoard"];
    [self.navigationController pushViewController:next animated:YES];
    
}

- (IBAction)btn_ALREADY_KEY:(id)sender {
    SSKeyVC * next = [self.storyboard instantiateViewControllerWithIdentifier:@"SSKeyVC"];
    [self.navigationController pushViewController:next animated:YES];
}


@end
