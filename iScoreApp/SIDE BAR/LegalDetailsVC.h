//
//  LegalDetailsVC.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 8/9/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>

@interface LegalDetailsVC : UIViewController



@property (retain , nonatomic) NSString *title_to;


@property (weak, nonatomic) IBOutlet UILabel *lbl_title;
//@property (weak, nonatomic) IBOutlet UIWebView *web_legal;
@property (strong, nonatomic) IBOutlet WKWebView *web_legal;


- (IBAction)btn_BACK:(id)sender;

@end
