//
//  QueryVC.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 8/9/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QueryVC : UIViewController




//Outlet
@property (weak, nonatomic) IBOutlet UITextView *txt_edit;
@property (weak, nonatomic) IBOutlet UIButton *btn_send_msg;

//Action
- (IBAction)btn_SEND:(id)sender;
- (IBAction)btn_BACK:(id)sender;
- (IBAction)btn_CALL:(id)sender;


@end
