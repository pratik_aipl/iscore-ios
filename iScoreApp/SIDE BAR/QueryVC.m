//
//  QueryVC.m
//  iScoreApp
//
//  Created by ADMIN-Khushal on 8/9/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "QueryVC.h"
#import "UIColor+CL.h"
#import "HomeVC.h"
#import "Constant.h"
#import "FMResultSet.h"
#import "MyModel.h"

#import "EditProfileVC.h"
#import "ApplicationConst.h"




@interface QueryVC ()

@end

@implementation QueryVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden=YES;
    
    _txt_edit.layer.borderWidth = 1.0f;
    _txt_edit.layer.cornerRadius=5.0f;
    _txt_edit.layer.borderColor = [[UIColor grayColor] CGColor];
    _txt_edit.delegate=self;
    _txt_edit.text = @"Type your feedback message";
    _txt_edit.textColor = [UIColor lightGrayColor];
    
    
    _btn_send_msg.layer.cornerRadius=5.0;
    
    
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    if([textView.text isEqual: @"Type your feedback message"])
    {
        textView.text = @"";
        textView.textColor = [UIColor blackColor];
    }
    return YES;
}
- (IBAction)btn_SEND:(id)sender {
   
    if (![_txt_edit.text isEqual: @"Type your feedback message"] && ![_txt_edit.text isEqual: @""]) {
        NSLog(@"Call API");
        [self CallPutQuery];
        
    }
    else
    {
         NSLog(@"Enter valid Feedback");
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@"Alert"
                                     message:@"Enter valid Feedback"
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        //Add Buttons
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
                                        //Handle your yes please button action here
                                        
                                    }];
       
        
        [alert addAction:yesButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    }

}

- (IBAction)btn_BACK:(id)sender {
    HomeVC * next = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeVC"];
    [self.navigationController pushViewController:next animated:NO];
}

- (IBAction)btn_CALL:(id)sender {
    //7710012112
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@",@"7710012112"]]];
}

-(void)CallPutQuery
{
    [APP_DELEGATE showLoadingView:@""];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setValue:[[NSUserDefaults standardUserDefaults] valueForKeyPath:@"ParseData.EmailID"] forKey:@"from"];
    [params setValue:_txt_edit.text forKey:@"QueryMessage"];
    
    
    NSLog(@"params===%@",params);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    NSLog(@"%@",[NSString stringWithFormat:@"%@query_message?",BaseURLAPI]);
    
    [manager POST:[NSString stringWithFormat:@"%@query_message?",BaseURLAPI] parameters:[params copy] success:^(AFHTTPRequestOperation *operation, id responseObject){
        NSLog(@"responseObject ==%@",responseObject);
        
        if ([responseObject valueForKey:@"status"]) {
            UIAlertController * alert = [UIAlertController
                                         alertControllerWithTitle:@"Alert"
                                         message:@"Send feedback successfully"
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            
            UIAlertAction* yesButton = [UIAlertAction
                                        actionWithTitle:@"OK"
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * action) {
                                            //Handle your yes please button action here
                                            _txt_edit.text = @"Type your feedback message";
                                            _txt_edit.textColor = [UIColor lightGrayColor];
                                            [_txt_edit resignFirstResponder];
                                        }];
            
            
             [alert addAction:yesButton];
             [self presentViewController:alert animated:YES completion:nil];
            
            
            [APP_DELEGATE hideLoadingView];
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
         [APP_DELEGATE hideLoadingView];
        
    }];
}



@end
