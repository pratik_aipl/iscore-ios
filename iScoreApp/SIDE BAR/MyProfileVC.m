//
//  MyProfileVC.m
//  iScoreApp
//
//  Created by ADMIN-Khushal on 8/9/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "MyProfileVC.h"
#import "FMResultSet.h"
#import "MyModel.h"

#import "HomeVC.h"
#import "EditProfileVC.h"
#import "ApplicationConst.h"



@interface MyProfileVC ()

@end

@implementation MyProfileVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden=YES;
    
    _scrollvw_main.delegate= self;
    [_scrollvw_main setShowsHorizontalScrollIndicator:NO];
    [_scrollvw_main setShowsVerticalScrollIndicator:NO];
    _scrollvw_main.scrollEnabled= YES;
    _scrollvw_main.userInteractionEnabled= YES;
    _scrollvw_main.contentSize= CGSizeMake(_scrollvw_main.frame.size.width ,740);
    
    
    
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"LASTSync"] length]<1) {
        _lbl_lastsync_dt.text=@"N/A";
    }
    else
    {
        _lbl_lastsync_dt.text=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"LASTSync"]];
    }
    
    
    [self setShadow:_vw_1];
    [self setShadow:_vw_2];
    [self setShadow:_vw_3];
    
    
    
    ///ACCUracy
    [self CallGetAccuracy];
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    //[self CallChangeSyncDate];
    
    NSString *subjAcc=[[NSString alloc]init];
    int totacc = 0;
    
    for (int i=0; i<arrAccu.count; i++) {
        totacc+=[[[arrAccu valueForKey:@"accuracy1"] objectAtIndex:i] intValue];
    }
    
   // NSLog(@"%@",[NSString stringWithFormat:@"%02lu",totacc/arrAccu.count]);
    
    //////WEBVIEW
    NSString *progressPath = [[NSBundle mainBundle] pathForResource:@"progress" ofType:@"html"];
    NSString* filePath=[NSString stringWithContentsOfFile:progressPath encoding:NSASCIIStringEncoding error:nil];
    
    filePath = [filePath stringByReplacingOccurrencesOfString:@"0011" withString:[NSString stringWithFormat:@"%02lu",totacc/arrAccu.count]];
    
    filePath = [filePath stringByReplacingOccurrencesOfString:@"0012" withString:[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"CYear"]]];
    NSData *htmlData = [NSKeyedArchiver archivedDataWithRootObject:filePath];
    [filePath writeToFile:progressPath atomically:YES encoding:NSASCIIStringEncoding error:nil];
    
    NSLog(@"====%@",filePath);
    
    //NSData *htmlData = [NSData dataWithContentsOfFile:filePath];
    if (htmlData) {
        NSBundle *bundle = [NSBundle mainBundle];
        NSString *path = [bundle bundlePath];
        NSString *fullPath = [NSBundle pathForResource:@"progress" ofType:@"html" inDirectory:path];
        [_web_goal loadRequest:[NSURLRequest requestWithURL:[NSURL fileURLWithPath:fullPath]]];
        _web_goal.userInteractionEnabled = NO;
    }
    
    
    ParseData=[[NSMutableDictionary alloc]init];
    ParseData=[[NSUserDefaults standardUserDefaults] valueForKey:@"ParseData"];
    
    NSLog(@"ParseData ==%@",ParseData);
    
    _lbl_flnm.text=[NSString stringWithFormat:@"%@ %@",[ParseData valueForKey:@"FirstName"],[ParseData valueForKey:@"LastName"]];
    _lbl_email.text=[NSString stringWithFormat:@"%@",[ParseData valueForKey:@"EmailID"]];
    _lbl_mobile.text=[NSString stringWithFormat:@"%@",[ParseData valueForKey:@"RegMobNo"]];
    _lbl_key.text=[NSString stringWithFormat:@"%@",[ParseData valueForKey:@"StudentKey"]];
    _lbl_board.text=[NSString stringWithFormat:@"%@",[ParseData valueForKey:@"BoardName"]];
    _lbl_medium.text=[NSString stringWithFormat:@"%@",[ParseData valueForKey:@"MediumName"]];
    _lbl_class.text=[NSString stringWithFormat:@"%@",[ParseData valueForKey:@"ClassName"]];
    
    _lbl_pyear.text=[NSString stringWithFormat:@"%@%%",[[NSUserDefaults standardUserDefaults] valueForKey:@"PYear"]];
    
    _lbl_flname.text=[NSString stringWithFormat:@"%@ %@",[ParseData valueForKey:@"FirstName"],[ParseData valueForKey:@"LastName"]];
    
    
    //_img_user_prof.image=[ParseData valueForKey:@"ClassName"];
    
    
}

- (IBAction)btn_BACK:(id)sender {
    //[self.navigationController popViewControllerAnimated:YES];
    
    HomeVC * next = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeVC"];
    [self.navigationController pushViewController:next animated:NO];
}

- (IBAction)btn_EDIT1:(id)sender {
    EditProfileVC * next = [self.storyboard instantiateViewControllerWithIdentifier:@"EditProfileVC"];
    [self.navigationController pushViewController:next animated:YES];
}

- (IBAction)btn_EDIT2:(id)sender {
    EditProfileVC * next = [self.storyboard instantiateViewControllerWithIdentifier:@"EditProfileVC"];
    [self.navigationController pushViewController:next animated:YES];
}




-(void)CallMCQParseData
{
    
    NSMutableArray *chapterArray=[[NSMutableArray alloc]init];
    NSMutableArray *detailArray=[[NSMutableArray alloc]init];
    NSMutableArray *levelArray=[[NSMutableArray alloc]init];
    
    NSMutableArray *temp=[[NSMutableArray alloc]init];
    temp=[[NSUserDefaults standardUserDefaults] valueForKey:@"tempHDRArr"];
    
    for (int i=0; i<temp.count; i++) {
        NSMutableDictionary *tempdict=[[NSMutableDictionary alloc]initWithDictionary:[temp objectAtIndex:i]];
        chapterArray=[[NSMutableArray alloc]initWithArray:[tempdict valueForKey:@"chapterArray"]];
        [tempdict removeObjectForKey:@"chapterArray"];
        
        detailArray=[[NSMutableArray alloc]initWithArray:[tempdict valueForKey:@"detailArray"]];
        [tempdict removeObjectForKey:@"detailArray"];
        
        levelArray=[[NSMutableArray alloc]initWithArray:[tempdict valueForKey:@"levelArray"]];
        [tempdict removeObjectForKey:@"levelArray"];
        
        [tempdict removeObjectForKey:@"StudentMCQTestHDRID"];
        
        NSLog(@"tempdict %@",tempdict);
        
        [MyModel insertInto_papertype:tempdict :@"student_mcq_test_hdr"];
        
        int lastID = [MyModel getLastInsertedRowID];
        NSLog(@"lastID %d",lastID);
        
        
        
        for (int c=0; c<chapterArray.count; c++) {
            NSMutableDictionary *temp=[[NSMutableDictionary alloc]initWithDictionary:[chapterArray objectAtIndex:c]];
            
            [temp removeObjectForKey:@"StudentMCQTestChapterID"];
            
            [temp setValue:[NSString stringWithFormat:@"%d",lastID] forKey:@"StudentMCQTestHDRID"];
            
            [MyModel insertInto_papertype:temp :@"student_mcq_test_chapter"];
        }
        
        
        for (int d=0; d<detailArray.count; d++) {
            NSMutableDictionary *temp=[[NSMutableDictionary alloc]initWithDictionary:[detailArray objectAtIndex:d]];
            
            [temp removeObjectForKey:@"StudentMCQTestDTLID"];
            
            [temp setValue:[NSString stringWithFormat:@"%d",lastID] forKey:@"StudentMCQTestHDRID"];
            
            [MyModel insertInto_papertype:temp :@"student_mcq_test_dtl"];
        }
        
        
        for (int l=0; l<levelArray.count; l++) {
            NSMutableDictionary *temp=[[NSMutableDictionary alloc]initWithDictionary:[levelArray objectAtIndex:l]];
            
            [temp removeObjectForKey:@"StudentMCQTestLevelID"];
            
            [temp setValue:[NSString stringWithFormat:@"%d",lastID] forKey:@"StudentMCQTestHDRID"];
            
            [MyModel insertInto_papertype:temp :@"student_mcq_test_level"];
        }
        
    }
    
}

-(void)CallGPParseData
{
    NSMutableArray *chapterArray=[[NSMutableArray alloc]init];
    NSMutableArray *detailArray=[[NSMutableArray alloc]init];
    NSMutableArray *setDetailArray=[[NSMutableArray alloc]init];
    NSMutableArray *queTypeArray=[[NSMutableArray alloc]init];
    //NSMutableArray *subQueArray=[[NSMutableArray alloc]init];
    
    
    NSMutableArray *temp=[[NSMutableArray alloc]init];
    temp=[[NSUserDefaults standardUserDefaults] valueForKey:@"temparrGP"];
    
    
    for (int i=0; i<temp.count; i++) {
        NSMutableDictionary *tempdict=[[NSMutableDictionary alloc]initWithDictionary:[temp objectAtIndex:i]];
        
        [tempdict removeObjectForKey:@"StudentQuestionPaperID"];
        
        chapterArray=[[NSMutableArray alloc]initWithArray:[tempdict valueForKey:@"chapterArray"]];
        [tempdict removeObjectForKey:@"chapterArray"];
        
        detailArray=[[NSMutableArray alloc]initWithArray:[tempdict valueForKey:@"detailArray"]];
        [tempdict removeObjectForKey:@"detailArray"];
        
        queTypeArray=[[NSMutableArray alloc]initWithArray:[tempdict valueForKey:@"queTypeArray"]];
        [tempdict removeObjectForKey:@"queTypeArray"];
        
        
        NSLog(@"tempdict %@",tempdict);
        [MyModel insertInto_papertype:tempdict :@"student_question_paper"];
        
        int lastID = [MyModel getLastInsertedRowID];
        NSLog(@"lastID %d",lastID);
        
        
        for (int c=0; c<chapterArray.count; c++) {
            NSMutableDictionary *temp=[[NSMutableDictionary alloc]initWithDictionary:[chapterArray objectAtIndex:c]];
            
            [temp removeObjectForKey:@"StudentQuestionPaperChapterID"];
            
            [temp setValue:[NSString stringWithFormat:@"%d",lastID] forKey:@"StudentQuestionPaperID"];
            
            [MyModel insertInto_papertype:temp :@"student_question_paper_chapter"];
        }
        
        
        for (int d=0; d<detailArray.count; d++) {
            NSMutableDictionary *temp=[[NSMutableDictionary alloc]initWithDictionary:[detailArray objectAtIndex:d]];
            
            [temp removeObjectForKey:@"StudentQuestionPaperDetailID"];
            [temp removeObjectForKey:@"subQuesArray"];
            
            [temp setValue:[NSString stringWithFormat:@"%d",lastID] forKey:@"StudentQuestionPaperID"];
            
            [MyModel insertInto_papertype:temp :@"student_question_paper_detail"];
        }
        
        
        for (int e=0; e<queTypeArray.count; e++) {
            NSMutableDictionary *temp=[[NSMutableDictionary alloc]initWithDictionary:[queTypeArray objectAtIndex:e]];
            
            [temp removeObjectForKey:@"StudentSetPaperQuestionTypeID"];
            [temp setValue:[NSString stringWithFormat:@"%d",lastID] forKey:@"StudentQuestionPaperID"];
            
            setDetailArray=[[NSMutableArray alloc]initWithArray:[temp valueForKey:@"setdetailArray"]];
            [temp removeObjectForKey:@"setdetailArray"];
            
            [MyModel insertInto_papertype:temp :@"student_set_paper_question_type"];
            int setID = [MyModel getLastInsertedRowID];
            
            for (int n=0; n<setDetailArray.count; n++) {
                NSMutableDictionary *temp1=[[NSMutableDictionary alloc]initWithDictionary:[setDetailArray objectAtIndex:n]];
                [temp1 removeObjectForKey:@"StudentSetPaperDetailID"];
                [temp1 setValue:[NSString stringWithFormat:@"%d",setID] forKey:@"StudentSetPaperQuestionTypeID"];
                [temp removeObjectForKey:@"subQuesArray"];
                
                [MyModel insertInto_papertype:temp :@"student_set_paper_detail"];
                
            }
        }
    }
}

- (IBAction)btn_SYNC:(id)sender {
    
    [APP_DELEGATE showLoadingView:@"DATA SYNC"];
    [self CallGenaratJson];
    [self CallMCQJson];
    [self CallInCorrectJson];
    [self CallNotApperedJson];
    [self CallProfileSync];
    [self getCallZOOKI];
    
    
}


-(void)CallNotApperedJson
{
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"ProfileLastSync"] length]<1) {
        _ProfileLastSync.text=@"0000-00-00 00:00:00";
    }
    else
    {
        _ProfileLastSync.text=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"ProfileLastSync"]];
    }
    
    NSMutableArray *mainArr=[[NSMutableArray alloc]init];
    NSMutableDictionary *mainDict=[[NSMutableDictionary alloc]init];
    
    FMResultSet *results = [MyModel selectQuery:[NSString stringWithFormat:@"select * from student_not_appeared_question where CreatedOn <'%@'",_ProfileLastSync.text]];
    while ([results next])
    {
        mainDict=[[NSMutableDictionary alloc]initWithDictionary:[results resultDictionary]];
        [mainArr addObject:mainDict];
    }
    
    NSLog(@"mainArr == %@",mainArr);
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:mainArr options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    NSLog(@"jsonString ==%@",jsonString);
    
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setValue:@"NotAppearedQuestion" forKey:@"action"];
    [params setValue:jsonString forKey:@"data"];
    [params setValue:[ParseData valueForKey:@"StudentID"] forKey:@"StudID"];
    
    
    NSLog(@"params===%@",params);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    NSLog(@"%@",[NSString stringWithFormat:@"%@datasync?",BaseURLAPI]);
    
    [manager POST:[NSString stringWithFormat:@"%@datasync?",BaseURLAPI] parameters:[params copy] success:^(AFHTTPRequestOperation *operation, id responseObject){
        NSLog(@"responseObject ==%@",responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        
    }];
    
}


-(void)CallInCorrectJson
{
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"ProfileLastSync"] length]<1) {
        _ProfileLastSync.text=@"0000-00-00 00:00:00";
    }
    else
    {
        _ProfileLastSync.text=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"ProfileLastSync"]];
    }
    
    NSMutableArray *mainArr=[[NSMutableArray alloc]init];
    NSMutableDictionary *mainDict=[[NSMutableDictionary alloc]init];
    
    FMResultSet *results = [MyModel selectQuery:[NSString stringWithFormat:@"select * from student_incorrect_question where CreatedOn <'%@'",_ProfileLastSync.text]];
    while ([results next])
    {
        mainDict=[[NSMutableDictionary alloc]initWithDictionary:[results resultDictionary]];
        [mainArr addObject:mainDict];
    }
    
    NSLog(@"mainArr == %@",mainArr);
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:mainArr options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    NSLog(@"jsonString ==%@",jsonString);
    
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setValue:@"IncorrectQuestion" forKey:@"action"];
    [params setValue:jsonString forKey:@"data"];
    [params setValue:[ParseData valueForKey:@"StudentID"] forKey:@"StudID"];
    
    
    NSLog(@"params===%@",params);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    NSLog(@"%@",[NSString stringWithFormat:@"%@datasync?",BaseURLAPI]);
    
    [manager POST:[NSString stringWithFormat:@"%@datasync?",BaseURLAPI] parameters:[params copy] success:^(AFHTTPRequestOperation *operation, id responseObject){
        NSLog(@"responseObject ==%@",responseObject);
        
        NSMutableArray *arrInCorr=[[NSMutableArray alloc]init];
        
        if ([responseObject valueForKey:@"status"]) {
            arrInCorr=[[NSMutableArray alloc]initWithArray:[responseObject valueForKey:@"data"]];
        }
        
        [[NSUserDefaults standardUserDefaults] setObject:arrInCorr forKey:@"tempInCorrArr"];
        NSLog(@"arrHDR== %@",arrInCorr);
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        
    }];
    
}


-(void)CallMCQJson
{
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"ProfileLastSync"] length]<1) {
        _ProfileLastSync.text=@"0000-00-00 00:00:00";
    }
    else
    {
        _ProfileLastSync.text=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"ProfileLastSync"]];
    }
    
    NSMutableArray *mainArr=[[NSMutableArray alloc]init];
    NSMutableArray *chapterArr=[[NSMutableArray alloc]init];
    NSMutableArray *levelArr=[[NSMutableArray alloc]init];
    NSMutableArray *detailsArr=[[NSMutableArray alloc]init];
    
    NSMutableDictionary *mainDict=[[NSMutableDictionary alloc]init];
    NSMutableDictionary *chapterDict=[[NSMutableDictionary alloc]init];
    NSMutableDictionary *levelDict=[[NSMutableDictionary alloc]init];
    NSMutableDictionary *detailsDict=[[NSMutableDictionary alloc]init];
    
    FMResultSet *results = [MyModel selectQuery:[NSString stringWithFormat:@"select * from student_mcq_test_hdr where CreatedOn >'%@'",_ProfileLastSync.text]];
    
    while ([results next])
    {
        mainDict=[[NSMutableDictionary alloc]initWithDictionary:[results resultDictionary]];
        [mainDict setValue:@"" forKey:@"TimerStatus"];//
        [mainDict setValue:@"1" forKey:@"TestTime"];
        int SQPID=[[mainDict valueForKey:@"StudentMCQTestHDRID"] intValue];
        
        FMResultSet *results1 = [MyModel selectQuery:[NSString stringWithFormat:@"select * from student_mcq_test_level where StudentMCQTestHDRID=%d",SQPID]];
        levelArr=[[NSMutableArray alloc]init];
        while ([results1 next])
        {
            [levelArr addObject:[results1 resultDictionary]];
            
        }
        [mainDict setObject:levelArr forKey:@"levelArray"];
        
        FMResultSet *results2 = [MyModel selectQuery:[NSString stringWithFormat:@"select * from student_mcq_test_chapter where StudentMCQTestHDRID=%d",SQPID]];
        chapterArr=[[NSMutableArray alloc]init];
        while ([results2 next])
        {
            chapterDict=[[NSMutableDictionary alloc]initWithDictionary:[results2 resultDictionary]];
            [chapterArr addObject:chapterDict];
        }
        [mainDict setObject:chapterArr forKey:@"chapterArray"];
        
        
        FMResultSet *results3 = [MyModel selectQuery:[NSString stringWithFormat:@"select * from student_mcq_test_dtl where StudentMCQTestHDRID=%d",SQPID]];
        detailsArr=[[NSMutableArray alloc]init];
        detailsDict=[[NSMutableDictionary alloc]init];
        while ([results3 next])
        {
            detailsDict=[[NSMutableDictionary alloc]initWithDictionary:[results3 resultDictionary]];
            [detailsArr addObject:detailsDict];
        }
        [mainDict setObject:detailsArr forKey:@"detailArray"];
        
        [mainArr addObject:mainDict];
    }
    
    NSLog(@"mainArr==%@",mainArr);
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:mainArr options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    NSLog(@"jsonString ==%@",jsonString);
    
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setValue:@"mcq" forKey:@"action"];
    [params setValue:jsonString forKey:@"data"];
    [params setValue:[ParseData valueForKey:@"StudentID"] forKey:@"StudID"];
    
    
    NSLog(@"params===%@",params);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    NSLog(@"%@",[NSString stringWithFormat:@"%@datasync?",BaseURLAPI]);
    
    [manager POST:[NSString stringWithFormat:@"%@datasync?",BaseURLAPI] parameters:[params copy] success:^(AFHTTPRequestOperation *operation, id responseObject){
        NSLog(@"responseObject ==%@",responseObject);
        
        NSMutableArray *arrHDR=[[NSMutableArray alloc]init];
        
        if ([responseObject valueForKey:@"status"]) {
            arrHDR=[[NSMutableArray alloc]initWithArray:[responseObject valueForKey:@"data"]];
        }
        
        [[NSUserDefaults standardUserDefaults] setObject:arrHDR forKey:@"tempHDRArr"];
        NSLog(@"arrHDR== %@",arrHDR);
        [self CallMCQParseData];
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        
    }];
    
}



-(void)CallGenaratJson
{
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"ProfileLastSync"] length]<1) {
        _ProfileLastSync.text=@"0000-00-00 00:00:00";
    }
    else
    {
        _ProfileLastSync.text=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"ProfileLastSync"]];
    }
    
    NSMutableArray *mainArr=[[NSMutableArray alloc]init];
    NSMutableArray *chapterArr=[[NSMutableArray alloc]init];
    NSMutableArray *detailsArr=[[NSMutableArray alloc]init];
    NSMutableArray *QueTypeArr=[[NSMutableArray alloc]init];
    NSMutableArray *setDetailsArr=[[NSMutableArray alloc]init];
    NSMutableArray *subQueArr=[[NSMutableArray alloc]init];
    
    NSMutableDictionary *mainDict=[[NSMutableDictionary alloc]init];
    NSMutableDictionary *detailsDict=[[NSMutableDictionary alloc]init];
    NSMutableDictionary *QueTypeDict=[[NSMutableDictionary alloc]init];
    NSMutableDictionary *setDetailsDict=[[NSMutableDictionary alloc]init];
    
    
    FMResultSet *results = [MyModel selectQuery:[NSString stringWithFormat:@"select * from student_question_paper where CreatedOn >'%@'",_ProfileLastSync.text]];
    
    while ([results next])
    {
        mainDict=[[NSMutableDictionary alloc]initWithDictionary:[results resultDictionary]];
        int SQPID=[[mainDict valueForKey:@"StudentQuestionPaperID"] intValue];
        
        FMResultSet *results1 = [MyModel selectQuery:[NSString stringWithFormat:@"select * from student_question_paper_chapter where StudentQuestionPaperID=%d",SQPID]];
        chapterArr=[[NSMutableArray alloc]init];
        while ([results1 next])
        {
            [chapterArr addObject:[results1 resultDictionary]];
        }
        [mainDict setObject:chapterArr forKey:@"chapterArray"];
        
        FMResultSet *results2 = [MyModel selectQuery:[NSString stringWithFormat:@"select * from student_question_paper_detail where StudentQuestionPaperID=%d",SQPID]];
        detailsArr=[[NSMutableArray alloc]init];
        subQueArr=[[NSMutableArray alloc]init];
        while ([results2 next])
        {
            detailsDict=[[NSMutableDictionary alloc]initWithDictionary:[results2 resultDictionary]];
            [detailsDict setObject:subQueArr forKey:@"subQuesArray"];
            [detailsArr addObject:detailsDict];
        }
        [mainDict setObject:detailsArr forKey:@"detailArray"];
        
        
        FMResultSet *results3 = [MyModel selectQuery:[NSString stringWithFormat:@"select * from student_set_paper_question_type where StudentQuestionPaperID=%d",SQPID]];
        QueTypeArr=[[NSMutableArray alloc]init];
        
        while ([results3 next])
        {
            QueTypeDict=[[NSMutableDictionary alloc]initWithDictionary:[results3 resultDictionary]];
            int SQTID=[[QueTypeDict valueForKey:@"StudentSetPaperQuestionTypeID"]  intValue];
            FMResultSet *results4 = [MyModel selectQuery:[NSString stringWithFormat:@"select * from student_set_paper_detail where StudentSetPaperQuestionTypeID=%d",SQTID]];
            setDetailsArr=[[NSMutableArray alloc]init];
            subQueArr=[[NSMutableArray alloc]init];
            while ([results4 next])
            {
                setDetailsDict=[[NSMutableDictionary alloc]initWithDictionary:[results4 resultDictionary]];
                [setDetailsDict setObject:subQueArr forKey:@"subQuesArray"];
                [setDetailsArr addObject:setDetailsDict];
            }
            [QueTypeDict setObject:setDetailsArr forKey:@"setdetailArray"];
            [QueTypeArr addObject:QueTypeDict];
        }
        [mainDict setObject:QueTypeArr forKey:@"queTypeArray"];
        [mainArr addObject:mainDict];
    }
    
    NSLog(@"mainArr==%@",mainArr);
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:mainArr options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    NSLog(@"jsonString ==%@",jsonString);
    
    
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setValue:@"GeneratePaper" forKey:@"action"];
    [params setValue:jsonString forKey:@"data"];
    [params setValue:[ParseData valueForKey:@"StudentID"] forKey:@"StudID"];
    
    
    NSLog(@"params===%@",params);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    NSLog(@"%@",[NSString stringWithFormat:@"%@datasync?",BaseURLAPI]);
    
    [manager POST:[NSString stringWithFormat:@"%@datasync?",BaseURLAPI] parameters:[params copy] success:^(AFHTTPRequestOperation *operation, id responseObject){
        NSLog(@"responseObject ==%@",responseObject);
        
        NSMutableArray *arrGP=[[NSMutableArray alloc]init];
        
        if ([responseObject valueForKey:@"status"]) {
            arrGP=[[NSMutableArray alloc]initWithArray:[responseObject valueForKey:@"data"]];
        }
        
        [[NSUserDefaults standardUserDefaults] setObject:arrGP forKey:@"temparrGP"];
        NSLog(@"temparrGP== %@",arrGP);
        [self CallGPParseData];
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        
    }];
    
    
}


-(void)CallProfileSync
{
    NSArray * arr = [_lbl_flnm.text componentsSeparatedByString:@" "];
    NSLog(@"arr %@",arr);
    
    NSUserDefaults *userD=[NSUserDefaults standardUserDefaults];
    
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    
    [AddPost setValue:@"UpdateProfile" forKey:@"action"];
    [AddPost setValue:[arr objectAtIndex:0] forKey:@"Firstname"];
    [AddPost setValue:[arr objectAtIndex:1] forKey:@"Lastname"];
    [AddPost setValue:[userD valueForKey:@"CYear"] forKey:@"SetTarget"];
    [AddPost setValue:[userD valueForKey:@"PYear"] forKey:@"PreviousYearScore"];
    [AddPost setValue:[ParseData valueForKey:@"StudentID"] forKey:@"StudID"];
    [AddPost setValue:@"123456" forKey:@"player_id"];
    
    
    NSData *imageData = UIImageJPEGRepresentation(_img_user_prof.image, 1);
    
    NSString *queryStringss = [NSString stringWithFormat:@"%@datasync?",BaseURLAPI];
    queryStringss = [queryStringss stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    [manager POST:queryStringss parameters:AddPost constructingBodyWithBlock:^(id<AFMultipartFormData> formData)
     {
         [formData appendPartWithFileData:imageData name:@"image" fileName:@"tour_board.jpg" mimeType:[self contentTypeForImageData:imageData]];
         
     }
          success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSLog(@"responseObject %@",responseObject);
         [[NSUserDefaults standardUserDefaults] setValue:[responseObject valueForKey:@"ProfileLastSync"] forKey:@"ProfileLastSync"];
         
         NSArray * arr = [[responseObject valueForKey:@"ProfileLastSync"] componentsSeparatedByString:@" "];
         NSLog(@"arr %@",arr);
         
         _lbl_lastsync_dt.text=[NSString stringWithFormat:@"%@ | %@",[self getDateFormat:[arr objectAtIndex:0]],[self getTimeFormat:[arr objectAtIndex:1]]];
         
         [[NSUserDefaults standardUserDefaults] setValue:_lbl_lastsync_dt.text forKey:@"LASTSync"];
         
         [APP_DELEGATE hideLoadingView];
         
         UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"UPDATE"
                                                         message:@"Data Sync Successfully" delegate:self cancelButtonTitle:nil
                                               otherButtonTitles:@"OK", nil];
         [alert show];
         
     }
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [APP_DELEGATE hideLoadingView];
         NSLog(@"Error: %@ ***** %@", operation.responseString, error);
     }];
    
}


-(void)setShadow :(UIView*)view
{
    view.layer.shadowRadius  = 2.2f;
    view.layer.shadowColor   = [UIColor colorWithRed:176.f/255.f green:199.f/255.f blue:226.f/255.f alpha:1.f].CGColor;
    view.layer.shadowOffset  = CGSizeMake(0.0f, 0.0f);
    view.layer.shadowOpacity = 0.7f;
    view.layer.masksToBounds = NO;
    
    UIEdgeInsets shadowInsets     = UIEdgeInsetsMake(0, 0, -1.5f, 0);
    UIBezierPath *shadowPath      = [UIBezierPath bezierPathWithRect:UIEdgeInsetsInsetRect(view.bounds, shadowInsets)];
    view.layer.shadowPath    = shadowPath.CGPath;
}

-(void)CallGetAccuracy
{
    FMResultSet *results = [MyModel selectQuery:[NSString stringWithFormat:@"select s.*,(select CreatedOn from student_mcq_test_hdr as sthd where sthd.SubjectID=s.SubjectID                       order by CreatedOn desc limit 1) as Last_Date, count(smth.StudentMCQTestHDRID) as TotalSubjectTest,                       (( (select count(StudentMCQTestDTLID) from student_mcq_test_dtl as dtl left join mcqoption as                           opt on opt.MCQOPtionID=dtl.AnswerID where dtl.IsAttempt=1 and opt.isCorrect=1 and EXISTS                           (select StudentMCQTestHDRID from student_mcq_test_hdr hdr where hdr.StudentMCQTestHDRID=dtl.StudentMCQTestHDRID                            and hdr.SubjectID=s.SubjectID ) ) *100) / (select count (StudentMCQTestDTLID)                                                                       from student_mcq_test_dtl dtl where EXISTS (select StudentMCQTestHDRID from student_mcq_test_hdr hdr where                                                                                                                   hdr.StudentMCQTestHDRID=dtl.StudentMCQTestHDRID and hdr.SubjectID=s.SubjectID )) ) as accuracy1                       from subjects as s left join student_mcq_test_hdr as smth on s.SubjectID=smth.SubjectID where s.StandardID=%@ AND s.isMCQ=1 group by s.SubjectID order by s.SubjectOrder",[[NSUserDefaults standardUserDefaults]valueForKeyPath:@"ParseData.StandardID"]]];
    
    
    
    arrAccu=[[NSMutableArray alloc]init];
    
    while ([results next])
    {
        NSMutableDictionary *mdict=[[NSMutableDictionary alloc]init];
        
        [mdict setValue:[results stringForColumn:@"SubjectID"] forKey:@"SubjectID"];
        [mdict setValue:[results stringForColumn:@"SubjectName"] forKey:@"SubjectName"];
        
        if ([[results stringForColumn:@"accuracy1"] intValue]==nil) {
            [mdict setValue:@"0" forKey:@"accuracy1"];
        }
        else
        {
            [mdict setValue:[results stringForColumn:@"accuracy1"] forKey:@"accuracy1"];
        }
        
        [arrAccu addObject:mdict];
    }
    NSLog(@"%@",arrAccu);
}


-(NSString *)contentTypeForImageData:(NSData *)data {
    uint8_t c;
    [data getBytes:&c length:1];
    
    switch (c) {
        case 0xFF:
            return @"image/jpeg";
        case 0x89:
            return @"image/png";
        case 0x47:
            return @"image/gif";
        case 0x49:
        case 0x4D:
            return @"image/tiff";
    }
    return nil;
}

-(NSString*)getDateFormat :(NSString*)date
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *fdate  = [dateFormatter dateFromString:date];
    
    // Convert to new Date Format
    [dateFormatter setDateFormat:@"dd-MMM-yyyy"];
    NSString *newDate = [dateFormatter stringFromDate:fdate];
    
    return newDate;
}
-(NSString*)getTimeFormat :(NSString*)time
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm:ss"];
    NSDate *fdate  = [dateFormatter dateFromString:time];
    
    // Convert to new Date Format
    [dateFormatter setDateFormat:@"hh:mm a"];
    NSString *newDate = [dateFormatter stringFromDate:fdate];
    
    return newDate;
    
}

-(void)CallChangeSyncDate
{
    [APP_DELEGATE showLoadingView:@""];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setValue:[[NSUserDefaults standardUserDefaults]valueForKeyPath:@"ParseData.StudentID"] forKey:@"StudID"];
    [params setValue:@"b752b39e-9606-4149-ae0c-e396cf5aee" forKey:@"player_id"];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
    [manager GET:[NSString stringWithFormat:@"%@update_last_sync_time",BaseURLAPI] parameters:[params copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        
        
        [APP_DELEGATE hideLoadingView];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [APP_DELEGATE hideLoadingView];
        NSLog(@"Error: %@", error);
    }];
}


/////////////////////ZOOKI///////////////////////////

#pragma mark - ZOOKI
-(void)getCallZOOKI
{
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    //[AddPost setValue:[ParseData valueForKey:@"StudentID"] forKey:@"StudID"];
    [AddPost setValue:[[NSUserDefaults standardUserDefaults]valueForKeyPath:@"ParseData.StudentID"] forKey:@"StudID"];
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    [manager GET:[NSString stringWithFormat:@"%@get-zooki?",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject){
        NSLog(@"JSON: %@", responseObject);
        
        if ([[responseObject valueForKey:@"status"] intValue]==1) {
            
            NSArray *tempdata=[[NSArray alloc]initWithArray:[responseObject valueForKeyPath:@"data"]];
            
            [MyModel UpdateQuery:[NSString stringWithFormat:@"DELETE FROM zooki"]];
            
            for (int i=0; i<(unsigned long)tempdata.count; i++) {
                [MyModel insertInto_papertype:[tempdata objectAtIndex:i] :@"zooki"];
            }
            
            
            ShareData.rootImages=[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"folder_path"]];
            
            ShareData.marrImages=[NSMutableArray arrayWithArray:[responseObject valueForKeyPath:@"data.image"]];
            
            
            NSLog(@"  %@",ShareData.marrQueImages);
            NSLog(@"  %@",ShareData.rootImages);
            NSLog(@"  %@",ShareData.marrImages);
            
            
            for (int j=0; j<ShareData.marrImages.count; j++) {
                
                
                //*************FILE PATH**********************
                NSString *imageURL = [NSString stringWithFormat:@"%@/%@",ShareData.rootImages,[ShareData.marrImages objectAtIndex:j]];
                
                NSString *httpReplaceString = @"http://staff.parshvaa.com/";
                NSString *httpsReplaceString = @"https://staff.parshvaa.com/";
                NSString *finalURL;
                
                finalURL = [imageURL stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
                if([imageURL containsString:@"https"]){
                    finalURL = [imageURL stringByReplacingOccurrencesOfString:httpsReplaceString withString:@""];
                } else {
                    finalURL = [imageURL stringByReplacingOccurrencesOfString:httpReplaceString withString:@""];
                }
                NSArray *parts = [finalURL componentsSeparatedByString:@"/"];
                NSFileManager *fileManager = [NSFileManager defaultManager];
                NSString *folderPath =@"";
                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                
                NSError *error;
                
                
                for (int k=3; k<parts.count-1; k++) {
                    if(k!=0){
                        
                        folderPath = [NSString stringWithFormat:@"%@/%@",folderPath,parts[k]];
                        NSString *documentsDirectory = [paths objectAtIndex:0];
                        [fileManager createDirectoryAtPath:[documentsDirectory stringByAppendingPathComponent:folderPath] withIntermediateDirectories:NO attributes:nil error:&error];
                    }else {
                        folderPath = [NSString stringWithFormat:@"%@",parts[k]];
                        NSString *documentsDirectory = [paths objectAtIndex:0];
                        [fileManager createDirectoryAtPath:[documentsDirectory stringByAppendingPathComponent:folderPath] withIntermediateDirectories:NO attributes:nil error:&error];
                    }
                }
                NSString *writablePath = [folderPath stringByAppendingPathComponent:[parts lastObject]];
                
                if(![fileManager fileExistsAtPath:writablePath]){
                    if ([parts lastObject]){
                        NSString *lastPart = [parts lastObject];
                        NSLog(@"Image Name => , %@",lastPart);
                        if([lastPart containsString:@"gif"]){
                            [self getImageFromURLAndSaveItToLocalData:[parts lastObject] fileURL:imageURL inDirectory:folderPath];
                            
                        } else {
                            [self getImageFromURLAndSaveItToLocalData:[parts lastObject] fileURL:imageURL inDirectory:folderPath];
                        }
                    }
                }
                
            }
            
            
            
            NSLog(@"ShareData.marrQueImages %lu",(unsigned long)ShareData.marrQueImages.count);
            NSLog(@"ShareData.marrImages %lu",(unsigned long)ShareData.marrImages.count);
            
            
        }
        //[APP_DELEGATE hideLoadingView];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        //[APP_DELEGATE hideLoadingView];
    }];
}



-(void) getImageFromURLAndSaveItToLocalData:(NSString *)imageName fileURL:(NSString *)fileURL inDirectory:(NSString *)directoryPath {
    
    
    NSLog(@"IMAGE NAME :: %@",imageName);
    NSLog(@"IMAGE URL :: %@",fileURL);
    
    NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString:fileURL]];
    
    NSError *error = nil;
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *tempPath = [paths objectAtIndex:0];;
    
    directoryPath = [tempPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@", directoryPath]];
    directoryPath = [directoryPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@", imageName]];
    
    [[NSFileManager defaultManager] createFileAtPath:directoryPath
                                            contents:data
                                          attributes:nil];
    
}



@end
