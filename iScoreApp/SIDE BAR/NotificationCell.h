//
//  NotificationCell.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 8/11/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UIView *ve_cell;
@property (weak, nonatomic) IBOutlet UILabel *lbl_title;
@property (weak, nonatomic) IBOutlet UILabel *lbl_desc;
@property (weak, nonatomic) IBOutlet UILabel *lbl_days;



@end
