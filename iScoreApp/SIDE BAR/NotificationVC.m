//
//  NotificationVC.m
//  iScoreApp
//
//  Created by ADMIN-Khushal on 8/11/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "NotificationVC.h"
#import "NotificationCell.h"
#import "HomeVC.h"
#import "NotificationCollectionVC.h"



@interface NotificationVC ()

@end

@implementation NotificationVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden=YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NotificationCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //Change the selected background view of the cell.
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
    NotificationCollectionVC * next = [self.storyboard instantiateViewControllerWithIdentifier:@"NotificationCollectionVC"];
    [self.navigationController pushViewController:next animated:NO];
    
    
    
}

- (IBAction)btn_BACK:(id)sender {
    
    HomeVC * next = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeVC"];
    [self.navigationController pushViewController:next animated:NO];
    
}
@end
