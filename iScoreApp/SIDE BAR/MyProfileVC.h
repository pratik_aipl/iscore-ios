//
//  MyProfileVC.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 8/9/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>

@interface MyProfileVC : UIViewController
{
    NSMutableArray *arrAccu;
    NSMutableDictionary *ParseData;
}



@property (weak, nonatomic) IBOutlet UIScrollView *scrollvw_main;

@property (weak, nonatomic) IBOutlet UIView *vw_1;
@property (weak, nonatomic) IBOutlet UIView *vw_2;
@property (weak, nonatomic) IBOutlet UIView *vw_3;
//@property (weak, nonatomic) IBOutlet UIWebView *web_goal;
@property (strong, nonatomic) IBOutlet WKWebView *web_goal;

@property (weak, nonatomic) IBOutlet UILabel *lbl_flname;


@property (weak, nonatomic) IBOutlet UILabel *lbl_flnm;
@property (weak, nonatomic) IBOutlet UILabel *lbl_email;
@property (weak, nonatomic) IBOutlet UILabel *lbl_mobile;
@property (weak, nonatomic) IBOutlet UILabel *lbl_key;
@property (weak, nonatomic) IBOutlet UILabel *lbl_board;
@property (weak, nonatomic) IBOutlet UILabel *lbl_medium;
@property (weak, nonatomic) IBOutlet UILabel *lbl_class;

@property (weak, nonatomic) IBOutlet UILabel *lbl_pyear;

@property (weak, nonatomic) IBOutlet UIButton *btn_syncro;

@property (weak, nonatomic) IBOutlet UIImageView *img_user_prof;

@property (weak, nonatomic) IBOutlet UILabel *lbl_lastsync_dt;
@property (weak, nonatomic) IBOutlet UILabel *ProfileLastSync;


//Action

- (IBAction)btn_BACK:(id)sender;
- (IBAction)btn_EDIT1:(id)sender;
- (IBAction)btn_EDIT2:(id)sender;
- (IBAction)btn_SYNC:(id)sender;


-(void)CallProfileSync;



@end
