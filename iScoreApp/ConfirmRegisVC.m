//
//  ConfirmRegisVC.m
//  iScoreApp
//
//  Created by ADMIN-Khushal on 2/28/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "ConfirmRegisVC.h"
#import "DownloadVC.h"
#import <AFNetworking/AFNetworking.h>
#import "ApplicationConst.h"
#import "HomeVC.h"
#import "Reachability.h"


@interface ConfirmRegisVC ()

-(void)reachabilityChanged:(NSNotification*)note;

@property(strong) Reachability * googleReach;
@property(strong) Reachability * localWiFiReach;
@property(strong) Reachability * internetConnectionReach;


@end

@implementation ConfirmRegisVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _vw_details.layer.borderWidth=1;
    _vw_details.layer.borderColor=[UIColor lightGrayColor].CGColor;
    _vw_details.layer.cornerRadius=4;
    
    _btn_confirm.layer.cornerRadius=4;
    
    NSArray *fontFamilies = [UIFont familyNames];
    
    for (int i = 0; i < [fontFamilies count]; i++)
    {
        NSString *fontFamily = [fontFamilies objectAtIndex:i];
        NSArray *fontNames = [UIFont fontNamesForFamilyName:[fontFamilies objectAtIndex:i]];
        NSLog (@"%@: %@", fontFamily, fontNames);
    }
    _img_profimage.clipsToBounds=YES;
    _img_profimage.layer.cornerRadius=_img_profimage.frame.size.height/2;
   
    
    if ([[_dictdata valueForKey:@"imgflag"]isEqualToString:@"0"]) {
         _img_profimage.image=[_dictdata valueForKey:@"studimg"];
    }
    else
    {
        _img_profimage.image=[_dictdata valueForKey:@"teachimg"];
    }
    
    
    
    NSLog(@"-- %@ ",_dictdata);
    
    
    
    
    
    
    _lbl_usernm.text=[_dictdata valueForKey:@"FirstName"];
    _lbl_email.text=[_dictdata valueForKey:@"EmailID"];
    _lbl_mobile.text=[_dictdata valueForKey:@"RegMobNo"];
    _lbl_board.text=[_dictdata valueForKey:@"BoardName"];
    _lbl_medium.text=[_dictdata valueForKey:@"MediumName"];
    _lbl_standard.text=[_dictdata valueForKey:@"StandardName"];
    
    [_lbl_board setNumberOfLines:0];
    _lbl_board.clipsToBounds=YES;
    CGRect mainframe = _lbl_board.frame;
    mainframe.size.height =  [self getLabelHeight:_lbl_board];
    _lbl_board.frame = mainframe;
    
    [_lbl_email setNumberOfLines:0];
    _lbl_email.clipsToBounds=YES;
    CGRect mainframe1 = _lbl_email.frame;
    mainframe1.size.height =  [self getLabelHeight:_lbl_email];
    _lbl_email.frame = mainframe1;
    
    ///FRAME
 /*   CGRect frame = _lbl_email.frame;
    frame.origin.y =  10;
    _lbl_email.frame = frame;
    
    CGRect frame1 = _lbl_mobile.frame;
    frame1.origin.y =  _lbl_email.frame.origin.y + _lbl_email.frame.size.height+10;
    _lbl_mobile.frame = frame1;
    
    CGRect frame2 = _lbl_board.frame;
    frame2.origin.y =  _lbl_mobile.frame.origin.y + _lbl_mobile.frame.size.height+10;
    _lbl_board.frame = frame2;
    
    CGRect frame3 = _lbl_medium.frame;
    frame3.origin.y =  _lbl_board.frame.origin.y + _lbl_board.frame.size.height+10;
    _lbl_medium.frame = frame3;
    
    CGRect frame4 = _lbl_standard.frame;
    frame4.origin.y =  _lbl_medium.frame.origin.y + _lbl_medium.frame.size.height+10;
    _lbl_standard.frame = frame4;
    */
    
    
    
    CGRect autosize4 = _vw_details.frame;
    autosize4.size.height = _lbl_standard.frame.origin.y+_lbl_standard.frame.size.height+10;
    _vw_details.frame = autosize4;
    
    
  /*
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reachabilityChanged:)
                                                 name:kReachabilityChangedNotification
                                               object:nil];
    
    
    __weak __block typeof(self) weakself = self;
    
    //////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////
    //
    // create a Reachability object for www.google.com
    
    self.googleReach = [Reachability reachabilityWithHostname:@"www.google.com"];
    
    self.googleReach.reachableBlock = ^(Reachability * reachability)
    {
        NSString * temp = [NSString stringWithFormat:@"GOOGLE Block Says Reachable(%@)", reachability.currentReachabilityString];
        NSLog(@"%@", temp);
        
        // to update UI components from a block callback
        // you need to dipatch this to the main thread
        // this uses NSOperationQueue mainQueue
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            //weakself.blockLabel.text = temp;
            //weakself.blockLabel.textColor = [UIColor blackColor];
            isInternet=YES;
        }];
    };
    
    self.googleReach.unreachableBlock = ^(Reachability * reachability)
    {
        NSString * temp = [NSString stringWithFormat:@"GOOGLE Block Says Unreachable(%@)", reachability.currentReachabilityString];
        NSLog(@"%@", temp);
        
        // to update UI components from a block callback
        // you need to dipatch this to the main thread
        // this one uses dispatch_async they do the same thing (as above)
        dispatch_async(dispatch_get_main_queue(), ^{
            //weakself.blockLabel.text = temp;
            //weakself.blockLabel.textColor = [UIColor redColor];
            isInternet=NO;
        });
    };
    
    [self.googleReach startNotifier];
   */
    
    
    
}
- (CGFloat)getLabelHeight:(UILabel*)label
{
    CGSize constraint = CGSizeMake(label.frame.size.width, CGFLOAT_MAX);
    CGSize size;
    
    NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
    CGSize boundingBox = [label.text boundingRectWithSize:constraint
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                               attributes:@{NSFontAttributeName:label.font}
                                                  context:context].size;
    
    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    
    return size.height;
}

/*
-(void)confirm{
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    //Set Params
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:60];
    [request setHTTPMethod:@"POST"];
    //Create boundary, it can be anything
    NSString *boundary = @"------VohpleBoundary4QuqLuM1cE5lMwCy";
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    // post body
    NSMutableData *body = [NSMutableData data];
    //Populate a dictionary with all the regular values you would like to send.
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
//    [parameters setValue:_jobid forKey:@"job_id"];
//
//    [parameters setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"id"]forKey:@"user_id"];
//    [parameters setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"email"]forKey:@"login_email"];
//    [parameters setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"app_token"] forKey:@"login_token"];
    
    [parameters setValue:[_dictdata valueForKey:@"fname"] forKey:@"first_name"];
    [parameters setValue:[_dictdata valueForKey:@"lname"] forKey:@"last_name"];
    [parameters setValue:[_dictdata valueForKey:@"email"] forKey:@"email"];
    [parameters setValue:[_dictdata valueForKey:@"mobile"] forKey:@"register_mobile"];
    [parameters setValue:[_dictdata valueForKey:@"boardID"] forKey:@"board_id"];
    [parameters setValue:[_dictdata valueForKey:@"mediumID"] forKey:@"medium_id"];
    [parameters setValue:[_dictdata valueForKey:@"standID"] forKey:@"standard_id"];
    [parameters setValue:@"b752b39e-9606-4149-ae0c-e3cf5aeed7" forKey:@"player_id"];
    [parameters setValue:@"student" forKey:@"UserType"];
    [parameters setValue:@"" forKey:@"branch_id"];
    [parameters setValue:@"" forKey:@"StudID"];
    [parameters setValue:@"" forKey:@"imei_number"];
    [parameters setValue:@"" forKey:@"LocationDetails"];
    
    NSLog(@"Parameters = %@",parameters);
    // add params (all params are strings)
    for (NSString *param in parameters) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", [parameters objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    NSString *FileParamConstant = @"image";
    NSData *imageData = UIImageJPEGRepresentation([_dictdata valueForKey:@"studimg"], 0);
    
    //Assuming data is not nil we add this to the multipart form
    if (imageData)
    {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"image.jpg\"\r\n", FileParamConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type:image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:imageData];
        [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    //Close off the request with the boundary
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    // setting the body of the post to the request
    [request setHTTPBody:body];
    str = [NSString stringWithFormat:@"%@direct_register",BaseURLAPI];  //,userid];
    // set URL
    [request setURL:[NSURL URLWithString:str]];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse* response, NSData* data, NSError *error) {
                               NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
                               if ([httpResponse statusCode] == 200) {
                                   NSLog(@"success");
                                   NSMutableDictionary* json = [NSJSONSerialization JSONObjectWithData:data
                                                                                               options:kNilOptions
                                                                                                 error:&error];
                                //   [APP_DELEGATE hideLoadingView];
                                   
                                   NSLog(@"%@",json);
                                   NSLog(@"%@",[json valueForKey:@"data"]);
                                   NSDictionary * dicSignIn = [json valueForKey:@"data"];
                                   NSString * status =[NSString stringWithFormat:@"%@",[json valueForKey:@"status"]];
//                                   if ([status isEqualToString:@"200"]) {
//
//                                       Detailview *s =[self.storyboard instantiateViewControllerWithIdentifier:@"Detailview"];
//                                       s.jobid = _jobid;
//                                       [self.navigationController pushViewController:s animated:YES];
//                                       NSString * message = [NSString stringWithFormat:@"%@",[json valueForKey:@"message"]];
//                                       showAlert(AlertTitle, message);
//                                   }else{
//                                       NSString * message = [NSString stringWithFormat:@"%@",[json valueForKey:@"message"]];
//                                       showAlert(AlertTitle, message);
//                                       [APP_DELEGATE hideLoadingView];
//                                   }
                                   
                            //       [APP_DELEGATE hideLoadingView];
                               }
                          //     [APP_DELEGATE hideLoadingView];
                           }];
    
}*/

-(void)CallConfirm
{
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    [AddPost setValue:[_dictdata valueForKey:@"FirstName"] forKey:@"first_name"];
    [AddPost setValue:[_dictdata valueForKey:@"LastName"] forKey:@"last_name"];
    [AddPost setValue:[_dictdata valueForKey:@"EmailID"] forKey:@"email"];
    [AddPost setValue:[_dictdata valueForKey:@"RegMobNo"] forKey:@"register_mobile"];
    [AddPost setValue:[_dictdata valueForKey:@"BoardID"] forKey:@"board_id"];
    [AddPost setValue:[_dictdata valueForKey:@"MediumID"] forKey:@"medium_id"];
    [AddPost setValue:[_dictdata valueForKey:@"StandardID"] forKey:@"standard_id"];
    [AddPost setValue:@"b752b39e-9606-4149-ae0c-e3cf5aeed7" forKey:@"player_id"];
    [AddPost setValue:@"student" forKey:@"UserType"];
    [AddPost setValue:@"" forKey:@"branch_id"];
    [AddPost setValue:@"" forKey:@"StudID"];
    [AddPost setValue:@"" forKey:@"imei_number"];
    [AddPost setValue:@"" forKey:@"LocationDetails"];
    
  
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html", @"image/jpg",  @"image/jpeg", @"image/gif", @"image/png", @"application/pdf",@"application/json", nil];

    [manager POST:[NSString stringWithFormat:@"%@direct_register",BaseURLAPI] parameters:[AddPost copy] constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        //add img data one by one
        NSData *imageData = UIImageJPEGRepresentation([_dictdata valueForKey:@"studimg"],0.0);
        //NSData *imageData = UIImagePNGRepresentation([_dictdata valueForKey:@"studimg"]);
        [formData appendPartWithFileData:imageData name:[NSString stringWithFormat:@"image"] fileName:[NSString stringWithFormat:@"image.jpg"] mimeType:@"image/jpeg"];
        
    } success:^(AFHTTPRequestOperation *operation, id dicsResponse) {
        NSLog(@"Success: %@", dicsResponse);
        
        
        DownloadVC * next = [self.storyboard instantiateViewControllerWithIdentifier:@"DownloadVC"];
        next.dictdata=[dicsResponse valueForKey:@"data"];
        [self.navigationController pushViewController:next animated:NO];
        
        [APP_DELEGATE hideLoadingView];
    }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              NSLog(@"Error: %@", error);
              [APP_DELEGATE hideLoadingView];
          }];
    
}

-(void)CallConfirmWithKey
{
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    [AddPost setValue:[_dictdata valueForKey:@"FirstName"] forKey:@"first_name"];
    [AddPost setValue:[_dictdata valueForKey:@"LastName"] forKey:@"last_name"];
    [AddPost setValue:[_dictdata valueForKey:@"EmailID"] forKey:@"email"];
    [AddPost setValue:[_dictdata valueForKey:@"RegMobNo"] forKey:@"register_mobile"];
    [AddPost setValue:[_dictdata valueForKey:@"BoardID"] forKey:@"board_id"];
    [AddPost setValue:[_dictdata valueForKey:@"MediumID"] forKey:@"medium_id"];
    [AddPost setValue:[_dictdata valueForKey:@"StandardID"] forKey:@"standard_id"];
    [AddPost setValue:[_dictdata valueForKey:@"BatchID"] forKey:@"batch_id"];
    [AddPost setValue:[_dictdata valueForKey:@"ClassID"] forKey:@"class_id"];
    [AddPost setValue:[_dictdata valueForKey:@"KeyType"] forKey:@"KeyType"];
    [AddPost setValue:[_dictdata valueForKey:@"BranchID"] forKey:@"branch_id"];
    [AddPost setValue:[_dictdata valueForKey:@"StudentKey"] forKey:@"RegKey"];
    [AddPost setValue:@"b752b39e-9606-4149-ae0c-e3cf5aeed7" forKey:@"player_id"];
    [AddPost setValue:@"student" forKey:@"UserType"];
    [AddPost setValue:@"6000000b3" forKey:@"imei_number"];
    [AddPost setValue:@"Rajkot" forKey:@"LocationDetails"];
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html", @"image/jpg",  @"image/jpeg", @"image/gif", @"image/png", @"application/pdf",@"application/json", nil];
    
    [manager POST:[NSString stringWithFormat:@"%@register",BaseURLAPI] parameters:[AddPost copy] constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        //add img data one by one
        NSData *imageData = UIImageJPEGRepresentation([_dictdata valueForKey:@"studimg"],0.0);
        //NSData *imageData = UIImagePNGRepresentation([_dictdata valueForKey:@"studimg"]);
        [formData appendPartWithFileData:imageData name:[NSString stringWithFormat:@"image"] fileName:[NSString stringWithFormat:@"image.jpg"] mimeType:@"image/jpeg"];
        
    } success:^(AFHTTPRequestOperation *operation, id dicsResponse) {
        NSLog(@"Success: %@", dicsResponse);
        
        if ([[dicsResponse valueForKey:@"status"] integerValue]==1) {
            DownloadVC * next = [self.storyboard instantiateViewControllerWithIdentifier:@"DownloadVC"];
            next.dictdata=[dicsResponse valueForKey:@"data"];
            [self.navigationController pushViewController:next animated:NO];
        }
        
        
        [APP_DELEGATE hideLoadingView];
    }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              NSLog(@"Error: %@", error);
              [APP_DELEGATE hideLoadingView];
          }];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (IBAction)btn_BACK:(id)sender {
    [self.navigationController popViewControllerAnimated:NO];
}

- (IBAction)btn_CONFIRM_A:(id)sender {
   
    
    if ([[_dictdata valueForKey:@"StudentKey"] integerValue]==0) {
        
        if ([AppDelegate isInternetConnected]) {
            [self CallConfirm];
        }
        else
        {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Alert" message:@"Internet is not available." preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                
                // Enter code here
            }];
            [alert addAction:defaultAction];
            
            // Present action where needed
            [self presentViewController:alert animated:YES completion:nil];
        }
        
    }
    else
    {
        if ([AppDelegate isInternetConnected]) {
             [self CallConfirmWithKey];
        }
        else
        {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Alert" message:@"Internet is not available." preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                
                // Enter code here
            }];
            [alert addAction:defaultAction];
            
            // Present action where needed
            [self presentViewController:alert animated:YES completion:nil];
        }
       
    }
    
}
@end
