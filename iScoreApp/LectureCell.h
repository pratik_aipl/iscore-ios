//
//  LectureCell.h
//  iScoreApp
//
//  Created by My Mac on 10/04/20.
//  Copyright © 2020 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LectureCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblteachername;
@property (strong, nonatomic) IBOutlet UILabel *lblboardname;
@property (strong, nonatomic) IBOutlet UILabel *lblstandardname;
@property (strong, nonatomic) IBOutlet UILabel *lblmediumname;
@property (strong, nonatomic) IBOutlet UILabel *lblsubjectname;
@property (strong, nonatomic) IBOutlet UILabel *lbltitlename;
@property (strong, nonatomic) IBOutlet UILabel *lblcreated;
@property (strong, nonatomic) IBOutlet UILabel *lblstarttime;
@property (strong, nonatomic) IBOutlet UILabel *lblduration;
@property (retain, nonatomic) IBOutlet UIButton *btnplay;
@property (strong, nonatomic) IBOutlet UIView *vwmain;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *btnwidth;

@end

NS_ASSUME_NONNULL_END
