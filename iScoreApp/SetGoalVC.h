//
//  SetGoalVC.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 7/31/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SetGoalVC : UIViewController



//Outlets
@property (weak, nonatomic) IBOutlet UITextField *txt_ctgt;
@property (weak, nonatomic) IBOutlet UITextField *txt_pmarks;
@property (weak, nonatomic) IBOutlet UIButton *btn_submit;




//Action
- (IBAction)btn_SUBMIT_A:(id)sender;


@end
