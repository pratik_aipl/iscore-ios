//
//  OTPVC.m
//  iScoreApp
//
//  Created by ADMIN-Khushal on 2/26/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "OTPVC.h"
#import "SelectBoardVC.h"
#import <AFNetworking/AFNetworking.h>
#import "ApplicationConst.h"
#import "ConfirmRegisVC.h"
#import "DownloadVC.h"
#import "AppDelegate.h"
#import "Constant.h"
#import "HomeVC.h"

@interface OTPVC ()

@end

@implementation OTPVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden=YES;
    // Do any additional setup after loading the view.
    [self setLblTime];
    seconds = 120;
    
    //BTN
    _btn_timer.layer.cornerRadius=4;
    _btn_cancel.layer.cornerRadius=4;
    _btn_confirm.layer.cornerRadius=4;
    
    _txt_otp4.delegate=self;
    _txt_otp3.delegate=self;
    _txt_otp2.delegate=self;
    _txt_otp1.delegate=self;
    
    NSLog(@"DictData --- %@",_dictdata);
    
  /*  if ([[_dictdata valueForKey:@"RegMobNo"]isEqualToString:@"8000092530"]) {
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@""
                                     message:@"This Number is use only itune testing pupose so not need to OPT."
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
                                        //Handle your yes please button action here
                                        if ([[_dictdata valueForKey:@"BoardName"]isEqualToString:@""]) {
                                            SelectBoardVC * next = [self.storyboard instantiateViewControllerWithIdentifier:@"SelectBoardVC"];
                                            next.dictdata=_dictdata;
                                            [self.navigationController pushViewController:next animated:YES];
                                        }
                                        else if (![[_dictdata valueForKey:@"BoardName"]isEqualToString:@""]&&[[_dictdata valueForKey:@"FlowFlag"]isEqualToString:@"LOGIN"]) {
                                            DownloadVC * next = [self.storyboard instantiateViewControllerWithIdentifier:@"DownloadVC"];
                                            next.dictdata=_dictdata;
                                            [self.navigationController pushViewController:next animated:YES];
                                        }
                                        else
                                        {
                                            
                                            ConfirmRegisVC * next = [self.storyboard instantiateViewControllerWithIdentifier:@"ConfirmRegisVC"];
                                            next.dictdata=_dictdata;
                                            [self.navigationController pushViewController:next animated:YES];
                                        }
                                    }];
        
       
        
        [alert addAction:yesButton];
        [self presentViewController:alert animated:YES completion:nil];
    }
    */
}

-(void)CallResendOTP
{
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    [AddPost setValue:[_dictdata valueForKey:@"RegMobNo"] forKey:@"mobile_number"];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    [manager GET:[NSString stringWithFormat:@"%@resend_otp",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject){
        NSLog(@"JSON: %@", responseObject);
        [_dictdata setValue:[responseObject valueForKey:@"otp"] forKey:@"MOTP"];
        NSLog(@"Dictdata-- %@",_dictdata);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
    }];
    
}

-(void)CallOTP
{
    //Post Method Request
    [APP_DELEGATE showLoadingView:@""];
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    [AddPost setValue:[_dictdata valueForKey:@"RegMobNo"] forKey:@"RegMobNo"];
     [AddPost setValue:strotp forKey:@"OTP"];
    //https://staff.parshvaa.com/web_services/version_58/web_services/verify_otp_new/
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    [manager POST:[NSString stringWithFormat:@"%@verify_otp_new",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject){
        NSLog(@"JSON: %@", responseObject);
//        [_dictdata setValue:[responseObject valueForKey:@"otp"] forKey:@"MOTP"];
//        NSLog(@"Dictdata-- %@",_dictdata);
        
        if ([[responseObject valueForKey:@"status"] intValue]==1) {
            
           NSLog(@" OTP MATCH");
                   if ([[_dictdata valueForKey:@"BoardName"]isEqualToString:@""]) {
                       SelectBoardVC * next = [self.storyboard instantiateViewControllerWithIdentifier:@"SelectBoardVC"];
                       next.dictdata=_dictdata;
                       [self.navigationController pushViewController:next animated:YES];
                   }
                   else if (![[_dictdata valueForKey:@"BoardName"]isEqualToString:@""]&&[[_dictdata valueForKey:@"FlowFlag"]isEqualToString:@"LOGIN"]) {
           //            DownloadVC * next = [self.storyboard instantiateViewControllerWithIdentifier:@"DownloadVC"];
           //            next.dictdata=_dictdata;
           //            [self.navigationController pushViewController:next animated:YES];
                     
                       [self DataParsing];
                       
                   }
                   else
                   {
                       ConfirmRegisVC * next = [self.storyboard instantiateViewControllerWithIdentifier:@"ConfirmRegisVC"];
                       next.dictdata=_dictdata;
                       [self.navigationController pushViewController:next animated:YES];
                   }
            
        }
        else
        {
                       UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                            message:[responseObject valueForKey:@"message"]
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];

        }
[APP_DELEGATE hideLoadingView];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
       [ APP_DELEGATE hideLoadingView];
    }];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)setLblTime{
    seconds --;
    [_btn_timer setTitle:[NSString stringWithFormat:@"%d:%ld",(seconds / 60) % 60,(long)seconds % 60] forState:UIControlStateNormal];
    //lblname.text = [NSString stringWithFormat:@"%dm:%lds",(seconds / 60) % 60,(long)seconds % 60];
   
    if (seconds != 0) {
        [self performSelector:@selector(setLblTime) withObject:nil    afterDelay:1];
    }
    if (seconds == 0){
        _btn_timer.userInteractionEnabled=YES;
       [_btn_timer setTitle:@"RESEND" forState:UIControlStateNormal];
        //[self performSelector:@selector(call2:) withObject:nil afterDelay:0.01];
    }
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    // This allows numeric text only, but also backspace for deletes
    if (string.length > 0 && ![[NSScanner scannerWithString:string] scanInt:NULL])
        return NO;
    
    NSUInteger oldLength = [textField.text length];
    NSUInteger replacementLength = [string length];
    NSUInteger rangeLength = range.length;
    
    NSUInteger newLength = oldLength - rangeLength + replacementLength;
    
    // This 'tabs' to next field when entering digits
    if (newLength == 1) {
        if (textField == _txt_otp1)
        {
            [self performSelector:@selector(setNextResponder:) withObject:_txt_otp2 afterDelay:0];
        }
        else if (textField ==_txt_otp2)
        {
            [self performSelector:@selector(setNextResponder:) withObject:_txt_otp3 afterDelay:0];
        }
        else if (textField == _txt_otp3)
        {
            [self performSelector:@selector(setNextResponder:) withObject:_txt_otp4 afterDelay:0];
        }
    }
    //this goes to previous field as you backspace through them, so you don't have to tap into them individually
    else if (oldLength > 0 && newLength == 0) {
        if (textField ==_txt_otp4)
        {
            [self performSelector:@selector(setNextResponder:) withObject:_txt_otp3 afterDelay:0];
        }
        else if (textField == _txt_otp3)
        {
            [self performSelector:@selector(setNextResponder:) withObject:_txt_otp2 afterDelay:0];
        }
        else if (textField == _txt_otp2)
        {
            [self performSelector:@selector(setNextResponder:) withObject:_txt_otp1 afterDelay:0];
        }
    }
    return newLength <= 1;
}


- (void)setNextResponder:(UITextField *)nextResponder
{
    [nextResponder becomeFirstResponder];
}
- (BOOL)keyboardInputShouldDelete:(UITextField *)textField {
    BOOL shouldDelete = YES;
    
    if ([textField.text length] == 0 && [textField.text isEqualToString:@""]) {
        long tagValue = textField.tag - 1;
        UITextField *txtField = (UITextField*) [self.view viewWithTag:tagValue];
        
        [txtField becomeFirstResponder];
    }
    return shouldDelete;
}
/*
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    // This allows numeric text only, but also backspace for deletes
    if (string.length > 0 && ![[NSScanner scannerWithString:string] scanInt:NULL])
        return NO;
    
    if ([textField.text length] == 0) {
        [self performSelector:@selector(changeTextFieldFocusToNextTextField:) withObject:textField afterDelay:0];
    }
    
    
    return YES;
}*/

-(void)changeTextFieldFocusToNextTextField:(UITextField*)textField{
    long tagValue = textField.tag + 1;
    UITextField *txtField = (UITextField*) [self.view viewWithTag:tagValue];
    [txtField becomeFirstResponder];
}


-(void)VerifyOTP
{
    strotp=[NSString stringWithFormat:@"%@%@%@%@",_txt_otp1.text,_txt_otp2.text,_txt_otp3.text,_txt_otp4.text];
    NSLog(@"OTP %@",strotp);
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}

- (IBAction)btn_CANCEL:(id)sender {
    [self.view endEditing:YES];
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btn_RESEND:(id)sender {
    
    if([AppDelegate isInternetConnected]){
        [self CallResendOTP];
        seconds = 120;
        [self setLblTime];
    }
    else
    {
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@"Alert"
                                     message:@"Intenet is not Connected."
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        //Add Buttons
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
                                        //Handle your yes please button action here
                                    }];
        
        [alert addAction:yesButton];
        [self presentViewController:alert animated:YES completion:nil];
    }
    
}

- (IBAction)btn_BACK:(id)sender {
   [self.view endEditing:YES];
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btn_CONFIRM_A:(id)sender {
    [self VerifyOTP];
    [self.view endEditing:YES];

    [self CallOTP];
//    if([[NSString stringWithFormat:@"%@",[_dictdata valueForKey:@"MOTP"]] isEqualToString:[NSString stringWithFormat:@"%@",strotp]])
//    {
//
//    }
//    else
//    {
//        NSLog(@"OTP NOT MATCH");
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Please enter valid OTP." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
//        [alert show];
//    }

}

-(void)DataParsing
{
    
    if (_dictdata.count>0) {
        ParseData=[[NSMutableDictionary alloc]init];
        [ParseData setDictionary:[[_dictdata valueForKey:@"KeyData"] objectAtIndex:0]];
        NSLog(@"temp dict %@",ParseData);
        [ParseData setValue:[_dictdata valueForKey:@"EmailID"] forKey:@"EmailID"];
        [ParseData setValue:[_dictdata valueForKey:@"FirstName"] forKey:@"FirstName"];
        [ParseData setValue:[_dictdata valueForKey:@"FlowFlag"] forKey:@"FlowFlag"];
        [ParseData setValue:[_dictdata valueForKey:@"IMEINo"] forKey:@"IMEINo"];
        [ParseData setValue:[_dictdata valueForKey:@"LastName"] forKey:@"LastName"];
        [ParseData setValue:[_dictdata valueForKey:@"MOTP"] forKey:@"MOTP"];
        [ParseData setValue:[_dictdata valueForKey:@"ProfileImage"] forKey:@"ProfileImage"];
        [ParseData setValue:[_dictdata valueForKey:@"RegMobNo"] forKey:@"RegMobNo"];
        [ParseData setValue:[_dictdata valueForKey:@"SchoolName"] forKey:@"SchoolName"];
        [ParseData setValue:[_dictdata valueForKey:@"StudentCode"] forKey:@"StudentCode"];
        [ParseData setValue:[_dictdata valueForKey:@"StudentID"] forKey:@"StudentID"];
        
        NSLog(@"Parse dtata %@",ParseData);
        
        [[NSUserDefaults standardUserDefaults] setObject:[MyModel cleanJsonToObject:ParseData] forKey:@"ParseData"];
        [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:@"DataDownloadStatus"];
//        SWRevealViewController * next = [self.storyboard instantiateViewControllerWithIdentifier:@"SWRevealViewController"];
//                         [self.navigationController pushViewController:next animated:NO];
        
        SWRevealViewController *loginController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"SWRevealViewController"]; //or the homeController
        //    UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:loginController];
            APP_DELEGATE.window.rootViewController = loginController;
        
    }
    
}
@end
