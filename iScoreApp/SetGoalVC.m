//
//  SetGoalVC.m
//  iScoreApp
//
//  Created by ADMIN-Khushal on 7/31/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "SetGoalVC.h"
#import "UIColor+CL.h"
#import "HomeVC.h"



@interface SetGoalVC ()

@end

@implementation SetGoalVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _txt_ctgt.userInteractionEnabled=YES;
    [_txt_ctgt becomeFirstResponder];
    _txt_ctgt.layer.borderColor=[UIColor colorWithHex:0x888888].CGColor;
    _txt_ctgt.layer.borderWidth=1;
    _txt_ctgt.layer.cornerRadius=4;
    
    
    _txt_ctgt.textColor=[UIColor colorWithHex:0x454545];
    [_txt_ctgt addTarget:self action:@selector(textFieldDidChange1:) forControlEvents:UIControlEventEditingChanged];
    
    _txt_pmarks.userInteractionEnabled=YES;
    _txt_pmarks.textColor=[UIColor colorWithHex:0x454545];
    _txt_pmarks.layer.borderColor=[UIColor colorWithHex:0x888888].CGColor;
    _txt_pmarks.layer.borderWidth=1;
    _txt_pmarks.layer.cornerRadius=4;
    [_txt_pmarks addTarget:self action:@selector(textFieldDidChange2:) forControlEvents:UIControlEventEditingChanged];
    
    
    
    
    
}


-(void)textFieldDidChange1 :(UITextField *)theTextField
{
   
    
    if ([theTextField.text integerValue]<=100) {
        NSLog(@"ok");
        _txt_ctgt.text =[NSString stringWithFormat:@"%ld",[theTextField.text integerValue]];
        
        
    }
    else
    {
        NSString *message = [NSString stringWithFormat:@"Should not more than 100"];
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil
                                                                       message:message
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:nil];
        
        int duration = 2; // duration in seconds
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [alert dismissViewControllerAnimated:YES completion:nil];
        });
        
        NSLog(@"NOT OK");
        NSString *lastString=[_txt_ctgt.text substringToIndex:[_txt_ctgt.text length] - 1];;
        [_txt_ctgt setText:lastString];
    }
    
    
}

-(void)textFieldDidChange2 :(UITextField *)theTextField
{
    
    
    if ([theTextField.text integerValue]<=100) {
        NSLog(@"ok");
        _txt_pmarks.text =[NSString stringWithFormat:@"%ld",[theTextField.text integerValue]];
        
        
    }
    else
    {
        NSString *message = [NSString stringWithFormat:@"Should not more than 100"];
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil
                                                                       message:message
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:nil];
        
        int duration = 2; // duration in seconds
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [alert dismissViewControllerAnimated:YES completion:nil];
        });
        
        NSLog(@"NOT OK");
        NSString *lastString=[_txt_pmarks.text substringToIndex:[_txt_pmarks.text length] - 1];;
        [_txt_pmarks setText:lastString];
    }
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}

- (IBAction)btn_SUBMIT_A:(id)sender {
    
    [[NSUserDefaults standardUserDefaults] setValue:_txt_ctgt.text forKey:@"CYear"];
    [[NSUserDefaults standardUserDefaults] setValue:_txt_pmarks.text forKey:@"PYear"];
    
    
    if ([_txt_ctgt.text integerValue]>0) {
        
        if ([_txt_pmarks.text integerValue]>0) {
            HomeVC *next = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeVC"];
            [self.navigationController pushViewController:next animated:NO];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                            message:@"Please Enter Previous year marks."
                                                           delegate:nil
                                                  cancelButtonTitle:nil
                                                  otherButtonTitles:@"OK",nil];
            [alert show];
        }
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                        message:@"Please Enter Current Year."
                                                       delegate:nil
                                              cancelButtonTitle:nil
                                              otherButtonTitles:@"OK",nil];
        [alert show];
    }
    
    
    
    
    
    
    
}
@end
