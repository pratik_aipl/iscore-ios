//
//  AppDelegate.m
//  iScoreApp
//
//  Created by ADMIN-Khushal on 2/23/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "AppDelegate.h"
#import "MyModel.h"
#import "SharedData.h"
#import "Constant.h"
#import "DGActivityIndicatorView.h"
#import "UIColor+CL.h"
#import "HomeVC.h"
#import "DownloadVC.h"
#import "ViewController.h"
#import "PreViewController.h"
#import "Reachability.h"
#import "SWRevealViewController.h"
#import "MenuListViewVC.h"


@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
   [application setStatusBarHidden:NO];
    
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    [UIApplication sharedApplication].idleTimerDisabled = YES;
    
  
    
  
    
    if (@available(iOS 13.0, *)) {
        UIView *statusBar = [[UIView alloc]initWithFrame:[UIApplication sharedApplication].keyWindow.windowScene.statusBarManager.statusBarFrame] ;
        //    statusBar.backgroundColor = [UIColor whiteColor];
        //    [[UIApplication sharedApplication].keyWindow addSubview:statusBar]
        
        if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
            
            statusBar.backgroundColor = [UIColor blackColor];//set whatever color you like
            
        }
    } else {
        // Fallback on earlier versions
          UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
        
        if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
                   
                   statusBar.backgroundColor = [UIColor blackColor];//set whatever color you like
                   
               }
    }
    
    [self checkDateIsExpired];
    
    // Replace '11111111-2222-3333-4444-0123456789ab' with your OneSignal App ID.
    [OneSignal initWithLaunchOptions:launchOptions
                               appId:@"11111111-2222-3333-4444-0123456789ab"
            handleNotificationAction:nil
                            settings:@{kOSSettingsKeyAutoPrompt: @false}];
    OneSignal.inFocusDisplayType = OSNotificationDisplayTypeNotification;
    
    // Recommend moving the below line to prompt for push after informing the user about
    //   how your app will use them.
    [OneSignal promptForPushNotificationsWithUserResponse:^(BOOL accepted) {
        NSLog(@"User accepted notifications: %d", accepted);
    }];
    
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"DataDownloadStatus"] intValue]== 1)
    {
//        DownloadVC *loginController1 = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"DownloadVC"]; //or the homeController
//        UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:loginController1];
//        self.window.rootViewController = navController;
        SWRevealViewController *loginController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"SWRevealViewController"]; //or the homeController
        UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:loginController];
        self.window.rootViewController = navController;
        
    }
    else if([[[NSUserDefaults standardUserDefaults] valueForKey:@"DataDownloadStatus"] intValue] == 2)
    {
        SWRevealViewController *loginController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"SWRevealViewController"]; //or the homeController
         UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:loginController];
         self.window.rootViewController = navController;
        
        /*HomeVC *loginController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"HomeVC"]; //or the homeController
        UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:loginController];
        self.window.rootViewController = navController;*/
      
        
    }
    else
    {
        PreViewController *loginController2 = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"PreViewController"]; //or the homeController
        UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:loginController2];
        self.window.rootViewController = navController;
    }
    
    
///////////RECHABILITY=========
    
    
    
    
    return YES;
}

-(void)AlertForMSG:(NSString *)msg
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:[NSString stringWithFormat:@"%@",msg] delegate:nil cancelButtonTitle:@"" otherButtonTitles:@"OK", nil];
    [alert show];
}

-(void)showLoadingView:(NSString *)strMSG
{
    [self hideLoadingView];
    
    UIView *view = [[UIView alloc] initWithFrame:self.window.bounds];
    [view setBackgroundColor:[UIColor blackColor]];
    view.tag = 4444;
    [view setAlpha:0.6];
    
    
    DGActivityIndicatorView *act = [[DGActivityIndicatorView alloc] initWithType:(DGActivityIndicatorAnimationType)DGActivityIndicatorAnimationTypeBallScaleRippleMultiple tintColor:[UIColor colorWithHex:0x155683]];
    [act startAnimating];
    [view addSubview:act];
    act.center = view.center;
    [self.window addSubview:view];
    
}
-(void)hideLoadingView
{
    for(UIView *view in self.window.subviews)
    {
        if(view.tag==4444)
        {
            [view removeFromSuperview];
        }
    }
}
-(NSString*)getCurrentDate
{
    NSDate * now = [NSDate date];
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *cDate = [dateFormatter stringFromDate:now];
    
    return cDate;
}

-(NSString*)getDateInFormat :(NSString*)strdt
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date  = [dateFormatter dateFromString:strdt];
    // Convert to new Date Format
    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
    return [dateFormatter stringFromDate:date];
}


-(void)checkDateIsExpired{
    
    NSString *dateString = @"30-10-2020";
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    NSDate *dateFromString = [[NSDate alloc] init];
    dateFromString = [dateFormatter dateFromString:dateString];
    
    NSTimeInterval expireDateMilisecond = [dateFromString timeIntervalSince1970]*1000;
    
    NSTimeInterval currentDateMilisecond = [[NSDate date] timeIntervalSince1970]*1000;
    
    NSLog(@"expireDateMilisecond : - %f",expireDateMilisecond);
    NSLog(@"currentDateMilisecond : - %f",currentDateMilisecond);
    
    
    if (currentDateMilisecond > expireDateMilisecond) {
        
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Expired"
                                                        message:@"Your iScore Product has Expired, Kindly contact Parshwa Publications"
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        
    }else{
        
        [self copyDatabase];
        
        self.sharedData = [SharedData sharedInstance];
        
        
        [MyModel MyModelInit];
        
        
     
        
        
    }
}

- (void)alertView:(UIAlertView *)alertView
clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == [alertView cancelButtonIndex]){
        exit(0);
    }
}

-(void)copyDatabase{
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *txtPath = [documentsDirectory stringByAppendingPathComponent:@"iScoreApp.sqlite"];
    NSLog(@"path:%@",txtPath);
    if ([fileManager fileExistsAtPath:txtPath] == NO)
    {
        NSLog(@"copy");
        NSString *resourcePath = [[NSBundle mainBundle] pathForResource:@"iScoreApp.sqlite" ofType:nil];
        [fileManager copyItemAtPath:resourcePath toPath:txtPath error:&error];
    }
    else
    {
        NSLog(@"available");
    }
    
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    
    NSLog(@"applicationWillResignActive");
    
    [self performSelector:@selector(someMethod) withObject:nil    afterDelay:1];
    
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    //[NSTimer scheduledTimerWithTimeInterval:0.1f target:self selector:@selector(handleTimer) userInfo:nil repeats:YES];
    
    [self handleTimer];
    
}

-(void)handleTimer
{
    NSLog(@"Log 1");
    
     //[NSTimer scheduledTimerWithTimeInterval:0.1f target:self selector:@selector(handleTimer1) userInfo:nil repeats:YES];
   //[self performSelector:@selector(handleTimer) withObject:nil    afterDelay:1];
    
    
    
}
-(void)someMethod
{
    NSLog(@"Log 2");
    //[self performSelector:@selector(someMethod) withObject:nil    afterDelay:1];
    
    
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    NSLog(@"applicationWillEnterForeground");
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    NSLog(@"applicationDidBecomeActive");
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    NSLog(@"applicationWillTerminate");
    
    [self performSelector:@selector(handleTimer) withObject:nil    afterDelay:1];
    
}

+ (BOOL) isInternetConnected {
if ([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
{
    return false;
}
else
{
     return true;
}
}

@end
