//
//  SPTempCell2.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 7/25/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPTempCell2 : UITableViewCell


@property (weak, nonatomic) IBOutlet UIView *vw_details;

@property (weak, nonatomic) IBOutlet UILabel *lbl_ques;
@property (weak, nonatomic) IBOutlet UILabel *lbl_totques;
@property (weak, nonatomic) IBOutlet UILabel *lbl_subnm;
@property (weak, nonatomic) IBOutlet UIProgressView *prog_accu;
@property (weak, nonatomic) IBOutlet UILabel *lbl_date;
@property (weak, nonatomic) IBOutlet UILabel *lbl_accuracy;

@end
