//
//  SearchPaperMainVC.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 7/22/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchPaperMainVC : UIViewController


//Outles

@property (weak, nonatomic) IBOutlet UIView *vw_main;

@property (weak, nonatomic) IBOutlet UIButton *btn_mcqt;
@property (weak, nonatomic) IBOutlet UIButton *btn_ppapert;

@property (weak, nonatomic) IBOutlet UILabel *lb_mcq;
@property (weak, nonatomic) IBOutlet UILabel *lb_pracpap;


//Actions
- (IBAction)btn_BACK:(id)sender;
- (IBAction)btn_PracticePaper:(id)sender;

- (IBAction)btn_MCQTest:(id)sender;

@end
