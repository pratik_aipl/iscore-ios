//
//  SPTempCell2.m
//  iScoreApp
//
//  Created by ADMIN-Khushal on 7/25/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "SPTempCell2.h"

@implementation SPTempCell2

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    _vw_details.layer.shadowRadius  = 3.5f;
    _vw_details.layer.shadowColor   = [UIColor colorWithRed:176.f/255.f green:199.f/255.f blue:226.f/255.f alpha:1.f].CGColor;
    _vw_details.layer.shadowOffset  = CGSizeMake(0.0f, 0.0f);
    _vw_details.layer.shadowOpacity = 2.1f;
    _vw_details.layer.masksToBounds = NO;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
