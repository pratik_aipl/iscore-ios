//
//  SPTempVC2.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 7/25/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyDBQueries.h"
#import "MyDBResultParser.h"
#import "MyModel.h"

@interface SPTempVC2 : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
    
    MyDBQueries *myDbQueris;
    MyDBResultParser *myDBParser;
    NSString *filePath;
    
    NSMutableArray *arrSubjects,*arrchk,*arrPtype;
    NSMutableArray *arrList;
     NSMutableArray *arrSubIds,*arrLevelIds;
    
}






//Outlets
@property (weak, nonatomic) IBOutlet UIView *vw_left;
@property (weak, nonatomic) IBOutlet UIView *vw_right;
@property (weak, nonatomic) IBOutlet UIButton *btn_background;

@property (weak, nonatomic) IBOutlet UITableView *tbl_left;
@property (weak, nonatomic) IBOutlet UITableView *tbl_right;
@property (weak, nonatomic) IBOutlet UITableView *tbl_list;
@property (weak, nonatomic) IBOutlet UIView *vw_tbl;




//Action
- (IBAction)btn_LEFT:(id)sender;
- (IBAction)btn_RIGHT:(id)sender;
- (IBAction)btn_DISMISSTAB:(id)sender;

- (IBAction)btn_FILTERL:(id)sender;
- (IBAction)btn_FILTERR:(id)sender;


@end
