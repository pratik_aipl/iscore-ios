//
//  SPTempVC.m
//  iScoreApp
//
//  Created by ADMIN-Khushal on 7/24/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "SPTempVC.h"
#import "CollectionView.h"
#import "TableviewCell.h"
#import "QuePaperPDFVC.h"
#import "QueAnsPaperPDFVC.h"

//#import "PaperTypeModel.h"
#import "SetPaperModel.h"
#import "ApplicationConst.h"
#import "SetPaperQuestionModel.h"
#import "UIColor+CL.h"
#import "AllSubjectModel.h"
#import "MCQFilterModel.h"
#import "SPTempCell.h"
#import "MCQRedirectModel.h"

#import "TestReportVC.h"




@interface SPTempVC ()

@end

@implementation SPTempVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    
    
    
    
    arrSubjects=[[NSMutableArray alloc]init];
    arrchk=[[NSMutableArray alloc]init];
    arrlevel=[[NSMutableArray alloc]init];
    arrList=[[NSMutableArray alloc]init];
    
    
    [self CallgetSubject];
    [self CallgetTestList];

    for (int i=0; i<arrSubjects.count; i++) {
        [arrchk addObject:@"0"];
    }
    
    for (int i=0; i<3; i++) {
        NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
        [dict setValue:@"Level" forKey:@"level"];
        [dict setValue:@"0" forKey:@"isCheck"];
        [arrlevel addObject:dict];
    }
    
    //SHADOW
    _vw_tbl.layer.shadowRadius  = 3.5f;
    _vw_tbl.layer.shadowColor   = [UIColor colorWithRed:176.f/255.f green:199.f/255.f blue:226.f/255.f alpha:1.f].CGColor;
    _vw_tbl.layer.shadowOffset  = CGSizeMake(0.0f, 0.0f);
    _vw_tbl.layer.shadowOpacity = 2.1f;
    _vw_tbl.layer.masksToBounds = NO;
    
    
}

-(void)CallgetSubject
{
    myDbQueris =[[MyDBQueries alloc]init];
    myDBParser=[[MyDBResultParser alloc]init];
    filePath = [MyModel getFilePath];
    
    FMResultSet *rs = [MyModel selectQuery:[myDbQueris getAllsubjects]];
    while ([rs next]) {
        AllSubjectModel *SubModel = [[AllSubjectModel alloc] init];
        [arrSubjects  addObject:[myDBParser parseDBResult:rs :SubModel]];
    }
    
   
}

-(void)CallgetTestList
{
    myDbQueris =[[MyDBQueries alloc]init];
    myDBParser=[[MyDBResultParser alloc]init];
    filePath = [MyModel getFilePath];
    
    FMResultSet *rs = [MyModel selectQuery:[myDbQueris getAllTestList]];
    while ([rs next]) {
        MCQFilterModel *SubModel = [[MCQFilterModel alloc] init];
        [arrList  addObject:[myDBParser parseDBResult:rs :SubModel]];
    }
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView==_tbl_list) {
        return arrList.count;
    }
    else if (tableView==_tbl_left) {
        return arrSubjects.count;
    }
    else
    {
        return arrlevel.count;
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==_tbl_list) {
        
        MCQFilterModel *Flist=[arrList objectAtIndex:indexPath.row];
        
        SPTempCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellList"];
        if (cell == nil)
        {
            // Creating a cell here
            cell = [[SPTempCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CellList"];
        }
        
        cell.lbl_totques.text=[NSString stringWithFormat:@"%@",Flist.Total_que];
        cell.lbl_ques.text=[NSString stringWithFormat:@"%@",Flist.Total_Right];
        cell.lbl_date.text=[NSString stringWithFormat:@"%@",Flist.CreatedOn];
        cell.lbl_date.text= [cell.lbl_date.text substringToIndex:[cell.lbl_date.text length] - 8];
        cell.lbl_subnm.text=[NSString stringWithFormat:@"%@",Flist.SubjectName];
        cell.lbl_accuracy.text=[NSString stringWithFormat:@"Accuracy %@%%",Flist.accuracy1];
        
        cell.prog_accu.transform = CGAffineTransformMakeScale(1.0f, 2.0);
        [cell.prog_accu setProgress:(float)([Flist.accuracy1 floatValue]/100) animated:YES];
        
        cell.prog_accu.clipsToBounds = YES;
        cell.prog_accu.layer.cornerRadius = 1.5;
        
        
        
        
        return cell;
    }
    else if (tableView==_tbl_left)
    {
        AllSubjectModel *subject=[arrSubjects objectAtIndex:indexPath.row];
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
        if (cell == nil)
        {
            // Creating a cell here
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
            
            UILabel *theLabelIWant = [[UILabel alloc] initWithFrame:CGRectMake(10, 14, 120, 21)];
            theLabelIWant.tag = 1;
            [cell.contentView addSubview:theLabelIWant];
            
            UIImageView *dot =[[UIImageView alloc] initWithFrame:CGRectMake(150,14,21,21)];
            dot.tag=2;
            
            [cell.contentView addSubview:dot];
        }
        
        UILabel *finalLabel = (UILabel *)[[cell contentView] viewWithTag:1];
        [finalLabel setText:subject.SubjectName];
        UIImageView *dot =(UIImageView *)[[cell contentView] viewWithTag:2];
        if ([[arrchk objectAtIndex:indexPath.row] isEqualToString:@"0"]) {
            dot.image=[UIImage imageNamed:@"checkbox-unselected1"];
        }
        else
        {
            dot.image=[UIImage imageNamed:@"checkbox-selected1"];
        }
        
        return cell;
    }
    else
    {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell1"];
        if (cell == nil)
        {
            // Creating a cell here
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell1"];
            
            UILabel *theLabelIWant = [[UILabel alloc] initWithFrame:CGRectMake(10, 14, 120, 21)];
            theLabelIWant.tag = 1;
            [cell.contentView addSubview:theLabelIWant];
            
            
            UIImageView *dot =[[UIImageView alloc] initWithFrame:CGRectMake(150,14,21,21)];
            dot.tag=2;
            
            [cell.contentView addSubview:dot];
            
        }
        
        UILabel *finalLabel = (UILabel *)[[cell contentView] viewWithTag:1];
       // [finalLabel setText:[NSString stringWithFormat:@"%@ %ld",[[arrlevel objectAtIndex:indexPath.row] valueForKey:@"level"],indexPath.row+1]];
        
        
        switch (indexPath.row) {
            case 0:
            [finalLabel setText:[NSString stringWithFormat:@"Level I"]];
            break;
            
            case 1:
            [finalLabel setText:[NSString stringWithFormat:@"Level II"]];
            break;
            
            case 2:
            [finalLabel setText:[NSString stringWithFormat:@"Level III"]];
            break;
            
            default:
            break;
        }
        
        
        UIImageView *dot =(UIImageView *)[[cell contentView] viewWithTag:2];
        if ([[[arrlevel objectAtIndex:indexPath.row] valueForKey:@"isCheck"] isEqualToString:@"0"]) {
            dot.image=[UIImage imageNamed:@"checkbox-unselected1"];
        }
        else
        {
            dot.image=[UIImage imageNamed:@"checkbox-selected1"];
        }
        
        
        return cell;
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    if (tableView==_tbl_list) {
    
        MCQFilterModel *Flist=[arrList objectAtIndex:indexPath.row];
        
        [self CallGetRedirectMCQ:Flist.StudentMCQTestHDRID :Flist.SubjectName :Flist.SubjectID];
        
      /*  PDFView *vc2 = [[PDFView alloc] initWithNibName:@"PDFView" bundle:nil];
        [[self navigationController] pushViewController:vc2 animated:YES];*/
        
    }
    else if (tableView==_tbl_left) {
        
        if ([[arrchk objectAtIndex:indexPath.row] isEqualToString:@"0"]) {
            [arrchk replaceObjectAtIndex:indexPath.row withObject:@"1"];
        }
        else
        {
            [arrchk replaceObjectAtIndex:indexPath.row withObject:@"0"];
        }
        [_tbl_left reloadData];
    }
    else
    {
        if ([[[arrlevel objectAtIndex:indexPath.row] valueForKey:@"isCheck"] isEqualToString:@"0"]) {
            
            NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
            [dict setValue:@"Level" forKey:@"level"];
            [dict setValue:@"1" forKey:@"isCheck"];
            
            [arrlevel replaceObjectAtIndex:indexPath.row withObject:dict];
        }
        else
        {
            NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
            [dict setValue:@"Level" forKey:@"level"];
            [dict setValue:@"0" forKey:@"isCheck"];
            
            [arrlevel replaceObjectAtIndex:indexPath.row withObject:dict];
        }
        [_tbl_right reloadData];
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}


-(void)CallGetRedirectMCQ :(NSString *)SHDRID :(NSString *)Subnm :(NSString *)subId
{
    tempCorrect=[[NSMutableArray alloc]init];
    tempIncorrect=[[NSMutableArray alloc]init];
    tempNotapper=[[NSMutableArray alloc]init];
    tempCorrectOp=[[NSMutableArray alloc]init];
    tempIncorrectOp=[[NSMutableArray alloc]init];
    tempNotapperOp=[[NSMutableArray alloc]init];
    
    tempMCQQuestion=[[NSMutableArray alloc]init];
    tempMCQOtion=[[NSMutableArray alloc]init];
    
    
    
    
    
    NSMutableArray *arrMCQTDTL=[[NSMutableArray alloc]init];
    
    myDbQueris =[[MyDBQueries alloc]init];
    myDBParser=[[MyDBResultParser alloc]init];
    filePath = [MyModel getFilePath];
    
    FMResultSet *rs = [MyModel selectQuery:[myDbQueris getMCQTestDTL:[NSString stringWithFormat:@"%@",SHDRID]]];
    
    while ([rs next]) {
        MCQRedirectModel *SubModel = [[MCQRedirectModel alloc] init];
        [arrMCQTDTL  addObject:[myDBParser parseDBResult:rs :SubModel]];
    }
    
    NSMutableArray *arrTempIDs=[[NSMutableArray alloc]init];
    
    for (int i=0; i<arrMCQTDTL.count; i++) {
        MCQRedirectModel *queIds=[arrMCQTDTL objectAtIndex:i];
        //[arrTempIDs addObject:[NSString stringWithFormat:@"%@",queIds.QuestionID]];
        
        
        if ([queIds.IsAttempt intValue]==1) {
            
            FMResultSet *results = [MyModel selectQuery:[NSString stringWithFormat:@"select * from mcqoption where MCQQuestionID=%@ and MCQOptionID=%@",queIds.QuestionID,queIds.AnswerID]];
            NSMutableDictionary *mdict=[[NSMutableDictionary alloc]init];
            NSString *optionOn=[[NSString alloc]init];
            
            while ([results next])
            {
                [mdict setValue:[results stringForColumn:@"isCorrect"] forKey:@"isCorrect"];
                [mdict setValue:[results stringForColumn:@"OptionNo"] forKey:@"OptionNo"];
            }
            
            optionOn=[mdict valueForKey:@"OptionNo"];
            
            if ([[mdict valueForKey:@"isCorrect"] intValue]==1) {
                FMResultSet *results = [MyModel selectQuery:[NSString stringWithFormat:@"select * from mcqquestion where MCQQuestionID =%@",queIds.QuestionID]];
                
                while ([results next])
                {
                    NSMutableDictionary* mdict = [[NSMutableDictionary alloc] init];
                    
                    [mdict setValue:[results stringForColumn:@"MCQQuestionID"] forKey:@"MCQQuestionID"];
                    [mdict setValue:[results stringForColumn:@"Solution"] forKey:@"Solution"];
                    [mdict setValue:[results stringForColumn:@"Question"] forKey:@"Question"];
                    [mdict setValue:[results stringForColumn:@"ImageURL"] forKey:@"ImageURL"];
                    [mdict setValue:[results stringForColumn:@"BoardID"] forKey:@"BoardID"];
                    [mdict setValue:[results stringForColumn:@"PageNumberID"] forKey:@"PageNumberID"];
                    [mdict setValue:[results stringForColumn:@"is_demo"] forKey:@"is_demo"];
                    [mdict setValue:[results stringForColumn:@"MediumID"] forKey:@"MediumID"];
                    [mdict setValue:[results stringForColumn:@"ClassID"] forKey:@"ClassID"];
                    [mdict setValue:[results stringForColumn:@"StanderdID"] forKey:@"StanderdID"];
                    [mdict setValue:[results stringForColumn:@"SubjectID"] forKey:@"SubjectID"];
                    [mdict setValue:[results stringForColumn:@"ChapterID"] forKey:@"ChapterID"];
                    [mdict setValue:[results stringForColumn:@"QuestionTypeID"] forKey:@"QuestionTypeID"];
                    [mdict setValue:[results stringForColumn:@"QuestionLevelID"] forKey:@"QuestionLevelID"];
                    [mdict setValue:[results stringForColumn:@"Duration"] forKey:@"Duration"];
                    [mdict setValue:@"1" forKey:@"isRight"];
                    [mdict setValue:optionOn forKey:@"selectedAns"];
                    
                    [tempCorrect addObject:mdict];
                    [tempMCQQuestion addObject:mdict];
                    
                }
                
                ////Add Option
                
                FMResultSet *results1 = [MyModel selectQuery:[NSString stringWithFormat:@"select * from mcqoption where MCQQuestionID=%@",queIds.QuestionID]];
                
                NSMutableArray *arrtemp=[[NSMutableArray alloc]init];
                while ([results1 next])
                {
                    NSMutableDictionary* mdict = [[NSMutableDictionary alloc] init];
                    
                    [mdict setValue:[results1 stringForColumn:@"MCQQuestionID"] forKey:@"MCQQuestionID"];
                    [mdict setValue:[results1 stringForColumn:@"MCQOptionID"] forKey:@"MCQOptionID"];
                    [mdict setValue:[results1 stringForColumn:@"Options"] forKey:@"Options"];
                    [mdict setValue:[results1 stringForColumn:@"ImageURL"] forKey:@"ImageURL"];
                    [mdict setValue:[results1 stringForColumn:@"OptionOn"] forKey:@"OptionOn"];
                    [mdict setValue:[results1 stringForColumn:@"isCorrect"] forKey:@"isCorrect"];
                    
                    [mdict setValue:[results1 stringForColumn:@"CreatedBy"] forKey:@"CreatedBy"];
                    [mdict setValue:[results1 stringForColumn:@"CreatedOn"] forKey:@"CreatedOn"];
                    [mdict setValue:[results1 stringForColumn:@"ModifiedOn"] forKey:@"ModifiedOn"];
                    [mdict setValue:[results1 stringForColumn:@"ModifiedBy"] forKey:@"ModifiedBy"];
                   
                    [arrtemp addObject:mdict];
                }
                
                [tempCorrectOp addObject:arrtemp];
                [tempMCQOtion addObject:arrtemp];
                
                
            }
            else
            {
                FMResultSet *results = [MyModel selectQuery:[NSString stringWithFormat:@"select * from mcqquestion where MCQQuestionID =%@",queIds.QuestionID]];
                
                while ([results next])
                {
                    NSMutableDictionary* mdict = [[NSMutableDictionary alloc] init];
                    
                    [mdict setValue:[results stringForColumn:@"MCQQuestionID"] forKey:@"MCQQuestionID"];
                    [mdict setValue:[results stringForColumn:@"Solution"] forKey:@"Solution"];
                    [mdict setValue:[results stringForColumn:@"Question"] forKey:@"Question"];
                    [mdict setValue:[results stringForColumn:@"ImageURL"] forKey:@"ImageURL"];
                    [mdict setValue:[results stringForColumn:@"BoardID"] forKey:@"BoardID"];
                    [mdict setValue:[results stringForColumn:@"PageNumberID"] forKey:@"PageNumberID"];
                    [mdict setValue:[results stringForColumn:@"is_demo"] forKey:@"is_demo"];
                    [mdict setValue:[results stringForColumn:@"MediumID"] forKey:@"MediumID"];
                    [mdict setValue:[results stringForColumn:@"ClassID"] forKey:@"ClassID"];
                    [mdict setValue:[results stringForColumn:@"StanderdID"] forKey:@"StanderdID"];
                    [mdict setValue:[results stringForColumn:@"SubjectID"] forKey:@"SubjectID"];
                    [mdict setValue:[results stringForColumn:@"ChapterID"] forKey:@"ChapterID"];
                    [mdict setValue:[results stringForColumn:@"QuestionTypeID"] forKey:@"QuestionTypeID"];
                    [mdict setValue:[results stringForColumn:@"QuestionLevelID"] forKey:@"QuestionLevelID"];
                    [mdict setValue:[results stringForColumn:@"Duration"] forKey:@"Duration"];
                    [mdict setValue:@"0" forKey:@"isRight"];
                    [mdict setValue:optionOn forKey:@"selectedAns"];
                    [tempIncorrect addObject:mdict];
                    [tempMCQQuestion addObject:mdict];
                    
                }
                
                ////Add Option
                
                FMResultSet *results1 = [MyModel selectQuery:[NSString stringWithFormat:@"select * from mcqoption where MCQQuestionID=%@",queIds.QuestionID]];
                
                NSMutableArray *arrtemp=[[NSMutableArray alloc]init];
                while ([results1 next])
                {
                    NSMutableDictionary* mdict = [[NSMutableDictionary alloc] init];
                    
                    [mdict setValue:[results1 stringForColumn:@"MCQQuestionID"] forKey:@"MCQQuestionID"];
                    [mdict setValue:[results1 stringForColumn:@"MCQOptionID"] forKey:@"MCQOptionID"];
                    [mdict setValue:[results1 stringForColumn:@"Options"] forKey:@"Options"];
                    [mdict setValue:[results1 stringForColumn:@"ImageURL"] forKey:@"ImageURL"];
                    [mdict setValue:[results1 stringForColumn:@"OptionOn"] forKey:@"OptionOn"];
                    [mdict setValue:[results1 stringForColumn:@"isCorrect"] forKey:@"isCorrect"];
                    
                    [mdict setValue:[results1 stringForColumn:@"CreatedBy"] forKey:@"CreatedBy"];
                    [mdict setValue:[results1 stringForColumn:@"CreatedOn"] forKey:@"CreatedOn"];
                    [mdict setValue:[results1 stringForColumn:@"ModifiedOn"] forKey:@"ModifiedOn"];
                    [mdict setValue:[results1 stringForColumn:@"ModifiedBy"] forKey:@"ModifiedBy"];
                    
                    [arrtemp addObject:mdict];
                    
                }
                
                [tempIncorrectOp addObject:arrtemp];
                [tempMCQOtion addObject:mdict];
            }
            
        }
        else
        {
            FMResultSet *results = [MyModel selectQuery:[NSString stringWithFormat:@"select * from mcqquestion where MCQQuestionID =%@",queIds.QuestionID]];
            
            while ([results next])
            {
                NSMutableDictionary* mdict = [[NSMutableDictionary alloc] init];
                
                [mdict setValue:[results stringForColumn:@"MCQQuestionID"] forKey:@"MCQQuestionID"];
                [mdict setValue:[results stringForColumn:@"Solution"] forKey:@"Solution"];
                [mdict setValue:[results stringForColumn:@"Question"] forKey:@"Question"];
                [mdict setValue:[results stringForColumn:@"ImageURL"] forKey:@"ImageURL"];
                [mdict setValue:[results stringForColumn:@"BoardID"] forKey:@"BoardID"];
                [mdict setValue:[results stringForColumn:@"PageNumberID"] forKey:@"PageNumberID"];
                [mdict setValue:[results stringForColumn:@"is_demo"] forKey:@"is_demo"];
                [mdict setValue:[results stringForColumn:@"MediumID"] forKey:@"MediumID"];
                [mdict setValue:[results stringForColumn:@"ClassID"] forKey:@"ClassID"];
                [mdict setValue:[results stringForColumn:@"StanderdID"] forKey:@"StanderdID"];
                [mdict setValue:[results stringForColumn:@"SubjectID"] forKey:@"SubjectID"];
                [mdict setValue:[results stringForColumn:@"ChapterID"] forKey:@"ChapterID"];
                [mdict setValue:[results stringForColumn:@"QuestionTypeID"] forKey:@"QuestionTypeID"];
                [mdict setValue:[results stringForColumn:@"QuestionLevelID"] forKey:@"QuestionLevelID"];
                [mdict setValue:[results stringForColumn:@"Duration"] forKey:@"Duration"];
                [mdict setValue:@"0" forKey:@"isRight"];
                [tempNotapper addObject:mdict];
                [tempMCQQuestion addObject:mdict];
                
            }
            
            ////Add Option
            
            FMResultSet *results1 = [MyModel selectQuery:[NSString stringWithFormat:@"select * from mcqoption where MCQQuestionID=%@",queIds.QuestionID]];
            
            NSMutableArray *arrtemp=[[NSMutableArray alloc]init];
            while ([results1 next])
            {
                NSMutableDictionary* mdict = [[NSMutableDictionary alloc] init];
                
                [mdict setValue:[results1 stringForColumn:@"MCQQuestionID"] forKey:@"MCQQuestionID"];
                [mdict setValue:[results1 stringForColumn:@"MCQOptionID"] forKey:@"MCQOptionID"];
                [mdict setValue:[results1 stringForColumn:@"Options"] forKey:@"Options"];
                [mdict setValue:[results1 stringForColumn:@"ImageURL"] forKey:@"ImageURL"];
                [mdict setValue:[results1 stringForColumn:@"OptionOn"] forKey:@"OptionOn"];
                [mdict setValue:[results1 stringForColumn:@"isCorrect"] forKey:@"isCorrect"];
                
                [mdict setValue:[results1 stringForColumn:@"CreatedBy"] forKey:@"CreatedBy"];
                [mdict setValue:[results1 stringForColumn:@"CreatedOn"] forKey:@"CreatedOn"];
                [mdict setValue:[results1 stringForColumn:@"ModifiedOn"] forKey:@"ModifiedOn"];
                [mdict setValue:[results1 stringForColumn:@"ModifiedBy"] forKey:@"ModifiedBy"];
                
                [arrtemp addObject:mdict];
            }
            
            [tempNotapperOp addObject:arrtemp];
            [tempMCQOtion addObject:arrtemp];
        }
        
        
    }
    
    NSLog(@"tempCorrect %@",tempCorrect);
    NSLog(@"tempIncorrect %@",tempIncorrect);
    NSLog(@"tempNotapper %@",tempNotapper);
    NSLog(@"tempCorrectOp %@",tempCorrectOp);
    NSLog(@"tempInCorrectOp %@",tempIncorrectOp);
    NSLog(@"tempNotapperOp %@",tempNotapperOp);
    
    TestReportVC *add = [[TestReportVC alloc] initWithNibName:@"TestReportVC" bundle:nil];
    add.tempNotapper=tempNotapper;
    add.tempIncorrect=tempIncorrect;
    add.tempCorrect=tempCorrect;
    add.tempNotapperOp=tempNotapperOp;
    add.tempIncorrectOp=tempIncorrectOp;
    add.tempCorrectOp=tempCorrectOp;
    
    add.TakanTime=@"00";
    add.isFrom=@"SearchPaper";
    
    add.chapterList=@"";
    add.TBStatus=@"";
    add.subjectId=subId;
    add.subjectName=Subnm;
    add.levelId=@"1";
    add.mcqQuestions=tempMCQQuestion;
    add.mcqOption=tempMCQOtion;
    //[self presentViewController:add animated:YES completion:nil];
    [self.navigationController pushViewController:add animated:YES];
    
    
}




- (IBAction)btn_LEFT:(id)sender {
    
    _vw_left.hidden=NO;
    _vw_right.hidden=YES;
    _btn_background.hidden=NO;
    
    [_tbl_left reloadData];
    
}

- (IBAction)btn_RIGHT:(id)sender {
    _vw_left.hidden=YES;
    _vw_right.hidden=NO;
    _btn_background.hidden=NO;
    
    [_tbl_right reloadData];
    
}

- (IBAction)btn_DISMISSTAB:(id)sender {
    
    _vw_left.hidden=YES;
    _vw_right.hidden=YES;
    _btn_background.hidden=YES;
}

- (IBAction)btn_FILTERL:(id)sender {
    _vw_left.hidden=YES;
    _vw_right.hidden=YES;
    _btn_background.hidden=YES;
    
    arrSubIds=[[NSMutableArray alloc]init];
    
    for (int i=0; i<arrSubjects.count; i++) {
        if ([[arrchk objectAtIndex:i]isEqualToString:@"1"]) {
            [arrSubIds addObject:[[arrSubjects objectAtIndex:i] SubjectID]];
        }
    }

    
    [self CallFilter];
    
}

- (IBAction)btn_FILTERR:(id)sender {
    _vw_left.hidden=YES;
    _vw_right.hidden=YES;
    _btn_background.hidden=YES;
    
    arrLevelIds=[[NSMutableArray alloc]init];
    
    for (int i=0; i<arrlevel.count; i++) {
        if ([[[arrlevel objectAtIndex:i] valueForKey:@"isCheck"] isEqualToString:@"1"]) {
            [arrLevelIds addObject:[NSString stringWithFormat:@"%d",i+1]];
        }
    }

    
    [self CallFilter];
    
}

-(void)CallFilter
{
    myDbQueris =[[MyDBQueries alloc]init];
    myDBParser=[[MyDBResultParser alloc]init];
    filePath = [MyModel getFilePath];
    
    NSString *subId=@"";
    NSString *levelId=@"";
    
    if (arrSubIds.count>0) {
        subId=[NSString stringWithFormat:@"and s.SubjectID in(%@)",[arrSubIds componentsJoinedByString:@","]];
    }
   
    
    if (arrLevelIds.count>0)
    {
        levelId=[NSString stringWithFormat:@"and smth.StudentMCQTestHDRID IN (select StudentMCQTestHDRID from student_mcq_test_level where LevelID IN (%@))",[arrLevelIds componentsJoinedByString:@","]];
    }
    

    arrList=[[NSMutableArray alloc]init];
    
    FMResultSet *rs = [MyModel selectQuery:[myDbQueris getAllTestListWithFilter:subId :levelId]];
    while ([rs next]) {
        MCQFilterModel *SubModel = [[MCQFilterModel alloc] init];
        [arrList  addObject:[myDBParser parseDBResult:rs :SubModel]];
    }
    
    [_tbl_list reloadData];
    
    
}

@end
