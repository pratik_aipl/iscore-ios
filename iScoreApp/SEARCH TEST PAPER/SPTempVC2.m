//
//  SPTempVC2.m
//  iScoreApp
//
//  Created by ADMIN-Khushal on 7/25/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "SPTempVC2.h"

#import "ApplicationConst.h"
#import "UIColor+CL.h"
#import "AllSubjectModel.h"
#import "PractisePaperListModel.h"
#import "SPTempCell2.h"
#import "PDFView.h"

#import "FMResultSet.h"
#import "MyModel.h"


@interface SPTempVC2 ()

@end

@implementation SPTempVC2

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    arrSubjects=[[NSMutableArray alloc]init];
    arrchk=[[NSMutableArray alloc]init];
    arrPtype=[[NSMutableArray alloc]init];
    arrList=[[NSMutableArray alloc]init];
    
    [self CallgetSubject];
    [self CallgetTestList];
    
    for (int i=0; i<arrSubjects.count; i++) {
        [arrchk addObject:@"0"];
    }
    
    for (int i=0; i<3; i++) {
        [arrPtype addObject:@"0"];
    }
    
    //SHADOW
    _vw_tbl.layer.shadowRadius  = 3.5f;
    _vw_tbl.layer.shadowColor   = [UIColor colorWithRed:176.f/255.f green:199.f/255.f blue:226.f/255.f alpha:1.f].CGColor;
    _vw_tbl.layer.shadowOffset  = CGSizeMake(0.0f, 0.0f);
    _vw_tbl.layer.shadowOpacity = 2.1f;
    _vw_tbl.layer.masksToBounds = NO;
    
}

-(void)CallgetSubject
{
    myDbQueris =[[MyDBQueries alloc]init];
    myDBParser=[[MyDBResultParser alloc]init];
    filePath = [MyModel getFilePath];
    
    FMResultSet *rs = [MyModel selectQuery:[myDbQueris getPractisePaperSubject]];
    while ([rs next]) {
        AllSubjectModel *SubModel = [[AllSubjectModel alloc] init];
        [arrSubjects  addObject:[myDBParser parseDBResult:rs :SubModel]];
    }
    
    
}

-(void)CallgetTestList
{
    myDbQueris =[[MyDBQueries alloc]init];
    myDBParser=[[MyDBResultParser alloc]init];
    filePath = [MyModel getFilePath];
    
    FMResultSet *rs = [MyModel selectQuery:[myDbQueris getAllTestListPractisePaper]];
    while ([rs next]) {
        PractisePaperListModel *SubModel = [[PractisePaperListModel alloc] init];
        [arrList  addObject:[myDBParser parseDBResult:rs :SubModel]];
    }
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView==_tbl_list) {
        return arrList.count;
    }
    else if (tableView==_tbl_left) {
        return arrSubjects.count;
    }
    else
    {
        return arrPtype.count;
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==_tbl_list) {
        
        PractisePaperListModel *Flist=[arrList objectAtIndex:indexPath.row];
        
        SPTempCell2 *cell = [tableView dequeueReusableCellWithIdentifier:@"CellList"];
        if (cell == nil)
        {
            // Creating a cell here
            cell = [[SPTempCell2 alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CellList"];
        }
        
        //cell.lbl_totques.text=[NSString stringWithFormat:@"%@",Flist.Total_que];
        cell.lbl_date.text=[NSString stringWithFormat:@"%@",Flist.CreatedOn];
        cell.lbl_date.text= [cell.lbl_date.text substringToIndex:[cell.lbl_date.text length] - 8];
        cell.lbl_subnm.text=[NSString stringWithFormat:@"%@",Flist.SubjectName];
        
        
        if ([Flist.PaperTypeID intValue] == 3) {
            cell.lbl_accuracy.text=[NSString stringWithFormat:@"%@",Flist.PaperTypeName];
            cell.lbl_ques.text=[NSString stringWithFormat:@"%@",Flist.SetPaperMarks];
        }
        else
        {
            cell.lbl_accuracy.text=[NSString stringWithFormat:@"%@",Flist.ExamTypeName];
            cell.lbl_ques.text=[NSString stringWithFormat:@"%@",Flist.TotalMarks];
        }
        
        CGAffineTransform transform = CGAffineTransformMakeScale(1.0f, 2.0f);
        cell.prog_accu.transform = transform;
        cell.prog_accu.clipsToBounds = YES;
        cell.prog_accu.layer.cornerRadius = 1.5;
        
        /////////Date Format//////////
        NSString *str = cell.lbl_date.text;
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        NSDate *date = [dateFormatter dateFromString: str];
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd/MM/yyyy"];
        NSString *convertedString = [dateFormatter stringFromDate:date];
        cell.lbl_date.text=[NSString stringWithFormat:@"%@",convertedString];
        //////////////////////////////
        

        
        
        
        return cell;
    }
    else if (tableView==_tbl_left)
    {
        AllSubjectModel *subject=[arrSubjects objectAtIndex:indexPath.row];
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
        if (cell == nil)
        {
            // Creating a cell here
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
            
            UILabel *theLabelIWant = [[UILabel alloc] initWithFrame:CGRectMake(10, 14, 120, 21)];
            theLabelIWant.tag = 1;
            [cell.contentView addSubview:theLabelIWant];
            
            UIImageView *dot =[[UIImageView alloc] initWithFrame:CGRectMake(150,14,21,21)];
            dot.tag=2;
            
            [cell.contentView addSubview:dot];
        }
        
        UILabel *finalLabel = (UILabel *)[[cell contentView] viewWithTag:1];
        [finalLabel setText:subject.SubjectName];
        UIImageView *dot =(UIImageView *)[[cell contentView] viewWithTag:2];
        if ([[arrchk objectAtIndex:indexPath.row] isEqualToString:@"0"]) {
            dot.image=[UIImage imageNamed:@"checkbox-unselected1"];
        }
        else
        {
            dot.image=[UIImage imageNamed:@"checkbox-selected1"];
        }
        
        return cell;
    }
    else
    {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell1"];
        if (cell == nil)
        {
            // Creating a cell here
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell1"];
            
            UILabel *theLabelIWant = [[UILabel alloc] initWithFrame:CGRectMake(10, 14, 120, 21)];
            theLabelIWant.tag = 1;
            [cell.contentView addSubview:theLabelIWant];
            
            
            UIImageView *dot =[[UIImageView alloc] initWithFrame:CGRectMake(150,14,21,21)];
            dot.tag=2;
            
            [cell.contentView addSubview:dot];
            
        }
        
        UILabel *finalLabel = (UILabel *)[[cell contentView] viewWithTag:1];
        if (indexPath.row==0) {
             [finalLabel setText:[NSString stringWithFormat:@"Prilim Paper"]];
        }
        else if (indexPath.row==1)
        {
            [finalLabel setText:[NSString stringWithFormat:@"Ready Paper"]];
        }
        else
        {
            [finalLabel setText:[NSString stringWithFormat:@"Set Paper"]];
        }
       
        
        
        UIImageView *dot =(UIImageView *)[[cell contentView] viewWithTag:2];
        if ([[arrPtype objectAtIndex:indexPath.row] isEqualToString:@"0"]) {
            dot.image=[UIImage imageNamed:@"checkbox-unselected1"];
        }
        else
        {
            dot.image=[UIImage imageNamed:@"checkbox-selected1"];
        }
        
        
        return cell;
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
     [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (tableView==_tbl_list) {
        PractisePaperListModel *Flist=[arrList objectAtIndex:indexPath.row];
        
        [APP_DELEGATE showLoadingView:@""];
        
        if ([Flist.PaperTypeID isEqualToString:@"3"]) {
            
            FMResultSet *results = [MyModel selectQuery:[NSString stringWithFormat:@"select StudentSetPaperQuestionTypeID from student_set_paper_question_type where StudentQuestionPaperID = %@",Flist.StudentQuestionPaperID]];
            NSMutableArray * marr = [[NSMutableArray alloc]init];
            
            while ([results next])
            {
                NSMutableDictionary* mdict = [[NSMutableDictionary alloc] init];
                [mdict setValue:[results stringForColumn:@"StudentSetPaperQuestionTypeID"] forKey:@"StudentSetPaperQuestionTypeID"];
                [marr addObject:[mdict valueForKey:@"StudentSetPaperQuestionTypeID"]];
                
            }
            NSLog(@"===marr %@",marr);
            
            PDFView *vc2 = [[PDFView alloc] initWithNibName:@"PDFView" bundle:nil];
            vc2.arrSQPID=marr;
            vc2.SearchPaper=@"YES";
            vc2.isFrom=@"3";
            [[self navigationController] pushViewController:vc2 animated:YES];
            
            
            
        }
        else
        {
            PDFView *vc2 = [[PDFView alloc] initWithNibName:@"PDFView" bundle:nil];
            vc2.SQPID=[NSString stringWithFormat:@"%@",Flist.StudentQuestionPaperID];
            vc2.SearchPaper=@"YES";
            [[self navigationController] pushViewController:vc2 animated:YES];
        }
        
        [APP_DELEGATE hideLoadingView];
        
    }
    else if (tableView==_tbl_left) {
        
        if ([[arrchk objectAtIndex:indexPath.row] isEqualToString:@"0"]) {
            [arrchk replaceObjectAtIndex:indexPath.row withObject:@"1"];
        }
        else
        {
            [arrchk replaceObjectAtIndex:indexPath.row withObject:@"0"];
        }
        [_tbl_left reloadData];
    }
    else
    {
        if ([[arrPtype objectAtIndex:indexPath.row] isEqualToString:@"0"]) {
            [arrPtype replaceObjectAtIndex:indexPath.row withObject:@"1"];
        }
        else
        {
            [arrPtype replaceObjectAtIndex:indexPath.row withObject:@"0"];
        }
        [_tbl_right reloadData];
    }
   
    
}

- (IBAction)btn_LEFT:(id)sender {
    
    _vw_left.hidden=NO;
    _vw_right.hidden=YES;
    _btn_background.hidden=NO;
    
    [_tbl_left reloadData];
    
}

- (IBAction)btn_RIGHT:(id)sender {
    _vw_left.hidden=YES;
    _vw_right.hidden=NO;
    _btn_background.hidden=NO;
    
    [_tbl_right reloadData];
    
}

- (IBAction)btn_DISMISSTAB:(id)sender {
    
    _vw_left.hidden=YES;
    _vw_right.hidden=YES;
    _btn_background.hidden=YES;
}

- (IBAction)btn_FILTERL:(id)sender {
    _vw_left.hidden=YES;
    _vw_right.hidden=YES;
    _btn_background.hidden=YES;
    
    arrSubIds=[[NSMutableArray alloc]init];
    
    for (int i=0; i<arrSubjects.count; i++) {
        if ([[arrchk objectAtIndex:i]isEqualToString:@"1"]) {
            [arrSubIds addObject:[[arrSubjects objectAtIndex:i] SubjectID]];
        }
    }
    
    [self CallFilter];
    
    
}

- (IBAction)btn_FILTERR:(id)sender {
    _vw_left.hidden=YES;
    _vw_right.hidden=YES;
    _btn_background.hidden=YES;
    
    arrLevelIds=[[NSMutableArray alloc]init];
    
    for (int i=0; i<arrPtype.count; i++) {
        if ([[arrPtype objectAtIndex:i]  isEqualToString:@"1"]) {
            [arrLevelIds addObject:[NSString stringWithFormat:@"%d",i+1]];
        }
    }
    
    [self CallFilter];
}

-(void)CallFilter
{
    myDbQueris =[[MyDBQueries alloc]init];
    myDBParser=[[MyDBResultParser alloc]init];
    filePath = [MyModel getFilePath];
    
    NSString *subId=@"";
    NSString *levelId=@"";
    
    if (arrSubIds.count>0) {
        subId=[NSString stringWithFormat:@"AND `sqp`.`SubjectID` in(%@)",[arrSubIds componentsJoinedByString:@","]];
    }
    
    
    if (arrLevelIds.count>0)
    {
        levelId=[NSString stringWithFormat:@"AND `sqp`.PaperTypeID in(%@)",[arrLevelIds componentsJoinedByString:@","]];
    }
    
    arrList=[[NSMutableArray alloc]init];
    
    FMResultSet *rs = [MyModel selectQuery:[myDbQueris getPractisePaperFilter:subId :levelId]];
    while ([rs next]) {
        PractisePaperListModel *SubModel = [[PractisePaperListModel alloc] init];
        [arrList  addObject:[myDBParser parseDBResult:rs :SubModel]];
    }
    
    [_tbl_list reloadData];
    
}


@end

