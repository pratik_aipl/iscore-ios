//
//  MenuListViewVC.m
//  iScoreApp
//
//  Created by ADMIN-Khushal on 3/2/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "MenuListViewVC.h"
#import "MenuListViewCell.h"
#import "MyProfileVC.h"
#import "QueryVC.h"
#import "LegalVC.h"
#import "SubscribeVC.h"
#import "NotificationVC.h"




#import "SWRevealViewController.h"


#import "HomeVC.h"


@interface MenuListViewVC ()

@end

@implementation MenuListViewVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.navigationController.navigationBarHidden=YES;
    // Do any additional setup after loading the view.
    
   
    
    
    
//    imgiconarr =[[NSMutableArray alloc] initWithObjects:@"nav_home",@"nav_profile",@"nav_query",@"nav_refer",@"nav_rateus",@"nav_legal",@"nav_subscib",@"noti_bell", nil];
//    arrcateg=[[NSMutableArray alloc] initWithObjects:@"DASHBOARD",@"MY PROFILE",@"QUERY?CALL US",@"REFER A FRIEND",@"RATE US",@"LEGAL",@"SUBSCRIBE",@"NOTIFICATION", nil];
    
    imgiconarr =[[NSMutableArray alloc] initWithObjects:@"nav_home",@"nav_profile",@"nav_query",@"nav_refer",@"nav_rateus",@"nav_legal",@"noti_bell", nil];
     arrcateg=[[NSMutableArray alloc] initWithObjects:@"DASHBOARD",@"MY PROFILE",@"QUERY?CALL US",@"REFER A FRIEND",@"RATE US",@"LEGAL",@"NOTIFICATION", nil];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    _img_user_prof.clipsToBounds=YES;
    _img_user_prof.layer.cornerRadius=_img_user_prof.frame.size.height/2;
    
    
    NSMutableDictionary* ParseData=[[NSMutableDictionary alloc]init];
    ParseData=[[NSUserDefaults standardUserDefaults] valueForKey:@"ParseData"];

    _lbl_flname.text=[NSString stringWithFormat:@"%@ %@",[ParseData valueForKey:@"FirstName"],[ParseData valueForKey:@"LastName"]];
    
    //UIImageView *img=[[UIImageView alloc]init];
    
    //img=[[NSUserDefaults standardUserDefaults]valueForKey:@"IMG_USER_PROF"];
    //_img_user_prof.image=img.image;
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrcateg.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    MenuListViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[MenuListViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    cell.img_icon.image=[UIImage imageNamed:[NSString stringWithFormat:@"%@",imgiconarr[indexPath.row]]];
    cell.lbl_title.text=[NSString stringWithFormat:@"%@",arrcateg[indexPath.row]];
    
    
    //etc.
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if (cell.selected) {
        // ... Uncheck
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
    
    SWRevealViewController *revealController = self.revealViewController;
    UIViewController *newFrontController = nil;
    
    
    switch (indexPath.row) {
        case 0:
        {
            newFrontController = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeVC"];
            newFrontController.hidesBottomBarWhenPushed = YES;
            break;
        }
        case 1:
        {
            newFrontController = [self.storyboard instantiateViewControllerWithIdentifier:@"MyProfileVC"];
            newFrontController.hidesBottomBarWhenPushed = YES;
            
            break;
        }
        case 2:
        {
            newFrontController = [self.storyboard instantiateViewControllerWithIdentifier:@"QueryVC"];
            newFrontController.hidesBottomBarWhenPushed = YES;
            
            break;
        }
        case 3:
        {
            
            NSString *texttoshare = @"https://itunes.apple.com/us/app/apple-store/id1507962509?mt=8"; //this is your text string to share
            NSArray *activityItems = @[texttoshare];
            UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
            activityVC.excludedActivityTypes = @[UIActivityTypeAssignToContact, UIActivityTypePrint];
            [self presentViewController:activityVC animated:TRUE completion:nil];
            
            newFrontController = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeVC"];
            newFrontController.hidesBottomBarWhenPushed = YES;

            break;
            
        }
        case 4:
        {
            HomeVC * next = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeVC"];
            next.shareApp=@"YES";
            newFrontController = next;
            newFrontController.hidesBottomBarWhenPushed = YES;
            
            break;
            
        }
        case 5:
        {
            newFrontController = [self.storyboard instantiateViewControllerWithIdentifier:@"LegalVC"];
            newFrontController.hidesBottomBarWhenPushed = YES;
            
            break;
            
        }
//        case 6:
//        {
//            newFrontController = [self.storyboard instantiateViewControllerWithIdentifier:@"SubscribeVC"];
//            newFrontController.hidesBottomBarWhenPushed = YES;
//
//            break;
//
//        }
        case 6:
        {
            newFrontController = [self.storyboard instantiateViewControllerWithIdentifier:@"NotificationVC"];
            newFrontController.hidesBottomBarWhenPushed = YES;
            
            break;
            
        }
        default:
            newFrontController = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeVC"];
            newFrontController.hidesBottomBarWhenPushed = YES;
            break;
    }
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:newFrontController];
    
    [revealController pushFrontViewController:navigationController animated:YES];
    
    
}


@end
