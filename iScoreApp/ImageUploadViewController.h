//
//  ImageUploadViewController.h
//  iScoreApp
//
//  Created by My Mac on 11/05/20.
//  Copyright © 2020 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <Photos/Photos.h>
@import OpalImagePicker;

NS_ASSUME_NONNULL_BEGIN

@interface ImageUploadViewController : UIViewController<OpalImagePickerControllerDelegate>
@property (strong, nonatomic) NSDictionary *dictdata;
@property (strong, nonatomic) IBOutlet UICollectionView *Vw_Collection;
@property (assign, nonatomic)  BOOL View1;
@property(nonatomic, strong) OpalImagePickerController *conttrl;
@property (nonatomic, strong) PHImageRequestOptions *requestOptions;
@end

NS_ASSUME_NONNULL_END
