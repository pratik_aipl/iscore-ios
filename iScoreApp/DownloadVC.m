//
//  DownloadVC.m
//  iScoreApp
//
//  Created by ADMIN-Khushal on 3/1/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "DownloadVC.h"
#import "HomeVC.h"
#import <AFNetworking/AFNetworking.h>
#import "ApplicationConst.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <Photos/Photos.h>
#import "UIColor+CL.h"
#import "DGActivityIndicatorView.h"
#import "Reachability.h"



@interface DownloadVC ()
{
    Reachability *reachabilityNoti;
    NSNotification *internetNotify;
}

-(void)reachabilityChanged:(NSNotification*)note;

@property(strong) Reachability * googleReach;
@property(strong) Reachability * localWiFiReach;
@property(strong) Reachability * internetConnectionReach;


@end

@implementation DownloadVC

- (void)viewDidLoad {
    [super viewDidLoad];
   
    CallFlag=0;
    progVal=0;
    totrecord=100;
    self.navigationController.navigationBarHidden=YES;
    // Do any additional setup after loading the view.
    NSLog(@"DICT DATA  %@",_dictdata);
    [self ClearAllDataofDB];
    
    if([AppDelegate isInternetConnected]){
        [self ProgressView];
        [self DataParsing];
        [self showActivityIndicator];
        [self CallTableCount];
        [self CallAPIs];
    } else {
       
    }
    
    
    [NSTimer scheduledTimerWithTimeInterval:0.3f
                                     target:self selector:@selector(UpdateProgress) userInfo:nil repeats:YES];
    
    
    
    //REACHABILITY CHANGE NOTIFY
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reachabilityChanged:)
                                                 name:kReachabilityChangedNotification
                                               object:nil];
    
    
    __weak __block typeof(self) weakself = self;
    
    
    
    
   
    
    self.internetConnectionReach = [Reachability reachabilityForInternetConnection];
    
    self.internetConnectionReach.reachableBlock = ^(Reachability * reachability)
    {
        NSString * temp = [NSString stringWithFormat:@" InternetConnection Says Reachable(%@)", reachability.currentReachabilityString];
        NSLog(@"%@", temp);
        
        dispatch_async(dispatch_get_main_queue(), ^{
         
            
        });
    };
    
    self.internetConnectionReach.unreachableBlock = ^(Reachability * reachability)
    {
        NSString * temp = [NSString stringWithFormat:@"InternetConnection Block Says Unreachable(%@)", reachability.currentReachabilityString];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^(){
           
            NSThread *therd=[NSThread currentThread];
            
            
        });
        
    };
    
    [self.internetConnectionReach startNotifier];
    
}

- (void)internetDisconnectAlert
{

    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"Alert"
                                 message:@"Intenet is not Connected."
                                 preferredStyle:UIAlertControllerStyleAlert];

    //Add Buttons
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"OK"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
                                    //Handle your yes please button action here
                                    [self reachabilityChanged:internetNotify];
                                }];
 
   
    [alert addAction:yesButton];
    [self presentViewController:alert animated:YES completion:nil];
    
}



#pragma mark - Indicator method
-(void)showActivityIndicator{
    
    DGActivityIndicatorView *activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallClipRotate tintColor:[UIColor colorWithHex:0x0A385A] size:20.0];
    
    //activityIndicatorView.frame = CGRectMake(12, 12, 20, 20);
    [activityIndicatorView setCenter:CGPointMake(_view_indicator.frame.size.width-10, _view_indicator.frame.size.height-20)];
    
    //[activityIndicatorView setCenter:CGPointMake(20, 10)];
    
    [activityIndicatorView startAnimating];
    [self.view_indicator addSubview:activityIndicatorView];
    
    
}
-(void)ProgressView
{
    [_pro_download.layer setCornerRadius:_pro_download.frame.size.height/2];
    _pro_download.clipsToBounds = true;
    [self.pro_download setTransform:CGAffineTransformMakeScale(1.0f, 4.0f)];
    
    float f= (float)progVal/(float)totrecord;
    NSLog(@"%f",f);
    
    _pro_download.progress =  (float)progVal/(float)totrecord;
    _pro_download.trackTintColor = [UIColor colorWithHex:0xDDDDDD];
    _pro_download.progressTintColor = [UIColor colorWithHex:0x0A385A];
    _lbl_pers.text=[NSString stringWithFormat:@"%.f %%",((float)progVal/(float)totrecord)*100];
    
    if (totrecord<progVal) {
        progVal=totrecord;
    }
    
}
-(void)UpdateProgress
{
    _lbl_pers.text=[NSString stringWithFormat:@"%.f %%",((float)progVal/(float)totrecord)*100];
    _pro_download.progress =  (float)progVal/(float)totrecord;
    
}

-(void)DataParsing
{
    
    if (_dictdata.count>0) {
        ParseData=[[NSMutableDictionary alloc]init];
        [ParseData setDictionary:[[_dictdata valueForKey:@"KeyData"] objectAtIndex:0]];
        NSLog(@"temp dict %@",ParseData);
        [ParseData setValue:[_dictdata valueForKey:@"EmailID"] forKey:@"EmailID"];
        [ParseData setValue:[_dictdata valueForKey:@"FirstName"] forKey:@"FirstName"];
        [ParseData setValue:[_dictdata valueForKey:@"FlowFlag"] forKey:@"FlowFlag"];
        [ParseData setValue:[_dictdata valueForKey:@"IMEINo"] forKey:@"IMEINo"];
        [ParseData setValue:[_dictdata valueForKey:@"LastName"] forKey:@"LastName"];
        [ParseData setValue:[_dictdata valueForKey:@"MOTP"] forKey:@"MOTP"];
        [ParseData setValue:[_dictdata valueForKey:@"ProfileImage"] forKey:@"ProfileImage"];
        [ParseData setValue:[_dictdata valueForKey:@"RegMobNo"] forKey:@"RegMobNo"];
        [ParseData setValue:[_dictdata valueForKey:@"SchoolName"] forKey:@"SchoolName"];
        [ParseData setValue:[_dictdata valueForKey:@"StudentCode"] forKey:@"StudentCode"];
        [ParseData setValue:[_dictdata valueForKey:@"StudentID"] forKey:@"StudentID"];
        
        NSLog(@"Parse dtata %@",ParseData);
        
        [[NSUserDefaults standardUserDefaults] setObject:ParseData forKey:@"ParseData"];
        [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:@"DataDownloadStatus"];
        
        
    }
    
}

#pragma REACHABILITY
-(void)reachabilityChanged:(NSNotification*)note
{
    internetNotify=note;
    reachabilityNoti = (Reachability *)[note object];
    
    if ([reachabilityNoti isReachable]) {
        NSLog(@"Reachable");
        if(CallFlag==0)
            [self CallTableCount];
        else
            [self CallAPIs1];
        
        
    } else {
        NSLog(@"Unreachable");
        [self internetDisconnectAlert];
        
    }
}

-(void)CallAPIs1
{
    CallFlag--;
    [self CallAPIs];
}

-(void)ClearAllDataofDB
{
    
    ////REMOVE ALL OLD DATA
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *txtPath = [documentsDirectory stringByAppendingPathComponent:@"subject_icon"];
    NSLog(@"path:%@",txtPath);
    BOOL success = [fileManager removeItemAtPath:[NSString stringWithFormat:@"%@", txtPath] error:&error];
    
    txtPath = [documentsDirectory stringByAppendingPathComponent:@"Uploads"];
    NSLog(@"path:%@",txtPath);
    success = [fileManager removeItemAtPath:[NSString stringWithFormat:@"%@", txtPath] error:&error];
    
    
    
   
    [MyModel UpdateQuery:[NSString stringWithFormat:@"DELETE FROM subjects"]];
    [MyModel UpdateQuery:[NSString stringWithFormat:@"DELETE FROM chapters"]];
    [MyModel UpdateQuery:[NSString stringWithFormat:@"DELETE FROM masterquestion"]];
    [MyModel UpdateQuery:[NSString stringWithFormat:@"DELETE FROM examtype_subject"]];
    [MyModel UpdateQuery:[NSString stringWithFormat:@"DELETE FROM mcqquestion"]];
    [MyModel UpdateQuery:[NSString stringWithFormat:@"DELETE FROM exam_type_pattern_detail"]];
    [MyModel UpdateQuery:[NSString stringWithFormat:@"DELETE FROM question_types"]];
    [MyModel UpdateQuery:[NSString stringWithFormat:@"DELETE FROM mcqoption"]];
    [MyModel UpdateQuery:[NSString stringWithFormat:@"DELETE FROM examtypes"]];
    [MyModel UpdateQuery:[NSString stringWithFormat:@"DELETE FROM exam_type_pattern"]];
    [MyModel UpdateQuery:[NSString stringWithFormat:@"DELETE FROM paper_type"]];
    
}

-(void)CallAPIs
{
 
    if([AppDelegate isInternetConnected]){
        CallFlag++;
        switch (CallFlag) {
            case 1:
                [self CallGetSubject];
                NSLog(@"CallGetSubject===");
                break;
                
            case 2:
                [self CallGetChapter];
                NSLog(@"CallGetChapter===");
                break;
                
            case 3:
               
                [self CallMasterQuestion];
                NSLog(@"CallGetImages===");
                break;
                
            case 4:
                [self CallExamtype_subject];
                NSLog(@"CallExamtype_subject===");
                break;
                
            case 5:
                [self CallMCQQuestions];
                NSLog(@"CallMCQQuestions===");
                break;
                
            case 6:
                [self CallGetImages];
                NSLog(@"CallExamtypesPattern===");
                break;
                
            case 7:
                [self CallExamtypesPatternDtl];
                NSLog(@"CallExamtypesPatternDtl===");
                break;
                
            case 8:
                [self CallQuestionType];
                NSLog(@"CallQuestionType===");
                break;
                
            case 9:
                [self CallMCQOptions];
                NSLog(@"CallMCQOptions===");
                break;
                
            case 10:
                [self CallExamtypes];
                NSLog(@"CallExamtypes===");
                break;
                
            case 11:
                [self CallExamtypesPattern];
                NSLog(@"CallMasterQuestion===");
                break;
                
            case 12:
                [self CallPaperType];
                NSLog(@"CallPaperType===");
                break;
                
            case 13:
                [self NEXT];
                NSLog(@"NEXT===");
                break;
                
            default:
                break;
        }
    }
    else
    {
        [self internetDisconnectAlert];
    }
    
    
            
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void) getImageFromURLAndSaveItToLocalData:(NSString *)imageName fileURL:(NSString *)fileURL inDirectory:(NSString *)directoryPath {
    
    if ([AppDelegate isInternetConnected]) {
        NSLog(@"IMAGE NAME :: %@",imageName);
        NSLog(@"IMAGE URL :: %@",fileURL);
        
        NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString:fileURL]];
        
        NSError *error = nil;
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *tempPath = [paths objectAtIndex:0];;
        
        directoryPath = [tempPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@", directoryPath]];
        directoryPath = [directoryPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@", imageName]];
        
        [[NSFileManager defaultManager] createFileAtPath:directoryPath
                                                contents:data
                                              attributes:nil];
    }
    else
    {
        [self internetDisconnectAlert];
    }
    
}


-(void)CallChangeSyncDate
{
    [APP_DELEGATE showLoadingView:@""];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setValue:[[NSUserDefaults standardUserDefaults]valueForKeyPath:@"ParseData.StudentID"] forKey:@"StudID"];
    [params setValue:@"b752b39e-9606-4149-ae0c-e396cf5aee" forKey:@"player_id"];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    [manager POST:[NSString stringWithFormat:@"%@update_last_sync_time",BaseURLAPI] parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        
        [APP_DELEGATE hideLoadingView];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [APP_DELEGATE hideLoadingView];
        NSLog(@"Error: %@", error);
    }];
}

-(void)CallGetImages
{
    //[APP_DELEGATE showLoadingView:@""];
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0ul);
    dispatch_async(queue, ^{
        dispatch_sync(dispatch_get_main_queue(), ^{
            //CODE
            _lbl_ass_picture.text=@"Contants";
            //Post Method Request
            NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
            
            //[AddPost setValue:[ParseData valueForKey:@"StudentID"] forKey:@"StudID"];
            [AddPost setValue:[[NSUserDefaults standardUserDefaults]valueForKeyPath:@"ParseData.StudentID"] forKey:@"StudID"];
            
            
            AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
            manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
            [manager GET:[NSString stringWithFormat:@"%@get_images?",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject){
                NSLog(@"JSON: %@", responseObject);
                
                if ([[responseObject valueForKey:@"status"] intValue]==1) {
                    
                    ShareData.marrQueImages=[NSMutableArray arrayWithArray:[responseObject valueForKeyPath:@"data.question_images"]];
                    
                    ShareData.rootImages=[NSString stringWithFormat:@"%@%@",[responseObject valueForKey:@"root_path"],[responseObject valueForKeyPath:@"data.subject_icon.folder_path"]];
                    
                    ShareData.marrImages=[NSMutableArray arrayWithArray:[responseObject valueForKeyPath:@"data.subject_icon.images"]];
                    
                    
                    NSLog(@"ShareData.marrQueImages %lu",(unsigned long)ShareData.marrQueImages.count);
                    NSLog(@"ShareData.marrImages %lu",(unsigned long)ShareData.marrImages.count);
                    
                    NSLog(@"===%d",totrecord);
                    totrecord +=ShareData.marrQueImages.count;
                    totrecord +=ShareData.marrImages.count;
                    NSLog(@"===%d",totrecord);
                    
                    [self WriteImage];
                    
                    for (int j=0; j<ShareData.marrQueImages.count; j++) {
                        progVal++;
                        
                        //*************FILE PATH**********************
                        NSString *imageURL = [NSString stringWithFormat:@"%@",[ShareData.marrQueImages objectAtIndex:j]];
                        
                        NSString *httpReplaceString = @"http://staff.parshvaa.com/";
                        NSString *httpsReplaceString = @"https://staff.parshvaa.com/";
                        NSString *finalURL;
                        
                        finalURL = [imageURL stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
                        if([imageURL containsString:@"https"]){
                            finalURL = [imageURL stringByReplacingOccurrencesOfString:httpsReplaceString withString:@""];
                        } else {
                            finalURL = [imageURL stringByReplacingOccurrencesOfString:httpReplaceString withString:@""];
                        }
                        NSArray *parts = [finalURL componentsSeparatedByString:@"/"];
                        NSFileManager *fileManager = [NSFileManager defaultManager];
                        NSString *folderPath =@"";
                        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                        NSError *error;
                        
                        
                        for (int k=0; k<parts.count-1; k++) {
                            if(k!=0){
                                
                                folderPath = [NSString stringWithFormat:@"%@/%@",folderPath,parts[k]];
                                NSString *documentsDirectory = [paths objectAtIndex:0];
                                [fileManager createDirectoryAtPath:[documentsDirectory stringByAppendingPathComponent:folderPath] withIntermediateDirectories:NO attributes:nil error:&error];
                            }else {
                                folderPath = [NSString stringWithFormat:@"%@",parts[k]];
                                NSString *documentsDirectory = [paths objectAtIndex:0];
                                [fileManager createDirectoryAtPath:[documentsDirectory stringByAppendingPathComponent:folderPath] withIntermediateDirectories:NO attributes:nil error:&error];
                            }
                        }
                        NSString *writablePath = [folderPath stringByAppendingPathComponent:[parts lastObject]];
                        
                        if(![fileManager fileExistsAtPath:writablePath]){
                            NSString *lastPart = [parts lastObject];
                                  NSLog(@"Image Name => , %@",lastPart);
                            if([lastPart containsString:@"gif"]){
                                //[self getImageFromURLAndSaveItToLocalData:[parts lastObject] fileURL:imageURL inDirectory:folderPath];
                                
                                imageURL = [imageURL stringByReplacingOccurrencesOfString:@" "
                                                                     withString:@"%20"];
                                [self getImageFromURLAndSaveItToLocalData:[parts lastObject] fileURL:imageURL inDirectory:folderPath];
                                
                            } else {
                                imageURL = [imageURL stringByReplacingOccurrencesOfString:@" "
                                                                               withString:@"%20"];
                                [self getImageFromURLAndSaveItToLocalData:[parts lastObject] fileURL:imageURL inDirectory:folderPath];
                            }
                        }
                    }
                }
                if([AppDelegate isInternetConnected])
                {
                    [self CallAPIs];
                }
                else
                {
                    [self internetDisconnectAlert];
                }
                //[APP_DELEGATE hideLoadingView];
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                NSLog(@"Error: %@", error);
                //[APP_DELEGATE hideLoadingView];
            }];
        });
    });
    
    
    
}
-(void)WriteImage
{
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0ul);
    dispatch_async(queue, ^{
        dispatch_sync(dispatch_get_main_queue(), ^{
            //CODE
            
            for (int j=0; j<ShareData.marrImages.count; j++) {
                progVal++;
                
                //*************FILE PATH**********************
                NSString *imageURL = [NSString stringWithFormat:@"%@/%@",ShareData.rootImages,[ShareData.marrImages objectAtIndex:j]];
                
                NSString *httpReplaceString = @"http://staff.parshvaa.com/";
                NSString *httpsReplaceString = @"https://staff.parshvaa.com/";
                NSString *finalURL;
                
                finalURL = [imageURL stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
                if([imageURL containsString:@"https"]){
                    finalURL = [imageURL stringByReplacingOccurrencesOfString:httpsReplaceString withString:@""];
                } else {
                    finalURL = [imageURL stringByReplacingOccurrencesOfString:httpReplaceString withString:@""];
                }
                NSArray *parts = [finalURL componentsSeparatedByString:@"/"];
                NSFileManager *fileManager = [NSFileManager defaultManager];
                NSString *folderPath =@"";
                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                
                NSError *error;
                
                
                for (int k=3; k<parts.count-1; k++) {
                    if(k!=0){
                        
                        folderPath = [NSString stringWithFormat:@"%@/%@",folderPath,parts[k]];
                        NSString *documentsDirectory = [paths objectAtIndex:0];
                        [fileManager createDirectoryAtPath:[documentsDirectory stringByAppendingPathComponent:folderPath] withIntermediateDirectories:NO attributes:nil error:&error];
                    }else {
                        folderPath = [NSString stringWithFormat:@"%@",parts[k]];
                        NSString *documentsDirectory = [paths objectAtIndex:0];
                        [fileManager createDirectoryAtPath:[documentsDirectory stringByAppendingPathComponent:folderPath] withIntermediateDirectories:NO attributes:nil error:&error];
                    }
                }
                NSString *writablePath = [folderPath stringByAppendingPathComponent:[parts lastObject]];
                
                if(![fileManager fileExistsAtPath:writablePath]){
                    if ([parts lastObject]){
                        NSString *lastPart = [parts lastObject];
                        NSLog(@"Image Name => , %@",lastPart);
                    if([lastPart containsString:@"gif"]){
                        [self getImageFromURLAndSaveItToLocalData:[parts lastObject] fileURL:imageURL inDirectory:folderPath];
                                                    
                        } else {
                              [self getImageFromURLAndSaveItToLocalData:[parts lastObject] fileURL:imageURL inDirectory:folderPath];
                        }
                    }
                }
                
            }
            
        });
    });
    
    
    
}


///////////////GET ALL TABLE DATA INFO//////////////
-(void)CallTableCount
{
    //[APP_DELEGATE showLoadingView:@""];
    
    //Post Method Request
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setValue:[[NSUserDefaults standardUserDefaults]valueForKeyPath:@"ParseData.StudentID"] forKey:@"StudID"];//[ParseData valueForKey:@"StudentID"]
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    [manager.requestSerializer setTimeoutInterval:150];
    [manager GET:[NSString stringWithFormat:@"%@get-table-count?",BaseURLAPI] parameters:[params copy] success:^(AFHTTPRequestOperation *operation, id responseObject){
        
        NSLog(@"responce %@",responseObject);
        
        responseObject=[responseObject valueForKey:@"data"];
        
        for(id key in responseObject) {
            totrecord += [[responseObject valueForKey:key] intValue];
        }
        NSLog(@"%d",totrecord);
        
        
        //[APP_DELEGATE hideLoadingView];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        //[APP_DELEGATE hideLoadingView];
    }];
}




-(void)CallMCQOptions
{
    double delayInSeconds = 0.20;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        NSLog(@"Do some work");
        [self TruncateTable:@"mcqoption"];
        [self callNetworkData1:@"mcqoption" :@"get_mcqoptions" :[[NSUserDefaults standardUserDefaults]valueForKeyPath:@"ParseData.StudentID"] :0 :@"MCQOPtionID"];
    });
    
}
-(void)CallMasterQuestion
{
    double delayInSeconds = 0.20;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        NSLog(@"Do some work");
        [self TruncateTable:@"masterquestion"];
        [self callNetworkData1:@"masterquestion" :@"get-question" :[[NSUserDefaults standardUserDefaults]valueForKeyPath:@"ParseData.StudentID"] :0 :@"MQuestionID"];
    });
}
-(void)CallMCQQuestions
{
    _lbl_ass_picture.text=@"Assets & pictures";
    double delayInSeconds = 0.20;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        NSLog(@"Do some work");
        [self TruncateTable:@"mcqquestion"];
        [self callNetworkData1:@"mcqquestion" :@"get_mcqquestions" :[[NSUserDefaults standardUserDefaults]valueForKeyPath:@"ParseData.StudentID"] :0 :@"MCQQuestionID"];
    });
}

-(void)callNetworkData1 :(NSString*)tableName :(NSString*)apiURL :(NSString*)StudID :(int)lastRecordID :(NSString*)tblField
{
    //[APP_DELEGATE showLoadingView:@""];
    
    //Post Method Request
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setValue:StudID forKey:@"StudID"];//
    [params setValue:[NSString stringWithFormat:@"%d",lastRecordID] forKey:@"q_id"];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    [manager.requestSerializer setTimeoutInterval:150];
    [manager GET:[NSString stringWithFormat:@"%@%@?",BaseURLAPI,apiURL] parameters:[params copy] success:^(AFHTTPRequestOperation *operation, id responseObject){
        NSLog(@"responseObject %@",responseObject);
        
        NSLog(@"responseObject %lu ",(unsigned long)[responseObject count]);
        
        
        
        if ([responseObject count] >0) {
            
            for (int i=0; i<(unsigned long)[responseObject count]; i++) {
                progVal++;
                [MyModel insertInto_papertype:[responseObject objectAtIndex:i] :tableName];
            }
            
            NSString* strquery=[NSString stringWithFormat:@"select %@ from %@ order by %@ DESC LIMIT 1",tblField,tableName,tblField];
            
            FMResultSet *rs = [MyModel selectQuery:strquery];
            
            while ([rs next]) {
                NSLog(@"Total Records :%d", [rs intForColumn:tblField]);
                int s=[rs intForColumn:tblField];
                [self callNetworkData1:tableName :apiURL :StudID :s :tblField];
                NSLog(@" LAST ID ------ %d",s);
            }
        }
        else
        {
            if([AppDelegate isInternetConnected])
            {
                [self CallAPIs];
            }
            else
            {
                [self internetDisconnectAlert];
            }
        }
        
        //[APP_DELEGATE hideLoadingView];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        
        //[APP_DELEGATE hideLoadingView];
    }];
    
    
    //[APP_DELEGATE showLoadingView:@""];
    
}


/////////////////////////NO REPEAT CALL ////////////////
-(void)CallGetSubject
{
    double delayInSeconds = 0.20;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        NSLog(@"Do some work");
        
        [self TruncateTable:@"subjects"];
        [self callNetworkData:@"subjects" :@"get_subjects?" :[[NSUserDefaults standardUserDefaults] valueForKeyPath:@"ParseData.StudentID"] :0];
    });
}

-(void)CallGetChapter
{
    double delayInSeconds = 0.20;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        NSLog(@"Do some work");
        [self TruncateTable:@"chapters"];
        [self callNetworkData:@"chapters" :@"get_chapter?" :[[NSUserDefaults standardUserDefaults] valueForKeyPath:@"ParseData.StudentID"] :0];
    });
}

-(void)CallPaperType
{
    double delayInSeconds = 0.20;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        NSLog(@"Do some work");
        [self TruncateTable:@"paper_type"];
        [self callNetworkData:@"paper_type" :@"get_paper_type?" :[[NSUserDefaults standardUserDefaults] valueForKeyPath:@"ParseData.StudentID"] :0];
    });
    
}

-(void)CallExamtype_subject
{
    double delayInSeconds = 0.20;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        NSLog(@"Do some work");
        [self TruncateTable:@"examtype_subject"];
        [self callNetworkData:@"examtype_subject" :@"get_examtype_subject?" :[[NSUserDefaults standardUserDefaults] valueForKeyPath:@"ParseData.StudentID"] :0];
    });
    
}

-(void)CallExamtypes
{
    double delayInSeconds = 0.20;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        NSLog(@"Do some work");
        [self TruncateTable:@"examtypes"];
        [self callNetworkData:@"examtypes" :@"get_examtypes?" :[[NSUserDefaults standardUserDefaults] valueForKeyPath:@"ParseData.StudentID"] :0];
    });
    
}

-(void)CallExamtypesPattern
{
    double delayInSeconds = 0.20;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        NSLog(@"Do some work");
        [self TruncateTable:@"exam_type_pattern"];
        [self callNetworkData:@"exam_type_pattern" :@"get_exam_type_pattern?" :[[NSUserDefaults standardUserDefaults] valueForKeyPath:@"ParseData.StudentID"] :0];
    });
    
}

-(void)CallExamtypesPatternDtl
{
    double delayInSeconds = 0.20;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        NSLog(@"Do some work");
        [self TruncateTable:@"exam_type_pattern_detail"];
        [self callNetworkData:@"exam_type_pattern_detail" :@"get_exam_type_pattern_detail?" :[[NSUserDefaults standardUserDefaults] valueForKeyPath:@"ParseData.StudentID"] :0];
    });
    
}

-(void)CallQuestionType
{
    double delayInSeconds = 0.20;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        NSLog(@"Do some work");
        [self TruncateTable:@"question_types"];
        [self callNetworkData:@"question_types" :@"get_question_types?" :[[NSUserDefaults standardUserDefaults] valueForKeyPath:@"ParseData.StudentID"] :0];
    });
    
}




-(void)callNetworkData :(NSString*)tableName :(NSString*)apiURL :(NSString*)StudID :(int)lastRecordID
{
    
    
    //[APP_DELEGATE showLoadingView:@""];
    
    //Post Method Request
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setValue:StudID forKey:@"StudID"];//
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager.requestSerializer setTimeoutInterval:150];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    [manager GET:[NSString stringWithFormat:@"%@%@",BaseURLAPI,apiURL] parameters:[params copy] success:^(AFHTTPRequestOperation *operation, id responseObject){
        
        [MyModel UpdateQuery:[NSString stringWithFormat:@"DELETE FROM %@",tableName]];
        
        for (int i=0; i<(unsigned long)[responseObject count]; i++) {
            progVal++;
            [MyModel insertInto_papertype:[responseObject objectAtIndex:i] :tableName];
        }
        
        if([AppDelegate isInternetConnected])
        {
            [self CallAPIs];
        }
        else
        {
            [self internetDisconnectAlert];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);

    }];
    
}

-(void)TruncateTable:(NSString*)tableName
{
    [MyModel UpdateQuery:[NSString stringWithFormat:@"DELETE FROM %@",tableName]];
}

-(void)NEXT
{
    double delayInSeconds = 2.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        NSLog(@"NEXT");
        
        [[NSUserDefaults standardUserDefaults] setValue:@"2" forKey:@"DataDownloadStatus"];
        
        HomeVC * next = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeVC"];
        [self.navigationController pushViewController:next animated:NO];
        
    });
    
}


@end
