//
//  FreeRegisterVC.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 2/24/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Photos/Photos.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "TextFieldValidator.h"

@interface FreeRegisterVC : UIViewController<UIImagePickerControllerDelegate,UITextFieldDelegate>
{
    NSMutableArray *arrimage;
    UIImage *studimg;
    UIImage *teachimg;
    
    
    NSString *otp;
    
    BOOL isInternet;
    
}
@property(retain , nonatomic)NSMutableDictionary *dictdata;


//Outlets
@property (strong, nonatomic) IBOutlet UIButton *btn_student;
@property (strong, nonatomic) IBOutlet UIButton *btn_teacher;
@property (strong, nonatomic) IBOutlet UIImageView *img_down_arrow;

@property (strong, nonatomic) IBOutlet UIButton *btn_browse_image;
@property (strong, nonatomic) IBOutlet TextFieldValidator *txt_fname;
@property (strong, nonatomic) IBOutlet TextFieldValidator *txt_lname;
@property (strong, nonatomic) IBOutlet TextFieldValidator *txt_email;
@property (strong, nonatomic) IBOutlet TextFieldValidator *txt_mobile_no;
@property (strong, nonatomic) IBOutlet UIButton *btn_next;
@property (strong, nonatomic) IBOutlet UIImageView *img_student;
@property (strong, nonatomic) IBOutlet UIImageView *img_teacher;



//Action
- (IBAction)btn_STUDENT_A:(id)sender;
- (IBAction)btn_TEACHER_A:(id)sender;
- (IBAction)btn_NEXT_A:(id)sender;
- (IBAction)btn_BACK:(id)sender;
- (IBAction)btn_CHOOSE_IMG:(id)sender;


@end
