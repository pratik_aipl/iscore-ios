//
//  SSubVC.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 3/7/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SSubCell.h"
#import "MyDBQueries.h"
#import "MyDBResultParser.h"
#import "QuestionTypeTVC.h"
#import "TVCDispChapters.h"

@interface SSubVC : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    NSMutableArray *arrsubjnm;
    NSMutableArray *arrsunjimg;
    MyDBQueries *myDbQueris;
    MyDBResultParser *myDBParser;
    NSMutableArray *subjectArray ;
    NSString *filePath;
}

@property (retain , nonatomic)NSString *BoardID;
@property (retain , nonatomic)NSString *MediumID;
@property (retain , nonatomic)NSString *StandardID;

//Outlets
@property (strong, nonatomic) IBOutlet UITableView *tbl_ssub;




//Action
- (IBAction)btn_BACK:(id)sender;



@end
