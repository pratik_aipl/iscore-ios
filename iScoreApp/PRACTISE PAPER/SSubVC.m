//
//  SSubVC.m
//  iScoreApp
//
//  Created by ADMIN-Khushal on 3/7/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "SSubVC.h"
#import "MyModel.h"
#import "MyDBQueries.h"
#import "MCQSubjectModel.h"
#import "UIColor+CL.h"
#import "Chapters.h"
#import "MyModel.h"
#import "MyDBQueries.h"
#import "ChapterListModel.h"
#import "UIColor+CL.h"
#import "SubjectCell.h"
#import "ApplicationConst.h"
#import "HomeVC.h"
#import "SelectPaperType.h"

@interface SSubVC ()

@end

@implementation SSubVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [_tbl_ssub registerNib:[UINib nibWithNibName:@"SSubCell" bundle:nil] forCellReuseIdentifier:@"Cell"];
    
    //arrsubjnm =[[NSMutableArray alloc]initWithObjects:@"English",@"Hindi",@"Marathi",@"Algebra",@"Geometry",@"Science l",@"Science ll",@"History & PS",@"Geog & Eco",@"General Maths l",@"General Maths ll",@"ICT", nil];
    //arrsunjimg=[[NSMutableArray alloc] initWithObjects:@"sub_english",@"sub_hindi",@"sub_marathi",@"sub_algebra",@"sub_geomatry",@"sub_science_one",@"sub_science_two",@"sub_history",@"sub_gepgrapy",@"sub_math_one",@"sub_math_two",@"sub_ict", nil];
    
    
    
    myDbQueris =[[MyDBQueries alloc]init];
    myDBParser=[[MyDBResultParser alloc]init];
    filePath = [MyModel getFilePath];
    
    FMResultSet *rs = [MyModel selectQuery:[myDbQueris getPRECSubjectsQuery:_StandardID]];
    subjectArray = [[NSMutableArray alloc] init];
    while ([rs next]) {
        
        MCQSubjectModel *SubModel = [[MCQSubjectModel alloc] init];
        [subjectArray  addObject:[myDBParser parseDBResult:rs :SubModel]];
        NSLog(@"--%@",subjectArray);
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return subjectArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    MCQSubjectModel *mySubject = [subjectArray objectAtIndex:indexPath.row];
    
    static NSString *CellIdentifier = @"Cell";
    
    
    SSubCell *cell = (SSubCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:self options:nil];
        cell = (SSubCell *)[nib objectAtIndex:0];
    }
    
    cell.lbl_subtitle.text=[NSString stringWithFormat:@"%@",mySubject.SubjectName];
    cell.lbl_tot_test.text=[NSString stringWithFormat:@"%@",mySubject.TotalSubjectTest];
    
    
    if (mySubject.Last_Date==[NSNull null])
    {
        if (![cell.lbl_lastatte_dt.text isEqualToString:@"N/A"]) {
            // cell.lbl_dt1.frame = CGRectMake(cell.lbl_dt1.frame.origin.x+40, cell.lbl_dt1.frame.origin.y, cell.lbl_dt1.frame.size.width, cell.lbl_dt1.frame.size.height);
            
            CGRect border = cell.lbl_lastatte_dt1.frame;
            border.origin.x =210;
            cell.lbl_lastatte_dt1.frame = border;
            
            
        }
        
        cell.lbl_lastatte_dt.text=@"N/A";
    }
    else
    {
        cell.lbl_lastatte_dt.text=[NSString stringWithFormat:@"%@",mySubject.Last_Date];
        CGRect border = cell.lbl_lastatte_dt1.frame;
        border.origin.x =179;
        cell.lbl_lastatte_dt1.frame = border;
    }
    
    
    NSString* imagepath = [NSString stringWithFormat:@"%@/subject_icon/%@",filePath,[mySubject.SubjectIcon stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
    
    NSLog(@"Subject Icon Path :: %@",[mySubject.SubjectIcon stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]);
    
    NSFileManager *fileManager = [[NSFileManager alloc] init];
    
    UIImage *theImage=[UIImage imageWithContentsOfFile:imagepath];
    cell.img_subimage.image=theImage;
    
       
    
    //etc.
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MCQSubjectModel *mySubject = [subjectArray objectAtIndex:indexPath.row];
   
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if (cell.selected) {
        // ... Uncheck
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
    
    SelectPaperType *VC=[[SelectPaperType alloc]initWithNibName:@"SelectPaperType" bundle:nil];
    VC.SubjectID=mySubject.SubjectID;
    VC.BoardID=_BoardID;
    VC.StandardID=_StandardID;
    VC.MediumID=_MediumID;
    
    [self.navigationController pushViewController:VC animated:YES];
    
}


- (IBAction)btn_BACK:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
