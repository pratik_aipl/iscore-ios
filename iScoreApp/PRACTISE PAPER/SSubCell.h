//
//  SSubCell.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 3/7/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SSubCell : UITableViewCell

//Outlet
@property (strong, nonatomic) IBOutlet UIImageView *img_subimage;
@property (strong, nonatomic) IBOutlet UILabel *lbl_subtitle;

@property (strong, nonatomic) IBOutlet UIProgressView *pro_subject;
@property (strong, nonatomic) IBOutlet UILabel *lbl_tot_test;
@property (strong, nonatomic) IBOutlet UILabel *lbl_lastatte_dt;
@property (strong, nonatomic) IBOutlet UILabel *lbl_lastatte_dt1;


//Action



@end
