//
//  CommonVCCCT.m
//  iScoreApp
//
//  Created by ADMIN-Khushal on 8/7/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "CommonVCCCT.h"
#import "CommonCellMCQ.h"

@interface CommonVCCCT ()

@end

@implementation CommonVCCCT

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _tbl_cct.tableHeaderView = _vw_header;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TableView DataSource Implementation

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 10;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"CellCCT";
    
    CommonCellMCQ *cell = (CommonCellMCQ *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"CommonCellMCQ" owner:self options:nil];
        cell = [nib objectAtIndex:1];
        
    }
    
    NSLog(@"Sting  %ld",(long)indexPath.row);
    
    return cell;
}
@end
