//
//  MyReportsVC.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 8/6/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyReportsVC : UIViewController
{
    NSMutableArray *arrAccu;
}


//Outlet
@property (weak, nonatomic) IBOutlet UIView *vw_main;
@property (weak, nonatomic) IBOutlet UIView *vw_accu_level;
@property (weak, nonatomic) IBOutlet UIView *vw_set_tgt;
@property (weak, nonatomic) IBOutlet UILabel *lbl_curr_level;
@property (weak, nonatomic) IBOutlet UILabel *lbl_tgt;


//Action
- (IBAction)btn_BACK:(id)sender;


@end
