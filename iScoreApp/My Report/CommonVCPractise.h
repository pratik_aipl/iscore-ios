//
//  CommonVCPractise.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 8/7/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommonVCPractise : UIViewController
{
    NSMutableArray *arrPractise;
}

@property (weak, nonatomic) IBOutlet UIView *vw_header;
@property (weak, nonatomic) IBOutlet UITableView *tbl_prectise;

@property (weak, nonatomic) IBOutlet UILabel *lbl_prac_tot;
@property (weak, nonatomic) IBOutlet UILabel *lbl_tot_prac_prelim;
@property (weak, nonatomic) IBOutlet UILabel *lbl_tot_prac_ready;
@property (weak, nonatomic) IBOutlet UILabel *lbl_tot_prac_set;




@end
