//
//  CommonCellMCQ.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 8/7/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommonCellMCQ : UITableViewCell


//Outlet
@property (weak, nonatomic) IBOutlet UILabel *lbl_mcq_subnm;
@property (weak, nonatomic) IBOutlet UILabel *lbl_mcq_tot;
@property (weak, nonatomic) IBOutlet UILabel *lbl_mcq_accu;


////////////Practise paper
@property (weak, nonatomic) IBOutlet UILabel *lbl_pre_subnm;
@property (weak, nonatomic) IBOutlet UILabel *lbl_pre_tot_test;
@property (weak, nonatomic) IBOutlet UILabel *lbl_pre_tot_prelim;
@property (weak, nonatomic) IBOutlet UILabel *lbl_pre_tot_ready;
@property (weak, nonatomic) IBOutlet UILabel *lbl_pre_tot_set;





@end
