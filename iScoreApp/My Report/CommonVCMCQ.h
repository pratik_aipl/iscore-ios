//
//  CommonVCMCQ.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 8/7/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommonVCMCQ : UIViewController
{
    NSMutableArray *arrMcqTIDs;
}



//Outlets
@property (weak, nonatomic) IBOutlet UIView *vw_heade;
@property (weak, nonatomic) IBOutlet UITableView *tbl_mcq;

@property (weak, nonatomic) IBOutlet UILabel *lbl_level1;
@property (weak, nonatomic) IBOutlet UILabel *lbl_level2;
@property (weak, nonatomic) IBOutlet UILabel *lbl_level3;

@property (weak, nonatomic) IBOutlet UILabel *lbl_mcq_tot;
@property (weak, nonatomic) IBOutlet UILabel *lbl_mcq_accu;






@end
