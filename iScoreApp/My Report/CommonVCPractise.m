//
//  CommonVCPractise.m
//  iScoreApp
//
//  Created by ADMIN-Khushal on 8/7/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "CommonVCPractise.h"
#import "CommonCellMCQ.h"
#import "FMResultSet.h"
#import "MyModel.h"


@interface CommonVCPractise ()

@end

@implementation CommonVCPractise

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    _tbl_prectise.tableHeaderView = _vw_header;
    
    [self CallPaperType];
    [self CallTotalPractisePaper];
    [self CallPractisePaper];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TableView DataSource Implementation

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrPractise.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"CellPractise";
    
    CommonCellMCQ *cell = (CommonCellMCQ *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"CommonCellMCQ" owner:self options:nil];
        cell = [nib objectAtIndex:2];
        
    }
    
    
    cell.lbl_pre_subnm.text=[NSString stringWithFormat:@"%@ (Total Test)",[arrPractise valueForKey:@"SubjectName"][indexPath.row]];
    cell.lbl_pre_tot_test.text=[NSString stringWithFormat:@"%02d",[[[arrPractise valueForKey:@"TotalPaper"] objectAtIndex:indexPath.row] intValue]];
    
    cell.lbl_pre_tot_prelim.text=[NSString stringWithFormat:@"%02d",[[[arrPractise valueForKey:@"Prilem"] objectAtIndex:indexPath.row] intValue]];
    cell.lbl_pre_tot_ready.text=[NSString stringWithFormat:@"%02d",[[[arrPractise valueForKey:@"Ready"] objectAtIndex:indexPath.row] intValue]];
    cell.lbl_pre_tot_set.text=[NSString stringWithFormat:@"%02d",[[[arrPractise valueForKey:@"SetPaper"] objectAtIndex:indexPath.row] intValue]];
    
    
    
    
    NSLog(@"Sting  %ld",(long)indexPath.row);
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 125;
}

-(void)CallPractisePaper
{
    
    arrPractise=[[NSMutableArray alloc]init];
    
        
    FMResultSet *results = [MyModel selectQuery:[NSString stringWithFormat:@"SELECT GROUP_CONCAT(student_question_paper.StudentQuestionPaperID) as StudentQuestionPaperID, count(student_question_paper.StudentQuestionPaperID) as TotalPaper, (select count(sqp.StudentQuestionPaperID) as Total_Paper from student_question_paper as sqp where sqp.SubjectID = student_question_paper.SubjectID AND sqp.PaperTypeID = 1) as Prilem , (select count(sqp.StudentQuestionPaperID) as Total_Paper from student_question_paper as sqp where sqp.SubjectID = student_question_paper.SubjectID AND sqp.PaperTypeID = 2) as Ready , (select count(sqp.StudentQuestionPaperID) as Total_Paper from student_question_paper as sqp  where sqp.SubjectID = student_question_paper.SubjectID AND sqp.PaperTypeID = 3) as SetPaper ,  student_question_paper.SubjectID as SubjectID, subjects.SubjectName FROM student_question_paper  LEFT JOIN subjects on student_question_paper.SubjectID=subjects.SubjectID   GROUP BY student_question_paper.SubjectID"]];
        
        while ([results next])
        {
            NSMutableDictionary *mdict=[[NSMutableDictionary alloc]init];
            
            [mdict setValue:[results stringForColumn:@"StudentQuestionPaperID"] forKey:@"StudentQuestionPaperID"];
            [mdict setValue:[results stringForColumn:@"TotalPaper"] forKey:@"TotalPaper"];
            [mdict setValue:[results stringForColumn:@"Prilem"] forKey:@"Prilem"];
            [mdict setValue:[results stringForColumn:@"Ready"] forKey:@"Ready"];
            [mdict setValue:[results stringForColumn:@"SetPaper"] forKey:@"SetPaper"];
            [mdict setValue:[results stringForColumn:@"SubjectID"] forKey:@"SubjectID"];
            [mdict setValue:[results stringForColumn:@"SubjectName"] forKey:@"SubjectName"];
            
            
            [arrPractise addObject:mdict];
        }
    
    
}

-(void)CallTotalPractisePaper
{
    
    NSMutableArray *arrTotPPaper=[[NSMutableArray alloc]init];
    
    FMResultSet *results2 = [MyModel selectQuery:[NSString stringWithFormat:@"select * from student_question_paper"]];
    
    while ([results2 next])
    {
        [arrTotPPaper addObject:[results2 stringForColumn:@"StudentQuestionPaperID"]];
        
    }
    
    _lbl_prac_tot.text=[NSString stringWithFormat:@"%02lu",(unsigned long)arrTotPPaper.count];
    
}

-(void)CallPaperType
{
    
    for (int i=0; i<3; i++) {
        
        NSMutableArray *arrPaperType=[[NSMutableArray alloc]init];
        
        FMResultSet *results1 = [MyModel selectQuery:[NSString stringWithFormat:@"select * from student_question_paper where PaperTypeID = %d",i+1]];
        
        while ([results1 next])
        {
            [arrPaperType addObject:[results1 stringForColumn:@"StudentQuestionPaperID"]];
            
        }
        
        switch (i) {
            case 0:
                _lbl_tot_prac_prelim.text=[NSString stringWithFormat:@"%2lu",(unsigned long)arrPaperType.count];
                break;
            case 1:
                _lbl_tot_prac_ready.text=[NSString stringWithFormat:@"%2lu",(unsigned long)arrPaperType.count];
                break;
            case 2:
                _lbl_tot_prac_set.text=[NSString stringWithFormat:@"%2lu",(unsigned long)arrPaperType.count];
                break;
                
            default:
                break;
        }
        
    }
    
}



@end
