//
//  CommonVCMCQ.m
//  iScoreApp
//
//  Created by ADMIN-Khushal on 8/7/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "CommonVCMCQ.h"
#import "CommonCellMCQ.h"
#import "FMResultSet.h"
#import "MyModel.h"

int totQues1,totCorrect1;

@interface CommonVCMCQ ()

@end

@implementation CommonVCMCQ

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    _tbl_mcq.tableHeaderView = _vw_heade;
    
    [self CallMCQTestIDs];
    [self CallMCQTotQues];
    [self CallMCQTotCorrect];
    
    
    [self CallMCQTestLevels];
    [self CallMCQTestSubjects];
    
   // UINib *nib = [UINib nibWithNibName:@"CellMCQ" bundle:nil];
   // [[self tbl_mcq] registerNib:nib forCellReuseIdentifier:@"CellMCQ"];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TableView DataSource Implementation

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrMcqTIDs.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"CellMCQ";
    CommonCellMCQ *cell = (CommonCellMCQ *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"CommonCellMCQ" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    //[string componentsSeparatedByString:@","]
    
    
    cell.lbl_mcq_subnm.text=[NSString stringWithFormat:@"%@ (Attempt/Accuracy)",[arrMcqTIDs valueForKey:@"SubjectName"][indexPath.row]];
    
    NSArray *j=[[arrMcqTIDs valueForKey:@"StudentMCQTestHDRID"][indexPath.row] componentsSeparatedByString:@","];
    cell.lbl_mcq_tot.text=[NSString stringWithFormat:@"%02lu",(unsigned long)j.count];
    
    int tq=[[[arrMcqTIDs valueForKey:@"total_question"] objectAtIndex:indexPath.row] intValue];
    int tr=[[[arrMcqTIDs valueForKey:@"total_right"] objectAtIndex:indexPath.row] intValue];
    
    float precentage = (100 * tr)/tq;
    
    cell.lbl_mcq_accu.text=[NSString stringWithFormat:@"(%2.0f)%%",precentage];
    
    
    return cell;
}

-(void)CallMCQTestLevels
{
    
    for (int i=0; i<3; i++) {
        arrMcqTIDs=[[NSMutableArray alloc]init];
        
        FMResultSet *results = [MyModel selectQuery:[NSString stringWithFormat:@"select StudentMCQTestHDRID from student_mcq_test_hdr where StudentMCQTestHDRID IN (select StudentMCQTestHDRID from  student_mcq_test_level  where LevelID=%d)",i+1]];
        
        while ([results next])
        {
            [arrMcqTIDs addObject:[results stringForColumn:@"StudentMCQTestHDRID"]];
        }
        
        switch (i) {
            case 0:
                _lbl_level1.text=[NSString stringWithFormat:@"%2lu",(unsigned long)arrMcqTIDs.count];
                break;
                
            case 1:
                _lbl_level2.text=[NSString stringWithFormat:@"%2lu",(unsigned long)arrMcqTIDs.count];
                break;
                
            case 2:
                _lbl_level3.text=[NSString stringWithFormat:@"%2lu",(unsigned long)arrMcqTIDs.count];
                break;
                
            default:
                break;
        }
    }
}

-(void)CallMCQTestSubjects
{
    
        arrMcqTIDs=[[NSMutableArray alloc]init];
        
        
    
    FMResultSet *results = [MyModel selectQuery:[NSString stringWithFormat:@"SELECT GROUP_CONCAT(distinct(main_smth.StudentMCQTestHDRID)) as StudentMCQTestHDRID, main_smth.SubjectID as SubjectID, subjects.SubjectName ,count(smtd.StudentMCQTestDTLID) as total_question, ( SELECT count(StudentMCQTestDTLID) as total_correct FROM student_mcq_test_dtl dtl LEFT JOIN mcqoption op ON dtl.AnswerID = op.MCQOPtionID WHERE   dtl.StudentMCQTestHDRID in(select StudentMCQTestHDRID from student_mcq_test_hdr as ssmth where ssmth.SubjectID = main_smth.SubjectID) AND dtl.IsAttempt = 1 AND op.isCorrect = 1) as total_right  FROM student_mcq_test_hdr as main_smth   LEFT JOIN subjects on main_smth.SubjectID=subjects.SubjectID  LEFT JOIN student_mcq_test_dtl as smtd on main_smth.StudentMCQTestHDRID=smtd.StudentMCQTestHDRID GROUP BY main_smth.SubjectID"]];
    
    
    
    
        
        while ([results next])
        {
            NSMutableDictionary *mdict=[[NSMutableDictionary alloc]init];
            
            [mdict setValue:[results stringForColumn:@"StudentMCQTestHDRID"] forKey:@"StudentMCQTestHDRID"];
            [mdict setValue:[results stringForColumn:@"SubjectID"] forKey:@"SubjectID"];
            [mdict setValue:[results stringForColumn:@"SubjectName"] forKey:@"SubjectName"];
            [mdict setValue:[results stringForColumn:@"total_question"] forKey:@"total_question"];
            [mdict setValue:[results stringForColumn:@"total_right"] forKey:@"total_right"];
            
            [arrMcqTIDs addObject:mdict];
        }
   
    
}

-(void)CallMCQTestIDs
{
    arrMcqTIDs=[[NSMutableArray alloc]init];
    
    FMResultSet *results = [MyModel selectQuery:[NSString stringWithFormat:@"SELECT * FROM student_mcq_test_hdr"]];
    
    while ([results next])
    {
        [arrMcqTIDs addObject:[results stringForColumn:@"StudentMCQTestHDRID"]];
    }
    
    _lbl_mcq_tot.text=[NSString stringWithFormat:@"%02lu",(unsigned long)arrMcqTIDs.count];
}

-(void)CallMCQTotQues
{
    
    totQues1=0;
    
    FMResultSet *results2 = [MyModel selectQuery:[NSString stringWithFormat:@"SELECT count(StudentMCQTestDTLID) as total_right FROM student_mcq_test_dtl dtl WHERE   dtl.StudentMCQTestHDRID in(%@)",[arrMcqTIDs componentsJoinedByString:@","]]];
    
    while ([results2 next])
    {
        totQues1=[[results2 stringForColumn:@"total_right"] intValue];
    }
    
}

-(void)CallMCQTotCorrect
{
    totCorrect1=0;
    
    FMResultSet *results1 = [MyModel selectQuery:[NSString stringWithFormat:@"SELECT count(StudentMCQTestDTLID) as total_correct FROM student_mcq_test_dtl dtl LEFT JOIN mcqoption op ON dtl.AnswerID = op.MCQOPtionID WHERE   dtl.StudentMCQTestHDRID in(%@) AND dtl.IsAttempt = 1 AND op.isCorrect = 1",[arrMcqTIDs componentsJoinedByString:@","]]];
    
    while ([results1 next])
    {
        totCorrect1=[[results1 stringForColumn:@"total_correct"] intValue];
    }
    
    
    
    float precentage = (100 * (float)totCorrect1)/(float)totQues1;
    _lbl_mcq_accu.text=[NSString stringWithFormat:@"%2.0f%% Accuracy",precentage];
    
    
    if(isnan(precentage))
    {
        _lbl_mcq_accu.text=[NSString stringWithFormat:@"00%% Accuracy"];
    }
    else
    {
        _lbl_mcq_accu.text=[NSString stringWithFormat:@"%2.0f%% Accuracy",precentage];
    }
    
    
}


@end
