//
//  MyReportsVC.m
//  iScoreApp
//
//  Created by ADMIN-Khushal on 8/6/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "MyReportsVC.h"
#import "UIColor+CL.h"
#import "TestSummeryVC.h"
#import "TSummeryVC.h"
#import "HomeVC.h"
#import "SubjectVC.h"
#import "StartTestVC.h"
#import "FMResultSet.h"
#import "MyModel.h"


@interface MyReportsVC ()

@end

@implementation MyReportsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self performSegueWithIdentifier:@"mainview" sender:nil];
    
    [self setViewlayer:_vw_accu_level];
    [self setViewlayer:_vw_set_tgt];
    
    [self CallGetAccuracy];

    NSString *subjAcc=[[NSString alloc]init];
    int totacc = 0;
    
    for (int i=0; i<arrAccu.count; i++) {
        totacc+=[[[arrAccu valueForKey:@"accuracy1"] objectAtIndex:i] intValue];
    }
    
    _lbl_curr_level.text=[NSString stringWithFormat:@"%02lu",totacc/arrAccu.count];
    _lbl_tgt.text=[NSString stringWithFormat:@"%2@",[[NSUserDefaults standardUserDefaults]valueForKey:@"CYear"]];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setViewlayer :(UIView*)view
{
    view.layer.borderWidth = 2.5;
    view.layer.borderColor = [UIColor colorWithHex:0xCCCCCC].CGColor;
    view.clipsToBounds=YES;
    view.layer.cornerRadius=view.frame.size.height/2;
    
}

-(void)CallGetAccuracy
{
    FMResultSet *results = [MyModel selectQuery:[NSString stringWithFormat:@"select s.*,(select CreatedOn from student_mcq_test_hdr as sthd where sthd.SubjectID=s.SubjectID                       order by CreatedOn desc limit 1) as Last_Date, count(smth.StudentMCQTestHDRID) as TotalSubjectTest,                       (( (select count(StudentMCQTestDTLID) from student_mcq_test_dtl as dtl left join mcqoption as                           opt on opt.MCQOPtionID=dtl.AnswerID where dtl.IsAttempt=1 and opt.isCorrect=1 and EXISTS                           (select StudentMCQTestHDRID from student_mcq_test_hdr hdr where hdr.StudentMCQTestHDRID=dtl.StudentMCQTestHDRID                            and hdr.SubjectID=s.SubjectID ) ) *100) / (select count (StudentMCQTestDTLID)                                                                       from student_mcq_test_dtl dtl where EXISTS (select StudentMCQTestHDRID from student_mcq_test_hdr hdr where                                                                                                                   hdr.StudentMCQTestHDRID=dtl.StudentMCQTestHDRID and hdr.SubjectID=s.SubjectID )) ) as accuracy1                       from subjects as s left join student_mcq_test_hdr as smth on s.SubjectID=smth.SubjectID where s.StandardID=%@ AND s.isMCQ=1 group by s.SubjectID order by s.SubjectOrder",[[NSUserDefaults standardUserDefaults]valueForKeyPath:@"ParseData.StandardID"]]];
    
    
    
    arrAccu=[[NSMutableArray alloc]init];
    
    while ([results next])
    {
        NSMutableDictionary *mdict=[[NSMutableDictionary alloc]init];
        
        [mdict setValue:[results stringForColumn:@"SubjectID"] forKey:@"SubjectID"];
        [mdict setValue:[results stringForColumn:@"SubjectName"] forKey:@"SubjectName"];
        
        if ([[results stringForColumn:@"accuracy1"] intValue]==nil) {
            [mdict setValue:@"0" forKey:@"accuracy1"];
        }
        else
        {
            [mdict setValue:[results stringForColumn:@"accuracy1"] forKey:@"accuracy1"];
        }
        
        [arrAccu addObject:mdict];
    }
    
    NSLog(@"%@",arrAccu);
    
}



- (IBAction)btn_BACK:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
