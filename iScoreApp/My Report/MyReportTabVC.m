//
//  MyReportTabVC.m
//  iScoreApp
//
//  Created by ADMIN-Khushal on 8/6/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "MyReportTabVC.h"
#import "KPSmartTabBar.h"
#import "AppDelegate.h"
#import "UIColor+CL.h"

#import "CommonViewController.h"
#import "CommonVCMCQ.h"
#import "CommonVCCCT.h"
#import "CommonVCPractise.h"
#import "CommonVCCompare.h"







@interface MyReportTabVC ()<KPSmartTabBarDelegate>
{
    NSMutableArray *arrController,*arrTabes,*arrDColor,*arrColors, *arrImages, *arrTmpImg;
    NSInteger currentSelectedIndex;
}
@property (nonatomic) KPSmartTabBar *tabbar;
@end

@implementation MyReportTabVC

- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    [self setData];
    
    NSMutableArray *t=[[NSUserDefaults standardUserDefaults] objectForKey:@"arr"];
    
        _arrTabsController=_arrColor=[[NSMutableArray alloc] init];
        [_arrTabsController addObject:@"Overall"];
        [_arrTabsController addObject:@"MCQ"];
        [_arrTabsController addObject:@"CCT"];
        [_arrTabsController addObject:@"Practise"];
        [_arrTabsController addObject:@"Comparis"];
        
        [[NSUserDefaults standardUserDefaults] setObject:_arrTabsController forKey:@"arr"];
        [[NSUserDefaults standardUserDefaults] synchronize];
   
    
    currentSelectedIndex = 1;
    [self setSingleColorTab];
    [[NSUserDefaults standardUserDefaults] setInteger:currentSelectedIndex forKey:@"SelectedOption"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self dismissViewControllerAnimated:YES completion:^{
    }];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self setData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setData{
    
    arrTabes = [NSMutableArray new];
    arrController = [NSMutableArray new];
    arrDColor = [NSMutableArray new];
    arrColors = [NSMutableArray new];
    arrImages = [NSMutableArray new];
    
    arrTmpImg = [[NSMutableArray alloc] initWithObjects:@"img1.jpg", @"img2.jpg", @"img3.jpg", @"img4.jpg", @"img5.jpg", nil];
    
    arrTabes=[self getUserTab];
    
    [arrDColor addObject:[UIColor colorWithRed:238.0/255.0 green:64.0/255.0 blue:53.0/255.0 alpha:1.0]];
    [arrDColor addObject:[UIColor colorWithRed:243.0/255.0 green:119.0/255.0 blue:54.0/255.0 alpha:1.0]];
    [arrDColor addObject:[UIColor colorWithRed:123.0/255.0 green:192.0/255.0 blue:67.0/255.0 alpha:1.0]];
    [arrDColor addObject:[UIColor colorWithRed:3.0/255.0 green:146.0/255.0 blue:207.0/255.0 alpha:1.0]];
    [arrDColor addObject:[UIColor colorWithRed:146.0/255.0 green:86.0/255.0 blue:223.0/255.0 alpha:1.0]];
    
    
    int j=0;
    for (int i=0; i<arrTabes.count; i++) {
        @try{
            //[arrImages addObject:[arrTmpImg objectAtIndex:j]];
           // [arrColors addObject:[arrDColor objectAtIndex:j]];
            j++;
            CommonViewController *controller=[[CommonViewController alloc] initWithNibName:@"CommonViewController" bundle:nil];
            controller.title=[arrTabes objectAtIndex:i]; //You Have To Assign This Property For Tab Title
            controller.delegate=self;
            [arrController addObject:controller];
        }
        @catch(NSException *ex){
            j=0;
            //i--;
        }
    }
    
    //[arrTabes addObject:@"+"];
    //CommonViewController *controller=[[CommonViewController alloc] initWithNibName:@"CommonViewController" bundle:nil];
    ////controller.title=[arrTabes lastObject];
    //controller.delegate=self;
    ///[arrController addObject:controller];
    //[arrColors addObject:[UIColor lightGrayColor]];
    
    currentSelectedIndex = [[NSUserDefaults standardUserDefaults] integerForKey:@"SelectedOption"];
    if(currentSelectedIndex == 1)
        [self setSingleColorTab];
   
    
}

-(void)setSingleColorTab{
    
    _viewNav.backgroundColor = [UIColor colorWithRed:13.0/255.0 green:63.0/255.0 blue:108.0/255.0 alpha:1.0];
    
    NSMutableArray *arrControllers = [NSMutableArray new];
    CommonViewController *controller1=[[CommonViewController alloc] initWithNibName:@"CommonViewController" bundle:nil];
    controller1.title= @"Overall";
    controller1.delegate=self;
    [arrControllers addObject:controller1];
    
    CommonVCMCQ *controller2=[[CommonVCMCQ alloc] initWithNibName:@"CommonVCMCQ" bundle:nil];
    controller2.title=@"MCQ";
    //controller2.delegate=self;
    [arrControllers addObject:controller2];
    
    CommonVCCCT *controller3=[[CommonVCCCT alloc] initWithNibName:@"CommonVCCCT" bundle:nil];
    controller3.title=@"CCT";
    //controller3.delegate=self;
    [arrControllers addObject:controller3];
    
    CommonVCPractise *controller4=[[CommonVCPractise alloc] initWithNibName:@"CommonVCPractise" bundle:nil];
    controller4.title=@"Prectise";
    //controller4.delegate=self;
    [arrControllers addObject:controller4];
    
    CommonVCCompare *controller5=[[CommonVCCompare alloc] initWithNibName:@"CommonVCCompare" bundle:nil];
    controller5.title=@"Comparis";
    //controller5.delegate=self;
    [arrControllers addObject:controller5];
    
    NSDictionary *parameters = @{
                                 KPSmartTabBarOptionMenuItemFont: [UIFont fontWithName:@"Roboto-Bold" size:14.0],
                                 KPSmartTabBarOptionMenuItemSelectedFont: [UIFont fontWithName:@"Roboto-Bold" size:15.0],
                                 KPSmartTabBarOptionMenuItemSeparatorWidth : @(4.3),
                                 KPSmartTabBarOptionMenuItemSeparatorColor : [UIColor lightGrayColor],
                                 KPSmartTabBarOptionScrollMenuBackgroundColor : [UIColor whiteColor],
                                 KPSmartTabBarOptionViewBackgroundColor : [UIColor colorWithRed:247.0/255.0 green:247.0/255.0 blue:247.0/255.0 alpha:1.0],
                                 KPSmartTabBarOptionSelectionIndicatorColor : [UIColor colorWithHex:0x085787],
                                 KPSmartTabBarOptionMenuMargin:@(20.0),
                                 KPSmartTabBarOptionMenuHeight: @(40.0),
                                 KPSmartTabBarOptionSelectedMenuItemLabelColor:[UIColor colorWithHex:0x085787],
                                 KPSmartTabBarOptionUnselectedMenuItemLabelColor :[UIColor colorWithHex:0x085787],
                                 KPSmartTabBarOptionCenterMenuItems: @(YES),
                                 KPSmartTabBarOptionUseMenuLikeSegmentedControl : @(YES),
                                 KPSmartTabBarOptionMenuItemSeparatorRoundEdges:@(YES),
                                 KPSmartTabBarOptionEnableHorizontalBounce:@(NO),
                                 KPSmartTabBarOptionMenuItemSeparatorPercentageHeight:@(0.1),
                                 KPSmartTabBarOptionSelectionIndicatorHeight:@(2.0),
                                 KPSmartTabBarOptionAddBottomMenuHairline : @(YES),
                                 KPSmartTabBarOptionBottomMenuHairlineHeight : @(1.0),
                                 KPSmartTabBarOptionBottomMenuHairlineColor : [UIColor lightGrayColor]
                                 };
    
    _tabbar = [[KPSmartTabBar alloc] initWithViewControllers:arrControllers frame:CGRectMake(0.0, 0.0, self.view.frame.size.width, self.view.frame.size.height) options:parameters];
    [self.view addSubview:_tabbar.view];
    
    
    
}

-(void)refreshMe{
    [self viewDidLoad];
    [_tabbar moveToPage:arrTabes.count-2];
}

-(NSMutableArray *)getUserTab{
    return [[[NSUserDefaults standardUserDefaults] objectForKey:@"arr"] mutableCopy];
}

-(void)willMoveToPage:(UIViewController *)controller index:(NSInteger)index{
    NSLog(@"Called");
}

@end
