//
//  CommonVCCompare.m
//  iScoreApp
//
//  Created by ADMIN-Khushal on 8/7/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "CommonVCCompare.h"
#import "FMResultSet.h"
#import "MyModel.h"

@interface CommonVCCompare ()

@end

@implementation CommonVCCompare

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self CallMCQTestIDs];
    [self CallMCQTotQues];
    [self CallMCQTotCorrect];
    [self CallPractiseIDs];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)CallMCQTestIDs
{
    arrMcqTIDs=[[NSMutableArray alloc]init];
    
    FMResultSet *results = [MyModel selectQuery:[NSString stringWithFormat:@"SELECT * FROM student_mcq_test_hdr"]];
    
    while ([results next])
    {
        [arrMcqTIDs addObject:[results stringForColumn:@"StudentMCQTestHDRID"]];
    }
    _lbl_mcq_tot.text=[NSString stringWithFormat:@"%02lu",(unsigned long)arrMcqTIDs.count];
}

-(void)CallMCQTotQues
{
    
    totQues=0;
    
    FMResultSet *results2 = [MyModel selectQuery:[NSString stringWithFormat:@"SELECT count(StudentMCQTestDTLID) as total_right FROM student_mcq_test_dtl dtl WHERE   dtl.StudentMCQTestHDRID in(%@)",[arrMcqTIDs componentsJoinedByString:@","]]];
    
    while ([results2 next])
    {
        totQues=[[results2 stringForColumn:@"total_right"] intValue];
    }
    
}

-(void)CallMCQTotCorrect
{
    totCorrect=0;
    
    FMResultSet *results1 = [MyModel selectQuery:[NSString stringWithFormat:@"SELECT count(StudentMCQTestDTLID) as total_correct FROM student_mcq_test_dtl dtl LEFT JOIN mcqoption op ON dtl.AnswerID = op.MCQOPtionID WHERE   dtl.StudentMCQTestHDRID in(%@) AND dtl.IsAttempt = 1 AND op.isCorrect = 1",[arrMcqTIDs componentsJoinedByString:@","]]];
    
    while ([results1 next])
    {
        totCorrect=[[results1 stringForColumn:@"total_correct"] intValue];
    }
    
    
    float precentage = (100 * (float)totCorrect)/(float)totQues;
    
    _lbl_mcq_accu.text=[NSString stringWithFormat:@"%2.0f%%",precentage];
    
    if(isnan(precentage))
    {
        _lbl_mcq_accu.text=[NSString stringWithFormat:@"00%% Accuracy"];
    }
    else
    {
        _lbl_mcq_accu.text=[NSString stringWithFormat:@"%2.0f%% Accuracy",precentage];
    }
    
    NSLog(@"%@",_lbl_mcq_accu.text);
}

-(void)CallPractiseIDs
{
    arrPractiseIDs=[[NSMutableArray alloc]init];
    
    FMResultSet *results = [MyModel selectQuery:[NSString stringWithFormat:@"SELECT * FROM student_question_paper"]];
    
    while ([results next])
    {
        [arrPractiseIDs addObject:[results stringForColumn:@"StudentQuestionPaperID"]];
    }
    _lbl_prac_tot.text=[NSString stringWithFormat:@"%02lu",(unsigned long)arrPractiseIDs.count];
    
}




@end
