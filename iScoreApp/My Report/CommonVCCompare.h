//
//  CommonVCCompare.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 8/7/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommonVCCompare : UIViewController
{
    NSMutableArray *arrMcqTIDs,*arrPractiseIDs;
    int totQues,totCorrect;
}

//Outlet

@property (weak, nonatomic) IBOutlet UILabel *lbl_mcq_tot;
@property (weak, nonatomic) IBOutlet UILabel *lbl_mcq_accu;

@property (weak, nonatomic) IBOutlet UILabel *lbl_cct_tot;
@property (weak, nonatomic) IBOutlet UILabel *lbl_cct_accu;

@property (weak, nonatomic) IBOutlet UILabel *lbl_prac_tot;

@end
