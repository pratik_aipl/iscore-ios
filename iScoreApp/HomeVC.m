//
//  HomeVC.m
//  iScoreApp
//
//  Created by ADMIN-Khushal on 3/1/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "HomeVC.h"
#import "SWRevealViewController.h"

#import "SSubVC.h"
#import "Chapters.h"
#import "SubjectVC.h"
#import "SubjectForP.h"
#import "SearchPaperMainVC.h"
#import "SPMCQTest.h"
#import "SPTempVC.h"
#import "SPTempVC2.h"
#import "SetGoalVC.h"
#import "UIColor+CL.h"
#import "MyReportsVC.h"
#import "MyReportTabVC.h"
#import "SubjectReviseVC.h"

#import "MenuListViewVC.h"
#import "MyProfileVC.h"
#import "ICDashBoard.h"

#import "ZookiModel.h"
#import "ZookiCell.h"
#import "ZookiWebView.h"
#import "NotificationCollectionVC.h"





@interface HomeVC ()<SWRevealViewControllerDelegate>

@end

@implementation HomeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden=YES;
    // Do any additional setup after loading the view.
    
    [self CallGetZooki];
    
    [self customSetup];
    
    [self setShadow:_vw_allmenu];
    [self setShadow:_vw_zooki];
    
    _scroll_main.scrollEnabled=YES;
    _scroll_main.delegate=self;
    _scroll_main.userInteractionEnabled=YES;
    _scroll_main.contentSize = CGSizeMake(375,888);
    _scroll_main.bounces=NO;
    _scroll_main.showsHorizontalScrollIndicator=NO;
    _scroll_main.showsVerticalScrollIndicator=NO;
    

    
    ///ADD TAP
    picker = [[UIView alloc] initWithFrame:self.view.bounds];
    picker.backgroundColor=[UIColor blackColor];
    picker.alpha=0.4;
    [self.view addSubview: picker];
    picker.hidden=YES;
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self.revealViewController
                                            action:@selector(revealToggle:)];
    [picker addGestureRecognizer:singleFingerTap];
    
    
    _img_user_prof.clipsToBounds=YES;
    _img_user_prof.layer.cornerRadius=_img_user_prof.frame.size.height/2;
    _img_user_prof.layer.borderWidth=4;
    _img_user_prof.layer.borderColor=[UIColor whiteColor].CGColor;
    
    
    NSLog(@"DICT DATA %@",[[NSUserDefaults standardUserDefaults] valueForKey:@"ParseData"]);
    
    ParseData=[[NSMutableDictionary alloc]init];
    ParseData=[[NSUserDefaults standardUserDefaults] valueForKey:@"ParseData"];
    
    _user_nm.text=[NSString stringWithFormat:@"%@ %@",[ParseData  valueForKey:@"FirstName"],[ParseData valueForKey:@"LastName"]];
    _std_id.text=[NSString stringWithFormat:@"%@",[ParseData valueForKey:@"StudentCode"]];
    _lbl_board.text=[NSString stringWithFormat:@"%@",[ParseData valueForKey:@"BoardName"]];
    _lbl_medium.text=[NSString stringWithFormat:@"%@",[ParseData valueForKey:@"MediumName"]];
    _lbl_class.text=[NSString stringWithFormat:@"CLASS %@",[ParseData valueForKey:@"StandardName"]];
    
    
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"PYear"]==nil ) {
        SetGoalVC *homescreen = [self.storyboard instantiateViewControllerWithIdentifier:@"SetGoalVC"];
        homescreen.view.frame = self.view.bounds;
        [homescreen.view setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.5f]];
        [self.view addSubview:homescreen.view];
        [self addChildViewController:homescreen];
        [homescreen didMoveToParentViewController:self];
    }
    
    
    
    _lbl_class.layer.cornerRadius = _lbl_class.layer.frame.size.height/2;
    _lbl_class.clipsToBounds = YES;
    _lbl_class.layer.borderWidth = 1;
    _lbl_class.layer.borderColor = [UIColor colorWithHex:0xFFFFFF].CGColor;
    
    
    if ([_shareApp isEqualToString:@"YES"]) {
            [self shareAppp];
    }
    
    if([Utils isNetworkAvailable] ==YES){
        
        NSLog(@"Network Connection available");
    }
    else
    {
        NSLog(@"Network Connection Not available");
    }
    
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    //isScrollingStart=YES;
    //NSLog(@"scrollViewDidScroll  %f , %f",scrollView.contentOffset.x,scrollView.contentOffset.y);
    
    
    if (scrollView.contentOffset.y<=124) {
        _vw_1.alpha=scrollView.contentOffset.y/124;
    }
    else
    {
        _vw_1.alpha=1.0;
    }
    
 /*
    CGFloat contentOffSet = scrollView.contentOffset.y;
    CGFloat contentHeight = scrollView.contentSize.height;
    
    difference1 = contentHeight - contentOffSet;
    
    NSLog(@"contentOffSet == %f",contentOffSet);
    
    if (difference1 > difference2) {
        NSLog(@"Down");
        
        CGRect myFrame = _img_user_prof.frame;
        myFrame.origin.x = _img_user_prof.frame.origin.x-1;
        _img_user_prof.frame = myFrame;
        
        CGRect myFrame1 = _img_user_prof.frame;
        myFrame1.origin.y = _img_user_prof.frame.origin.y+1;
        _img_user_prof.frame = myFrame1;
       
        
        
    }else{
        
        if (scrollView.contentOffset.y<=118) {
            CGRect myFrame = _img_user_prof.frame;
            myFrame.origin.x = _img_user_prof.frame.origin.x+1;
            _img_user_prof.frame = myFrame;
            
            CGRect myFrame1 = _img_user_prof.frame;
            myFrame1.origin.y = _img_user_prof.frame.origin.y-1;
            _img_user_prof.frame = myFrame1;
        }
        else if (scrollView.contentOffset.y>=119)
        {
            CGRect myFrame = _img_user_prof.frame;
            myFrame.origin.x = _img_user_prof.frame.origin.x+2.2;
            _img_user_prof.frame = myFrame;
        }
        
        NSLog(@"Up");
        
    }
    
    difference2 = contentHeight - contentOffSet;
    
    NSLog(@"x == %f",_img_user_prof.frame.origin.x);
    NSLog(@"y == %f",_img_user_prof.frame.origin.y);
    */
    
    
}

-(void)CallGetZooki
{
    myDbQueris =[[MyDBQueries alloc]init];
    myDBParser=[[MyDBResultParser alloc]init];
    filePath = [MyModel getFilePath];
    
    FMResultSet *rs = [MyModel selectQuery:[myDbQueris getZooki]];
    arrZooki = [[NSMutableArray alloc] init];
    while ([rs next]) {
        
        ZookiModel *SubModel = [[ZookiModel alloc] init];
        [arrZooki  addObject:[myDBParser parseDBResult:rs :SubModel]];
        NSLog(@"--%@",arrZooki);
    }
    
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return arrZooki.count+1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ZookiCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    
    if(indexPath.row == arrZooki.count)
    {
        
        cell.img_zooki.hidden=YES;
        cell.lbl_title.hidden=YES;
        cell.btn_viewmore.hidden=NO;
        cell.btn_viewmore.userInteractionEnabled=YES;
        
    }
    else
    {
        cell.img_zooki.hidden=NO;
        cell.lbl_title.hidden=NO;
        cell.btn_viewmore.hidden=YES;
        
        ZookiModel *myzooki = [arrZooki objectAtIndex:indexPath.row];
        
        cell.lbl_title.text=[NSString stringWithFormat:@"%@",myzooki.Title];
        
        NSString* imagepath = [NSString stringWithFormat:@"%@/zooki/%@",filePath,[myzooki.image stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
        
        NSLog(@"Subject Icon Path :: %@",[myzooki.image stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]);
        
        NSFileManager *fileManager = [[NSFileManager alloc] init];
        
        UIImage *theImage=[UIImage imageWithContentsOfFile:imagepath];
        cell.img_zooki.image=theImage;
        
    }

    return cell;
   
}

- (void)collectionView:(UICollectionView *)collectionView     didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"select ==== %ld",(long)indexPath.row);
    
    if(indexPath.row == arrZooki.count)
    {
        ZookiWebView * next = [self.storyboard instantiateViewControllerWithIdentifier:@"ZookiWebView"];
        [self.navigationController pushViewController:next animated:YES];
        
    }
    else
    {
        //NotificationCollectionVC
        
        NotificationCollectionVC * next = [self.storyboard instantiateViewControllerWithIdentifier:@"NotificationCollectionVC"];
        [self.navigationController pushViewController:next animated:YES];
        
    }
    
    
    
    
    
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)customSetup
{
    
    SWRevealViewController *revealViewController = self.revealViewController;
    //self.revealViewController.rearViewRevealWidth = 145;
    [revealViewController setDelegate:self];
    
    if ( revealViewController )
    {
        
        [_btn_menu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [picker addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        [self.view addGestureRecognizer: self.revealViewController.panGestureRecognizer];

    }
    
    
    
   
    
}
- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position
{
    
    if (revealController.frontViewPosition==FrontViewPositionRight)
    {
        picker.hidden=NO;
    }
    if (revealController.frontViewPosition==FrontViewPositionLeft)
    {
        picker.hidden=YES;
    }
}


- (IBAction)btn_MENU_A:(id)sender {
}

- (IBAction)btn_MCQTEST:(id)sender {
    
    NSUserDefaults *standerId=[[NSUserDefaults alloc]init];
    /* SelectSubVC *viewController=[[SelectSubVC alloc]initWithNibName:@"SelectSubVC" bundle:nil];
     
     viewController.BoardID=[standerId valueForKey:@"BoardID"];
     viewController.MediumID=[standerId valueForKey:@"MediumID"];
     viewController.StandardID=[standerId valueForKey:@"StandardID"];
     
     [self.navigationController pushViewController:viewController animated:YES];
     */
    
    SubjectVC * next = [self.storyboard instantiateViewControllerWithIdentifier:@"SubjectVC"];
    next.BoardID=[standerId valueForKeyPath:@"ParseData.BoardID"];
    next.MediumID=[standerId valueForKeyPath:@"ParseData.MediumID"];
    next.StandardID=[standerId valueForKeyPath:@"ParseData.StandardID"];
    [self.navigationController pushViewController:next animated:YES];
  
    
}

- (IBAction)btn_PRACTISE:(id)sender {
    NSUserDefaults *standerId=[[NSUserDefaults alloc]init];
    SubjectForP * next = [self.storyboard instantiateViewControllerWithIdentifier:@"SubjectForP"];
    next.BoardID=[standerId valueForKeyPath:@"ParseData.BoardID"];
    next.MediumID=[standerId valueForKeyPath:@"ParseData.MediumID"];
    next.StandardID=[standerId valueForKeyPath:@"ParseData.StandardID"];
    [self.navigationController pushViewController:next animated:YES];
    
    /*UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
     message:@"Coming Soon..."
     delegate:nil
     cancelButtonTitle:nil
     otherButtonTitles:@"OK",nil];
     [alert show];*/
}

- (IBAction)btn_REVISE:(id)sender {
   /* UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                    message:@"Coming Soon..."
                                                   delegate:nil
                                          cancelButtonTitle:nil
                                          otherButtonTitles:@"OK",nil];
    [alert show];
    */
    
    NSUserDefaults *standerId=[[NSUserDefaults alloc]init];
    SubjectReviseVC * next = [self.storyboard instantiateViewControllerWithIdentifier:@"SubjectReviseVC"];
    next.BoardID=[standerId valueForKeyPath:@"ParseData.BoardID"];
    next.MediumID=[standerId valueForKeyPath:@"ParseData.MediumID"];
    next.StandardID=[standerId valueForKeyPath:@"ParseData.StandardID"];
    [self.navigationController pushViewController:next animated:YES];
}

- (IBAction)btn_MYREPORT:(id)sender {
    /*  UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
     message:@"Coming Soon..."
     delegate:nil
     cancelButtonTitle:nil
     otherButtonTitles:@"OK",nil];
     [alert show];
     */
    
    MyReportsVC * next = [self.storyboard instantiateViewControllerWithIdentifier:@"MyReportsVC"];
    [self.navigationController pushViewController:next animated:YES];
    
}

- (IBAction)btn_INSTITUTE:(id)sender {
  /*  UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                    message:@"Coming Soon..."
                                                   delegate:nil
                                          cancelButtonTitle:nil
                                          otherButtonTitles:@"OK",nil];
    [alert show];*/
    
    
      SWRevealViewController *revealController = self.revealViewController;
    
    ICDashBoard * next = [self.storyboard instantiateViewControllerWithIdentifier:@"ICDashBoard"];
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:next];
    navigationController.navigationBar.hidden = YES;
    [revealController pushFrontViewController:navigationController animated:YES];
  //  [self.revealViewController pushFrontViewController:next animated:YES];

   // [self.navigationController pushViewController:next animated:YES];
    
}

- (IBAction)btn_SEARCHPAPER:(id)sender {
    /* UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
     message:@"Coming Soon..."
     delegate:nil
     cancelButtonTitle:nil
     otherButtonTitles:@"OK",nil];
     [alert show];*/
    
    SearchPaperMainVC * next = [self.storyboard instantiateViewControllerWithIdentifier:@"SearchPaperMainVC"];
    [self.navigationController pushViewController:next animated:YES];
    
    
}


-(void)setShadow :(UIView*)vview
{
    vview.layer.shadowRadius  = 2.5f;
    vview.layer.shadowColor   = [UIColor colorWithRed:176.f/255.f green:199.f/255.f blue:226.f/255.f alpha:1.f].CGColor;
    vview.layer.shadowOffset  = CGSizeMake(0.0f, 0.0f);
    vview.layer.shadowOpacity = 0.7f;
    vview.layer.masksToBounds = NO;
    
    UIEdgeInsets shadowInsets     = UIEdgeInsetsMake(0, 0, -1.5f, 0);
    UIBezierPath *shadowPath      = [UIBezierPath bezierPathWithRect:UIEdgeInsetsInsetRect(vview.bounds, shadowInsets)];
    vview.layer.shadowPath    = shadowPath.CGPath;
    
}
-(void)shareAppp
{
    NSString *str;
    str = [NSString stringWithFormat:@"https://itunes.apple.com/us/app/apple-store/id1507962509?mt=8"];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
    
}


- (IBAction)menu_click:(UIButton *)sender {
    
    SWRevealViewController *revealViewController = [self revealViewController];
       //self.revealViewController.rearViewRevealWidth = 145;
       [revealViewController setDelegate:self];
       
       [revealViewController panGestureRecognizer];
          [revealViewController tapGestureRecognizer];
       
       [_btn_menu addTarget:revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
}
@end
