//
//  SignupActivationKeyVC.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 2/23/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TextFieldValidator.h"


@interface SignupActivationKeyVC : UIViewController
{
    BOOL isInternet;
}
@property (retain , nonatomic)NSMutableDictionary *dictdata;
//Outlet
@property (strong, nonatomic) IBOutlet UIView *vw_activkey;
@property (strong, nonatomic) IBOutlet UIView *vwuserid;
@property (strong, nonatomic) IBOutlet UIImageView *img_key_icon;
@property (strong, nonatomic) IBOutlet UIButton *btn_next_b;
@property (strong, nonatomic) IBOutlet UIButton *btn_free_b;
@property (strong, nonatomic) IBOutlet UIView *vw_mob_icon;
@property (strong, nonatomic) IBOutlet TextFieldValidator *txt_akey;


//Action
- (IBAction)btn_FREE:(id)sender;
- (IBAction)btn_NEXT:(id)sender;
- (IBAction)btn_BACK:(id)sender;



@end
