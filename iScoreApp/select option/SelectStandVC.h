//
//  SelectStandVC.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 2/27/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectStandVC : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource>
{
    UIView *picker;
    NSMutableArray* marrSelect;
    
    int selectedIndex;
    
    NSMutableArray* stdrd;
    NSMutableArray* stdrdID;
}
@property (retain , nonatomic)NSMutableDictionary *dictdata;
//Outlet
@property (strong, nonatomic) IBOutlet UICollectionView *colle_view;
@property (strong, nonatomic) IBOutlet UIButton *btn_priv;
@property (strong, nonatomic) IBOutlet UIButton *btn_next;

//Action
- (IBAction)btn_NEXT_A:(id)sender;
- (IBAction)btn_PRIV_A:(id)sender;



@end
