//
//  SelectMediumVC.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 2/27/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectMediumVC : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    NSMutableArray* marrSelect;
    NSMutableArray* cells_Array;
    
    int selectedIndex;
    int status;
    
    NSMutableArray* mboard;
    NSMutableArray* mboardID;
    
}
@property (strong, nonatomic) IBOutlet UITableView *tbl_medium;

@property (retain , nonatomic)NSMutableDictionary *dictdata;
//Outlet
@property (strong, nonatomic) IBOutlet UITableView *tableview;
@property (strong, nonatomic) IBOutlet UIButton *btn_arr_priv;
@property (strong, nonatomic) IBOutlet UIButton *btn_arr_next;

//Action
- (IBAction)btn_ARR_NEXT_A:(id)sender;
- (IBAction)btn_ARR_PRIV_A:(id)sender;


@end
