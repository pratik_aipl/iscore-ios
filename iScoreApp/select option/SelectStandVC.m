//
//  SelectStandVC.m
//  iScoreApp
//
//  Created by ADMIN-Khushal on 2/27/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "SelectStandVC.h"
#import "SelectStandCell.h"
#import "ConfirmRegisVC.h"
#import <AFNetworking/AFNetworking.h>
#import "ApplicationConst.h"


@interface SelectStandVC ()

@end

@implementation SelectStandVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    stdrd = [[NSMutableArray alloc] initWithObjects:@"8th",@"10th", nil];

     marrSelect = [[NSMutableArray alloc] initWithObjects:@"0", nil];
    
    
    _btn_priv.layer.cornerRadius=_btn_priv.frame.size.height/2;
    _btn_next.layer.cornerRadius=_btn_next.frame.size.height/2;
    
    [self CallSelectStand];
}

-(void)CallSelectStand
{
    [APP_DELEGATE showLoadingView:@""];
    
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    [AddPost setValue:[_dictdata valueForKey:@"MediumID"] forKey:@"medium_id"];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    [manager GET:[NSString stringWithFormat:@"%@getStandards",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject){
        NSLog(@"JSON: %@", responseObject);
        
        
        if ([[responseObject valueForKey:@"status"] intValue]==1) {
            responseObject=[responseObject valueForKey:@"data"];
            stdrd=[[NSMutableArray alloc] init];
            stdrdID=[[NSMutableArray alloc]init];
            
            
            for (int i=0; i<[responseObject count]; i++) {
                [stdrd addObject:[[responseObject objectAtIndex:i]valueForKey:@"StandardName"]];
                [stdrdID addObject:[[responseObject objectAtIndex:i]valueForKey:@"StandardID"]];
            }
            
            [_colle_view reloadData];
        }
        
        [APP_DELEGATE hideLoadingView];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {

        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];
        
    }];
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return stdrd.count;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"Cell";
    SelectStandCell *cell = (SelectStandCell *)[collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    picker = [[UIView alloc] initWithFrame:CGRectMake(10, 10, cell.frame.size.width-20, cell.frame.size.height-20)];
    picker.tag = indexPath.row;
   
    cell.lbl_stand.text=[NSString stringWithFormat:@"%@",[stdrd objectAtIndex:indexPath.row] ];
    
    if ([[NSString stringWithFormat:@"%ld",(long)picker.tag] isEqualToString:[marrSelect objectAtIndex:indexPath.section]]) {
        picker.backgroundColor=[UIColor clearColor];
        picker.layer.borderWidth=1;
        picker.layer.borderColor=[UIColor redColor].CGColor;
        picker.hidden = NO;
        [cell addSubview: picker];
    }else{
        picker.backgroundColor=[UIColor clearColor];
        picker.layer.borderWidth=1;
        picker.layer.borderColor=[UIColor whiteColor].CGColor;
        picker.hidden = NO;
        [cell addSubview: picker];
        
    }
   

    return cell;
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath

{
    SelectStandCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
        NSLog(@"didSelectRowAtIndexPath");
    
    
    [marrSelect replaceObjectAtIndex:indexPath.section withObject:[NSString stringWithFormat:@"%ld",(long)indexPath.row]];
    selectedIndex = (int)indexPath.row;
    NSLog(@"%d",selectedIndex);
 
    [self.colle_view reloadData];
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btn_NEXT_A:(id)sender {
    NSString * value = [NSString stringWithFormat:@"%@",[stdrd objectAtIndex:selectedIndex]];
    NSLog( @"%@",value);
    [_dictdata setValue:[stdrd objectAtIndex:selectedIndex] forKey:@"StandardName"];
    [_dictdata setValue:[stdrdID objectAtIndex:selectedIndex] forKey:@"StandardID"];
    
    ConfirmRegisVC * next = [self.storyboard instantiateViewControllerWithIdentifier:@"ConfirmRegisVC"];
    next.dictdata=_dictdata;
    [self.navigationController pushViewController:next animated:YES];
    
}

- (IBAction)btn_PRIV_A:(id)sender {
    [self.navigationController popViewControllerAnimated:NO];
}
@end
