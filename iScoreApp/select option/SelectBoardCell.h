//
//  SelectBoardCell.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 2/26/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectBoardCell : UITableViewCell

//Outlets
@property (strong, nonatomic) IBOutlet UIButton *btn_checkBtn;
@property (strong, nonatomic) IBOutlet UILabel *lbl_name;

//Action

@end
