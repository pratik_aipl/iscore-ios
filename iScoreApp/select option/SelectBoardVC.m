//
//  SelectBoardVC.m
//  iScoreApp
//
//  Created by ADMIN-Khushal on 2/26/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "SelectBoardVC.h"
#import "SelectBoardCell.h"
#import "SelectMediumVC.h"
#import <AFNetworking/AFNetworking.h>
#import "ApplicationConst.h"


@interface SelectBoardVC ()
//@property (nonatomic, strong) UITableView *tableView;
@end

@implementation SelectBoardVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    board = [[NSMutableArray alloc] initWithObjects:@"Maharashtra State Board (MHSB)",@"Gujarat State Board (GSB)", nil];
    
    marrSelect = [[NSMutableArray alloc] initWithObjects:@"0", nil];
    
    
    _btn_arr_priv.layer.cornerRadius=_btn_arr_priv.frame.size.height/2;
    _btn_arr_next.layer.cornerRadius=_btn_arr_next.frame.size.height/2;
    
    NSLog(@"Dict Data --- %@",_dictdata);
    

    [self CallSelectBoard];
}

-(void)CallSelectBoard
{
   [APP_DELEGATE showLoadingView:@""];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    [manager POST:[NSString stringWithFormat:@"%@getBoards",BaseURLAPI] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        if ([[responseObject valueForKey:@"status"] intValue]==1) {
            responseObject =[responseObject valueForKey:@"data"];
            NSLog(@"-- %@",responseObject);
            board=[[NSMutableArray alloc]init];
            boardID=[[NSMutableArray alloc]init];
            for (int i=0; i<[responseObject count]; i++) {
                [board addObject:[[responseObject objectAtIndex:i]valueForKey:@"BoardFullName"]];
                [boardID addObject:[[responseObject objectAtIndex:i]valueForKey:@"BoardID"]];
                
            }
            [_tbl_board reloadData];
        }
        
      [APP_DELEGATE hideLoadingView];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [APP_DELEGATE hideLoadingView];
        NSLog(@"Error: %@", error);
    }];

    
    
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark TableViewDelegate

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    return board.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView
        cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Cell";
    SelectBoardCell *cell =
    [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc]
                initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:cellIdentifier owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
    cell.lbl_name.text=[NSString stringWithFormat:@"%@",[board objectAtIndex:indexPath.row]];
    cell.btn_checkBtn.userInteractionEnabled = NO;
     [cell.btn_checkBtn setSelected:false];
    if ([[marrSelect objectAtIndex:indexPath.section] isEqualToString:[NSString stringWithFormat:@"%ld",(long)indexPath.row]])
    {
        [cell.btn_checkBtn setSelected:true];
    }
        [cells_Array addObject:cell];
        return cell;
    
}

- (void)checkBoxClicked:(id)sender event:(id)event
{
    NSSet *touches = [event allTouches];
    UITouch *touch = [touches anyObject];
    CGPoint currentTouchPosition = [touch locationInView:_tbl_board]; //here tv is TableView Object
    NSIndexPath *indexPath1 = [_tbl_board indexPathForRowAtPoint: currentTouchPosition];
    
    NSLog(@"value of indePath.section %ld ,indexPath.row %d",(long)indexPath1.section,indexPath1.row);
    
    UIButton *button=(UIButton *) sender;
    NSIndexPath *indexpath = [NSIndexPath indexPathForRow:indexPath1.row inSection:indexPath1.section];
    SelectBoardCell *tappedCell = (SelectBoardCell*)[_tbl_board cellForRowAtIndexPath:indexpath];
    
    if( [[tappedCell.btn_checkBtn imageForState:UIControlStateNormal] isEqual:[UIImage imageNamed:@"unselect.png"]])
    {
        [tappedCell.btn_checkBtn setImage:[UIImage imageNamed:@"radio-unselect.png"] forState:UIControlStateNormal];
    }
    else
    {
        [tappedCell.btn_checkBtn setImage:[UIImage imageNamed:@"radio-select.png"] forState:UIControlStateSelected];
    }
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath* )indexPath
{

    SelectBoardCell *cellView = [_tbl_board cellForRowAtIndexPath:indexPath];
    
    
    [marrSelect replaceObjectAtIndex:indexPath.section withObject:[NSString stringWithFormat:@"%ld",(long)indexPath.row]];
    
    selectedIndex = (int)indexPath.row;
    NSLog(@"%d",selectedIndex);
    
    
    
    [tableView reloadData];
}
- (IBAction)myButtonClicked:(id)sender{
    UIButton *btn=sender;
    if(btn.selected == YES){
        btn.selected = NO;
    }else{
        btn.selected = YES;
    }
    NSLog(@"button was clicked for the following row %li", (long)btn.tag);
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
        return 50.0f;
}
-(CGFloat)heightForText:(NSString *)text
{
    NSInteger MAX_HEIGHT = 2000;
    UITextView * textView = [[UITextView alloc] initWithFrame: CGRectMake(0, 0, self.view.frame.size.width, MAX_HEIGHT+150)];
    textView.text = text;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        textView.font = [UIFont fontWithName:@"Roboto-Light" size:22.0];
    }
    else
    {
        textView.font = [UIFont fontWithName:@"Roboto-Light" size:12.0];
    }
    [textView sizeToFit];
    
    return textView.frame.size.height;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 1.0f;
}
-(void)textFieldDidEndEditing:(UITextField *)textField{
        [marrSelect replaceObjectAtIndex:textField.tag withObject:textField.text];
        NSLog(@"marrSelect :: %@",marrSelect);
}

- (IBAction)btn_ARR_NEXT:(id)sender {
    
    NSString * value = [NSString stringWithFormat:@"%@",[board objectAtIndex:selectedIndex]];
    NSLog( @"%@",value);
    //_dictdata=[[NSMutableDictionary alloc]init];
    
    [_dictdata setValue:value forKey:@"BoardName"];
    [_dictdata setValue:[boardID objectAtIndex:selectedIndex] forKey:@"BoardID"];
    
    [self CallSelectMedium];
    
}

- (IBAction)btn_ARR_PRIV:(id)sender {
    [self.navigationController popViewControllerAnimated:NO];
}

/////////////////////////NEXT PAGE API////////
-(void)CallSelectMedium
{
    [APP_DELEGATE showLoadingView:@""];
    
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    [AddPost setValue:[_dictdata valueForKey:@"BoardID"] forKey:@"board_id"];
    //[AddPost setValue:@"1" forKey:@"board_id"];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    [manager GET:[NSString stringWithFormat:@"%@getMediums",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject){
        NSLog(@"JSON: %@", responseObject);
        status=[[responseObject valueForKey:@"status"] intValue];
        
        if (status==1) {
            SelectMediumVC * next = [self.storyboard instantiateViewControllerWithIdentifier:@"SelectMediumVC"];
            next.dictdata=_dictdata;
            [self.navigationController pushViewController:next animated:NO];
        }
        
        [APP_DELEGATE hideLoadingView];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [APP_DELEGATE hideLoadingView];
        NSLog(@"Error: %@", error);
    }];
}












@end
