//
//  SelectMediumVC.m
//  iScoreApp
//
//  Created by ADMIN-Khushal on 2/27/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "SelectMediumVC.h"
#import "SelectMediumCell.h"
#import "SelectStandVC.h"
#import <AFNetworking/AFNetworking.h>
#import "ApplicationConst.h"


@interface SelectMediumVC ()
@property (nonatomic, strong) UITableView *tableView;
@end

@implementation SelectMediumVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    mboard = [[NSMutableArray alloc] initWithObjects:@"English",@"Hindi", nil];
    marrSelect = [[NSMutableArray alloc] initWithObjects:@"0", nil];
    
    
    _btn_arr_priv.layer.cornerRadius=_btn_arr_priv.frame.size.height/2;
    _btn_arr_next.layer.cornerRadius=_btn_arr_next.frame.size.height/2;
    
    
    NSLog(@"dict data --%@",_dictdata);
    
    
    [self CallSelectMedium];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)CallSelectMedium
{
    [APP_DELEGATE showLoadingView:@""];
    
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    [AddPost setValue:[_dictdata valueForKey:@"BoardID"] forKey:@"board_id"];
    //[AddPost setValue:@"1" forKey:@"board_id"];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    [manager GET:[NSString stringWithFormat:@"%@getMediums",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject){
         NSLog(@"JSON: %@", responseObject);
        status=[[responseObject valueForKey:@"status"] intValue];
        
        if ([[responseObject valueForKey:@"status"] integerValue]==1) {
            responseObject=[responseObject valueForKey:@"data"];
            
            mboard=[[NSMutableArray alloc] init];
            mboardID=[[NSMutableArray alloc]init];
            
            for (int i=0; i<[responseObject count]; i++) {
                [mboard addObject:[[responseObject objectAtIndex:i]valueForKey:@"MediumName"]];
                [mboardID addObject:[[responseObject objectAtIndex:i]valueForKey:@"MediumID"]];
            }
            [_tbl_medium reloadData];
        }
        
        [APP_DELEGATE hideLoadingView];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [APP_DELEGATE hideLoadingView];
        NSLog(@"Error: %@", error);
    }];
}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark TableViewDelegate

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    
    return mboard.count;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Cell";
    SelectMediumCell *cell =
    [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc]
                initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:cellIdentifier owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    cell.lbl_name.text=[NSString stringWithFormat:@"%@",[mboard objectAtIndex:indexPath.row] ];
    cell.btn_checkBtn.userInteractionEnabled = NO;
    [cell.btn_checkBtn setSelected:false];
    if ([[marrSelect objectAtIndex:indexPath.section] isEqualToString:[NSString stringWithFormat:@"%ld",(long)indexPath.row]])
    {
        [cell.btn_checkBtn setSelected:true];
    }
    [cells_Array addObject:cell];
    return cell;
    
}

- (void)checkBoxClicked:(id)sender event:(id)event
{
    NSSet *touches = [event allTouches];
    UITouch *touch = [touches anyObject];
    CGPoint currentTouchPosition = [touch locationInView:self.tableview]; //here tv is TableView Object
    NSIndexPath *indexPath1 = [self.tableview indexPathForRowAtPoint: currentTouchPosition];
    
    NSLog(@"value of indePath.section %ld ,indexPath.row %d",(long)indexPath1.section,indexPath1.row);
    
    UIButton *button=(UIButton *) sender;
    NSIndexPath *indexpath = [NSIndexPath indexPathForRow:indexPath1.row inSection:indexPath1.section];
    SelectMediumCell *tappedCell = (SelectMediumCell*)[_tableView cellForRowAtIndexPath:indexpath];
    
    if( [[tappedCell.btn_checkBtn imageForState:UIControlStateNormal] isEqual:[UIImage imageNamed:@"unselect.png"]])
    {
        [tappedCell.btn_checkBtn setImage:[UIImage imageNamed:@"radio-unselect.png"] forState:UIControlStateNormal];
    }
    else
    {
        [tappedCell.btn_checkBtn setImage:[UIImage imageNamed:@"radio-select.png"] forState:UIControlStateSelected];
    }
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath* )indexPath
{
    
    SelectMediumCell *cellView = [self.tableView cellForRowAtIndexPath:indexPath];
    [marrSelect replaceObjectAtIndex:indexPath.section withObject:[NSString stringWithFormat:@"%ld",(long)indexPath.row]];
    selectedIndex = (int)indexPath.row;
    NSLog(@"%d",selectedIndex);
    
    [tableView reloadData];
}
- (IBAction)myButtonClicked:(id)sender{
    UIButton *btn=sender;
    if(btn.selected == YES){
        btn.selected = NO;
    }else{
        btn.selected = YES;
    }
    NSLog(@"button was clicked for the following row %li", (long)btn.tag);
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50.0f;
}
-(CGFloat)heightForText:(NSString *)text
{
    NSInteger MAX_HEIGHT = 2000;
    UITextView * textView = [[UITextView alloc] initWithFrame: CGRectMake(0, 0, self.view.frame.size.width, MAX_HEIGHT+150)];
    textView.text = text;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        textView.font = [UIFont fontWithName:@"Roboto-Light" size:22.0];
    }
    else
    {
        textView.font = [UIFont fontWithName:@"Roboto-Light" size:12.0];
    }
    [textView sizeToFit];
    
    return textView.frame.size.height;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 1.0f;
}
-(void)textFieldDidEndEditing:(UITextField *)textField{
    
    
    [marrSelect replaceObjectAtIndex:textField.tag withObject:textField.text];
    
    NSLog(@"marrSelect :: %@",marrSelect);
    
}


- (IBAction)btn_ARR_NEXT_A:(id)sender {
    NSString * value = [NSString stringWithFormat:@"%@",[mboard objectAtIndex:selectedIndex]];
    NSLog( @"%@",value);
    [_dictdata setValue:value forKey:@"MediumName"];
    [_dictdata setValue:[mboardID objectAtIndex:selectedIndex] forKey:@"MediumID"];
    
    [self CallSelectStand];
    
    
    
}

- (IBAction)btn_ARR_PRIV_A:(id)sender {
    [self.navigationController popViewControllerAnimated:NO];
}

///////////////////// NEXT PAGE API /////////////////


-(void)CallSelectStand
{
    [APP_DELEGATE showLoadingView:@""];
    
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    [AddPost setValue:[_dictdata valueForKey:@"MediumID"] forKey:@"medium_id"];
    //[AddPost setValue:@"1" forKey:@"medium_id"];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    [manager GET:[NSString stringWithFormat:@"%@getStandards",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject){
        NSLog(@"JSON: %@", responseObject);
        
        status =[[responseObject valueForKey:@"status"] intValue];
        
        if (status==1) {
            
            SelectStandVC * next = [self.storyboard instantiateViewControllerWithIdentifier:@"SelectStandVC"];
            next.dictdata=_dictdata;
            [self.navigationController pushViewController:next animated:NO];
            
        }
        
        [APP_DELEGATE hideLoadingView];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];
        
    }];
    
}




@end
