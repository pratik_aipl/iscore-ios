//
//  SelectBoardCell.m
//  iScoreApp
//
//  Created by ADMIN-Khushal on 2/26/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "SelectBoardCell.h"

@implementation SelectBoardCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
