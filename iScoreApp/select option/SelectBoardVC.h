//
//  SelectBoardVC.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 2/26/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectBoardVC : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
    NSMutableArray* marrSelect;
    NSMutableArray* cells_Array;
    
    int selectedIndex;
    int status;
    
    NSMutableArray* board;
    NSMutableArray* boardID;
}
@property (strong, nonatomic) IBOutlet UITableView *tbl_board;
@property (retain , nonatomic)NSMutableDictionary *dictdata;
//@property (strong, nonatomic) IBOutlet UITableView *tableview;
@property (strong, nonatomic) IBOutlet UIButton *btn_arr_priv;
@property (strong, nonatomic) IBOutlet UIButton *btn_arr_next;

//Action
- (IBAction)btn_ARR_NEXT:(id)sender;
- (IBAction)btn_ARR_PRIV:(id)sender;




@end
