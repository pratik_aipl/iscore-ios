//
//  TestSummeryVC.m
//  iScoreApp
//
//  Created by ADMIN-Khushal on 3/19/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "TestSummeryVC.h"
#import "MXSegmentedPager.h"
#import "TestReportVC.h"
#import "UIColor+CL.h"
#import <WebKit/WebKit.h>

@interface TestSummeryVC ()<MXSegmentedPagerDelegate, MXSegmentedPagerDataSource,UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) MXSegmentedPager  * segmentedPager;
@property (nonatomic, strong) UITableView       * tableView;
@property (nonatomic, strong) WKWebView         * webView;
@property (nonatomic, strong) UITextView        * textView;

@end

@implementation TestSummeryVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.view.backgroundColor = UIColor.whiteColor;
    _segmentedPager.layer.backgroundColor=[UIColor clearColor].CGColor;
    [self.vw_main addSubview:self.segmentedPager];
    self.segmentedPager.segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    self.segmentedPager.segmentedControl.selectionIndicatorColor = [UIColor colorWithHex:0xff69b4];
    self.segmentedPager.segmentedControl.selectionStyle = HMSegmentedControlSelectionStyleFullWidthStripe;
    self.segmentedPager.segmentedControl.selectedTitleTextAttributes = @{NSForegroundColorAttributeName : [UIColor colorWithHex:0xff69b4]};
    
    self.segmentedPager.pager.gutterWidth = 20;
    
    
    [self.segmentedPager.pager registerClass:[UITextView class] forPageReuseIdentifier:@"TextPage"];
    
    [self CreateScrollview];
}

-(void)CreateScrollview
{
    UIScrollView *ScrView = [[UIScrollView alloc]initWithFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height)];
    ScrView.showsVerticalScrollIndicator=YES;
    [ScrView setBackgroundColor:[UIColor yellowColor]];
    [ScrView setScrollEnabled:YES];
    [ScrView setContentSize:CGSizeMake(0, 850)];
    [ScrView setShowsHorizontalScrollIndicator:NO];
    [ScrView setShowsVerticalScrollIndicator:NO];
    [self.view addSubview:ScrView];
    
    [ScrView addSubview:_vw_header];
    [ScrView addSubview:_vw_main];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}

- (void)viewWillLayoutSubviews {
    self.segmentedPager.frame = (CGRect){
        .origin.x       = 0.f,
        .origin.y       = 0.f,
        .size.width     = self.view.frame.size.width,
        .size.height    = self.view.frame.size.height - 150.f
    };
    [super viewWillLayoutSubviews];
}
#pragma -mark Properties

- (MXSegmentedPager *)segmentedPager {
    if (!_segmentedPager) {
        
        // Set a segmented pager
        _segmentedPager = [[MXSegmentedPager alloc] init];
        _segmentedPager.delegate    = self;
        _segmentedPager.dataSource  = self;
    }
    return _segmentedPager;
}

- (UITableView *)tableView {
    if (!_tableView) {
        //Add a table page
        _tableView = [[UITableView alloc] init];
        _tableView.delegate = self;
        _tableView.dataSource = self;
    }
    return _tableView;
}

- (WKWebView *)webView {
    if (!_webView) {
        // Add a web page
        _webView = [[WKWebView alloc] init];
        NSString *strURL = @"http://nshipster.com/";
        NSURL *url = [NSURL URLWithString:strURL];
        NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
        [_webView loadRequest:urlRequest];
    }
    return _webView;
}

- (UITextView *)textView {
    if (!_textView) {
        // Add a text page
        _textView = [[UITextView alloc] init];
        /*  NSString *filePath = [[NSBundle mainBundle]pathForResource:@"LongText" ofType:@"txt"];
         _textView.text = [[NSString alloc]initWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
         */
        _textView.text=[NSString stringWithFormat:@"PAGE "];
        
        
    }
    return _textView;
}

#pragma -mark <MXSegmentedPagerDelegate>

- (void)segmentedPager:(MXSegmentedPager *)segmentedPager didSelectViewWithTitle:(NSString *)title {
    NSLog(@"%@ page selected.", title);
}

#pragma -mark <MXSegmentedPagerDataSource>

- (NSInteger)numberOfPagesInSegmentedPager:(MXSegmentedPager *)segmentedPager {
    return 3;
}

- (NSString *)segmentedPager:(MXSegmentedPager *)segmentedPager titleForSectionAtIndex:(NSInteger)index {
    if (index < 3) {
        return [@[@"Correct", @"Incorrect", @"Not Appeard Incorrect Correct"] objectAtIndex:index];
    }
    return [NSString stringWithFormat:@"%li", (long) index];
}

- (UIView *)segmentedPager:(MXSegmentedPager *)segmentedPager viewForPageAtIndex:(NSInteger)index {
    if (index < 3) {
        return @[self.tableView, self.tableView, self.tableView][index];
    }
    
    //Dequeue reusable page
    UITextView *page = [segmentedPager.pager dequeueReusablePageWithIdentifier:@"TextPage"];
    NSString *filePath = [[NSBundle mainBundle]pathForResource:@"LongText" ofType:@"txt"];
    page.text = [[NSString alloc]initWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
    
    return page;
}

#pragma -mark <UITableViewDelegate>

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSInteger index = (indexPath.row % 2) + 1;
    [self.segmentedPager.pager showPageAtIndex:index animated:YES];
}

#pragma -mark <UITableViewDataSource>

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 5;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    cell.textLabel.text = (indexPath.row % 2)? @"Text": @"Web";
    
    return cell;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btn_BACK:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
    //[self dismissViewControllerAnimated:NO completion:nil];
    //[self.navigationController dismissViewControllerAnimated:YES completion:nil];
}
@end
