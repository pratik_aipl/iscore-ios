//
//  CCTTSummeryVC.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 10/1/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MZTimerLabel.h"
#import "MCQCCTModel.h"


@interface CCTTSummeryVC : UIViewController
{
    int SelectedTab;
    
    
    NSMutableArray *arrResponce,*arrOption,*marrFlag;
    
    NSMutableArray *arrCorrect,*arrIncorrect,*arrnotAppered;
    
    
    NSString * strQuestion1 , * strOption11 , * strOption21 ,* strOption31 , * strOption41;
    int ans,ans1,ans2;
}

@property (retain , nonatomic)NSString *cctPaperID;
@property (retain , nonatomic)NSMutableArray<MCQCCTModel *> *mcqCCTArray;


//Outlet
@property (strong, nonatomic) IBOutlet UIView *vw_header;

@property (strong, nonatomic) IBOutlet UILabel *lbl_l1;
@property (strong, nonatomic) IBOutlet UILabel *lbl_l2;
@property (strong, nonatomic) IBOutlet UILabel *lbl_l3;


@property (strong, nonatomic) IBOutlet UILabel *lbl_correct;
@property (strong, nonatomic) IBOutlet UILabel *lbl_incorrect;
@property (strong, nonatomic) IBOutlet UILabel *lbl_notapper;


- (IBAction)btn_BACK:(id)sender;

@end
