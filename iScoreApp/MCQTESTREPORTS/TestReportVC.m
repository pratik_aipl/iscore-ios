//
//  TestReportVC.m
//  iScoreApp
//
//  Created by ADMIN-Khushal on 3/19/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "TestReportVC.h"
#import "UIColor+CL.h"
#import "TestSummeryVC.h"
#import "TSummeryVC.h"
#import "HomeVC.h"
#import "SubjectVC.h"
#import "StartTestVC.h"
#import "FMResultSet.h"
#import "MyModel.h"
#import "Constant.h"
#import "SharedData.h"
#import "AppDelegate.h"
//#import "Dashboard.h"
#import "MyModel.h"
#import "API.h"
#import "TestReportVC.h"
#import "MyModel.h"
#import "UIColor+CL.h"
#import "HomeVC.h"
#import "ApplicationConst.h"

#import "CCTTSummeryVC.h"




@interface TestReportVC ()

@end

@implementation TestReportVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    if ([_isCCT isEqualToString:@"1"]) {
        _img_background.clipsToBounds=YES;
        _img_background.layer.cornerRadius=_img_background.frame.size.height/2;
        _img_background.layer.borderWidth=2;
        _img_background.layer.borderColor=[UIColor colorWithHex:0x016CA4].CGColor;
        
        _vw_score1.layer.borderWidth=0.7;
        _vw_score1.layer.borderColor=[UIColor colorWithHex:0xBBBBBB].CGColor;
        _vw_score2.layer.borderWidth=0.7;
        _vw_score2.layer.borderColor=[UIColor colorWithHex:0xBBBBBB].CGColor;
        _vw_score3.layer.borderWidth=0.7;
        _vw_score3.layer.borderColor=[UIColor colorWithHex:0xBBBBBB].CGColor;
        _vw_score4.layer.borderWidth=0.7;
        _vw_score4.layer.borderColor=[UIColor colorWithHex:0xBBBBBB].CGColor;
        
        _img_header.alpha=0.0;
        
        _lbl_tottime.layer.cornerRadius=10;
        _lbl_avgtime.layer.cornerRadius=13;
        
        //[self CallGetAccuracy];
        
        [self CallCCTGetAccuracy];
        
        //question size represents width of one question on progress bar. You just divide the width of the entire bar with number of possible questions (max)
        
        
        
        //_scroll_vw.contentSize=CGSizeMake(_scroll_vw.frame.size.width,1100);
        self.scroll_vw.bounces = false;
        self.scroll_vw.showsVerticalScrollIndicator=NO;
        
        CGSize scrollableSize = CGSizeMake(320, 1100);
        [_scroll_vw setContentSize:scrollableSize];
        
        
        _scroll_vw.delegate = self;
        _btn_startnew.layer.cornerRadius=3;
        
        
        ////////////----------------------------
        NSLog(@"tempNotapper %@",_tempNotapper);
        NSLog(@"tempIncorrect %@",_tempIncorrect);
        NSLog(@"tempCorrect %@",_tempCorrect);
        NSLog(@"_chapterList %@",_chapterList);
        NSLog(@"_TBStatus %@",_TBStatus);
        NSLog(@"_subjectId %@",_subjectId);
        NSLog(@"_subjectName %@",_subjectName);
        NSLog(@"_levelId %@",_levelId);
        
        
        
        
        
        
        
        
     /*
        _lbl_totcorrect.text=[NSString stringWithFormat:@"%lu Correct",(unsigned long)_tempCorrect.count];
        _lbl_totincorrect.text=[NSString stringWithFormat:@"%lu Incorrect",(unsigned long)_tempIncorrect.count];
        _lbl_totnotapper.text=[NSString stringWithFormat:@"%lu Not Appered",(unsigned long)_tempNotapper.count];
        
        _lbl_subjectnm.text=[NSString stringWithFormat:@"%@ Subject",_subjectName];
        _lbl_scoreoftest.text=[NSString stringWithFormat:@"Score of Test %lu",[_tempNotapper count]+[_tempIncorrect count]+[_tempCorrect count]];
        
        _lbl1_l0.text=[NSString stringWithFormat:@"%lu",(unsigned long)_tempCorrect.count];
        _lbl2_l0.text=[NSString stringWithFormat:@"%lu",[_tempNotapper count]+[_tempIncorrect count]+[_tempCorrect count]];
        [self LevelWiseCount];
        
        
        //============ WebView ==============
        NSString *subjAcc=[[NSString alloc]init];
        int totacc = 0;
        
        for (int i=0; i<arrAccu.count; i++) {
            if ([[[arrAccu valueForKey:@"SubjectID"] objectAtIndex:i]isEqualToString:[NSString stringWithFormat:@"%@",_subjectId]]) {
                subjAcc=[[arrAccu valueForKey:@"accuracy1"] objectAtIndex:i];
            }
            
            totacc+=[[[arrAccu valueForKey:@"accuracy1"] objectAtIndex:i] intValue];
        }
        
        
        
        
        
        _lbl_schoolmarks.text=[NSString stringWithFormat:@"Your previous year school result %@",[[NSUserDefaults standardUserDefaults] valueForKey:@"PYear"]];
        
        
        float precentage = (100 * [_lbl1_l0.text floatValue])/[_lbl2_l0.text floatValue];
        
        NSString *progressPath = [[NSBundle mainBundle] pathForResource:@"progress" ofType:@"html"];
        NSString* filePath=[NSString stringWithContentsOfFile:progressPath encoding:NSASCIIStringEncoding error:nil];
        
        filePath = [filePath stringByReplacingOccurrencesOfString:@"0011" withString:[NSString stringWithFormat:@"%.0f",precentage]];
        
        filePath = [filePath stringByReplacingOccurrencesOfString:@"0012" withString:[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"CYear"]]];
        NSData *htmlData = [NSKeyedArchiver archivedDataWithRootObject:filePath];
        [filePath writeToFile:progressPath atomically:YES encoding:NSASCIIStringEncoding error:nil];
        
        NSLog(@"====%@",filePath);
        
        //NSData *htmlData = [NSData dataWithContentsOfFile:filePath];
        if (htmlData) {
            NSBundle *bundle = [NSBundle mainBundle];
            NSString *path = [bundle bundlePath];
            NSString *fullPath = [NSBundle pathForResource:@"progress" ofType:@"html" inDirectory:path];
            [_web_goalview loadRequest:[NSURLRequest requestWithURL:[NSURL fileURLWithPath:fullPath]]];
            _web_goalview.userInteractionEnabled = NO;
        }
        
        
        
        //=======SUBJECT STASTIC
        
        _lbl_comp_with.text=[NSString stringWithFormat:@"Comparison With %@ Subject",_subjectName];
        _lbl_all_sub_acc.text=[NSString stringWithFormat:@"Your Accuracy For All Subject : %lu%%",totacc/arrAccu.count];
        
        
        
        NSString *progressPath1 = [[NSBundle mainBundle] pathForResource:@"progress1" ofType:@"html"];
        NSString* filePath1=[NSString stringWithContentsOfFile:progressPath1 encoding:NSASCIIStringEncoding error:nil];
        
        filePath1 = [filePath1 stringByReplacingOccurrencesOfString:@"0011" withString:[NSString stringWithFormat:@"%.0f",precentage]];
        
        filePath1 = [filePath1 stringByReplacingOccurrencesOfString:@"0012" withString:[NSString stringWithFormat:@"%@",subjAcc]];
        
        filePath1 = [filePath1 stringByReplacingOccurrencesOfString:@"MATHss" withString:[NSString stringWithFormat:@"%@%@%%",_subjectName,subjAcc]];
        
        NSData *htmlData1 = [NSKeyedArchiver archivedDataWithRootObject:filePath1];
        [filePath1 writeToFile:progressPath1 atomically:YES encoding:NSASCIIStringEncoding error:nil];
        
        NSLog(@"====%@",filePath1);
        
        if (htmlData1) {
            NSBundle *bundle = [NSBundle mainBundle];
            NSString *path = [bundle bundlePath];
            NSString *fullPath = [NSBundle pathForResource:@"progress1" ofType:@"html" inDirectory:path];
            [_web_sub_accu loadRequest:[NSURLRequest requestWithURL:[NSURL fileURLWithPath:fullPath]]];
            _web_sub_accu.userInteractionEnabled = NO;
        }
        
        /////Total Time
        //_lbl_tottime.text=[self formatTimeFromSeconds:[_TakanTime intValue]];
        _lbl_tottime.text=@"00:00";
        
        _lbl_tottime.text=[NSString stringWithFormat:@"%@",_TakanTime];
         
         */
    }
    else
    {
        _img_background.clipsToBounds=YES;
        _img_background.layer.cornerRadius=_img_background.frame.size.height/2;
        _img_background.layer.borderWidth=2;
        _img_background.layer.borderColor=[UIColor colorWithHex:0x016CA4].CGColor;
        
        _vw_score1.layer.borderWidth=0.7;
        _vw_score1.layer.borderColor=[UIColor colorWithHex:0xBBBBBB].CGColor;
        _vw_score2.layer.borderWidth=0.7;
        _vw_score2.layer.borderColor=[UIColor colorWithHex:0xBBBBBB].CGColor;
        _vw_score3.layer.borderWidth=0.7;
        _vw_score3.layer.borderColor=[UIColor colorWithHex:0xBBBBBB].CGColor;
        _vw_score4.layer.borderWidth=0.7;
        _vw_score4.layer.borderColor=[UIColor colorWithHex:0xBBBBBB].CGColor;
        
        _img_header.alpha=0.0;
        
        _lbl_tottime.layer.cornerRadius=10;
        _lbl_avgtime.layer.cornerRadius=13;
        
        [self CallGetAccuracy];
        
        
        //question size represents width of one question on progress bar. You just divide the width of the entire bar with number of possible questions (max)
        
        
        CGRect grayProgress = CGRectMake( 0, 0, self.view.frame.size.width-45, 10 );
        _num_possible_question.frame = grayProgress;
        
        CGRect redProgress = CGRectMake( 0, 0, self.view.frame.size.width-100, 10 );
        _redLabel.frame = redProgress;
        
        CGRect greenProgress = CGRectMake( 0, 0, self.view.frame.size.width-295, 10 );
        _grayLabel.frame = greenProgress;
        
        //INDICATOR
        CGRect grayProgress1 = CGRectMake( 8, 85, 555, 10 );
        _lbl_gray.frame = grayProgress1;
        
        CGRect redProgress1 = CGRectMake( 8, 85, 360, 10 );
        _lbl_blue.frame = redProgress1;
        
        CGRect greenProgress1 = CGRectMake( 8, 85, 65, 10 );
        _lbl_yellow.frame = greenProgress1;
        
        
        //_scroll_vw.contentSize=CGSizeMake(_scroll_vw.frame.size.width,1100);
        self.scroll_vw.bounces = false;
        self.scroll_vw.showsVerticalScrollIndicator=NO;
        
        CGSize scrollableSize = CGSizeMake(320, 1100);
        [_scroll_vw setContentSize:scrollableSize];
        
        
        _scroll_vw.delegate = self;
        _btn_startnew.layer.cornerRadius=3;
        
        
        ////////////----------------------------
        NSLog(@"tempNotapper %@",_tempNotapper);
        NSLog(@"tempIncorrect %@",_tempIncorrect);
        NSLog(@"tempCorrect %@",_tempCorrect);
        NSLog(@"_chapterList %@",_chapterList);
        NSLog(@"_TBStatus %@",_TBStatus);
        NSLog(@"_subjectId %@",_subjectId);
        NSLog(@"_subjectName %@",_subjectName);
        NSLog(@"_levelId %@",_levelId);
        
        _lbl_totcorrect.text=[NSString stringWithFormat:@"%lu Correct",(unsigned long)_tempCorrect.count];
        _lbl_totincorrect.text=[NSString stringWithFormat:@"%lu Incorrect",(unsigned long)_tempIncorrect.count];
        _lbl_totnotapper.text=[NSString stringWithFormat:@"%lu Not Appered",(unsigned long)_tempNotapper.count];
        
        _lbl_subjectnm.text=[NSString stringWithFormat:@"%@ Subject",_subjectName];
        _lbl_scoreoftest.text=[NSString stringWithFormat:@"Score of Test %lu",[_tempNotapper count]+[_tempIncorrect count]+[_tempCorrect count]];
        
        _lbl1_l0.text=[NSString stringWithFormat:@"%lu",(unsigned long)_tempCorrect.count];
        _lbl2_l0.text=[NSString stringWithFormat:@"%lu",[_tempNotapper count]+[_tempIncorrect count]+[_tempCorrect count]];
        [self LevelWiseCount];
        
        
        //============ WebView ==============
        NSString *subjAcc=[[NSString alloc]init];
        int totacc = 0;
        
        for (int i=0; i<arrAccu.count; i++) {
            if ([[[arrAccu valueForKey:@"SubjectID"] objectAtIndex:i]isEqualToString:[NSString stringWithFormat:@"%@",_subjectId]]) {
                subjAcc=[[arrAccu valueForKey:@"accuracy1"] objectAtIndex:i];
            }
            
            totacc+=[[[arrAccu valueForKey:@"accuracy1"] objectAtIndex:i] intValue];
        }
        
        
        
        
        
        _lbl_schoolmarks.text=[NSString stringWithFormat:@"Your previous year school result %@",[[NSUserDefaults standardUserDefaults] valueForKey:@"PYear"]];
        
        
        float precentage = (100 * [_lbl1_l0.text floatValue])/[_lbl2_l0.text floatValue];
        
        NSString *progressPath = [[NSBundle mainBundle] pathForResource:@"progress" ofType:@"html"];
        NSString* filePath=[NSString stringWithContentsOfFile:progressPath encoding:NSASCIIStringEncoding error:nil];
        
        filePath = [filePath stringByReplacingOccurrencesOfString:@"0011" withString:[NSString stringWithFormat:@"%.0f",precentage]];
        
        filePath = [filePath stringByReplacingOccurrencesOfString:@"0012" withString:[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"CYear"]]];
        NSData *htmlData = [NSKeyedArchiver archivedDataWithRootObject:filePath];
        [filePath writeToFile:progressPath atomically:YES encoding:NSASCIIStringEncoding error:nil];
        
        NSLog(@"====%@",filePath);
        
        //NSData *htmlData = [NSData dataWithContentsOfFile:filePath];
        if (htmlData) {
            NSBundle *bundle = [NSBundle mainBundle];
            NSString *path = [bundle bundlePath];
            NSString *fullPath = [NSBundle pathForResource:@"progress" ofType:@"html" inDirectory:path];
            _web_goalview.hidden=NO;
            [_web_goalview loadRequest:[NSURLRequest requestWithURL:[NSURL fileURLWithPath:fullPath]]];
            _web_goalview.userInteractionEnabled = NO;
        }
        
        
        
        //=======SUBJECT STASTIC
        
        _lbl_comp_with.text=[NSString stringWithFormat:@"Comparison With %@ Subject",_subjectName];
        _lbl_all_sub_acc.text=[NSString stringWithFormat:@"Your Accuracy For All Subject : %lu%%",totacc/arrAccu.count];
        
        
        
        NSString *progressPath1 = [[NSBundle mainBundle] pathForResource:@"progress1" ofType:@"html"];
        NSString* filePath1=[NSString stringWithContentsOfFile:progressPath1 encoding:NSASCIIStringEncoding error:nil];
        
        filePath1 = [filePath1 stringByReplacingOccurrencesOfString:@"0011" withString:[NSString stringWithFormat:@"%.0f",precentage]];
        
        filePath1 = [filePath1 stringByReplacingOccurrencesOfString:@"0012" withString:[NSString stringWithFormat:@"%@",subjAcc]];
        
        filePath1 = [filePath1 stringByReplacingOccurrencesOfString:@"MATHss" withString:[NSString stringWithFormat:@"%@%@%%",_subjectName,subjAcc]];
        
        NSData *htmlData1 = [NSKeyedArchiver archivedDataWithRootObject:filePath1];
        [filePath1 writeToFile:progressPath1 atomically:YES encoding:NSASCIIStringEncoding error:nil];
        
        NSLog(@"====%@",filePath1);
        
        if (htmlData1) {
            NSBundle *bundle = [NSBundle mainBundle];
            NSString *path = [bundle bundlePath];
            NSString *fullPath = [NSBundle pathForResource:@"progress1" ofType:@"html" inDirectory:path];
            [_web_sub_accu loadRequest:[NSURLRequest requestWithURL:[NSURL fileURLWithPath:fullPath]]];
            _web_sub_accu.userInteractionEnabled = NO;
        }
        
        /////Total Time
        //_lbl_tottime.text=[self formatTimeFromSeconds:[_TakanTime intValue]];
        _lbl_tottime.text=@"00:00";
        
        _lbl_tottime.text=[NSString stringWithFormat:@"%@",_TakanTime];
        _lbl_avgtime.text=[NSString stringWithFormat:@"%@",_avgTakanTime];
       // _lbl_avgtime.text
    }
    
    
    
    
}

-(void)CallCCTGetAccuracy
{
    [APP_DELEGATE showLoadingView:@""];
    
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    [AddPost setValue: [[NSUserDefaults standardUserDefaults] valueForKeyPath:@"ParseData.StudentID"] forKey:@"StudID"];//taken_test
    [AddPost setValue: _CCTPaperId forKey:@"PaperID"];
    
    NSLog(@"StudID %@",AddPost);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html",@"application/json",nil];
    //  manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html",@"application/json",nil];
    //%@get_student_class_dtl?",BaseURLAPI
    
    [manager GET:[NSString stringWithFormat:@"%@get-mcq-test-result?",ICAPIURL] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        
        if ([[responseObject valueForKey:@"status"] intValue]==1) {
            responseObject=[responseObject valueForKey:@"data"];
            
            
            NSLog(@"tempNotapper %@",_tempNotapper);
            NSLog(@"tempIncorrect %@",_tempIncorrect);
            NSLog(@"tempCorrect %@",_tempCorrect);
            NSLog(@"_chapterList %@",_chapterList);
            NSLog(@"_TBStatus %@",_TBStatus);
            NSLog(@"_subjectId %@",_subjectId);
            NSLog(@"_subjectName %@",_subjectName);
            NSLog(@"_levelId %@",_levelId);
            
            
            _lbl_totnotapper.text=[NSString stringWithFormat:@"%@ Not answerred",[responseObject valueForKey:@"TotalNotAppeared"]];
            _lbl_totincorrect.text=[NSString stringWithFormat:@"%@ Incorrect",[responseObject valueForKey:@"TotalIncorrectQue"]];
            _lbl_totcorrect.text=[NSString stringWithFormat:@"%@ Correct",[responseObject valueForKey:@"TotalRight"]];
            
            _lbl_subjectnm.text=[NSString stringWithFormat:@"%@ Subject",_subjectName];
            //TotalQquestion
            _lbl_scoreoftest.text=[NSString stringWithFormat:@"Score of Test %@",[responseObject valueForKey:@"TotalQquestion"]];
            
            _lbl1_l0.text=[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"TotalRight"]];
            _lbl2_l0.text=[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"TotalQquestion"]];
            
            _lbl1_l1.text=[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"total_right_level_1"]];
            _lbl2_l1.text=[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"total_level_1"]];
            
            _lbl1_l2.text=[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"total_right_level_2"]];
            _lbl2_l2.text=[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"total_level_2"]];
            
            _lbl1_l3.text=[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"total_right_level_3"]];
            _lbl2_l3.text=[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"total_level_3"]];
            
            
            _lbl_avgtime.text=[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"AvaragePerQuestionTime"]];
            
            
            float precentage = (100 * [_lbl1_l0.text floatValue])/[_lbl2_l0.text floatValue];
            
            NSString *progressPath = [[NSBundle mainBundle] pathForResource:@"progress" ofType:@"html"];
            NSString* filePath=[NSString stringWithContentsOfFile:progressPath encoding:NSASCIIStringEncoding error:nil];
            
            filePath = [filePath stringByReplacingOccurrencesOfString:@"0011" withString:[NSString stringWithFormat:@"%.0f",precentage]];
            
            filePath = [filePath stringByReplacingOccurrencesOfString:@"0012" withString:[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"CYear"]]];
            NSData *htmlData = [NSKeyedArchiver archivedDataWithRootObject:filePath];
            [filePath writeToFile:progressPath atomically:YES encoding:NSASCIIStringEncoding error:nil];
            
            NSLog(@"====%@",filePath);
            
            //NSData *htmlData = [NSData dataWithContentsOfFile:filePath];
            if (htmlData) {
                NSBundle *bundle = [NSBundle mainBundle];
                NSString *path = [bundle bundlePath];
                NSString *fullPath = [NSBundle pathForResource:@"progress" ofType:@"html" inDirectory:path];
                _web_goalview.hidden=NO;
                [_web_goalview loadRequest:[NSURLRequest requestWithURL:[NSURL fileURLWithPath:fullPath]]];
                _web_goalview.userInteractionEnabled = NO;
            }
            
            
            
            //SUBJECT VIEW
            
            
            _lbl_comp_with.text=[NSString stringWithFormat:@"Comparison With %@ Subject",_subjectName];
            _lbl_all_sub_acc.text=[NSString stringWithFormat:@"Your Accuracy For All Subject : %@%%",[responseObject valueForKey:@"over_all_accuracy"]];
            
            
            
            NSString *progressPath1 = [[NSBundle mainBundle] pathForResource:@"progress1" ofType:@"html"];
            NSString* filePath1=[NSString stringWithContentsOfFile:progressPath1 encoding:NSASCIIStringEncoding error:nil];
            
            filePath1 = [filePath1 stringByReplacingOccurrencesOfString:@"0011" withString:[NSString stringWithFormat:@"%.0f",precentage]];
            
            filePath1 = [filePath1 stringByReplacingOccurrencesOfString:@"0012" withString:[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"subject_accuracy"]]];
            
            filePath1 = [filePath1 stringByReplacingOccurrencesOfString:@"MATHss" withString:[NSString stringWithFormat:@"%@%@%%",_subjectName,[responseObject valueForKey:@"subject_accuracy"]]];
            
            NSData *htmlData1 = [NSKeyedArchiver archivedDataWithRootObject:filePath1];
            [filePath1 writeToFile:progressPath1 atomically:YES encoding:NSASCIIStringEncoding error:nil];
            
            NSLog(@"====%@",filePath1);
            
            if (htmlData1) {
                NSBundle *bundle = [NSBundle mainBundle];
                NSString *path = [bundle bundlePath];
                NSString *fullPath = [NSBundle pathForResource:@"progress1" ofType:@"html" inDirectory:path];
                [_web_sub_accu loadRequest:[NSURLRequest requestWithURL:[NSURL fileURLWithPath:fullPath]]];
                _web_sub_accu.userInteractionEnabled = NO;
            }
            
            
            _lbl_avgtime.text=@"00:00";
            
            _lbl_tottime.text=[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"AvaragePerQuestionTime"]];
            
            
        }
        else
        {
            
        }
        
        
        
        
        [APP_DELEGATE hideLoadingView];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [APP_DELEGATE hideLoadingView];
        NSLog(@"Error: %@", error);
    }];
    
}


-(void)SetCCTView
{
    
    _lbl_subjectnm.text=[NSString stringWithFormat:@"%@ Subject",_subjectName];
    _lbl_scoreoftest.text=[NSString stringWithFormat:@"Score of Test %lu",[_tempNotapper count]+[_tempIncorrect count]+[_tempCorrect count]];
    
    _lbl1_l0.text=[NSString stringWithFormat:@"%lu",(unsigned long)_tempCorrect.count];
    _lbl2_l0.text=[NSString stringWithFormat:@"%lu",[_tempNotapper count]+[_tempIncorrect count]+[_tempCorrect count]];
    [self LevelWiseCount];
    
    
    //============ WebView ==============
    NSString *subjAcc=[[NSString alloc]init];
    int totacc = 0;
    
    for (int i=0; i<arrAccu.count; i++) {
        if ([[[arrAccu valueForKey:@"SubjectID"] objectAtIndex:i]isEqualToString:[NSString stringWithFormat:@"%@",_subjectId]]) {
            subjAcc=[[arrAccu valueForKey:@"accuracy1"] objectAtIndex:i];
        }
        
        totacc+=[[[arrAccu valueForKey:@"accuracy1"] objectAtIndex:i] intValue];
    }
    
    
    
    
    
    _lbl_schoolmarks.text=[NSString stringWithFormat:@"Your previous year school result %@",[[NSUserDefaults standardUserDefaults] valueForKey:@"PYear"]];
    
    
    float precentage = (100 * [_lbl1_l0.text floatValue])/[_lbl2_l0.text floatValue];
    
    NSString *progressPath = [[NSBundle mainBundle] pathForResource:@"progress" ofType:@"html"];
    NSString* filePath=[NSString stringWithContentsOfFile:progressPath encoding:NSASCIIStringEncoding error:nil];
    
    filePath = [filePath stringByReplacingOccurrencesOfString:@"0011" withString:[NSString stringWithFormat:@"%.0f",precentage]];
    
    filePath = [filePath stringByReplacingOccurrencesOfString:@"0012" withString:[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"CYear"]]];
    NSData *htmlData = [NSKeyedArchiver archivedDataWithRootObject:filePath];
    [filePath writeToFile:progressPath atomically:YES encoding:NSASCIIStringEncoding error:nil];
    
    NSLog(@"====%@",filePath);
    
    //NSData *htmlData = [NSData dataWithContentsOfFile:filePath];
    if (htmlData) {
        NSBundle *bundle = [NSBundle mainBundle];
        NSString *path = [bundle bundlePath];
        NSString *fullPath = [NSBundle pathForResource:@"progress" ofType:@"html" inDirectory:path];
        _web_goalview.hidden=NO;
        [_web_goalview loadRequest:[NSURLRequest requestWithURL:[NSURL fileURLWithPath:fullPath]]];
        _web_goalview.userInteractionEnabled = NO;
    }
    
    
    
    //=======SUBJECT STASTIC
    
    _lbl_comp_with.text=[NSString stringWithFormat:@"Comparison With %@ Subject",_subjectName];
    _lbl_all_sub_acc.text=[NSString stringWithFormat:@"Your Accuracy For All Subject : %lu%%",totacc/arrAccu.count];
    
    
    
    NSString *progressPath1 = [[NSBundle mainBundle] pathForResource:@"progress1" ofType:@"html"];
    NSString* filePath1=[NSString stringWithContentsOfFile:progressPath1 encoding:NSASCIIStringEncoding error:nil];
    
    filePath1 = [filePath1 stringByReplacingOccurrencesOfString:@"0011" withString:[NSString stringWithFormat:@"%.0f",precentage]];
    
    filePath1 = [filePath1 stringByReplacingOccurrencesOfString:@"0012" withString:[NSString stringWithFormat:@"%@",subjAcc]];
    
    filePath1 = [filePath1 stringByReplacingOccurrencesOfString:@"MATHss" withString:[NSString stringWithFormat:@"%@%@%%",_subjectName,subjAcc]];
    
    NSData *htmlData1 = [NSKeyedArchiver archivedDataWithRootObject:filePath1];
    [filePath1 writeToFile:progressPath1 atomically:YES encoding:NSASCIIStringEncoding error:nil];
    
    NSLog(@"====%@",filePath1);
    
    if (htmlData1) {
        NSBundle *bundle = [NSBundle mainBundle];
        NSString *path = [bundle bundlePath];
        NSString *fullPath = [NSBundle pathForResource:@"progress1" ofType:@"html" inDirectory:path];
        [_web_sub_accu loadRequest:[NSURLRequest requestWithURL:[NSURL fileURLWithPath:fullPath]]];
        _web_sub_accu.userInteractionEnabled = NO;
    }
    
    /////Total Time
    //_lbl_tottime.text=[self formatTimeFromSeconds:[_TakanTime intValue]];
    _lbl_tottime.text=@"00:00";
    
    _lbl_tottime.text=[NSString stringWithFormat:@"%@",_TakanTime];
    _lbl_avgtime.text=[NSString stringWithFormat:@"%@",_avgTakanTime];
}


-(void)CallGetAccuracy
{
    FMResultSet *results = [MyModel selectQuery:[NSString stringWithFormat:@"select s.*,(select CreatedOn from student_mcq_test_hdr as sthd where sthd.SubjectID=s.SubjectID                       order by CreatedOn desc limit 1) as Last_Date, count(smth.StudentMCQTestHDRID) as TotalSubjectTest,                       (( (select count(StudentMCQTestDTLID) from student_mcq_test_dtl as dtl left join mcqoption as                           opt on opt.MCQOPtionID=dtl.AnswerID where dtl.IsAttempt=1 and opt.isCorrect=1 and EXISTS                           (select StudentMCQTestHDRID from student_mcq_test_hdr hdr where hdr.StudentMCQTestHDRID=dtl.StudentMCQTestHDRID                            and hdr.SubjectID=s.SubjectID ) ) *100) / (select count (StudentMCQTestDTLID)                                                                       from student_mcq_test_dtl dtl where EXISTS (select StudentMCQTestHDRID from student_mcq_test_hdr hdr where                                                                                                                   hdr.StudentMCQTestHDRID=dtl.StudentMCQTestHDRID and hdr.SubjectID=s.SubjectID )) ) as accuracy1                       from subjects as s left join student_mcq_test_hdr as smth on s.SubjectID=smth.SubjectID where s.StandardID=%@ AND s.isMCQ=1 group by s.SubjectID order by s.SubjectOrder",[[NSUserDefaults standardUserDefaults]valueForKeyPath:@"ParseData.StandardID"]]];
    
    
    
    arrAccu=[[NSMutableArray alloc]init];
    
    while ([results next])
    {
        NSMutableDictionary *mdict=[[NSMutableDictionary alloc]init];
        
        [mdict setValue:[results stringForColumn:@"SubjectID"] forKey:@"SubjectID"];
        [mdict setValue:[results stringForColumn:@"SubjectName"] forKey:@"SubjectName"];
        
        if ([[results stringForColumn:@"accuracy1"] intValue]==nil) {
            [mdict setValue:@"0" forKey:@"accuracy1"];
        }
        else
        {
            [mdict setValue:[results stringForColumn:@"accuracy1"] forKey:@"accuracy1"];
        }
        
        [arrAccu addObject:mdict];
    }
    
    NSLog(@"%@",arrAccu);
    
}




-(NSString *)formatTimeFromSeconds:(int)numberOfSeconds
{
    
    int seconds = numberOfSeconds % 60;
    int minutes = (numberOfSeconds / 60) % 60;
    int hours = numberOfSeconds / 3600;
    
    //we have >=1 hour => example : 3h:25m
    if (hours) {
        return [NSString stringWithFormat:@"%dh:%02dm", hours, minutes];
    }
    //we have 0 hours and >=1 minutes => example : 3m:25s
    if (minutes) {
        return [NSString stringWithFormat:@"%02dm:%02ds", minutes, seconds];
    }
    //we have only seconds example : 25s
    return [NSString stringWithFormat:@"%02d:%02ds",0,seconds];
}

-(NSString *)getHTMLString :(NSString*)strprog :(NSString*)strprg2 :(NSString*)title :(NSString*)htmlstring
{
    
 
    htmlstring = [htmlstring stringByReplacingOccurrencesOfString:@"1001"
                                                       withString:strprog];
    htmlstring = [htmlstring stringByReplacingOccurrencesOfString:@"1002"
                                                       withString:strprg2];
    
    
    return htmlstring;
   
}



-(void)LevelWiseCount
{
    int level1tot=0;
    int level1cor=0;
    int level2tot=0;
    int level2cor=0;
    int level3tot=0;
    int level3cor=0;
    
    for (int i=0; _mcqQuestions.count>i; i++) {
        if ([[[_mcqQuestions valueForKey:@"QuestionLevelID"] objectAtIndex:i] intValue]==1) {
            level1tot+=1;
            if ([[[_mcqQuestions valueForKey:@"isRight"]objectAtIndex:i] intValue]==1)
                level1cor+=1;
            
        }
        else if ([[[_mcqQuestions valueForKey:@"QuestionLevelID"]objectAtIndex:i] intValue]==2){
            level2tot+=1;
            if ([[[_mcqQuestions valueForKey:@"isRight"]objectAtIndex:i] intValue]==1)
                level2cor+=1;
        }
        else
        {
            if ([[[_mcqQuestions valueForKey:@"isRight"]objectAtIndex:i] intValue]==1)
                level3cor+=1;
            level3tot+=1;
        }
    }
    
    _lbl1_l1.text=[NSString stringWithFormat:@"%d",level1cor];
    _lbl2_l1.text=[NSString stringWithFormat:@"%d",level1tot];
    
    _lbl1_l2.text=[NSString stringWithFormat:@"%d",level2cor];
    _lbl2_l2.text=[NSString stringWithFormat:@"%d",level2tot];
    
    _lbl1_l3.text=[NSString stringWithFormat:@"%d",level3cor];
    _lbl2_l3.text=[NSString stringWithFormat:@"%d",level3tot];
    
    
    
    NSLog(@"level1tot %d",level1tot);
    NSLog(@"level1cor %d",level1cor);
    NSLog(@"level2tot %d",level2tot);
    NSLog(@"level2cor %d",level2cor);
    NSLog(@"level3tot %d",level3tot);
    NSLog(@"level1cor %d",level3cor);
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    isScrollingStart=YES;
    NSLog(@"scrollViewDidScroll  %f , %f",_scroll_vw.contentOffset.x,_scroll_vw.contentOffset.y);
    if (_scroll_vw.contentOffset.y<=135) {
        _img_header.alpha=_scroll_vw.contentOffset.y/135;
    }
    else
    {
        _img_header.alpha=1.0;
    }
}




- (IBAction)btn_BACK:(id)sender {
    
    if ([_isCCT isEqualToString:@"1"]) {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        if ([_isFrom isEqualToString:@"SearchPaper"]) {
            [self.navigationController popViewControllerAnimated:YES];
        }
        else
        {
            UINavigationController *navigationController = self.navigationController;
            [navigationController popViewControllerAnimated:NO];
            [navigationController popViewControllerAnimated:YES];
        }
    }
    
}

- (IBAction)btn_SUMMERY:(id)sender {
    //TSummeryVC *viewController=[[TSummeryVC alloc]initWithNibName:@"TSummeryVC" bundle:nil];
    //[self.navigationController pushViewController:viewController animated:YES];
    
  /*  TSummeryVC *add = [[TSummeryVC alloc] initWithNibName:@"TSummeryVC" bundle:nil];
    add.tempCorrect=_tempCorrect;
    add.tempIncorrect=_tempIncorrect;
    add.tempNotapper=_tempNotapper;
    add.tempCorrectOp=_tempCorrectOp;
    add.tempIncorrectOp=_tempIncorrectOp;
    add.tempNotapperOp=_tempNotapperOp;
    
    add.chapterList=_chapterList;
    add.TBStatus=_TBStatus;
    add.subjectId=_subjectId;
    add.subjectName=_subjectName;
    add.levelId=_levelId;
    add.mcqQuestion=_mcqQuestions;
    add.mcqOption=_mcqOption;
     
    //[self presentViewController:add animated:YES completion:nil];
    [self.navigationController pushViewController:add animated:YES];
  */
    
    CCTTSummeryVC *add = [[CCTTSummeryVC alloc] initWithNibName:@"CCTTSummeryVC" bundle:nil];
    add.cctPaperID=_CCTPaperId;
    //[self presentViewController:add animated:YES completion:nil];
    [self.navigationController pushViewController:add animated:YES];
    
    
}

- (IBAction)btn_STARTNEW_A:(id)sender {
   
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    SubjectVC *view = [mainStoryboard instantiateViewControllerWithIdentifier:@"SubjectVC"];
    view.BoardID=[[NSUserDefaults standardUserDefaults]valueForKeyPath:@"ParseData.BoardID"];
    view.StandardID=[[NSUserDefaults standardUserDefaults]valueForKeyPath:@"ParseData.StandardID"];
    view.MediumID=[[NSUserDefaults standardUserDefaults]valueForKeyPath:@"ParseData.MediumID"];
    
    [self.navigationController pushViewController:view animated:YES];
    
}

- (IBAction)btn_HOME_A:(id)sender {
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    HomeVC *view = [mainStoryboard instantiateViewControllerWithIdentifier:@"HomeVC"];
    [self.navigationController pushViewController:view animated:YES];
}
@end
