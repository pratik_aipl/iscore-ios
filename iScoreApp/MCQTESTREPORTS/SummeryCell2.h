//
//  SummeryCell2.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 3/28/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>

@interface SummeryCell2 : UITableViewCell

//Outlet
@property (strong, nonatomic) IBOutlet UIView *viewDispQuestion;

@property (strong, nonatomic) IBOutlet UITextView *txtVQuestion;
@property (strong, nonatomic) IBOutlet WKWebView *webVQuestion;
//@property (strong, nonatomic) IBOutlet UIWebView *webVQuestion;
@property (strong, nonatomic) IBOutlet UILabel *lbl_queNo;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollVQuestion;
@property (strong, nonatomic) IBOutlet UILabel *lbl_quetimer;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *webvQuestionoHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewQuestionHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *txtVQuestionHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewOption1Height;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewOption2Height;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewOption3Height;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewOption4Height;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *txtVOption1Height;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *txtVOption2Height;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *txtVOption3Height;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *txtVOption4Height;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *webVOption1Height;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *webVOption2Height;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *webVOption3Height;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *webVOption4Height;


@property (strong, nonatomic) IBOutlet UIView *viewOption1;
@property (strong, nonatomic) IBOutlet UIView *viewOption2;
@property (strong, nonatomic) IBOutlet UIView *viewOption3;
@property (strong, nonatomic) IBOutlet UIView *viewOption4;



@property (weak, nonatomic) IBOutlet UILabel *lblA;
@property (weak, nonatomic) IBOutlet UILabel *lblB;
@property (weak, nonatomic) IBOutlet UILabel *lblC;
@property (weak, nonatomic) IBOutlet UILabel *lblD;


@property (weak, nonatomic) IBOutlet UITextView *txtVOption1;
@property (weak, nonatomic) IBOutlet UITextView *txtVOption2;
@property (weak, nonatomic) IBOutlet UITextView *txtVOption3;
@property (weak, nonatomic) IBOutlet UITextView *txtVOption4;
//@property (weak, nonatomic) IBOutlet UIWebView *webVOption1;
//@property (weak, nonatomic) IBOutlet UIWebView *webVOption2;
//@property (weak, nonatomic) IBOutlet UIWebView *webVOption3;
//@property (weak, nonatomic) IBOutlet UIWebView *webVOption4;

@property (strong, nonatomic) IBOutlet WKWebView *webVOption1;
@property (strong, nonatomic) IBOutlet WKWebView *webVOption2;
@property (strong, nonatomic) IBOutlet WKWebView *webVOption3;
@property (strong, nonatomic) IBOutlet WKWebView *webVOption4;

@property (weak, nonatomic) IBOutlet UIButton *btnOption1;
@property (weak, nonatomic) IBOutlet UIButton *bntOption2;
@property (weak, nonatomic) IBOutlet UIButton *btnOption3;
@property (weak, nonatomic) IBOutlet UIButton *btnOption4;
@property (strong, nonatomic) IBOutlet UIButton *btn_flag;


//Action


@end
