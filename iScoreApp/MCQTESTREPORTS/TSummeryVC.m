//
//  TSummeryVC.m
//  iScoreApp
//
//  Created by ADMIN-Khushal on 3/22/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "TSummeryVC.h"
#import "MXSegmentedPager.h"
#import "TestReportVC.h"
#import "UIColor+CL.h"
#import "SummeryCell.h"
#import "SummeryCell1.h"
#import "SummeryCell2.h"
#import "ApplicationConst.h"



@interface TSummeryVC ()<MXSegmentedPagerDelegate, MXSegmentedPagerDataSource,UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) MXSegmentedPager  * segmentedPager;
@property (nonatomic, strong) UITableView       * tableView;
@property (nonatomic, strong) UITableView       * tableView1;
@property (nonatomic, strong) UITableView       * tableView2;
//@property (nonatomic, strong) UIWebView         * webView;
@property (nonatomic, strong) UITextView        * textView;

@end

@implementation TSummeryVC
NSString * strQuestion1 , * strOption11 , * strOption21 ,* strOption31 , * strOption41;
int ans,ans1,ans2;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    SelectedTab=1;
    // Do any additional setup after loading the view from its nib.
    self.view.backgroundColor = UIColor.whiteColor;
    
    [self.view addSubview:self.segmentedPager];
    
    // Parallax Header
    self.segmentedPager.parallaxHeader.view = _vw_header;
    self.segmentedPager.parallaxHeader.mode = MXParallaxHeaderModeFill;
    self.segmentedPager.parallaxHeader.height = 170;
    self.segmentedPager.parallaxHeader.minimumHeight = 20;
    self.segmentedPager.bounces=NO;
    self.segmentedPager.backgroundColor=[UIColor clearColor];
    
    [self.segmentedPager.segmentedControl setTintColor:[UIColor redColor]];
    self.segmentedPager.layer.cornerRadius = 10;
    self.segmentedPager.clipsToBounds = YES;
    
    // Segmented Control customization
    
    self.segmentedPager.segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    self.segmentedPager.segmentedControl.backgroundColor = [UIColor colorWithHex:0x075584];
    self.segmentedPager.segmentedControl.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
    self.segmentedPager.segmentedControl.selectedTitleTextAttributes = @{NSForegroundColorAttributeName : [UIColor orangeColor]};
    self.segmentedPager.segmentedControl.selectionStyle = HMSegmentedControlSelectionStyleFullWidthStripe;
    self.segmentedPager.segmentedControl.selectionIndicatorColor = [UIColor orangeColor];
    //self.segmentedPager.segmentedControl.
    
    //self.segmentedPager.segmentedControlEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
    
    
    [self labelProp:_lbl_l1];
    [self labelProp:_lbl_l2];
    [self labelProp:_lbl_l3];
    
    
    _lbl_correct.text=[NSString stringWithFormat:@"%lu Correct",(unsigned long)_tempCorrect.count];
    _lbl_incorrect.text=[NSString stringWithFormat:@"%lu Incorrect",(unsigned long)_tempIncorrect.count];
    _lbl_notapper.text=[NSString stringWithFormat:@"%lu Not Appered",(unsigned long)_tempNotapper.count];
    
    
}


-(void)labelProp:(UILabel*)lbl
{
    lbl.clipsToBounds=YES;
    lbl.alpha=0.5;
    lbl.layer.cornerRadius=lbl.frame.size.height/2;
}

- (void)viewWillLayoutSubviews {
   /* self.segmentedPager.frame = (CGRect){
        .origin = CGPointZero,
        .size   = self.view.frame.size
    };
    [super viewWillLayoutSubviews];*/
    
    self.segmentedPager.frame = (CGRect){
        .origin.x       = 0.f,
        .origin.y       = 0.f,
        .size.width     = self.view.frame.size.width,
        .size.height    = self.view.frame.size.height
    };
    
}

#pragma mark Properties

- (UIView *)vw_header {
    if (!_vw_header) {
        // Set a cover on the top of the view
        _vw_header = [self.nibBundle loadNibNamed:@"vw_header" owner:nil options:nil].firstObject;
    }
    return _vw_header;
}

- (MXSegmentedPager *)segmentedPager {
    if (!_segmentedPager) {
        
        // Set a segmented pager below the cover
        _segmentedPager = [[MXSegmentedPager alloc] init];
        _segmentedPager.delegate    = self;
        _segmentedPager.dataSource  = self;
    }
    return _segmentedPager;
}

- (UITableView *)tableView {
    if (!_tableView) {
        //Add a table page
        _tableView = [[UITableView alloc] init];
        [_tableView registerNib:[UINib nibWithNibName:@"SummeryCell" bundle:nil] forCellReuseIdentifier:@"CellSummery"];
        //[_tableView registerNib:[UINib nibWithNibName:@"SummeryCell1" bundle:nil] forCellReuseIdentifier:@"CellSummery1"];
        _tableView.bounces = NO;
        _tableView.alwaysBounceVertical = NO;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorColor = [UIColor clearColor];
        //self.tableView.alwaysBounceVertical = NO;
    }
    return _tableView;
}
- (UITableView *)tableView1 {
    if (!_tableView1) {
        //Add a table page
        _tableView1 = [[UITableView alloc] init];
        //[_tableView registerNib:[UINib nibWithNibName:@"SummeryCell" bundle:nil] forCellReuseIdentifier:@"CellSummery"];
        [_tableView1 registerNib:[UINib nibWithNibName:@"SummeryCell1" bundle:nil] forCellReuseIdentifier:@"CellSummery1"];
        _tableView1.bounces = NO;
        _tableView1.alwaysBounceVertical = NO;
        _tableView1.delegate = self;
        _tableView1.dataSource = self;
        _tableView1.separatorColor = [UIColor clearColor];
        //self.tableView.alwaysBounceVertical = NO;
    }
    return _tableView1;
}
- (UITableView *)tableView2 {
    if (!_tableView2) {
        //Add a table page
        _tableView2 = [[UITableView alloc] init];
        //[_tableView registerNib:[UINib nibWithNibName:@"SummeryCell" bundle:nil] forCellReuseIdentifier:@"CellSummery"];
        [_tableView2 registerNib:[UINib nibWithNibName:@"SummeryCell2" bundle:nil] forCellReuseIdentifier:@"CellSummery2"];
        _tableView2.bounces = NO;
        _tableView2.alwaysBounceVertical = NO;
        _tableView2.separatorColor = [UIColor clearColor];
        _tableView2.delegate = self;
        _tableView2.dataSource = self;
        //self.tableView.alwaysBounceVertical = NO;
    }
    return _tableView2;
}

/*
- (UIWebView *)webView {
    if (!_webView) {
        // Add a web page
        _webView = [[UIWebView alloc] init];
        _webView.delegate = self;
        NSString *strURL = @"http://nshipster.com/";
        NSURL *url = [NSURL URLWithString:strURL];
        NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
        [_webView loadRequest:urlRequest];
    }
    return _webView;
}

- (UITextView *)textView {
    if (!_textView) {
        // Add a text page
        _textView = [[UITextView alloc] init];
        NSString *filePath = [[NSBundle mainBundle]pathForResource:@"LongText" ofType:@"txt"];
        _textView.text = [[NSString alloc]initWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
        
    }
    return _textView;
}
*/
#pragma mark <MXSegmentedPagerDelegate>

- (CGFloat)heightForSegmentedControlInSegmentedPager:(MXSegmentedPager *)segmentedPager {
    return 50.f;
}

- (void)segmentedPager:(MXSegmentedPager *)segmentedPager didSelectViewWithTitle:(NSString *)title {
    
    [APP_DELEGATE showLoadingView:@""];
    
    NSLog(@"%@ page selected.", title);
    if ([title isEqualToString:@" Correct "]) {
        SelectedTab=1;
        [_tableView reloadData];
        
    }
    else if ([title isEqualToString:@" Incorrect "]) {
        SelectedTab=2;
        [_tableView1 reloadData];
        
    }
    else if ([title isEqualToString:@" Not Appeared "]) {
        SelectedTab=3;
        [_tableView2 reloadData];
    }
    [APP_DELEGATE hideLoadingView];
    
}
/*
- (void)segmentedPager:(MXSegmentedPager *)segmentedPager didScrollWithParallaxHeader:(MXParallaxHeader *)parallaxHeader {
    NSLog(@"progress %f", parallaxHeader.progress);
    
}
*/
#pragma mark <MXSegmentedPagerDataSource>

- (NSInteger)numberOfPagesInSegmentedPager:(MXSegmentedPager *)segmentedPager {
    return 3;
}

- (NSString *)segmentedPager:(MXSegmentedPager *)segmentedPager titleForSectionAtIndex:(NSInteger)index {
    
    return @[@" Correct ", @" Incorrect ", @" Not Appeared "][index];
}

- (UIView *)segmentedPager:(MXSegmentedPager *)segmentedPager viewForPageAtIndex:(NSInteger)index {

    return @[self.tableView, self.tableView1, self.tableView2][index];
}

#pragma mark <UITableViewDelegate>

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //NSInteger index = (indexPath.row % 2) + 1;
    //[self.segmentedPager.pager showPageAtIndex:index animated:YES];
}
/*
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    //isScrollingStart=YES;
    NSLog(@"scrollViewDidScroll  %f , %f",scrollView.contentOffset.x,scrollView.contentOffset.y);
    
    if (scrollView.contentOffset.y<=135) {
        self.segmentedPager.segmentedControl.alpha=scrollView.contentOffset.y/135;
    }
    else
    {
        self.segmentedPager.segmentedControl.alpha=1.0;
    }
}*/
#pragma mark - Check Question and Option String

- (NSString*)displayWebviewORTextView:(NSString *)Str Webview:(WKWebView*)Webview TextView:(UITextView*)TextView BGView:(NSLayoutConstraint*)BGViewHeightConstant {
    
    if ([Str containsString:@"<img"]|| [Str containsString:@"<math"] || [Str containsString:@"<table"] ) {
        
        NSLog(@"string contains tag!");
        if ([Str containsString:@"src"]) {
            Str = [Str stringByReplacingOccurrencesOfString:@"src=\"/" withString:@"src=\"http://staff.parshvaa.com/"];
            NSLog(@"apdateString %@",Str);
        }
        TextView.hidden =true;
        Webview.hidden =false;
        [Webview loadHTMLString:Str baseURL:nil];
        
        return @"Web";
        
    } else {
        
        NSLog(@"string does not contain tags");
        Webview.hidden =true;
        TextView.hidden =false;
        TextView.attributedText = [self convertIntoAttributadStr:[NSString stringWithFormat:@"%@",Str]];
        //[self changeTextViewHeight:TextView :BGViewHeightConstant];

        return @"Text";
    }
}

#pragma mark - Change Height Of Textview

- (void)changeTextViewHeight:(UITextView *)textView :(NSLayoutConstraint*)heightConstraint
{
    NSLog(@"changeTextViewHeight");
    
    CGSize constraint = CGSizeMake(textView.frame.size.width, CGFLOAT_MAX);
    CGSize size;
    
    //NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
    //CGSize boundingBox = [textView.text boundingRectWithSize:constraint
                                                    // options:NSStringDrawingUsesLineFragmentOrigin
                                                  //attributes:@{NSFontAttributeName:textView.font}
                                                   //  context:context].size;
    
    //size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    //heightConstraint.constant = size.height + 10;
    
    //NSLog(@"height : %f",size.height);
    

}


#pragma mark - Convert HTML Formated String
-(NSAttributedString *)convertIntoAttributadStr:(NSString *)str{
    
    NSString *htmlString = str;
    NSAttributedString *attributedString = [[NSAttributedString alloc]
                                            initWithData: [htmlString dataUsingEncoding:NSUnicodeStringEncoding]
                                            options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
                                            documentAttributes: nil
                                            error: nil
                                            ];
    
    
    return attributedString;
}

#pragma mark <UITableViewDataSource>

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (SelectedTab==1) {
        return _tempCorrect.count;
    }
    else if (SelectedTab==2)
    {
        return _tempIncorrect.count;
    }
    else if (SelectedTab==3)
    {
        NSLog(@"_tempNotapper.count %lu",(unsigned long)_tempNotapper.count);
        return _tempNotapper.count;
    }
    
    return 0;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (SelectedTab==1) {
        static NSString *CellIdentifier = @"CellSummery";
        SummeryCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[SummeryCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        //cell.textLabel.text = (indexPath.row % 2)? @"Text" : @"Web";
        cell.lbl_queNo.text=[NSString stringWithFormat:@"%ld.",(long)indexPath.row+1];
        
        cell.webVQuestion.frame=[[UIScreen mainScreen] bounds];
        
        cell.viewQuestionHeight.constant = 50.0;
        cell.viewOption1Height.constant = 50.0;
        cell.viewOption2Height.constant = 50.0;
        cell.viewOption3Height.constant = 50.0;
        cell.viewOption4Height.constant = 50.0;
        
        cell.webVQuestion.tag = 1;
        cell.webVOption1.tag = 2;
        cell.webVOption2.tag = 3;
        cell.webVOption3.tag = 4;
        cell.webVOption4.tag = 5;
        

        
        strQuestion1 = [self displayWebviewORTextView:[[_tempCorrect objectAtIndex:indexPath.row] valueForKey:@"Question"] Webview:cell.webVQuestion TextView:cell.txtVQuestion BGView:cell.viewQuestionHeight];
        
        
        cell.viewOption1.hidden=YES;
        cell.viewOption2.hidden=YES;
        cell.viewOption3.hidden=YES;
        cell.viewOption4.hidden=YES;
        
        
        int optot=[[_tempCorrectOp objectAtIndex:indexPath.row] count];
        
        NSLog(@"optot == %d",optot);
        
        for (int i=0; i < optot; i++) {
            switch (i) {
                case 0:
                    
                    cell.viewOption1.hidden=NO;
                    strOption11 = [self displayWebviewORTextView:[[[_tempCorrectOp objectAtIndex:indexPath.row] objectAtIndex:0] valueForKey:@"Options"] Webview:cell.webVOption1 TextView:cell.txtVOption1 BGView:cell.viewOption1Height];
                    break;
                case 1:
                    cell.viewOption2.hidden=NO;
                    strOption21 = [self displayWebviewORTextView:[[[_tempCorrectOp objectAtIndex:indexPath.row] objectAtIndex:1] valueForKey:@"Options"] Webview:cell.webVOption2 TextView:cell.txtVOption2 BGView:cell.viewOption2Height];
                    break;
                case 2:
                    cell.viewOption3.hidden=NO;
                    strOption31 = [self displayWebviewORTextView:[[[_tempCorrectOp objectAtIndex:indexPath.row] objectAtIndex:2] valueForKey:@"Options"] Webview:cell.webVOption3 TextView:cell.txtVOption3 BGView:cell.viewOption3Height];
                    break;
                case 3:
                    cell.viewOption4.hidden=NO;
                    strOption41 = [self displayWebviewORTextView:[[[_tempCorrectOp objectAtIndex:indexPath.row] objectAtIndex:3] valueForKey:@"Options"] Webview:cell.webVOption4 TextView:cell.txtVOption4 BGView:cell.viewOption4Height];
                    break;
                    
                default:
                    break;
            }
        }
        
        /*
        strOption11 = [self displayWebviewORTextView:[[[_tempCorrectOp objectAtIndex:indexPath.row] objectAtIndex:0] valueForKey:@"Options"] Webview:cell.webVOption1 TextView:cell.txtVOption1 BGView:cell.viewOption1Height];
        
        strOption21 = [self displayWebviewORTextView:[[[_tempCorrectOp objectAtIndex:indexPath.row] objectAtIndex:1] valueForKey:@"Options"] Webview:cell.webVOption2 TextView:cell.txtVOption2 BGView:cell.viewOption2Height];
        
        strOption31 = [self displayWebviewORTextView:[[[_tempCorrectOp objectAtIndex:indexPath.row] objectAtIndex:2] valueForKey:@"Options"] Webview:cell.webVOption3 TextView:cell.txtVOption3 BGView:cell.viewOption3Height];
        
        strOption41 = [self displayWebviewORTextView:[[[_tempCorrectOp objectAtIndex:indexPath.row] objectAtIndex:3] valueForKey:@"Options"] Webview:cell.webVOption4 TextView:cell.txtVOption4 BGView:cell.viewOption4Height];
        */
        int ans=[[[_tempCorrect objectAtIndex:indexPath.row] valueForKey:@"selectedAns"] intValue];
        
        switch (ans) {
            case 1:
                
                [self selectedOptionColorG:cell.lblA ];
                [self unSelectedOptionColor:cell.lblB :cell.lblC  :cell.lblD];
                
                break;
                
            case 2:
                
                [self selectedOptionColorG:cell.lblB];
                [self unSelectedOptionColor:cell.lblA :cell.lblC  :cell.lblD ];
                break;
                
            case 3:
                
                [self selectedOptionColorG:cell.lblC ];
                [self unSelectedOptionColor:cell.lblA :cell.lblB  :cell.lblD ];
                break;
                
            case 4:
                
                [self selectedOptionColorG:cell.lblD ];
                [self unSelectedOptionColor:cell.lblA :cell.lblB  :cell.lblC ];
                break;
                
            default:
                break;
        }
        
        return cell;
    }
    else if (SelectedTab==2)
    {
        static NSString *CellIdentifier = @"CellSummery1";
        SummeryCell1 *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[SummeryCell1 alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        
        cell.lbl_queNo.text=[NSString stringWithFormat:@"%ld.",(long)indexPath.row+1];
        
        cell.webVQuestion.frame=[[UIScreen mainScreen] bounds];
        
        cell.viewQuestionHeight.constant = 50.0;
        cell.viewOption1Height.constant = 50.0;
        cell.viewOption2Height.constant = 50.0;
        cell.viewOption3Height.constant = 50.0;
        cell.viewOption4Height.constant = 50.0;
        
        cell.webVQuestion.tag = 1;
        cell.webVOption1.tag = 2;
        cell.webVOption2.tag = 3;
        cell.webVOption3.tag = 4;
        cell.webVOption4.tag = 5;
        
        
        strQuestion1 = [self displayWebviewORTextView:[[_tempIncorrect objectAtIndex:indexPath.row] valueForKey:@"Question"] Webview:cell.webVQuestion TextView:cell.txtVQuestion BGView:cell.viewQuestionHeight];
        
        
        cell.viewOption1.hidden=YES;
        cell.viewOption2.hidden=YES;
        cell.viewOption3.hidden=YES;
        cell.viewOption4.hidden=YES;
        
        
        int optot=[[_tempIncorrectOp objectAtIndex:indexPath.row] count];
        
        NSLog(@"optot == %d",optot);
        
        for (int i=0; i < optot; i++) {
            switch (i) {
                case 0:
                    
                    cell.viewOption1.hidden=NO;
                    strOption11 = [self displayWebviewORTextView:[[[_tempIncorrectOp objectAtIndex:indexPath.row] objectAtIndex:0] valueForKey:@"Options"] Webview:cell.webVOption1 TextView:cell.txtVOption1 BGView:cell.viewOption1Height];
                    break;
                case 1:
                    cell.viewOption2.hidden=NO;
                    strOption21 = [self displayWebviewORTextView:[[[_tempIncorrectOp objectAtIndex:indexPath.row] objectAtIndex:1] valueForKey:@"Options"] Webview:cell.webVOption2 TextView:cell.txtVOption2 BGView:cell.viewOption2Height];
                    break;
                case 2:
                    cell.viewOption3.hidden=NO;
                    strOption31 = [self displayWebviewORTextView:[[[_tempIncorrectOp objectAtIndex:indexPath.row] objectAtIndex:2] valueForKey:@"Options"] Webview:cell.webVOption3 TextView:cell.txtVOption3 BGView:cell.viewOption3Height];
                    break;
                case 3:
                    cell.viewOption4.hidden=NO;
                    strOption41 = [self displayWebviewORTextView:[[[_tempIncorrectOp objectAtIndex:indexPath.row] objectAtIndex:3] valueForKey:@"Options"] Webview:cell.webVOption4 TextView:cell.txtVOption4 BGView:cell.viewOption4Height];
                    break;
                    
                default:
                    break;
            }
        }
        
        
        /*
        strOption11 = [self displayWebviewORTextView:[[[_tempIncorrectOp objectAtIndex:indexPath.row] objectAtIndex:0] valueForKey:@"Options"] Webview:cell.webVOption1 TextView:cell.txtVOption1 BGView:cell.viewOption1Height];
        
        strOption21 = [self displayWebviewORTextView:[[[_tempIncorrectOp objectAtIndex:indexPath.row] objectAtIndex:1] valueForKey:@"Options"] Webview:cell.webVOption2 TextView:cell.txtVOption2 BGView:cell.viewOption2Height];
        
        strOption31 = [self displayWebviewORTextView:[[[_tempIncorrectOp objectAtIndex:indexPath.row] objectAtIndex:2] valueForKey:@"Options"] Webview:cell.webVOption3 TextView:cell.txtVOption3 BGView:cell.viewOption3Height];
        
        strOption41 = [self displayWebviewORTextView:[[[_tempIncorrectOp objectAtIndex:indexPath.row] objectAtIndex:3] valueForKey:@"Options"] Webview:cell.webVOption4 TextView:cell.txtVOption4 BGView:cell.viewOption4Height];
        
         */
        
        NSMutableDictionary *dict=[_tempIncorrectOp objectAtIndex:indexPath.row];
        
        for (int i=0; [dict count]>i; i++) {
            if ([[dict valueForKey:@"isCorrect"][i] intValue]==1) {
                ans1=i+1;
            }
        }
     
        ans=[[[_tempIncorrect objectAtIndex:indexPath.row] valueForKey:@"selectedAns"] intValue];
        
        switch (ans) {
            case 1:
                [self selectedOptionColor:cell.lblA ];
                [self unSelectedOptionColor:cell.lblB :cell.lblC  :cell.lblD];
                break;
            case 2:
                [self selectedOptionColor:cell.lblB];
                [self unSelectedOptionColor:cell.lblA :cell.lblC  :cell.lblD ];
                break;
            case 3:
                [self selectedOptionColor:cell.lblC ];
                [self unSelectedOptionColor:cell.lblA :cell.lblB  :cell.lblD ];
                break;
            case 4:
                [self selectedOptionColor:cell.lblD ];
                [self unSelectedOptionColor:cell.lblA :cell.lblB  :cell.lblC ];
                break;
            default:
                break;
        }
        
        
        switch (ans1) {
            case 1:
                [self selectedOptionColorG:cell.lblA ];
                //[self unSelectedOptionColor:cell.lblB :cell.lblC  :cell.lblD];
                break;
            case 2:
                [self selectedOptionColorG:cell.lblB];
                //[self unSelectedOptionColor:cell.lblA :cell.lblC  :cell.lblD ];
                break;
            case 3:
                [self selectedOptionColorG:cell.lblC ];
                //[self unSelectedOptionColor:cell.lblA :cell.lblB  :cell.lblD ];
                break;
            case 4:
                [self selectedOptionColorG:cell.lblD ];
                //[self unSelectedOptionColor:cell.lblA :cell.lblB  :cell.lblC ];
                break;
            default:
                break;
        }
       
        return cell;
    }
    else if (SelectedTab==3) {
        static NSString *CellIdentifier = @"CellSummery2";
        SummeryCell2 *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[SummeryCell2 alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        cell.lbl_queNo.text=[NSString stringWithFormat:@"%ld.",(long)indexPath.row+1];
        
        cell.webVQuestion.frame=[[UIScreen mainScreen] bounds];
        
        cell.viewQuestionHeight.constant = 50.0;
        cell.viewOption1Height.constant = 50.0;
        cell.viewOption2Height.constant = 50.0;
        cell.viewOption3Height.constant = 50.0;
        cell.viewOption4Height.constant = 50.0;
        
        cell.webVQuestion.tag = 1;
        cell.webVOption1.tag = 2;
        cell.webVOption2.tag = 3;
        cell.webVOption3.tag = 4;
        cell.webVOption4.tag = 5;
        
        
        strQuestion1 = [self displayWebviewORTextView:[[_tempNotapper objectAtIndex:indexPath.row] valueForKey:@"Question"] Webview:cell.webVQuestion TextView:cell.txtVQuestion BGView:cell.viewQuestionHeight];
        
        cell.viewOption1.hidden=YES;
        cell.viewOption2.hidden=YES;
        cell.viewOption3.hidden=YES;
        cell.viewOption4.hidden=YES;
        
        int optot=[[_tempNotapperOp objectAtIndex:indexPath.row] count];
        
        NSLog(@"optot == %d",optot);
        
        for (int i=0; i < optot; i++) {
            switch (i) {
                case 0:
                    
                    cell.viewOption1.hidden=NO;
                    strOption11 = [self displayWebviewORTextView:[[[_tempNotapperOp objectAtIndex:indexPath.row] objectAtIndex:0] valueForKey:@"Options"] Webview:cell.webVOption1 TextView:cell.txtVOption1 BGView:cell.viewOption1Height];
                    break;
                case 1:
                    cell.viewOption2.hidden=NO;
                    strOption21 = [self displayWebviewORTextView:[[[_tempNotapperOp objectAtIndex:indexPath.row] objectAtIndex:1] valueForKey:@"Options"] Webview:cell.webVOption2 TextView:cell.txtVOption2 BGView:cell.viewOption2Height];
                    break;
                case 2:
                    cell.viewOption3.hidden=NO;
                    strOption31 = [self displayWebviewORTextView:[[[_tempNotapperOp objectAtIndex:indexPath.row] objectAtIndex:2] valueForKey:@"Options"] Webview:cell.webVOption3 TextView:cell.txtVOption3 BGView:cell.viewOption3Height];
                    break;
                case 3:
                    cell.viewOption4.hidden=NO;
                    strOption41 = [self displayWebviewORTextView:[[[_tempNotapperOp objectAtIndex:indexPath.row] objectAtIndex:3] valueForKey:@"Options"] Webview:cell.webVOption4 TextView:cell.txtVOption4 BGView:cell.viewOption4Height];
                    break;
                    
                default:
                    break;
            }
        }
        
        
       /* strOption11 = [self displayWebviewORTextView:[[[_tempNotapperOp objectAtIndex:indexPath.row] objectAtIndex:0] valueForKey:@"Options"] Webview:cell.webVOption1 TextView:cell.txtVOption1 BGView:cell.viewOption1Height];
        
        strOption21 = [self displayWebviewORTextView:[[[_tempNotapperOp objectAtIndex:indexPath.row] objectAtIndex:1] valueForKey:@"Options"] Webview:cell.webVOption2 TextView:cell.txtVOption2 BGView:cell.viewOption2Height];
        
        strOption31 = [self displayWebviewORTextView:[[[_tempNotapperOp objectAtIndex:indexPath.row] objectAtIndex:2] valueForKey:@"Options"] Webview:cell.webVOption3 TextView:cell.txtVOption3 BGView:cell.viewOption3Height];
        
        strOption41 = [self displayWebviewORTextView:[[[_tempNotapperOp objectAtIndex:indexPath.row] objectAtIndex:3] valueForKey:@"Options"] Webview:cell.webVOption4 TextView:cell.txtVOption4 BGView:cell.viewOption4Height];
        */
        
        NSMutableDictionary *dict=[_tempNotapperOp objectAtIndex:indexPath.row];
        
        for (int i=0; [dict count]>i; i++) {
            if ([[dict valueForKey:@"isCorrect"][i] intValue]==1) {
                ans2=i+1;
            }
        }
        
        switch (ans2) {
            case 1:
                [self selectedOptionColorG:cell.lblA ];
                [self unSelectedOptionColor:cell.lblB :cell.lblC  :cell.lblD];
                break;
            case 2:
                [self selectedOptionColorG:cell.lblB];
                [self unSelectedOptionColor:cell.lblA :cell.lblC  :cell.lblD ];
                break;
            case 3:
                [self selectedOptionColorG:cell.lblC ];
                [self unSelectedOptionColor:cell.lblA :cell.lblB  :cell.lblD ];
                break;
            case 4:
                [self selectedOptionColorG:cell.lblD ];
                [self unSelectedOptionColor:cell.lblA :cell.lblB  :cell.lblC ];
                break;
            default:
                break;
        }
        
        return cell;
    }
    return nil;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 450;
}

#pragma mark <UIWebViewDelegate>

//- (void)webViewDidFinishLoad:(WKWebView *)webView {
//    
//    
//}

#pragma mark - ANSWER Select_Unselect Method

-(void)selectedOptionColor:(UILabel*)label{
    
    label.layer.backgroundColor = [UIColor redColor].CGColor;
    label.layer.cornerRadius = label.layer.frame.size.height/2;
    
}
-(void)selectedOptionColorG:(UILabel*)label{
    
    label.layer.backgroundColor = [UIColor greenColor].CGColor;
    label.layer.cornerRadius = label.layer.frame.size.height/2;
    
}

-(void)unSelectedOptionColor:(UILabel*)label1 :(UILabel*)label2 :(UILabel*)label3{
    
    label1.layer.backgroundColor = [UIColor clearColor].CGColor;
    label2.layer.backgroundColor = [UIColor clearColor].CGColor;
    label3.layer.backgroundColor = [UIColor clearColor].CGColor;
}


- (IBAction)btn_BACK:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
    //[self.navigationController dismissViewControllerAnimated:YES completion:nil];
}
@end
