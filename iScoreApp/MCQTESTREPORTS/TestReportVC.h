//
//  TestReportVC.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 3/19/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>

@interface TestReportVC : UIViewController<UIScrollViewDelegate>
{
    BOOL isScrollingStart;
    NSMutableArray *arrAccu;
}
@property (retain , nonatomic )NSMutableArray *tempCorrect,*tempIncorrect,*tempNotapper;
@property (retain , nonatomic )NSMutableArray *tempCorrectOp,*tempIncorrectOp,*tempNotapperOp;
@property (retain , nonatomic )NSMutableArray *mcqQuestions,*mcqOption;
@property (retain , nonatomic)NSString *chapterList;
@property (retain , nonatomic)NSString *TBStatus;

@property(nonatomic,assign)NSString * subjectId;
@property(nonatomic,assign)NSString * subjectName;
@property(nonatomic,assign)NSString * levelId,*TakanTime,*avgTakanTime;

@property(nonatomic,assign)NSString * isFrom;
@property(nonatomic,assign)NSString * isCCT;
@property(nonatomic,assign)NSString * CCTPaperId;


@property (weak, nonatomic) IBOutlet UILabel *lbl_schoolmarks;
@property (weak, nonatomic) IBOutlet UILabel *lbl_comp_with;
@property (weak, nonatomic) IBOutlet UILabel *lbl_all_sub_acc;





//Outlet
@property (strong, nonatomic) IBOutlet UIImageView *img_background;
@property (strong, nonatomic) IBOutlet UIView *vw_score1;
@property (strong, nonatomic) IBOutlet UIView *vw_score2;
@property (strong, nonatomic) IBOutlet UIImageView *img_header;
@property (strong, nonatomic) IBOutlet UILabel *num_possible_question;
@property (strong, nonatomic) IBOutlet UILabel *redLabel;
@property (strong, nonatomic) IBOutlet UILabel *grayLabel;
@property (strong, nonatomic) IBOutlet UIScrollView *scroll_vw;
@property (strong, nonatomic) IBOutlet UIView *vw_main;

@property (strong, nonatomic) IBOutlet UILabel *lbl_gray;
@property (strong, nonatomic) IBOutlet UILabel *lbl_blue;
@property (strong, nonatomic) IBOutlet UILabel *lbl_yellow;

@property (strong, nonatomic) IBOutlet UIView *vw_score3;
@property (strong, nonatomic) IBOutlet UIView *vw_score4;

@property (strong, nonatomic) IBOutlet UILabel *lbl_tottime;
@property (strong, nonatomic) IBOutlet UILabel *lbl_avgtime;
@property (strong, nonatomic) IBOutlet UIButton *btn_startnew;

///---------------
@property (strong, nonatomic) IBOutlet UILabel *lbl1_l0;
@property (strong, nonatomic) IBOutlet UILabel *lbl2_l0;

@property (strong, nonatomic) IBOutlet UILabel *lbl1_l1;
@property (strong, nonatomic) IBOutlet UILabel *lbl2_l1;

@property (strong, nonatomic) IBOutlet UILabel *lbl1_l2;
@property (strong, nonatomic) IBOutlet UILabel *lbl2_l2;

@property (strong, nonatomic) IBOutlet UILabel *lbl1_l3;
@property (strong, nonatomic) IBOutlet UILabel *lbl2_l3;


@property (strong, nonatomic) IBOutlet UILabel *lbl_totcorrect;
@property (strong, nonatomic) IBOutlet UILabel *lbl_totincorrect;
@property (strong, nonatomic) IBOutlet UILabel *lbl_totnotapper;
@property (strong, nonatomic) IBOutlet UILabel *lbl_subjectnm;
@property (strong, nonatomic) IBOutlet UILabel *lbl_scoreoftest;

//@property (strong, nonatomic) IBOutlet UIWebView *web_goalview;
//@property (strong, nonatomic) IBOutlet UIWebView *web_sub_accu;

@property (strong, nonatomic) IBOutlet WKWebView *web_goalview;
@property (strong, nonatomic) IBOutlet WKWebView *web_sub_accu;



//Action
- (IBAction)btn_BACK:(id)sender;
- (IBAction)btn_SUMMERY:(id)sender;
- (IBAction)btn_STARTNEW_A:(id)sender;
- (IBAction)btn_HOME_A:(id)sender;


@end
