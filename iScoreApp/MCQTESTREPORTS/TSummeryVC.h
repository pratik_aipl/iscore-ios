//
//  TSummeryVC.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 3/22/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>

@interface TSummeryVC : UIViewController
{
    int SelectedTab;
}

@property (retain , nonatomic )NSMutableArray *tempCorrect,*tempIncorrect,*tempNotapper;
@property (retain , nonatomic )NSMutableArray *tempCorrectOp,*tempIncorrectOp,*tempNotapperOp;
@property (retain , nonatomic )NSMutableArray *mcqQuestion,*mcqOption;
@property (retain , nonatomic)NSString *chapterList;
@property (retain , nonatomic)NSString *TBStatus;
@property(nonatomic,assign)NSString * subjectId;
@property(nonatomic,assign)NSString * subjectName;
@property(nonatomic,assign)NSString * levelId;

//Outlet
@property (strong, nonatomic) IBOutlet UIView *vw_header;

@property (strong, nonatomic) IBOutlet UILabel *lbl_l1;
@property (strong, nonatomic) IBOutlet UILabel *lbl_l2;
@property (strong, nonatomic) IBOutlet UILabel *lbl_l3;


@property (strong, nonatomic) IBOutlet UILabel *lbl_correct;
@property (strong, nonatomic) IBOutlet UILabel *lbl_incorrect;
@property (strong, nonatomic) IBOutlet UILabel *lbl_notapper;





//Action
- (IBAction)btn_BACK:(id)sender;


@end
