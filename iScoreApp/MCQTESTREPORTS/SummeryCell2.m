//
//  SummeryCell2.m
//  iScoreApp
//
//  Created by ADMIN-Khushal on 3/28/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "SummeryCell2.h"

@implementation SummeryCell2

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code

    [self shadowView:_txtVOption1 :_webVOption1];
    [self shadowView:_txtVOption2 :_webVOption2];
    [self shadowView:_txtVOption3 :_webVOption3];
    [self shadowView:_txtVOption4 :_webVOption4];
    
}


-(void)shadowView :(UITextView*)textview :(WKWebView*)webview
{
    
    textview.layer.borderColor = [UIColor lightGrayColor].CGColor;
    textview.layer.borderWidth = 0.7f;
    
    webview.layer.borderColor = [UIColor lightGrayColor].CGColor;
    webview.layer.borderWidth = 0.7f;
    
    
    
    [self setShadow:_txtVOption1];
    [self setShadow:_txtVOption2];
    [self setShadow:_txtVOption3];
    [self setShadow:_txtVOption4];
    
    
    
    
}

-(void)setShadow :(UITextView*)txtV
{
    
    
    CGFloat topCorrect = ([txtV bounds].size.height - [txtV     contentSize].height * [txtV zoomScale])/2.0;
    topCorrect = ( topCorrect < 0.0 ? 0.0 : topCorrect );
    [txtV setContentInset:UIEdgeInsetsMake(topCorrect,0,0,0)];
    
    
    txtV.layer.shadowRadius  = 1.5f;
    txtV.layer.shadowColor   = [UIColor lightGrayColor].CGColor;
    txtV.layer.shadowOffset  = CGSizeMake(0.0f, 0.0f);
    txtV.layer.shadowOpacity = 0.9f;
    txtV.layer.masksToBounds = NO;
    
    UIEdgeInsets shadowInsets     = UIEdgeInsetsMake(0, 0, -1.5f, 0);
    UIBezierPath *shadowPath      = [UIBezierPath bezierPathWithRect:UIEdgeInsetsInsetRect(txtV.bounds, shadowInsets)];
    txtV.layer.shadowPath    = shadowPath.CGPath;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
