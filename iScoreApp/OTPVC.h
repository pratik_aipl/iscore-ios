//
//  OTPVC.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 2/26/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OTPVC : UIViewController{
    
    int seconds;
    
    NSString *strotp;
    NSMutableDictionary *ParseData;
}
@property (retain,nonatomic )NSMutableDictionary *dictdata;
//Outlet
@property (strong, nonatomic) IBOutlet UITextField *txt_otp1;
@property (strong, nonatomic) IBOutlet UITextField *txt_otp2;
@property (strong, nonatomic) IBOutlet UITextField *txt_otp3;
@property (strong, nonatomic) IBOutlet UITextField *txt_otp4;
@property (strong, nonatomic) IBOutlet UIButton *btn_confirm;
@property (strong, nonatomic) IBOutlet UIButton *btn_cancel;
@property (strong, nonatomic) IBOutlet UIButton *btn_timer;



//Action
- (IBAction)btn_CANCEL:(id)sender;
- (IBAction)btn_RESEND:(id)sender;

- (IBAction)btn_BACK:(id)sender;
- (IBAction)btn_CONFIRM_A:(id)sender;




//Action


@end
