//
//  FreeRegisterVC.m
//  iScoreApp
//
//  Created by ADMIN-Khushal on 2/24/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "FreeRegisterVC.h"
#import "UIColor+CL.h"
#import "OTPVC.h"
#import <AFNetworking/AFNetworking.h>
#import "ApplicationConst.h"
#import "Reachability.h"



#define REGEX_USER_NAME_LIMIT @"^.{0,10}$"
#define REGEX_USER_NAME @"[A-Za-z]{0,25}"
#define REGEX_EMAIL @"[A-Z0-9a-z._%+-]{3,}+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
#define REGEX_PASSWORD_LIMIT @"^.{6,20}$"
#define REGEX_PASSWORD @"[A-Za-z0-9]{6,20}"
#define REGEX_PHONE_DEFAULT @"[0-9]{10}"

@interface FreeRegisterVC ()

-(void)reachabilityChanged:(NSNotification*)note;

@property(strong) Reachability * googleReach;
@property(strong) Reachability * localWiFiReach;
@property(strong) Reachability * internetConnectionReach;


@end

@implementation FreeRegisterVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupAlerts];
    self.navigationController.navigationBarHidden=YES;
    // Do any additional setup after loading the view.
    _btn_browse_image.layer.borderWidth=1;
    //_btn_browse_image.layer.borderColor=[UIColor colorWithHex:0x3E3E3E].CGColor;
    _btn_browse_image.layer.borderColor=[UIColor lightGrayColor].CGColor;
    _btn_browse_image.layer.cornerRadius=3;
    
    _btn_next.layer.cornerRadius=3;
    
    _txt_fname.placeholder=@"First Name";
    _txt_lname.placeholder=@"Last Name";
    _txt_email.placeholder=@"Email";
    _txt_mobile_no.placeholder=@"Mobile Number";
    
    [self CustomTextView:_txt_fname];
    [self CustomTextView:_txt_lname];
    [self CustomTextView:_txt_email];
    [self CustomTextView:_txt_mobile_no];
    
    _img_student.clipsToBounds=YES;
    _img_teacher.clipsToBounds=YES;
    
    _img_student.layer.cornerRadius=_img_student.frame.size.height/2;
    _img_teacher.layer.cornerRadius=_img_teacher.frame.size.height/2;
    _img_down_arrow.image = [UIImage imageNamed: @"left-down-arrow.jpg"];
    
    arrimage=[[NSMutableArray alloc] initWithObjects:@"default_student_image.jpg",@"default_teacher_image.jpg", nil];
    
    studimg=[UIImage imageNamed:@"default_student_image.jpg"];
    
    
    _txt_fname.delegate=self;
    _txt_lname.delegate=self;
    
    _txt_fname.keyboardType=UIKeyboardTypeASCIICapable;
    _txt_lname.keyboardType=UIKeyboardTypeASCIICapable;
    
    [_txt_mobile_no addTarget:self action:@selector(textFieldDidChange1:) forControlEvents:UIControlEventEditingChanged];
    
    
    NSLog(@"DICT DATA %@",_dictdata);
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reachabilityChanged:)
                                                 name:kReachabilityChangedNotification
                                               object:nil];
    
    
    __weak __block typeof(self) weakself = self;
    
    //////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////
    //
    // create a Reachability object for www.google.com
    
    self.googleReach = [Reachability reachabilityWithHostname:@"www.google.com"];
    
    self.googleReach.reachableBlock = ^(Reachability * reachability)
    {
        NSString * temp = [NSString stringWithFormat:@"GOOGLE Block Says Reachable(%@)", reachability.currentReachabilityString];
        NSLog(@"%@", temp);
        
        // to update UI components from a block callback
        // you need to dipatch this to the main thread
        // this uses NSOperationQueue mainQueue
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            //weakself.blockLabel.text = temp;
            //weakself.blockLabel.textColor = [UIColor blackColor];
            isInternet=YES;
        }];
    };
    
    self.googleReach.unreachableBlock = ^(Reachability * reachability)
    {
        NSString * temp = [NSString stringWithFormat:@"GOOGLE Block Says Unreachable(%@)", reachability.currentReachabilityString];
        NSLog(@"%@", temp);
        
        // to update UI components from a block callback
        // you need to dipatch this to the main thread
        // this one uses dispatch_async they do the same thing (as above)
        dispatch_async(dispatch_get_main_queue(), ^{
            //weakself.blockLabel.text = temp;
            //weakself.blockLabel.textColor = [UIColor redColor];
            isInternet=NO;
        });
    };
    
    [self.googleReach startNotifier];
    
    
    
    
    
}

-(void)textFieldDidChange1 :(UITextField *)theTextField
{
    
    
    if ([theTextField.text length]<=10) {
        NSLog(@"ok");
        _txt_mobile_no.text =[NSString stringWithFormat:@"%ld",[theTextField.text integerValue]];
        
        
    }
    else
    {
        NSString *message = [NSString stringWithFormat:@"Should not more than 10 digit"];
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil
                                                                       message:message
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:nil];
        
        int duration = 2; // duration in seconds
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [alert dismissViewControllerAnimated:YES completion:nil];
        });
        
        NSLog(@"NOT OK");
        NSString *lastString=[_txt_mobile_no.text substringToIndex:[_txt_mobile_no.text length] - 1];;
        [_txt_mobile_no setText:lastString];
    }
    
}

#pragma Rechablity
-(void)reachabilityChanged:(NSNotification*)note
{
    Reachability * reach = [note object];
    
    if(reach == self.googleReach)
    {
        if([reach isReachable])
        {
            NSString * temp = [NSString stringWithFormat:@"GOOGLE Notification Says Reachable(%@)", reach.currentReachabilityString];
            NSLog(@"%@", temp);
            
            //[self CheckInternet];
            isInternet=YES;
            
        }
        else
        {
            NSString * temp = [NSString stringWithFormat:@"GOOGLE Notification Says Unreachable(%@)", reach.currentReachabilityString];
            NSLog(@"%@", temp);
            isInternet=NO;
            // [self CheckInternet];
            
        }
    }
}

-(void)CustomTextView:(UITextField*)txtview
{
    txtview.layer.cornerRadius=5;
    txtview.layer.borderWidth=1;
    txtview.layer.borderColor=[UIColor lightGrayColor].CGColor;
}
-(void)setupAlerts{
    
    [_txt_fname addRegx:REGEX_USER_NAME withMsg:@"Only alpha numeric characters are allowed."];
    [_txt_lname addRegx:REGEX_USER_NAME withMsg:@"Only alpha numeric characters are allowed."];
    [_txt_email addRegx:REGEX_EMAIL withMsg:@"Enter valid email."];
    [_txt_mobile_no addRegx:REGEX_PHONE_DEFAULT  withMsg:@"Enter valid mobile number"];
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(void)CallRegister
{
    [APP_DELEGATE showLoadingView:@""];
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    [AddPost setValue:_txt_email.text forKey:@"email"];
    [AddPost setValue:_txt_mobile_no.text forKey:@"mobile_number"];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    [manager GET:[NSString stringWithFormat:@"%@send-otp",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject){
        NSLog(@"JSON: %@", responseObject);
        
        if ([[responseObject valueForKey:@"status"] intValue]==1) {
            otp=[responseObject valueForKey:@"otp"];
            [self MoveNext];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                            message:@"Mobile No Already Exist, Please Try Another Number."
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }
        
        
        [APP_DELEGATE hideLoadingView];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];
    }];
    
}
/*
-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}
*/
/*
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if ([string isEqualToString:@""]) {
        
    }
    else{
        NSCharacterSet *set = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"];
        
        if ([string rangeOfCharacterFromSet:set].location == NSNotFound) {
            return NO;
        }
    }
    
    
    
    return YES;
}*/

- (IBAction)btn_STUDENT_A:(id)sender {
     _img_down_arrow.image = [UIImage imageNamed: @"left-down-arrow.jpg"];
    if (studimg==nil) {
        _img_teacher.image=[UIImage imageNamed:[arrimage objectAtIndex:1]];
    }
    else
    {
        _img_student.image=studimg;
        _img_teacher.image=[UIImage imageNamed:[arrimage objectAtIndex:1]];
    }
    
}

- (IBAction)btn_TEACHER_A:(id)sender {
    _img_down_arrow.image = [UIImage imageNamed: @"right-down-arrow.jpg"];
    if (teachimg==nil) {
        _img_student.image=[UIImage imageNamed:[arrimage objectAtIndex:0]];
    }
    else
    {
        _img_teacher.image=teachimg;
        _img_student.image=[UIImage imageNamed:[arrimage objectAtIndex:0]];
    }
    
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}
//Image Picker
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
   
    
    NSURL *imageURL = [info valueForKey:UIImagePickerControllerReferenceURL];
    PHAsset *phAsset = [[PHAsset fetchAssetsWithALAssetURLs:@[imageURL] options:nil] lastObject];
    NSString *imageName = [phAsset valueForKey:@"filename"];
    
    UIImage *photo = [info valueForKey:UIImagePickerControllerOriginalImage];
    
    NSLog(@"Picked image: %@ width: %f x height: %f",imageName, photo.size.width, photo.size.height);
    

    
    NSData *data1 = UIImagePNGRepresentation(_img_down_arrow.image);
    NSData *data2 = UIImagePNGRepresentation([UIImage imageNamed:@"left-down-arrow.jpg"]);
                                              
    if ([data1 isEqual:data2])
    {
        _img_student.image=chosenImage;
        studimg=chosenImage;
       
    }else{
        _img_teacher.image=chosenImage;
        teachimg=chosenImage;
    }
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
  
    
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}
- (IBAction)btn_NEXT_A:(id)sender {
    [self.view endEditing:YES];
    
    if ([_txt_email validate]&&[_txt_fname validate]&&[_txt_fname validate]&&[_txt_lname validate]) {
        
        if (_dictdata==nil) {
            _dictdata=[[NSMutableDictionary alloc]init];
        }
        
        NSData *data1 = UIImagePNGRepresentation(_img_down_arrow.image);
        NSData *data2 = UIImagePNGRepresentation([UIImage imageNamed:@"left-down-arrow.jpg"]);
        
        if ([data1 isEqual:data2])
        {
            [_dictdata setValue:@"0" forKey:@"imgflag"];
            [_dictdata setValue:studimg forKey:@"studimg"];
            
        }else{
            [_dictdata setValue:@"1" forKey:@"imgflag"];
            [_dictdata setValue:teachimg forKey:@"teachimg"];
        }
        
        
        if ([[_dictdata valueForKey:@"imgflag"] intValue]==0) {
            if ([_dictdata valueForKey:@"studimg"]==nil) {
               // UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Image not present" message:@"Please Choose Student image first." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
               // [alert show];
                if (isInternet) {
                    [self CallRegister];
                }
                else
                {
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Alert" message:@"Internet is not available." preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                    }];
                    [alert addAction:defaultAction];
                    [self presentViewController:alert animated:YES completion:nil];
                }
            }
            else
            {
                if (isInternet) {
                    [self CallRegister];
                }
                else
                {
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Alert" message:@"Internet is not available." preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                        
                        // Enter code here
                    }];
                    [alert addAction:defaultAction];
                    
                    // Present action where needed
                    [self presentViewController:alert animated:YES completion:nil];
                }
                
            }
        }
        else
        {
            if ([_dictdata valueForKey:@"teachimg"]==nil) {
                //UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Image not present" message:@"Please Choose teacher image first." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                //[alert show];
                if (isInternet) {
                    [self CallRegister];
                }
                else
                {
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Alert" message:@"Internet is not available." preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                        
                        // Enter code here
                    }];
                    [alert addAction:defaultAction];
                    
                    // Present action where needed
                    [self presentViewController:alert animated:YES completion:nil];
                }
            }
            else
            {
                if (isInternet) {
                    [self CallRegister];
                }
                else
                {
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Alert" message:@"Internet is not available." preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                        
                        // Enter code here
                    }];
                    [alert addAction:defaultAction];
                    
                    // Present action where needed
                    [self presentViewController:alert animated:YES completion:nil];
                }
            }
        }
    }
}
-(void)MoveNext
{
    [APP_DELEGATE showLoadingView:@""];
    double delayInSeconds = 1.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        NSLog(@"Do some work");
        OTPVC * next = [self.storyboard instantiateViewControllerWithIdentifier:@"OTPVC"];
        
        [_dictdata setValue:_txt_fname.text forKey:@"FirstName"];
        [_dictdata setValue:_txt_lname.text forKey:@"LastName"];
        [_dictdata setValue:_txt_email.text forKey:@"EmailID"];
        [_dictdata setValue:_txt_mobile_no.text forKey:@"RegMobNo"];
        [_dictdata setValue:otp forKey:@"MOTP"];
        
        //==========
        
        if (![_dictdata valueForKey:@"StudentKey"]) {
            [_dictdata setValue:@"" forKey:@"ActiveKey"];
            [_dictdata setValue:@"" forKey:@"BatchID"];
            [_dictdata setValue:@"" forKey:@"BoardID"];
            [_dictdata setValue:@"" forKey:@"BoardName"];
            [_dictdata setValue:@"" forKey:@"BranchID"];
            [_dictdata setValue:@"" forKey:@"ClassID"];
            [_dictdata setValue:@"" forKey:@"KeyType"];
            [_dictdata setValue:@"" forKey:@"MediumID"];
            [_dictdata setValue:@"" forKey:@"MediumName"];
            [_dictdata setValue:@"" forKey:@"StandardID"];
            [_dictdata setValue:@"" forKey:@"StandardName"];
            [_dictdata setValue:@"" forKey:@"StudentID"];
            [_dictdata setValue:@"0" forKey:@"StudentKey"];
        }
        
        //===============
        
        next.dictdata=_dictdata;
        [APP_DELEGATE hideLoadingView];
        [self.navigationController pushViewController:next animated:YES];
    });
}

-(BOOL)textFieldShouldReturn:(UITextField*)textField
{
    NSInteger nextTag = textField.tag + 1;
    // Try to find next responder
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else {
        // Not found, so remove keyboard.
        [textField resignFirstResponder];
    }
    return NO; // We do not want UITextField to insert line-breaks.
}

- (IBAction)btn_BACK:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btn_CHOOSE_IMG:(id)sender {
    
    NSLog(@"CHOOSE IMAGE!!!");
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:NULL];
}
@end
