//
//  PDFView.m
//  iScoreApp
//
//  Created by ADMIN-Khushal on 6/4/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "PDFView.h"
#import "CollectionView.h"
#import "TableviewCell.h"
#import "QuePaperPDFVC.h"
#import "QueAnsPaperPDFVC.h"

//#import "PaperTypeModel.h"
#import "SetPaperModel.h"
#import "ApplicationConst.h"
#import "SetPaperQuestionModel.h"
#import "UIColor+CL.h"
#import "ReviseModel.h"
#import "SetPaperModel.h"



@interface PDFView ()
@property (nonatomic, assign) CGFloat lastContentOffset;

@end

@implementation PDFView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [_colle_vw registerNib:[UINib nibWithNibName:@"CollectionView" bundle:nil] forCellWithReuseIdentifier:@"Cell"];
    
    _btn_answerp.layer.cornerRadius=2;
    _btn_questionp.layer.cornerRadius=2;
    
    NSLog(@"arrChapterIDs===%@",_arrChapterIDs);
    arrSetPaperTemp=[[NSMutableArray alloc]init];
    arrPagging=[[NSMutableArray alloc]init];
    
    _lbl_currque.text=[NSString stringWithFormat:@"Page 1"];
    
    QueCount=0;
    
    isExpand=[[NSMutableArray alloc]init];
    
    
    NSLog(@"IS FROM %@",_isFrom);
    
    if ([_isFrom isEqualToString:@"3"]) {
        
        if ([_SearchPaper isEqualToString:@"YES"]) {
            [self CallSearchPaperForSetPaper];
            
            arrQueCount=[[NSMutableArray alloc]init];
            
            
            for (int i=0; i<arrSetPaperTemp.count; i++) {
                [isExpand addObject:[NSString stringWithFormat:@"0"]];
            }
        }
        else
        {
            ///Setpaper
            NSLog(@"paperType %@",_paperType);
            NSLog(@"Chapter List %@",_chapterList);
            NSLog(@"PaperDuration %@",_paperType);
            
            
            _arrChapterIDs=[NSMutableArray arrayWithArray:[self convertToArrayFromCommaSeparated:_chapterList]];
            NSLog(@"_arrChapterIDs %@",_arrChapterIDs);
            
            [self CallSetPaper];
            [self CallChapters];
            [self CallSetPaperQuesType];
            
            
            
            arrQueCount=[[NSMutableArray alloc]init];
            
            
            for (int i=0; i<arrSetPaperTemp.count; i++) {
                [isExpand addObject:[NSString stringWithFormat:@"0"]];
            }
        }
        
        
    }
    else if ([_isFrom isEqualToString:@"2"]) {
        ///Ready Paper
        [self CallExamTPattern1];
        [self CallExamTPattenDetails];
        [self CallChapters];
        [self CallMasterQuesrionwithChapter];
        [self CallPrilimPaper];
        [self CallPagingArr];
        
        arrQueCount=[[NSMutableArray alloc]init];
        
        int k=1;
        for (int i=0; i<arrPrilimPaper.count; i++) {
            [isExpand addObject:[NSString stringWithFormat:@"0"]];
            if ([[[arrPrilimPaper objectAtIndex:i] isQuestion] intValue]) {
                [arrQueCount addObject:[NSString stringWithFormat:@"%d",k]];
                k++;
            }
            else
            {
                [arrQueCount addObject:@"0"];
            }
        }
    }
    else if([_isFrom isEqualToString:@"1"])
    {
        ///Prelim paper
        
        if ([_SearchPaper isEqualToString:@"YES"]) {
            SQPID=_SQPID;
            [self CallPrilimPaper];
            [self CallPagingArr];
            
            arrQueCount=[[NSMutableArray alloc]init];
            
            int k=1;
            for (int i=0; i<arrPrilimPaper.count; i++) {
                [isExpand addObject:[NSString stringWithFormat:@"0"]];
                if ([[[arrPrilimPaper objectAtIndex:i] isQuestion] intValue]) {
                    [arrQueCount addObject:[NSString stringWithFormat:@"%d",k]];
                    k++;
                }
                else
                {
                    [arrQueCount addObject:@"0"];
                }
            }
        }
        else
        {
            
            [self CallExamTID];
            [self CallExamTPattern];
            [self CallExamTPattern1];
            [self CallExamTPattenDetails];
            
            
            
            arrQueCount=[[NSMutableArray alloc]init];
            
            int k=1;
            for (int i=0; i<arrPrilimPaper.count; i++) {
                [isExpand addObject:[NSString stringWithFormat:@"0"]];
                if ([[[arrPrilimPaper objectAtIndex:i] isQuestion] intValue]) {
                    [arrQueCount addObject:[NSString stringWithFormat:@"%d",k]];
                    k++;
                }
                else
                {
                    [arrQueCount addObject:@"0"];
                }
            }
        }
        
        
    }
    else if ([_isFrom isEqualToString:@"Revise"])
    {
        //Revise
        
        //QUE ANS BUTTON
        _btn_questionp.hidden=YES;
        [_btn_answerp setTitle:@"PREVIEW Q&A" forState:UIControlStateNormal];
        _btn_answerp.layer.backgroundColor=[UIColor colorWithHex:0x075584].CGColor;
        _lbl_view_title.text=[NSString stringWithFormat:@"REVISION"];//Generate Paper
        
        [self CallQuestionType];
        
    }
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)CallSetPaper
{
    
    NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
    NSUserDefaults *stander=[[NSUserDefaults alloc]init];
    [dict setValue:[stander valueForKeyPath:@"ParseData.StudentID"] forKey:@"StudentID"];
    [dict setValue:[NSString stringWithFormat:@"%@",_subjectID] forKey:@"SubjectID"];
    [dict setValue:@"3" forKey:@"PaperTypeID"];
    [dict setValue:@"0" forKey:@"ExamTypePatternID"];
    [dict setValue:_pDuration forKey:@"Duration"];
    [dict setValue:_totMarks forKey:@"TotalMarks"];
    [dict setValue:_totMarks forKey:@"TotalMarks"];
    [dict setValue:[APP_DELEGATE getCurrentDate] forKey:@"CreatedOn"];
    [MyModel insertInto_papertype:dict :@"student_question_paper"];
    
    int lastID = [MyModel getLastInsertedRowID];
    SQPID=[NSString stringWithFormat:@"%d",lastID];
    
}
-(void)CallSetPaperQuesType
{
    for (int i=0; i<_paperType.count; i++) {
        SetPaperModel *PTM=[_paperType objectAtIndex:i];
        
        NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
        [dict setValue:SQPID forKey:@"StudentQuestionPaperID"];
        [dict setValue:[NSString stringWithFormat:@"%@",PTM.QuestionTypeID] forKey:@"QuestionTypeID"];
        [dict setValue:[NSString stringWithFormat:@"%@",PTM.Txttoask] forKey:@"TotalAsk"];
        [dict setValue:[NSString stringWithFormat:@"%@",PTM.Txttoans] forKey:@"ToAnswer"];
        [dict setValue:[NSString stringWithFormat:@"%d",[PTM.Marks intValue]*[PTM.Txttoans intValue]] forKey:@"TotalMark"];
        [MyModel insertInto_papertype:dict :@"student_set_paper_question_type"];
        
        int lastID = [MyModel getLastInsertedRowID];
        
        myDbQueris =[[MyDBQueries alloc]init];
        myDBParser=[[MyDBResultParser alloc]init];
        filePath = [MyModel getFilePath];
        
        NSLog(@"File Directory : -- %@",filePath);
        
        FMResultSet *rs = [MyModel selectQuery:[myDbQueris getSetPaperMasterQue:_chapterList :[dict valueForKey:@"QuestionTypeID"] :PTM.Txttoask]];
        
        arrSetMQuestion=[[NSMutableArray alloc]init];
        
        while ([rs next]) {
            MasterQueModel *SubModel = [[MasterQueModel alloc] init];
            [arrSetMQuestion  addObject:[myDBParser parseDBResult:rs :SubModel]];
            
        }
        
        NSString *Mstr=[[NSString alloc]init];
        
        for (MasterQueModel* myclass in arrSetMQuestion) {
            NSLog(@"This Schedule contains class: %@ withValue: %@", myclass, myclass.MQuestionID);
            Mstr=myclass.MQuestionID;
            //[arrMQueId addObject:myclass.MQuestionID];
            NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
            [dict setValue:[NSString stringWithFormat:@"%d",lastID] forKey:@"StudentSetPaperQuestionTypeID"];
            [dict setValue:[NSString stringWithFormat:@"%@",Mstr] forKey:@"MQuestionID"];
            
            [MyModel insertInto_papertype:dict :@"student_set_paper_detail"];
        }
        
        
        FMResultSet *result = [MyModel selectQuery:[myDbQueris getStudentSetPaperQueType:[NSString stringWithFormat:@"%d",lastID]]];
        
        while ([result next]) {
            SetPaperQuestionModel *SubModel = [[SetPaperQuestionModel alloc] init];
            [arrSetPaperTemp  addObject:[myDBParser parseDBResult:result :SubModel]];
        }
        //[arrPagging addObject:arrSubPagging];
    }
    
    NSLog(@"arrSetPaperTemp ===%@",arrSetPaperTemp);
    
    arrPagging=[[NSMutableArray alloc]init];
    arrSubPagging=[[NSMutableArray alloc]init];
    int page=[[[arrSetPaperTemp objectAtIndex:0] QuestionTypeID] intValue];
    
    for (int i=0; i<arrSetPaperTemp.count; i++) {
        SetPaperQuestionModel *PPM =[arrSetPaperTemp objectAtIndex:i];
        
        if (page==[PPM.QuestionTypeID intValue]) {
            [arrSubPagging addObject:PPM];
        }else
        {
            [arrPagging addObject:arrSubPagging];
            arrSubPagging=[[NSMutableArray alloc]init];
            page=[PPM.QuestionTypeID intValue];
            [arrSubPagging addObject:PPM];
        }
    }
    [arrPagging addObject:arrSubPagging];
    
    NSLog(@"======  %@",arrPagging);
    
}


-(void)CallSearchPaperForSetPaper
{
    myDbQueris =[[MyDBQueries alloc]init];
    myDBParser=[[MyDBResultParser alloc]init];
    filePath = [MyModel getFilePath];
    
    
    for (int i=0; i<_arrSQPID.count; i++) {
        FMResultSet *result = [MyModel selectQuery:[myDbQueris getStudentSetPaperQueType:[_arrSQPID objectAtIndex:i]]];
        
        while ([result next]) {
            SetPaperQuestionModel *SubModel = [[SetPaperQuestionModel alloc] init];
            [arrSetPaperTemp  addObject:[myDBParser parseDBResult:result :SubModel]];
        }
    }
    
    
    
    NSLog(@"arrSetPaperTemp ===%@",arrSetPaperTemp);
    
    arrPagging=[[NSMutableArray alloc]init];
    arrSubPagging=[[NSMutableArray alloc]init];
    int page=[[[arrSetPaperTemp objectAtIndex:0] QuestionTypeID] intValue];
    
    for (int i=0; i<arrSetPaperTemp.count; i++) {
        SetPaperQuestionModel *PPM =[arrSetPaperTemp objectAtIndex:i];
        
        if (page==[PPM.QuestionTypeID intValue]) {
            [arrSubPagging addObject:PPM];
        }else
        {
            [arrPagging addObject:arrSubPagging];
            arrSubPagging=[[NSMutableArray alloc]init];
            page=[PPM.QuestionTypeID intValue];
            [arrSubPagging addObject:PPM];
        }
    }
    [arrPagging addObject:arrSubPagging];
    
    NSLog(@"======  %@",arrPagging);
}


-(void)CallExamTID
{
    myDbQueris =[[MyDBQueries alloc]init];
    myDBParser=[[MyDBResultParser alloc]init];
    filePath = [MyModel getFilePath];
    
    NSLog(@"File Directory : -- %@",filePath);
    
    FMResultSet *rs = [MyModel selectQuery:[myDbQueris getExamtype_subject:[NSString stringWithFormat:@"%@",_subjectID]]];
    
    arrExamTID=[[NSMutableArray alloc]init];
    
    while ([rs next]) {
        ExamTypeSubjectModel *SubModel = [[ExamTypeSubjectModel alloc] init];
        [arrExamTID  addObject:[myDBParser parseDBResult:rs :SubModel]];
    }
    NSLog(@"arr-- %@",arrExamTID);
}

-(void)CallExamTPattern
{
    myDbQueris =[[MyDBQueries alloc]init];
    myDBParser=[[MyDBResultParser alloc]init];
    filePath = [MyModel getFilePath];
    
    NSMutableArray *arrTemp=[[NSMutableArray alloc]init];
    for (int i=0; i<arrExamTID.count; i++) {
        ExamTypeSubjectModel *ETId = [arrExamTID objectAtIndex:i];
        [arrTemp addObject:ETId.ExamTypeID];
    }
    
    
    FMResultSet *rs = [MyModel selectQuery:[myDbQueris getExamPatten:[NSString stringWithFormat:@"%@",_subjectID] :[arrTemp componentsJoinedByString:@","]:@"1"]];
    arrExamTPatten=[[NSMutableArray alloc]init];
    while ([rs next]) {
        ExamTypeSubjectModel *SubModel = [[ExamTypeSubjectModel alloc] init];
        [arrExamTPatten  addObject:[myDBParser parseDBResult:rs :SubModel]];
    }
    NSLog(@"arr-- %@",arrExamTPatten);
    
    
    
    
}

-(void)CallExamTPattern1
{
    myDbQueris =[[MyDBQueries alloc]init];
    myDBParser=[[MyDBResultParser alloc]init];
    filePath = [MyModel getFilePath];
    
    NSMutableArray *arrTemp=[[NSMutableArray alloc]init];
    for (int i=0; i<arrExamTPatten.count; i++) {
        ExamTypeSubjectModel *ETId = [arrExamTPatten objectAtIndex:i];
        [arrTemp addObject:ETId.ExamTypeID];
    }
    
    if ([_isFrom isEqualToString:@"2"]) {
        FMResultSet *rs = [MyModel selectQuery:[myDbQueris getExamTypePatten:[NSString stringWithFormat:@"%@",_subjectID] :_examTypeID]];
        arrExamTPatten1=[[NSMutableArray alloc]init];
        while ([rs next]) {
            ETPDetailsModel *SubModel = [[ETPDetailsModel alloc] init];
            [arrExamTPatten1  addObject:[myDBParser parseDBResult:rs :SubModel]];
        }
        NSLog(@"arr-- %@",arrExamTPatten1);
    }
    else
    {
        FMResultSet *rs = [MyModel selectQuery:[myDbQueris getExamTypePatten:[NSString stringWithFormat:@"%@",_subjectID] :[arrTemp componentsJoinedByString:@","]]];
        arrExamTPatten1=[[NSMutableArray alloc]init];
        while ([rs next]) {
            ETPDetailsModel *SubModel = [[ETPDetailsModel alloc] init];
            [arrExamTPatten1  addObject:[myDBParser parseDBResult:rs :SubModel]];
        }
        NSLog(@"arr-- %@",arrExamTPatten1);
    }
}


-(void)CallExamTPattenDetails
{
    myDbQueris =[[MyDBQueries alloc]init];
    myDBParser=[[MyDBResultParser alloc]init];
    filePath = [MyModel getFilePath];
    
    NSMutableArray *arrTemp=[[NSMutableArray alloc]init];
    for (int i=0; i<arrExamTPatten1.count; i++) {
        ExamTypeSubjectModel *ETId = [arrExamTPatten1 objectAtIndex:i];
        [arrTemp addObject:ETId.ExamTypePatternID];
    }
    
    FMResultSet *rs = [MyModel selectQuery:[myDbQueris getExamTypePattenDetails:[arrTemp componentsJoinedByString:@","]]];
    arrExamTDetails=[[NSMutableArray alloc]init];
    while ([rs next]) {
        ETPDetailsModel *SubModel = [[ETPDetailsModel alloc] init];
        [arrExamTDetails  addObject:[myDBParser parseDBResult:rs :SubModel]];
    }
    NSLog(@"arrExamTDetails-- %@",arrExamTDetails);
    
    if (arrExamTDetails.count < 1) {
        NSLog(@"DATA NOT AVAILABLE");
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                        message:@"No Questions..." delegate:self cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil, nil];
        [alert show];
        [self.navigationController popViewControllerAnimated:YES];
        
    }
    else
    {
        
        if ([_isFrom isEqualToString:@"2"]) {
            NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
            [dict setValue:[arrTemp componentsJoinedByString:@","] forKey:@"ExamTypePatternID"];
            NSUserDefaults *stander=[[NSUserDefaults alloc]init];
            [dict setValue:[stander valueForKeyPath:@"ParseData.StudentID"] forKey:@"StudentID"];
            [dict setValue:[NSString stringWithFormat:@"%@",_subjectID] forKey:@"SubjectID"];
            [dict setValue:@"2" forKey:@"PaperTypeID"];
            [dict setValue:[APP_DELEGATE getCurrentDate] forKey:@"CreatedOn"];
            [MyModel insertInto_papertype:dict :@"student_question_paper"];
        }else
        {
            NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
            [dict setValue:[arrTemp componentsJoinedByString:@","] forKey:@"ExamTypePatternID"];
            NSUserDefaults *stander=[[NSUserDefaults alloc]init];
            [dict setValue:[stander valueForKeyPath:@"ParseData.StudentID"] forKey:@"StudentID"];
            [dict setValue:[NSString stringWithFormat:@"%@",_subjectID] forKey:@"SubjectID"];
            [dict setValue:@"1" forKey:@"PaperTypeID"];
            [dict setValue:[APP_DELEGATE getCurrentDate] forKey:@"CreatedOn"];
            [MyModel insertInto_papertype:dict :@"student_question_paper"];
        }
        
        
        int lastID = [MyModel getLastInsertedRowID];
        SQPID=[NSString stringWithFormat:@"%d",lastID];
        
        [self CallMasterQuesrion];
        [self CallPrilimPaper];
        [self CallPagingArr];
        
    }
    
}

-(void)CallMasterQuesrion
{
    int lastID = [MyModel getLastInsertedRowID];
    SQPID=[NSString stringWithFormat:@"%d",lastID];
    [[NSUserDefaults standardUserDefaults] setValue:SQPID forKey:@"SQPID"];
    NSMutableArray *arrMQueId=[[NSMutableArray alloc]init];
    
    
    for (int i=0; i<arrExamTDetails.count; i++) {
        if ([[[arrExamTDetails objectAtIndex:i] isQuestion] intValue]) {
            NSLog(@"1");
            
            myDbQueris =[[MyDBQueries alloc]init];
            myDBParser=[[MyDBResultParser alloc]init];
            filePath = [MyModel getFilePath];
            
            FMResultSet *rs = [MyModel selectQuery:[myDbQueris getMasterQuestion:[NSString stringWithFormat:@"%@",_subjectID] : [[arrExamTDetails objectAtIndex:i] QuestionTypeID] :[arrMQueId componentsJoinedByString:@","]]];
            
            arrMQuestion=[[NSMutableArray alloc]init];
            while ([rs next]) {
                MasterQueModel *SubModel = [[MasterQueModel alloc] init];
                [arrMQuestion  addObject:[myDBParser parseDBResult:rs :SubModel]];
                
            }
            NSString *Mstr=[[NSString alloc]init];
            for (MasterQueModel* myclass in arrMQuestion) {
                NSLog(@"This Schedule contains class: %@ withValue: %@", myclass, myclass.MQuestionID);
                Mstr=myclass.MQuestionID;
                [arrMQueId addObject:myclass.MQuestionID];
            }
            
            NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
            [dict setValue:[NSString stringWithFormat:@"%d",lastID] forKey:@"StudentQuestionPaperID"];
            [dict setValue:[[arrExamTDetails objectAtIndex:i] ExamTypePatternDetailID] forKey:@"ExamTypePatternDetailID"];
            [dict setValue:[NSString stringWithFormat:@"%@",Mstr] forKey:@"MQuestionID"];
            
            [MyModel insertInto_papertype:dict :@"student_question_paper_detail"];
            
            NSLog(@"arrMQuestion---- %@",arrMQuestion);
        }
        else
        {
            NSLog(@"0");
        }
        
    }
    
}

-(void)CallMasterQuesrionwithChapter
{
    //int lastID = [MyModel getLastInsertedRowID];
    //SQPID=[NSString stringWithFormat:@"%d",lastID];
    [[NSUserDefaults standardUserDefaults] setValue:SQPID forKey:@"SQPID"];
    NSMutableArray *arrMQueId=[[NSMutableArray alloc]init];
    
    
    for (int i=0; i<arrExamTDetails.count; i++) {
        if ([[[arrExamTDetails objectAtIndex:i] isQuestion] intValue]) {
            NSLog(@"1");
            myDbQueris =[[MyDBQueries alloc]init];
            myDBParser=[[MyDBResultParser alloc]init];
            filePath = [MyModel getFilePath];
            
            FMResultSet *rs = [MyModel selectQuery:[myDbQueris getMasterQuestionwithChapter:[[arrExamTDetails objectAtIndex:i] QuestionTypeID] : [_arrChapterIDs componentsJoinedByString:@","] :[arrMQueId componentsJoinedByString:@","]]];
            
            arrMQuestion=[[NSMutableArray alloc]init];
            while ([rs next]) {
                MasterQueModel *SubModel = [[MasterQueModel alloc] init];
                [arrMQuestion  addObject:[myDBParser parseDBResult:rs :SubModel]];
            }
            NSString *Mstr=[[NSString alloc]init];
            for (MasterQueModel* myclass in arrMQuestion) {
                NSLog(@"This Schedule contains class: %@ withValue: %@", myclass, myclass.MQuestionID);
                Mstr=myclass.MQuestionID;
                [arrMQueId addObject:myclass.MQuestionID];
            }
            
            NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
            [dict setValue:[NSString stringWithFormat:@"%@",SQPID] forKey:@"StudentQuestionPaperID"];
            [dict setValue:[[arrExamTDetails objectAtIndex:i] ExamTypePatternDetailID] forKey:@"ExamTypePatternDetailID"];
            [dict setValue:[NSString stringWithFormat:@"%@",Mstr] forKey:@"MQuestionID"];
            [MyModel insertInto_papertype:dict :@"student_question_paper_detail"];
            NSLog(@"arrMQuestion---- %@",arrMQuestion);
            
        }
        else
        {
            NSLog(@"0");
        }
    }
}


-(void)CallPrilimPaper
{
    myDbQueris =[[MyDBQueries alloc]init];
    myDBParser=[[MyDBResultParser alloc]init];
    filePath = [MyModel getFilePath];
    
    FMResultSet *rs = [MyModel selectQuery:[myDbQueris getPrilimPaper:SQPID]];
    
    arrPrilimPaper=[[NSMutableArray alloc]init];
    while ([rs next]) {
        PrilimPaperModel *SubModel = [[PrilimPaperModel alloc] init];
        [arrPrilimPaper  addObject:[myDBParser parseDBResult:rs :SubModel]];
        
    }
}

-(void)CallPagingArr
{
    
    if (arrPrilimPaper.count < 1) {
        NSLog(@"DATA NOT AVAILABLE");
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                        message:@"No Questions..." delegate:self cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil, nil];
        [alert show];
        [self.navigationController popViewControllerAnimated:YES];
    }
    else{
        arrPagging=[[NSMutableArray alloc]init];
        arrSubPagging=[[NSMutableArray alloc]init];
        int page=[[[arrPrilimPaper objectAtIndex:0] PageNo] intValue];
        
        for (int i=0; i<arrPrilimPaper.count; i++) {
            PrilimPaperModel *PPM =[arrPrilimPaper objectAtIndex:i];
            
            if (page==[PPM.PageNo intValue]) {
                [arrSubPagging addObject:PPM];
            }else
            {
                [arrPagging addObject:arrSubPagging];
                arrSubPagging=[[NSMutableArray alloc]init];
                page=[PPM.PageNo intValue];
                [arrSubPagging addObject:PPM];
            }
        }
        [arrPagging addObject:arrSubPagging];
        NSLog(@"======  %@",arrPagging);
    }
    
}

-(void)CallChapters
{
    
    for (int i=0; i<_arrChapterIDs.count; i++) {
        NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
        [dict setValue:SQPID forKey:@"StudentQuestionPaperID"];
        [dict setValue:[NSString stringWithFormat:@"%@",[_arrChapterIDs objectAtIndex:i]] forKey:@"ChapterID"];
        
        [MyModel insertInto_papertype:dict :@"student_question_paper_chapter"];
        
    }
    
}


////////Revise

-(void)CallQuestionType
{
    
    myDbQueris =[[MyDBQueries alloc]init];
    myDBParser=[[MyDBResultParser alloc]init];
    filePath = [MyModel getFilePath];
    
    
    arrPagging=[[NSMutableArray alloc]init];
    arrSubPagging=[[NSMutableArray alloc]init];
    
    
    NSLog(@"File Directory : -- %@",filePath);
    // -(NSString*)getQuestionTypes :(NSString*)subjectID :(NSString*)chapterList
    // -(NSString*)getReviseQuetion :(NSString*)queType :(NSString*)chapterList
    
    for (int i=0; i<_arrSelectedQue.count; i++) {
        
        SetPaperModel *QueType=[_arrSelectedQue objectAtIndex:i];
        
        FMResultSet *rs = [MyModel selectQuery:[myDbQueris getReviseQuetion:QueType.QuestionTypeID :_chapterList]];
        
        
        arrSubPagging=[[NSMutableArray alloc]init];
        
        
        while ([rs next]) {
            ReviseModel *SubModel = [[ReviseModel alloc] init];
            [arrSubPagging  addObject:[myDBParser parseDBResult:rs :SubModel]];
        }
        NSLog(@"arr-- %@",arrSubPagging);
        [arrPagging addObject:arrSubPagging];
    }
    
    
    NSLog(@"arrPagging --%@",arrPagging);
    
    
    
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    _lbl_totque.text=[NSString stringWithFormat:@"/%lu",(unsigned long)arrPagging.count];
    return arrPagging.count;
}


- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CollectionView *cell=(CollectionView *)[collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    
    //_lbl_currque.text=[NSString stringWithFormat:@"Page %ld",indexPath.row+1];
    
    //NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"TableviewCell" owner:self options:nil];
    //cell.tbl_collection = (TableviewCell *)[nib objectAtIndex:0];
    cell.tbl_collection.delegate=self;
    cell.tbl_collection.dataSource = self;
    _coll_tbl=cell.tbl_collection;
    arrSubPagging =[[NSMutableArray alloc]init];
    arrSubPagging=[arrPagging objectAtIndex:indexPath.row];
    [self CallisExpand:indexPath.row];
    [cell.tbl_collection reloadData];
    return cell;
    
    
}

-(void)CallisExpand :(NSInteger)sender
{
    
    isExpand=[[NSMutableArray alloc]init];
    
    for (int i=0; arrSubPagging.count>i; i++) {
        [isExpand addObject:[NSString stringWithFormat:@"0"]];
    }
    
    NSLog(@"isExpand.count=== %lu",(unsigned long)isExpand.count);
    
    
}




//////  =======================    ///////
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if ([_isFrom isEqualToString:@"Revise"]) {
        return arrSubPagging.count;
    }
    else
    {
        return arrSubPagging.count;
    }
    
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    
    if ([_isFrom isEqualToString:@"Revise"])
    {
        if (indexPath.row<1) {
            
            SetPaperModel *SPM=[_arrSelectedQue objectAtIndex:indexPath.row];
            
            static NSString *CellIdentifier = @"CellHeader";
            TableviewCell *cell = (TableviewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            cell = nil;
            if (cell == nil)
            {
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"TableviewCell" owner:self options:nil];
                cell = (TableviewCell *)[nib objectAtIndex:1];
            }
            
            
            //QueCount++;
            cell.lbl_test.text=[NSString stringWithFormat:@"%ld.",(long)indexPath.row+1];
            
            cell.btn_view_solution.tag=indexPath.row;
            [cell.btn_view_solution addTarget:self action:@selector(aButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
            
            //NSString *strtemp=[self getStringFormat:PPM.QuestionTypeText];
            //cell.lbl_header.text=strtemp;
            
            NSString *strtemp=[NSString stringWithFormat:@"%@",[self getStringFormat:SPM.QuestionType]];
            CGSize maximumLabelSize = CGSizeMake(296, FLT_MAX);
            CGSize expectedLabelSize = [strtemp sizeWithFont:cell.lbl_header.font constrainedToSize:maximumLabelSize lineBreakMode:cell.lbl_header.lineBreakMode];
            CGRect newFrame = cell.lbl_header.frame;
            newFrame.size.height = expectedLabelSize.height+15;
            cell.lbl_header.frame = newFrame;
            cell.lbl_header.text=strtemp;
            
            CGRect Frame12 = cell.vw_header.frame;
            Frame12.size.height =cell.lbl_header.frame.size.height+10;
            cell.vw_header.frame = Frame12;
            
            hdrHeit=cell.vw_header.frame.size.height;
            return cell;
        }
        else
        {
            ReviseModel *PPM =[arrSubPagging objectAtIndex:indexPath.row];
            static NSString *CellIdentifier1 = @"Cell";
            TableviewCell *cell = (TableviewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier1];
            
            cell = nil;
            if (cell == nil)
            {
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"TableviewCell" owner:self options:nil];
                cell = (TableviewCell *)[nib objectAtIndex:0];
            }
            
            
            //QueCount++;
            cell.lbl_test.text=[NSString stringWithFormat:@"%ld.",(long)indexPath.row];
            
            cell.btn_view_solution.tag=indexPath.row;
            [cell.btn_view_solution addTarget:self action:@selector(aButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
            
            if ([[isExpand objectAtIndex:indexPath.row]isEqualToString:@"0"]) {
                cell.vw2.hidden=YES;
                [cell.btn_view_solution setTitle: @"Show Solution" forState: UIControlStateNormal];
                
                
                
                
                
                
                ////////HTML REPLACE//////
                NSString *path = [[NSBundle mainBundle] pathForResource:@"A" ofType:@"html"];
                NSString *content = [NSString stringWithContentsOfFile:path encoding:NSASCIIStringEncoding error:nil];
                
                //NSLog(@"HTML===%@",content);
                NSString * strQue = [content stringByReplacingOccurrencesOfString:@"ioshtml" withString:PPM.Question];
                
                NSString * path_temp = [path stringByReplacingOccurrencesOfString:@"A.html" withString:@""];
                
                NSString * strQue1 = [strQue stringByReplacingOccurrencesOfString:@" filepath " withString:path_temp];
                
                NSString *question=[self getStringFormat:strQue1];
                CGSize maximumLabelSize = CGSizeMake(296, FLT_MAX);
                CGSize expectedLabelSize = [question sizeWithFont:cell.lbl_question.font constrainedToSize:maximumLabelSize lineBreakMode:cell.lbl_question.lineBreakMode];
                CGRect newFrame = cell.lbl_question.frame;
                newFrame.size.height = expectedLabelSize.height+40;
                cell.lbl_question.frame = newFrame;
                cell.lbl_question.text=question;
                
                ////////////////FIND AND REPLACE////////
                
                if ([PPM.Question rangeOfString:@"src"].location == NSNotFound) {
                    NSLog(@"string does not Image");
                    cell.lbl_question.hidden=NO;
                    cell.web_question.hidden=YES;
                    
                } else {
                    NSLog(@"strQue===%@",strQue);
                    cell.web_question.tag=indexPath.row;
                    cell.web_question.hidden=NO;
                    cell.lbl_question.hidden=YES;
                    NSString *strhtml=[NSString stringWithFormat:@"%@",PPM.Question];
                    filePath = [MyModel getFilePath];
                    filePath=[NSString stringWithFormat:@"file://%@",filePath];
                    NSString * strQue = [strhtml stringByReplacingOccurrencesOfString:@"http://staff.parshvaa.com" withString:filePath];
                    
                    float ff=[self extractSuitableImagesFromRawHTMLEntry:strQue];
                    NSLog(@"ff %f",ff);
                    
                    [cell.web_question loadHTMLString:strQue1 baseURL:nil];
                    
                    cell.web_question.navigationDelegate = self;
                    cell.web_question.scrollView.scrollEnabled = NO;
                    cell.web_question.userInteractionEnabled = NO;
                    cell.web_question.contentMode = UIViewContentModeScaleAspectFit;
                    
                    CGRect newFrame2 = cell.web_question.frame;
                    newFrame2.size.height =cell.web_question.scrollView.contentSize.height;
                    cell.web_question.frame = newFrame2;
                    
                    /* CGRect queSizeIncress = cell.lbl_question.frame;
                     queSizeIncress.size.height =cell.lbl_question.frame.size.height;
                     cell.lbl_question.frame = queSizeIncress;
                     */
                    NSLog(@"-----%@",strQue);
                    NSLog(@"string Image");
                }
                
                
                
                
                
                ////////
                NSString *answer=[self getStringFormat:[NSString stringWithFormat:@"<p> Solution : </p> <br> %@",PPM.Answer]];
                CGSize maximumLabelSize1 = CGSizeMake(296, FLT_MAX);
                CGSize expectedLabelSize1 = [answer sizeWithFont:cell.lbl_solution.font constrainedToSize:maximumLabelSize1 lineBreakMode:cell.lbl_solution.lineBreakMode];
                CGRect newFrame1 = cell.lbl_solution.frame;
                newFrame1.size.height = expectedLabelSize1.height+5;
                cell.lbl_solution.frame = newFrame1;
                cell.lbl_solution.text=answer;
                
                
                ////////
                
                
                CGRect newFrame2 = cell.vw2.frame;
                newFrame2.origin.y =cell.lbl_question.frame.origin.y+cell.lbl_question.frame.size.height;
                cell.vw2.frame = newFrame2;
                
                CGRect newFrame3 = cell.vw2.frame;
                newFrame3.size.height =cell.lbl_solution.frame.size.height;
                cell.vw2.frame = newFrame3;
                
                
                CGRect newFrame4 = cell.vw1.frame;
                newFrame4.size.height = cell.lbl_question.frame.size.height+cell.lbl_question.frame.origin.y;
                cell.vw1.frame = newFrame4;
                
                Hsolution=cell.vw1.frame.origin.y+cell.vw1.frame.size.height+20;
                //Wsolution=cell.vw1.frame.size.height+23;
                
                ////--------SHADOW
                [self shadowView:cell.vw1];
                
                return cell;
            }
            else
            {
                cell.vw2.hidden=NO;
                [cell.btn_view_solution setTitle: @"Hide Solution" forState: UIControlStateNormal];
                NSString *question=[self getStringFormat:PPM.Question];
                CGSize maximumLabelSize = CGSizeMake(296, FLT_MAX);
                CGSize expectedLabelSize = [question sizeWithFont:cell.lbl_question.font constrainedToSize:maximumLabelSize lineBreakMode:cell.lbl_question.lineBreakMode];
                CGRect newFrame = cell.lbl_question.frame;
                newFrame.size.height = expectedLabelSize.height+40;
                cell.lbl_question.frame = newFrame;
                cell.lbl_question.text=question;
                
                ////////
                
                NSString *answer=[self getStringFormat:[NSString stringWithFormat:@"<p> Solution : </p> <br> %@",PPM.Answer]];
                CGSize maximumLabelSize1 = CGSizeMake(296, FLT_MAX);
                CGSize expectedLabelSize1 = [answer sizeWithFont:cell.lbl_solution.font constrainedToSize:maximumLabelSize1 lineBreakMode:cell.lbl_solution.lineBreakMode];
                CGRect newFrame1 = cell.lbl_solution.frame;
                newFrame1.size.height = expectedLabelSize1.height+5;
                cell.lbl_solution.frame = newFrame1;
                cell.lbl_solution.text=answer;
                
                ////////
                
                CGRect newFrame2 = cell.vw2.frame;
                newFrame2.origin.y =cell.lbl_question.frame.origin.y+cell.lbl_question.frame.size.height;
                cell.vw2.frame = newFrame2;
                
                CGRect newFrame3 = cell.vw2.frame;
                newFrame3.size.height =cell.lbl_solution.frame.size.height;
                cell.vw2.frame = newFrame3;
                
                CGRect newFrame4 = cell.vw1.frame;
                newFrame4.size.height = cell.vw2.frame.size.height+cell.vw2.frame.origin.y;
                cell.vw1.frame = newFrame4;
                
                Wsolution=cell.vw1.frame.origin.y+cell.vw1.frame.size.height+20;
                
                ////--------SHADOW
                [self shadowView:cell.vw1];
                
                
                return cell;
            }
        }
    }
    else if ([_isFrom isEqualToString:@"3"]) {
        
        SetPaperQuestionModel *PPM =[arrSubPagging objectAtIndex:indexPath.row];
        
        static NSString *CellIdentifier1 = @"Cell";
        TableviewCell *cell = (TableviewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier1];
        
        
        cell = nil;
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"TableviewCell" owner:self options:nil];
            cell = (TableviewCell *)[nib objectAtIndex:0];
            
        }
        
        ////////
        //QueCount++;
        cell.lbl_test.text=[NSString stringWithFormat:@"%ld.",(long)indexPath.row+1];
        
        cell.btn_view_solution.tag=indexPath.row;
        [cell.btn_view_solution addTarget:self action:@selector(aButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        
        if(indexPath.row <  1) {
            /////////////// CREATE VIEW /////////////////
            UILabel *fromLabel = [[UILabel alloc]initWithFrame:CGRectMake(5, 0, 300, 25)];
            fromLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:14];
            fromLabel.textColor = [UIColor colorWithHex:0x3E3E3E];
            fromLabel.numberOfLines = 0;
            fromLabel.clipsToBounds = YES;
            fromLabel.textAlignment = NSTextAlignmentLeft;
            
            NSString *strtemp=[NSString stringWithFormat:@"%@",[self getStringFormat:PPM.QuestionType]];
            CGSize maximumLabelSize = CGSizeMake(296, FLT_MAX);
            CGSize expectedLabelSize = [strtemp sizeWithFont:fromLabel.font constrainedToSize:maximumLabelSize lineBreakMode:fromLabel.lineBreakMode];
            CGRect newFrame = fromLabel.frame;
            newFrame.size.height = expectedLabelSize.height+5;
            fromLabel.frame = newFrame;
            fromLabel.text=strtemp;
            
            UIView *newView = [[UIView alloc] initWithFrame:CGRectMake(8,0,cell.frame.size.width-38,fromLabel.frame.size.height)];
            newView.backgroundColor=[UIColor colorWithHex:0xF5F5F5];
            
            [newView addSubview:fromLabel];
            [cell addSubview:newView];
            
            CGRect newFrame5 = cell.vw1.frame;
            newFrame5.origin.y = newView.frame.size.height;
            cell.vw1.frame = newFrame5;
        }
        
        if ([[isExpand objectAtIndex:indexPath.row]isEqualToString:@"0"]) {
            cell.vw2.hidden=YES;
            [cell.btn_view_solution setTitle: @"Show Solution" forState: UIControlStateNormal];
            
            ////////HTML REPLACE//////
            NSString *path = [[NSBundle mainBundle] pathForResource:@"A" ofType:@"html"];
            NSString *content = [NSString stringWithContentsOfFile:path encoding:NSASCIIStringEncoding error:nil];
            
            //NSLog(@"HTML===%@",content);
            NSString * strQue = [content stringByReplacingOccurrencesOfString:@"ioshtml" withString:PPM.Question];
            
            NSString * path_temp = [path stringByReplacingOccurrencesOfString:@"A.html" withString:@""];
            
            NSString * strQue1 = [strQue stringByReplacingOccurrencesOfString:@" filepath " withString:path_temp];
            
            NSString *question=[self getStringFormat:strQue1];
            CGSize maximumLabelSize = CGSizeMake(296, FLT_MAX);
            CGSize expectedLabelSize = [question sizeWithFont:cell.lbl_question.font constrainedToSize:maximumLabelSize lineBreakMode:cell.lbl_question.lineBreakMode];
            CGRect newFrame = cell.lbl_question.frame;
            newFrame.size.height = expectedLabelSize.height+40;
            cell.lbl_question.frame = newFrame;
            cell.lbl_question.text=question;
            
            ////////////////FIND AND REPLACE////////
            
            if ([PPM.Question rangeOfString:@"src"].location == NSNotFound) {
                NSLog(@"string does not Image");
                cell.lbl_question.hidden=NO;
                cell.web_question.hidden=YES;
                
            } else {
                NSLog(@"strQue===%@",strQue);
                cell.web_question.tag=indexPath.row;
                cell.web_question.hidden=NO;
                cell.lbl_question.hidden=YES;
                NSString *strhtml=[NSString stringWithFormat:@"%@",PPM.Question];
                filePath = [MyModel getFilePath];
                filePath=[NSString stringWithFormat:@"file://%@",filePath];
                NSString * strQue = [strhtml stringByReplacingOccurrencesOfString:@"http://staff.parshvaa.com" withString:filePath];
                
                float ff=[self extractSuitableImagesFromRawHTMLEntry:strQue];
                NSLog(@"ff %f",ff);
                
                [cell.web_question loadHTMLString:strQue1 baseURL:nil];
                
                cell.web_question.navigationDelegate = self;
                cell.web_question.scrollView.scrollEnabled = NO;
                cell.web_question.userInteractionEnabled = NO;
                cell.web_question.contentMode = UIViewContentModeScaleAspectFit;
                
                CGRect newFrame2 = cell.web_question.frame;
                newFrame2.size.height =cell.web_question.scrollView.contentSize.height;
                cell.web_question.frame = newFrame2;
                
                CGRect queSizeIncress = cell.lbl_question.frame;
                queSizeIncress.size.height =cell.lbl_question.frame.size.height;
                cell.lbl_question.frame = queSizeIncress;
                
                NSLog(@"-----%@",strQue);
                NSLog(@"string Image");
            }
            
            
            
            ////////
            NSString *answer=[self getStringFormat:PPM.Answer];
            CGSize maximumLabelSize1 = CGSizeMake(296, FLT_MAX);
            CGSize expectedLabelSize1 = [answer sizeWithFont:cell.lbl_solution.font constrainedToSize:maximumLabelSize1 lineBreakMode:cell.lbl_solution.lineBreakMode];
            CGRect newFrame1 = cell.lbl_solution.frame;
            newFrame1.size.height = expectedLabelSize1.height+40;
            cell.lbl_solution.frame = newFrame1;
            cell.lbl_solution.text=answer;
            
            
            ////////
            
            
            CGRect newFrame2 = cell.vw2.frame;
            newFrame2.origin.y =cell.lbl_question.frame.origin.y+cell.lbl_question.frame.size.height;
            cell.vw2.frame = newFrame2;
            
            CGRect newFrame3 = cell.vw2.frame;
            newFrame3.size.height =cell.lbl_solution.frame.size.height;
            cell.vw2.frame = newFrame3;
            
            
            CGRect newFrame4 = cell.vw1.frame;
            newFrame4.size.height = cell.lbl_question.frame.size.height+cell.lbl_question.frame.origin.y;
            cell.vw1.frame = newFrame4;
            
            Hsolution=cell.vw1.frame.origin.y+cell.vw1.frame.size.height+20;
            //Wsolution=cell.vw1.frame.size.height+23;
            
            ////--------SHADOW
            [self shadowView:cell.vw1];
            
            return cell;
        }
        else
        {
            cell.vw2.hidden=NO;
            [cell.btn_view_solution setTitle: @"Hide Solution" forState: UIControlStateNormal];
            NSString *question=[self getStringFormat:PPM.Question];
            CGSize maximumLabelSize = CGSizeMake(296, FLT_MAX);
            CGSize expectedLabelSize = [question sizeWithFont:cell.lbl_question.font constrainedToSize:maximumLabelSize lineBreakMode:cell.lbl_question.lineBreakMode];
            CGRect newFrame = cell.lbl_question.frame;
            newFrame.size.height = expectedLabelSize.height+40;
            cell.lbl_question.frame = newFrame;
            cell.lbl_question.text=question;
            
            ////////
            
            NSString *answer=[self getStringFormat:PPM.Answer];
            CGSize maximumLabelSize1 = CGSizeMake(296, FLT_MAX);
            CGSize expectedLabelSize1 = [answer sizeWithFont:cell.lbl_solution.font constrainedToSize:maximumLabelSize1 lineBreakMode:cell.lbl_solution.lineBreakMode];
            CGRect newFrame1 = cell.lbl_solution.frame;
            newFrame1.size.height = expectedLabelSize1.height+20;
            cell.lbl_solution.frame = newFrame1;
            cell.lbl_solution.text=answer;
            
            ////////
            
            CGRect newFrame2 = cell.vw2.frame;
            newFrame2.origin.y =cell.lbl_question.frame.origin.y+cell.lbl_question.frame.size.height;
            cell.vw2.frame = newFrame2;
            
            CGRect newFrame3 = cell.vw2.frame;
            newFrame3.size.height =cell.lbl_solution.frame.size.height;
            cell.vw2.frame = newFrame3;
            
            CGRect newFrame4 = cell.vw1.frame;
            newFrame4.size.height = cell.vw2.frame.size.height+cell.vw2.frame.origin.y;
            cell.vw1.frame = newFrame4;
            
            Wsolution=cell.vw1.frame.origin.y+cell.vw1.frame.size.height+20;
            
            ////--------SHADOW
            [self shadowView:cell.vw1];
            
            
            return cell;
            
        }
    }
    else
    {
        PrilimPaperModel *PPM =[arrSubPagging objectAtIndex:indexPath.row];
        if ([PPM.isQuestion intValue]) {
            
            static NSString *CellIdentifier1 = @"Cell";
            TableviewCell *cell = (TableviewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier1];
            
            cell = nil;
            if (cell == nil)
            {
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"TableviewCell" owner:self options:nil];
                cell = (TableviewCell *)[nib objectAtIndex:0];
            }
            ////////
            //QueCount++;
            cell.lbl_test.text=[NSString stringWithFormat:@"%@.",[arrQueCount objectAtIndex:indexPath.row]];
            
            cell.btn_view_solution.tag=indexPath.row;
            [cell.btn_view_solution addTarget:self action:@selector(aButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
            
            
            if ([[isExpand objectAtIndex:indexPath.row]isEqualToString:@"0"]) {
                cell.vw2.hidden=YES;
                [cell.btn_view_solution setTitle: @"Show Solution" forState: UIControlStateNormal];
                
                ////////HTML REPLACE//////
                NSString *path = [[NSBundle mainBundle] pathForResource:@"A" ofType:@"html"];
                NSString *content = [NSString stringWithContentsOfFile:path encoding:NSASCIIStringEncoding error:nil];
                
                //NSLog(@"HTML===%@",content);
                
                NSString * strQue = [content stringByReplacingOccurrencesOfString:@"ioshtml" withString:PPM.Question];
                
                NSString * path_temp = [path stringByReplacingOccurrencesOfString:@"A.html" withString:@""];
                
                NSString * strQue1 = [strQue stringByReplacingOccurrencesOfString:@" filepath " withString:path_temp];
                
                NSString *question=[self getStringFormat:strQue1];
                CGSize maximumLabelSize = CGSizeMake(296, FLT_MAX);
                CGSize expectedLabelSize = [question sizeWithFont:cell.lbl_question.font constrainedToSize:maximumLabelSize lineBreakMode:cell.lbl_question.lineBreakMode];
                CGRect newFrame = cell.lbl_question.frame;
                newFrame.size.height = expectedLabelSize.height+40;
                cell.lbl_question.frame = newFrame;
                cell.lbl_question.text=question;
                
                ////////////////FIND AND REPLACE////////
                
                if ([PPM.Question rangeOfString:@"src"].location == NSNotFound) {
                    NSLog(@"string does not Image");
                    cell.lbl_question.hidden=NO;
                    cell.web_question.hidden=YES;
                    
                } else {
                    NSLog(@"strQue===%@",strQue);
                    cell.web_question.tag=indexPath.row;
                    cell.web_question.hidden=NO;
                    cell.lbl_question.hidden=YES;
                    NSString *strhtml=[NSString stringWithFormat:@"%@",PPM.Question];
                    filePath = [MyModel getFilePath];
                    filePath=[NSString stringWithFormat:@"file://%@",filePath];
                    NSString * strQue = [strhtml stringByReplacingOccurrencesOfString:@"http://staff.parshvaa.com" withString:filePath];
                    
                    float ff=[self extractSuitableImagesFromRawHTMLEntry:strQue];
                    NSLog(@"ff %f",ff);
                    
                    [cell.web_question loadHTMLString:strQue1 baseURL:nil];
                    
                    //cell.web_question.delegate = self;
                    //cell.web_question.scrollView.scrollEnabled = YES;
                    //cell.web_question.userInteractionEnabled = YES;
                    //cell.web_question.contentMode = UIViewContentModeScaleAspectFit;
                    
                    
                    CGRect newFrame2 = cell.lbl_question.frame;
                    newFrame2.size.height =cell.lbl_question.frame.size.height;
                    cell.web_question.frame = newFrame2;
                    
                    
                    
                    
                    CGRect queSizeIncress = cell.lbl_question.frame;
                    queSizeIncress.size.height =cell.lbl_question.frame.size.height+cell.lbl_question.frame.origin.y;
                    cell.lbl_question.frame = queSizeIncress;
                    
                    NSLog(@"-----%@",strQue);
                    NSLog(@"string Image");
                }
                
                
                ////////
                NSString *answer=[self getStringFormat:PPM.Answer];
                CGSize maximumLabelSize1 = CGSizeMake(296, FLT_MAX);
                CGSize expectedLabelSize1 = [answer sizeWithFont:cell.lbl_solution.font constrainedToSize:maximumLabelSize1 lineBreakMode:cell.lbl_solution.lineBreakMode];
                CGRect newFrame1 = cell.lbl_solution.frame;
                newFrame1.size.height = expectedLabelSize1.height+40;
                cell.lbl_solution.frame = newFrame1;
                cell.lbl_solution.text=answer;
                
                ////////
                
                CGRect newFrame2 = cell.vw2.frame;
                newFrame2.origin.y =cell.lbl_question.frame.origin.y+cell.lbl_question.frame.size.height;
                cell.vw2.frame = newFrame2;
                
                CGRect newFrame3 = cell.vw2.frame;
                newFrame3.size.height =cell.lbl_solution.frame.size.height;
                cell.vw2.frame = newFrame3;
                
                
                CGRect newFrame4 = cell.vw1.frame;
                newFrame4.size.height = cell.lbl_question.frame.size.height+cell.lbl_question.frame.origin.y;
                cell.vw1.frame = newFrame4;
                
                Hsolution=cell.vw1.frame.origin.y+cell.vw1.frame.size.height+20;
                //Wsolution=cell.vw1.frame.size.height+23;
                
                ////--------SHADOW
                [self shadowView:cell.vw1];
            }
            else
            {
                cell.vw2.hidden=NO;
                [cell.btn_view_solution setTitle: @"Hide Solution" forState: UIControlStateNormal];
                NSString *question=[self getStringFormat:PPM.Question];
                CGSize maximumLabelSize = CGSizeMake(296, FLT_MAX);
                CGSize expectedLabelSize = [question sizeWithFont:cell.lbl_question.font constrainedToSize:maximumLabelSize lineBreakMode:cell.lbl_question.lineBreakMode];
                CGRect newFrame = cell.lbl_question.frame;
                newFrame.size.height = expectedLabelSize.height+40;
                cell.lbl_question.frame = newFrame;
                cell.lbl_question.text=question;
                
                ////////
                
                NSString *answer=[self getStringFormat:PPM.Answer];
                CGSize maximumLabelSize1 = CGSizeMake(296, FLT_MAX);
                CGSize expectedLabelSize1 = [answer sizeWithFont:cell.lbl_solution.font constrainedToSize:maximumLabelSize1 lineBreakMode:cell.lbl_solution.lineBreakMode];
                CGRect newFrame1 = cell.lbl_solution.frame;
                newFrame1.size.height = expectedLabelSize1.height+40;
                cell.lbl_solution.frame = newFrame1;
                cell.lbl_solution.text=answer;
                
                ////////
                
                
                CGRect newFrame2 = cell.vw2.frame;
                newFrame2.origin.y =cell.lbl_question.frame.origin.y+cell.lbl_question.frame.size.height;
                cell.vw2.frame = newFrame2;
                
                CGRect newFrame3 = cell.vw2.frame;
                newFrame3.size.height =cell.lbl_solution.frame.size.height;
                cell.vw2.frame = newFrame3;
                
                CGRect newFrame4 = cell.vw1.frame;
                newFrame4.size.height = cell.vw2.frame.size.height+cell.vw2.frame.origin.y;
                cell.vw1.frame = newFrame4;
                
                
                Wsolution=cell.vw1.frame.origin.y+cell.vw1.frame.size.height+20;
                
                ////--------SHADOW
                [self shadowView:cell.vw1];
            }
            return cell;
        }
        else
        {
            static NSString *CellIdentifier = @"CellHeader";
            TableviewCell *cell = (TableviewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            cell = nil;
            if (cell == nil)
            {
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"TableviewCell" owner:self options:nil];
                cell = (TableviewCell *)[nib objectAtIndex:1];
            }
            
            //NSString *strtemp=[self getStringFormat:PPM.QuestionTypeText];
            //cell.lbl_header.text=strtemp;
            
            NSString *strtemp=[NSString stringWithFormat:@"%@%@%@",PPM.QuestionNO,PPM.SubQuestionNO,[self getStringFormat:PPM.QuestionTypeText]];
            CGSize maximumLabelSize = CGSizeMake(296, FLT_MAX);
            CGSize expectedLabelSize = [strtemp sizeWithFont:cell.lbl_header.font constrainedToSize:maximumLabelSize lineBreakMode:cell.lbl_header.lineBreakMode];
            CGRect newFrame = cell.lbl_header.frame;
            newFrame.size.height = expectedLabelSize.height+5;
            cell.lbl_header.frame = newFrame;
            cell.lbl_header.text=strtemp;
            
            
            CGRect Frame12 = cell.vw_header.frame;
            Frame12.size.height =cell.lbl_header.frame.size.height+10;
            cell.vw_header.frame = Frame12;
            
            hdrHeit=cell.vw_header.frame.size.height;
            
            return cell;
            
        }
    }
    
}

-(IBAction) aButtonTapped:(id)sender
{
    //UIButton *aButton = (UIButton *)sender;
    UIButton *btn = (UIButton *)sender;
    CGPoint origin = btn.frame.origin;
    CGPoint point = [btn.superview convertPoint:origin toView:_coll_tbl];
    NSIndexPath * indexPath = [_coll_tbl indexPathForRowAtPoint:point];
    TableviewCell *cell=(TableviewCell *)[_coll_tbl cellForRowAtIndexPath:indexPath];
    
    if ([[isExpand objectAtIndex:btn.tag]isEqualToString:@"1"]) {
        [isExpand replaceObjectAtIndex:btn.tag withObject:@"0"];
        cell.vw2.hidden=YES;
        NSLog(@"%f",cell.vw2.frame.size.height);
        
        [cell.btn_view_solution setTitle: @"Show Solution" forState: UIControlStateNormal];
        
    }
    else
    {
        [isExpand replaceObjectAtIndex:btn.tag withObject:@"1"];
        cell.vw2.hidden=NO;
        NSLog(@"%f",cell.vw2.frame.size.height);
        [cell.btn_view_solution setTitle:@"Hide Solution" forState: UIControlStateNormal];
        
    }
    
    [_coll_tbl reloadData];
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    currentIndex = _colle_vw.contentOffset.x / _colle_vw.frame.size.width;
    NSLog(@"currentIndex === %ld",(long)currentIndex);
    
    _lbl_currque.text=[NSString stringWithFormat:@"Page %ld",currentIndex+1];
    
}

-(void)moveToPreviousQuestion{
    
    NSLog(@"moveToPreviousQuestion");
    currentIndex--;
    
    if (currentIndex<0) {
        NSLog(@"AT FIRST");
        currentIndex++;
    }
    else
    {
        NSIndexPath *nextItem = [NSIndexPath indexPathForItem:currentIndex  inSection:0];
        [self.colle_vw scrollToItemAtIndexPath:nextItem
                              atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally
                                      animated:YES];
    }
    
    
}
-(void)moveToNextQuestion{
    
    NSLog(@"moveToNextQuestion");
    currentIndex++;
    
    if (arrPagging.count>currentIndex) {
        NSIndexPath *nextItem = [NSIndexPath indexPathForItem:currentIndex  inSection:0];
        
        [self.colle_vw scrollToItemAtIndexPath:nextItem
                              atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally
                                      animated:YES];
    }
    else
    {
        NSLog(@"AT LAST");
        currentIndex--;
    }
    
}



-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([_isFrom isEqualToString:@"Revise"]) {
        
        
        if (indexPath.row<1) {
            
            return 50;
        }
        else
        {
            if ([[isExpand objectAtIndex:indexPath.row]isEqualToString:@"1"]) {
                
                return Wsolution;
            }
            else
            {
                return Hsolution;
            }
        }
    }
    else if ([_isFrom isEqualToString:@"3"]) {
        SetPaperQuestionModel *PPM =[arrSubPagging objectAtIndex:indexPath.row];
        static NSString *CellIdentifier = @"Cell";
        TableviewCell *cell = (TableviewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        cell = nil;
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"TableviewCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        
        if ([[isExpand objectAtIndex:indexPath.row]isEqualToString:@"1"]) {
            
            return Wsolution;
        }
        else
        {
            return Hsolution;
        }
    }
    else
    {
        PrilimPaperModel *PPM =[arrSubPagging objectAtIndex:indexPath.row];
        if ([PPM.isQuestion intValue]) {
            
            static NSString *CellIdentifier = @"Cell";
            TableviewCell *cell = (TableviewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            cell = nil;
            if (cell == nil)
            {
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"TableviewCell" owner:self options:nil];
                cell = [nib objectAtIndex:0];
            }
            
            if ([[isExpand objectAtIndex:indexPath.row]isEqualToString:@"1"]) {
                
                return Wsolution;
            }
            else
            {
                
                return Hsolution;
                
            }
        }
        else
        {
            if ([[isExpand objectAtIndex:indexPath.row]isEqualToString:@"1"]) {
                
                return hdrHeit;
            }
            else
            {
                return hdrHeit;
            }
        }
    }
    
}



- (float)extractSuitableImagesFromRawHTMLEntry:(NSString *)searchedString {
    
    NSRange rangeOfString = NSMakeRange(0, [searchedString length]);
    //NSLog(@"searchedString: %@", searchedString);
    
    NSString *pattern = @"src=\"([^\"]+)\"";
    NSError* error = nil;
    
    NSRegularExpression* regex = [NSRegularExpression regularExpressionWithPattern:pattern options:0 error:&error];
    NSArray *matchs = [regex matchesInString:searchedString options:0 range:rangeOfString];
    NSString *imgPath=[[NSString alloc]init];
    for (NSTextCheckingResult* match in matchs) {
        imgPath=[NSString stringWithFormat:@"%@",[searchedString substringWithRange:[match rangeAtIndex:1]]];
        NSLog(@"url: %@", [searchedString substringWithRange:[match rangeAtIndex:1]]);
    }
    imgPath = [imgPath stringByReplacingOccurrencesOfString:@" "
                                                 withString:@"%20"];
    NSURL *url = [NSURL URLWithString:imgPath];
    NSData *data = [[NSData alloc] initWithContentsOfURL:url];
    UIImage *tmpImage = [[UIImage alloc] initWithData:data];
    
    NSLog(@"img -- %f",tmpImage.size.height);
    NSLog(@"img -- %@",tmpImage);
    
    return tmpImage.size.height;
    
}

-(NSString*)getStringFormat :(NSString*)strHTML
{
    
    
    //NSString *htmlString = @"..."
    NSData *textData = [strHTML dataUsingEncoding:NSUnicodeStringEncoding];
    NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc]
                                          initWithData:textData
                                          options:@{ NSDocumentTypeDocumentAttribute : NSHTMLTextDocumentType }
                                          documentAttributes:nil error:nil];
    [attrStr setAttributes:@{NSFontAttributeName : [UIFont fontWithName:@"avenir" size:14.0f]} range:NSMakeRange(0,attrStr.length)];
    
    UILabel *myLabel=[[UILabel alloc]init];
    myLabel.attributedText = attrStr;
    
    
    return myLabel.text;
}

-(void)labelHeightAsText :(NSString*)str :(UILabel*)yourLabel
{
    CGSize maximumLabelSize = CGSizeMake(296, FLT_MAX);
    CGSize expectedLabelSize = [str sizeWithFont:yourLabel.font constrainedToSize:maximumLabelSize lineBreakMode:yourLabel.lineBreakMode];
    CGRect newFrame = yourLabel.frame;
    newFrame.size.height = expectedLabelSize.height;
    yourLabel.frame = newFrame;
}

-(void)shadowView :(UIView*)viewCheck
{
    viewCheck.layer.shadowRadius  = 3.5f;
    viewCheck.layer.shadowColor   = [UIColor colorWithRed:176.f/255.f green:199.f/255.f blue:226.f/255.f alpha:1.f].CGColor;
    viewCheck.layer.shadowOffset  = CGSizeMake(0.0f, 0.0f);
    viewCheck.layer.shadowOpacity = 2.1f;
    viewCheck.layer.masksToBounds = NO;
    
}


//- (void)webViewDidFinishLoad:(WKWebView *)webView
//{
//   __block CGRect frame = webView.frame;
////    NSString *heightStrig = [webView stringByEvaluatingJavaScriptFromString:@"(document.height !== undefined) ? document.height : document.body.offsetHeight;"];
//    
//    [webView evaluateJavaScript:@"(document.height !== undefined) ? document.height : document.body.offsetHeight;" completionHandler:^(NSString *result, NSError *error)
//       {
//           
//           NSLog(@"INNER HTML: %@",result);
//            float height = result.floatValue + 10.0;
//              frame.size.height = height;
//              webView.frame = frame;
//       }];
//   
//    
//}
- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
   __block CGRect frame = webView.frame;
//    NSString *heightStrig = [webView stringByEvaluatingJavaScriptFromString:@"(document.height !== undefined) ? document.height : document.body.offsetHeight;"];
    
    [webView evaluateJavaScript:@"(document.height !== undefined) ? document.height : document.body.offsetHeight;" completionHandler:^(NSString *result, NSError *error)
       {
           
           NSLog(@"INNER HTML: %@",result);
            float height = result.floatValue + 10.0;
              frame.size.height = height;
              webView.frame = frame;
       }];
   
    
}
-(float)webViewHeight :(WKWebView*)web_view
{
   __block CGRect frame = web_view.frame;
   // NSString *heightStrig = [web_view stringByEvaluatingJavaScriptFromString:@"(document.height !== undefined) ? document.height : document.body.offsetHeight;"];
    
    [web_view evaluateJavaScript:@"(document.height !== undefined) ? document.height : document.body.offsetHeight;" completionHandler:^(NSString *result, NSError *error)
          {
              
              NSLog(@"INNER HTML: %@",result);
                float height = result.floatValue + 10.0;
                  frame.size.height = height;
                  web_view.frame = frame;
          }];
   
    
    return web_view.frame.size.height;
}

- (IBAction)btn_BACK:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btn_QUE_PAPER:(id)sender {
    
    if ([_isFrom isEqualToString:@"3"]) {
        QuePaperPDFVC *add = [[QuePaperPDFVC alloc] initWithNibName:@"QuePaperPDFVC" bundle:nil];
        add.arrPagging=arrPagging;
        add.isFrom=@"3";
        [self.navigationController pushViewController:add animated:YES];
    }
    else
    {
        QuePaperPDFVC *add = [[QuePaperPDFVC alloc] initWithNibName:@"QuePaperPDFVC" bundle:nil];
        add.arrPagging=arrPagging;
        add.isFrom=@"2";
        [self.navigationController pushViewController:add animated:YES];
    }
    
}

- (IBAction)btn_ANS_PAPER:(id)sender {
    
    if ([_isFrom isEqualToString:@"Revise"]) {
        QueAnsPaperPDFVC *add = [[QueAnsPaperPDFVC alloc] initWithNibName:@"QueAnsPaperPDFVC" bundle:nil];
        add.arrPagging=arrPagging;
        add.arrSelectedQue=_arrSelectedQue;
        add.isFrom=@"Revise";
        [self.navigationController pushViewController:add animated:YES];
    }
    else if ([_isFrom isEqualToString:@"3"]) {
        QueAnsPaperPDFVC *add = [[QueAnsPaperPDFVC alloc] initWithNibName:@"QueAnsPaperPDFVC" bundle:nil];
        add.arrPagging=arrPagging;
        add.isFrom=@"3";
        [self.navigationController pushViewController:add animated:YES];
    }
    else
    {
        QueAnsPaperPDFVC *add = [[QueAnsPaperPDFVC alloc] initWithNibName:@"QueAnsPaperPDFVC" bundle:nil];
        add.arrPagging=arrPagging;
        add.isFrom=@"2";
        [self.navigationController pushViewController:add animated:YES];
    }
    
    
}

- (IBAction)btn_PRIV:(id)sender {
    [self moveToPreviousQuestion];
}

- (IBAction)btn_NEXT:(id)sender {
    [self moveToNextQuestion];
}

- (IBAction)btn_quepaper:(id)sender {
}
-(NSArray *)convertToArrayFromCommaSeparated:(NSString*)string{
    return [string componentsSeparatedByString:@","];
}





@end
