//
//  TableviewCell.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 6/4/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>

@interface TableviewCell : UITableViewCell


@property (strong, nonatomic) IBOutlet UIView *vw1;
@property (strong, nonatomic) IBOutlet UILabel *lbl_test;
@property (strong, nonatomic) IBOutlet UIButton *btn_view_solution;
@property (strong, nonatomic) IBOutlet UILabel *lbl_question;
@property (weak, nonatomic) IBOutlet WKWebView *web_question;

@property (strong, nonatomic) IBOutlet UIView *vw2;
@property (strong, nonatomic) IBOutlet UILabel *lbl_solution;

//Cell Header
@property (weak, nonatomic) IBOutlet UIView *vw_header;


@property (weak, nonatomic) IBOutlet UILabel *lbl_header;

@end
