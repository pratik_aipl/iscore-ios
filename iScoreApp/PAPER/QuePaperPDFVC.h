//
//  QuePaperPDFVC.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 6/15/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyDBQueries.h"
#import "MyDBResultParser.h"
#import "MyModel.h"
#import "PDFView.h"
#import <WebKit/WebKit.h>

@interface QuePaperPDFVC : UIViewController
{
    MyDBQueries *myDbQueris;
    MyDBResultParser *myDBParser;
    NSString *filePath;
    NSMutableArray *arrPrilimPaper;
    NSString *HTML;
    NSString *strHTML;
    
    
}

@property ( retain , nonatomic)NSString *PaperId;
@property ( retain , nonatomic)NSString *ICPaperType;

@property (retain ,nonatomic)NSString *isFrom;
@property (retain,nonatomic)NSMutableArray *arrPagging;
@property (retain,nonatomic)NSMutableArray *arrSubPagging;

//Outlet
//@property (weak, nonatomic) IBOutlet UIWebView *web_question;
@property (strong, nonatomic) IBOutlet WKWebView *web_question;

//Action
- (IBAction)btn_BACK:(id)sender;
- (IBAction)btn_ROUTING:(id)sender;


@end
