//
//  QuePaperPDFVC.m
//  iScoreApp
//
//  Created by ADMIN-Khushal on 6/15/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "QuePaperPDFVC.h"
#import "PrilimPaperModel.h"
#import "ExamTypeSubjectModel.h"
#import "ETPDetailsModel.h"
#import "MasterQueModel.h"
#import "MyModel.h"
#import "SetPaperQuestionModel.h"
#import "Constant.h"
#import <AFNetworking/AFHTTPRequestOperationManager.h>
#import "ApplicationConst.h"
#import "ReaderDocument.h"
#import "ReaderViewController.h"




@interface QuePaperPDFVC ()



@end

@implementation QuePaperPDFVC

NSString *tdocumentDirectoryFilename11=@"";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    if ([_ICPaperType length]>1) {
        [self getData];
    }
    else
    {
        HTML=[[NSString alloc]init];
        
        
        strHTML=@"<!DOCTYPE html> <html lang=\"en\"> <head><meta name=viewport content=width=device-width, initial-scale=1.0><title>Question List</title><style type=\"text/css\">table {width: 100%; } body * { font: 100%; }.table tr td { vertical-align: top;  }.question p:first-child { margin-top: 0;}.answer p:first-child {  margin-top: 0;} body { width: 100%; height: auto; margin: 0; padding: 0; font: 12pt \"Tahoma\"; } * { box-sizing: border-box; -moz-box-sizing: border-box; }.page { width: 210mm; min-height: 297mm; padding: 0mm;  margin: 10mm auto; background: white; } .subpage { padding: 1cm; height: 257mm;}.sub p {display:inline; }            @page { size: A4; margin: 0;  } @media print { html, body { width: 210mm;  height: 297mm;   } .page {  margin: 0; border: initial;  border-radius: initial; width: initial; min-height: initial;  background: initial; } }.tablenew tr td { vertical-align: top; }       .table tr.mtc td p{ margin: 0; display: inline-block; }         </style><script type=\"text/javascript\"> </script>  <link rel=\"stylesheet\" href=\"file:///jqmath-0.4.3.css\"> <script type=\"text/javascript\" src=\"file:///jquery-1.4.3.min.js\"></script>  <script type=\"text/javascript\">   var mtcarray = [];</script><script type=\"text/javascript\" src=\"file:///jqmath-etc-0.4.6.min.js\"></script>  </head> <body style=\"font-size:12px !important; font-family:'arial' !important;\"> <div class=\"book\"><div class=\"page\"> <div class=\"subpage\"> <table cellpadding=\"1\" cellspacing=\"1\" border=\"0\" class=\"table\" style=\"border: 1px solid black;\"> <tr> <td colspan=\"3\" align=\"center\"><h3 style='line-height:20px; margin:5px auto;'>Enter Your Class Name </h3></td> </tr><tr><td width=\"100\"><b>Exam: </b></td><td align=\"center\"><b>English</b></td> <td align=\"right\" width=\"100\"><b>Marks: </b></td> </tr><tr>    <td width=\"170\"><b>Date : 13-Jul-2018 </b></td>    <td align=\"center\"><b>Chapter: All</b></td>    <td align=\"right\" width=\"150\"><b>Time: </b></td>    </tr>    </table>    <br/> <table cellpadding=\"1\" cellspacing=\"1\" border=\"0\" class=\"table\" > IOSHTML </table><div style=\"height:150px !important; \"></div>    </div>    </div>    </div>    <script type=\"text/javascript\">    function shuffleArray(array) {        var counter = array.length, temp, index;        // While there are elements in the array        while (counter > 0) {            index = Math.floor(Math.random() * counter); counter--; temp = array[ counter ];array[ counter ] = array[ index ]; array[ index ] = temp; } return array; } var shuffledArray = shuffleArray(mtcarray);</script> </body> </html> ";
        
        
        if ([_isFrom isEqualToString:@"3"]) {
            [self createWebHTMLSetP];
        }
        else
        {
            [self CallPrilimPaper];
            [self createWebHTML];
        }
        
        
        
        
        NSLog(@"HTML==== %@",HTML);
       // _web_question.scalesPageToFit = YES;
        _web_question.contentMode = UIViewContentModeScaleAspectFit;
        [_web_question loadHTMLString:HTML baseURL:nil];
    }
    
}

-(void)getData{
    
    [APP_DELEGATE showLoadingView:@""];
    
    //Post Method Request
    
   
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    [AddPost setValue:_PaperId forKey:@"id"];
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    //%@get_student_class_dtl?",BaseURLAPI
    
    
    [manager GET:[NSString stringWithFormat:@"%@view-paper?",VIEW_PAPER_EVALUTER_URL] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject){
        
        NSLog(@"JSON: %@", responseObject);
        if ([[responseObject valueForKey:@"status"] intValue]==1) {
            
           strHTML=[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"data"]];
            
            NSLog(@"HTML==== %@",strHTML);
           // _web_question.scalesPageToFit = YES;
            _web_question.contentMode = UIViewContentModeScaleAspectFit;
            [_web_question loadHTMLString:strHTML baseURL:nil];
            
            
            
            [APP_DELEGATE hideLoadingView];
        }
        
        [APP_DELEGATE hideLoadingView];
        
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                        message:@"Can not load data."
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        [self.navigationController popViewControllerAnimated:NO];
        
        
    }];
    
    
}

- (void)dismissReaderViewController:(ReaderViewController *)viewController
{
    //[self dismissViewControllerAnimated:NO completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
    
}

-(void)createWebHTML
{
    
    for (int i=0; i<_arrPagging.count; i++) {
        _arrSubPagging=[[NSMutableArray alloc]initWithArray:[_arrPagging objectAtIndex:i]];
       
        NSString *sting1=[[NSString alloc]init];
        
        for (int j=0; j<_arrSubPagging.count; j++) {
            PrilimPaperModel *PPM =[_arrSubPagging objectAtIndex:j];
            
            
            if ([PPM.isQuestion intValue])
            {
                if ([PPM.Question rangeOfString:@"src"].location == NSNotFound)
                {
                    NSString* headerQue=[NSString stringWithFormat:@"<tr class=\"margin_top\"><td width=\"30\"></td> <td width=\"30\" style=\"vertical-align: top;\"><b></b></td>  <td width=\"660\" >%@</td> <td align=\"right\" width=\"30\"></td></tr>",PPM.Question];
                    
                    sting1=[NSString stringWithFormat:@"%@%@",sting1,headerQue];
                    
                }else
                {
                    NSString *strhtml=[NSString stringWithFormat:@"%@",PPM.Question];
                    filePath = [MyModel getFilePath];
                    filePath=[NSString stringWithFormat:@"file://%@",filePath];
                    NSString * strQue = [strhtml stringByReplacingOccurrencesOfString:@"http://staff.parshvaa.com" withString:filePath];
                    
                    NSString* headerQue=[NSString stringWithFormat:@"<tr class=\"margin_top\"><td width=\"30\"></td> <td width=\"30\" style=\"vertical-align: top;\"><b></b></td>  <td width=\"660\" >%@</td> <td align=\"right\" width=\"30\"></td></tr>",strQue];
                    
                    sting1=[NSString stringWithFormat:@"%@%@",sting1,headerQue];
                }
                
                
                
                
            }
            else
            {
                NSString* headerQue=[NSString stringWithFormat:@"<tr class=\"margin_top\"><td width=\"30\"><b>%@</b></td> <td width=\"30\"><b>%@</b></td>  <td width=\"660\"><b>%@</b></td>  <td align=\"right\" width=\"30\"><b>%@</b></td> </tr>",PPM.QuestionNO,PPM.SubQuestionNO,PPM.QuestionTypeText,PPM.QuestionMarks];
                 sting1=[NSString stringWithFormat:@"%@%@",sting1,headerQue];
            }
        }
        
        HTML = [strHTML stringByReplacingOccurrencesOfString:@"IOSHTML" withString:sting1];
        
        
    }
}

-(void)createWebHTMLSetP
{
    NSString *sting1=[[NSString alloc]init];
    for (int i=0; i<_arrPagging.count; i++) {
        _arrSubPagging=[[NSMutableArray alloc]initWithArray:[_arrPagging objectAtIndex:i]];
        
        
        
        int isFirstTime=1;
        
        for (int j=0; j<_arrSubPagging.count; j++) {
           
            SetPaperQuestionModel *PPM =[_arrSubPagging objectAtIndex:j];
            
            if (isFirstTime==1)
            {
                NSString* headerQue1=[NSString stringWithFormat:@"<tr class=\"margin_top\"><td width=\"30\"><b>Q.%d)</b></td> <td width=\"30\"><b></b></td>  <td width=\"660\"><b>%@</b></td>  <td align=\"right\" width=\"30\"><b>%@</b></td> </tr>",i+1,PPM.QuestionType,PPM.TotalMark];
                
                NSString* headerQue=[NSString stringWithFormat:@"<tr class=\"margin_top\"><td width=\"30\">%d)</td> <td width=\"30\" style=\"vertical-align: top;\"><b></b></td>  <td width=\"660\" >%@</td> <td align=\"right\" width=\"30\"></td></tr>",j+1,PPM.Question];
                sting1=[NSString stringWithFormat:@"%@%@%@",sting1,headerQue1,headerQue];
                
                isFirstTime=2;
            }
            else
            {
                NSString* headerQue=[NSString stringWithFormat:@"<tr class=\"margin_top\"><td width=\"30\">%d)</td> <td width=\"30\" style=\"vertical-align: top;\"><b></b></td>  <td width=\"660\" >%@</td> <td align=\"right\" width=\"30\"></td></tr>",j+1,PPM.Question];
                sting1=[NSString stringWithFormat:@"%@%@",sting1,headerQue];
            }
        }
        
    }
    HTML = [strHTML stringByReplacingOccurrencesOfString:@"IOSHTML" withString:sting1];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)CallPrilimPaper
{
    myDbQueris =[[MyDBQueries alloc]init];
    myDBParser=[[MyDBResultParser alloc]init];
    filePath = [MyModel getFilePath];
    
    FMResultSet *rs = [MyModel selectQuery:[myDbQueris getPrilimPaper:[[NSUserDefaults standardUserDefaults]valueForKey:@"SQPID"]]];
    
    arrPrilimPaper=[[NSMutableArray alloc]init];
    while ([rs next]) {
        PrilimPaperModel *SubModel = [[PrilimPaperModel alloc] init];
        [arrPrilimPaper  addObject:[myDBParser parseDBResult:rs :SubModel]];
    }
}


- (IBAction)btn_BACK:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];

}

- (IBAction)btn_ROUTING:(id)sender {
    
    /*if ([[UIDevice currentDevice]orientation] == UIInterfaceOrientationLandscapeRight){
        
        NSNumber *value = [NSNumber numberWithInt:UIInterfaceOrientationPortrait];
        [[UIDevice currentDevice] setValue:value forKey:@"orientation"];
        NSLog(@"portrait");
        
    }else if ([[UIDevice currentDevice]orientation] == UIInterfaceOrientationPortrait){
        
        NSNumber *value = [NSNumber numberWithInt:UIInterfaceOrientationLandscapeRight];
        [[UIDevice currentDevice] setValue:value forKey:@"orientation"];
        NSLog(@"landscape right");
        
    }*/
    
    NSLog(@"btnSave");
    
    NSString *fileName = [NSString stringWithFormat:@"%@.pdf",[MyModel getCuruntDateTime]];
    //NSString *fileName =[[NSString alloc]init];
    
    
    [self createPDFfromUIView:_web_question saveToDocumentsWithFileName:fileName];
    
    NSString *file = tdocumentDirectoryFilename11;
    
    ReaderDocument *document = [ReaderDocument withDocumentFilePath:file password:nil];
    
    if (document != nil)
    {
        ReaderViewController *readerViewController = [[ReaderViewController alloc] initWithReaderDocument:document];
        readerViewController.delegate = self;
        
        readerViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        readerViewController.modalPresentationStyle = UIModalPresentationFullScreen;
        
        
        [self.navigationController pushViewController:readerViewController animated:YES];
    }
    
}




-(void)createPDFfromUIView:(UIView*)aView saveToDocumentsWithFileName:(NSString*)aFilename
{
    // Creates a mutable data object for updating with binary data, like a byte array
    WKWebView *webView = (WKWebView*)aView;
    
  //  NSString *heightStr = [webView stringByEvaluatingJavaScriptFromString:@"(document.height !== undefined) ? document.height : document.body.offsetHeight;"];
    
    [webView evaluateJavaScript:@"(document.height !== undefined) ? document.height : document.body.offsetHeight;" completionHandler:^(NSString *result, NSError *error)
       {
           
           NSLog(@"INNER HTML: %@",result);
           int height = [result intValue];
           //  CGRect screenRect = [[UIScreen mainScreen] bounds];
           //  CGFloat screenHeight = (self.contentWebView.hidden)?screenRect.size.width:screenRect.size.height;
           CGFloat screenHeight = webView.bounds.size.height;
           int pages = ceil(height / screenHeight);
           
           NSMutableData *pdfData = [NSMutableData data];
           UIGraphicsBeginPDFContextToData(pdfData, webView.bounds, nil);
           CGRect frame = [webView frame];
           for (int i = 0; i < pages-1; i++) {
               // Check to screenHeight if page draws more than the height of the UIWebView
               if ((i+1) * screenHeight  > height) {
                   CGRect f = [webView frame];
                   f.size.height -= (((i+1) * screenHeight) - height);
                   [webView setFrame: f];
               }
               
               UIGraphicsBeginPDFPage();
               CGContextRef currentContext = UIGraphicsGetCurrentContext();
               //      CGContextTranslateCTM(currentContext, 72, 72); // Translate for 1" margins
               
               [[[webView subviews] lastObject] setContentOffset:CGPointMake(0, screenHeight * i) animated:NO];
               [webView.layer renderInContext:currentContext];
           }
           
           UIGraphicsEndPDFContext();
           // Retrieves the document directories from the iOS device
           NSArray* documentDirectories = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask,YES);
           
           NSString* documentDirectory = [documentDirectories objectAtIndex:0];
           tdocumentDirectoryFilename11 = [documentDirectory stringByAppendingPathComponent:aFilename];
           
           // instructs the mutable data object to write its context to a file on disk
           NSLog(@"documentDirectoryFilename %@",tdocumentDirectoryFilename11);
           [pdfData writeToFile:tdocumentDirectoryFilename11 atomically:YES];
           [webView setFrame:frame];
       }];
    
    
}




@end
