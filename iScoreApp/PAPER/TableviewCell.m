//
//  TableviewCell.m
//  iScoreApp
//
//  Created by ADMIN-Khushal on 6/4/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "TableviewCell.h"

@implementation TableviewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    [_btn_view_solution.layer setBorderWidth:1.5f];
    [_btn_view_solution.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    _btn_view_solution.layer.cornerRadius=_btn_view_solution.frame.size.height/2;
    
    //////////
    
  
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
