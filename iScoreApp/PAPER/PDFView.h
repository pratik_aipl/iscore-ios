//
//  PDFView.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 6/4/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyDBQueries.h"
#import "MyDBResultParser.h"
#import "MyModel.h"
#import "PDFView.h"

#import "TableviewCell.h"
#import "ExamTypeSubjectModel.h"
#import "ETPDetailsModel.h"
#import "MasterQueModel.h"
#import "MyModel.h"
#import "PrilimPaperModel.h"
#import <WebKit/WebKit.h>

@interface PDFView : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource,UITableViewDelegate,UITableViewDataSource>
{
    float heit,hdrHeit;
    float Hsolution,Wsolution;
    
    NSMutableArray *isExpand,*arrQueCount;
    
    MyDBQueries *myDbQueris;
    MyDBResultParser *myDBParser;
    NSString *filePath;
    
    NSMutableArray *arrExamTID;
    NSMutableArray *arrExamTPatten;
    NSMutableArray *arrExamTPatten1;
    NSMutableArray *arrExamTDetails;
    NSMutableArray *arrMQuestion,*arrSetMQuestion;
    NSMutableArray *arrPrilimPaper;
    ///////SET PAPER
    NSMutableArray *arrSetPaperTemp;
    
    
    NSString *SQPID;
    int QueCount;
    
    NSInteger currentIndex;

    NSMutableArray *arrPagging,*arrSubPagging;
    BOOL reload;
}

@property ( retain , nonatomic )NSString* subjectID;
@property ( retain , nonatomic )NSString *SQPID;
@property ( retain , nonatomic )NSMutableArray *arrSQPID;
@property ( retain , nonatomic )NSString *SearchPaper;
@property ( retain , nonatomic )NSString* isFrom;
@property ( retain , nonatomic )NSString* examTypeID;
@property ( retain , nonatomic )NSMutableArray* arrChapterIDs,*paperType,*arrSelectedQue;
@property ( retain , nonatomic )NSString* chapterList,*pDuration,*totMarks;


@property (weak, nonatomic) IBOutlet UILabel *lbl_view_title;

@property (strong, nonatomic)UITableView *coll_tbl;
@property (strong, nonatomic) IBOutlet UICollectionView *colle_vw;
@property (strong, nonatomic) IBOutlet UIButton *btn_answerp;
@property (strong, nonatomic) IBOutlet UIButton *btn_questionp;

@property (weak, nonatomic) IBOutlet UILabel *lbl_totque;
@property (weak, nonatomic) IBOutlet UILabel *lbl_currque;




- (IBAction)btn_BACK:(id)sender;
- (IBAction)btn_QUE_PAPER:(id)sender;
- (IBAction)btn_ANS_PAPER:(id)sender;
- (IBAction)btn_PRIV:(id)sender;
- (IBAction)btn_NEXT:(id)sender;




@end
