//
//  ViewController.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 2/23/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
//Outlets
@property (strong, nonatomic) IBOutlet UIButton *btn_login;
@property (strong, nonatomic) IBOutlet UIButton *btn_signup;



//Action
- (IBAction)btn_LOGIN_ACTION:(id)sender;
- (IBAction)btn_SIGNUP_ACTION:(id)sender;


@end

