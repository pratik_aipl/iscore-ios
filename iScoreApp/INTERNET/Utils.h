//
//  Utils.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 9/6/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Utils : NSObject

 +(BOOL)isNetworkAvailable;

@end


/*
 if([Utils isNetworkAvailable] ==YES){
 
 NSLog(@"Network Connection available");
 }
 else
 {
 NSLog(@"Network Connection Not available");
 }
 */
