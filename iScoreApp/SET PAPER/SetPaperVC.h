//
//  SetPaperVC.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 4/2/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SetPaperCell.h"
#import "MyDBQueries.h"
#import "MyDBResultParser.h"
#import "QuestionTypeTVC.h"
#import "TVCDispChapters.h"

@interface SetPaperVC : UIViewController<UITabBarDelegate,UITableViewDataSource>
{
    MyDBQueries *myDbQueris;
    MyDBResultParser *myDBParser;
    NSMutableArray *PaperType ;
    NSString *filePath;
    
   
    
    
}

@property (retain , nonatomic) NSString *chapterList;
@property (retain , nonatomic) NSString *SubjectID;
@property ( retain , nonatomic )NSString* isFrom;

//Outlets
@property (strong, nonatomic) IBOutlet UITableView *tbl_setpaper;

- (IBAction)tap_DISMISS:(id)sender;


//Actions
- (IBAction)btn_BACK:(id)sender;
- (IBAction)btn_NEXT:(id)sender;



@end
