//
//  SetPaperCell.m
//  iScoreApp
//
//  Created by ADMIN-Khushal on 4/2/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "SetPaperCell.h"

@implementation SetPaperCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.vw_marks.bounds byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerBottomLeft) cornerRadii:CGSizeMake(7.0, 7.0)];
    
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.vw_marks.bounds;
    maskLayer.path  = maskPath.CGPath;
    self.vw_marks.layer.mask = maskLayer;
    
    
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
