//
//  SetPaperCell.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 4/2/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SetPaperCell : UITableViewCell

//Outlets
@property (strong, nonatomic) IBOutlet UIImageView *img_chkbox;
@property (strong, nonatomic) IBOutlet UIButton *btn_chkbox;
@property (strong, nonatomic) IBOutlet UILabel *lbl_que_title;
@property (strong, nonatomic) IBOutlet UILabel *lbl_noofque;
@property (strong, nonatomic) IBOutlet UILabel *lbl_mk_perque;
@property (strong, nonatomic) IBOutlet UITextField *txt_toask;
@property (strong, nonatomic) IBOutlet UITextField *txt_toans;
@property (strong, nonatomic) IBOutlet UILabel *lbl_totmk;
@property (strong, nonatomic) IBOutlet UIView *vw_marks;




//Action



@end
