//
//  ConfirmQTCell.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 5/31/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ConfirmQTCell : UITableViewCell


@property (strong, nonatomic) IBOutlet UIView *vw_marks;
@property (strong, nonatomic) IBOutlet UILabel *lbl_marks;
@property (strong, nonatomic) IBOutlet UIView *vw_main;

@property (strong, nonatomic) IBOutlet UILabel *lbl_quetion;
@property (strong, nonatomic) IBOutlet UILabel *lbl_toask;
@property (strong, nonatomic) IBOutlet UILabel *lbl_toans;
@property (strong, nonatomic) IBOutlet UILabel *lbl_tot_marks;



@end
