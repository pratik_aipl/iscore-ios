//
//  ConfirmQTCell.m
//  iScoreApp
//
//  Created by ADMIN-Khushal on 5/31/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "ConfirmQTCell.h"

@implementation ConfirmQTCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.vw_marks.bounds byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerBottomLeft) cornerRadii:CGSizeMake(7.0, 7.0)];
    
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.vw_marks.bounds;
    maskLayer.path  = maskPath.CGPath;
    self.vw_marks.layer.mask = maskLayer;
    
    ////////
    _vw_main.layer.shadowRadius  = 1.5f;
    _vw_main.layer.shadowColor   = [UIColor colorWithRed:176.f/255.f green:199.f/255.f blue:226.f/255.f alpha:1.f].CGColor;
    _vw_main.layer.shadowOffset  = CGSizeMake(0.0f, 0.0f);
    _vw_main.layer.shadowOpacity = 0.9f;
    _vw_main.layer.masksToBounds = NO;
    
    UIEdgeInsets shadowInsets     = UIEdgeInsetsMake(0, 0, -1.5f, 0);
    UIBezierPath *shadowPath      = [UIBezierPath bezierPathWithRect:UIEdgeInsetsInsetRect(_vw_main.bounds, shadowInsets)];
    _vw_main.layer.shadowPath    = shadowPath.CGPath;
    
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
