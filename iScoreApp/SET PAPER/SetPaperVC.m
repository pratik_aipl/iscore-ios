//
//  SetPaperVC.m
//  iScoreApp
//
//  Created by ADMIN-Khushal on 4/2/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "SetPaperVC.h"
#import "SetPaperCell.h"
#import "UIColor+CL.h"
#import "MyModel.h"
#import "MyDBQueries.h"
#import "UIColor+CL.h"
#import "MyModel.h"
#import "ApplicationConst.h"
#import "SetPaperModel.h"
#import "ConfirmQueType.h"

@interface SetPaperVC ()

@end

@implementation SetPaperVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [_tbl_setpaper registerNib:[UINib nibWithNibName:@"SetPaperCell" bundle:nil] forCellReuseIdentifier:@"Cell"];
    
    
    myDbQueris =[[MyDBQueries alloc]init];
    myDBParser=[[MyDBResultParser alloc]init];
    filePath = [MyModel getFilePath];
    
    FMResultSet *rs = [MyModel selectQuery:[myDbQueris getSetPaperQueTypes:_chapterList:_SubjectID]];
    PaperType = [[NSMutableArray alloc] init];
    while ([rs next]) {
        SetPaperModel *SubModel = [[SetPaperModel alloc] init];
        [PaperType  addObject:[myDBParser parseDBResult:rs :SubModel]];
        
        NSLog(@"--%@",PaperType);
    }
    
    
    for (int i=0; i<PaperType.count; i++) {
        SetPaperModel *setM=[PaperType objectAtIndex:i];
        setM.Checked=NO;
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return PaperType.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    SetPaperCell *cell=(SetPaperCell *)[tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
   
    SetPaperModel *paperTypeM = [PaperType objectAtIndex:indexPath.row];
    
    cell.lbl_que_title.text=[NSString stringWithFormat:@"%@",paperTypeM.QuestionType];
    cell.lbl_noofque.text=[NSString stringWithFormat:@"%@",paperTypeM.TotalQuestion];
    cell.lbl_mk_perque.text=[NSString stringWithFormat:@"%@",paperTypeM.Marks];
    
    if (paperTypeM.Checked) {
        cell.btn_chkbox.selected=YES;
        cell.txt_toask.text=[paperTypeM valueForKey:@"Txttoask"];
        cell.txt_toans.text=[paperTypeM valueForKey:@"Txttoans"];
        cell.lbl_que_title.textColor=[UIColor colorWithHex:0x075584];
    }
    else
    {
        cell.btn_chkbox.selected=NO;
        cell.txt_toask.text=[paperTypeM valueForKey:@"Txttoask"];
        cell.txt_toans.text=[paperTypeM valueForKey:@"Txttoans"];
        cell.lbl_que_title.textColor=[UIColor colorWithHex:0x3E3E3E];
    }
    
    
    cell.btn_chkbox.tag = indexPath.row;
    
    [cell.btn_chkbox addTarget:self
               action:@selector(aMethod:)
     forControlEvents:UIControlEventTouchUpInside];
    
    //etc.
    return cell;
}
-(void)aMethod:(id)sender
{
    NSLog(@"User Like %@",sender);
    
    //NSInteger ival=[sender integerValue];
    
    UIButton *btn = (UIButton *)sender;
    
    CGPoint origin = btn.frame.origin;
    CGPoint point = [btn.superview convertPoint:origin toView:_tbl_setpaper];
    NSIndexPath * indexPath = [_tbl_setpaper indexPathForRowAtPoint:point];
    
    SetPaperCell *cell=(SetPaperCell *)[_tbl_setpaper cellForRowAtIndexPath:indexPath];
    
    SetPaperModel *paperTypeM = [PaperType objectAtIndex:indexPath.row];
    
    [cell.txt_toask addTarget:self action:@selector(textFieldDidChange1:) forControlEvents:UIControlEventEditingChanged];
    [cell.txt_toans addTarget:self action:@selector(textFieldDidChange2:) forControlEvents:UIControlEventEditingChanged];


    cell.txt_toask.delegate=self;
    cell.txt_toans.delegate=self;
    
    
    if (paperTypeM.Checked)
    {
        cell.btn_chkbox.selected = NO;
        cell.txt_toask.layer.borderWidth=0;
        cell.txt_toask.layer.borderColor=[UIColor greenColor].CGColor;
        cell.txt_toans.layer.borderWidth=0;
        cell.txt_toans.layer.borderColor=[UIColor greenColor].CGColor;
        cell.txt_toask.enabled=NO;
        cell.txt_toans.enabled=NO;
        cell.txt_toask.userInteractionEnabled=NO;
        cell.txt_toans.userInteractionEnabled=NO;
        cell.lbl_que_title.textColor=[UIColor colorWithHex:0x3E3E3E];
        paperTypeM.Checked=NO;
        
    }
    else
    {
        cell.btn_chkbox.selected = YES;
        cell.txt_toask.enabled=YES;
        cell.txt_toans.enabled=YES;
        cell.txt_toask.userInteractionEnabled=YES;
        cell.txt_toans.userInteractionEnabled=YES;
        [cell.txt_toask becomeFirstResponder];
        cell.lbl_que_title.textColor=[UIColor colorWithHex:0x075584];
        paperTypeM.Checked=YES;
    }
    
}
-(void)textFieldDidChange1 :(UITextField *)theTextField
{
    UIButton *btn = (UIButton *)theTextField;
    
    CGPoint origin = btn.frame.origin;
    CGPoint point = [btn.superview convertPoint:origin toView:_tbl_setpaper];
    NSIndexPath * indexPath = [_tbl_setpaper indexPathForRowAtPoint:point];
    
    SetPaperModel *paperTypeM = [PaperType objectAtIndex:indexPath.row];
    SetPaperCell *cell=(SetPaperCell *)[_tbl_setpaper cellForRowAtIndexPath:indexPath];
    
    if ([theTextField.text integerValue]<=[cell.lbl_noofque.text integerValue]) {
        NSLog(@"ok");
        paperTypeM.Txttoask =[NSString stringWithFormat:@"%ld",[theTextField.text integerValue]];
        
       
    }
    else
    {
        NSString *message = [NSString stringWithFormat:@"Should not more than %@",cell.lbl_noofque.text];
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil
                                                                       message:message
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:nil];
        
        int duration = 2; // duration in seconds
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [alert dismissViewControllerAnimated:YES completion:nil];
        });
        
        NSLog(@"NOT OK");
        NSString *lastString=[cell.txt_toask.text substringToIndex:[cell.txt_toask.text length] - 1];;
        [cell.txt_toask setText:lastString];
    }
    cell.txt_toans.text=@"";
    cell.lbl_totmk.text=@"0";
    
}
-(void)textFieldDidChange2 :(UITextField *)theTextField
{
    UIButton *btn = (UIButton *)theTextField;
    
    CGPoint origin = btn.frame.origin;
    CGPoint point = [btn.superview convertPoint:origin toView:_tbl_setpaper];
    NSIndexPath * indexPath = [_tbl_setpaper indexPathForRowAtPoint:point];
    
    SetPaperModel *paperTypeM = [PaperType objectAtIndex:indexPath.row];
    SetPaperCell *cell=(SetPaperCell *)[_tbl_setpaper cellForRowAtIndexPath:indexPath];
    
    if ([theTextField.text integerValue]<=[cell.txt_toask.text integerValue]) {
        NSLog(@"ok");
        
        cell.lbl_totmk.text=[NSString stringWithFormat:@"%ld",[cell.lbl_mk_perque.text integerValue]*[cell.txt_toans.text integerValue]];
        
        paperTypeM.Txttoans=[NSString stringWithFormat:@"%ld",[theTextField.text integerValue]];
        
    }
    else
    {
        NSString *message = [NSString stringWithFormat:@"Should not more than %@",cell.txt_toask.text];
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil
                                                                       message:message
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:nil];
        
        int duration = 2; // duration in seconds
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [alert dismissViewControllerAnimated:YES completion:nil];
        });
        NSLog(@"NOT OK");
        NSString *lastString=[cell.txt_toans.text substringToIndex:[cell.txt_toans.text length] - 1];;
        [cell.txt_toans setText:lastString];
        
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    // Prevent crashing undo bug – see note below.
    if(range.length + range.location > textField.text.length)
    {
        return NO;
    }
    
    
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    return newLength <= 3;
}


- (IBAction)btn_BACK:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btn_NEXT:(id)sender {
    
    NSMutableArray *tempPaper=[[NSMutableArray alloc]init];
    for (int i=0; i<PaperType.count; i++) {
        SetPaperModel *paperTypeM = [PaperType objectAtIndex:i];
        if (paperTypeM.Checked) {
            
            if ([paperTypeM.Txttoask intValue]>0) {
                if([paperTypeM.Txttoans intValue]>0 )
                {
                    [tempPaper addObject:[PaperType objectAtIndex:i]];
                }
                else
                {
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Alert" message:@"Please Enter Question To Answer" preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                                         {                                 }];
                    [alert addAction:ok];
                    [self presentViewController:alert animated:YES completion:nil];
                }
                
            }
            else
            {
               
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Alert" message:@"Please Enter Question To ASK" preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                                     {                                 }];
                [alert addAction:ok];
                [self presentViewController:alert animated:YES completion:nil];
            }
            
            
        }
        
        
    }
    
    
    if (tempPaper.count>0) {
        ConfirmQueType *add = [[ConfirmQueType alloc] initWithNibName:@"ConfirmQueType" bundle:nil];
        add.PaperType=tempPaper;
        add.chapterList=_chapterList;
        add.SubjectID=_SubjectID;
        add.isFrom=_isFrom;
        [self.navigationController pushViewController:add animated:YES];
    }
    else
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Alert" message:@"Please Select any one." preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                             {                             }];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }
    
    
}
- (IBAction)tap_DISMISS:(id)sender {

    [self.view endEditing:YES];
}
@end
