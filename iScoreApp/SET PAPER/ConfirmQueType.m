//
//  ConfirmQueType.m
//  iScoreApp
//
//  Created by ADMIN-Khushal on 5/31/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "ConfirmQueType.h"
#import "ConfirmQTCell.h"
#import "SetPaperModel.h"
#import "WYPopoverController.h"
#import "PDFView.h"



@interface ConfirmQueType ()<WYPopoverControllerDelegate>{
    WYPopoverController* popoverController;
}

@end

@implementation ConfirmQueType

- (void)viewDidLoad {
    [super viewDidLoad];
    totMarks=0;
    // Do any additional setup after loading the view from its nib.
    [_tbl_table registerNib:[UINib nibWithNibName:@"ConfirmQTCell" bundle:nil] forCellReuseIdentifier:@"Cell"];
    
    for (int i=0; i<_PaperType.count; i++) {
        SetPaperModel *paperTypeM = [_PaperType objectAtIndex:i];
        totMarks+=[paperTypeM.Marks intValue]*[paperTypeM.Txttoans intValue];
    }
    
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _PaperType.count;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ConfirmQTCell *cell=(ConfirmQTCell *)[tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
   
    SetPaperModel *paperTypeM = [_PaperType objectAtIndex:indexPath.row];
    
    cell.lbl_quetion.text=[NSString stringWithFormat:@"%@",paperTypeM.QuestionType];
    cell.lbl_marks.text=[NSString stringWithFormat:@"%@",paperTypeM.Marks];
    
    if (paperTypeM.Checked) {
        cell.lbl_toask.text=[NSString stringWithFormat:@"%@",paperTypeM.Txttoask];
        cell.lbl_toans.text=[NSString stringWithFormat:@"%@",paperTypeM.Txttoans];
        
        cell.lbl_tot_marks.text=[NSString stringWithFormat:@"%d",[paperTypeM.Marks intValue]*[paperTypeM.Txttoans intValue]];
    }
    else
    {
        cell.lbl_toask.text=[NSString stringWithFormat:@"%@",paperTypeM.Txttoask];
        cell.lbl_toans.text=[NSString stringWithFormat:@"%@",paperTypeM.Txttoans];
    }
    
    return cell;
    
}

- (IBAction)btn_SELECTTIME:(id)sender
{
    
    [_txt_selecttime setTag:2];
    currentTextField = _txt_selecttime;
    [self showPopover:sender];
    
  
}

- (IBAction)btn_GENARATEPAPER:(id)sender {
    
    if (_txt_selecttime.text && _txt_selecttime.text.length > 0)
    {
        /* not empty - do something */
        PDFView *add = [[PDFView alloc] initWithNibName:@"PDFView" bundle:nil];
        add.isFrom=_isFrom;
        add.subjectID=_SubjectID;
        add.paperType=_PaperType;
        add.chapterList=_chapterList;
        add.pDuration=_txt_selecttime.text;
        add.totMarks=[NSString stringWithFormat:@"%d",totMarks];
        
        [self.navigationController pushViewController:add animated:YES];
        
    }
    else
    {
        /* what ever */
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Alert" message:@"Please select Duration." preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                             {                                 }];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }
    
    
    
}



- (IBAction)btn_BACK:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)showPopover:(UIView*)btn;
{
    
    if (popoverController == nil)
    {
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                 bundle: nil];
        PopOverView *settingsViewController = [mainStoryboard instantiateViewControllerWithIdentifier:@"PopOverView"];
        
        //[self.navigationController pushViewController:view animated:YES];
        
        settingsViewController.index = currentTextField.tag;
        
        
        
        if ([settingsViewController respondsToSelector:@selector(setPreferredContentSize:)]) {
            settingsViewController.preferredContentSize = CGSizeMake(200, 170);             // iOS 7
        }
        else {
#pragma clang diagnostic pushc
#pragma GCC diagnostic ignored "-Wdeprecated"
            settingsViewController.contentSizeForViewInPopover = CGSizeMake(200, 170);      // iOS < 7
#pragma clang diagnostic pop
        }
        settingsViewController.delegates = self;
        
        [[self navigationController] setNavigationBarHidden:YES animated:YES];
        
        settingsViewController.modalInPopover = NO;
        popoverController = [[WYPopoverController alloc] initWithContentViewController:settingsViewController];
        
        popoverController.delegate = self;
        
        popoverController.passthroughViews = @[btn];
        popoverController.popoverLayoutMargins = UIEdgeInsetsMake(0, 10, 10 , 10);
        popoverController.wantsDefaultContentAppearance = YES;
        
        [popoverController presentPopoverFromRect:btn.bounds
                                           inView:btn
                         permittedArrowDirections:WYPopoverArrowDirectionAny
                                         animated:YES
                                          options:WYPopoverAnimationOptionFadeWithScale];
    }
    
}

- (BOOL)popoverControllerShouldDismissPopover:(WYPopoverController *)controller
{
    return YES;
}

- (void)popoverControllerDidDismissPopover:(WYPopoverController *)controller
{
    popoverController.delegate = nil;
    popoverController = nil;
}
-(void)dismissPopUpViewController
{
    [popoverController dismissPopoverAnimated:YES];
    popoverController.delegate = nil;
    popoverController = nil;
}
-(void)Popvalueselected:(NSString*)strValue{
    NSLog(@"POP:%@",strValue);
    
    
    currentTextField.text = [NSString stringWithFormat:@"%@",strValue];
    currentTextField.text = strValue;
   
    
}





@end
