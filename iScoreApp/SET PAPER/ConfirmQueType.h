//
//  ConfirmQueType.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 5/31/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PopOverView.h"
#import "DropDownListView.h"


@interface ConfirmQueType : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
    UITextField *currentTextField;
    
    DropDownListView * Dropobj;
    NSMutableArray *arrDuration;
    int totMarks;
    
}
@property (retain , nonatomic )NSMutableArray *PaperType;
@property (retain , nonatomic )NSString *SubjectID;
@property (retain , nonatomic )NSString *chapterList;
@property ( retain , nonatomic )NSString* isFrom;

@property (strong, nonatomic) IBOutlet UITableView *tbl_table;
@property (strong, nonatomic) IBOutlet UITextField *txt_selecttime;



- (IBAction)btn_BACK:(id)sender;
- (IBAction)btn_SELECTTIME:(id)sender;
- (IBAction)btn_GENARATEPAPER:(id)sender;



-(void)Popvalueselected:(NSString*)strValue;
-(void)Popvalveselected:(NSString*)strValve;
-(void)dismissPopUpViewController;

@end
