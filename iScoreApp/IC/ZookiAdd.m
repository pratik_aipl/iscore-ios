//
//  ZookiAdd.m
//  iScoreApp
//
//  Created by ADMIN-Khushal on 9/14/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "ZookiAdd.h"
#import "ApplicationConst.h"
#import <AFNetworking/AFNetworking.h>

@interface ZookiAdd ()

@end

@implementation ZookiAdd

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)getCallZOOKI
{
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    //[AddPost setValue:[ParseData valueForKey:@"StudentID"] forKey:@"StudID"];
    [AddPost setValue:[[NSUserDefaults standardUserDefaults]valueForKeyPath:@"ParseData.StudentID"] forKey:@"StudID"];
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    [manager GET:[NSString stringWithFormat:@"%@get-zooki?",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject){
        NSLog(@"JSON: %@", responseObject);
        
        if ([[responseObject valueForKey:@"status"] intValue]==1) {
            
            NSArray *tempdata=[[NSArray alloc]initWithArray:[responseObject valueForKeyPath:@"data"]];
            
            [MyModel UpdateQuery:[NSString stringWithFormat:@"DELETE FROM zooki"]];
            
            for (int i=0; i<(unsigned long)tempdata.count; i++) {
                [MyModel insertInto_papertype:[tempdata objectAtIndex:i] :@"zooki"];
            }
            
            
            ShareData.rootImages=[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"folder_path"]];
            
            ShareData.marrImages=[NSMutableArray arrayWithArray:[responseObject valueForKeyPath:@"data.image"]];
            
            
            NSLog(@"  %@",ShareData.marrQueImages);
            NSLog(@"  %@",ShareData.rootImages);
            NSLog(@"  %@",ShareData.marrImages);
            
            
            for (int j=0; j<ShareData.marrImages.count; j++) {
                
                
                //*************FILE PATH**********************
                NSString *imageURL = [NSString stringWithFormat:@"%@/%@",ShareData.rootImages,[ShareData.marrImages objectAtIndex:j]];
                
                NSString *httpReplaceString = @"http://staff.parshvaa.com/";
                NSString *httpsReplaceString = @"https://staff.parshvaa.com/";
                NSString *finalURL;
                
                finalURL = [imageURL stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
                if([imageURL containsString:@"https"]){
                    finalURL = [imageURL stringByReplacingOccurrencesOfString:httpsReplaceString withString:@""];
                } else {
                    finalURL = [imageURL stringByReplacingOccurrencesOfString:httpReplaceString withString:@""];
                }
                NSArray *parts = [finalURL componentsSeparatedByString:@"/"];
                NSFileManager *fileManager = [NSFileManager defaultManager];
                NSString *folderPath =@"";
                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                
                NSError *error;
                
                
                for (int k=3; k<parts.count-1; k++) {
                    if(k!=0){
                        
                        folderPath = [NSString stringWithFormat:@"%@/%@",folderPath,parts[k]];
                        NSString *documentsDirectory = [paths objectAtIndex:0];
                        [fileManager createDirectoryAtPath:[documentsDirectory stringByAppendingPathComponent:folderPath] withIntermediateDirectories:NO attributes:nil error:&error];
                    }else {
                        folderPath = [NSString stringWithFormat:@"%@",parts[k]];
                        NSString *documentsDirectory = [paths objectAtIndex:0];
                        [fileManager createDirectoryAtPath:[documentsDirectory stringByAppendingPathComponent:folderPath] withIntermediateDirectories:NO attributes:nil error:&error];
                    }
                }
                NSString *writablePath = [folderPath stringByAppendingPathComponent:[parts lastObject]];
                
                if(![fileManager fileExistsAtPath:writablePath]){
                    if ([parts lastObject]){
                        NSString *lastPart = [parts lastObject];
                        NSLog(@"Image Name => , %@",lastPart);
                        if([lastPart containsString:@"gif"]){
                            [self getImageFromURLAndSaveItToLocalData:[parts lastObject] fileURL:imageURL inDirectory:folderPath];
                            
                        } else {
                            [self getImageFromURLAndSaveItToLocalData:[parts lastObject] fileURL:imageURL inDirectory:folderPath];
                        }
                    }
                }
            
            }
            
            
            
            NSLog(@"ShareData.marrQueImages %lu",(unsigned long)ShareData.marrQueImages.count);
            NSLog(@"ShareData.marrImages %lu",(unsigned long)ShareData.marrImages.count);
            
          
        }
        //[APP_DELEGATE hideLoadingView];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        //[APP_DELEGATE hideLoadingView];
    }];
}



-(void) getImageFromURLAndSaveItToLocalData:(NSString *)imageName fileURL:(NSString *)fileURL inDirectory:(NSString *)directoryPath {
    
   
        NSLog(@"IMAGE NAME :: %@",imageName);
        NSLog(@"IMAGE URL :: %@",fileURL);
        
        NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString:fileURL]];

        NSError *error = nil;
    
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *tempPath = [paths objectAtIndex:0];;
        
        directoryPath = [tempPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@", directoryPath]];
        directoryPath = [directoryPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@", imageName]];
        
        [[NSFileManager defaultManager] createFileAtPath:directoryPath
                                                contents:data
                                              attributes:nil];
  
    
}

- (IBAction)btn_ZOOKI:(id)sender {

    [self getCallZOOKI];
    
}
@end
