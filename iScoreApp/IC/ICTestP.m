//
//  ICTestP.m
//  iScoreApp
//
//  Created by ADMIN-Khushal on 9/10/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "ICTestP.h"
#import "ApplicationConst.h"
#import <AFNetworking/AFNetworking.h>
#import "ICTestPCell.h"
#import "UIColor+CL.h"





@interface ICTestP ()

@end

@implementation ICTestP

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    arrSubjects=[[NSMutableArray alloc]init];
    arrchk=[[NSMutableArray alloc]init];
    arrlevel=[[NSMutableArray alloc]init];
    arrList=[[NSMutableArray alloc]init];
    
    
    [self CallgetSubjects];
    [self CallgetTestList];
    
    
    
    for (int i=0; i<3; i++) {
        NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
        [dict setValue:@"Level" forKey:@"level"];
        [dict setValue:@"0" forKey:@"isCheck"];
        [arrlevel addObject:dict];
    }
    
    //SHADOW
    _vw_tbl.layer.shadowRadius  = 3.5f;
    _vw_tbl.layer.shadowColor   = [UIColor colorWithRed:176.f/255.f green:199.f/255.f blue:226.f/255.f alpha:1.f].CGColor;
    _vw_tbl.layer.shadowOffset  = CGSizeMake(0.0f, 0.0f);
    _vw_tbl.layer.shadowOpacity = 2.1f;
    _vw_tbl.layer.masksToBounds = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)CallgetSubjects
{
    [APP_DELEGATE showLoadingView:@""];
    
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    
    [AddPost setValue: [[NSUserDefaults standardUserDefaults] valueForKeyPath:@"ParseData.StudentID"] forKey:@"StudID"];//taken_test
    [AddPost setValue: @"generate" forKey:@"section_type"];
    
    NSLog(@"StudID %@",AddPost);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    //%@get_student_class_dtl?",BaseURLAPI
    
    [manager GET:[NSString stringWithFormat:@"%@get-evaluator-subjects?",ICAPIURL] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject){
        NSLog(@"JSON: %@", responseObject);
        
        
        
        if ([[responseObject valueForKey:@"status"] intValue]==1) {
            
            arrSubjects=[responseObject valueForKey:@"data"];
            
            for (int i=0; i<arrSubjects.count; i++) {
                [arrchk addObject:@"0"];
            }
            
            [_tbl_left reloadData];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                            message:@"Some Error"
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }
        
        
        [APP_DELEGATE hideLoadingView];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                        message:@"Can not load data."
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        
        
    }];
    
}


-(void)CallgetTestList
{
    [APP_DELEGATE showLoadingView:@""];
    
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    
    [AddPost setValue: [[NSUserDefaults standardUserDefaults] valueForKeyPath:@"ParseData.StudentID"] forKey:@"StudID"];//taken_test
    
    
    NSLog(@"StudID %@",AddPost);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    //%@get_student_class_dtl?",BaseURLAPI
    
    [manager GET:[NSString stringWithFormat:@"%@get-recent-question-paper?",ICAPIURL] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject){
        NSLog(@"JSON: %@", responseObject);
        
        
        
        if ([[responseObject valueForKey:@"status"] intValue]==1) {
            
            arrList=[responseObject valueForKey:@"data"];
            
            [_tbl_list reloadData];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                            message:@"No Data Found"
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }
        
        
        [APP_DELEGATE hideLoadingView];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                        message:@"Can not load data."
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        
        [self.navigationController popViewControllerAnimated:NO];
        
    }];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView==_tbl_list) {
        return arrList.count;
    }
    else if (tableView==_tbl_left)
    {
        return  arrSubjects.count;
    }
   
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==_tbl_list) {
        
        ICTestPCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellList"];
        if (cell == nil)
        {
            // Creating a cell here
            cell = [[ICTestPCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CellList"];
        }
        
        
        cell.vw_shadow.hidden=YES;
        cell.cw_shadow2.hidden=YES;
        
        if ([[[arrList valueForKey:@"IsStudentMarkUploaded"] objectAtIndex:indexPath.row] isEqualToString:@"0"]) {
            cell.vw_shadow.hidden=NO;
            cell.lbl_subjectnm.text=[NSString stringWithFormat:@"%@",[arrList valueForKey:@"SubjectName"][indexPath.row]];
            
            //-----TODATE---------
            NSString * yourJSONString = [arrList valueForKey:@"PaperDate"][indexPath.row];//EndDate
            NSDateFormatter *currentDTFormatter = [[NSDateFormatter alloc] init];;
            [currentDTFormatter setDateFormat:@"yyyy-MM-dd"];
            NSDate *dateFromString = [currentDTFormatter dateFromString:yourJSONString];
            [currentDTFormatter setDateFormat:@"MMM"];
            NSString *monthnm = [currentDTFormatter stringFromDate:dateFromString];
            [currentDTFormatter setDateFormat:@"dd"];
            NSString *onlydt = [currentDTFormatter stringFromDate:dateFromString];
            
            cell.lbl_month.text=[NSString stringWithFormat:@"%@",monthnm];
            cell.lbl_dt.text=[NSString stringWithFormat:@"%@",onlydt];
            
            cell.lbl_marks.text=[NSString stringWithFormat:@"%@ Marks",[arrList valueForKey:@"TotalMarks"][indexPath.row]];
        }
        else
        {
            cell.cw_shadow2.hidden=NO;
            
            cell.lbl_subjectnm2.text=[NSString stringWithFormat:@"%@",[arrList valueForKey:@"SubjectName"][indexPath.row]];
            //-----TODATE---------
            NSString * yourJSONString = [arrList valueForKey:@"PaperDate"][indexPath.row];//EndDate
            NSDateFormatter *currentDTFormatter = [[NSDateFormatter alloc] init];;
            [currentDTFormatter setDateFormat:@"yyyy-MM-dd"];
            NSDate *dateFromString = [currentDTFormatter dateFromString:yourJSONString];
            [currentDTFormatter setDateFormat:@"MMM"];
            NSString *monthnm = [currentDTFormatter stringFromDate:dateFromString];
            [currentDTFormatter setDateFormat:@"dd"];
            NSString *onlydt = [currentDTFormatter stringFromDate:dateFromString];
            
            cell.lbl_month2.text=[NSString stringWithFormat:@"%@",monthnm];
            cell.lbl_dt2.text=[NSString stringWithFormat:@"%@",onlydt];
            
            cell.lbl_marks2.text=[NSString stringWithFormat:@"%@/%@ Marks",[arrList valueForKey:@"StudentMark"][indexPath.row],[arrList valueForKey:@"TotalMarks"][indexPath.row]];
            
            
            
            float f=[[[arrList valueForKey:@"StudentMark"] objectAtIndex:indexPath.row] floatValue];
            float f2=[[[arrList valueForKey:@"TotalMarks"] objectAtIndex:indexPath.row] floatValue];
            
            ////////PROGRESSBAR
            
            [cell.prog_marks.layer setCornerRadius:cell.prog_marks.frame.size.height/2];
            cell.prog_marks.clipsToBounds = true;
            [cell.prog_marks setTransform:CGAffineTransformMakeScale(1.0f, 3.0f)];
            
            cell.prog_marks.progress =  f/100.0;
            cell.prog_marks.trackTintColor = [UIColor colorWithHex:0xDDDDDD];
            cell.prog_marks.progressTintColor = [UIColor colorWithHex:0x0A385A];
        }
        
        
        
       
        
        return cell;
        
        
        
        return cell;
    }
    else if (tableView==_tbl_left)
    {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
        if (cell == nil)
        {
            // Creating a cell here
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
            
            UILabel *theLabelIWant = [[UILabel alloc] initWithFrame:CGRectMake(10, 14, 120, 21)];
            theLabelIWant.tag = 1;
            [cell.contentView addSubview:theLabelIWant];
            
            UIImageView *dot =[[UIImageView alloc] initWithFrame:CGRectMake(150,14,21,21)];
            dot.tag=2;
            
            [cell.contentView addSubview:dot];
        }
        
        UILabel *finalLabel = (UILabel *)[[cell contentView] viewWithTag:1];
        [finalLabel setText:[NSString stringWithFormat:@"%@",[arrSubjects valueForKey:@"SubjectName"][indexPath.row]]];
        UIImageView *dot =(UIImageView *)[[cell contentView] viewWithTag:2];
        if ([[arrchk objectAtIndex:indexPath.row] isEqualToString:@"0"]) {
            dot.image=[UIImage imageNamed:@"checkbox-unselected1"];
        }
        else
        {
            dot.image=[UIImage imageNamed:@"checkbox-selected1"];
        }
        
        return cell;
    }
    
    return 0;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    if (tableView==_tbl_list) {
        
        
        //[self CallGetRedirectMCQ:Flist.StudentMCQTestHDRID :Flist.SubjectName :Flist.SubjectID];
        
        /*  PDFView *vc2 = [[PDFView alloc] initWithNibName:@"PDFView" bundle:nil];
         [[self navigationController] pushViewController:vc2 animated:YES];*/
        
    }
    else if (tableView == _tbl_left)
    {
        if ([[arrchk objectAtIndex:indexPath.row] isEqualToString:@"0"]) {
            [arrchk replaceObjectAtIndex:indexPath.row withObject:@"1"];
        }
        else
        {
            [arrchk replaceObjectAtIndex:indexPath.row withObject:@"0"];
        }
        [_tbl_left reloadData];
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}


- (IBAction)btn_LEFT:(id)sender
{
    _vw_left.hidden=NO;
    _btn_background.hidden=NO;
    
    [_tbl_left reloadData];
}
- (IBAction)btn_DISMISSTAB:(id)sender
{
    _vw_left.hidden=YES;
    _btn_background.hidden=YES;
}

- (IBAction)btn_FILTERL:(id)sender
{
    _vw_left.hidden=YES;
    _btn_background.hidden=YES;
    
    arrSubIds=[[NSMutableArray alloc]init];
    
    for (int i=0; i<arrSubjects.count; i++) {
        if ([[arrchk objectAtIndex:i]isEqualToString:@"1"]) {
            [arrSubIds addObject:[[arrSubjects objectAtIndex:i] valueForKey:@"SubjectID"]];
        }
    }
    
    
    [self CallFilter];
}

-(void)CallFilter
{
    [APP_DELEGATE showLoadingView:@""];
    
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    
    [AddPost setValue: [[NSUserDefaults standardUserDefaults] valueForKeyPath:@"ParseData.StudentID"] forKey:@"StudID"];//taken_test
    [AddPost setValue:[NSString stringWithFormat:@"%@",[arrSubIds componentsJoinedByString:@","]] forKey:@"subject_id"];
    [AddPost setValue: @"1" forKey:@"class_id"];
    
    NSLog(@"StudID %@",AddPost);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    //%@get_student_class_dtl?",BaseURLAPI
    
    [manager GET:[NSString stringWithFormat:@"%@get-subject-paper?",ICAPIURL] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject){
        NSLog(@"JSON: %@", responseObject);
        
        
        
        if ([[responseObject valueForKey:@"status"] intValue]==1) {
            
            arrList=[[NSMutableArray alloc]init];
            
            arrList=[responseObject valueForKey:@"data"];
            
            
            [_tbl_list reloadData];
        }
        else
        {
            arrList=[[NSMutableArray alloc]init];
            
            //arrList=[responseObject valueForKey:@"data"];
            
            
            [_tbl_list reloadData];
            /*   UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
             message:@"Some Error"
             delegate:self
             cancelButtonTitle:@"OK"
             otherButtonTitles:nil];
             [alert show];*/
        }
        
        
        [APP_DELEGATE hideLoadingView];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                        message:@"Can not load data."
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        
        
    }];
    
}



@end
