//
//  CCTCell.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 9/10/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CCTCell : UITableViewCell




@property (weak, nonatomic) IBOutlet UIView *vw_shadow;
@property (weak, nonatomic) IBOutlet UIView *vw_dt;
@property (weak, nonatomic) IBOutlet UILabel *lbl_month;
@property (weak, nonatomic) IBOutlet UILabel *lbl_date;
@property (weak, nonatomic) IBOutlet UILabel *lbl_sub;
@property (weak, nonatomic) IBOutlet UILabel *lbl_expire;




@end
