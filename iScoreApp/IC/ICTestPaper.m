//
//  ICTestPaper.m
//  iScoreApp
//
//  Created by ADMIN-Khushal on 9/8/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "ICTestPaper.h"
#import "ICTestPaperCell.h"
#import "ApplicationConst.h"
#import <AFNetworking/AFNetworking.h>
#import "UIColor+CL.h"
#import "ICPDF.h"
#import "viewpdfViewController.h"
#import "ImageUploadViewController.h"

@interface ICTestPaper ()

@end

@implementation ICTestPaper
-(void)viewWillAppear:(BOOL)animated
{
    [_tbl_testpaper reloadData];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
     [_tbl_testpaper registerNib:[UINib nibWithNibName:@"ICTestPaperCell" bundle:nil] forCellReuseIdentifier:@"Cell"];
    
    
    [self CallClassTestPaper];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}

-(void)CallClassTestPaper
{
    [APP_DELEGATE showLoadingView:@""];
    
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    [AddPost setValue: [[NSUserDefaults standardUserDefaults] valueForKeyPath:@"ParseData.StudentID"] forKey:@"StudID"];//taken_test
   
    
    NSLog(@"StudID %@",AddPost);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    //%@get_student_class_dtl?",BaseURLAPI
    // https://staff.parshvaa.com/web_services/version_58/evaluator_web_services/get-recent-question-paper?StudID=16748
    [manager GET:[NSString stringWithFormat:@"%@get-recent-question-paper?",ICAPIURL] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject){
        NSLog(@"JSON: %@", responseObject);
        
        
        
        if ([[responseObject valueForKey:@"status"] intValue]==1) {
            
            Dictdata=[responseObject valueForKey:@"data"];
            
            
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                            message:@"No Data Found"
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }
        
        [_tbl_testpaper reloadData];
        
        [APP_DELEGATE hideLoadingView];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                        message:@"Can not load data."
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        
        [self.navigationController popViewControllerAnimated:NO];
        
    }];
    
}

-(void)Callgeneratepaperdata:(NSDictionary *)dict
{
    [APP_DELEGATE showLoadingView:@""];
    
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    [AddPost setValue: [dict valueForKey:@"PaperID"] forKey:@"id"];//taken_test
   
    
    NSLog(@"StudID %@",AddPost);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    //https://evaluater.parshvaa.com/class-admin/web-services-paper/view-paper?id=481148
    
    [manager GET:[NSString stringWithFormat:@"%@view-paper?",VIEW_PAPER_EVALUTER_URL] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject){
        NSLog(@"JSON: %@", responseObject);
        
        
        
        if ([[responseObject valueForKey:@"status"] intValue]==1) {
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
                               viewpdfViewController * next = [storyboard instantiateViewControllerWithIdentifier:@"viewpdfViewController"];
                               next.dictdata = dict;
            next.htmlstring = [responseObject valueForKey:@"data"];
                               [self.navigationController pushViewController:next animated:YES];
            
            
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                            message:@"No Data Found"
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }
        
       
        
        [APP_DELEGATE hideLoadingView];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                        message:@"Can not load data."
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        
        [self.navigationController popViewControllerAnimated:NO];
        
    }];
    
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return Dictdata.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 75;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ICTestPaperCell *cell = (ICTestPaperCell *)[_tbl_testpaper dequeueReusableCellWithIdentifier:@"Cell"];
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"CCTCell" owner:self options:nil];
    if(cell == nil){
        cell = nib[0];
    }
    
    
    cell.vw_shadow.hidden=YES;
    cell.vw_shadow2.hidden=YES;
    
    if ([[Dictdata valueForKey:@"IsStudentMarkUploaded"][indexPath.row] isEqualToString:@"0"]) {
        ///////VIEW 1////////
        cell.vw_shadow.hidden=NO;
        cell.lbl_subjectnm.text=[NSString stringWithFormat:@"%@",[Dictdata valueForKey:@"SubjectName"][indexPath.row]];
        //-----TODATE---------
        NSString * yourJSONString = [Dictdata valueForKey:@"PaperDate"][indexPath.row];//EndDate
        NSDateFormatter *currentDTFormatter = [[NSDateFormatter alloc] init];;
        [currentDTFormatter setDateFormat:@"yyyy-MM-dd"];
        NSDate *dateFromString = [currentDTFormatter dateFromString:yourJSONString];
        [currentDTFormatter setDateFormat:@"MMM"];
        NSString *monthnm = [currentDTFormatter stringFromDate:dateFromString];
        [currentDTFormatter setDateFormat:@"dd"];
        NSString *onlydt = [currentDTFormatter stringFromDate:dateFromString];
        
        cell.lbl_month.text=[NSString stringWithFormat:@"%@",monthnm];
        cell.lbl_dt.text=[NSString stringWithFormat:@"%@",onlydt];
        
        cell.lbl_marks.text=[NSString stringWithFormat:@"%@ Marks",[Dictdata valueForKey:@"TotalMarks"][indexPath.row]];
        
        cell.lblstarttime.text = [Dictdata valueForKey:@"OnlyStartTime"][indexPath.row];
        cell.lblendtime.text = [Dictdata valueForKey:@"OnlyEndTime"][indexPath.row];
        
        
        cell.btnplay.tag = indexPath.row;
        [cell.btnplay addTarget:self action:@selector(yourButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        cell.btnplay.hidden = YES;
         NSDate *today = [NSDate date]; // it will give you current date
           NSDateFormatter *dateFormatte = [[NSDateFormatter alloc] init];
            [dateFormatte setDateFormat:@"yyyy-MM-dd hh:mm a"];
            
            NSDate *startTime = [dateFormatte dateFromString:[Dictdata valueForKey:@"StartTime"][indexPath.row]];// your date
            NSDate *endTime = [dateFormatte dateFromString:[Dictdata valueForKey:@"EndTime"][indexPath.row]];
            
            NSComparisonResult result,result1;
            //has three possible values: NSOrderedSame,NSOrderedDescending, NSOrderedAscending

            result = [today compare:startTime];
              result1 = [today compare:endTime];
            // comparing two dates

            if ((result==NSOrderedDescending) && (result1 == NSOrderedAscending))
            {
                [cell.btnplay setTitle:@"P" forState:UIControlStateNormal];
                cell.btnplay.hidden = false;
                cell.userInteractionEnabled = true;
                NSLog(@"today is less");
            cell.btnplay.alpha = 1.0f;
                [cell.btnplay setImage:[UIImage imageNamed:@"play"] forState:UIControlStateNormal];
            [UIView animateWithDuration:1.0
              delay:0.5
              options:UIViewAnimationOptionCurveEaseInOut |
                      UIViewAnimationOptionRepeat |
                      UIViewAnimationOptionAutoreverse |
                      UIViewAnimationOptionAllowUserInteraction
              animations:^{
               cell.btnplay.alpha = 0.1f;
               //  cell.btnplay.userInteractionEnabled = true;
            }
            completion:^(BOOL finished){
            // Do nothing
            }];
            }
        //    else if(result==NSOrderedDescending)
        //    {
        //        //cell.btnplay.enabled = false;
        //        NSLog(@"newDate is less");
        //    }
            else
                {
                    NSLog(@"Both dates are same");
                  //  cell.btnplay.hidden = true;
                }
        
        
        
        
        NSDate *UploadStartTime = [dateFormatte dateFromString:[Dictdata valueForKey:@"UploadStartTime"][indexPath.row]];// your date
            NSDate *UploadEndTime = [dateFormatte dateFromString:[Dictdata valueForKey:@"UploadEndTime"][indexPath.row]];
            
            NSComparisonResult resultupload,resultupload1;
            //has three possible values: NSOrderedSame,NSOrderedDescending, NSOrderedAscending

            resultupload = [today compare:UploadStartTime];
              resultupload1 = [today compare:UploadEndTime];
            // comparing two dates

            if ((resultupload==NSOrderedDescending) && (resultupload1 == NSOrderedAscending))
            {
                [cell.btnplay setTitle:@"U" forState:UIControlStateNormal];
                 cell.btnplay.hidden = false;
                cell.userInteractionEnabled = true;
                NSLog(@"today is less");
            cell.btnplay.alpha = 1.0f;
                [cell.btnplay setImage:[UIImage imageNamed:@"uploadpaperblack"] forState:UIControlStateNormal];
            [UIView animateWithDuration:1.0
              delay:0.5
              options:UIViewAnimationOptionCurveEaseInOut |
                      UIViewAnimationOptionRepeat |
                      UIViewAnimationOptionAutoreverse |
                      UIViewAnimationOptionAllowUserInteraction
              animations:^{
               cell.btnplay.alpha = 0.1f;
               //  cell.btnplay.userInteractionEnabled = true;
            }
            completion:^(BOOL finished){
            // Do nothing
            }];
            }
        //    else if(result==NSOrderedDescending)
        //    {
        //        //cell.btnplay.enabled = false;
        //        NSLog(@"newDate is less");
        //    }
            else
            {
                NSLog(@"Both dates are same");
              //  cell.btnplay.hidden = true;
            }
        
        
    }
    else
    {
        cell.vw_shadow2.hidden=NO;
        ///////VIEW 2////////
        cell.lbl_subjectnm2.text=[NSString stringWithFormat:@"%@",[Dictdata valueForKey:@"SubjectName"][indexPath.row]];
        //-----TODATE---------
        NSString * yourJSONString = [Dictdata valueForKey:@"PaperDate"][indexPath.row];//EndDate
        NSDateFormatter *currentDTFormatter = [[NSDateFormatter alloc] init];;
        [currentDTFormatter setDateFormat:@"yyyy-MM-dd"];
        NSDate *dateFromString = [currentDTFormatter dateFromString:yourJSONString];
        [currentDTFormatter setDateFormat:@"MMM"];
        NSString *monthnm = [currentDTFormatter stringFromDate:dateFromString];
        [currentDTFormatter setDateFormat:@"dd"];
        NSString *onlydt = [currentDTFormatter stringFromDate:dateFromString];
        
        cell.lbl_month2.text=[NSString stringWithFormat:@"%@",monthnm];
        cell.lbl_dt2.text=[NSString stringWithFormat:@"%@",onlydt];
        
        cell.lbl_marks2.text=[NSString stringWithFormat:@"%@/%@ Marks",[Dictdata valueForKey:@"StudentMark"][indexPath.row],[Dictdata valueForKey:@"TotalMarks"][indexPath.row]];
        
        
        float f=[[[Dictdata valueForKey:@"StudentMark"] objectAtIndex:indexPath.row] floatValue];
        float f2=[[[Dictdata valueForKey:@"TotalMarks"] objectAtIndex:indexPath.row] floatValue];
        
        ////////PROGRESSBAR
        
        [cell.prog_mark.layer setCornerRadius:cell.prog_mark.frame.size.height/2];
        cell.prog_mark.clipsToBounds = true;
        [cell.prog_mark setTransform:CGAffineTransformMakeScale(1.0f, 3.0f)];
        
        cell.prog_mark.progress =  f/100.0;
        cell.prog_mark.trackTintColor = [UIColor colorWithHex:0xDDDDDD];
        cell.prog_mark.progressTintColor = [UIColor colorWithHex:0x0A385A];
       
        
    }
    
    return cell;
    
}

-(void)yourButtonClicked:(UIButton*)sender
{
    NSDictionary *dict = [Dictdata objectAtIndex:sender.tag];
    if ([[dict valueForKey:@"Type"]isEqualToString:@"upload"]) {
        if ([sender.titleLabel.text isEqualToString:@"P"]) {
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];

            viewpdfViewController * next = [storyboard instantiateViewControllerWithIdentifier:@"viewpdfViewController"];
            next.dictdata = dict;
            [self.navigationController pushViewController:next animated:YES];
        }
        else
        {
            NSLog(@"title is U");
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
            ImageUploadViewController * next = [storyboard instantiateViewControllerWithIdentifier:@"ImageUploadViewController"];
            next.dictdata = dict;
            [self.navigationController pushViewController:next animated:YES];
        }
        
    }
    else
    {
//            ICPDF *add = [[ICPDF alloc] initWithNibName:@"ICPDF" bundle:nil];
//            add.PaperId=[dict valueForKey:@"PaperID"];
//            add.ICPaperType=[dict valueForKey:@"PaperTypeName"];
//            [self.navigationController pushViewController:add animated:YES];
        
        if ([sender.titleLabel.text isEqualToString:@"P"]) {
//                   UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
//
//                   viewpdfViewController * next = [storyboard instantiateViewControllerWithIdentifier:@"viewpdfViewController"];
//                   next.dictdata = dict;
//                   [self.navigationController pushViewController:next animated:YES];
            [self Callgeneratepaperdata:dict];
               }
               else
               {
                   NSLog(@"title is U");
                   UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
                   ImageUploadViewController * next = [storyboard instantiateViewControllerWithIdentifier:@"ImageUploadViewController"];
                   next.dictdata = dict;
                   [self.navigationController pushViewController:next animated:YES];
               }
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
//    ICPDF *add = [[ICPDF alloc] initWithNibName:@"ICPDF" bundle:nil];
//    add.PaperId=[Dictdata valueForKey:@"PaperID"][indexPath.row];
//    add.ICPaperType=[Dictdata valueForKey:@"PaperTypeName"][indexPath.row];
//    [self.navigationController pushViewController:add animated:YES];
    
}



- (IBAction)btn_BACK:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btn_SEARCH:(id)sender {
}
@end
