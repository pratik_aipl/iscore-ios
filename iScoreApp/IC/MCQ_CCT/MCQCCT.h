//
//  MCQCCT.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 9/17/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MZTimerLabel.h"
#import "MCQCCTModel.h"

@interface MCQCCT : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource,UIScrollViewDelegate>
{
    MZTimerLabel *timerExample11;
    NSMutableArray *marrFlag;
    NSMutableArray *marrFlag1;
    
    
    
    NSMutableArray *Rtimer,*totAttempt;
    
    NSMutableArray *tempCorrect,*tempIncorrect,*tempNotapper;
    NSMutableArray *tempCorrectOp,*tempIncorrectOp,*tempNotapperOp;
    
    int Stopflag,Takantime,CountDownTime;
    UILabel *lbltotTaken;
    
    NSMutableArray* arrChapter;
    
    NSMutableArray *arrResponce,*arrOption;
    
    
    
}

@property (retain , nonatomic)NSString *cctPaperID;

@property (retain , nonatomic)NSMutableArray<MCQCCTModel *> *mcqCCTArray;
@property (retain , nonatomic)NSString *chapterList;
@property (retain , nonatomic)NSString *TBStatus;
@property(nonatomic,assign)NSString * subjectId;
@property(nonatomic,assign)NSString * subjectName;
@property(nonatomic,assign)NSString * levelId;


@property (retain , nonatomic)NSString *isRendom;
@property (retain , nonatomic)NSString *isNotAppered;
@property (retain , nonatomic)NSString *inCorredt;


@property (strong, nonatomic) IBOutlet UILabel *lbl_attemp;


//Outlet
@property (strong, nonatomic) IBOutlet UICollectionView *collDispQuestionNo;
@property (strong, nonatomic) IBOutlet UICollectionView *collDispQuestions;

@property (weak, nonatomic) IBOutlet UILabel *lblDispQuestionNo;
@property (strong, nonatomic) IBOutlet UILabel *lbl_countdown;
@property (strong, nonatomic) IBOutlet UIView *vw_countdown;

@property (strong, nonatomic) IBOutlet UILabel *lbl_outoff;



//Outlet


- (IBAction)btn_SUBMIT:(id)sender;
- (IBAction)btn_BACK_A:(id)sender;
- (IBAction)btn_HOME_A:(id)sender;



@end
