//
//  MCQCCT.m
//  iScoreApp
//
//  Created by ADMIN-Khushal on 9/17/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "MCQCCT.h"
#import "CVCDispQuestions.h"
#import "CVCQuestionNo.h"
#import "Constant.h"
#import "SharedData.h"
#import "AppDelegate.h"
//#import "Dashboard.h"
#import "MyModel.h"
#import "API.h"
#import "TestReportVC.h"
#import "MyModel.h"
#import "UIColor+CL.h"
#import "HomeVC.h"
#import "ApplicationConst.h"

#import "MCQCCTModel.h"




@interface MCQCCT ()

@property (nonatomic, assign) CGFloat lastContentOffset;

@end

NSString * strUrlcct;
NSString * strQuestioncct , * strOption1cct , * strOption2cct ,* strOption3cct , * strOption4cct;
CVCDispQuestions *cellcct ;


@implementation MCQCCT

int curruntRowcct;
int lastHeaderIDcct;
NSUInteger attemptedcct;
MZTimerLabel *timercct,*totTakencct;


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    Stopflag=0;
    Takantime=0;
    curruntRowcct=0;
    attemptedcct=0;
    lastHeaderIDcct=0;
    CountDownTime=0;
    
    
    [_collDispQuestions registerNib:[UINib nibWithNibName:@"CVCDispQuestions" bundle:nil] forCellWithReuseIdentifier:@"Cell"];
    
    [_collDispQuestionNo registerNib:[UINib nibWithNibName:@"CVCQuestionNo" bundle:nil] forCellWithReuseIdentifier:@"Cell"];
    
    arrChapter = [[NSMutableArray alloc]initWithArray:[_chapterList componentsSeparatedByString:@","]];
    
    
    NSLog(@"chapter List %@",_chapterList);
    NSLog(@"subject ID %@",_subjectId);
    NSLog(@"Levels %@",_levelId);
    NSLog(@"timercct Status %@",_TBStatus);
    Rtimer=[[NSMutableArray alloc]init];
    [self getData];
    //[self getTotalDuration];
    
    
    
    cellcct.scrollVQuestion.delegate = self;
    
    marrFlag=[[NSMutableArray alloc]init];
    marrFlag1=[[NSMutableArray alloc]init];
    totAttempt=[[NSMutableArray alloc]init];
    
    tempCorrect=[[NSMutableArray alloc]init];
    tempIncorrect=[[NSMutableArray alloc]init];
    tempNotapper=[[NSMutableArray alloc]init];
    tempCorrectOp=[[NSMutableArray alloc]init];
    tempIncorrectOp=[[NSMutableArray alloc]init];
    tempNotapperOp=[[NSMutableArray alloc]init];
    
    for(int i = 0; i < ShareData.marrmcqquestion.count; i++) {
        
        [Rtimer addObject:@"0"];
        [totAttempt addObject:@"0"];
        [marrFlag addObject:[NSString stringWithFormat:@"0"]];
        
    }
    NSLog(@"myArray:\n%@", marrFlag);
    
    for(int i = 0; i < ShareData.marrmcqquestion.count; i++) {
        [marrFlag1 addObject:[NSString stringWithFormat:@"0"]];
    }
    NSLog(@"myArray:\n%@", marrFlag1);
    
    
    timercct = [[MZTimerLabel alloc] initWithLabel:_lbl_countdown andTimerType:MZTimerLabelTypeTimer];
    [timercct setCountDownTime:CountDownTime];
    timercct.delegate=self;
    [timercct start];
    
    
    
    
    if ([_TBStatus intValue]==1)
    {
        cellcct.lbl_quetimer.hidden=NO;
        _vw_countdown.hidden=NO;
        
        
        [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(takantime) userInfo:nil repeats:YES];
        
    }
    else
    {
        cellcct.lbl_quetimer.hidden=YES;
        _vw_countdown.hidden=YES;
    }
    
    
    
    lbltotTaken=[[UILabel alloc] init];
    
    totTakencct = [[MZTimerLabel alloc] initWithLabel:lbltotTaken andTimerType:MZTimerLabelTypeStopWatch];
    [totTakencct start];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)timerLabel:(MZTimerLabel*)timerLabel finshedCountDownTimerWithTime:(NSTimeInterval)countTime{
    
    //[totTakencct pause];
    
   /* UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"Alert"
                                 message:@"Do you want to Continues test."
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    //Add Buttons
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"Continue.."
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
                                    //Handle your yes please button action here
                                    timercct = [[MZTimerLabel alloc] initWithLabel:_lbl_countdown andTimerType:MZTimerLabelTypeStopWatch];
                                    [timercct start];
                                    [timercct addTimeCountedByTime:CountDownTime];
                                    [timercct start];
                                    [totTakencct start];
                                    
                                }];
    
    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle:@"NO"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   //Handle no, thanks button
                                   [self btnSubmit];
                               }];
    
    //Add your buttons to alert controller
    
    [alert addAction:yesButton];
    [alert addAction:noButton];
    
    [self presentViewController:alert animated:YES completion:nil];
    
    */
    
}

-(void)getTotalDuration
{
    for (int i=0; i<[ShareData.marrmcqquestion count]; i++) {
        CountDownTime+=[[[ShareData.marrmcqquestion objectAtIndex:i] valueForKey:@"Duration"] intValue];
    }
    
}


-(void)takantime
{
    Takantime+=1;
    /* if ([_lbl_countdown.text isEqualToString:@"00:00"]) {
     
     }*/
    
    /*   if (Takantime==CountDownTime) {
     [timercct pause];
     [cellcct.mztime pause];
     UIAlertController * alert = [UIAlertController
     alertControllerWithTitle:@"Alert"
     message:@"Do you still wish to continue.?"
     preferredStyle:UIAlertControllerStyleAlert];
     
     //Add Buttons
     
     UIAlertAction* yesButton = [UIAlertAction
     actionWithTitle:@"YES"
     style:UIAlertActionStyleDefault
     handler:^(UIAlertAction * action) {
     //Handle your yes please button action here
     
     [timercct start];
     [cellcct.mztime start];
     }];
     
     UIAlertAction* noButton = [UIAlertAction
     actionWithTitle:@"NO"
     style:UIAlertActionStyleDefault
     handler:^(UIAlertAction * action) {
     //Handle no, thanks button
     [self btn_SUBMIT:nil];
     }];
     
     //Add your buttons to alert controller
     
     [alert addAction:yesButton];
     [alert addAction:noButton];
     
     [self presentViewController:alert animated:YES completion:nil];
     }
     */
    
    
}
-(void)viewWillLayoutSubviews{
    
    
}

#pragma mark - UICollectionView Method


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    if(collectionView == self.collDispQuestionNo){
        return _mcqCCTArray.count;
    }else{
        return _mcqCCTArray.count;
    }
    
}


-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    if(collectionView == self.collDispQuestionNo){
        
        CVCQuestionNo *cellNo = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
        cellNo.lblQuestionNo.text = [NSString stringWithFormat:@"%ld",indexPath.row + 1];
        [cellNo.lblSelection setHidden:true];
        if(indexPath.row == curruntRowcct){
            [cellNo.lblSelection setHidden:false];
        }
        
        cellNo.btn_flag1.tag=5;
        if ([[marrFlag objectAtIndex:indexPath.row] intValue]==0) {
            cellNo.btn_flag1.selected=NO;
        }
        else
        {
            cellNo.btn_flag1.selected=YES;
        }
       
        
        return cellNo;
        
    }else{
        
        cellcct = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];

        MCQCCTModel *arrmcq=[_mcqCCTArray objectAtIndex:indexPath.row];
        
        
        
        
        
        if ([_TBStatus intValue]==1)
        {
            cellcct.lbl_quetimer.hidden=NO;
            _vw_countdown.hidden=NO;
        }
        else
        {
            cellcct.lbl_quetimer.hidden=YES;
            _vw_countdown.hidden=YES;
        }
        
        cellcct.webVQuestion.frame=[[UIScreen mainScreen] bounds];
        
        cellcct.viewQuestionHeight.constant = 50.0;
        cellcct.viewOption1Height.constant = 50.0;
        cellcct.viewOption2Height.constant = 50.0;
        cellcct.viewOption3Height.constant = 50.0;
        cellcct.viewOption4Height.constant = 50.0;
        
        cellcct.webVQuestion.tag = 1;
        cellcct.webVOption1.tag = 2;
        cellcct.webVOption2.tag = 3;
        cellcct.webVOption3.tag = 4;
        cellcct.webVOption4.tag = 5;
        
        cellcct.webVQuestion.contentMode = UIViewContentModeScaleAspectFit;
        
        strQuestioncct = [self displayWebviewORTextView:arrmcq.Question Webview:cellcct.webVQuestion TextView:cellcct.txtVQuestion BGView:cellcct.viewQuestionHeight];
        
        
        cellcct.viewOption1.hidden=YES;
        cellcct.viewOption2.hidden=YES;
        cellcct.viewOption3.hidden=YES;
        cellcct.viewOption4.hidden=YES;
        
        
        
        //int optot=[NSString stringWithFormat:@"%d",[arrmcq.optionsArray count]];
        
        //NSLog(@"optot == %d",optot);
        
        for (int i=0; i < [arrmcq.optionsArray count]; i++) {
            
            MCQCCTOptions *arrOption=[arrmcq.optionsArray objectAtIndex:i];
            
            NSLog(@"%@",arrOption.Options);
            NSLog(@"%@",arrOption.MCQOPtionID);
            
            
            switch (i) {
                case 0:
                    
                    cellcct.viewOption1.hidden=NO;
                    strOption1cct = [self displayWebviewORTextView:arrOption.Options Webview:cellcct.webVOption1 TextView:cellcct.txtVOption1 BGView:cellcct.viewOption1Height];
                    break;
                case 1:
                    cellcct.viewOption2.hidden=NO;
                    strOption2cct = [self displayWebviewORTextView:arrOption.Options Webview:cellcct.webVOption2 TextView:cellcct.txtVOption2 BGView:cellcct.viewOption2Height];
                    break;
                case 2:
                    cellcct.viewOption3.hidden=NO;
                    strOption3cct = [self displayWebviewORTextView:arrOption.Options Webview:cellcct.webVOption3 TextView:cellcct.txtVOption3 BGView:cellcct.viewOption3Height];
                    break;
                case 3:
                    cellcct.viewOption4.hidden=NO;
                    strOption4cct = [self displayWebviewORTextView:arrOption.Options Webview:cellcct.webVOption4 TextView:cellcct.txtVOption4 BGView:cellcct.viewOption4Height];
                    break;
                    
                default:
                    break;
            }
        }
        
        
        //[Rtimer replaceObjectAtIndex:curruntRowcct withObject:cellcct.lbl_quetimer.text];
        
        cellcct.btnOption1.tag = indexPath.row;
        cellcct.bntOption2.tag = indexPath.row;
        cellcct.btnOption3.tag = indexPath.row;
        cellcct.btnOption4.tag = indexPath.row;
        
        [cellcct.btnOption1 addTarget:self action:@selector(selectedAnswer1:) forControlEvents:UIControlEventTouchUpInside];
        [cellcct.bntOption2 addTarget:self action:@selector(selectedAnswer2:) forControlEvents:UIControlEventTouchUpInside];
        [cellcct.btnOption3 addTarget:self action:@selector(selectedAnswer3:) forControlEvents:UIControlEventTouchUpInside];
        [cellcct.btnOption4 addTarget:self action:@selector(selectedAnswer4:) forControlEvents:UIControlEventTouchUpInside];
        
        if ([arrmcq.selectedAns intValue] != 0) {
            int  SelectedA=[arrmcq.selectedAns intValue];
            NSLog(@"selected Answer : %d",SelectedA);
            
            switch (SelectedA) {
                case 1:
                    
                    [self selectedOptionColor:cellcct.lblA ];
                    [self unSelectedOptionColor:cellcct.lblB :cellcct.lblC  :cellcct.lblD];
                    
                    break;
                    
                case 2:
                    
                    [self selectedOptionColor:cellcct.lblB];
                    [self unSelectedOptionColor:cellcct.lblA :cellcct.lblC  :cellcct.lblD ];
                    break;
                    
                case 3:
                    
                    [self selectedOptionColor:cellcct.lblC ];
                    [self unSelectedOptionColor:cellcct.lblA :cellcct.lblB  :cellcct.lblD ];
                    break;
                    
                case 4:
                    
                    
                    [self selectedOptionColor:cellcct.lblD ];
                    [self unSelectedOptionColor:cellcct.lblA :cellcct.lblB  :cellcct.lblC ];
                    break;
                    
                default:
                    //[self unSelectedAllOptionColor:cellcct.lblA :cellcct.lblB :cellcct.lblC :cellcct.lblD];
                    break;
                    
            }
            
        }
        else
        {
            cellcct.lblA.layer.backgroundColor = [UIColor clearColor].CGColor;
            cellcct.lblB.layer.backgroundColor = [UIColor clearColor].CGColor;
            cellcct.lblC.layer.backgroundColor = [UIColor clearColor].CGColor;
            cellcct.lblD.layer.backgroundColor = [UIColor clearColor].CGColor;
        }
        
        cellcct.lbl_queNo.text=[NSString stringWithFormat:@"%ld",indexPath.row+1];
        
        
        
        //FLAG
        cellcct.btn_flag.tag=indexPath.row;
        cellcct.lbl_queNo.text=[NSString stringWithFormat:@"%ld",indexPath.row+1];
        [cellcct.btn_flag addTarget:self action:@selector(selectedAnswer5:) forControlEvents:UIControlEventTouchUpInside];
        
        
        if ([[marrFlag objectAtIndex:indexPath.row] intValue]==0) {
            cellcct.btn_flag.selected=NO;
        }
        else
        {
            cellcct.btn_flag.selected=YES;
        }
        
        
        return cellcct;
    }
    
}


- (void)onTick:(NSIndexPath*) first
{
    NSLog(@" %@",first);
    [_collDispQuestions reloadItemsAtIndexPaths:@[first]];
    
}



-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if(collectionView == self.collDispQuestionNo){
        
        [self moveToQuestion:indexPath];
        
    }else{
        
        
    }
    
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if(collectionView == self.collDispQuestionNo){
        return CGSizeMake((self.collDispQuestionNo.frame.size.width/10), (self.collDispQuestionNo.frame.size.height));
    }
    else{
        
        return CGSizeMake((self.collDispQuestions.frame.size.width),(self.collDispQuestions.frame.size.height));
    }
}

#pragma mark - UIScrollView Method

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
    if (scrollView == _collDispQuestionNo) {
        NSLog(@"_collDispQuestionNo ");
    }else
    {
        NSLog(@"scrollViewDidEndDecelerating");
        if (self.lastContentOffset > scrollView.contentOffset.x){
            [self moveToPreviousQuestion];
        }
        else if(self.lastContentOffset < scrollView.contentOffset.x) {
            [self moveToNextQuestion];
        }
    }
    _lastContentOffset = scrollView.contentOffset.x;
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView{
    
    if(scrollView == self.collDispQuestions){
        NSLog(@"scrollViewDidEndScrollingAnimation");
        _lastContentOffset = scrollView.contentOffset.x;
        NSLog(@"lastContentOffset : %f",_lastContentOffset);
    }else{
    }
}

#pragma mark - Change Other Method
-(void)changeSelection:(int )index{
    
    MCQCCTModel *arrmcq=[_mcqCCTArray objectAtIndex:index];
    
    int j=0;
    for (int i=0; i<_mcqCCTArray.count; i++) {
        MCQCCTModel *temp=[_mcqCCTArray objectAtIndex:i];
        if ([temp.IsAttempt intValue]==1) {
            j++;
        }
    }
    _lbl_attemp.text=[NSString stringWithFormat:@"%02d",j];
    
    
    
    if (arrmcq.selectedAns!=0) {
        int  SelectedA=[arrmcq.selectedAns intValue];
        NSLog(@"selected Answer : %d",SelectedA);
        
        switch (SelectedA) {
            case 1:
                
                [self selectedOptionColor:cellcct.lblA ];
                [self unSelectedOptionColor:cellcct.lblB :cellcct.lblC  :cellcct.lblD];
                
                break;
                
            case 2:
                
                [self selectedOptionColor:cellcct.lblB];
                [self unSelectedOptionColor:cellcct.lblA :cellcct.lblC  :cellcct.lblD ];
                break;
                
            case 3:
                
                [self selectedOptionColor:cellcct.lblC ];
                [self unSelectedOptionColor:cellcct.lblA :cellcct.lblB  :cellcct.lblD ];
                break;
                
            case 4:
                
                
                [self selectedOptionColor:cellcct.lblD ];
                [self unSelectedOptionColor:cellcct.lblA :cellcct.lblB  :cellcct.lblC ];
                break;
                
            default:
                //[self unSelectedAllOptionColor:cellcct.lblA :cellcct.lblB :cellcct.lblC :cellcct.lblD];
                break;
                
        }
        
    }
    else
    {
        cellcct.lblA.layer.backgroundColor = [UIColor clearColor].CGColor;
        cellcct.lblB.layer.backgroundColor = [UIColor clearColor].CGColor;
        cellcct.lblC.layer.backgroundColor = [UIColor clearColor].CGColor;
        cellcct.lblD.layer.backgroundColor = [UIColor clearColor].CGColor;
    }
    
    
}


#pragma mark - Convert HTML Formated String
-(NSAttributedString *)convertIntoAttributadStr:(NSString *)str{
    
    
    NSString *htmlString = str;
    NSAttributedString *attributedString = [[NSAttributedString alloc]
                                            initWithData: [htmlString dataUsingEncoding:NSUnicodeStringEncoding]
                                            options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
                                            documentAttributes: nil
                                            error: nil
                                            ];
    
    return attributedString;
}



#pragma mark - Change Height Of Textview

- (void)changeTextViewHeight:(UITextView *)textView :(NSLayoutConstraint*)heightConstraint
{
    NSLog(@"changeTextViewHeight");
    
    
    CGSize constraint = CGSizeMake(textView.frame.size.width, CGFLOAT_MAX);
    CGSize size;
    
    NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
    CGSize boundingBox = [textView.text boundingRectWithSize:constraint
                                                     options:NSStringDrawingUsesLineFragmentOrigin
                                                  attributes:@{NSFontAttributeName:textView.font}
                                                     context:context].size;
    
    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    heightConstraint.constant = size.height + 10;
    
    NSLog(@"height : %f",size.height);
    
    
    
}

#pragma mark - change Webview height

- (void)changeheightOfWebview:(WKWebView *)aWebView :(NSLayoutConstraint*)heightConstraint{
    
  //  NSString *str = [aWebView stringByEvaluatingJavaScriptFromString:@"(document.height !== undefined) ? document.height : document.body.offsetHeight;"];
    
    [aWebView evaluateJavaScript:@"(document.height !== undefined) ? document.height : document.body.offsetHeight;" completionHandler:^(NSString *result, NSError *error)
       {
           
           NSLog(@"INNER HTML: %@",result);
           CGFloat height = result.floatValue;
           heightConstraint.constant = height + 15.0;
           NSLog(@"height: %f", height);
       }];
    
    
}


#pragma mark - Webview Delegate

//- (void)webViewDidFinishLoad:(UIWebView *)webView {
//
//
//    if (webView.tag == 1) {
//
//        if ([strQuestioncct isEqualToString:@"Web"]) {
//            [self changeheightOfWebview:cellcct.webVQuestion :cellcct.viewQuestionHeight];
//
//        }
//    }
//
//    if (webView.tag == 2) {
//
//        if ([strOption1cct isEqualToString:@"Web"]){
//            [self changeheightOfWebview:cellcct.webVOption1 :cellcct.viewOption1Height];
//        }
//    }
//
//    if (webView.tag == 3) {
//
//        if ([strOption2cct isEqualToString:@"Web"]){
//            [self changeheightOfWebview:cellcct.webVOption2 :cellcct.viewOption2Height];
//        }
//    }
//
//    if (webView.tag == 4) {
//
//        if ([strOption3cct isEqualToString:@"Web"]){
//            [self changeheightOfWebview:cellcct.webVOption3 :cellcct.viewOption3Height];
//        }
//    }
//
//    if (webView.tag == 5) {
//
//        if ([strOption4cct isEqualToString:@"Web"]){
//            [self changeheightOfWebview:cellcct.webVOption4 :cellcct.viewOption4Height];
//        }
//    }
//}

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    
    
    if (webView.tag == 1) {
        
        if ([strQuestioncct isEqualToString:@"Web"]) {
            [self changeheightOfWebview:cellcct.webVQuestion :cellcct.viewQuestionHeight];
            
        }
    }
    
    if (webView.tag == 2) {
        
        if ([strOption1cct isEqualToString:@"Web"]){
            [self changeheightOfWebview:cellcct.webVOption1 :cellcct.viewOption1Height];
        }
    }
    
    if (webView.tag == 3) {
        
        if ([strOption2cct isEqualToString:@"Web"]){
            [self changeheightOfWebview:cellcct.webVOption2 :cellcct.viewOption2Height];
        }
    }
    
    if (webView.tag == 4) {
        
        if ([strOption3cct isEqualToString:@"Web"]){
            [self changeheightOfWebview:cellcct.webVOption3 :cellcct.viewOption3Height];
        }
    }
    
    if (webView.tag == 5) {
        
        if ([strOption4cct isEqualToString:@"Web"]){
            [self changeheightOfWebview:cellcct.webVOption4 :cellcct.viewOption4Height];
        }
    }
}

#pragma mark - Check Question and Option String

- (NSString*)displayWebviewORTextView:(NSString *)Str Webview:(WKWebView*)Webview TextView:(UITextView*)TextView BGView:(NSLayoutConstraint*)BGViewHeightConstant {
    
    NSString *filePath = [MyModel getFilePath];
    
    if (![Str containsString:@"<img"]|| ![Str containsString:@"<math"] || ![Str containsString:@"<table"] ) {
        
        NSLog(@"string does not contain tags");
        Webview.hidden =true;
        TextView.hidden =false;
        NSString *filterstr = [Str stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@" "];
        TextView.attributedText = [self convertIntoAttributadStr:[NSString stringWithFormat:@"%@",filterstr]];
      //  [self changeTextViewHeight:TextView :BGViewHeightConstant];
        return @"Text";
        
    } else {
        
        NSLog(@"string contains tag!");
        if ([Str containsString:@"src"]) {
            NSLog(@"Replace Path");
            
            NSString *offUrl=[NSString stringWithFormat:@"src=\"%@/",filePath];
            
            Str = [Str stringByReplacingOccurrencesOfString:@"src=\"http://staff.parshvaa.com/" withString:offUrl];//
            NSLog(@"apdateString %@",Str);
            
        }
        
        TextView.hidden =true;
        Webview.hidden =false;
        NSURL *baseURL = [NSURL fileURLWithPath:filePath];
        [Webview loadHTMLString:Str baseURL:baseURL];
        
        Str=[self MathHTMLString:Str];
        
        
        return @"Web";
        
    }
    
}

- (NSString *)extractTextFromXML:(NSString *)xml{
//Will hold just the text
NSMutableString *text = [NSMutableString string];
NSInteger startOfSubstring = 0;
//Finds first instance of "<"
NSRange startTagRange = [xml rangeOfString:@"<"];
while(startTagRange.location != NSNotFound){
    //Extracts text from last location up to "<"
    NSString *substring = [xml substringWithRange:NSMakeRange(startOfSubstring, startTagRange.location-startOfSubstring)];
    //Removes whitespace from substring
    [text appendString:[substring stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]];

    //Searches for ">" from "<" to end of string
    NSRange startTagToEndRange = NSMakeRange(startTagRange.location, [xml length]-startTagRange.location);
    NSRange endTagRange = [xml rangeOfString:@">" options:NSCaseInsensitiveSearch range:startTagToEndRange];
    //If ">" found, then sets next location of substring to after that
    if(endTagRange.location != NSNotFound){
        startOfSubstring = endTagRange.location+1;
    }
    //If no ">", then appends rest of string and returns
    else{
        [text appendString:[xml substringFromIndex:startTagRange.location]];
        return text;
    }
    //Finds next "<" in string
    NSRange endTagToEndRange = NSMakeRange(startOfSubstring, [xml length]-startOfSubstring);
    startTagRange = [xml rangeOfString:@"<" options:NSCaseInsensitiveSearch range:endTagToEndRange];
}

return text;
}

-(NSString*)MathHTMLString :(NSString*)str
{
    NSString *htstr=[NSString stringWithFormat:@"<!DOCTYPE html>\n"
                     "<html>\n"
                     "<head>\n"
                     "<title>MathJax MathML Test Page</title>\n"
                     "<!-- Copyright (c) 2010-2016 The MathJax Consortium -->\n"
                     "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\n"
                     "<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" />\n"
                     "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n"
                     "\n"
                     "\n"
                     "<link rel=\"stylesheet\" href=\"file:///mathscribe/jqmath-0.4.3.css\">"
                     "\n"
                     "<script type=\"text/javascript\" src=\"file:///mathscribe/jquery-1.4.3.min.js\"></script>\n"
                     "\n"
                     "<script type=\"text/javascript\" src=\"file:///mathscribe/jqmath-etc-0.4.6.min.js\"></script>\n"
                     "\n"
                     "<style> body{} </style>"
                     "\n"
                     "</head>\n"
                     "<body style='font-family:'arial';font-size:22px;color:#000000;'> \n"
                     "\n"
                     "<p>"
                     "</p>\n"
                     "\n"
                     "</body>\n"
                     "</html>"];
    
    return htstr;
}



#pragma mark - Action Method

- (IBAction)btnBack:(id)sender {
    
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"Alert" message:@"Are you sure you want to Leave the test?" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction * yes = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        
    }];
    UIAlertAction * no = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault handler:nil];
    
    [alert addAction:no];
    [alert addAction:yes];
    
    [self presentViewController:alert animated:YES completion:nil];
    
}

- (IBAction)bntPrevious:(id)sender {
    
    NSLog(@"btnPrevious tap");
    [self moveToPreviousQuestion];
}

- (IBAction)btnNext:(id)sender {
    
    NSLog(@"btnNext tap");
    [self moveToNextQuestion];
    
}
-(void)CallIncorrect
{
    NSMutableArray *temp3=[[NSMutableArray alloc]initWithArray:ShareData.marrmcqquestion];
    NSMutableArray *temp4=[[NSMutableArray alloc]initWithArray:ShareData.marrmcqoption];
    
    NSLog(@"temp3  %@",temp3);
    NSUserDefaults *stander=[[NSUserDefaults alloc]init];
    
    for (int i = 0; i < temp3.count; i++) {
        
        //Filter Questions
        if ([[[temp3 valueForKey:@"isAttempted"] objectAtIndex:i] intValue]==1) {
            
            [MyModel deleteIncorrectAndNotappered:@"student_not_appeared_question" :[[temp3 objectAtIndex:i] valueForKey:@"MCQQuestionID"]];
            
            if ([[[temp3 valueForKey:@"isRight"] objectAtIndex:i] intValue]==0) {
                NSMutableDictionary *mutDic=[[NSMutableDictionary alloc] init];
                
                
                [mutDic setValue:[[temp3 objectAtIndex:i] valueForKey:@"MCQQuestionID"] forKey:@"QuestionID"];
                [mutDic setValue:[stander valueForKeyPath:@"ParseData.StudentID"] forKey:@"StudentID"];
                [mutDic setValue:[MyModel getCuruntDateTime] forKey:@"CreatedOn"];
                [MyModel insertInto_papertype:mutDic :@"student_incorrect_question"];
                [tempIncorrect addObject:[temp3 objectAtIndex:i]];
                [tempIncorrectOp addObject:[temp4 objectAtIndex:i]];
                
            }
            else
            {
                [tempCorrect addObject:[temp3 objectAtIndex:i]];
                ///REMOVE SUTDENT
                [MyModel deleteIncorrectAndNotappered:@"student_incorrect_question" :[[temp3 objectAtIndex:i] valueForKey:@"MCQQuestionID"]];
                [tempCorrectOp addObject:[temp4 objectAtIndex:i]];
            }
        }
        else
        {
            [tempNotapper addObject:[temp3 objectAtIndex:i]];
            [MyModel deleteIncorrectAndNotappered:@"student_incorrect_question" :[[temp3 objectAtIndex:i] valueForKey:@"MCQQuestionID"]];
            [tempNotapperOp addObject:[temp4 objectAtIndex:i]];
        }
    }
    
    NSLog(@"tempIncorrect %lu",(unsigned long)tempIncorrect.count);
    NSLog(@"tempCorrect %lu",(unsigned long)tempCorrect.count);
    NSLog(@"tempNotapper %lu",(unsigned long)tempNotapper.count);
}
-(void)CallnotAppered
{
    NSMutableArray *temp3=[[NSMutableArray alloc]initWithArray:ShareData.marrmcqquestion];
    NSUserDefaults *stander=[[NSUserDefaults alloc]init];
    NSLog(@"temp3  %@",temp3);
    
    for (int i = 0; i < temp3.count; i++) {
        
        if ([[[temp3 valueForKey:@"isAttempted"] objectAtIndex:i] intValue]==0) {
            NSMutableDictionary *mutDic=[[NSMutableDictionary alloc] init];
            [mutDic setValue:[[temp3 objectAtIndex:i] valueForKey:@"MCQQuestionID"] forKey:@"QuestionID"];
            [mutDic setValue:[stander valueForKeyPath:@"ParseData.StudentID"] forKey:@"StudentID"];
            [mutDic setValue:[MyModel getCuruntDateTime] forKey:@"CreatedOn"];
            [MyModel insertInto_papertype:mutDic :@"student_not_appeared_question"];
        }
    }
}


- (void)btnSubmit {
    
    //[MyModel deleteStudentTabledata];
   /*
    [self insertIntostudent_mcq_test_hdrTable];
    [self gettemp_mcq_test_dtl];
    [self insertIntostudent_mcq_test_dtlTable];
    [self gettemp_mcq_test_chapter];
    [self insertIntostudent_mcq_test_levelTable];
    [self insertIntostudent_mcq_test_chapterTable];
    
    [self CallIncorrect];
    [self CallnotAppered];
    [MyModel deletetemporaryTabledata];
    
    NSMutableArray *temp_arr1=[[NSMutableArray alloc] initWithArray:ShareData.marrtemp_mcq_test_dtl];
    NSMutableArray *temp_arr2=[[NSMutableArray alloc] initWithArray:ShareData.marrtemp_mcq_test_hdr];
    
    
    NSLog(@"Log %@",temp_arr1);
    NSLog(@"Log %@",temp_arr2);
    
    
    
    
    attemptedcct = [MyModel getCount:[NSString stringWithFormat:@"select COUNT(*) from  student_mcq_test_dtl  where StudentMCQTestHDRID= %d  AND IsAttempt= %d",lastHeaderIDcct,1]];
    
    NSLog(@"attemptedcct : %lu",(unsigned long)attemptedcct);
    
    [self insertIntonotificationTable];
    [self getnotificationTableData];
    
    if ([[UserDefault valueForKey:@"LastSyncTime"] isEqualToString:@""]) {
        
        strUrlcct = [NSString stringWithFormat:@"select * FROM student_mcq_test_hdr"];
        [self getstudent_mcq_test_hdrTableData:strUrlcct];
        
    }else{
        
        //NSLog(@"last : %@",[UserDefault valueForKey:@"LastSyncTime"]);
        NSString *dateStr =[NSString stringWithFormat:@"%@",[UserDefault valueForKey:@"LastSyncTime"]];
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSDate *date = [dateFormat dateFromString:dateStr];
        
        [dateFormat setDateFormat:@"dd-MM-yyyy HH:mm:ss"];
        dateStr = [dateFormat stringFromDate:date];
        
        strUrlcct =[NSString stringWithFormat:@"select * FROM student_mcq_test_hdr where CreatedOn > '%@'",dateStr];
        [self getstudent_mcq_test_hdrTableData:strUrlcct];
    }
    
    for (int i = 0; i<ShareData.marrstudent_mcq_test_hdr.count; i++) {
        
        NSString * hdrID  = [[ShareData.marrstudent_mcq_test_hdr objectAtIndex:i]valueForKey:@"StudentMCQTestHDRID"];
        NSLog(@"hdrID : %@",hdrID);
        
        FMResultSet * result = [MyModel selectQuery:[NSString stringWithFormat:@"select * FROM student_mcq_test_chapter where StudentMCQTestHDRID = %@ ",hdrID]];
        
        NSMutableDictionary* mdict = [[NSMutableDictionary alloc]init];
        NSMutableArray * marr = [[NSMutableArray alloc]init];
        
        while ([result next]) {
            
            NSMutableDictionary * subDict = [[NSMutableDictionary alloc]init];
            
            [subDict setValue:[result stringForColumn:@"StudentMCQTestChapterID"] forKey:@"StudentMCQTestChapterID"];
            [subDict setValue:[result stringForColumn:@"StudentMCQTestHDRID"] forKey:@"StudentMCQTestHDRID"];
            [subDict setValue:[result stringForColumn:@"ChapterID"] forKey:@"ChapterID"];
            
            [marr addObject:subDict];
        }
        
        [[ShareData.marrstudent_mcq_test_hdr objectAtIndex:i] setValue:marr forKey:@"chapterArray"];
        
        FMResultSet * result1 = [MyModel selectQuery:[NSString stringWithFormat:@"select * FROM student_mcq_test_dtl where StudentMCQTestHDRID = %@ ",hdrID]];
        
        mdict  = [[NSMutableDictionary alloc]init];
        marr = [[NSMutableArray alloc]init];
        
        while ([result1 next]) {
            
            NSMutableDictionary * subDict = [[NSMutableDictionary alloc]init];
            
            [subDict setValue:[result1 stringForColumn:@"StudentMCQTestDTLID"] forKey:@"StudentMCQTestDTLID"];
            [subDict setValue:[result1 stringForColumn:@"StudentMCQTestHDRID"] forKey:@"StudentMCQTestHDRID"];
            [subDict setValue:[result1 stringForColumn:@"srNo"] forKey:@"srNo"];
            [subDict setValue:[result1 stringForColumn:@"QuestionID"] forKey:@"QuestionID"];
            [subDict setValue:[result1 stringForColumn:@"AnswerID"] forKey:@"AnswerID"];
            [subDict setValue:[result1 stringForColumn:@"IsAttempt"] forKey:@"IsAttempt"];
            [subDict setValue:[result1 stringForColumn:@"CreatedOn"] forKey:@"CreatedOn"];
            [subDict setValue:[result1 stringForColumn:@"CreatedBy"] forKey:@"CreatedBy"];
            [subDict setValue:[result1 stringForColumn:@"ModifiedBy"] forKey:@"ModifiedBy"];
            [subDict setValue:[result1 stringForColumn:@"ModifiedOn"] forKey:@"ModifiedOn"];
            
            [marr addObject:subDict];
        }
        
        
        [[ShareData.marrstudent_mcq_test_hdr objectAtIndex:i] setValue:marr forKey:@"detailArray"];
    }
    NSLog(@"marrstudent_mcq_test_hdr : %@" ,ShareData.marrstudent_mcq_test_hdr);
    
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:ShareData.marrstudent_mcq_test_hdr options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSLog(@"Jsonstring:%@", jsonString);
    
    NSString * url = [NSString stringWithFormat:@"%@/web-services/datasync?",Main_URL];
    
    NSMutableDictionary * param = [[NSMutableDictionary alloc]init];
    */
    //[param setObject:@"mcq" forKey:@"action"];
    //[param setObject:jsonString forKey:@"data"];
    //[param setObject:[UserDefault valueForKey:@"reg_key"] forKey:@"RegKey"];
    
    /* [API PostUrl:url Parameters:param andCallback:^(NSMutableDictionary *result, NSError *error)  {
     
     if (error==nil) {
     
     NSLog(@"Response:-%@",result);
     
     if ([result valueForKey:@"status"]) {
     
     if ([result valueForKey:@"MCQLastSync"] != nil) {
     
     [UserDefault setValue:[result valueForKey:@"MCQLastSync"] forKey:@"LastSyncTime"];
     NSLog(@"LastSynctime : %@",[UserDefault valueForKey:@"LastSyncTime"]);
     
     }
     }
     
     }else{
     
     NSLog(@"Error : %@ ",error);
     
     }
     }];*/
    
    
    // ResultView * next = [self.storyboard instantiateViewControllerWithIdentifier:@"ResultView"];
    // [self.navigationController pushViewController:next animated:YES];
    
}

-(void)CallSendTestToServer
{
    
    [APP_DELEGATE showLoadingView:@""];
    ////////////TIME CALCULATION/////////////////
    NSLog(@"==%@",[APP_DELEGATE getCurrentDate]);
    NSString *strtime=[NSString stringWithFormat:@"%@",[APP_DELEGATE getCurrentDate]];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date = [dateFormat dateFromString:strtime];
    
    [dateFormat setDateFormat:@"HH"];
    NSString *hour=[dateFormat stringFromDate:date];
    
    [dateFormat setDateFormat:@"mm"];
    NSString *mins=[dateFormat stringFromDate:date];
    
    [dateFormat setDateFormat:@"ss"];
    NSString *sec=[dateFormat stringFromDate:date];
    
    int hours=[hour intValue]*60;
    int minu=hours+[mins intValue];
    int totSec=minu*60;
    NSLog(@"%d",totSec+[sec intValue]);
    
    
    
    ////////////Total Right/////////////////
    int totRight=0;
    
    for (int q = 0; q<_mcqCCTArray.count; q++) {
        MCQCCTModel *temp=[_mcqCCTArray objectAtIndex:q];
        
        if ([temp.IsAttempt intValue]==1) {
            
            for (int i=0; i < [temp.optionsArray count]; i++) {
                MCQCCTOptions *tempOp=[temp.optionsArray objectAtIndex:i];
                
                if ([tempOp.isCorrect intValue]==1) {
                    int opID=[tempOp.MCQOPtionID intValue];
                    int selectedId=[temp.AnswerID intValue];
                    if (opID==selectedId) {
                        totRight++;
                    }
                }
            }
        }
    }
    ////////////Create Details Array/////////////////
    
    NSMutableArray *detail_array=[[NSMutableArray alloc]init];
    
    for (int t=0; t<_mcqCCTArray.count; t++) {
         MCQCCTModel *tempModel=[_mcqCCTArray objectAtIndex:t];
        
        
        
        NSMutableDictionary *tempDict=[[NSMutableDictionary alloc]init];
        [tempDict setValue:tempModel.ClassMCQTestDTLID forKey:@"DetailID"];
        [tempDict setValue:tempModel.AnswerID forKey:@"SelectedAnswerID"];
        [tempDict setValue:tempModel.IsAttempt forKey:@"isAttempt"];
        
        [detail_array addObject:tempDict];
    }
    
    NSError *error = nil;
    NSString *createJSON = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:detail_array
                                                                                          options:NSJSONWritingPrettyPrinted
                                                                                            error:&error]
                                                 encoding:NSUTF8StringEncoding];
    
 
    
    //Post Method Request
    
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    [AddPost setValue: [[NSUserDefaults standardUserDefaults] valueForKeyPath:@"ParseData.StudentID"] forKey:@"StudID"];//taken_test
    [AddPost setValue: _lbl_attemp.text forKey:@"total_attempt"];
    [AddPost setValue: [NSString stringWithFormat:@"%lu",(unsigned long)_mcqCCTArray.count] forKey:@"total_question"];
    [AddPost setValue: [NSString stringWithFormat:@"%d",totRight] forKey:@"total_right"];
    [AddPost setValue: createJSON forKey:@"detail_array"];
    [AddPost setValue: [NSString stringWithFormat:@"%d",totSec+[sec intValue]] forKey:@"start_time"];
    [AddPost setValue: _cctPaperID forKey:@"paper_id"];//paper_id
    
    
    NSLog(@"StudID %@",AddPost);
    
    
    
    //[[NSUserDefaults standardUserDefaults]setObject:AddPost forKey:@"TempPost"];
    
    //NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    //AddPost=[[NSUserDefaults standardUserDefaults] valueForKey:@"TempPost"];
    
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
     manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html",@"application/json",nil];
  
    
    [manager POST:[NSString stringWithFormat:@"%@submit_mcq_test",ICAPIURL]  parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject){
        
        NSLog(@"JSON: %@", responseObject);
        
        if ([[responseObject valueForKey:@"status"] boolValue]) {
            
            
            UIAlertController * alert = [UIAlertController
                                         alertControllerWithTitle:@"DONE"
                                         message:@"Your Test Submited!"
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            //Add Buttons
            
            UIAlertAction* yesButton = [UIAlertAction
                                        actionWithTitle:@"OK"
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * action) {
                                            //Handle your yes please button action here
                                             [APP_DELEGATE hideLoadingView];
                                            [self.navigationController popViewControllerAnimated:YES];
                                        }];
            
            
            //Add your buttons to alert controller
            
            [alert addAction:yesButton];
            
            [self presentViewController:alert animated:YES completion:nil];
            
            
        }
        else
        {
            UIAlertController * alert = [UIAlertController
                                      alertControllerWithTitle:@"DONE"
                                      message:@"Somthing missing"
                                      preferredStyle:UIAlertControllerStyleAlert];
            
            //Add Buttons
            
            UIAlertAction* yesButton = [UIAlertAction
                                        actionWithTitle:@"OK"
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * action) {
                                            //Handle your yes please button action here
                                        }];
            
            //Add your buttons to alert controller
            
            [alert addAction:yesButton];
            [self presentViewController:alert animated:YES completion:nil];
            
        }
        
         [APP_DELEGATE hideLoadingView];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error){
        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];
    }];
    
    
}

#pragma mark - Move NEXT_PREVIOUS Method

-(void)moveToNextQuestion{
    
    NSLog(@"moveToNextQuestion");
    curruntRowcct++;
    
    if(curruntRowcct == [ShareData.marrmcqquestion count] - 1){
        
        //_btnNext.enabled = false;
        
    }else{
        
        if(curruntRowcct > 0 ){
            
            //_btnPrevious.enabled = true;
        }
    }
    
    NSIndexPath *nextItem = [NSIndexPath indexPathForItem:curruntRowcct inSection:0];
    
    [self.collDispQuestions scrollToItemAtIndexPath:nextItem atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
    [self.collDispQuestionNo scrollToItemAtIndexPath:nextItem atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:NO];
    
    [self changeQuestionNo:curruntRowcct];
    
    NSLog(@"curruntRowcct %d",curruntRowcct);
    
    [self.collDispQuestionNo reloadData];
    //[self.collDispQuestions reloadData];
    
    
}

-(void)moveToPreviousQuestion{
    
    NSLog(@"moveToPreviousQuestion");
    curruntRowcct--;
    
    if(curruntRowcct == 0){
        
        //_btnPrevious.enabled = false;
        
    }else{
        
        if(curruntRowcct < [_mcqCCTArray count] - 1 ){
            
            //_btnNext.enabled=true;
            
        }
    }
    
    NSIndexPath *nextItem = [NSIndexPath indexPathForItem:curruntRowcct inSection:0];
    
    [self.collDispQuestions scrollToItemAtIndexPath:nextItem atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
    [self.collDispQuestionNo scrollToItemAtIndexPath:nextItem atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
    
    [self changeQuestionNo:curruntRowcct];
    NSLog(@"curruntRowcct %d",curruntRowcct);
    
    [self.collDispQuestionNo reloadData];
    [self.collDispQuestions reloadData];
    
}

-(void)moveToQuestion:(NSIndexPath*)indexpath{
    
    NSLog(@"Indexpath : %ld",(long)indexpath.row);
    
    curruntRowcct = (int)indexpath.row;
    
    if (curruntRowcct > 0) {
        
        //_btnPrevious.enabled = true;
        
        if (curruntRowcct < [ShareData.marrmcqquestion count]-1) {
            
            //_btnNext.enabled = true;
            
        }else{
            
            //_btnNext.enabled = false;
        }
        
    }else{
        
        // _btnPrevious.enabled = false;
    }
    
    NSIndexPath *nextItem = [NSIndexPath indexPathForItem:curruntRowcct inSection:0];
    
    [self.collDispQuestions scrollToItemAtIndexPath:nextItem atScrollPosition:UICollectionViewScrollPositionNone animated:YES];
    [self.collDispQuestionNo scrollToItemAtIndexPath:nextItem atScrollPosition:UICollectionViewScrollPositionNone animated:NO];
    
    [self changeQuestionNo:curruntRowcct];
    
    [self.collDispQuestionNo reloadData];
    //[self.collDispQuestions reloadData];
}

- (void)changeQuestionNo:(int)questionNo{
    
    _lblDispQuestionNo.text = [NSString stringWithFormat:@"%d / %lu", questionNo + 1,(unsigned long)ShareData.marrmcqquestion.count];
    
}

#pragma mark - ANSWER Select_Unselect Method

-(void)selectedOptionColor:(UILabel*)label{
    
    label.layer.backgroundColor = [UIColor colorWithHex:0x075584].CGColor;
    label.layer.cornerRadius = label.layer.frame.size.height/2;
    // label.textColor=[UIColor whiteColor];
    
}

-(void)unSelectedOptionColor:(UILabel*)label1 :(UILabel*)label2 :(UILabel*)label3{
    
    label1.layer.backgroundColor = [UIColor clearColor].CGColor;
    label2.layer.backgroundColor = [UIColor clearColor].CGColor;
    label3.layer.backgroundColor = [UIColor clearColor].CGColor;
    
    /* label1.textColor=[UIColor blackColor];
     label2.textColor=[UIColor blackColor];
     label3.textColor=[UIColor blackColor];*/
}

-(void)unSelectedAllOptionColor:(UILabel*)label1 :(UILabel*)label2 :(UILabel*)label3 :(UILabel*)label4 {
    
    label1.layer.backgroundColor = [UIColor clearColor].CGColor;
    label2.layer.backgroundColor = [UIColor clearColor].CGColor;
    label3.layer.backgroundColor = [UIColor clearColor].CGColor;
    label4.layer.backgroundColor = [UIColor clearColor].CGColor;
    
    label1.textColor=[UIColor blackColor];
    label2.textColor=[UIColor blackColor];
    label3.textColor=[UIColor blackColor];
    label4.textColor=[UIColor blackColor];
}

-(void)selectedAnswer1:(UIButton *)sender{
    
    MCQCCTModel *arrmcq=[_mcqCCTArray objectAtIndex:sender.tag];
    MCQCCTOptions *arrOption=[arrmcq.optionsArray objectAtIndex:0];
    
    [arrmcq setValue:arrOption.MCQOPtionID forKey:@"AnswerID"];
    [arrmcq setValue:@"1" forKey:@"IsAttempt"];
    [arrmcq setValue:@"1" forKey:@"selectedAns"];
    
    
   /* [self updatetemp_mcq_test_dtl:sender.tag selectedOption:[[[[[arrResponce objectAtIndex:sender.tag] valueForKey:@"Question_option"] objectAtIndex:0] valueForKey:@"MCQOPtionID"] intValue]];
    [self updatemarrmcqquestion:(int)sender.tag :1];
    */
    
    [self changeSelection:sender.tag];
    [_collDispQuestions reloadData];
    
}
-(void)selectedAnswer2:(UIButton *)sender{
    
    
    MCQCCTModel *arrmcq=[_mcqCCTArray objectAtIndex:sender.tag];
    MCQCCTOptions *arrOption=[arrmcq.optionsArray objectAtIndex:1];
    
    [arrmcq setValue:arrOption.MCQOPtionID forKey:@"AnswerID"];
    [arrmcq setValue:@"1" forKey:@"IsAttempt"];
    [arrmcq setValue:@"2" forKey:@"selectedAns"];
    
    [self changeSelection:sender.tag];
    [_collDispQuestions reloadData];
    
    /*
    [self updatetemp_mcq_test_dtl:sender.tag selectedOption:[[[[[arrResponce objectAtIndex:sender.tag] valueForKey:@"Question_option"] objectAtIndex:1] valueForKey:@"MCQOPtionID"] intValue]];
    [self updatemarrmcqquestion:(int)sender.tag :2];
    */
}
-(void)selectedAnswer3:(UIButton *)sender{
    
    MCQCCTModel *arrmcq=[_mcqCCTArray objectAtIndex:sender.tag];
    MCQCCTOptions *arrOption=[arrmcq.optionsArray objectAtIndex:2];
    
    [arrmcq setValue:arrOption.MCQOPtionID forKey:@"AnswerID"];
    [arrmcq setValue:@"1" forKey:@"IsAttempt"];
    [arrmcq setValue:@"3" forKey:@"selectedAns"];
    
    [self changeSelection:sender.tag];
    [_collDispQuestions reloadData];
    /*
    [self updatetemp_mcq_test_dtl:sender.tag selectedOption:[[[[[arrResponce objectAtIndex:sender.tag] valueForKey:@"Question_option"] objectAtIndex:2] valueForKey:@"MCQOPtionID"] intValue]];
    [self updatemarrmcqquestion:(int)sender.tag :3];
    */
}

-(void)selectedAnswer4:(UIButton *)sender{
    
    MCQCCTModel *arrmcq=[_mcqCCTArray objectAtIndex:sender.tag];
    MCQCCTOptions *arrOption=[arrmcq.optionsArray objectAtIndex:3];
    
    [arrmcq setValue:arrOption.MCQOPtionID forKey:@"AnswerID"];//IsAttempt
    [arrmcq setValue:@"1" forKey:@"IsAttempt"];
    [arrmcq setValue:@"4" forKey:@"selectedAns"];
    
    [self changeSelection:sender.tag];
    [_collDispQuestions reloadData];
    
    /*
    [self updatetemp_mcq_test_dtl:sender.tag selectedOption:[[[[[arrResponce objectAtIndex:sender.tag] valueForKey:@"Question_option"] objectAtIndex:3] valueForKey:@"MCQOPtionID"] intValue]];
    [self updatemarrmcqquestion:(int)sender.tag :4];
    */
}
-(void)selectedAnswer5:(UIButton *)sender{
    
    //[self updatetemp_mcq_test_dtl:sender.tag selectedOption:[[[[ShareData.marrmcqoption objectAtIndex:sender.tag] objectAtIndex:3] valueForKey:@"MCQOPtionID"] intValue]];
    //[self updatemarrmcqquestion:(int)sender.tag :4];
    
    
    
    if ([[marrFlag objectAtIndex:sender.tag] intValue]==0) {
        [marrFlag replaceObjectAtIndex:sender.tag withObject:@"1"];
    }
    else
    {
        [marrFlag replaceObjectAtIndex:sender.tag withObject:@"0"];
    }
    
    
    NSLog(@"---%@",marrFlag);
    
    [_collDispQuestionNo reloadData];
    [_collDispQuestions reloadData];
    
    
    
}




#pragma mark - GET_SET DATA from DataBase Method

-(void)getData{
 
    [APP_DELEGATE showLoadingView:@""];
    
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    
    [AddPost setValue: [[NSUserDefaults standardUserDefaults] valueForKeyPath:@"ParseData.StudentID"] forKey:@"StudID"];//taken_test
    [AddPost setValue: _cctPaperID forKey:@"id"];
    
    NSLog(@"StudID %@",AddPost);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    //%@get_student_class_dtl?",BaseURLAPI
    
    [manager GET:[NSString stringWithFormat:@"%@mcq-view-paper?",ICAPIURL] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject){
        NSLog(@"JSON: %@", responseObject);
        
        
        
        if ([[responseObject valueForKey:@"status"] intValue]==1) {
            
            arrResponce=[responseObject valueForKeyPath:@"data.question_list"];
                        _mcqCCTArray= [[NSMutableArray<MCQCCTModel*> alloc] init];
            
            for(int i=0;i<[arrResponce count];i++){

                MCQCCTModel *mcqObj = [[MCQCCTModel alloc] init];
                mcqObj = [mcqObj parseObjectWithFMResultSet:[arrResponce objectAtIndex:i]];
                mcqObj.optionsArray = [[NSMutableArray<MCQCCTOptions*> alloc] init];
                
                NSMutableArray *optinonsArray =[[arrResponce objectAtIndex:i]
                                                 valueForKeyPath:@"Question_option"];
                for(int j=0;j<[optinonsArray count];j++){
                    MCQCCTOptions *optoinsObj = [[MCQCCTOptions alloc] init];
                    optoinsObj = [optoinsObj parseObjectsWithData:[optinonsArray objectAtIndex:j]];
                    [mcqObj.optionsArray addObject:optoinsObj];
                }
                
                [_mcqCCTArray addObject:mcqObj];
            }
          
            [[_mcqCCTArray objectAtIndex:0].optionsArray objectAtIndex:0];
            
            marrFlag=[[NSMutableArray alloc]init];
            
            for (int k=0; k<_mcqCCTArray.count; k++) {
                MCQCCTModel *arrtemp=[_mcqCCTArray objectAtIndex:k];
                [arrtemp setValue:@"0" forKey:@"selectedAns"];
                
                [marrFlag addObject:[NSString stringWithFormat:@"0"]];
                
            }
            
            _lbl_outoff.text=[NSString stringWithFormat:@"/%lu",(unsigned long)_mcqCCTArray.count];
            
            
        }
        else
        {
            UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"oops..!" message:@"No Questions Found ...!" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction * ok= [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
                
            }];
            
            
            [alert addAction:ok];
            
            [self presentViewController:alert animated:YES completion:nil];
            
        }
        
        [_collDispQuestions reloadData];
        [_collDispQuestionNo reloadData];
        
        [APP_DELEGATE hideLoadingView];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];
    }];
    
    
}

- (void)getmcqquestionNotApperedTableData{
    NSUserDefaults *standerId=[[NSUserDefaults alloc]init];
    _chapterList = [_chapterList stringByReplacingOccurrencesOfString:@"," withString:@"','"];
    
    FMResultSet *results = [MyModel selectQuery:[NSString stringWithFormat:@"select * FROM mcqquestion where  BoardID = '%@' AND MediumID = '%@' AND StandardID = '%@' AND SubjectID = '%@' AND QuestionLevelID IN (%@) AND ChapterID IN ('%@') and MCQQuestionID in(select QuestionID from student_not_appeared_question) ORDER BY Random() LIMIT 20",[standerId valueForKeyPath:@"ParseData.BoardID"],[standerId valueForKeyPath:@"ParseData.MediumID"],[standerId valueForKeyPath:@
                                                                                                                                                                                                                                                                                                                                                                                                                                                           "ParseData.StandardID"],_subjectId,_levelId,_chapterList ]];
    
    
    NSMutableArray * marr = [[NSMutableArray alloc]init];
    
    while ([results next])
    {
        NSMutableDictionary* mdict = [[NSMutableDictionary alloc] init];
        [mdict setValue:[results stringForColumn:@"MCQQuestionID"] forKey:@"MCQQuestionID"];
        [mdict setValue:[results stringForColumn:@"Question"] forKey:@"Question"];
        [mdict setValue:[results stringForColumn:@"ImageURL"] forKey:@"ImageURL"];
        [mdict setValue:[results stringForColumn:@"BoardID"] forKey:@"BoardID"];
        [mdict setValue:[results stringForColumn:@"MediumID"] forKey:@"MediumID"];
        [mdict setValue:[results stringForColumn:@"ClassID"] forKey:@"ClassID"];
        [mdict setValue:[results stringForColumn:@"StandardID"] forKey:@"StandardID"];
        [mdict setValue:[results stringForColumn:@"SubjectID"] forKey:@"SubjectID"];
        [mdict setValue:[results stringForColumn:@"ChapterID"] forKey:@"ChapterID"];
        [mdict setValue:[results stringForColumn:@"QuestionTypeID"] forKey:@"QuestionTypeID"];
        [mdict setValue:[results stringForColumn:@"QuestionLevelID"] forKey:@"QuestionLevelID"];
        [mdict setValue:[results stringForColumn:@"Duration"] forKey:@"Duration"];
        [mdict setValue:[results stringForColumn:@"CreatedBy"] forKey:@"CreatedBy"];
        [mdict setValue:[results stringForColumn:@"CreatedOn"] forKey:@"CreatedOn"];
        [mdict setValue:[results stringForColumn:@"ModifiedBy"] forKey:@"ModifiedBy"];
        [mdict setValue:[results stringForColumn:@"ModifiedOn"] forKey:@"ModifiedOn"];
        [mdict setValue:[NSString stringWithFormat:@"%d",false] forKey:@"isRight"];
        [mdict setValue:[NSString stringWithFormat:@"%s","0"] forKey:@"selectedAns"];
        [mdict setValue:[NSString stringWithFormat:@"%s","0"] forKey:@"isAttempted"];
        [marr addObject:mdict];
        
    }
    
    if (marr.count>0) {
        ShareData.marrmcqquestion = [NSMutableArray arrayWithArray:marr];
        NSLog(@"marrmcqquestion : %@" ,ShareData.marrmcqquestion);
    }
    else
    {
        [self dismissViewControllerAnimated:NO completion:nil];
        
    }
    
}

- (void)getmcqquestionInCorrectTableData{
    NSUserDefaults *standerId=[[NSUserDefaults alloc]init];
    _chapterList = [_chapterList stringByReplacingOccurrencesOfString:@"," withString:@"','"];
    
    FMResultSet *results = [MyModel selectQuery:[NSString stringWithFormat:@"select * FROM mcqquestion where  BoardID = '%@' AND MediumID = '%@' AND StandardID = '%@' AND SubjectID = '%@' AND QuestionLevelID IN (%@) AND ChapterID IN ('%@') and MCQQuestionID in(select QuestionID from student_incorrect_question) ORDER BY Random() LIMIT 20",[standerId valueForKeyPath:@"ParseData.BoardID"],[standerId valueForKeyPath:@"ParseData.MediumID"],[standerId valueForKeyPath:@
                                                                                                                                                                                                                                                                                                                                                                                                                                                        "ParseData.StandardID"],_subjectId,_levelId,_chapterList ]];
    
    
    
    NSMutableArray * marr = [[NSMutableArray alloc]init];
    
    while ([results next])
    {
        NSMutableDictionary* mdict = [[NSMutableDictionary alloc] init];
        
        [mdict setValue:[results stringForColumn:@"MCQQuestionID"] forKey:@"MCQQuestionID"];
        [mdict setValue:[results stringForColumn:@"Question"] forKey:@"Question"];
        [mdict setValue:[results stringForColumn:@"ImageURL"] forKey:@"ImageURL"];
        [mdict setValue:[results stringForColumn:@"BoardID"] forKey:@"BoardID"];
        [mdict setValue:[results stringForColumn:@"MediumID"] forKey:@"MediumID"];
        [mdict setValue:[results stringForColumn:@"ClassID"] forKey:@"ClassID"];
        [mdict setValue:[results stringForColumn:@"StandardID"] forKey:@"StandardID"];
        [mdict setValue:[results stringForColumn:@"SubjectID"] forKey:@"SubjectID"];
        [mdict setValue:[results stringForColumn:@"ChapterID"] forKey:@"ChapterID"];
        [mdict setValue:[results stringForColumn:@"QuestionTypeID"] forKey:@"QuestionTypeID"];
        [mdict setValue:[results stringForColumn:@"QuestionLevelID"] forKey:@"QuestionLevelID"];
        [mdict setValue:[results stringForColumn:@"Duration"] forKey:@"Duration"];
        [mdict setValue:[results stringForColumn:@"CreatedBy"] forKey:@"CreatedBy"];
        [mdict setValue:[results stringForColumn:@"CreatedOn"] forKey:@"CreatedOn"];
        [mdict setValue:[results stringForColumn:@"ModifiedBy"] forKey:@"ModifiedBy"];
        [mdict setValue:[results stringForColumn:@"ModifiedOn"] forKey:@"ModifiedOn"];
        [mdict setValue:[NSString stringWithFormat:@"%d",false] forKey:@"isRight"];
        [mdict setValue:[NSString stringWithFormat:@"%s","0"] forKey:@"selectedAns"];
        [mdict setValue:[NSString stringWithFormat:@"%s","0"] forKey:@"isAttempted"];
        
        [marr addObject:mdict];
        
    }
    
    if (marr.count>0) {
        ShareData.marrmcqquestion = [NSMutableArray arrayWithArray:marr];
        NSLog(@"marrmcqquestion : %@" ,ShareData.marrmcqquestion);
    }
    else
    {
        [self dismissViewControllerAnimated:NO completion:nil];
        
    }
    
}

- (void)getmcqquestionInCorrectAndNotApperedTableData{
    NSUserDefaults *standerId=[[NSUserDefaults alloc]init];
    _chapterList = [_chapterList stringByReplacingOccurrencesOfString:@"," withString:@"','"];
    
    FMResultSet *results = [MyModel selectQuery:[NSString stringWithFormat:@"select * FROM mcqquestion where  BoardID = '%@' AND MediumID = '%@' AND StandardID = '%@' AND SubjectID = '%@' AND QuestionLevelID IN (%@) AND ChapterID IN ('%@') and  ((MCQQuestionID in (select QuestionID from student_incorrect_question))OR (MCQQuestionID in (select QuestionID from student_not_appeared_question))) ORDER BY Random() LIMIT 20",[standerId valueForKeyPath:@"ParseData.BoardID"],[standerId valueForKeyPath:@"ParseData.MediumID"],[standerId valueForKeyPath:@
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          "ParseData.StandardID"],_subjectId,_levelId,_chapterList ]];
    
    
    
    NSMutableArray * marr = [[NSMutableArray alloc]init];
    
    while ([results next])
    {
        NSMutableDictionary* mdict = [[NSMutableDictionary alloc] init];
        
        [mdict setValue:[results stringForColumn:@"MCQQuestionID"] forKey:@"MCQQuestionID"];
        [mdict setValue:[results stringForColumn:@"Question"] forKey:@"Question"];
        [mdict setValue:[results stringForColumn:@"ImageURL"] forKey:@"ImageURL"];
        [mdict setValue:[results stringForColumn:@"BoardID"] forKey:@"BoardID"];
        [mdict setValue:[results stringForColumn:@"MediumID"] forKey:@"MediumID"];
        [mdict setValue:[results stringForColumn:@"ClassID"] forKey:@"ClassID"];
        [mdict setValue:[results stringForColumn:@"StandardID"] forKey:@"StandardID"];
        [mdict setValue:[results stringForColumn:@"SubjectID"] forKey:@"SubjectID"];
        [mdict setValue:[results stringForColumn:@"ChapterID"] forKey:@"ChapterID"];
        [mdict setValue:[results stringForColumn:@"QuestionTypeID"] forKey:@"QuestionTypeID"];
        [mdict setValue:[results stringForColumn:@"QuestionLevelID"] forKey:@"QuestionLevelID"];
        [mdict setValue:[results stringForColumn:@"Duration"] forKey:@"Duration"];
        [mdict setValue:[results stringForColumn:@"CreatedBy"] forKey:@"CreatedBy"];
        [mdict setValue:[results stringForColumn:@"CreatedOn"] forKey:@"CreatedOn"];
        [mdict setValue:[results stringForColumn:@"ModifiedBy"] forKey:@"ModifiedBy"];
        [mdict setValue:[results stringForColumn:@"ModifiedOn"] forKey:@"ModifiedOn"];
        [mdict setValue:[NSString stringWithFormat:@"%d",false] forKey:@"isRight"];
        [mdict setValue:[NSString stringWithFormat:@"%s","0"] forKey:@"selectedAns"];
        [mdict setValue:[NSString stringWithFormat:@"%s","0"] forKey:@"isAttempted"];
        
        [marr addObject:mdict];
        
    }
    
    if (marr.count>0) {
        ShareData.marrmcqquestion = [NSMutableArray arrayWithArray:marr];
        NSLog(@"marrmcqquestion : %@" ,ShareData.marrmcqquestion);
    }
    else
    {
        [self dismissViewControllerAnimated:NO completion:nil];
        
    }
    
}



- (void)getmcqquestionTableData{
    NSUserDefaults *standerId=[[NSUserDefaults alloc]init];
    _chapterList = [_chapterList stringByReplacingOccurrencesOfString:@"," withString:@"','"];
    
    FMResultSet *results = [MyModel selectQuery:[NSString stringWithFormat:@"select * FROM mcqquestion where  BoardID = '%@' AND MediumID = '%@' AND StandardID = '%@' AND SubjectID = '%@' AND QuestionLevelID IN (%@) AND ChapterID IN ('%@') ORDER BY Random() LIMIT 20",[standerId valueForKeyPath:@"ParseData.BoardID"],[standerId valueForKeyPath:@"ParseData.MediumID"],[standerId valueForKeyPath:@
                                                                                                                                                                                                                                                                                                                                                                                "ParseData.StandardID"],_subjectId,_levelId,_chapterList ]];
    
    
    
    NSMutableArray * marr = [[NSMutableArray alloc]init];
    
    while ([results next])
    {
        NSMutableDictionary* mdict = [[NSMutableDictionary alloc] init];
        
        [mdict setValue:[results stringForColumn:@"MCQQuestionID"] forKey:@"MCQQuestionID"];
        [mdict setValue:[results stringForColumn:@"Question"] forKey:@"Question"];
        [mdict setValue:[results stringForColumn:@"ImageURL"] forKey:@"ImageURL"];
        [mdict setValue:[results stringForColumn:@"BoardID"] forKey:@"BoardID"];
        [mdict setValue:[results stringForColumn:@"MediumID"] forKey:@"MediumID"];
        [mdict setValue:[results stringForColumn:@"ClassID"] forKey:@"ClassID"];
        [mdict setValue:[results stringForColumn:@"StandardID"] forKey:@"StandardID"];
        [mdict setValue:[results stringForColumn:@"SubjectID"] forKey:@"SubjectID"];
        [mdict setValue:[results stringForColumn:@"ChapterID"] forKey:@"ChapterID"];
        [mdict setValue:[results stringForColumn:@"QuestionTypeID"] forKey:@"QuestionTypeID"];
        [mdict setValue:[results stringForColumn:@"QuestionLevelID"] forKey:@"QuestionLevelID"];
        [mdict setValue:[results stringForColumn:@"Duration"] forKey:@"Duration"];
        [mdict setValue:[results stringForColumn:@"CreatedBy"] forKey:@"CreatedBy"];
        [mdict setValue:[results stringForColumn:@"CreatedOn"] forKey:@"CreatedOn"];
        [mdict setValue:[results stringForColumn:@"ModifiedBy"] forKey:@"ModifiedBy"];
        [mdict setValue:[results stringForColumn:@"ModifiedOn"] forKey:@"ModifiedOn"];
        [mdict setValue:[NSString stringWithFormat:@"%d",false] forKey:@"isRight"];
        [mdict setValue:[NSString stringWithFormat:@"%s","0"] forKey:@"selectedAns"];
        [mdict setValue:[NSString stringWithFormat:@"%s","0"] forKey:@"isAttempted"];
        
        [marr addObject:mdict];
        
    }
    
    if (marr.count>0) {
        ShareData.marrmcqquestion = [NSMutableArray arrayWithArray:marr];
        NSLog(@"marrmcqquestion : %@" ,ShareData.marrmcqquestion);
    }
    else
    {
        [self dismissViewControllerAnimated:NO completion:nil];
        
    }
    
}

-(void)updatetemp_mcq_test_dtl:(NSInteger )index selectedOption:(int)answerID{
    
    
    
    [MyModel UpdateQuery:[NSString stringWithFormat:@"UPDATE temp_mcq_test_dtl SET AnswerID = '%d' , IsAttempt = '%d' WHERE QuestionID = '%@'",answerID,1,[[ShareData.marrmcqquestion objectAtIndex:index] valueForKey:@"MCQQuestionID"]]];
    
    //selectedAns
    [self changeSelection:index];
    [_collDispQuestions reloadData];
    
}

-(void)insertIntostudent_mcq_test_hdrTable{
    
    int lastID = 0;
    FMResultSet * result = [MyModel selectQuery:[NSString stringWithFormat:@"SELECT  StudentMCQTestHDRID FROM student_mcq_test_hdr  ORDER BY StudentMCQTestHDRID DESC Limit 1"]];
    
    while ([result next]) {
        lastID = [[result stringForColumn:@"StudentMCQTestHDRID"] intValue];
        NSLog(@"lastid %d",lastID);
    }
    NSLog(@"lastid %d",lastID);
    
    NSUserDefaults *stander=[[NSUserDefaults alloc]init];
    NSMutableDictionary *mutDic=[[NSMutableDictionary alloc] init];
    [mutDic setValue:[NSString stringWithFormat:@"%d",++lastID] forKey:@"StudentMCQTestHDRID"];
    [mutDic setValue:[stander valueForKeyPath:@"ParseData.StudentID"] forKey:@"StudentID"];
    [mutDic setValue:[stander valueForKeyPath:@"ParseData.BoardID"] forKey:@"BoardID"];
    [mutDic setValue:[stander valueForKeyPath:@"ParseData.MediumID"] forKey:@"MediumID"];
    [mutDic setValue:[NSString stringWithFormat:@"%@",_subjectId] forKey:@"SubjectID"];
    [mutDic setValue:[NSString stringWithFormat:@"%@",_levelId] forKey:@"LevelID"];
    [mutDic setValue:@"" forKey:@"Timeleft"];
    [mutDic setValue:@"" forKey:@"CreatedBy"];
    //[mutDic setValue:[MyModel getCuruntDateTime] forKey:@"CreatedOn"];
    [mutDic setValue:[APP_DELEGATE getCurrentDate] forKey:@"CreatedOn"];//
    [mutDic setValue:@"" forKey:@"ModifiedBy"];
    [mutDic setValue:@"" forKey:@"ModifiedOn"];
    
    [MyModel insertInto_papertype:mutDic :@"student_mcq_test_hdr"];
    
    
    
}


-(void)gettemp_mcq_test_dtl{
    
    FMResultSet *results = [MyModel selectQuery:[NSString stringWithFormat:@"select * FROM temp_mcq_test_dtl ORDER BY TempMCQTestDTLID ASC"]];
    
    NSMutableArray* marr = [[NSMutableArray alloc]init];
    
    while ([results next])
    {
        NSMutableDictionary* mdict = [[NSMutableDictionary alloc] init];
        
        [mdict setValue:[results stringForColumn:@"QuestionID"] forKey:@"QuestionID"];
        [mdict setValue:[results stringForColumn:@"AnswerID"] forKey:@"AnswerID"];
        [mdict setValue:[results stringForColumn:@"IsAttempt"] forKey:@"IsAttempt"];
        
        [marr addObject:mdict];
        
    }
    ShareData.marrtemp_mcq_test_dtl = [NSMutableArray arrayWithArray:marr];
    NSLog(@"marrtemp_mcq_test_dtl : %@" ,ShareData.marrtemp_mcq_test_dtl);
    
}

-(void)insertIntostudent_mcq_test_dtlTable{
    
    lastHeaderIDcct = -1;
    FMResultSet * result = [MyModel selectQuery:[NSString stringWithFormat:@"SELECT  StudentMCQTestHDRID FROM student_mcq_test_hdr  ORDER BY StudentMCQTestHDRID DESC Limit 1"]];
    
    
    while ([result next]) {
        
        lastHeaderIDcct = [[result stringForColumn:@"StudentMCQTestHDRID"] intValue];
        NSLog(@"lastHeaderIDcct %d",lastHeaderIDcct);
    }
    
    for (int i = 0; i < ShareData.marrtemp_mcq_test_dtl.count; i++) {
        
        NSMutableDictionary *mutDic=[[NSMutableDictionary alloc] initWithCapacity:2];
        [mutDic setValue:[NSString stringWithFormat:@"%d",lastHeaderIDcct] forKey:@"StudentMCQTestHDRID"];
        [mutDic setValue:[[ShareData.marrtemp_mcq_test_dtl objectAtIndex:i] valueForKey:@"QuestionID"] forKey:@"QuestionID"];
        [mutDic setValue:[[ShareData.marrtemp_mcq_test_dtl objectAtIndex:i] valueForKey:@"AnswerID"] forKey:@"AnswerID"];
        [mutDic setValue:[[ShareData.marrtemp_mcq_test_dtl objectAtIndex:i] valueForKey:@"IsAttempt"] forKey:@"IsAttempt"];
        [mutDic setValue:@"" forKey:@"srNo"];
        [mutDic setValue:@"" forKey:@"CreatedBy"];
        [mutDic setValue:[MyModel getCuruntDateTime] forKey:@"CreatedOn"];
        [mutDic setValue:@"" forKey:@"ModifiedBy"];
        [mutDic setValue:@"" forKey:@"ModifiedOn"];
        
        [MyModel insertInto_papertype:mutDic :@"student_mcq_test_dtl"];
        
    }
    
}

-(void)gettemp_mcq_test_chapter{
    
    FMResultSet *results = [MyModel selectQuery:[NSString stringWithFormat:@"select * FROM temp_mcq_test_chapter"]];
    
    NSMutableArray* marr = [[NSMutableArray alloc]init];
    
    while ([results next])
    {
        NSMutableDictionary* mdict = [[NSMutableDictionary alloc] init];
        
        [mdict setValue:[results stringForColumn:@"TempMCQTestHDRID"] forKey:@"TempMCQTestHDRID"];
        [mdict setValue:[results stringForColumn:@"ChapterID"] forKey:@"ChapterID"];
        
        [marr addObject:mdict];
        
    }
    
    ShareData.marrtemp_mcq_test_chapter = [NSMutableArray arrayWithArray:marr];
    
    NSLog(@"marrtemp_mcq_test_chapter : %@" ,ShareData.marrtemp_mcq_test_chapter);
    
    
    
}

-(void)insertIntostudent_mcq_test_chapterTable{
    
    /*   NSLog(@"lastHeaderIDcct %d",lastHeaderIDcct);
     for (int i = 0; i < ShareData.marrtemp_mcq_test_chapter.count; i++) {
     NSMutableDictionary *mutDic=[[NSMutableDictionary alloc] initWithCapacity:2];
     [mutDic setValue:[NSString stringWithFormat:@"%d",lastHeaderIDcct] forKey:@"StudentMCQTestHDRID"];
     [mutDic setValue:[[ShareData.marrtemp_mcq_test_chapter objectAtIndex:i] valueForKey:@"ChapterID"] forKey:@"ChapterID"];
     [MyModel insertInto_papertype:mutDic :@"student_mcq_test_chapter"];
     
     }
     */
    
    
    NSLog(@"lastHeaderIDcct %d",lastHeaderIDcct);
    for (int i = 0; i < arrChapter.count; i++) {
        NSMutableDictionary *mutDic=[[NSMutableDictionary alloc] init];
        [mutDic setValue:[NSString stringWithFormat:@"%d",lastHeaderIDcct] forKey:@"StudentMCQTestHDRID"];
        [mutDic setValue:[arrChapter objectAtIndex:i] forKey:@"ChapterID"];
        [MyModel insertInto_papertype:mutDic :@"student_mcq_test_chapter"];
        
    }
    
}

-(void)insertIntostudent_mcq_test_levelTable{
    
    NSMutableArray *arrlevels=[[NSMutableArray alloc]init];
    arrlevels = [_levelId componentsSeparatedByString:@","];
    
    
    NSLog(@"lastHeaderIDcct %d",lastHeaderIDcct);
    for (int i = 0; i < arrlevels.count; i++) {
        NSMutableDictionary *mutDic=[[NSMutableDictionary alloc] initWithCapacity:2];
        [mutDic setValue:[NSString stringWithFormat:@"%d",lastHeaderIDcct] forKey:@"StudentMCQTestHDRID"];
        [mutDic setValue:[arrlevels objectAtIndex:i] forKey:@"LevelID"];
        [MyModel insertInto_papertype:mutDic :@"student_mcq_test_level"];
        
    }
    
}


-(void)updatemarrmcqquestion:(int )index :(int)selectedOption{
    
    
    //NSLog(@"isCorrect : %@",[[[ShareData.marrmcqoption objectAtIndex:index] objectAtIndex:selectedOption-1] valueForKey:@"isCorrect"]);
    if ([[[[ShareData.marrmcqoption objectAtIndex:index] objectAtIndex:selectedOption-1] valueForKey:@"isCorrect"] isEqual:@"1"]) {
        
        [[ShareData.marrmcqquestion objectAtIndex:index]  setValue:[NSString stringWithFormat:@"%d",true] forKey:@"isRight"];
        
    }else{
        
        [[ShareData.marrmcqquestion objectAtIndex:index]  setValue:[NSString stringWithFormat:@"%d",false] forKey:@"isRight"];
        
    }
    
    [[ShareData.marrmcqquestion objectAtIndex:index]  setValue:[NSString stringWithFormat:@"%d",selectedOption] forKey:@"selectedAns"];
    [[ShareData.marrmcqquestion objectAtIndex:index]  setValue:[NSString stringWithFormat:@"%d",1] forKey:@"isAttempted"];
    
    
    [self changeSelection:index];
    
    [totAttempt replaceObjectAtIndex:index withObject:@"1"];
    int j=0;
    for (int i=0; i<totAttempt.count; i++) {
        if ([[totAttempt objectAtIndex:i] intValue]==1) {
            j++;
        }
    }
    _lbl_attemp.text=[NSString stringWithFormat:@"%02d",j];
    
    NSLog(@"marrmcqquestion :%@ ",[ShareData.marrmcqquestion objectAtIndex:index]);
    
}

-(void)insertIntonotificationTable{
    
    NSDate * now = [NSDate date];
    
    NSDateFormatter *DateFormator = [[NSDateFormatter alloc] init];
    [DateFormator setDateFormat:@"dd-MM-yyyy HH:mm:ss"];
    NSString *strdate = [DateFormator stringFromDate:now];
    
    NSArray *components = [strdate componentsSeparatedByString:@" "];
    NSString *date = components[0];
    NSString *time = components[1];
    
    NSLog(@"time %@", time);
    NSLog(@"date %@", date);
    
    [MyModel UpdateQuery:[NSString stringWithFormat:@"Insert into notification(StudentKey,Title,Message,CreatedOn) values ('%@','%@','%@','%@')",[UserDefault valueForKey:@"reg_key"],@"Your Child activity on iScore.. !",[NSString stringWithFormat:@"Your Child attemptedcct a MCQ test.  Subject: %@ AT: Time (%@) / Date (%@)",_subjectName,time,date],strdate]];
    
}


-(void)getnotificationTableData{
    
    FMResultSet *results = [MyModel selectQuery:[NSString stringWithFormat:@"select * FROM notification"]];
    
    NSMutableArray* marr = [[NSMutableArray alloc]init];
    
    while ([results next])
    {
        NSMutableDictionary* mdict = [[NSMutableDictionary alloc] init];
        
        [mdict setValue:[results stringForColumn:@"ID"] forKey:@"id"];
        [mdict setValue:[results stringForColumn:@"StudentKey"] forKey:@"StudentKey"];
        [mdict setValue:[results stringForColumn:@"Title"] forKey:@"Title"];
        [mdict setValue:[results stringForColumn:@"Message"] forKey:@"Message"];
        [mdict setValue:[results stringForColumn:@"CreatedOn"] forKey:@"CreatedOn"];
        
        [marr addObject:mdict];
        
    }
    
    ShareData.marrnotification = [NSMutableArray arrayWithArray:marr];
    
    NSLog(@"marrnotification : %@" ,ShareData.marrnotification);
    
    NSData *jsonData2 = [NSJSONSerialization dataWithJSONObject:ShareData.marrnotification options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData2 encoding:NSUTF8StringEncoding];
    NSLog(@"jsonData as string:%@", jsonString);
    
    NSString * url = [NSString stringWithFormat:@"%@/web-services/notification?",Main_URL];
    
    NSMutableDictionary * param = [[NSMutableDictionary alloc]init];
    
    [param setObject:jsonString forKey:@"data"];
    [param setObject:@"" forKey:@"show"];
    
    [API PostUrl:url Parameters:param andCallback:^(NSMutableDictionary *result, NSError *error)  {
        
        if (error==nil) {
            
            NSLog(@"Response:-%@",result);
            
            if ([result valueForKey:@"status"]) {
                NSString * ID = [result valueForKey:@"data"];
                NSLog(@"ID : %@",ID);
                
                [MyModel UpdateQuery:[NSString stringWithFormat:@"DELETE FROM notification where id IN(%@)",ID]];
                
            }
            
        }else{
            
            NSLog(@"Error : %@ ",error);
            
        }
    }];
    
}

-(void)getstudent_mcq_test_hdrTableData:(NSString *)url{
    
    FMResultSet *results = [MyModel selectQuery:url];
    
    NSMutableArray* marr = [[NSMutableArray alloc]init];
    
    
    while ([results next])
    {
        NSMutableDictionary* mdict = [[NSMutableDictionary alloc] init];
        
        [mdict setValue:[results stringForColumn:@"StudentMCQTestHDRID"] forKey:@"StudentMCQTestHDRID"];
        [mdict setValue:[results stringForColumn:@"StudentID"] forKey:@"StudentID"];
        [mdict setValue:[results stringForColumn:@"BoardID"] forKey:@"BoardID"];
        [mdict setValue:[results stringForColumn:@"MediumID"] forKey:@"MediumID"];
        [mdict setValue:[results stringForColumn:@"SubjectID"] forKey:@"SubjectID"];
        [mdict setValue:[results stringForColumn:@"LevelID"] forKey:@"LevelID"];
        [mdict setValue:[results stringForColumn:@"Timeleft"] forKey:@"Timeleft"];
        [mdict setValue:[results stringForColumn:@"AddType"] forKey:@"AddType"];
        [mdict setValue:[results stringForColumn:@"CreatedBy"] forKey:@"CreatedBy"];
        [mdict setValue:[results stringForColumn:@"CreatedOn"] forKey:@"CreatedOn"];
        [mdict setValue:[results stringForColumn:@"ModifiedBy"] forKey:@"ModifiedBy"];
        [mdict setValue:[results stringForColumn:@"ModifiedOn"] forKey:@"ModifiedOn"];
        [mdict setObject:@"" forKey:@"chapterArray"];
        [mdict setObject:@"" forKey:@"detailArray"];
        
        [marr addObject:mdict];
        
    }
    
    ShareData.marrstudent_mcq_test_hdr = [NSMutableArray arrayWithArray:marr];
    
    NSLog(@"marrstudent_mcq_test_hdr : %@" ,ShareData.marrstudent_mcq_test_hdr);
    
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch(buttonIndex) {
        case 0:
        {
            /*TestReportVC *viewController=[[TestReportVC alloc]initWithNibName:@"TestReportVC" bundle:nil];
             [self.navigationController pushViewController:viewController animated:YES];
             */
            
            [self btnSubmit];
            
            NSLog(@"lbl_count %@",_lbl_countdown);
            
            TestReportVC *add = [[TestReportVC alloc] initWithNibName:@"TestReportVC" bundle:nil];
            add.tempNotapper=tempNotapper;
            add.tempIncorrect=tempIncorrect;
            add.tempCorrect=tempCorrect;
            add.tempNotapperOp=tempNotapperOp;
            add.tempIncorrectOp=tempIncorrectOp;
            add.tempCorrectOp=tempCorrectOp;
            
            if ([_TBStatus intValue]==1)
            {
                add.TakanTime=lbltotTaken.text;
            }
            else
            {
                add.TakanTime=@"00:00";
            }
            
            
            add.chapterList=_chapterList;
            add.TBStatus=_TBStatus;
            add.subjectId=_subjectId;
            add.subjectName=_subjectName;
            add.levelId=_levelId;
            add.mcqQuestions=ShareData.marrmcqquestion;
            add.mcqOption=ShareData.marrmcqoption;
            //[self presentViewController:add animated:YES completion:nil];
            [self.navigationController pushViewController:add animated:YES];
            
            break;
        }
        case 1:
            break;
    }
}


- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}



- (IBAction)btn_SUBMIT:(id)sender {
    
    [self CallSendTestToServer];
    
    /*
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                    message:@"Do you want to submit the test?"
                                                   delegate:self
                                          cancelButtonTitle:@"YES"
                                          otherButtonTitles:@"NO", nil];
    [alert show];
     */
}

- (IBAction)btn_BACK_A:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btn_HOME_A:(id)sender {
    
    [timercct pause];
    [totTakencct pause];
    
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"Alert"
                                 message:@"Are you sure you want to visit Deshboard?"
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    //Add Buttons
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"YES"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
                                    //Handle your yes please button action here
                                    [self TruncateTempTeble];
                                    
                                    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                                             bundle: nil];
                                    HomeVC *view = [mainStoryboard instantiateViewControllerWithIdentifier:@"HomeVC"];
                                    [self.navigationController pushViewController:view animated:YES];
                                    
                                    
                                }];
    
    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle:@"Cancel"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   //Handle no, thanks button
                                   [timercct start];
                                   [totTakencct start];
                               }];
    
    //Add your buttons to alert controller
    
    [alert addAction:yesButton];
    [alert addAction:noButton];
    
    [self presentViewController:alert animated:YES completion:nil];
    
}

-(void)TruncateTempTeble
{
    [MyModel UpdateQuery:[NSString stringWithFormat:@"DELETE FROM temp_mcq_test_chapter"]];
    [MyModel UpdateQuery:[NSString stringWithFormat:@"DELETE FROM temp_mcq_test_dtl"]];
    [MyModel UpdateQuery:[NSString stringWithFormat:@"DELETE FROM temp_mcq_test_hdr"]];
    [MyModel UpdateQuery:[NSString stringWithFormat:@"DELETE FROM temp_mcq_test_level"]];
}

@end
