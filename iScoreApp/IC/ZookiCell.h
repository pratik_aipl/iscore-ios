//
//  ZookiCell.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 9/14/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZookiCell : UICollectionViewCell



@property (weak, nonatomic) IBOutlet UIImageView *img_zooki;
@property (weak, nonatomic) IBOutlet UILabel *lbl_title;

@property (weak, nonatomic) IBOutlet UIView *vw_zookicell;
@property (weak, nonatomic) IBOutlet UIButton *btn_viewmore;

@end
