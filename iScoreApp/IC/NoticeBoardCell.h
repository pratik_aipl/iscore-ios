//
//  NoticeBoardCell.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 9/11/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NoticeBoardCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UIView *vw_shadow;
@property (weak, nonatomic) IBOutlet UILabel *lbl_title;
@property (weak, nonatomic) IBOutlet UILabel *lbl_desc;
@property (weak, nonatomic) IBOutlet UILabel *lbl_datetime;





@end
