//
//  NoticeBoard.m
//  iScoreApp
//
//  Created by ADMIN-Khushal on 9/11/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "NoticeBoard.h"
#import "NoticeBoardCell.h"

#import "ApplicationConst.h"
#import <AFNetworking/AFNetworking.h>





@interface NoticeBoard ()

@end

@implementation NoticeBoard

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [_tbl_notise registerNib:[UINib nibWithNibName:@"NoticeBoardCell" bundle:nil] forCellReuseIdentifier:@"Cell"];
    
    [self CallNotice];
}

-(void)CallNotice
{
    [APP_DELEGATE showLoadingView:@""];
    
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    
    [AddPost setValue: [[NSUserDefaults standardUserDefaults] valueForKeyPath:@"ParseData.StudentID"] forKey:@"StudID"];//taken_test
   
    
    NSLog(@"StudID %@",AddPost);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    //%@get_student_class_dtl?",BaseURLAPI
    
    [manager GET:[NSString stringWithFormat:@"%@get_notice_board?",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject){
        NSLog(@"JSON: %@", responseObject);
        
        
        
        if ([[responseObject valueForKey:@"status"] intValue]==1) {
            
            Dictdata=[responseObject valueForKey:@"data"];
            
            
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                            message:@"No Data Found"
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }
        
        [_tbl_notise reloadData];
        
        [APP_DELEGATE hideLoadingView];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                        message:@"Can not load data."
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        
        
    }];
    
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return Dictdata.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return CellHeight;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NoticeBoardCell *cell = (NoticeBoardCell *)[_tbl_notise dequeueReusableCellWithIdentifier:@"Cell"];
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"NoticeBoardCell" owner:self options:nil];
    if(cell == nil){
        cell = nib[0];
    }
    
    
    //-----TODATE---------
    NSString * yourJSONString = [Dictdata valueForKey:@"CreatedOn"][indexPath.row];//EndDate
    NSDateFormatter *currentDTFormatter = [[NSDateFormatter alloc] init];;
    [currentDTFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *dateFromString = [currentDTFormatter dateFromString:yourJSONString];
    
    [currentDTFormatter setDateFormat:@"MMM"];
    NSString *monthnm = [currentDTFormatter stringFromDate:dateFromString];
    
    [currentDTFormatter setDateFormat:@"dd"];
    NSString *onlydt = [currentDTFormatter stringFromDate:dateFromString];
    
    [currentDTFormatter setDateFormat:@"yyyy"];
    NSString *onlyear = [currentDTFormatter stringFromDate:dateFromString];
    
    [currentDTFormatter setDateFormat:@"hh:mm a"];
    NSString *time = [currentDTFormatter stringFromDate:dateFromString];
    
    NSLog(@"%@ %@,%@ %@",monthnm,onlydt,onlyear,time);
    
    
    cell.lbl_datetime.text=[NSString stringWithFormat:@"%@ %@, %@ %@",monthnm,onlydt,onlyear,time];
    
    cell.lbl_title.text=[NSString stringWithFormat:@"%@",[Dictdata valueForKey:@"Title"][indexPath.row]];
    
    cell.lbl_desc.text=[NSString stringWithFormat:@"%@",[Dictdata valueForKey:@"Description"][indexPath.row]];

    
    CGRect autosizeprof = cell.lbl_desc.frame;
    autosizeprof.size.height = [self heightForText:cell.lbl_desc.text];
    cell.lbl_desc.frame = autosizeprof;

    CellHeight=cell.lbl_desc.frame.size.height+50;
    
    [cell.lbl_desc setNumberOfLines:0];
    [cell.lbl_desc sizeToFit];
    
    
    
    return cell;
}

-(CGFloat)heightForText:(NSString *)text
{
    NSInteger MAX_HEIGHT = 2000;
    UITextView * textView = [[UITextView alloc] initWithFrame: CGRectMake(0, 0, self.view.frame.size.width-80, MAX_HEIGHT+150)];
    textView.text = text;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        textView.font = [UIFont fontWithName:@"Roboto-Light" size:12.0];
    }
    else
    {
        textView.font = [UIFont fontWithName:@"Roboto-Light" size:12.0];
    }
    
    [textView sizeToFit];
    
    return textView.frame.size.height;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




- (IBAction)btn_BACK:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
