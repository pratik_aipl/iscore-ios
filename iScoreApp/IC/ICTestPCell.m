//
//  ICTestPCell.m
//  iScoreApp
//
//  Created by ADMIN-Khushal on 9/10/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "ICTestPCell.h"

@implementation ICTestPCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setNeedsLayout
{
    [self setShadow:_vw_shadow];
    [self setShadow:_cw_shadow2];
    
    //_vw_dt.layer.cornerRadius=5;
    
    [self topCorner];
    [self bottomCorner];
    
    [self topCorner2];
    [self bottomCorner2];
}

-(void)topCorner
{
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:_lbl_month.bounds
                                                   byRoundingCorners:(UIRectCornerTopRight | UIRectCornerTopLeft)
                                                         cornerRadii:CGSizeMake(5.0, 5.0)];
    
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = _lbl_month.bounds;
    maskLayer.path = maskPath.CGPath;
    _lbl_month.layer.mask = maskLayer;
    _lbl_month.layer.masksToBounds = YES;
}
-(void)bottomCorner
{
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:_lbl_dt.bounds
                                                   byRoundingCorners:(UIRectCornerBottomLeft | UIRectCornerBottomRight)
                                                         cornerRadii:CGSizeMake(5.0, 5.0)];
    
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = _lbl_dt.bounds;
    maskLayer.path = maskPath.CGPath;
    _lbl_dt.layer.mask = maskLayer;
    _lbl_dt.layer.masksToBounds = YES;
    
    
    _lbl_dt.layer.borderWidth=1;
    
}


-(void)topCorner2
{
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:_lbl_month2.bounds
                                                   byRoundingCorners:(UIRectCornerTopRight | UIRectCornerTopLeft)
                                                         cornerRadii:CGSizeMake(5.0, 5.0)];
    
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = _lbl_month2.bounds;
    maskLayer.path = maskPath.CGPath;
    _lbl_month2.layer.mask = maskLayer;
    _lbl_month2.layer.masksToBounds = YES;
}
-(void)bottomCorner2
{
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:_lbl_dt2.bounds
                                                   byRoundingCorners:(UIRectCornerBottomLeft | UIRectCornerBottomRight)
                                                         cornerRadii:CGSizeMake(5.0, 5.0)];
    
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = _lbl_dt2.bounds;
    maskLayer.path = maskPath.CGPath;
    _lbl_dt2.layer.mask = maskLayer;
    _lbl_dt2.layer.masksToBounds = YES;
    
    
    _lbl_dt2.layer.borderWidth=1;
    
}




-(void)setShadow :(UIView*)vview
{
    vview.layer.shadowRadius  = 1.5f;
    vview.layer.shadowColor   = [UIColor colorWithRed:176.f/255.f green:199.f/255.f blue:226.f/255.f alpha:1.f].CGColor;
    vview.layer.shadowOffset  = CGSizeMake(0.0f, 0.0f);
    vview.layer.shadowOpacity = 0.9f;
    vview.layer.masksToBounds = NO;
    
    UIEdgeInsets shadowInsets     = UIEdgeInsetsMake(0, 0, -1.5f, 0);
    UIBezierPath *shadowPath      = [UIBezierPath bezierPathWithRect:UIEdgeInsetsInsetRect(vview.bounds, shadowInsets)];
    vview.layer.shadowPath    = shadowPath.CGPath;
    
    
    /*
     //Drop Shadow
     [view.layer setShadowColor: [UIColor grayColor].CGColor];
     [view.layer setShadowOpacity:0.8];
     [view.layer setShadowRadius:3.0];
     [view.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
     */
    
    
}



@end
