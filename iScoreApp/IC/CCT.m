//
//  CCT.m
//  iScoreApp
//
//  Created by ADMIN-Khushal on 9/8/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "CCT.h"
#import "CCTCell.h"
#import "ApplicationConst.h"
#import <AFNetworking/AFNetworking.h>

#import "ICSearchPaper.h"
#import "MCQCCT.h"





@interface CCT ()

@end

@implementation CCT

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [_tbl_cct registerNib:[UINib nibWithNibName:@"CCTCell" bundle:nil] forCellReuseIdentifier:@"Cell"];

    [self CallClassCCT];
    //[self CallTemp];
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(void)CallTemp
{
    //NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    //AddPost=[[NSUserDefaults standardUserDefaults] valueForKey:@"TempPost"];
    
    
    
    
    NSDictionary *foo = @{
                          @"DetailID" : @"2011011",
                          @"SelectedAnswerID" : @"2212",
                          @"isAttempt" : @"1"
                          };
    
    
    NSArray *arr = [NSArray arrayWithObjects:foo, nil];
    
    
    // convert your object to JSOn String
    NSError *error = nil;
    NSString *createJSON = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:arr
                                                                                          options:NSJSONWritingPrettyPrinted
                                                                                            error:&error]
                                                 encoding:NSUTF8StringEncoding];
    
    
    //NSDictionary *pardsams = @{@"data": createJSON};
    
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    [AddPost setValue: [[NSUserDefaults standardUserDefaults] valueForKeyPath:@"ParseData.StudentID"] forKey:@"StudID"];//taken_test
    [AddPost setValue: @"09" forKey:@"total_attempt"];
    [AddPost setValue: @"15" forKey:@"total_question"];
    [AddPost setValue: @"1" forKey:@"total_right"];
    [AddPost setValue: createJSON forKey:@"detail_array"];
    [AddPost setValue: @"40953" forKey:@"start_time"];
    [AddPost setValue: @"5384" forKey:@"paper_id"];//paper_id
    
    
    
    
   
    
  /*  NSData *jsonData2 = [NSJSONSerialization dataWithJSONObject:[AddPost valueForKey:@"detail_array"] options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData2 encoding:NSUTF8StringEncoding];
    NSLog(@"jsonData as string:\n%@", jsonString);
    
    [AddPost setValue:jsonString forKeyPath:@"TempPost.detail_array"];
    
    NSLog(@"==%@",AddPost);
   */
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html",@"application/json",nil];
    //  manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html",@"application/json",nil];
    //%@get_student_class_dtl?",BaseURLAPI
    
    [manager POST:[NSString stringWithFormat:@"%@submit_mcq_test",ICAPIURL]  parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject){
        
        NSLog(@"JSON: %@", responseObject);
        
        if ([[responseObject valueForKey:@"status"] boolValue]) {
            
            
            UIAlertController * alert = [UIAlertController
                                         alertControllerWithTitle:@"DONE"
                                         message:@"Your Test Submited!"
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            //Add Buttons
            
            UIAlertAction* yesButton = [UIAlertAction
                                        actionWithTitle:@"OK"
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * action) {
                                            //Handle your yes please button action here
                                            [APP_DELEGATE hideLoadingView];
                                            [self.navigationController popViewControllerAnimated:YES];
                                        }];
            
            
            //Add your buttons to alert controller
            
            [alert addAction:yesButton];
            
            [self presentViewController:alert animated:YES completion:nil];
            
            
        }
        else
        {
            UIAlertController * alert = [UIAlertController
                                         alertControllerWithTitle:@"DONE"
                                         message:@"Somthing missing"
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            //Add Buttons
            
            UIAlertAction* yesButton = [UIAlertAction
                                        actionWithTitle:@"OK"
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * action) {
                                            //Handle your yes please button action here
                                        }];
            
            //Add your buttons to alert controller
            
            [alert addAction:yesButton];
            [self presentViewController:alert animated:YES completion:nil];
            
        }
        
        [APP_DELEGATE hideLoadingView];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error){
        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];
    }];
}
















-(void)CallClassCCT
{
    [APP_DELEGATE showLoadingView:@""];
    
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    
    [AddPost setValue: [[NSUserDefaults standardUserDefaults] valueForKeyPath:@"ParseData.StudentID"] forKey:@"StudID"];//taken_test
    [AddPost setValue: @"0" forKey:@"taken_test"];
    
    NSLog(@"StudID %@",AddPost);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    //%@get_student_class_dtl?",BaseURLAPI
    
    [manager GET:[NSString stringWithFormat:@"%@get-recent-mcq-paper?",ICAPIURL] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject){
        NSLog(@"JSON: %@", responseObject);
        
        
        
        if ([[responseObject valueForKey:@"status"] intValue]==1) {
            
            Dictdata=[responseObject valueForKey:@"data"];
            
            
        }
        else
        {
            /*UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                            message:@"Mobile Number Not Exist, Please Signup First"
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
             */
        }
        
        [_tbl_cct reloadData];
        
        [APP_DELEGATE hideLoadingView];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                        message:@"Can not load data."
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        
        [self.navigationController popViewControllerAnimated:NO];
        
    }];
    
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return Dictdata.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 75;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CCTCell *cell = (CCTCell *)[_tbl_cct dequeueReusableCellWithIdentifier:@"Cell"];
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"CCTCell" owner:self options:nil];
    if(cell == nil){
        cell = nib[0];
    }
    
    cell.lbl_sub.text=[NSString stringWithFormat:@"%@",[Dictdata valueForKey:@"SubjectName"][indexPath.row]];
    
    //-----TODATE---------
    NSString * yourJSONString = [Dictdata valueForKey:@"ModifiedOn"][indexPath.row];//EndDate
    NSDateFormatter *currentDTFormatter = [[NSDateFormatter alloc] init];;
    [currentDTFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *dateFromString = [currentDTFormatter dateFromString:yourJSONString];
    [currentDTFormatter setDateFormat:@"MMM"];
    NSString *monthnm = [currentDTFormatter stringFromDate:dateFromString];
    [currentDTFormatter setDateFormat:@"dd"];
    NSString *onlydt = [currentDTFormatter stringFromDate:dateFromString];
    
    cell.lbl_month.text=[NSString stringWithFormat:@"%@",monthnm];
    cell.lbl_date.text=[NSString stringWithFormat:@"%@",onlydt];
    
    //-------Expire Date--------
    
    NSString * yourstring = [Dictdata valueForKey:@"ExpiryDate"][indexPath.row];//EndDate
    NSDateFormatter *currentDTFormatter1 = [[NSDateFormatter alloc] init];;
    [currentDTFormatter1 setDateFormat:@"yyyy-MM-dd"];
    NSDate *dateFromString1 = [currentDTFormatter1 dateFromString:yourstring];
    [currentDTFormatter1 setDateFormat:@"dd-MM-yyyy"];
    NSString *expdt = [currentDTFormatter1 stringFromDate:dateFromString1];
    
    cell.lbl_expire.text=[NSString stringWithFormat:@"%@",expdt];
    
    
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    MCQCCT *add = [[MCQCCT alloc] initWithNibName:@"MCQCCT" bundle:nil];
    add.cctPaperID=[Dictdata valueForKey:@"PaperID"][indexPath.row];
    [self.navigationController pushViewController:add animated:YES];
    
    
}



- (IBAction)btn_BACK:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btn_SEARCH:(id)sender {
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ICSearchPaper *detailVC = [sb instantiateViewControllerWithIdentifier:@"ICSearchPaper"];
    [self.navigationController pushViewController:detailVC animated:YES];
  
}
@end
