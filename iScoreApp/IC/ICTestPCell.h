//
//  ICTestPCell.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 9/10/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ICTestPCell : UITableViewCell



@property (weak, nonatomic) IBOutlet UIView *vw_shadow;
@property (weak, nonatomic) IBOutlet UIView *vw_dt;
@property (weak, nonatomic) IBOutlet UILabel *lbl_month;
@property (weak, nonatomic) IBOutlet UILabel *lbl_dt;
@property (weak, nonatomic) IBOutlet UILabel *lbl_subjectnm;
@property (weak, nonatomic) IBOutlet UILabel *lbl_marks;

@property (weak, nonatomic) IBOutlet UIView *cw_shadow2;
@property (weak, nonatomic) IBOutlet UIView *vw_dt2;
@property (weak, nonatomic) IBOutlet UILabel *lbl_month2;
@property (weak, nonatomic) IBOutlet UILabel *lbl_dt2;
@property (weak, nonatomic) IBOutlet UILabel *lbl_subjectnm2;
@property (weak, nonatomic) IBOutlet UILabel *lbl_marks2;
@property (weak, nonatomic) IBOutlet UIProgressView *prog_marks;






@end
