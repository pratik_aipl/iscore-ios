//
//  ICSearchPaper.m
//  iScoreApp
//
//  Created by ADMIN-Khushal on 9/10/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "ICSearchPaper.h"
#import "SPMCQTest.h"
#import "UIColor+CL.h"

@interface ICSearchPaper ()

@end

@implementation ICSearchPaper

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self performSegueWithIdentifier:@"mcqview1" sender:nil];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




- (IBAction)btn_BACK:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (IBAction)btn_PracticePaper:(id)sender {
    _lb_mcq.hidden=YES;
    _lb_pracpap.hidden=NO;
    
    [self performSegueWithIdentifier:@"practicepaper" sender:nil];
}

- (IBAction)btn_MCQTest:(id)sender {
    _lb_mcq.hidden=NO;
    _lb_pracpap.hidden=YES;
    [self performSegueWithIdentifier:@"mcqview1" sender:nil];
}

@end
