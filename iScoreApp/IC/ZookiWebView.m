//
//  ZookiWebView.m
//  iScoreApp
//
//  Created by ADMIN-Khushal on 9/14/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "ZookiWebView.h"
#import "ApplicationConst.h"
#import "AppDelegate.h"


@interface ZookiWebView ()

@end

@implementation ZookiWebView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //[APP_DELEGATE showLoadingView:@""];
    
   // [_web_zooki loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"https://parshvaa.com/blog_copy/"]]];
    
    
    NSURL *targetURL = [NSURL URLWithString:@"https://parshvaa.com/blog_copy/"];
    NSURLRequest *request = [NSURLRequest requestWithURL:targetURL];
    [_web_zooki loadRequest:request];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
//- (void)webViewDidFinishLoad:(UIWebView *)webView
//{
//    //[APP_DELEGATE hideLoadingView];
//}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btn_BACK:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
