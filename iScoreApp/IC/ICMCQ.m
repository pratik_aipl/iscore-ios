//
//  ICMCQ.m
//  iScoreApp
//
//  Created by ADMIN-Khushal on 9/10/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "ICMCQ.h"
#import "CollectionView.h"
#import "TableviewCell.h"
#import "QuePaperPDFVC.h"
#import "QueAnsPaperPDFVC.h"
#import <AFNetworking/AFNetworking.h>

//#import "PaperTypeModel.h"
#import "SetPaperModel.h"
#import "ApplicationConst.h"
#import "SetPaperQuestionModel.h"
#import "UIColor+CL.h"
#import "AllSubjectModel.h"
#import "MCQFilterModel.h"
#import "ICMCQCell.h"

#import "MCQRedirectModel.h"

#import "TestReportVC.h"
#import "MCQCCT.h"
#import "UIColor+CL.h"



@interface ICMCQ ()

@end

@implementation ICMCQ

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    arrSubjects=[[NSMutableArray alloc]init];
    arrchk=[[NSMutableArray alloc]init];
    arrlevel=[[NSMutableArray alloc]init];
    arrList=[[NSMutableArray alloc]init];
    
    
    [self CallgetSubjects];
    [self CallgetTestList];
    
    
    
    for (int i=0; i<3; i++) {
        NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
        [dict setValue:@"Level" forKey:@"level"];
        [dict setValue:@"0" forKey:@"isCheck"];
        [arrlevel addObject:dict];
    }
    
    //SHADOW
    _vw_tbl.layer.shadowRadius  = 3.5f;
    _vw_tbl.layer.shadowColor   = [UIColor colorWithRed:176.f/255.f green:199.f/255.f blue:226.f/255.f alpha:1.f].CGColor;
    _vw_tbl.layer.shadowOffset  = CGSizeMake(0.0f, 0.0f);
    _vw_tbl.layer.shadowOpacity = 2.1f;
    _vw_tbl.layer.masksToBounds = NO;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)CallgetSubjects
{
    [APP_DELEGATE showLoadingView:@""];
    
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    
    [AddPost setValue: [[NSUserDefaults standardUserDefaults] valueForKeyPath:@"ParseData.StudentID"] forKey:@"StudID"];//taken_test
    [AddPost setValue: @"mcq" forKey:@"section_type"];
    
    NSLog(@"StudID %@",AddPost);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    //%@get_student_class_dtl?",BaseURLAPI
    
    [manager GET:[NSString stringWithFormat:@"%@get-evaluator-subjects?",ICAPIURL] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject){
        NSLog(@"JSON: %@", responseObject);
        
        
        
        if ([[responseObject valueForKey:@"status"] intValue]==1) {
            
            arrSubjects=[responseObject valueForKey:@"data"];
            
            for (int i=0; i<arrSubjects.count; i++) {
                [arrchk addObject:@"0"];
            }
            
            [_tbl_left reloadData];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                            message:@"Some Error"
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }
        
        
        [APP_DELEGATE hideLoadingView];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                        message:@"Can not load data."
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        
    }];
    
}

-(void)CallgetTestList
{
    [APP_DELEGATE showLoadingView:@""];
    
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    
    [AddPost setValue: [[NSUserDefaults standardUserDefaults] valueForKeyPath:@"ParseData.StudentID"] forKey:@"StudID"];//taken_test
    [AddPost setValue: @"1" forKey:@"taken_test"];
    
    NSLog(@"StudID %@",AddPost);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    //%@get_student_class_dtl?",BaseURLAPI
    
    [manager GET:[NSString stringWithFormat:@"%@get-recent-mcq-paper?",ICAPIURL] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject){
        NSLog(@"JSON: %@", responseObject);
        
        
        
        if ([[responseObject valueForKey:@"status"] intValue]==1) {
            
            arrList=[responseObject valueForKey:@"data"];
            
            [_tbl_list reloadData];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                            message:@"Mobile Number Not Exist, Please Signup First"
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }
        
        
        [APP_DELEGATE hideLoadingView];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                        message:@"Can not load data."
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        
        [self.navigationController popViewControllerAnimated:NO];
        
    }];
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView==_tbl_list) {
        return arrList.count;
    }
    else if (tableView==_tbl_left)
    {
        return arrSubjects.count;
    }
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==_tbl_list) {
        
        ICMCQCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellList"];
        if (cell == nil)
        {
            // Creating a cell here
            cell = [[ICMCQCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CellList"];
        }
        
        cell.lbl_sub.text=[NSString stringWithFormat:@"%@",[arrList valueForKey:@"SubjectName"][indexPath.row]];
        
        //-----TODATE---------
        NSString * yourJSONString = [arrList valueForKey:@"ModifiedOn"][indexPath.row];//EndDate
        NSDateFormatter *currentDTFormatter = [[NSDateFormatter alloc] init];;
        [currentDTFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSDate *dateFromString = [currentDTFormatter dateFromString:yourJSONString];
        [currentDTFormatter setDateFormat:@"MMM"];
        NSString *monthnm = [currentDTFormatter stringFromDate:dateFromString];
        [currentDTFormatter setDateFormat:@"dd"];
        NSString *onlydt = [currentDTFormatter stringFromDate:dateFromString];
        
        cell.lbl_month.text=[NSString stringWithFormat:@"%@",monthnm];
        cell.lbl_date.text=[NSString stringWithFormat:@"%@",onlydt];
        
        //-------Expire Date--------
        
        NSString * yourstring = [arrList valueForKey:@"ExpiryDate"][indexPath.row];//EndDate
        NSDateFormatter *currentDTFormatter1 = [[NSDateFormatter alloc] init];;
        [currentDTFormatter1 setDateFormat:@"yyyy-MM-dd"];
        NSDate *dateFromString1 = [currentDTFormatter1 dateFromString:yourstring];
        [currentDTFormatter1 setDateFormat:@"dd-MM-yyyy"];
        NSString *expdt = [currentDTFormatter1 stringFromDate:dateFromString1];
        
        cell.lbl_expire.text=[NSString stringWithFormat:@"%@",expdt];
        
        
        if ([[[arrList valueForKey:@"TakenTest"] objectAtIndex:indexPath.row] intValue]>0) {
            cell.lbl_takenTest.hidden=NO;
            cell.lbl_takenTest.text=@"View Report";
            cell.lbl_takenTest.textColor=[UIColor colorWithHex:0xD81D09];
            
        }
        else
        {
            cell.lbl_takenTest.hidden=NO;
            cell.lbl_takenTest.text=@"TakenTest";//085787
            cell.lbl_takenTest.textColor=[UIColor colorWithHex:0x085787];
            
        }
        
        
        return cell;
    }
    else if (tableView == _tbl_left)
    {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
        if (cell == nil)
        {
            // Creating a cell here
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
            
            UILabel *theLabelIWant = [[UILabel alloc] initWithFrame:CGRectMake(10, 14, 120, 21)];
            theLabelIWant.tag = 1;
            [cell.contentView addSubview:theLabelIWant];
            
            UIImageView *dot =[[UIImageView alloc] initWithFrame:CGRectMake(150,14,21,21)];
            dot.tag=2;
            
            [cell.contentView addSubview:dot];
        }
        
        UILabel *finalLabel = (UILabel *)[[cell contentView] viewWithTag:1];
        [finalLabel setText:[NSString stringWithFormat:@"%@",[arrSubjects valueForKey:@"SubjectName"][indexPath.row]]];
        UIImageView *dot =(UIImageView *)[[cell contentView] viewWithTag:2];
        if ([[arrchk objectAtIndex:indexPath.row] isEqualToString:@"0"]) {
            dot.image=[UIImage imageNamed:@"checkbox-unselected1"];
        }
        else
        {
            dot.image=[UIImage imageNamed:@"checkbox-selected1"];
        }
        
        return cell;
    }
    
    return 0;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    if (tableView==_tbl_list) {
        
        //MCQFilterModel *Flist=[arrList objectAtIndex:indexPath.row];
        
        //[self CallGetRedirectMCQ:Flist.StudentMCQTestHDRID :Flist.SubjectName :Flist.SubjectID];
        
        /*  PDFView *vc2 = [[PDFView alloc] initWithNibName:@"PDFView" bundle:nil];
         [[self navigationController] pushViewController:vc2 animated:YES];*/
        
        
        
        
        if ([[[arrList valueForKey:@"TakenTest"] objectAtIndex:indexPath.row] intValue]>0) {
            TestReportVC *vc2 = [[TestReportVC alloc] initWithNibName:@"TestReportVC" bundle:nil];
            vc2.isCCT=@"1";
            vc2.CCTPaperId=[NSString stringWithFormat:@"%@",[[arrList valueForKey:@"PaperID"] objectAtIndex:indexPath.row]];
            vc2.subjectName=[NSString stringWithFormat:@"%@",[[arrList valueForKey:@"SubjectName"] objectAtIndex:indexPath.row]];
            [[self navigationController] pushViewController:vc2 animated:YES];
        }
        else
        {
            MCQCCT *add = [[MCQCCT alloc] initWithNibName:@"MCQCCT" bundle:nil];
            add.cctPaperID=[NSString stringWithFormat:@"%@",[[arrList valueForKey:@"PaperID"] objectAtIndex:indexPath.row]];
            [self.navigationController pushViewController:add animated:YES];
        }
        
        
        
    }
    else if (tableView == _tbl_left)
    {
        if ([[arrchk objectAtIndex:indexPath.row] isEqualToString:@"0"]) {
            [arrchk replaceObjectAtIndex:indexPath.row withObject:@"1"];
        }
        else
        {
            [arrchk replaceObjectAtIndex:indexPath.row withObject:@"0"];
        }
        [_tbl_left reloadData];
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}



- (IBAction)btn_LEFT:(id)sender
{
    _vw_left.hidden=NO;
    _btn_background.hidden=NO;
    
    [_tbl_left reloadData];
}

- (IBAction)btn_DISMISSTAB:(id)sender
{
    _vw_left.hidden=YES;
    _btn_background.hidden=YES;
}

- (IBAction)btn_FILTERL:(id)sender
{
    _vw_left.hidden=YES;
    _btn_background.hidden=YES;
    
    arrSubIds=[[NSMutableArray alloc]init];
    
    for (int i=0; i<arrSubjects.count; i++) {
        if ([[arrchk objectAtIndex:i]isEqualToString:@"1"]) {
            [arrSubIds addObject:[[arrSubjects objectAtIndex:i] valueForKey:@"SubjectID"]];
        }
    }
    
    
    [self CallFilter];
}


-(void)CallFilter
{
    [APP_DELEGATE showLoadingView:@""];
    
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    
    [AddPost setValue: [[NSUserDefaults standardUserDefaults] valueForKeyPath:@"ParseData.StudentID"] forKey:@"StudID"];//taken_test
    [AddPost setValue:[NSString stringWithFormat:@"%@",[arrSubIds componentsJoinedByString:@","]] forKey:@"subject_id"];
    [AddPost setValue: @"1" forKey:@"class_id"];
    
    NSLog(@"StudID %@",AddPost);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    //%@get_student_class_dtl?",BaseURLAPI
    
    [manager GET:[NSString stringWithFormat:@"%@get-subject-mcq-paper?",ICAPIURL] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject){
        NSLog(@"JSON: %@", responseObject);
        
        
        
        if ([[responseObject valueForKey:@"status"] intValue]==1) {
            
            arrList=[[NSMutableArray alloc]init];
            
            arrList=[responseObject valueForKey:@"data"];
            
            
            [_tbl_list reloadData];
        }
        else
        {
            arrList=[[NSMutableArray alloc]init];
            
            //arrList=[responseObject valueForKey:@"data"];
            
            
            [_tbl_list reloadData];
         /*   UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                            message:@"Some Error"
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];*/
        }
        
        
        [APP_DELEGATE hideLoadingView];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                        message:@"Can not load data."
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        
        
    }];
    
}


@end
