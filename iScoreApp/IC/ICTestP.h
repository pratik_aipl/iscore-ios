//
//  ICTestP.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 9/10/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ICTestP : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
    
    
    NSString *filePath;
    
    NSMutableArray *arrSubjects,*arrchk,*arrPtype;
    NSMutableArray *arrList,*arrlevel;
    NSMutableArray *arrSubIds,*arrLevelIds;
    
}






//Outlets
@property (weak, nonatomic) IBOutlet UIView *vw_left;

@property (weak, nonatomic) IBOutlet UIButton *btn_background;

@property (weak, nonatomic) IBOutlet UITableView *tbl_left;

@property (weak, nonatomic) IBOutlet UITableView *tbl_list;
@property (weak, nonatomic) IBOutlet UIView *vw_tbl;




//Action
- (IBAction)btn_LEFT:(id)sender;

- (IBAction)btn_DISMISSTAB:(id)sender;

- (IBAction)btn_FILTERL:(id)sender;




@end



