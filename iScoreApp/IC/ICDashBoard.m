//
//  ICDashBoard.m
//  iScoreApp
//
//  Created by ADMIN-Khushal on 9/8/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "ICDashBoard.h"
#import "SWRevealViewController.h"
#import "UIImageView+WebCache.h"
#import "CCT.h"
#import "ICTestPaper.h"

#import "ApplicationConst.h"
#import <AFNetworking/AFNetworking.h>

#import "ICSearchPaper.h"
#import "NoticeBoard.h"

#import "HomeVC.h"
#import "LectureViewController.h"




@interface ICDashBoard ()

@end

@implementation ICDashBoard

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self customSetup];
    
    ///ADD TAP
    picker = [[UIView alloc] initWithFrame:self.view.bounds];
    picker.backgroundColor=[UIColor blackColor];
    picker.alpha=0.4;
    [self.view addSubview: picker];
    picker.hidden=YES;
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self.revealViewController
                                            action:@selector(revealToggle:)];
    [picker addGestureRecognizer:singleFingerTap];
    
    [self CallClassDtl];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    //[self customSetup];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)CallClassDtl
{
    [APP_DELEGATE showLoadingView:@""];

    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
   
    
    [AddPost setValue: [[NSUserDefaults standardUserDefaults] valueForKeyPath:@"ParseData.StudentID"] forKey:@"StudID"];
    
    NSLog(@"StudID %@",AddPost);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    //%@get_student_class_dtl?",BaseURLAPI
    
    [manager GET:[NSString stringWithFormat:@"%@get_student_class_dtl?",ICAPIURL] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject){
        NSLog(@"JSON: %@", responseObject);
        
        
        
        
        if ([[responseObject valueForKey:@"status"] intValue]==1) {
            
            DictData=[responseObject valueForKey:@"data"];
            
            _lbl_classnm.text=[DictData valueForKey:@"ClassName"];
            NSURL *imgURL=[NSURL URLWithString:[NSString stringWithFormat:@"%@",[DictData valueForKey:@"ClassRoundLogo"]]];
            [_img_class sd_setImageWithURL:imgURL placeholderImage:[UIImage imageNamed:@"default_student_image"]];
            
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                            message:@"Responce is Null."
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }
        
        [APP_DELEGATE hideLoadingView];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                        message:@"Can not load data."
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        
        [self.navigationController popViewControllerAnimated:NO];
        
    }];
    
}

- (void)customSetup
{
    
    
 
    SWRevealViewController *revealViewController = self.revealViewController;
    [revealViewController setDelegate:self];
    
    
    if ( revealViewController )
    {
        [_btn_menu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [picker addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        [self.view addGestureRecognizer: self.revealViewController.panGestureRecognizer];
    }

  //  self.revealViewController.rearViewRevealWidth = 250;
    
}
- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position
{
    
    if (revealController.frontViewPosition==FrontViewPositionRight)
    {
        picker.hidden=NO;
    }
    if (revealController.frontViewPosition==FrontViewPositionLeft)
    {
        picker.hidden=YES;
    }
}



- (IBAction)btn_BACK:(id)sender {
  //  [self.navigationController popViewControllerAnimated:NO];
   // [self.revealViewController.navigationController popViewControllerAnimated:YES];
    //[self.revealViewController pu];
    
    SWRevealViewController *revealController = self.revealViewController;
       
       HomeVC * next = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeVC"];
       
       UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:next];
       navigationController.navigationBar.hidden = YES;
       [revealController pushFrontViewController:navigationController animated:YES];
}

- (IBAction)btn_CCT:(id)sender {
    CCT *add = [[CCT alloc] initWithNibName:@"CCT" bundle:nil];
    [self.navigationController pushViewController:add animated:YES];
}

- (IBAction)btn_TESTPAPER:(id)sender {
    ICTestPaper *add = [[ICTestPaper alloc] initWithNibName:@"ICTestPaper" bundle:nil];
    [self.navigationController pushViewController:add animated:YES];
}

- (IBAction)btn_SEARCHPAPER:(id)sender {
    
    ICSearchPaper * next = [self.storyboard instantiateViewControllerWithIdentifier:@"ICSearchPaper"];
    [self.navigationController pushViewController:next animated:YES];

}

- (IBAction)btn_NOTICE:(id)sender {
    
    NoticeBoard *add = [[NoticeBoard alloc] initWithNibName:@"NoticeBoard" bundle:nil];
    [self.navigationController pushViewController:add animated:YES];
    
}

- (IBAction)btnlecture_click:(UIButton *)sender {
    
    LectureViewController * next = [self.storyboard instantiateViewControllerWithIdentifier:@"LectureViewController"];
    [self.navigationController pushViewController:next animated:YES];
}


@end
