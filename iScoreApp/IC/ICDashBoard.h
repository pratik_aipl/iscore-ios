//
//  ICDashBoard.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 9/8/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ICDashBoard : UIViewController
{
    UIView *picker;
    NSMutableDictionary *DictData;

}


@property (weak, nonatomic) IBOutlet UILabel *lbl_classnm;
@property (weak, nonatomic) IBOutlet UIImageView *img_class;


@property (weak, nonatomic) IBOutlet UIButton *btn_menu;


- (IBAction)btn_BACK:(id)sender;

- (IBAction)btn_CCT:(id)sender;
- (IBAction)btn_TESTPAPER:(id)sender;
- (IBAction)btn_SEARCHPAPER:(id)sender;
- (IBAction)btn_NOTICE:(id)sender;
- (IBAction)btnlecture_click:(UIButton *)sender;



@end
