//
//  StartMCQTest.m
//  iScoreApp
//
//  Created by ADMIN-Khushal on 4/26/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "StartMCQTest.h"
#import "CVCDispQuestions.h"
#import "CVCQuestionNo.h"
#import "Constant.h"
#import "SharedData.h"
#import "AppDelegate.h"

//#import "Dashboard.h"

#import "MyModel.h"
#import "API.h"
#import "TestReportVC.h"
#import "MyModel.h"
#import "UIColor+CL.h"
#import "HomeVC.h"



@interface StartMCQTest ()

@property (nonatomic, assign) CGFloat lastContentOffset;

@end

NSString * strUrl;
NSString * strQuestion , * strOption1 , * strOption2 ,* strOption3 , * strOption4;
CVCDispQuestions *cell ;


@implementation StartMCQTest

int curruntRow;
int lastHeaderID;
NSUInteger attempted;
bool isLoaded = false;
MZTimerLabel *timer,*totTaken;


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    Stopflag=0;
    Takantime=0;
    curruntRow=0;
    attempted=0;
    lastHeaderID=0;
    CountDownTime=0;
    
    
    
    [_collDispQuestions registerNib:[UINib nibWithNibName:@"CVCDispQuestions" bundle:nil] forCellWithReuseIdentifier:@"Cell"];
    
    [_collDispQuestionNo registerNib:[UINib nibWithNibName:@"CVCQuestionNo" bundle:nil] forCellWithReuseIdentifier:@"Cell"];
    
    arrChapter = [[NSMutableArray alloc]initWithArray:[_chapterList componentsSeparatedByString:@","]];
    
    
    NSLog(@"chapter List %@",_chapterList);
    NSLog(@"subject ID %@",_subjectId);
    NSLog(@"Levels %@",_levelId);
    NSLog(@"Timer Status %@",_TBStatus);
    Rtimer=[[NSMutableArray alloc]init];
    [self getData];
    [self getTotalDuration];
    
    
    
    cell.scrollVQuestion.delegate = self;
    
    marrFlag=[[NSMutableArray alloc]init];
    marrFlag1=[[NSMutableArray alloc]init];
    totAttempt=[[NSMutableArray alloc]init];
    arrmztimer=[[NSMutableArray alloc]init];
    
    tempCorrect=[[NSMutableArray alloc]init];
    tempIncorrect=[[NSMutableArray alloc]init];
    tempNotapper=[[NSMutableArray alloc]init];
    tempCorrectOp=[[NSMutableArray alloc]init];
    tempIncorrectOp=[[NSMutableArray alloc]init];
    tempNotapperOp=[[NSMutableArray alloc]init];
    
    for(int i = 0; i < ShareData.marrmcqquestion.count; i++) {
        
        [Rtimer addObject:@"0"];
        [totAttempt addObject:@"0"];
        [marrFlag addObject:[NSString stringWithFormat:@"0"]];
        

      /*
        CGRect visibleRect = (CGRect){.origin = _collDispQuestions.contentOffset, .size = _collDispQuestions.bounds.size};
        CGPoint visiblePoint = CGPointMake(CGRectGetMidX(visibleRect), CGRectGetMidY(visibleRect));
        NSIndexPath *visibleIndexPath = [_collDispQuestions indexPathForItemAtPoint:visiblePoint];
        NSLog(@"visibleIndexPath %@",visibleIndexPath);
        CVCDispQuestions *cell= [_collDispQuestions dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:visibleIndexPath];
        
         cell.mztime=[[MZTimerLabel alloc]initWithLabel:cell.lbl_quetimer andTimerType:MZTimerLabelTypeStopWatch];
        
        cell.mztime.tag=i+1;
        */
        [arrmztimer addObject:@"0"];
       
        
        
        
        
    }
    
    
    
    
    NSLog(@"%@",[arrmztimer objectAtIndex:0]);
    
    NSLog(@"myArray:\n%@", marrFlag);
    
    for(int i = 0; i < ShareData.marrmcqquestion.count; i++) {
        [marrFlag1 addObject:[NSString stringWithFormat:@"0"]];
    }
    NSLog(@"myArray:\n%@", marrFlag1);
    
    
    
    timer = [[MZTimerLabel alloc] initWithLabel:_lbl_countdown andTimerType:MZTimerLabelTypeTimer];
    [timer setCountDownTime:CountDownTime];
    timer.delegate=self;
    [timer start];
    
    
    
    if ([_TBStatus intValue]==1)
    {
        cell.lbl_quetimer.hidden=NO;
        _vw_countdown.hidden=NO;
        
        
        [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(takantime) userInfo:nil repeats:YES];

    }
    else
    {
        cell.lbl_quetimer.hidden=YES;
        _vw_countdown.hidden=YES;
    }
    
    
    
    lbltotTaken=[[UILabel alloc] init];
    
    totTaken = [[MZTimerLabel alloc] initWithLabel:lbltotTaken andTimerType:MZTimerLabelTypeStopWatch];
    [totTaken start];
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)timerLabel:(MZTimerLabel*)timerLabel finshedCountDownTimerWithTime:(NSTimeInterval)countTime{
    
    [totTaken pause];
    [cell.mztime pause];
    
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"Alert"
                                 message:@"Do you want to Continues test."
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    //Add Buttons
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"Continue.."
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
                                    //Handle your yes please button action here
                                   timer = [[MZTimerLabel alloc] initWithLabel:_lbl_countdown andTimerType:MZTimerLabelTypeStopWatch];
                                    [timer start];
                                    [timer addTimeCountedByTime:CountDownTime];
                                    [timer start];
                                    [totTaken start];
                                    [cell.mztime start];
                                    
                                }];
    
    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle:@"NO"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   //Handle no, thanks button
                                   [self btnSubmit];
                                   
                                   TestReportVC *add = [[TestReportVC alloc] initWithNibName:@"TestReportVC" bundle:nil];
                                   add.tempNotapper=tempNotapper;
                                   add.tempIncorrect=tempIncorrect;
                                   add.tempCorrect=tempCorrect;
                                   add.tempNotapperOp=tempNotapperOp;
                                   add.tempIncorrectOp=tempIncorrectOp;
                                   add.tempCorrectOp=tempCorrectOp;
                                   
                                   if ([_TBStatus intValue]==1)
                                   {
                                       add.TakanTime=lbltotTaken.text;
                                   }
                                   else
                                   {
                                       add.TakanTime=@"00:00";
                                   }
                                   
                                   
                                   add.chapterList=_chapterList;
                                   add.TBStatus=_TBStatus;
                                   add.subjectId=_subjectId;
                                   add.subjectName=_subjectName;
                                   add.levelId=_levelId;
                                   add.mcqQuestions=ShareData.marrmcqquestion;
                                   add.mcqOption=ShareData.marrmcqoption;
                                   //[self presentViewController:add animated:YES completion:nil];
                                   [self.navigationController pushViewController:add animated:YES];
                                   
                                   //[self TruncateTempTeble];
                                   //[self.navigationController popViewControllerAnimated:YES];
                                   
                               }];
    
    //Add your buttons to alert controller
    
    [alert addAction:yesButton];
    [alert addAction:noButton];
    
    [self presentViewController:alert animated:YES completion:nil];
    
    
    
}

-(void)getTotalDuration
{
    for (int i=0; i<[ShareData.marrmcqquestion count]; i++) {
        CountDownTime+=[[[ShareData.marrmcqquestion objectAtIndex:i] valueForKey:@"Duration"] intValue];
    }
    
}


-(void)takantime
{
    Takantime+=1;
    /* if ([_lbl_countdown.text isEqualToString:@"00:00"]) {
     
     }*/
    
 /*   if (Takantime==CountDownTime) {
        [timer pause];
        [cell.mztime pause];
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@"Alert"
                                     message:@"Do you still wish to continue.?"
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        //Add Buttons
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"YES"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
                                        //Handle your yes please button action here
                                        
                                        [timer start];
                                        [cell.mztime start];
                                    }];
        
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"NO"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       //Handle no, thanks button
                                       [self btn_SUBMIT:nil];
                                   }];
        
        //Add your buttons to alert controller
        
        [alert addAction:yesButton];
        [alert addAction:noButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
   */
    
    
}
-(void)viewWillLayoutSubviews{
    
    
}

#pragma mark - UICollectionView Method
-(void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath{
    
    //NSLog(@"indexPath===%ld",(long)indexPath.row);
    
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    if(collectionView == self.collDispQuestionNo){
        return ShareData.marrmcqquestion.count;
    }else{
        return ShareData.marrmcqquestion.count;
    }
    
}


-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    if(collectionView == self.collDispQuestionNo){
        
        CVCQuestionNo *cellNo = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
        cellNo.lblQuestionNo.text = [NSString stringWithFormat:@"%ld",indexPath.row + 1];
        
        [cellNo.lblSelection setHidden:true];
        if(indexPath.row == curruntRow){
            [cellNo.lblSelection setHidden:false];
        }
        
        cellNo.btn_flag1.tag=5;
        if ([[marrFlag objectAtIndex:indexPath.row] intValue]==0) {
            cellNo.btn_flag1.selected=NO;
        }
        else
        {
            cellNo.btn_flag1.selected=YES;
        }
        
        return cellNo;
        
    }else{
        
        
        NSLog(@"Time Label  %@",cell.mztime.timeLabel.text);
    
        
        
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
        
      
        NSLog(@"tag %@",cell.mztime);
        NSLog(@"tag %ld",(long)cell.mztime.tag);
        
        
        
        
        
        
        if ([_TBStatus intValue]==1)
        {
            cell.lbl_quetimer.hidden=YES;
            _vw_countdown.hidden=NO;
            
            //cellTime=[[MZTimerLabel alloc]initWithLabel:cell.lbl_quetimer];
            //[cellTime start];
            
           
            
            
           /*
            if (![[arrmztimer objectAtIndex:indexPath.row]isEqualToString:@"0"]) {
                
                NSLog(@"dfsfs");
                
                
               // cell.mztime=[arrmztimer objectAtIndex:indexPath.row];
                
                
            }
            else
            {
                cell.mztime =[[MZTimerLabel alloc]initWithLabel:cell.lbl_quetimer];
                
                [cell.mztime start];
                
                cell.mztime.tag=indexPath.row;
                
                [arrmztimer replaceObjectAtIndex:indexPath.row withObject:cell.mztime];
            }
            */
            
            
            
        }
        else
        {
            cell.lbl_quetimer.hidden=YES;
            _vw_countdown.hidden=YES;
        }
        
        
        cell.webVQuestion.frame=[[UIScreen mainScreen] bounds];
        
        cell.viewQuestionHeight.constant = 50.0;
        cell.viewOption1Height.constant = 50.0;
        cell.viewOption2Height.constant = 50.0;
        cell.viewOption3Height.constant = 50.0;
        cell.viewOption4Height.constant = 50.0;
        
        cell.webVQuestion.tag = 1;
        cell.webVOption1.tag = 2;
        cell.webVOption2.tag = 3;
        cell.webVOption3.tag = 4;
        cell.webVOption4.tag = 5;
        
        cell.webVQuestion.contentMode = UIViewContentModeScaleAspectFit;
       // [cell.webVQuestion setScalesPageToFit:YES];
        
        strQuestion = [self displayWebviewORTextView:[[ShareData.marrmcqquestion objectAtIndex:indexPath.row] valueForKey:@"Question"] Webview:cell.webVQuestion TextView:cell.txtVQuestion BGView:cell.viewQuestionHeight];
        
        
        cell.viewOption1.hidden=YES;
        cell.viewOption2.hidden=YES;
        cell.viewOption3.hidden=YES;
        cell.viewOption4.hidden=YES;
        
        
        int optot=[[ShareData.marrmcqoption objectAtIndex:indexPath.row] count];
        
        NSLog(@"optot == %d",optot);
        
        for (int i=0; i < optot; i++) {
            switch (i) {
                case 0:
                    
                    cell.viewOption1.hidden=NO;
                    strOption1 = [self displayWebviewORTextView:[[[ShareData.marrmcqoption objectAtIndex:indexPath.row] objectAtIndex:0] valueForKey:@"Options"] Webview:cell.webVOption1 TextView:cell.txtVOption1 BGView:cell.viewOption1Height];
                    break;
                case 1:
                    cell.viewOption2.hidden=NO;
                    strOption2 = [self displayWebviewORTextView:[[[ShareData.marrmcqoption objectAtIndex:indexPath.row] objectAtIndex:1] valueForKey:@"Options"] Webview:cell.webVOption2 TextView:cell.txtVOption2 BGView:cell.viewOption2Height];
                    break;
                case 2:
                    cell.viewOption3.hidden=NO;
                    strOption3 = [self displayWebviewORTextView:[[[ShareData.marrmcqoption objectAtIndex:indexPath.row] objectAtIndex:2] valueForKey:@"Options"] Webview:cell.webVOption3 TextView:cell.txtVOption3 BGView:cell.viewOption3Height];
                    break;
                case 3:
                    cell.viewOption4.hidden=NO;
                    strOption4 = [self displayWebviewORTextView:[[[ShareData.marrmcqoption objectAtIndex:indexPath.row] objectAtIndex:3] valueForKey:@"Options"] Webview:cell.webVOption4 TextView:cell.txtVOption4 BGView:cell.viewOption4Height];
                    break;
                    
                default:
                    break;
            }
        }

        [Rtimer replaceObjectAtIndex:curruntRow withObject:cell.lbl_quetimer.text];
        
        
        cell.btnOption1.tag = indexPath.row;
        cell.bntOption2.tag = indexPath.row;
        cell.btnOption3.tag = indexPath.row;
        cell.btnOption4.tag = indexPath.row;
        
        [cell.btnOption1 addTarget:self action:@selector(selectedAnswer1:) forControlEvents:UIControlEventTouchUpInside];
        [cell.bntOption2 addTarget:self action:@selector(selectedAnswer2:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnOption3 addTarget:self action:@selector(selectedAnswer3:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnOption4 addTarget:self action:@selector(selectedAnswer4:) forControlEvents:UIControlEventTouchUpInside];
        
        
        if (![[[ShareData.marrmcqquestion objectAtIndex:indexPath.row] valueForKey:@"selectedAns"] isEqual:@"0"]) {
            
            int  SelectedAnswer = [[[ShareData.marrmcqquestion objectAtIndex:indexPath.row] valueForKey:@"selectedAns"] intValue];
            NSLog(@"selected Answer : %d",SelectedAnswer);
            
            switch (SelectedAnswer) {
                case 1:
                    
                    [self selectedOptionColor:cell.lblA ];
                    [self unSelectedOptionColor:cell.lblB :cell.lblC  :cell.lblD];
                    
                    break;
                    
                case 2:
                    
                    [self selectedOptionColor:cell.lblB];
                    [self unSelectedOptionColor:cell.lblA :cell.lblC  :cell.lblD ];
                    break;
                    
                case 3:
                    
                    [self selectedOptionColor:cell.lblC ];
                    [self unSelectedOptionColor:cell.lblA :cell.lblB  :cell.lblD ];
                    break;
                    
                case 4:
                    
                    [self selectedOptionColor:cell.lblD ];
                    [self unSelectedOptionColor:cell.lblA :cell.lblB  :cell.lblC ];
                    
                    break;
                    
                default:
                    break;
                    
            }
            
        }
        else{
            
            cell.lblA.layer.backgroundColor = [UIColor clearColor].CGColor;
            cell.lblB.layer.backgroundColor = [UIColor clearColor].CGColor;
            cell.lblC.layer.backgroundColor = [UIColor clearColor].CGColor;
            cell.lblD.layer.backgroundColor = [UIColor clearColor].CGColor;
            
            
        }
        
        //FLAG
        cell.btn_flag.tag=indexPath.row;
        cell.lbl_queNo.text=[NSString stringWithFormat:@"%ld",indexPath.row+1];
        [cell.btn_flag addTarget:self action:@selector(selectedAnswer5:) forControlEvents:UIControlEventTouchUpInside];
        
        
        if ([[marrFlag objectAtIndex:indexPath.row] intValue]==0) {
            cell.btn_flag.selected=NO;
        }
        else
        {
            cell.btn_flag.selected=YES;
        }
        
        
        return cell;
    }
    
}


- (void)onTick:(NSIndexPath*) first
{
    NSLog(@" %@",first);
    [_collDispQuestions reloadItemsAtIndexPaths:@[first]];
    
}



-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if(collectionView == self.collDispQuestionNo){
        
        [self moveToQuestion:indexPath];
        
    }else{
        
        
    }
    
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if(collectionView == self.collDispQuestionNo){
        return CGSizeMake((self.collDispQuestionNo.frame.size.width/10), (self.collDispQuestionNo.frame.size.height));
    }
    else{
        
        return CGSizeMake((self.collDispQuestions.frame.size.width),(self.collDispQuestions.frame.size.height));
    }
}

#pragma mark - UIScrollView Method

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
   
    
    
    CGRect visibleRect = (CGRect){.origin = _collDispQuestions.contentOffset, .size = _collDispQuestions.bounds.size};
    CGPoint visiblePoint = CGPointMake(CGRectGetMidX(visibleRect), CGRectGetMidY(visibleRect));
    NSIndexPath *visibleIndexPath = [_collDispQuestions indexPathForItemAtPoint:visiblePoint];
    NSLog(@"visibleIndexPath %@",visibleIndexPath);
    
    
    if (scrollView == _collDispQuestionNo) {
        NSLog(@"_collDispQuestionNo ");
    }
    else
    {
        NSLog(@"scrollViewDidEndDecelerating");
        if (self.lastContentOffset > scrollView.contentOffset.x){
            [self moveToPreviousQuestion];
            
            
           /* if (visibleIndexPath.row == cell.mztime.tag) {
               
            }else{
            
           
            
            [cell.mztime pause];
            }*/
        }
        else if(self.lastContentOffset < scrollView.contentOffset.x) {
            [self moveToNextQuestion];
          /*  if (visibleIndexPath.row == cell.mztime.tag) {
                
            }else{
                
                
                
                [cell.mztime pause];
            }*/
        }
        
        
        
    }
    
    _lastContentOffset = scrollView.contentOffset.x;
    
     [cell.mztime reset];
    
    
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView{
    
    if(scrollView == self.collDispQuestions){
        NSLog(@"scrollViewDidEndScrollingAnimation");
        _lastContentOffset = scrollView.contentOffset.x;
        NSLog(@"lastContentOffset : %f",_lastContentOffset);
    }else{
        
    }
}

#pragma mark - Change Other Method
-(void)changeSelection:(int )index{
    if (![[[ShareData.marrmcqquestion objectAtIndex:index] valueForKey:@"selectedAns"] isEqual:@"0"]) {
        
        int  SelectedAnswer = [[[ShareData.marrmcqquestion objectAtIndex:index] valueForKey:@"selectedAns"] intValue];
        NSLog(@"selected Answer : %d",SelectedAnswer);
        
        switch (SelectedAnswer) {
            case 1:
                
                [self selectedOptionColor:cell.lblA ];
                [self unSelectedOptionColor:cell.lblB :cell.lblC  :cell.lblD];
                
                break;
                
            case 2:
                
                [self selectedOptionColor:cell.lblB];
                [self unSelectedOptionColor:cell.lblA :cell.lblC  :cell.lblD ];
                break;
                
            case 3:
                
                [self selectedOptionColor:cell.lblC ];
                [self unSelectedOptionColor:cell.lblA :cell.lblB  :cell.lblD ];
                break;
                
            case 4:
                
                
                [self selectedOptionColor:cell.lblD ];
                [self unSelectedOptionColor:cell.lblA :cell.lblB  :cell.lblC ];
                break;
                
            default:
                //[self unSelectedAllOptionColor:cell.lblA :cell.lblB :cell.lblC :cell.lblD];
                break;
                
        }
        
    }
    else{
        
        cell.lblA.layer.backgroundColor = [UIColor clearColor].CGColor;
        cell.lblB.layer.backgroundColor = [UIColor clearColor].CGColor;
        cell.lblC.layer.backgroundColor = [UIColor clearColor].CGColor;
        cell.lblD.layer.backgroundColor = [UIColor clearColor].CGColor;
        
        
    }
    
}


#pragma mark - Convert HTML Formated String
-(NSAttributedString *)convertIntoAttributadStr:(NSString *)str{
    
    
    NSString *htmlString = str;
    NSAttributedString *attributedString = [[NSAttributedString alloc]
                                            initWithData: [htmlString dataUsingEncoding:NSUnicodeStringEncoding]
                                            options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
                                            documentAttributes: nil
                                            error: nil
                                            ];
    
    return attributedString;
}



#pragma mark - Change Height Of Textview

- (void)changeTextViewHeight:(UITextView *)textView :(NSLayoutConstraint*)heightConstraint
{
    NSLog(@"changeTextViewHeight");
    
    
    CGSize constraint = CGSizeMake(textView.frame.size.width, CGFLOAT_MAX);
    CGSize size;
    
    NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
    CGSize boundingBox = [textView.text boundingRectWithSize:constraint
                                                     options:NSStringDrawingUsesLineFragmentOrigin
                                                  attributes:@{NSFontAttributeName:textView.font}
                                                     context:context].size;
    
    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    heightConstraint.constant = size.height + 10;
    
    NSLog(@"height : %f",size.height);
    
    
    
}

#pragma mark - change Webview height

- (void)changeheightOfWebview:(WKWebView *)aWebView :(NSLayoutConstraint*)heightConstraint{
    
//    NSString *str = [aWebView stringByEvaluatingJavaScriptFromString:@"(document.height !== undefined) ? document.height : document.body.offsetHeight;"];
   
    [aWebView evaluateJavaScript:@"(document.height !== undefined) ? document.height : document.body.offsetHeight;" completionHandler:^(NSString *result, NSError *error)
       {
           
           NSLog(@"INNER HTML: %@",result);
           // [self updateCookies:result];
        CGFloat height = result.floatValue;
        heightConstraint.constant = height + 15.0;
        NSLog(@"height: %f", height);
       }];
    
}


#pragma mark - Webview Delegate

//- (void)webViewDidFinishLoad:(UIWebView *)webView {
//
//
//    if (webView.tag == 1) {
//
//        if ([strQuestion isEqualToString:@"Web"]) {
//            [self changeheightOfWebview:cell.webVQuestion :cell.viewQuestionHeight];
//
//        }
//    }
//
//    if (webView.tag == 2) {
//
//        if ([strOption1 isEqualToString:@"Web"]){
//            [self changeheightOfWebview:cell.webVOption1 :cell.viewOption1Height];
//        }
//    }
//
//    if (webView.tag == 3) {
//
//        if ([strOption2 isEqualToString:@"Web"]){
//            [self changeheightOfWebview:cell.webVOption2 :cell.viewOption2Height];
//        }
//    }
//
//    if (webView.tag == 4) {
//
//        if ([strOption3 isEqualToString:@"Web"]){
//            [self changeheightOfWebview:cell.webVOption3 :cell.viewOption3Height];
//        }
//    }
//
//    if (webView.tag == 5) {
//
//        if ([strOption4 isEqualToString:@"Web"]){
//            [self changeheightOfWebview:cell.webVOption4 :cell.viewOption4Height];
//        }
//    }
//}

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    
    
    if (webView.tag == 1) {
        
        if ([strQuestion isEqualToString:@"Web"]) {
            [self changeheightOfWebview:cell.webVQuestion :cell.viewQuestionHeight];
            
        }
    }
    
    if (webView.tag == 2) {
        
        if ([strOption1 isEqualToString:@"Web"]){
            [self changeheightOfWebview:cell.webVOption1 :cell.viewOption1Height];
        }
    }
    
    if (webView.tag == 3) {
        
        if ([strOption2 isEqualToString:@"Web"]){
            [self changeheightOfWebview:cell.webVOption2 :cell.viewOption2Height];
        }
    }
    
    if (webView.tag == 4) {
        
        if ([strOption3 isEqualToString:@"Web"]){
            [self changeheightOfWebview:cell.webVOption3 :cell.viewOption3Height];
        }
    }
    
    if (webView.tag == 5) {
        
        if ([strOption4 isEqualToString:@"Web"]){
            [self changeheightOfWebview:cell.webVOption4 :cell.viewOption4Height];
        }
    }
}

#pragma mark - Check Question and Option String

- (NSString*)displayWebviewORTextView:(NSString *)Str Webview:(WKWebView*)Webview TextView:(UITextView*)TextView BGView:(NSLayoutConstraint*)BGViewHeightConstant {
    
    NSString *filePath = [MyModel getFilePath];
    
    if (![Str containsString:@"<img"]|| ![Str containsString:@"<math"] || ![Str containsString:@"<table"] ) {
        
        NSLog(@"string does not contain tags");
        Webview.hidden =true;
        TextView.hidden =false;
        TextView.attributedText = [self convertIntoAttributadStr:[NSString stringWithFormat:@"%@",Str]];
        [self changeTextViewHeight:TextView :BGViewHeightConstant];
        return @"Text";
        
    } else {
        
        NSLog(@"string contains tag!");
        if ([Str containsString:@"src"]) {
            NSLog(@"Replace Path");
            
            NSString *offUrl=[NSString stringWithFormat:@"src=\"%@/",filePath];
            
            Str = [Str stringByReplacingOccurrencesOfString:@"src=\"http://staff.parshvaa.com/" withString:offUrl];//
            NSLog(@"apdateString %@",Str);
            
        }
        
        TextView.hidden =true;
        Webview.hidden =false;
        NSURL *baseURL = [NSURL fileURLWithPath:filePath];
        [Webview loadHTMLString:Str baseURL:baseURL];
        
        Str=[self MathHTMLString:Str];
        
        
        return @"Web";
        
    }
    
}

-(NSString*)MathHTMLString :(NSString*)str
{
    NSString *htstr=[NSString stringWithFormat:@"<!DOCTYPE html>\n"
                     "<html>\n"
                     "<head>\n"
                     "<title>MathJax MathML Test Page</title>\n"
                     "<!-- Copyright (c) 2010-2016 The MathJax Consortium -->\n"
                     "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\n"
                     "<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" />\n"
                     "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n"
                     "\n"
                     "\n"
                     "<link rel=\"stylesheet\" href=\"file:///mathscribe/jqmath-0.4.3.css\">"
                     "\n"
                     "<script type=\"text/javascript\" src=\"file:///mathscribe/jquery-1.4.3.min.js\"></script>\n"
                     "\n"
                     "<script type=\"text/javascript\" src=\"file:///mathscribe/jqmath-etc-0.4.6.min.js\"></script>\n"
                     "\n"
                     "<style> body{} </style>"
                     "\n"
                     "</head>\n"
                     "<body style='font-family:'arial';font-size:22px;color:#000000;'> \n"
                     "\n"
                     "<p>"
                     "</p>\n"
                     "\n"
                     "</body>\n"
                     "</html>"];
    
    return htstr;
}



#pragma mark - Action Method

- (IBAction)btnBack:(id)sender {
    
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"Alert" message:@"Are you sure you want to Leave the test?" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction * yes = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        
    }];
    UIAlertAction * no = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault handler:nil];
    
    [alert addAction:no];
    [alert addAction:yes];
    
    [self presentViewController:alert animated:YES completion:nil];
    
}

- (IBAction)bntPrevious:(id)sender {
    
    NSLog(@"btnPrevious tap");
    [self moveToPreviousQuestion];
}

- (IBAction)btnNext:(id)sender {
    
    NSLog(@"btnNext tap");
    [self moveToNextQuestion];
    
}
-(void)CallIncorrect
{
    NSMutableArray *temp3=[[NSMutableArray alloc]initWithArray:ShareData.marrmcqquestion];
    NSMutableArray *temp4=[[NSMutableArray alloc]initWithArray:ShareData.marrmcqoption];
    
    NSLog(@"temp3  %@",temp3);
    NSUserDefaults *stander=[[NSUserDefaults alloc]init];
    
    for (int i = 0; i < temp3.count; i++) {
        
        //Filter Questions
        if ([[[temp3 valueForKey:@"isAttempted"] objectAtIndex:i] intValue]==1) {
            
            [MyModel deleteIncorrectAndNotappered:@"student_not_appeared_question" :[[temp3 objectAtIndex:i] valueForKey:@"MCQQuestionID"]];
            
            if ([[[temp3 valueForKey:@"isRight"] objectAtIndex:i] intValue]==0) {
                NSMutableDictionary *mutDic=[[NSMutableDictionary alloc] init];
                
                
                [mutDic setValue:[[temp3 objectAtIndex:i] valueForKey:@"MCQQuestionID"] forKey:@"QuestionID"];
                [mutDic setValue:[stander valueForKeyPath:@"ParseData.StudentID"] forKey:@"StudentID"];
                [mutDic setValue:[MyModel getCuruntDateTime] forKey:@"CreatedOn"];
                [MyModel insertInto_papertype:mutDic :@"student_incorrect_question"];
                [tempIncorrect addObject:[temp3 objectAtIndex:i]];
                [tempIncorrectOp addObject:[temp4 objectAtIndex:i]];
                
            }
            else
            {
                [tempCorrect addObject:[temp3 objectAtIndex:i]];
                ///REMOVE SUTDENT
                [MyModel deleteIncorrectAndNotappered:@"student_incorrect_question" :[[temp3 objectAtIndex:i] valueForKey:@"MCQQuestionID"]];
                [tempCorrectOp addObject:[temp4 objectAtIndex:i]];
            }
        }
        else
        {
            [tempNotapper addObject:[temp3 objectAtIndex:i]];
            [MyModel deleteIncorrectAndNotappered:@"student_incorrect_question" :[[temp3 objectAtIndex:i] valueForKey:@"MCQQuestionID"]];
            [tempNotapperOp addObject:[temp4 objectAtIndex:i]];
        }
    }
    
    NSLog(@"tempIncorrect %lu",(unsigned long)tempIncorrect.count);
    NSLog(@"tempCorrect %lu",(unsigned long)tempCorrect.count);
    NSLog(@"tempNotapper %lu",(unsigned long)tempNotapper.count);
}
-(void)CallnotAppered
{
    NSMutableArray *temp3=[[NSMutableArray alloc]initWithArray:ShareData.marrmcqquestion];
    NSUserDefaults *stander=[[NSUserDefaults alloc]init];
    NSLog(@"temp3  %@",temp3);
    
    for (int i = 0; i < temp3.count; i++) {
        
        if ([[[temp3 valueForKey:@"isAttempted"] objectAtIndex:i] intValue]==0) {
            NSMutableDictionary *mutDic=[[NSMutableDictionary alloc] init];
            [mutDic setValue:[[temp3 objectAtIndex:i] valueForKey:@"MCQQuestionID"] forKey:@"QuestionID"];
            [mutDic setValue:[stander valueForKeyPath:@"ParseData.StudentID"] forKey:@"StudentID"];
            [mutDic setValue:[MyModel getCuruntDateTime] forKey:@"CreatedOn"];
            [MyModel insertInto_papertype:mutDic :@"student_not_appeared_question"];
        }
    }
}


- (void)btnSubmit {
    
    //[MyModel deleteStudentTabledata];
    
    [self insertIntostudent_mcq_test_hdrTable];
    [self gettemp_mcq_test_dtl];
    [self insertIntostudent_mcq_test_dtlTable];
    [self gettemp_mcq_test_chapter];
    [self insertIntostudent_mcq_test_levelTable];
    [self insertIntostudent_mcq_test_chapterTable];
    
    [self CallIncorrect];
    [self CallnotAppered];
    [MyModel deletetemporaryTabledata];
    
    NSMutableArray *temp_arr1=[[NSMutableArray alloc] initWithArray:ShareData.marrtemp_mcq_test_dtl];
    NSMutableArray *temp_arr2=[[NSMutableArray alloc] initWithArray:ShareData.marrtemp_mcq_test_hdr];
    
    
    NSLog(@"Log %@",temp_arr1);
    NSLog(@"Log %@",temp_arr2);
    
    
    
    
    attempted = [MyModel getCount:[NSString stringWithFormat:@"select COUNT(*) from  student_mcq_test_dtl  where StudentMCQTestHDRID= %d  AND IsAttempt= %d",lastHeaderID,1]];
    
    NSLog(@"attempted : %lu",(unsigned long)attempted);
    
    [self insertIntonotificationTable];
    [self getnotificationTableData];
    
    if ([[UserDefault valueForKey:@"LastSyncTime"] isEqualToString:@""]) {
        
        strUrl = [NSString stringWithFormat:@"select * FROM student_mcq_test_hdr"];
        [self getstudent_mcq_test_hdrTableData:strUrl];
        
    }else{
        
        //NSLog(@"last : %@",[UserDefault valueForKey:@"LastSyncTime"]);
        NSString *dateStr =[NSString stringWithFormat:@"%@",[UserDefault valueForKey:@"LastSyncTime"]];
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSDate *date = [dateFormat dateFromString:dateStr];
        
        [dateFormat setDateFormat:@"dd-MM-yyyy HH:mm:ss"];
        dateStr = [dateFormat stringFromDate:date];
        
        strUrl =[NSString stringWithFormat:@"select * FROM student_mcq_test_hdr where CreatedOn > '%@'",dateStr];
        [self getstudent_mcq_test_hdrTableData:strUrl];
    }
    
    for (int i = 0; i<ShareData.marrstudent_mcq_test_hdr.count; i++) {
        
        NSString * hdrID  = [[ShareData.marrstudent_mcq_test_hdr objectAtIndex:i]valueForKey:@"StudentMCQTestHDRID"];
        NSLog(@"hdrID : %@",hdrID);
        
        FMResultSet * result = [MyModel selectQuery:[NSString stringWithFormat:@"select * FROM student_mcq_test_chapter where StudentMCQTestHDRID = %@ ",hdrID]];
        
        NSMutableDictionary* mdict = [[NSMutableDictionary alloc]init];
        NSMutableArray * marr = [[NSMutableArray alloc]init];
        
        while ([result next]) {
            
            NSMutableDictionary * subDict = [[NSMutableDictionary alloc]init];
            
            [subDict setValue:[result stringForColumn:@"StudentMCQTestChapterID"] forKey:@"StudentMCQTestChapterID"];
            [subDict setValue:[result stringForColumn:@"StudentMCQTestHDRID"] forKey:@"StudentMCQTestHDRID"];
            [subDict setValue:[result stringForColumn:@"ChapterID"] forKey:@"ChapterID"];
            
            [marr addObject:subDict];
        }
        
        [[ShareData.marrstudent_mcq_test_hdr objectAtIndex:i] setValue:marr forKey:@"chapterArray"];
        
        FMResultSet * result1 = [MyModel selectQuery:[NSString stringWithFormat:@"select * FROM student_mcq_test_dtl where StudentMCQTestHDRID = %@ ",hdrID]];
        
        mdict  = [[NSMutableDictionary alloc]init];
        marr = [[NSMutableArray alloc]init];
        
        while ([result1 next]) {
            
            NSMutableDictionary * subDict = [[NSMutableDictionary alloc]init];
            
            [subDict setValue:[result1 stringForColumn:@"StudentMCQTestDTLID"] forKey:@"StudentMCQTestDTLID"];
            [subDict setValue:[result1 stringForColumn:@"StudentMCQTestHDRID"] forKey:@"StudentMCQTestHDRID"];
            [subDict setValue:[result1 stringForColumn:@"srNo"] forKey:@"srNo"];
            [subDict setValue:[result1 stringForColumn:@"QuestionID"] forKey:@"QuestionID"];
            [subDict setValue:[result1 stringForColumn:@"AnswerID"] forKey:@"AnswerID"];
            [subDict setValue:[result1 stringForColumn:@"IsAttempt"] forKey:@"IsAttempt"];
            [subDict setValue:[result1 stringForColumn:@"CreatedOn"] forKey:@"CreatedOn"];
            [subDict setValue:[result1 stringForColumn:@"CreatedBy"] forKey:@"CreatedBy"];
            [subDict setValue:[result1 stringForColumn:@"ModifiedBy"] forKey:@"ModifiedBy"];
            [subDict setValue:[result1 stringForColumn:@"ModifiedOn"] forKey:@"ModifiedOn"];
            
            [marr addObject:subDict];
        }
        
        
        [[ShareData.marrstudent_mcq_test_hdr objectAtIndex:i] setValue:marr forKey:@"detailArray"];
    }
    NSLog(@"marrstudent_mcq_test_hdr : %@" ,ShareData.marrstudent_mcq_test_hdr);
    
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:ShareData.marrstudent_mcq_test_hdr options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSLog(@"Jsonstring:%@", jsonString);
    
    NSString * url = [NSString stringWithFormat:@"%@/web-services/datasync?",Main_URL];
    
    NSMutableDictionary * param = [[NSMutableDictionary alloc]init];
    
    //[param setObject:@"mcq" forKey:@"action"];
    //[param setObject:jsonString forKey:@"data"];
    //[param setObject:[UserDefault valueForKey:@"reg_key"] forKey:@"RegKey"];
    
    /* [API PostUrl:url Parameters:param andCallback:^(NSMutableDictionary *result, NSError *error)  {
     
     if (error==nil) {
     
     NSLog(@"Response:-%@",result);
     
     if ([result valueForKey:@"status"]) {
     
     if ([result valueForKey:@"MCQLastSync"] != nil) {
     
     [UserDefault setValue:[result valueForKey:@"MCQLastSync"] forKey:@"LastSyncTime"];
     NSLog(@"LastSynctime : %@",[UserDefault valueForKey:@"LastSyncTime"]);
     
     }
     }
     
     }else{
     
     NSLog(@"Error : %@ ",error);
     
     }
     }];*/
    
    
    // ResultView * next = [self.storyboard instantiateViewControllerWithIdentifier:@"ResultView"];
    // [self.navigationController pushViewController:next animated:YES];
    
}

#pragma mark - Move NEXT_PREVIOUS Method

-(void)moveToNextQuestion{
    
    NSLog(@"moveToNextQuestion");
    
    
    
    curruntRow++;
    
    if(curruntRow == [ShareData.marrmcqquestion count] - 1){
        
        //_btnNext.enabled = false;
        
    }else{
        
        if(curruntRow > 0 ){
            
            //_btnPrevious.enabled = true;
        }
    }
    
    NSIndexPath *nextItem = [NSIndexPath indexPathForItem:curruntRow inSection:0];
    
    [self.collDispQuestions scrollToItemAtIndexPath:nextItem atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
    [self.collDispQuestionNo scrollToItemAtIndexPath:nextItem atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:NO];
    
    [self changeQuestionNo:curruntRow];
    
    NSLog(@"curruntRow %d",curruntRow);
    
    [self.collDispQuestionNo reloadData];
    //[self.collDispQuestions reloadData];
    
    
}

-(void)moveToPreviousQuestion{
    
    NSLog(@"moveToPreviousQuestion");
    curruntRow--;
    
    if(curruntRow == 0){
        
        //_btnPrevious.enabled = false;
        
    }else{
        
        if(curruntRow < [ShareData.marrmcqquestion count] - 1 ){
            
            //_btnNext.enabled=true;
            
        }
    }
    
    NSIndexPath *nextItem = [NSIndexPath indexPathForItem:curruntRow inSection:0];
    
    [self.collDispQuestions scrollToItemAtIndexPath:nextItem atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
    [self.collDispQuestionNo scrollToItemAtIndexPath:nextItem atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
    
    [self changeQuestionNo:curruntRow];
    NSLog(@"curruntRow %d",curruntRow);
    
    [self.collDispQuestionNo reloadData];
    [self.collDispQuestions reloadData];
    
}

-(void)moveToQuestion:(NSIndexPath*)indexpath{
    
    NSLog(@"Indexpath : %ld",(long)indexpath.row);
    
    curruntRow = (int)indexpath.row;
    
    if (curruntRow > 0) {
        
        //_btnPrevious.enabled = true;
        
        if (curruntRow < [ShareData.marrmcqquestion count]-1) {
            
            //_btnNext.enabled = true;
            
        }else{
            
            //_btnNext.enabled = false;
        }
        
    }else{
        
        // _btnPrevious.enabled = false;
    }
    
    NSIndexPath *nextItem = [NSIndexPath indexPathForItem:curruntRow inSection:0];
    
    [self.collDispQuestions scrollToItemAtIndexPath:nextItem atScrollPosition:UICollectionViewScrollPositionNone animated:YES];
    [self.collDispQuestionNo scrollToItemAtIndexPath:nextItem atScrollPosition:UICollectionViewScrollPositionNone animated:NO];
    
    [self changeQuestionNo:curruntRow];
    
    [self.collDispQuestionNo reloadData];
    //[self.collDispQuestions reloadData];
}

- (void)changeQuestionNo:(int)questionNo{
    
    _lblDispQuestionNo.text = [NSString stringWithFormat:@"%d / %lu", questionNo + 1,(unsigned long)ShareData.marrmcqquestion.count];
    
}

#pragma mark - ANSWER Select_Unselect Method

-(void)selectedOptionColor:(UILabel*)label{
    
    label.layer.backgroundColor = [UIColor colorWithHex:0x075584].CGColor;
    label.layer.cornerRadius = label.layer.frame.size.height/2;
   // label.textColor=[UIColor whiteColor];
    
}

-(void)unSelectedOptionColor:(UILabel*)label1 :(UILabel*)label2 :(UILabel*)label3{
    
    label1.layer.backgroundColor = [UIColor clearColor].CGColor;
    label2.layer.backgroundColor = [UIColor clearColor].CGColor;
    label3.layer.backgroundColor = [UIColor clearColor].CGColor;
    
   /* label1.textColor=[UIColor blackColor];
    label2.textColor=[UIColor blackColor];
    label3.textColor=[UIColor blackColor];*/
}

-(void)unSelectedAllOptionColor:(UILabel*)label1 :(UILabel*)label2 :(UILabel*)label3 :(UILabel*)label4 {
    
    label1.layer.backgroundColor = [UIColor clearColor].CGColor;
    label2.layer.backgroundColor = [UIColor clearColor].CGColor;
    label3.layer.backgroundColor = [UIColor clearColor].CGColor;
    label4.layer.backgroundColor = [UIColor clearColor].CGColor;
    
    label1.textColor=[UIColor blackColor];
    label2.textColor=[UIColor blackColor];
    label3.textColor=[UIColor blackColor];
    label4.textColor=[UIColor blackColor];
}

-(void)selectedAnswer1:(UIButton *)sender{
    
    [self updatetemp_mcq_test_dtl:sender.tag selectedOption:[[[[ShareData.marrmcqoption objectAtIndex:sender.tag] objectAtIndex:0] valueForKey:@"MCQOPtionID"] intValue]];
    [self updatemarrmcqquestion:(int)sender.tag :1];
    
    
    
}
-(void)selectedAnswer2:(UIButton *)sender{
    
    [self updatetemp_mcq_test_dtl:sender.tag selectedOption:[[[[ShareData.marrmcqoption objectAtIndex:sender.tag] objectAtIndex:1] valueForKey:@"MCQOPtionID"] intValue]];
    [self updatemarrmcqquestion:(int)sender.tag :2];
    
    
    
    
}
-(void)selectedAnswer3:(UIButton *)sender{
    
    [self updatetemp_mcq_test_dtl:sender.tag selectedOption:[[[[ShareData.marrmcqoption objectAtIndex:sender.tag] objectAtIndex:2] valueForKey:@"MCQOPtionID"] intValue]];
    [self updatemarrmcqquestion:(int)sender.tag :3];
    
    
    
}

-(void)selectedAnswer4:(UIButton *)sender{
    
    [self updatetemp_mcq_test_dtl:sender.tag selectedOption:[[[[ShareData.marrmcqoption objectAtIndex:sender.tag] objectAtIndex:3] valueForKey:@"MCQOPtionID"] intValue]];
    [self updatemarrmcqquestion:(int)sender.tag :4];
    
    
    
    
    
    
}
-(void)selectedAnswer5:(UIButton *)sender{
    
    //[self updatetemp_mcq_test_dtl:sender.tag selectedOption:[[[[ShareData.marrmcqoption objectAtIndex:sender.tag] objectAtIndex:3] valueForKey:@"MCQOPtionID"] intValue]];
    //[self updatemarrmcqquestion:(int)sender.tag :4];
    
    
    
    if ([[marrFlag objectAtIndex:sender.tag] intValue]==0) {
        [marrFlag replaceObjectAtIndex:sender.tag withObject:@"1"];
    }
    else
    {
        [marrFlag replaceObjectAtIndex:sender.tag withObject:@"0"];
    }
    

    NSLog(@"---%@",marrFlag);
    
    [_collDispQuestionNo reloadData];
    [_collDispQuestions reloadData];
    
    
    
}




#pragma mark - GET_SET DATA from DataBase Method

-(void)getData{
    
    if ([_isRendom isEqualToString:@"1"]) {
        [ShareData.marrmcqquestion removeAllObjects];
        [self getmcqquestionTableData];
    }
    else if ([_inCorredt isEqualToString:@"1"] && [_isNotAppered isEqualToString:@"1"])
    {
        [ShareData.marrmcqquestion removeAllObjects];
        [self getmcqquestionInCorrectAndNotApperedTableData];
    }
    else if ([_inCorredt isEqualToString:@"1"])
    {
        [ShareData.marrmcqquestion removeAllObjects];
        [self getmcqquestionInCorrectTableData];
    }
    else if ([_isNotAppered isEqualToString:@"1"])
    {
        [ShareData.marrmcqquestion removeAllObjects];
        [self getmcqquestionNotApperedTableData];
    }
    
    
    
    _lbl_outoff.text=[NSString stringWithFormat:@"/%02lu",(unsigned long)[ShareData.marrmcqquestion count]];
    
    if ([ShareData.marrmcqquestion count] != 0 ) {
        
        NSMutableArray * marrOptions = [[NSMutableArray alloc]init];
        
        for (int i = 0; i < ShareData.marrmcqquestion.count; i++) {
            
            FMResultSet *results1 = [MyModel selectQuery:[NSString stringWithFormat:@"select * FROM temp_mcq_test_dtl where QuestionID = '%@'",[[ShareData.marrmcqquestion objectAtIndex:i] valueForKey:@"MCQQuestionID"]]];
            
            
            if ([results1 hasAnotherRow] ? true : false) {
                
                //   ------ UPDATE temp_mcq_test_dtl Data  ------
                [MyModel UpdateQuery:[NSString stringWithFormat:@"UPDATE temp_mcq_test_dtl SET AnswerID = '%@' , IsAttempt = '%@' WHERE QuestionID = '%@'",  @"" , @"0" , [[ShareData.marrmcqquestion objectAtIndex:i] valueForKey:@"MCQQuestionID"]]];
                
                
                //
                
                NSMutableDictionary *mutDic=[[NSMutableDictionary alloc] init];
                [mutDic setValue:@"Score_Valaue" forKey:@"AnswerID"];
                [mutDic setValue:@"Score_Valaue" forKey:@"IsAttempt"];
                [mutDic setValue:@"Score_Valaue" forKey:@"player2-Score"];
                
            }else{
                // ------- INSERT Into  temp_mcq_test_dtl Data  ------
                [MyModel UpdateQuery:[NSString stringWithFormat:@"Insert into temp_mcq_test_dtl(QuestionID,AnswerID,IsAttempt) values ('%@','%@','%@')",[[ShareData.marrmcqquestion objectAtIndex:i] valueForKey:@"MCQQuestionID"],@"",@"0"]];
            }

            //  ------- GET mcqoption Table Data  --------
            
            FMResultSet *results = [MyModel selectQuery:[NSString stringWithFormat:@"select * FROM mcqoption where MCQQuestionID = '%@'",[[ShareData.marrmcqquestion objectAtIndex:i] valueForKey:@"MCQQuestionID"]]];
            
            NSMutableArray * marr = [[NSMutableArray alloc]init];
            while ([results next])
            {
                
                NSMutableDictionary* mdict = [[NSMutableDictionary alloc] init];
                
                [mdict setValue:[results stringForColumn:@"MCQOPtionID"] forKey:@"MCQOPtionID"];
                [mdict setValue:[results stringForColumn:@"MCQQuestionID"] forKey:@"MCQQuestionID"];
                [mdict setValue:[results stringForColumn:@"Options"] forKey:@"Options"];
                [mdict setValue:[results stringForColumn:@"ImageURL"] forKey:@"ImageURL"];
                [mdict setValue:[results stringForColumn:@"OptionNo"] forKey:@"OptionNo"];
                [mdict setValue:[results stringForColumn:@"isCorrect"] forKey:@"isCorrect"];
                [mdict setValue:[results stringForColumn:@"CreatedBy"] forKey:@"CreatedBy"];
                [mdict setValue:[results stringForColumn:@"CreatedOn"] forKey:@"CreatedOn"];
                [mdict setValue:[results stringForColumn:@"ModifiedBy"] forKey:@"ModifiedBy"];
                [mdict setValue:[results stringForColumn:@"ModifiedOn"] forKey:@"ModifiedOn"];
                
                [marr addObject:mdict];
                
                
            }
            
            [marrOptions addObject: marr];
        }
        
        ShareData.marrmcqoption = [NSMutableArray arrayWithArray:marrOptions];
        NSLog(@"marrmcqoption : %@",ShareData.marrmcqoption);
        
        
        
        
    }else{
        
        UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"oops..!" message:@"No Questions Found ...!" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction * ok= [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            //         Dashboard * next = [self.storyboard instantiateViewControllerWithIdentifier:@"Dashboard"];
            
            //     [self.navigationController pushViewController:next animated:YES];
            
        }];
        
        
        [alert addAction:ok];
        
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    
    
}

- (void)getmcqquestionNotApperedTableData{
    NSUserDefaults *standerId=[[NSUserDefaults alloc]init];
    _chapterList = [_chapterList stringByReplacingOccurrencesOfString:@"," withString:@"','"];
    
    FMResultSet *results = [MyModel selectQuery:[NSString stringWithFormat:@"select * FROM mcqquestion where  BoardID = '%@' AND MediumID = '%@' AND StandardID = '%@' AND SubjectID = '%@' AND QuestionLevelID IN (%@) AND ChapterID IN ('%@') and MCQQuestionID in(select QuestionID from student_not_appeared_question) ORDER BY Random() LIMIT 20",[standerId valueForKeyPath:@"ParseData.BoardID"],[standerId valueForKeyPath:@"ParseData.MediumID"],[standerId valueForKeyPath:@
                                                                                                                                                                                                                                                                                                                                                                                                                                                           "ParseData.StandardID"],_subjectId,_levelId,_chapterList ]];
    
    
    NSMutableArray * marr = [[NSMutableArray alloc]init];

    while ([results next])
    {
        NSMutableDictionary* mdict = [[NSMutableDictionary alloc] init];
        [mdict setValue:[results stringForColumn:@"MCQQuestionID"] forKey:@"MCQQuestionID"];
        [mdict setValue:[results stringForColumn:@"Question"] forKey:@"Question"];
        [mdict setValue:[results stringForColumn:@"ImageURL"] forKey:@"ImageURL"];
        [mdict setValue:[results stringForColumn:@"BoardID"] forKey:@"BoardID"];
        [mdict setValue:[results stringForColumn:@"MediumID"] forKey:@"MediumID"];
        [mdict setValue:[results stringForColumn:@"ClassID"] forKey:@"ClassID"];
        [mdict setValue:[results stringForColumn:@"StandardID"] forKey:@"StandardID"];
        [mdict setValue:[results stringForColumn:@"SubjectID"] forKey:@"SubjectID"];
        [mdict setValue:[results stringForColumn:@"ChapterID"] forKey:@"ChapterID"];
        [mdict setValue:[results stringForColumn:@"QuestionTypeID"] forKey:@"QuestionTypeID"];
        [mdict setValue:[results stringForColumn:@"QuestionLevelID"] forKey:@"QuestionLevelID"];
        [mdict setValue:[results stringForColumn:@"Duration"] forKey:@"Duration"];
        [mdict setValue:[results stringForColumn:@"CreatedBy"] forKey:@"CreatedBy"];
        [mdict setValue:[results stringForColumn:@"CreatedOn"] forKey:@"CreatedOn"];
        [mdict setValue:[results stringForColumn:@"ModifiedBy"] forKey:@"ModifiedBy"];
        [mdict setValue:[results stringForColumn:@"ModifiedOn"] forKey:@"ModifiedOn"];
        [mdict setValue:[NSString stringWithFormat:@"%d",false] forKey:@"isRight"];
        [mdict setValue:[NSString stringWithFormat:@"%s","0"] forKey:@"selectedAns"];
        [mdict setValue:[NSString stringWithFormat:@"%s","0"] forKey:@"isAttempted"];
        [marr addObject:mdict];
        
    }
    
    if (marr.count>0) {
        ShareData.marrmcqquestion = [NSMutableArray arrayWithArray:marr];
        NSLog(@"marrmcqquestion : %@" ,ShareData.marrmcqquestion);
    }
    else
    {
        [self dismissViewControllerAnimated:NO completion:nil];
        
    }
    
}

- (void)getmcqquestionInCorrectTableData{
    NSUserDefaults *standerId=[[NSUserDefaults alloc]init];
    _chapterList = [_chapterList stringByReplacingOccurrencesOfString:@"," withString:@"','"];
    
    FMResultSet *results = [MyModel selectQuery:[NSString stringWithFormat:@"select * FROM mcqquestion where  BoardID = '%@' AND MediumID = '%@' AND StandardID = '%@' AND SubjectID = '%@' AND QuestionLevelID IN (%@) AND ChapterID IN ('%@') and MCQQuestionID in(select QuestionID from student_incorrect_question) ORDER BY Random() LIMIT 20",[standerId valueForKeyPath:@"ParseData.BoardID"],[standerId valueForKeyPath:@"ParseData.MediumID"],[standerId valueForKeyPath:@
                                                                                                                                                                                                                                                                                                                                                                                                                                                        "ParseData.StandardID"],_subjectId,_levelId,_chapterList ]];
    
    
    
    NSMutableArray * marr = [[NSMutableArray alloc]init];
    
    while ([results next])
    {
        NSMutableDictionary* mdict = [[NSMutableDictionary alloc] init];
        
        [mdict setValue:[results stringForColumn:@"MCQQuestionID"] forKey:@"MCQQuestionID"];
        [mdict setValue:[results stringForColumn:@"Question"] forKey:@"Question"];
        [mdict setValue:[results stringForColumn:@"ImageURL"] forKey:@"ImageURL"];
        [mdict setValue:[results stringForColumn:@"BoardID"] forKey:@"BoardID"];
        [mdict setValue:[results stringForColumn:@"MediumID"] forKey:@"MediumID"];
        [mdict setValue:[results stringForColumn:@"ClassID"] forKey:@"ClassID"];
        [mdict setValue:[results stringForColumn:@"StandardID"] forKey:@"StandardID"];
        [mdict setValue:[results stringForColumn:@"SubjectID"] forKey:@"SubjectID"];
        [mdict setValue:[results stringForColumn:@"ChapterID"] forKey:@"ChapterID"];
        [mdict setValue:[results stringForColumn:@"QuestionTypeID"] forKey:@"QuestionTypeID"];
        [mdict setValue:[results stringForColumn:@"QuestionLevelID"] forKey:@"QuestionLevelID"];
        [mdict setValue:[results stringForColumn:@"Duration"] forKey:@"Duration"];
        [mdict setValue:[results stringForColumn:@"CreatedBy"] forKey:@"CreatedBy"];
        [mdict setValue:[results stringForColumn:@"CreatedOn"] forKey:@"CreatedOn"];
        [mdict setValue:[results stringForColumn:@"ModifiedBy"] forKey:@"ModifiedBy"];
        [mdict setValue:[results stringForColumn:@"ModifiedOn"] forKey:@"ModifiedOn"];
        [mdict setValue:[NSString stringWithFormat:@"%d",false] forKey:@"isRight"];
        [mdict setValue:[NSString stringWithFormat:@"%s","0"] forKey:@"selectedAns"];
        [mdict setValue:[NSString stringWithFormat:@"%s","0"] forKey:@"isAttempted"];
        
        [marr addObject:mdict];
        
    }
    
    if (marr.count>0) {
        ShareData.marrmcqquestion = [NSMutableArray arrayWithArray:marr];
        NSLog(@"marrmcqquestion : %@" ,ShareData.marrmcqquestion);
    }
    else
    {
        [self dismissViewControllerAnimated:NO completion:nil];
        
    }
    
}

- (void)getmcqquestionInCorrectAndNotApperedTableData{
    NSUserDefaults *standerId=[[NSUserDefaults alloc]init];
    _chapterList = [_chapterList stringByReplacingOccurrencesOfString:@"," withString:@"','"];
    
    FMResultSet *results = [MyModel selectQuery:[NSString stringWithFormat:@"select * FROM mcqquestion where  BoardID = '%@' AND MediumID = '%@' AND StandardID = '%@' AND SubjectID = '%@' AND QuestionLevelID IN (%@) AND ChapterID IN ('%@') and  ((MCQQuestionID in (select QuestionID from student_incorrect_question))OR (MCQQuestionID in (select QuestionID from student_not_appeared_question))) ORDER BY Random() LIMIT 20",[standerId valueForKeyPath:@"ParseData.BoardID"],[standerId valueForKeyPath:@"ParseData.MediumID"],[standerId valueForKeyPath:@
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          "ParseData.StandardID"],_subjectId,_levelId,_chapterList ]];
    
    
    
    NSMutableArray * marr = [[NSMutableArray alloc]init];
    
    while ([results next])
    {
        NSMutableDictionary* mdict = [[NSMutableDictionary alloc] init];
        
        [mdict setValue:[results stringForColumn:@"MCQQuestionID"] forKey:@"MCQQuestionID"];
        [mdict setValue:[results stringForColumn:@"Question"] forKey:@"Question"];
        [mdict setValue:[results stringForColumn:@"ImageURL"] forKey:@"ImageURL"];
        [mdict setValue:[results stringForColumn:@"BoardID"] forKey:@"BoardID"];
        [mdict setValue:[results stringForColumn:@"MediumID"] forKey:@"MediumID"];
        [mdict setValue:[results stringForColumn:@"ClassID"] forKey:@"ClassID"];
        [mdict setValue:[results stringForColumn:@"StandardID"] forKey:@"StandardID"];
        [mdict setValue:[results stringForColumn:@"SubjectID"] forKey:@"SubjectID"];
        [mdict setValue:[results stringForColumn:@"ChapterID"] forKey:@"ChapterID"];
        [mdict setValue:[results stringForColumn:@"QuestionTypeID"] forKey:@"QuestionTypeID"];
        [mdict setValue:[results stringForColumn:@"QuestionLevelID"] forKey:@"QuestionLevelID"];
        [mdict setValue:[results stringForColumn:@"Duration"] forKey:@"Duration"];
        [mdict setValue:[results stringForColumn:@"CreatedBy"] forKey:@"CreatedBy"];
        [mdict setValue:[results stringForColumn:@"CreatedOn"] forKey:@"CreatedOn"];
        [mdict setValue:[results stringForColumn:@"ModifiedBy"] forKey:@"ModifiedBy"];
        [mdict setValue:[results stringForColumn:@"ModifiedOn"] forKey:@"ModifiedOn"];
        [mdict setValue:[NSString stringWithFormat:@"%d",false] forKey:@"isRight"];
        [mdict setValue:[NSString stringWithFormat:@"%s","0"] forKey:@"selectedAns"];
        [mdict setValue:[NSString stringWithFormat:@"%s","0"] forKey:@"isAttempted"];
        
        [marr addObject:mdict];
        
    }
    
    if (marr.count>0) {
        ShareData.marrmcqquestion = [NSMutableArray arrayWithArray:marr];
        NSLog(@"marrmcqquestion : %@" ,ShareData.marrmcqquestion);
    }
    else
    {
        [self dismissViewControllerAnimated:NO completion:nil];
        
    }
    
}



- (void)getmcqquestionTableData{
    NSUserDefaults *standerId=[[NSUserDefaults alloc]init];
    _chapterList = [_chapterList stringByReplacingOccurrencesOfString:@"," withString:@"','"];
    
    FMResultSet *results = [MyModel selectQuery:[NSString stringWithFormat:@"select * FROM mcqquestion where  BoardID = '%@' AND MediumID = '%@' AND StandardID = '%@' AND SubjectID = '%@' AND QuestionLevelID IN (%@) AND ChapterID IN ('%@') ORDER BY Random() LIMIT 20",[standerId valueForKeyPath:@"ParseData.BoardID"],[standerId valueForKeyPath:@"ParseData.MediumID"],[standerId valueForKeyPath:@
                                                                                                                                                                                                                                                                                                                                                                                "ParseData.StandardID"],_subjectId,_levelId,_chapterList ]];
    
    
    
    NSMutableArray * marr = [[NSMutableArray alloc]init];
    
    while ([results next])
    {
        NSMutableDictionary* mdict = [[NSMutableDictionary alloc] init];
        
        [mdict setValue:[results stringForColumn:@"MCQQuestionID"] forKey:@"MCQQuestionID"];
        [mdict setValue:[results stringForColumn:@"Question"] forKey:@"Question"];
        [mdict setValue:[results stringForColumn:@"ImageURL"] forKey:@"ImageURL"];
        [mdict setValue:[results stringForColumn:@"BoardID"] forKey:@"BoardID"];
        [mdict setValue:[results stringForColumn:@"MediumID"] forKey:@"MediumID"];
        [mdict setValue:[results stringForColumn:@"ClassID"] forKey:@"ClassID"];
        [mdict setValue:[results stringForColumn:@"StandardID"] forKey:@"StandardID"];
        [mdict setValue:[results stringForColumn:@"SubjectID"] forKey:@"SubjectID"];
        [mdict setValue:[results stringForColumn:@"ChapterID"] forKey:@"ChapterID"];
        [mdict setValue:[results stringForColumn:@"QuestionTypeID"] forKey:@"QuestionTypeID"];
        [mdict setValue:[results stringForColumn:@"QuestionLevelID"] forKey:@"QuestionLevelID"];
        [mdict setValue:[results stringForColumn:@"Duration"] forKey:@"Duration"];
        [mdict setValue:[results stringForColumn:@"CreatedBy"] forKey:@"CreatedBy"];
        [mdict setValue:[results stringForColumn:@"CreatedOn"] forKey:@"CreatedOn"];
        [mdict setValue:[results stringForColumn:@"ModifiedBy"] forKey:@"ModifiedBy"];
        [mdict setValue:[results stringForColumn:@"ModifiedOn"] forKey:@"ModifiedOn"];
        [mdict setValue:[NSString stringWithFormat:@"%d",false] forKey:@"isRight"];
        [mdict setValue:[NSString stringWithFormat:@"%s","0"] forKey:@"selectedAns"];
        [mdict setValue:[NSString stringWithFormat:@"%s","0"] forKey:@"isAttempted"];
        
        [marr addObject:mdict];
        
    }
    
    if (marr.count>0) {
        ShareData.marrmcqquestion = [NSMutableArray arrayWithArray:marr];
        NSLog(@"marrmcqquestion : %@" ,ShareData.marrmcqquestion);
    }
    else
    {
        [self dismissViewControllerAnimated:NO completion:nil];
        
    }
    
}

-(void)updatetemp_mcq_test_dtl:(NSInteger )index selectedOption:(int)answerID{
    
    [MyModel UpdateQuery:[NSString stringWithFormat:@"UPDATE temp_mcq_test_dtl SET AnswerID = '%d' , IsAttempt = '%d' WHERE QuestionID = '%@'",answerID,1,[[ShareData.marrmcqquestion objectAtIndex:index] valueForKey:@"MCQQuestionID"]]];
    
    [self changeSelection:index];
    [_collDispQuestions reloadData];
    
}

-(void)insertIntostudent_mcq_test_hdrTable{
    
    int lastID = 0;
    FMResultSet * result = [MyModel selectQuery:[NSString stringWithFormat:@"SELECT  StudentMCQTestHDRID FROM student_mcq_test_hdr  ORDER BY StudentMCQTestHDRID DESC Limit 1"]];
    
    while ([result next]) {
        lastID = [[result stringForColumn:@"StudentMCQTestHDRID"] intValue];
        NSLog(@"lastid %d",lastID);
    }
    NSLog(@"lastid %d",lastID);
    
    NSUserDefaults *stander=[[NSUserDefaults alloc]init];
    NSMutableDictionary *mutDic=[[NSMutableDictionary alloc] init];
    [mutDic setValue:[NSString stringWithFormat:@"%d",++lastID] forKey:@"StudentMCQTestHDRID"];
    [mutDic setValue:[stander valueForKeyPath:@"ParseData.StudentID"] forKey:@"StudentID"];
    [mutDic setValue:[stander valueForKeyPath:@"ParseData.BoardID"] forKey:@"BoardID"];
    [mutDic setValue:[stander valueForKeyPath:@"ParseData.MediumID"] forKey:@"MediumID"];
    [mutDic setValue:[NSString stringWithFormat:@"%@",_subjectId] forKey:@"SubjectID"];
    [mutDic setValue:[NSString stringWithFormat:@"%@",_levelId] forKey:@"LevelID"];
    [mutDic setValue:@"" forKey:@"Timeleft"];
    [mutDic setValue:@"" forKey:@"CreatedBy"];
    //[mutDic setValue:[MyModel getCuruntDateTime] forKey:@"CreatedOn"];
    [mutDic setValue:[APP_DELEGATE getCurrentDate] forKey:@"CreatedOn"];//
    [mutDic setValue:@"" forKey:@"ModifiedBy"];
    [mutDic setValue:@"" forKey:@"ModifiedOn"];
    
    [MyModel insertInto_papertype:mutDic :@"student_mcq_test_hdr"];
    
    
    
}


-(void)gettemp_mcq_test_dtl{
    
    FMResultSet *results = [MyModel selectQuery:[NSString stringWithFormat:@"select * FROM temp_mcq_test_dtl ORDER BY TempMCQTestDTLID ASC"]];
    
    NSMutableArray* marr = [[NSMutableArray alloc]init];
    
    while ([results next])
    {
        NSMutableDictionary* mdict = [[NSMutableDictionary alloc] init];
        
        [mdict setValue:[results stringForColumn:@"QuestionID"] forKey:@"QuestionID"];
        [mdict setValue:[results stringForColumn:@"AnswerID"] forKey:@"AnswerID"];
        [mdict setValue:[results stringForColumn:@"IsAttempt"] forKey:@"IsAttempt"];
        
        [marr addObject:mdict];
        
    }
    ShareData.marrtemp_mcq_test_dtl = [NSMutableArray arrayWithArray:marr];
    NSLog(@"marrtemp_mcq_test_dtl : %@" ,ShareData.marrtemp_mcq_test_dtl);
    
}

-(void)insertIntostudent_mcq_test_dtlTable{
    
    lastHeaderID = -1;
    FMResultSet * result = [MyModel selectQuery:[NSString stringWithFormat:@"SELECT  StudentMCQTestHDRID FROM student_mcq_test_hdr  ORDER BY StudentMCQTestHDRID DESC Limit 1"]];
    
    
    while ([result next]) {
        
        lastHeaderID = [[result stringForColumn:@"StudentMCQTestHDRID"] intValue];
        NSLog(@"lastHeaderID %d",lastHeaderID);
    }
    
    for (int i = 0; i < ShareData.marrtemp_mcq_test_dtl.count; i++) {
        
        NSMutableDictionary *mutDic=[[NSMutableDictionary alloc] initWithCapacity:2];
        [mutDic setValue:[NSString stringWithFormat:@"%d",lastHeaderID] forKey:@"StudentMCQTestHDRID"];
        [mutDic setValue:[[ShareData.marrtemp_mcq_test_dtl objectAtIndex:i] valueForKey:@"QuestionID"] forKey:@"QuestionID"];
        [mutDic setValue:[[ShareData.marrtemp_mcq_test_dtl objectAtIndex:i] valueForKey:@"AnswerID"] forKey:@"AnswerID"];
        [mutDic setValue:[[ShareData.marrtemp_mcq_test_dtl objectAtIndex:i] valueForKey:@"IsAttempt"] forKey:@"IsAttempt"];
        [mutDic setValue:@"" forKey:@"srNo"];
        [mutDic setValue:@"" forKey:@"CreatedBy"];
        [mutDic setValue:[MyModel getCuruntDateTime] forKey:@"CreatedOn"];
        [mutDic setValue:@"" forKey:@"ModifiedBy"];
        [mutDic setValue:@"" forKey:@"ModifiedOn"];
        
        [MyModel insertInto_papertype:mutDic :@"student_mcq_test_dtl"];
        
    }
    
}

-(void)gettemp_mcq_test_chapter{
    
    FMResultSet *results = [MyModel selectQuery:[NSString stringWithFormat:@"select * FROM temp_mcq_test_chapter"]];
    
    NSMutableArray* marr = [[NSMutableArray alloc]init];
    
    while ([results next])
    {
        NSMutableDictionary* mdict = [[NSMutableDictionary alloc] init];
        
        [mdict setValue:[results stringForColumn:@"TempMCQTestHDRID"] forKey:@"TempMCQTestHDRID"];
        [mdict setValue:[results stringForColumn:@"ChapterID"] forKey:@"ChapterID"];
        
        [marr addObject:mdict];
        
    }
    
    ShareData.marrtemp_mcq_test_chapter = [NSMutableArray arrayWithArray:marr];
    
    NSLog(@"marrtemp_mcq_test_chapter : %@" ,ShareData.marrtemp_mcq_test_chapter);
    
    
    
}

-(void)insertIntostudent_mcq_test_chapterTable{
    
 /*   NSLog(@"lastHeaderID %d",lastHeaderID);
    for (int i = 0; i < ShareData.marrtemp_mcq_test_chapter.count; i++) {
        NSMutableDictionary *mutDic=[[NSMutableDictionary alloc] initWithCapacity:2];
        [mutDic setValue:[NSString stringWithFormat:@"%d",lastHeaderID] forKey:@"StudentMCQTestHDRID"];
        [mutDic setValue:[[ShareData.marrtemp_mcq_test_chapter objectAtIndex:i] valueForKey:@"ChapterID"] forKey:@"ChapterID"];
        [MyModel insertInto_papertype:mutDic :@"student_mcq_test_chapter"];
        
    }
   */
    
    
    NSLog(@"lastHeaderID %d",lastHeaderID);
    for (int i = 0; i < arrChapter.count; i++) {
        NSMutableDictionary *mutDic=[[NSMutableDictionary alloc] init];
        [mutDic setValue:[NSString stringWithFormat:@"%d",lastHeaderID] forKey:@"StudentMCQTestHDRID"];
        [mutDic setValue:[arrChapter objectAtIndex:i] forKey:@"ChapterID"];
        [MyModel insertInto_papertype:mutDic :@"student_mcq_test_chapter"];
        
    }
    
}

-(void)insertIntostudent_mcq_test_levelTable{
    
    NSMutableArray *arrlevels=[[NSMutableArray alloc]init];
    arrlevels = [_levelId componentsSeparatedByString:@","];
    
    
    NSLog(@"lastHeaderID %d",lastHeaderID);
    for (int i = 0; i < arrlevels.count; i++) {
        NSMutableDictionary *mutDic=[[NSMutableDictionary alloc] initWithCapacity:2];
        [mutDic setValue:[NSString stringWithFormat:@"%d",lastHeaderID] forKey:@"StudentMCQTestHDRID"];
        [mutDic setValue:[arrlevels objectAtIndex:i] forKey:@"LevelID"];
        [MyModel insertInto_papertype:mutDic :@"student_mcq_test_level"];
        
    }
    
}


-(void)updatemarrmcqquestion:(int )index :(int)selectedOption{
    
    
    //NSLog(@"isCorrect : %@",[[[ShareData.marrmcqoption objectAtIndex:index] objectAtIndex:selectedOption-1] valueForKey:@"isCorrect"]);
    if ([[[[ShareData.marrmcqoption objectAtIndex:index] objectAtIndex:selectedOption-1] valueForKey:@"isCorrect"] isEqual:@"1"]) {
        
        [[ShareData.marrmcqquestion objectAtIndex:index]  setValue:[NSString stringWithFormat:@"%d",true] forKey:@"isRight"];
        
    }else{
        
        [[ShareData.marrmcqquestion objectAtIndex:index]  setValue:[NSString stringWithFormat:@"%d",false] forKey:@"isRight"];
        
    }
    
    [[ShareData.marrmcqquestion objectAtIndex:index]  setValue:[NSString stringWithFormat:@"%d",selectedOption] forKey:@"selectedAns"];
    [[ShareData.marrmcqquestion objectAtIndex:index]  setValue:[NSString stringWithFormat:@"%d",1] forKey:@"isAttempted"];
    
    
    [self changeSelection:index];
    
    [totAttempt replaceObjectAtIndex:index withObject:@"1"];
    int j=0;
    for (int i=0; i<totAttempt.count; i++) {
        if ([[totAttempt objectAtIndex:i] intValue]==1) {
            j++;
        }
    }
    _lbl_attemp.text=[NSString stringWithFormat:@"%02d",j];
    
    
    
    NSLog(@"marrmcqquestion :%@ ",[ShareData.marrmcqquestion objectAtIndex:index]);
}

-(void)insertIntonotificationTable{
    
    NSDate * now = [NSDate date];
    
    NSDateFormatter *DateFormator = [[NSDateFormatter alloc] init];
    [DateFormator setDateFormat:@"dd-MM-yyyy HH:mm:ss"];
    NSString *strdate = [DateFormator stringFromDate:now];
    
    NSArray *components = [strdate componentsSeparatedByString:@" "];
    NSString *date = components[0];
    NSString *time = components[1];
    
    NSLog(@"time %@", time);
    NSLog(@"date %@", date);
    
    [MyModel UpdateQuery:[NSString stringWithFormat:@"Insert into notification(StudentKey,Title,Message,CreatedOn) values ('%@','%@','%@','%@')",[UserDefault valueForKey:@"reg_key"],@"Your Child activity on iScore.. !",[NSString stringWithFormat:@"Your Child attempted a MCQ test.  Subject: %@ AT: Time (%@) / Date (%@)",_subjectName,time,date],strdate]];
    
}


-(void)getnotificationTableData{
    
    FMResultSet *results = [MyModel selectQuery:[NSString stringWithFormat:@"select * FROM notification"]];
    
    NSMutableArray* marr = [[NSMutableArray alloc]init];
    
    while ([results next])
    {
        NSMutableDictionary* mdict = [[NSMutableDictionary alloc] init];
        
        [mdict setValue:[results stringForColumn:@"ID"] forKey:@"id"];
        [mdict setValue:[results stringForColumn:@"StudentKey"] forKey:@"StudentKey"];
        [mdict setValue:[results stringForColumn:@"Title"] forKey:@"Title"];
        [mdict setValue:[results stringForColumn:@"Message"] forKey:@"Message"];
        [mdict setValue:[results stringForColumn:@"CreatedOn"] forKey:@"CreatedOn"];
        
        [marr addObject:mdict];
        
    }
    
    ShareData.marrnotification = [NSMutableArray arrayWithArray:marr];
    
    NSLog(@"marrnotification : %@" ,ShareData.marrnotification);
    
    NSData *jsonData2 = [NSJSONSerialization dataWithJSONObject:ShareData.marrnotification options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData2 encoding:NSUTF8StringEncoding];
    NSLog(@"jsonData as string:%@", jsonString);
    
    NSString * url = [NSString stringWithFormat:@"%@/web-services/notification?",Main_URL];
    
    NSMutableDictionary * param = [[NSMutableDictionary alloc]init];
    
    [param setObject:jsonString forKey:@"data"];
    [param setObject:@"" forKey:@"show"];
    
    [API PostUrl:url Parameters:param andCallback:^(NSMutableDictionary *result, NSError *error)  {
        
        if (error==nil) {
            
            NSLog(@"Response:-%@",result);
            
            if ([result valueForKey:@"status"]) {
                NSString * ID = [result valueForKey:@"data"];
                NSLog(@"ID : %@",ID);
                
                [MyModel UpdateQuery:[NSString stringWithFormat:@"DELETE FROM notification where id IN(%@)",ID]];
                
            }
            
        }else{
            
            NSLog(@"Error : %@ ",error);
            
        }
    }];
    
}

-(void)getstudent_mcq_test_hdrTableData:(NSString *)url{
    
    FMResultSet *results = [MyModel selectQuery:url];
    
    NSMutableArray* marr = [[NSMutableArray alloc]init];
    
    
    while ([results next])
    {
        NSMutableDictionary* mdict = [[NSMutableDictionary alloc] init];
        
        [mdict setValue:[results stringForColumn:@"StudentMCQTestHDRID"] forKey:@"StudentMCQTestHDRID"];
        [mdict setValue:[results stringForColumn:@"StudentID"] forKey:@"StudentID"];
        [mdict setValue:[results stringForColumn:@"BoardID"] forKey:@"BoardID"];
        [mdict setValue:[results stringForColumn:@"MediumID"] forKey:@"MediumID"];
        [mdict setValue:[results stringForColumn:@"SubjectID"] forKey:@"SubjectID"];
        [mdict setValue:[results stringForColumn:@"LevelID"] forKey:@"LevelID"];
        [mdict setValue:[results stringForColumn:@"Timeleft"] forKey:@"Timeleft"];
        [mdict setValue:[results stringForColumn:@"AddType"] forKey:@"AddType"];
        [mdict setValue:[results stringForColumn:@"CreatedBy"] forKey:@"CreatedBy"];
        [mdict setValue:[results stringForColumn:@"CreatedOn"] forKey:@"CreatedOn"];
        [mdict setValue:[results stringForColumn:@"ModifiedBy"] forKey:@"ModifiedBy"];
        [mdict setValue:[results stringForColumn:@"ModifiedOn"] forKey:@"ModifiedOn"];
        [mdict setObject:@"" forKey:@"chapterArray"];
        [mdict setObject:@"" forKey:@"detailArray"];
        
        [marr addObject:mdict];
        
    }
    
    ShareData.marrstudent_mcq_test_hdr = [NSMutableArray arrayWithArray:marr];
    
    NSLog(@"marrstudent_mcq_test_hdr : %@" ,ShareData.marrstudent_mcq_test_hdr);
    
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch(buttonIndex) {
        case 0:
        {
            /*TestReportVC *viewController=[[TestReportVC alloc]initWithNibName:@"TestReportVC" bundle:nil];
             [self.navigationController pushViewController:viewController animated:YES];
             */
            
            [self btnSubmit];
            
            NSLog(@"lbl_count %@",_lbl_countdown);
            
            TestReportVC *add = [[TestReportVC alloc] initWithNibName:@"TestReportVC" bundle:nil];
            add.tempNotapper=tempNotapper;
            add.tempIncorrect=tempIncorrect;
            add.tempCorrect=tempCorrect;
            add.tempNotapperOp=tempNotapperOp;
            add.tempIncorrectOp=tempIncorrectOp;
            add.tempCorrectOp=tempCorrectOp;

            if ([_TBStatus intValue]==1)
            {
               add.TakanTime=lbltotTaken.text;
                
                NSLog(@"tot taken time %f",[totTaken getTimeCounted]);
                
                float totsec=[totTaken getTimeCounted];
               
                NSString *strtemp = [NSString stringWithFormat:@"%@",_lbl_outoff.text];
                strtemp = [strtemp substringFromIndex:1];
                totsec = totsec/[strtemp floatValue];
                NSLog(@"totsec %f",totsec);
                int result = (int)totsec;
                int minutes = totsec / 60;
                int seconds = result % 60;
                
                add.avgTakanTime=[NSString stringWithFormat:@"%02d:%02d",minutes,seconds];
                
                
                
                
                
            }
            else
            {
                add.TakanTime=@"00:00";
                add.avgTakanTime=@"00:00";
            }
            
                           
            add.chapterList=_chapterList;
            add.TBStatus=_TBStatus;
            add.subjectId=_subjectId;
            add.subjectName=_subjectName;
            add.levelId=_levelId;
            add.mcqQuestions=ShareData.marrmcqquestion;
            add.mcqOption=ShareData.marrmcqoption;
            //[self presentViewController:add animated:YES completion:nil];
            [self.navigationController pushViewController:add animated:YES];
            
            break;
        }
        case 1:
        {
            [totTaken start];
            [timer start];
        }
            break;
    }
}


- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}



- (IBAction)btn_SUBMIT:(id)sender {
    
    [totTaken pause];
    [timer pause];
    
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                    message:@"Do you want to submit the test?"
                                                   delegate:self
                                          cancelButtonTitle:@"YES"
                                          otherButtonTitles:@"NO", nil];
    [alert show];
}

- (IBAction)btn_BACK_A:(id)sender {
    
    [timer pause];
    [totTaken pause];
    [cell.mztime pause];
    
    
    
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"Alert"
                                 message:@"Do you want to leave the test?"
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    //Add Buttons
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"YES"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
                                    //Handle your yes please button action here
                                    [self TruncateTempTeble];
                                    [self.navigationController popViewControllerAnimated:YES];
                                    
                                }];
    
    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle:@"Cancel"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   //Handle no, thanks button
                                    [timer start];
                                   [totTaken pause];
                                   [cell.mztime start];
                               }];
    
    //Add your buttons to alert controller
    
    [alert addAction:yesButton];
    [alert addAction:noButton];
    
    [self presentViewController:alert animated:YES completion:nil];
    
    
}

- (IBAction)btn_HOME_A:(id)sender {
    
     [timer pause];
     [totTaken pause];
    
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"Alert"
                                 message:@"Are you sure you want to visit Deshboard?"
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    //Add Buttons
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"YES"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
                                    //Handle your yes please button action here
                                    [self TruncateTempTeble];
                                    
                                    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                                             bundle: nil];
                                    HomeVC *view = [mainStoryboard instantiateViewControllerWithIdentifier:@"HomeVC"];
                                    [self.navigationController pushViewController:view animated:YES];

                                    
                                }];
    
    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle:@"Cancel"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   //Handle no, thanks button
                                   [timer start];
                                   [totTaken start];
                               }];
    
    //Add your buttons to alert controller
    
    [alert addAction:yesButton];
    [alert addAction:noButton];
    
    [self presentViewController:alert animated:YES completion:nil];
    
}

-(void)TruncateTempTeble
{
    [MyModel UpdateQuery:[NSString stringWithFormat:@"DELETE FROM temp_mcq_test_chapter"]];
    [MyModel UpdateQuery:[NSString stringWithFormat:@"DELETE FROM temp_mcq_test_dtl"]];
    [MyModel UpdateQuery:[NSString stringWithFormat:@"DELETE FROM temp_mcq_test_hdr"]];
    [MyModel UpdateQuery:[NSString stringWithFormat:@"DELETE FROM temp_mcq_test_level"]];
}


@end
