//
//  StartTestVC.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 3/13/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StartTestVC : UIViewController



@property (strong, nonatomic)NSString *strLevels;
@property (retain , nonatomic)NSString *chapterlist;
@property (retain , nonatomic)NSString *SubjectID;
@property (retain , nonatomic)NSString *SubjectName;

@property (retain , nonatomic)NSString *isRendom;
@property (retain , nonatomic)NSString *isNotAppered;
@property (retain , nonatomic)NSString *inCorredt;



//Outlet
@property (strong, nonatomic) IBOutlet UIButton *btn_next;
@property (strong, nonatomic) IBOutlet UIButton *btn_toggle;





//Action
- (IBAction)btn_NEXT_A:(id)sender;
- (IBAction)btn_BACK:(id)sender;
- (IBAction)btn_TOGGLE_A:(id)sender;



@end
