//
//  SLevelCell.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 3/10/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SLevelCell : UITableViewCell

//Outlet
@property (strong, nonatomic) IBOutlet UIView *vw_innervw;
@property (strong, nonatomic) IBOutlet UIImageView *img_chkbox;
@property (strong, nonatomic) IBOutlet UILabel *lbl_level;
@property (strong, nonatomic) IBOutlet UILabel *lbl_queslel;

@property (strong, nonatomic) IBOutlet UIButton *btn_chkbox;
@property (strong, nonatomic) IBOutlet UILabel *lbl_correct;
@property (strong, nonatomic) IBOutlet UILabel *lbl_accu;
@property (strong, nonatomic) IBOutlet UIProgressView *pro_level;




//Action



@end
