//
//  SelectLevelVC.m
//  iScoreApp
//
//  Created by ADMIN-Khushal on 3/10/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "SelectLevelVC.h"
#import "SLevelCell.h"
#import "UIColor+CL.h"
#import "StartTestVC.h"
#import "MyModel.h"
#import "MyDBQueries.h"
#import "ChapterListModel.h"
#import "UIColor+CL.h"
#import "Chapters.h"
#import "SelectLevelVC.h"


@interface SelectLevelVC ()

@end

@implementation SelectLevelVC

- (void)viewDidLoad {
    [super viewDidLoad];
    myDbQueris =[[MyDBQueries alloc]init];
    myDBParser=[[MyDBResultParser alloc]init];
    
    [_tbl_slevel registerNib:[UINib nibWithNibName:@"SLevelCell" bundle:nil] forCellReuseIdentifier:@"Cell"];
    
    arrBLevel=[[NSMutableArray alloc]init];
    
    for (int i=0; i<3; i++) {
        [arrBLevel addObject:@"0"];
    }
    [self CallIncorrect];
    [self CallIncorrect1];
    [self CallLevel];
    [self CallAccuracy];
    
    NSLog(@"Chapter List  %@",_chapterList);
    NSLog(@"SubjectID %@",_SubjectID);
    NSLog(@"_subjectName %@",_subjectName);
    NSLog(@"_subjectIcon %@",_subjectIcon);
    
    
    //_vw_random.userInteractionEnabled=NO;
    //_vw_random.alpha=0.6;
    
    //_vw_incorrect.userInteractionEnabled=NO;
    //_vw_incorrect.alpha=0.6;
    
    //_vw_not_appeare.userInteractionEnabled=NO;
    //_vw_not_appeare.alpha=0.6;
    
    //===========================
    
    NSFileManager *fileManager = [[NSFileManager alloc] init];
    
    UIImage *theImage=[UIImage imageWithContentsOfFile:_subjectIcon];
    _img_sub_icon.image=theImage;
    _lbl_subnm.text=[NSString stringWithFormat:@"%@",_subjectName];
    //NSLog(@"%@",[[levelAccuracy objectAtIndex:0] accuracy]);
    
    if (!(levelAccuracy.count==0)) {
        if(([[levelAccuracy objectAtIndex:0] accuracy] == NULL || [[levelAccuracy objectAtIndex:0] accuracy] == nil || [[[levelAccuracy objectAtIndex:0] accuracy] isEqual:@""])){
            
            NSLog(@"%@",[[levelAccuracy objectAtIndex:0] accuracy]);
            _lbl_sub_accu.text=@"Accuracy 0%";
        }
        else
        {
            _lbl_sub_accu.text=[NSString stringWithFormat:@"Accuracy %@%%",[[levelAccuracy objectAtIndex:0] accuracy]];
        }
    }
    
    
    
    ////////PROGRESSBAR
    
    [_pro_accuracy.layer setCornerRadius:_pro_accuracy.frame.size.height/2];
    _pro_accuracy.clipsToBounds = true;
    [_pro_accuracy setTransform:CGAffineTransformMakeScale(1.0f, 3.0f)];
    
    if (!(levelAccuracy.count==0))
        _pro_accuracy.progress =  [[[levelAccuracy objectAtIndex:0] accuracy] floatValue]/100.0;
    
    _pro_accuracy.trackTintColor = [UIColor colorWithHex:0xDDDDDD];
    _pro_accuracy.progressTintColor = [UIColor colorWithHex:0xEF9430];
    
    //===========================
    
    _img_round.clipsToBounds=YES;
    _img_round.layer.cornerRadius=_img_round.frame.size.height/2;
    _img_round.layer.borderWidth=3.0;
    _img_round.layer.borderColor=[UIColor colorWithHex:0x075584].CGColor;
    
    
    arrlevel = [[NSMutableArray alloc]initWithObjects:@"LEVEL l",@"LEVEL ll",@"LEVEL lll", nil];
    arrleveldesc = [[NSMutableArray alloc]initWithObjects:@"Easy Level Questions ",@"Moderate Level Questions",@"Difficult Level Questions", nil];
    
    
    UIBezierPath *maskPath;
    maskPath = [UIBezierPath bezierPathWithRoundedRect:_vw_inner.bounds byRoundingCorners:(UIRectCornerBottomRight | UIRectCornerTopRight) cornerRadii:CGSizeMake(10.0, 10.0)];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.path = maskPath.CGPath;
    _vw_inner.layer.mask = maskLayer;
    
    
    CGRect mainframe1 = _vw_footer.frame;
    mainframe1.size.height = _vw_footer.frame.size.height+50;
    _vw_footer.frame = mainframe1;
    
    _tbl_slevel.tableHeaderView = _vw_header;
    _tbl_slevel.tableFooterView=_vw_footer;
    
    _btn_next.layer.cornerRadius=5;
    _img_header.alpha=0.0;
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    isScrollingStart=YES;
    NSLog(@"scrollViewDidScroll  %f , %f",scrollView.contentOffset.x,scrollView.contentOffset.y);
    
    if (scrollView.contentOffset.y<=135) {
        _img_header.alpha=scrollView.contentOffset.y/135;
    }
    else
    {
        _img_header.alpha=1.0;
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 35.0;
}
/*
 -(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
 {
 return @"SELECT SINGLE OR MULTIPLE LEVELS";
 }*/
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UILabel *myLabel = [[UILabel alloc] init];
    myLabel.frame = CGRectMake(8, 5, 320, 35);
    
    myLabel.font = [UIFont fontWithName:@"Roboto-Bold" size:14.0f];
    //myLabel.text = [self tableView:tableView titleForHeaderInSection:section];
    myLabel.text = @"SELECT SINGLE OR MULTIPLE LEVELS";
    [myLabel setTextColor:[UIColor colorWithHex:0x075584]];
    UIView *headerView = [[UIView alloc] init];
    [headerView addSubview:myLabel];
    
    return headerView;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrlevel.count;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"Cell";
    
    SLevelCell *cell = (SLevelCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:self options:nil];
        cell = (SLevelCell *)[nib objectAtIndex:0];
    }
    
    cell.lbl_level.text=[NSString stringWithFormat:@"%@",arrlevel[indexPath.row]];
    cell.lbl_queslel.text=[NSString stringWithFormat:@"(%@)",arrleveldesc[indexPath.row]];
    
    cell.btn_chkbox.tag = indexPath.row;
    [cell.btn_chkbox addTarget:self action:@selector(onDoneButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    
    if ([[arrBLevel objectAtIndex:indexPath.row] intValue]==0) {
        cell.btn_chkbox.selected=NO;
    }
    else
    {
        cell.btn_chkbox.selected=YES;
    }
    
    //Total Appeared Question 9 | Correct 0
    
    if (!(levelAccuracy.count==0)){
        cell.lbl_correct.text=[NSString stringWithFormat:@"Total Appeared Question %@ | Correct %@",[[levelArray objectAtIndex:indexPath.row] Total_attempt_level],[[levelArray objectAtIndex:indexPath.row] Right_Answer_level]];
        
        cell.lbl_accu.text=[NSString stringWithFormat:@"Accuracy %@%%",[[levelArray objectAtIndex:indexPath.row] accuracy]];
        
        ////////PROGRESSBAR
        
        [cell.pro_level.layer setCornerRadius:_pro_accuracy.frame.size.height/2];
        cell.pro_level.clipsToBounds = true;
        [cell.pro_level setTransform:CGAffineTransformMakeScale(1.0f, 3.0f)];
        
        if (!(levelArray.count==0) && !([[levelArray objectAtIndex:indexPath.row] accuracy] == [NSNull null] || [[levelArray objectAtIndex:indexPath.row] accuracy] == nil || [[[levelArray objectAtIndex:indexPath.row] accuracy] isEqual:@""]))
        {
            cell.pro_level.progress =  [[[levelArray objectAtIndex:indexPath.row] accuracy] floatValue]/100.0;
            
        }else
        {
            cell.lbl_accu.text=@"Accuracy 0%";
        }
        cell.pro_level.trackTintColor = [UIColor colorWithHex:0xDDDDDD];
        cell.pro_level.progressTintColor = [UIColor colorWithHex:0xEF9430];
    }
    
    NSMutableAttributedString *coloredText = [[NSMutableAttributedString alloc] initWithString:cell.lbl_correct.text];
    
    [coloredText addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHex:0xEF9430] range:NSMakeRange([coloredText length]-2,2)];
    cell.lbl_correct.attributedText=coloredText;
    
    /*
    //NSString *myString = @"I have to replace text 'Dr Andrew Murphy, John Smith' ";
    NSString *myString = cell.lbl_correct.text;
    
    //Create mutable string from original one
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:myString];
    
    //Fing range of the string you want to change colour
    //If you need to change colour in more that one place just repeat it
    NSRange range = [myString substringFromIndex:[myString length]-1];
    [attString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:(63/255.0) green:(163/255.0) blue:(158/255.0) alpha:1.0] range:range];
    
    //Add it to the label - notice its not text property but it's attributeText
    _label.attributedText = attString;
    
    */
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(void)onDoneButtonPressed:(UIButton*)sender
{
    
    NSLog(@"Check box click %ld",(long)sender.tag);
    UIButton *btn = (UIButton *)sender;
    CGPoint origin = btn.frame.origin;
    CGPoint point = [btn.superview convertPoint:origin toView:_tbl_slevel];
    NSIndexPath * indexPath = [_tbl_slevel indexPathForRowAtPoint:point];
    SLevelCell *cell=(SLevelCell *)[_tbl_slevel cellForRowAtIndexPath:indexPath];
    
    if (cell.btn_chkbox.selected==YES) {
        //cell.btn_chkbox.selected=NO;
        [arrBLevel replaceObjectAtIndex:indexPath.row withObject:@"0"];
    }
    else
    {
        [arrBLevel replaceObjectAtIndex:indexPath.row withObject:@"1"];
    }
    [_tbl_slevel reloadData];
    
}
-(void)CallAccuracy
{
    levelAccuracy=[[NSMutableArray alloc]init];
    FMResultSet *rs = [MyModel selectQuery:[myDbQueris getAccuracy:_chapterList subjectId:_SubjectID]];
    while ([rs next]) {
        LevelAccuracy *SubModel = [[LevelAccuracy alloc] init];
        [levelAccuracy  addObject:[myDBParser parseDBResult:rs :SubModel]];
        NSLog(@"--%@",levelAccuracy);
    }
}


-(void)CallLevel
{
    levelArray=[[NSMutableArray alloc]init];
    
    for (int i=0; i<3; i++) {
        NSString *levelID=[NSString stringWithFormat:@"%d",i+1];
        
        FMResultSet *rs = [MyModel selectQuery:[myDbQueris getLevels:_chapterList level:levelID subjectid:_SubjectID]];
        
        while ([rs next]) {
            LevelModel *SubModel = [[LevelModel alloc] init];
            [levelArray  addObject:[myDBParser parseDBResult:rs :SubModel]];
            NSLog(@"levelArray--%@",levelArray);
        }
    }
}


-(void)CallIncorrect
{
    incurrectArr=[[NSMutableArray alloc]init];
    
    FMResultSet *rs = [MyModel selectQuery:[myDbQueris inCurrect:_chapterList subjectId:_SubjectID boardId:[[NSUserDefaults standardUserDefaults]valueForKeyPath:@"ParseData.BoardID"] mediumId:[[NSUserDefaults standardUserDefaults]valueForKeyPath:@"ParseData.MediumID"] standerdId:[[NSUserDefaults standardUserDefaults]valueForKeyPath:@"ParseData.StandardID"]]];
    
    while ([rs next]) {
        LevelModel *SubModel = [[LevelModel alloc] init];
        [incurrectArr  addObject:[myDBParser parseDBResult:rs :SubModel]];
        NSLog(@"CallIncorrect-- %@",incurrectArr);
    }
    
    if (incurrectArr.count>0) {
        //  _vw_incorrect.userInteractionEnabled=NO;
        // _vw_incorrect.alpha=0.6;
        
        _vw_not_appeare.userInteractionEnabled=YES;
        _vw_not_appeare.alpha=1;
    }
    else
    {
        _vw_not_appeare.userInteractionEnabled=NO;
        _vw_not_appeare.alpha=0.6;
    }
    
    
    
}
-(void)CallIncorrect1
{
    incurrectArr1=[[NSMutableArray alloc]init];
    
    FMResultSet *rs = [MyModel selectQuery:[myDbQueris inCurrect1:_chapterList subjectId:_SubjectID boardId:[[NSUserDefaults standardUserDefaults]valueForKeyPath:@"ParseData.BoardID"] mediumId:[[NSUserDefaults standardUserDefaults]valueForKeyPath:@"ParseData.MediumID"] standerdId:[[NSUserDefaults standardUserDefaults]valueForKeyPath:@"ParseData.StandardID"]]];
    
    while ([rs next]) {
        LevelModel *SubModel = [[LevelModel alloc] init];
        [incurrectArr1  addObject:[myDBParser parseDBResult:rs :SubModel]];
        NSLog(@"CallIncorrect-- %@",incurrectArr1);
    }
    
    if (incurrectArr1.count>0) {
        _vw_incorrect.userInteractionEnabled=YES;
        _vw_incorrect.alpha=1;
    }
    else
    {
        _vw_incorrect.userInteractionEnabled=NO;
        _vw_incorrect.alpha=0.6;
    }
    
    
    
}



- (IBAction)btn_BACK:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
    
    // UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
    //                                                           bundle: nil];
    // Chapters *view = [mainStoryboard instantiateViewControllerWithIdentifier:@"Chapters"];
    //  [self.navigationController pushViewController:view animated:YES];
    
    
}

- (IBAction)btn_RAND:(id)sender {
    if (_btn_random.isSelected) {
        _btn_random.selected=NO;
    }
    else
    {
        _btn_random.selected=YES;
        _btn_incorrect.selected=NO;
        _btn_appeared.selected=NO;
    }
}

- (IBAction)btn_INCORE:(id)sender {
    if (_btn_incorrect.isSelected) {
        _btn_incorrect.selected=NO;
    }
    else
    {
        _btn_incorrect.selected=YES;
        _btn_random.selected=NO;
    }
}

- (IBAction)btn_NOTAPPE:(id)sender {
    if (_btn_appeared.isSelected) {
        _btn_appeared.selected=NO;
    }
    else
    {
        _btn_appeared.selected=YES;
        _btn_random.selected=NO;
    }
}

-(void)SelectedLevel
{
    NSMutableArray *temp=[[NSMutableArray alloc]init];
    levels=[[NSString alloc]init];
    
    for (int i=0; i<arrBLevel.count; i++) {
        if ([[arrBLevel objectAtIndex:i] intValue]==1) {
            [temp addObject:[NSString stringWithFormat:@"%d",i+1]];
        }
        
    }
    
    /*  if (_btn_random.isSelected){
     [temp addObject:[NSString stringWithFormat:@"4"]];
     }
     
     if (_btn_incorrect.isSelected)
     {
     [temp addObject:[NSString stringWithFormat:@"5"]];
     }
     
     if (_btn_appeared.isSelected)
     {
     [temp addObject:[NSString stringWithFormat:@"6"]];
     }
     
     */
    
    
    if (temp.count>0) {
        levels=[self convertToCommaSeparatedFromArray:temp];
        NSLog(@"Levels   %@",levels);
    }
    else
    {
        levels=nil;
    }
    
    
    
}

-(NSString *)convertToCommaSeparatedFromArray:(NSArray*)array{
    return [array componentsJoinedByString:@","];
}

- (IBAction)btn_NEXT_A:(id)sender {
    [self SelectedLevel];
    
    if (levels==nil) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                        message:@"Please Select Level."
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }else{
        if (_btn_random.isSelected || _btn_appeared.isSelected || _btn_incorrect.isSelected) {
            
            StartTestVC *add = [[StartTestVC alloc] initWithNibName:@"StartTestVC" bundle:nil];
            add.strLevels=levels;
            add.chapterlist=_chapterList;
            add.SubjectID=_SubjectID;
            add.SubjectName=_subjectName;
            
            if (_btn_random.isSelected){
                add.isRendom=@"1";
            }else{
                add.isRendom=@"0";
            }
            
            if (_btn_incorrect.isSelected){
                add.inCorredt=@"1";
            }else{
                add.inCorredt=@"0";
            }
            
            if (_btn_appeared.isSelected){
                add.isNotAppered=@"1";
            }else{
                add.isNotAppered=@"0";
            }
            
            
            
            [self.navigationController pushViewController:add animated:YES];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                            message:@"Please Select type."
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }
        
    }
    
    
    
    
}
@end
