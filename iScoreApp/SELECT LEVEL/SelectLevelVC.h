//
//  SelectLevelVC.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 3/10/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QuestionTypeTVC.h"
#import "TVCDispChapters.h"
#import "MyDBQueries.h"
#import "MyDBResultParser.h"
#import "LevelModel.h"
#import "LevelAccuracy.h"


@interface SelectLevelVC : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
    MyDBQueries *myDbQueris;
    MyDBResultParser *myDBParser;
    NSMutableArray *levelArray,*incurrectArr,*incurrectArr1;
    NSMutableArray *levelAccuracy;
    
    
    NSMutableArray *arrlevel;
    NSMutableArray *arrleveldesc;
    BOOL isScrollingStart;
    
    NSMutableArray *arrBLevel;
    NSString *levels;
    NSMutableArray *marrlevel;
    
}

//Outlet
@property (retain , nonatomic) NSString *chapterList;
@property (retain , nonatomic) NSString *SubjectID;
@property (retain , nonatomic) NSString *subjectName;
@property (retain , nonatomic) NSString *subjectIcon;


@property (strong, nonatomic) IBOutlet UIView *vw_header;
@property (strong, nonatomic) IBOutlet UIView *vw_inner;
@property (strong, nonatomic) IBOutlet UIImageView *img_round;
@property (strong, nonatomic) IBOutlet UIImageView *img_sub_icon;
@property (strong, nonatomic) IBOutlet UILabel *lbl_subnm;
@property (strong, nonatomic) IBOutlet UITableView *tbl_slevel;

@property (strong, nonatomic) IBOutlet UIView *vw_footer;

@property (strong, nonatomic) IBOutlet UIButton *btn_random;
@property (strong, nonatomic) IBOutlet UIButton *btn_incorrect;
@property (strong, nonatomic) IBOutlet UIButton *btn_appeared;
@property (strong, nonatomic) IBOutlet UIButton *btn_next;
@property (strong, nonatomic) IBOutlet UIImageView *img_header;

@property (strong, nonatomic) IBOutlet UIView *vw_random;
@property (strong, nonatomic) IBOutlet UIView *vw_incorrect;
@property (strong, nonatomic) IBOutlet UIView *vw_not_appeare;
@property (strong, nonatomic) IBOutlet UILabel *lbl_sub_accu;
@property (strong, nonatomic) IBOutlet UIProgressView *pro_accuracy;




//Action
- (IBAction)btn_BACK:(id)sender;
- (IBAction)btn_RAND:(id)sender;
- (IBAction)btn_INCORE:(id)sender;
- (IBAction)btn_NOTAPPE:(id)sender;
- (IBAction)btn_NEXT_A:(id)sender;


@end
