//
//  SLevelCell.m
//  iScoreApp
//
//  Created by ADMIN-Khushal on 3/10/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "SLevelCell.h"

@implementation SLevelCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    //_vw_innervw.layer.borderWidth=1;
    //_vw_innervw.layer.borderColor=[UIColor lightGrayColor].CGColor;
    
    _vw_innervw.layer.shadowRadius  = 2.2f;
    _vw_innervw.layer.shadowColor   = [UIColor colorWithRed:176.f/255.f green:199.f/255.f blue:226.f/255.f alpha:1.f].CGColor;
    _vw_innervw.layer.shadowOffset  = CGSizeMake(0.0f, 0.0f);
    _vw_innervw.layer.shadowOpacity = 0.7f;
    _vw_innervw.layer.masksToBounds = NO;
    
    UIEdgeInsets shadowInsets     = UIEdgeInsetsMake(0, 0, -1.5f, 0);
    UIBezierPath *shadowPath      = [UIBezierPath bezierPathWithRect:UIEdgeInsetsInsetRect(_vw_innervw.bounds, shadowInsets)];
    _vw_innervw.layer.shadowPath    = shadowPath.CGPath;
    
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
