//
//  CVCDispQuestions.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 4/26/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MZTimerLabel.h"
#import <WebKit/WebKit.h>

@interface CVCDispQuestions : UICollectionViewCell

@property ( retain ,nonatomic)MZTimerLabel *mztime;

@property (strong, nonatomic) IBOutlet UIView *viewDispQuestion;

@property (strong, nonatomic) IBOutlet UITextView *txtVQuestion;
@property (strong, nonatomic) IBOutlet WKWebView *webVQuestion;
@property (strong, nonatomic) IBOutlet UILabel *lbl_queNo;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollVQuestion;
@property (strong, nonatomic) IBOutlet UILabel *lbl_quetimer;

@property (strong, nonatomic) IBOutlet UIView *vw_question_view;


////Extra View
@property (strong, nonatomic) IBOutlet UIView *vw_op1;




@property (weak, nonatomic) IBOutlet NSLayoutConstraint *webvQuestionoHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewQuestionHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *txtVQuestionHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewOption1Height;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewOption2Height;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewOption3Height;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewOption4Height;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *txtVOption1Height;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *txtVOption2Height;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *txtVOption3Height;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *txtVOption4Height;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *webVOption1Height;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *webVOption2Height;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *webVOption3Height;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *webVOption4Height;


@property (strong, nonatomic) IBOutlet UIView *viewOption1;
@property (strong, nonatomic) IBOutlet UIView *viewOption2;
@property (strong, nonatomic) IBOutlet UIView *viewOption3;
@property (strong, nonatomic) IBOutlet UIView *viewOption4;



@property (weak, nonatomic) IBOutlet UILabel *lblA;
@property (weak, nonatomic) IBOutlet UILabel *lblB;
@property (weak, nonatomic) IBOutlet UILabel *lblC;
@property (weak, nonatomic) IBOutlet UILabel *lblD;


@property (weak, nonatomic) IBOutlet UITextView *txtVOption1;
@property (weak, nonatomic) IBOutlet UITextView *txtVOption2;
@property (weak, nonatomic) IBOutlet UITextView *txtVOption3;
@property (weak, nonatomic) IBOutlet UITextView *txtVOption4;
@property (weak, nonatomic) IBOutlet WKWebView *webVOption1;
@property (weak, nonatomic) IBOutlet WKWebView *webVOption2;
@property (weak, nonatomic) IBOutlet WKWebView *webVOption3;
@property (weak, nonatomic) IBOutlet WKWebView *webVOption4;


@property (weak, nonatomic) IBOutlet UIButton *btnOption1;
@property (weak, nonatomic) IBOutlet UIButton *bntOption2;
@property (weak, nonatomic) IBOutlet UIButton *btnOption3;
@property (weak, nonatomic) IBOutlet UIButton *btnOption4;
@property (strong, nonatomic) IBOutlet UIButton *btn_flag;

- (IBAction)btnOption1:(id)sender;
- (IBAction)btnOption2:(id)sender;
- (IBAction)btnOption3:(id)sender;
- (IBAction)btnOption4:(id)sender;
- (IBAction)btn_FLAG_A:(id)sender;







@end
