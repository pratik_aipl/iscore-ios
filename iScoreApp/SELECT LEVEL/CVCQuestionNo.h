//
//  CVCQuestionNo.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 4/26/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CVCQuestionNo : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblQuestionNo;
@property (weak, nonatomic) IBOutlet UILabel *lblSelection;
@property (strong, nonatomic) IBOutlet UIButton *btn_flag1;



@end
