//
//  CVCDispQuestions.m
//  iScoreApp
//
//  Created by ADMIN-Khushal on 4/26/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "CVCDispQuestions.h"
#import "MZTimerLabel.h"

@implementation CVCDispQuestions

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setBGColor:_webVQuestion];
    [self setBGColor:_webVOption1];
    [self setBGColor:_webVOption2];
    [self setBGColor:_webVOption3];
    [self setBGColor:_webVOption4];
    
    
    _lblA.layer.cornerRadius=_lblA.frame.size.height/2;
    _lblB.layer.cornerRadius=_lblB.frame.size.height/2;
    _lblC.layer.cornerRadius=_lblC.frame.size.height/2;
    _lblD.layer.cornerRadius=_lblD.frame.size.height/2;
    
    
    //_mztime=[[MZTimerLabel alloc]initWithLabel:_lbl_quetimer andTimerType:MZTimerLabelTypeStopWatch];
    //[_mztime start];
    
    
//    [_webVQuestion setScalesPageToFit:YES];
//    [_webVOption1 setScalesPageToFit:YES];
//    [_webVOption2 setScalesPageToFit:YES];
//    [_webVOption3 setScalesPageToFit:YES];
//    [_webVOption4 setScalesPageToFit:YES];
    
    
    
    //////===================
    
    [self shadowView:_txtVOption1 :_webVOption1];
    [self shadowView:_txtVOption2 :_webVOption2];
    [self shadowView:_txtVOption3 :_webVOption3];
    [self shadowView:_txtVOption4 :_webVOption4];
    
   
    
    
  /*  _vw_op1.layer.shadowRadius  = 2.2f;
    _vw_op1.layer.shadowColor   = [UIColor colorWithRed:176.f/255.f green:199.f/255.f blue:226.f/255.f alpha:1.f].CGColor;
    _vw_op1.layer.shadowOffset  = CGSizeMake(0.0f, 0.0f);
    _vw_op1.layer.shadowOpacity = 0.7f;
    _vw_op1.layer.masksToBounds = NO;
    
    UIEdgeInsets shadowInsets     = UIEdgeInsetsMake(0, 0, -1.5f, 0);
    UIBezierPath *shadowPath      = [UIBezierPath bezierPathWithRect:UIEdgeInsetsInsetRect(_vw_op1.bounds, shadowInsets)];
    _vw_op1.layer.shadowPath    = shadowPath.CGPath;
   */
    
}

-(void)shadowView :(UITextView*)textview :(WKWebView*)webview
{
    
    textview.layer.borderColor = [UIColor lightGrayColor].CGColor;
    textview.layer.borderWidth = 0.7f;
    
    webview.layer.borderColor = [UIColor lightGrayColor].CGColor;
    webview.layer.borderWidth = 0.7f;
    
   
   
    [self setShadow:_txtVOption1];
    [self setShadow:_txtVOption2];
    [self setShadow:_txtVOption3];
    [self setShadow:_txtVOption4];
    
    
    
    
}

-(void)setShadow :(UITextView*)txtV
{
    
    
    CGFloat topCorrect = ([txtV bounds].size.height - [txtV     contentSize].height * [txtV zoomScale])/2.0;
    topCorrect = ( topCorrect < 0.0 ? 0.0 : topCorrect );
    [txtV setContentInset:UIEdgeInsetsMake(topCorrect,0,0,0)];
    
    
    txtV.layer.shadowRadius  = 1.5f;
    txtV.layer.shadowColor   = [UIColor lightGrayColor].CGColor;
    txtV.layer.shadowOffset  = CGSizeMake(0.0f, 0.0f);
    txtV.layer.shadowOpacity = 0.9f;
    txtV.layer.masksToBounds = NO;
    
    UIEdgeInsets shadowInsets     = UIEdgeInsetsMake(0, 0, -1.5f, 0);
    UIBezierPath *shadowPath      = [UIBezierPath bezierPathWithRect:UIEdgeInsetsInsetRect(txtV.bounds, shadowInsets)];
    txtV.layer.shadowPath    = shadowPath.CGPath;
}


-(void)setBGColor:(WKWebView*)Webview{
    
    //Webview.delegate = self;
    //Webview.scrollView.scrollEnabled = NO;
    //Webview.scrollView.bounces = NO;
    [Webview setBackgroundColor:[UIColor clearColor]];
    [Webview setOpaque:NO];
    
}
- (IBAction)btnOption1:(id)sender {
    
    [self selectedOptionColor:_lblA ];
    [self unSelectedOptionColor:_lblB :_lblC  :_lblD  ];
    
}

- (IBAction)btnOption2:(id)sender {
    
    [self selectedOptionColor:_lblB];
    [self unSelectedOptionColor:_lblA :_lblC  :_lblD ];
    
}

- (IBAction)btnOption3:(id)sender {
    
    [self selectedOptionColor:_lblC];
    [self unSelectedOptionColor:_lblA :_lblB  :_lblD ];
    
}

- (IBAction)btnOption4:(id)sender {
    
    [self selectedOptionColor:_lblD ];
    [self unSelectedOptionColor:_lblA :_lblB  :_lblC];
    
}

- (IBAction)btn_FLAG_A:(id)sender {
}


-(void)selectedOptionColor:(UILabel*)label{
    
    label.layer.backgroundColor = [UIColor redColor].CGColor;
    //label.layer.cornerRadius = 2 ;
    
    /*
     textView.layer.borderWidth = 1;
     textView.layer.borderColor = [UIColor redColor].CGColor;
     textView.layer.cornerRadius = 2 ;*/
    
}

-(void)unSelectedOptionColor:(UILabel*)label1 :(UILabel*)label2 :(UILabel*)label3  {
    
    label1.layer.backgroundColor = [UIColor clearColor].CGColor;
    label2.layer.backgroundColor = [UIColor clearColor].CGColor;
    label3.layer.backgroundColor = [UIColor clearColor].CGColor;
    /*
     textView1.layer.borderColor = [UIColor clearColor].CGColor;
     textView2.layer.borderColor = [UIColor clearColor].CGColor;
     textView3.layer.borderColor = [UIColor clearColor].CGColor;*/
}
@end

