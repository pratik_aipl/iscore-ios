//
//  StartTestVC.m
//  iScoreApp
//
//  Created by ADMIN-Khushal on 3/13/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "StartTestVC.h"
#import "StartMCQTest.h"
#import "SelectLevelVC.h"



@interface StartTestVC ()

@end

@implementation StartTestVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    _btn_next.layer.cornerRadius=3;
    
    
    
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btn_NEXT_A:(id)sender {
    
  /*  MCQTestVC *viewController=[[MCQTestVC alloc]initWithNibName:@"MCQTestVC" bundle:nil];
    [self.navigationController pushViewController:viewController animated:YES];
   */
    
    StartMCQTest *add = [[StartMCQTest alloc] initWithNibName:@"StartMCQTest" bundle:nil];
    add.subjectId=_SubjectID;
    add.chapterList=_chapterlist;
    add.levelId=_strLevels;
    add.subjectName=_SubjectName;
    add.isRendom=_isRendom;
    add.isNotAppered=_isNotAppered;
    add.inCorredt=_inCorredt;
    
    
    if (_btn_toggle.isSelected) {
        add.TBStatus=@"1";
    }
    else
    {
        add.TBStatus=@"0";
    }
    //[self presentViewController:add animated:YES completion:nil];
    [self.navigationController pushViewController:add animated:YES];
    
}

- (IBAction)btn_BACK:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
   // SelectLevelVC *add = [[SelectLevelVC alloc] initWithNibName:@"SelectLevelVC" bundle:nil];
   // [self.navigationController pushViewController:add animated:YES];
    
}

- (IBAction)btn_TOGGLE_A:(id)sender {
    
    if (_btn_toggle.selected==YES) {
        _btn_toggle.selected=NO;
    }
    else
    {
        _btn_toggle.selected=YES;
    }
    
}
@end
