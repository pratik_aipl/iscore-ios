//
//  MenuListViewCell.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 3/2/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuListViewCell : UITableViewCell

//Outlets
@property (strong, nonatomic) IBOutlet UIImageView *img_icon;
@property (strong, nonatomic) IBOutlet UILabel *lbl_title;



//Action



@end
