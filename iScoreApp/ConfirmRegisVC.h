//
//  ConfirmRegisVC.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 2/28/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ConfirmRegisVC : UIViewController{
    NSString * str;
    NSMutableDictionary *ParseData;
    
    BOOL isInternet;
}


@property (retain , nonatomic)NSMutableDictionary* dictdata;
//Outlet
@property (strong, nonatomic) IBOutlet UIImageView *img_profimage;

@property (strong, nonatomic) IBOutlet UILabel *lbl_usernm;
@property (strong, nonatomic) IBOutlet UIView *vw_details;
@property (strong, nonatomic) IBOutlet UILabel *lbl_email;
@property (strong, nonatomic) IBOutlet UILabel *lbl_mobile;
@property (strong, nonatomic) IBOutlet UILabel *lbl_board;

@property (strong, nonatomic) IBOutlet UILabel *lbl_medium;
@property (strong, nonatomic) IBOutlet UILabel *lbl_standard;
@property (strong, nonatomic) IBOutlet UIButton *btn_confirm;



//Action
- (IBAction)btn_BACK:(id)sender;
- (IBAction)btn_CONFIRM_A:(id)sender;


@end
