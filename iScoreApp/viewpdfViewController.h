//
//  viewpdfViewController.h
//  iScoreApp
//
//  Created by My Mac on 09/05/20.
//  Copyright © 2020 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface viewpdfViewController : UIViewController
@property (strong, nonatomic) NSDictionary *dictdata;
@property (strong, nonatomic) NSString *htmlstring;
@end

NS_ASSUME_NONNULL_END
