//
//  AppDelegate.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 2/23/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyModel.h"
#import "SharedData.h"
#import "Constant.h"
#import <OneSignal/OneSignal.h>
#import "SWRevealViewController.h"
@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property(nonatomic,strong)     SharedData  *sharedData;
@property(nonatomic,strong)     AppDelegate *appDelegate;
@property (strong, nonatomic) SWRevealViewController *viewController;
-(void)showLoadingView:(NSString *)strMSG;
-(void)hideLoadingView;

-(void)AlertForMSG:(NSString *)msg;
-(NSString*)getCurrentDate;
-(NSString*)getDateInFormat :(NSString*)strdt;
+(BOOL) isInternetConnected;

@end


/*
 ////////////////STORY BOARD To STORY BOARD//////////////////
 
 OTPVC * next = [self.storyboard instantiateViewControllerWithIdentifier:@"OTPVC"];
 [self.navigationController pushViewController:next animated:YES];
 
 
 /////////////////XIB To XIB /////////////////////////////////
 StartMCQTest *add = [[StartMCQTest alloc] initWithNibName:@"StartMCQTest" bundle:nil];    [self.navigationController pushViewController:add animated:YES];
 
 
 ///////////////XIB To STORY BOARD///////////////////////////////////
 UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
 bundle: nil];
 SubjectVC *view = [mainStoryboard instantiateViewControllerWithIdentifier:@"SubjectVC"];
 [self.navigationController pushViewController:view animated:YES];
 
 
 
 #import "UzysImageCropperViewController.h"
 
 @property (nonatomic,retain) UzysImageCropperViewController *imgCropperViewController;
 @property (nonatomic,retain) UIImagePickerController *imgpicker;
 
 
 _imgCropperViewController = [[UzysImageCropperViewController alloc] initWithImage:tempImage andframeSize:_imgpicker.view.frame.size andcropSize:CGSizeMake(1180, 420)];
 _imgCropperViewController.delegate = self;
 [_imgpicker presentViewController:_imgCropperViewController animated:YES completion:nil];
 [_imgCropperViewController release];
 
 - (void)imageCropper:(UzysImageCropperViewController *)cropper didFinishCroppingWithImage:(UIImage *)image
 {
 if (flag==1) {
 ISimage=image;
 
 
 
 _img_chooseimg.image=image;
 _img_extra.hidden=YES;
 _lbl_extra_lab.hidden=YES;
 
 [self CallAddPortfolio];
 }
 [self dismissViewControllerAnimated:YES completion:nil];
 }
 
 - (void)imageCropperDidCancel:(UzysImageCropperViewController *)cropper
 {
 [self dismissViewControllerAnimated:YES completion:nil];
 // [_picker dismissViewControllerAnimated:YES completion:nil];
 }

 
 
 */

