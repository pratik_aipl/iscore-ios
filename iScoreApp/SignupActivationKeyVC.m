//
//  SignupActivationKeyVC.m
//  iScoreApp
//
//  Created by ADMIN-Khushal on 2/23/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "SignupActivationKeyVC.h"
#import "UIColor+CL.h"
#import "FreeRegisterVC.h"
#import "Reachability.h"




#import "ApplicationConst.h"
#import <AFNetworking/AFNetworking.h>

#define REGEX_PHONE_DEFAULT @"[0-9]{12}"

@interface SignupActivationKeyVC ()

-(void)reachabilityChanged:(NSNotification*)note;

@property(strong) Reachability * googleReach;
@property(strong) Reachability * localWiFiReach;
@property(strong) Reachability * internetConnectionReach;

@end

@implementation SignupActivationKeyVC


- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden=YES;
    // Do any additional setup after loading the view.
   // [self setupAlerts];
    
    
    _vw_activkey.layer.borderWidth=1.0;
    _vw_activkey.layer.borderColor=[UIColor colorWithHex:0x0A385A].CGColor;
    _vw_activkey.layer.cornerRadius=5;
    
    _vwuserid.layer.borderWidth=1.0;
       _vwuserid.layer.borderColor=[UIColor colorWithHex:0x0A385A].CGColor;
       _vwuserid.layer.cornerRadius=5;
   
    _btn_next_b.layer.cornerRadius=3;
    _btn_free_b.layer.cornerRadius=3;
    
    
    
    //Mobile Number
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.vw_mob_icon.bounds byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerBottomLeft) cornerRadii:CGSizeMake(5.0, 5.0)];
    
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.view.bounds;
    maskLayer.path  = maskPath.CGPath;
    self.vw_mob_icon.layer.mask = maskLayer;
    
    _dictdata=[[NSMutableDictionary alloc]init];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reachabilityChanged:)
                                                 name:kReachabilityChangedNotification
                                               object:nil];
    
    
    __weak __block typeof(self) weakself = self;
    
    
    [_txt_akey addTarget:self action:@selector(textFieldDidChange1:) forControlEvents:UIControlEventEditingChanged];
    
    
    //////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////
    //
    // create a Reachability object for www.google.com
    
    self.googleReach = [Reachability reachabilityWithHostname:@"www.google.com"];
    
    self.googleReach.reachableBlock = ^(Reachability * reachability)
    {
        NSString * temp = [NSString stringWithFormat:@"GOOGLE Block Says Reachable(%@)", reachability.currentReachabilityString];
        NSLog(@"%@", temp);
        
        // to update UI components from a block callback
        // you need to dipatch this to the main thread
        // this uses NSOperationQueue mainQueue
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            //weakself.blockLabel.text = temp;
            //weakself.blockLabel.textColor = [UIColor blackColor];
            isInternet=YES;
        }];
    };
    
    self.googleReach.unreachableBlock = ^(Reachability * reachability)
    {
        NSString * temp = [NSString stringWithFormat:@"GOOGLE Block Says Unreachable(%@)", reachability.currentReachabilityString];
        NSLog(@"%@", temp);
        
        // to update UI components from a block callback
        // you need to dipatch this to the main thread
        // this one uses dispatch_async they do the same thing (as above)
        dispatch_async(dispatch_get_main_queue(), ^{
            //weakself.blockLabel.text = temp;
            //weakself.blockLabel.textColor = [UIColor redColor];
            isInternet=NO;
        });
    };
    
    [self.googleReach startNotifier];
    
    
}

#pragma Rechablity
-(void)reachabilityChanged:(NSNotification*)note
{
    Reachability * reach = [note object];
    
    if(reach == self.googleReach)
    {
        if([reach isReachable])
        {
            NSString * temp = [NSString stringWithFormat:@"GOOGLE Notification Says Reachable(%@)", reach.currentReachabilityString];
            NSLog(@"%@", temp);
            
            //[self CheckInternet];
            isInternet=YES;
            
        }
        else
        {
            NSString * temp = [NSString stringWithFormat:@"GOOGLE Notification Says Unreachable(%@)", reach.currentReachabilityString];
            NSLog(@"%@", temp);
            isInternet=NO;
            // [self CheckInternet];
            
        }
    }
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)textFieldDidChange1 :(UITextField *)theTextField
{
    
    
    if ([theTextField.text length]<=12) {
        NSLog(@"ok");
        _txt_akey.text =[NSString stringWithFormat:@"%ld",[theTextField.text integerValue]];
        
        
    }
    else
    {
        NSString *message = [NSString stringWithFormat:@"Should not more than 12 digit"];
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil
                                                                       message:message
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        [self presentViewController:alert animated:YES completion:nil];
        
        int duration = 2; // duration in seconds
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [alert dismissViewControllerAnimated:YES completion:nil];
        });
        
        NSLog(@"NOT OK");
        NSString *lastString=[_txt_akey.text substringToIndex:[_txt_akey.text length] - 1];;
        [_txt_akey setText:lastString];
    }
    
}

-(void)setupAlerts{
    [_txt_akey addRegx:REGEX_PHONE_DEFAULT  withMsg:@"Enter valid password."];
}


-(void)CallActiveKey
{
    
    [APP_DELEGATE showLoadingView:@""];
    
    //Post Method Request
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    [AddPost setValue:_txt_akey.text forKey:@"app_key"];
    [AddPost setValue:@"222312125" forKey:@"imei_number"];
    [AddPost setValue:@"" forKey:@"device_id"];
    [AddPost setValue:@"iPhone" forKey:@"device_type"];
   
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    [manager GET:[NSString stringWithFormat:@"%@check_key?",BaseURLAPI] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject){
        NSLog(@"JSON: %@", responseObject);
        
        if ([[responseObject valueForKey:@"status"] intValue]==1) {
            
            //_dictdata=[responseObject valueForKey:@"data"];
            
            [_dictdata setDictionary:[responseObject valueForKey:@"data"]];
            
            
            
            FreeRegisterVC * next = [self.storyboard instantiateViewControllerWithIdentifier:@"FreeRegisterVC"];
            next.dictdata=_dictdata;
            [self.navigationController pushViewController:next animated:YES];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                            message:@"Please Enter valid password"
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }
        [APP_DELEGATE hideLoadingView];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];
    }];

}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}

- (IBAction)btn_FREE:(id)sender {
    [self.view endEditing:YES];
    FreeRegisterVC * next = [self.storyboard instantiateViewControllerWithIdentifier:@"FreeRegisterVC"];
    [self.navigationController pushViewController:next animated:YES];
    
}

- (IBAction)btn_NEXT:(id)sender {
    [self.view endEditing:YES];
    if ([_txt_akey validate]) {
        if (isInternet) {
             [self CallActiveKey];
        }
        else
        {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Alert" message:@"Internet is not available." preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                
                // Enter code here
            }];
            [alert addAction:defaultAction];
            
            // Present action where needed
            [self presentViewController:alert animated:YES completion:nil];
        }
       
    }
}

- (IBAction)btn_BACK:(id)sender {
    [self.view endEditing:YES];
    [self.navigationController popViewControllerAnimated:YES];
}
@end
