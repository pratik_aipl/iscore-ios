//
//  ReadyPaperCell.m
//  iScoreApp
//
//  Created by ADMIN-Khushal on 5/30/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "ReadyPaperCell.h"

@implementation ReadyPaperCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self shadowView:_vw_1];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)shadowView :(UIView*)viewCheck
{
    viewCheck.layer.shadowRadius  = 3.5f;
    viewCheck.layer.shadowColor   = [UIColor colorWithRed:176.f/255.f green:199.f/255.f blue:226.f/255.f alpha:1.f].CGColor;
    viewCheck.layer.shadowOffset  = CGSizeMake(0.0f, 0.0f);
    viewCheck.layer.shadowOpacity = 2.1f;
    viewCheck.layer.masksToBounds = NO;
    
}

@end
