//
//  SelectPaperTypeVC.m
//  iScoreApp
//
//  Created by ADMIN-Khushal on 3/28/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "SelectPaperType.h"
#import "SetPaperVC.h"
#import "MyModel.h"
#import "MyDBQueries.h"
#import "UIColor+CL.h"
#import "Chapters.h"
#import "MyModel.h"
#import "MyDBQueries.h"
#import "ChapterListModel.h"
#import "UIColor+CL.h"
#import "SubjectCell.h"
#import "ApplicationConst.h"
#import "HomeVC.h"
#import "PaperTypeModel.h"
#import "ChaptersVC.h"
#import "ReadyPaperVC.h"
#import "PDFView.h"



@interface SelectPaperType ()

@end

@implementation SelectPaperType

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    ptype=0;
    
    _tap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doSingleTap)];
    _tap1.numberOfTapsRequired = 1;
    [_vw_preilm addGestureRecognizer:_tap1];
    
    _tap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doSingleTap2)];
    _tap2.numberOfTapsRequired = 1;
    [_vw_ready addGestureRecognizer:_tap2];
    
    _tap3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doSingleTap3)];
    _tap3.numberOfTapsRequired = 1;
    [_vw_set addGestureRecognizer:_tap3];
    
    
    
    
    myDbQueris =[[MyDBQueries alloc]init];
    myDBParser=[[MyDBResultParser alloc]init];
    filePath = [MyModel getFilePath];
    
    FMResultSet *rs = [MyModel selectQuery:[myDbQueris getPaperType:_SubjectID]];
    PaperType = [[NSMutableArray alloc] init];
    while ([rs next]) {
        
        PaperTypeModel *SubModel = [[PaperTypeModel alloc] init];
        [PaperType  addObject:[myDBParser parseDBResult:rs :SubModel]];
        NSLog(@"--%@",PaperType);
    }
    
    PaperTypeModel *myPaperType=[[PaperTypeModel alloc]init];
    
    for (int i=0; i<3; i++) {
        myPaperType=[PaperType objectAtIndex:i];
        switch (i) {
            case 0:
                _lbl_papertype.text=[NSString stringWithFormat:@"%@",myPaperType.PaperTypeName];
                _lbl_tot_test.text=[NSString stringWithFormat:@"%@",myPaperType.TotalTest];
                _lbl_dt.text=[NSString stringWithFormat:@"%@",myPaperType.last_attempt];
                if (myPaperType.last_attempt==[NSNull null])
                {
                    if (![_lbl_dt.text isEqualToString:@"N/A"]) {
                        CGRect border = _lbl_dtemp.frame;
                        border.origin.x =220;
                        _lbl_dtemp.frame = border;
                    }
                    _lbl_dt.text=@"N/A";
                }
                else
                {
                    _lbl_dt.text=[NSString stringWithFormat:@"%@",myPaperType.last_attempt];
                    CGRect border = _lbl_dtemp.frame;
                    border.origin.x =180;
                    _lbl_dtemp.frame = border;
                    
                    _lbl_dt.text=[APP_DELEGATE getDateInFormat:_lbl_dt.text];
                    
                }
                break;
                
            case 1:
                _lbl_papertype1.text=[NSString stringWithFormat:@"%@",myPaperType.PaperTypeName];
                _lbl_tot_test1.text=[NSString stringWithFormat:@"%@",myPaperType.TotalTest];
                _lbl_dt1.text=[NSString stringWithFormat:@"%@",myPaperType.last_attempt];
                if (myPaperType.last_attempt==[NSNull null])
                {
                    if (![_lbl_dt1.text isEqualToString:@"N/A"]) {
                        CGRect border = _lbl_dtemp1.frame;
                        border.origin.x =220;
                        _lbl_dtemp1.frame = border;
                    }
                    _lbl_dt1.text=@"N/A";
                }
                else
                {
                    _lbl_dt1.text=[NSString stringWithFormat:@"%@",myPaperType.last_attempt];
                    CGRect border = _lbl_dtemp1.frame;
                    border.origin.x =180;
                    _lbl_dtemp1.frame = border;
                    
                    _lbl_dt1.text=[APP_DELEGATE getDateInFormat:_lbl_dt1.text];
                }
                break;
                
            case 2:
                _lbl_papertype2.text=[NSString stringWithFormat:@"%@",myPaperType.PaperTypeName];
                _lbl_tot_test2.text=[NSString stringWithFormat:@"%@",myPaperType.TotalTest];
                _lbl_dt2.text=[NSString stringWithFormat:@"%@",myPaperType.last_attempt];
                if (myPaperType.last_attempt==[NSNull null])
                {
                    if (![_lbl_dt2.text isEqualToString:@"N/A"]) {
                        CGRect border = _lbl_dtemp2.frame;
                        border.origin.x =220;
                        _lbl_dtemp2.frame = border;
                    }
                    _lbl_dt2.text=@"N/A";
                }
                else
                {
                    _lbl_dt2.text=[NSString stringWithFormat:@"%@",myPaperType.last_attempt];
                    CGRect border = _lbl_dtemp2.frame;
                    border.origin.x =180;
                    _lbl_dtemp2.frame = border;
                    
                    _lbl_dt2.text=[APP_DELEGATE getDateInFormat:_lbl_dt2.text];
                }
                break;
                
            default:
                break;
        }
        
    }
    
    
}
- (void)doSingleTap {
    //do your stuff for a single tap
    ptype=1;
    NSLog(@"Select View First...");
    _vw_ready.layer.borderWidth=0;
    _vw_set.layer.borderWidth=0;
    _vw_preilm.layer.borderWidth=1;
    _vw_preilm.layer.borderColor=[UIColor redColor].CGColor;
    
}

- (void)doSingleTap2 {
    //do your stuff for a single tap
    ptype=2;
    NSLog(@"Select View Second...");
    _vw_preilm.layer.borderWidth=0;
    _vw_set.layer.borderWidth=0;
    _vw_ready.layer.borderWidth=1;
    _vw_ready.layer.borderColor=[UIColor redColor].CGColor;
    
}
- (void)doSingleTap3 {
    //do your stuff for a single tap
    ptype=3;
    NSLog(@"Select View 3...");
    _vw_preilm.layer.borderWidth=0;
    _vw_ready.layer.borderWidth=0;
    _vw_set.layer.borderWidth=1;
    _vw_set.layer.borderColor=[UIColor redColor].CGColor;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (IBAction)btn_BACK_A:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btn_NEXT:(id)sender {
    
    
    [APP_DELEGATE showLoadingView:@""];
    
    if (ptype==1) {
      
        
     
        
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0ul);
        dispatch_async(queue, ^{
            dispatch_sync(dispatch_get_main_queue(), ^{
                PDFView *VC=[[PDFView alloc]initWithNibName:@"PDFView" bundle:nil];
                VC.isFrom=@"1";
                VC.subjectID=_SubjectID;

                [APP_DELEGATE hideLoadingView];
               [self.navigationController pushViewController:VC animated:YES];
                
            });
        });
       
        
    }
    else if (ptype==2)
    {
        //ReadyPVC *viewController=[[ReadyPVC alloc]initWithNibName:@"ReadyPVC" bundle:nil];
        //[self.navigationController pushViewController:viewController animated:YES];
        
        ReadyPaperVC * next = [self.storyboard instantiateViewControllerWithIdentifier:@"ReadyPaperVC"];
        next.isFrom=@"2";
        next.BoardID=_BoardID;
        next.MediumID=_MediumID;
        next.StandardID=_StandardID;
        next.SubjectID=_SubjectID;
        next.subjectName=_subjectName;
        next.totalTest=_totalTest;
        next.subjectIcon=_subjectIcon;
        [APP_DELEGATE hideLoadingView];
        [self.navigationController pushViewController:next animated:YES];
    }
    else if (ptype==3)
    {
        ChaptersVC * next = [self.storyboard instantiateViewControllerWithIdentifier:@"ChaptersVC"];
        next.isFrom=@"3";
        next.BoardID=_BoardID;
        next.MediumID=_MediumID;
        next.StandardID=_StandardID;
        next.SubjectID=_SubjectID;
        next.subjectName=_subjectName;
        next.totalTest=_totalTest;
        next.subjectIcon=_subjectIcon;
        [APP_DELEGATE hideLoadingView];
        [self.navigationController pushViewController:next animated:YES];
        
    }
    else
    {
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@""
                                                                       message:@"Please Select Paper Type."
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {
                                                                  NSLog(@"OK !!!");
                                                              }];
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
    
}






@end

