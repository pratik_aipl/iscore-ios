//
//  SetPaperCell.m
//  iScoreApp
//
//  Created by ADMIN-Khushal on 4/2/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "readypapercellrow.h"
#import "UIColor+CL.h"

@implementation readypapercellrow

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.vw_marks.bounds byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerBottomLeft) cornerRadii:CGSizeMake(7.0, 7.0)];
    
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.vw_marks.bounds;
    maskLayer.path  = maskPath.CGPath;
    self.vw_marks.layer.mask = maskLayer;
    
    
    _lbl_textvw.layer.borderWidth = 1.0f;
    _lbl_textvw.layer.borderColor = [UIColor colorWithHex:0x646464].CGColor;
    
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
