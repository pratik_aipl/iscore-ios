//
//  ReadyPaperCell.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 5/30/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReadyPaperCell : UITableViewCell



@property (strong, nonatomic) IBOutlet UILabel *lbl_title;
@property (strong, nonatomic) IBOutlet UIButton *img_redio;
@property (strong, nonatomic) IBOutlet UILabel *lbl_marks;

@property (weak, nonatomic) IBOutlet UIView *vw_1;

@end
