//
//  SelectPaperTypeVC.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 3/28/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyDBQueries.h"
#import "MyDBResultParser.h"
#import "QuestionTypeTVC.h"
#import "TVCDispChapters.h"


@interface SelectPaperType : UIViewController
{
    int ptype;
    
    MyDBQueries *myDbQueris;
    MyDBResultParser *myDBParser;
    NSMutableArray *PaperType ;
    NSString *filePath;
}

@property (retain , nonatomic)NSString *BoardID;
@property (retain , nonatomic)NSString *MediumID;
@property (retain , nonatomic)NSString *StandardID;
@property (retain , nonatomic)NSString *SubjectID;
@property (retain , nonatomic)NSString *subjectName;
@property (retain , nonatomic)NSString *totalTest;
@property (retain , nonatomic)NSString *subjectIcon;

@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *tap1;
@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *tap2;
@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *tap3;

//Outlet
@property (strong, nonatomic) IBOutlet UIView *vw_preilm;
@property (strong, nonatomic) IBOutlet UIView *vw_ready;
@property (strong, nonatomic) IBOutlet UIView *vw_set;

@property (strong, nonatomic) IBOutlet UILabel *lbl_papertype;
@property (strong, nonatomic) IBOutlet UILabel *lbl_tot_test;
@property (strong, nonatomic) IBOutlet UILabel *lbl_dt;

@property (strong, nonatomic) IBOutlet UILabel *lbl_papertype1;
@property (strong, nonatomic) IBOutlet UILabel *lbl_tot_test1;
@property (strong, nonatomic) IBOutlet UILabel *lbl_dt1;

@property (strong, nonatomic) IBOutlet UILabel *lbl_papertype2;
@property (strong, nonatomic) IBOutlet UILabel *lbl_tot_test2;
@property (strong, nonatomic) IBOutlet UILabel *lbl_dt2;

@property (strong, nonatomic) IBOutlet UILabel *lbl_dtemp;
@property (strong, nonatomic) IBOutlet UILabel *lbl_dtemp1;
@property (strong, nonatomic) IBOutlet UILabel *lbl_dtemp2;



//Action
- (IBAction)btn_BACK_A:(id)sender;

- (IBAction)btn_NEXT:(id)sender;




@end

