//
//  ReadyPaperVC.m
//  iScoreApp
//
//  Created by ADMIN-Khushal on 5/30/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "ReadyPaperVC.h"
#import "ReadyPaperCell.h"
#import "readypapercellrow.h"
#import "SetPaperVC.h"
#import "MyModel.h"
#import "MyDBQueries.h"
#import "UIColor+CL.h"
#import "Chapters.h"
#import "MyModel.h"
#import "MyDBQueries.h"
#import "ChapterListModel.h"
#import "UIColor+CL.h"
#import "SubjectCell.h"
#import "ApplicationConst.h"
#import "HomeVC.h"
#import "PaperTypeModel.h"
#import "ChaptersVC.h"
#import "ReadyPaperVC.h"
#import "ExamTypeSubjectModel.h"



@interface ReadyPaperVC (){
    NSMutableArray      *sectionTitleArray;
    NSMutableDictionary *sectionContentDict;
    NSMutableArray      *arrayForBool;
}

@end

@implementation ReadyPaperVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
     arrayForBool = [[NSMutableArray alloc] init];
    for (int i = 0; i<4; i++) {
        
        if (i==0) {
            NSString * rate = [NSString stringWithFormat:@"1"];
            [arrayForBool addObject:rate];
        }
        else
        {
            NSString * rate = [NSString stringWithFormat:@"0"];
            [arrayForBool addObject:rate];
        }
        
        
        
        
    }
     [self.tbl_table reloadData];
    
    myDbQueris =[[MyDBQueries alloc]init];
    myDBParser=[[MyDBResultParser alloc]init];
    filePath = [MyModel getFilePath];
    
    [self CallGetExamtype_subject];
    
    
    double delayInSeconds = 0.05;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        NSLog(@"Do some work");
        [_tbl_table reloadData];
    });
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)CallGetExamtype_subject
{
    
    FMResultSet *rs = [MyModel selectQuery:[myDbQueris getExamtype_subject:_SubjectID]];
    examType = [[NSMutableArray alloc] init];
    
    while ([rs next]) {
        ExamTypeSubjectModel *SubModel = [[ExamTypeSubjectModel alloc] init];
        [examType  addObject:[myDBParser parseDBResult:rs :SubModel]];
    }
    
    
    
    
    marrexamType = [NSMutableArray arrayWithArray:examType];
    [self CallExamPatten];
}

-(void)CallExamPatten{

    marrexamPatten=[[NSMutableArray alloc]init];
    NSMutableArray *temparr=[[NSMutableArray alloc]init];
   
    for (int i=0; i<marrexamType.count; i++) {
        ExamTypeSubjectModel *modelID=[marrexamType objectAtIndex:i];
        [temparr addObject:modelID.ExamTypeID];
    }
    
    NSString *tempstr=[self convertToCommaSeparatedFromArray:temparr];
    NSLog(@"--tempstr %@",tempstr);
    
    FMResultSet *rs = [MyModel selectQuery:[myDbQueris getExamPatten:@"1" :tempstr:@"2"]];
    NSMutableArray *tempPatten=[[NSMutableArray alloc]init];
    
    while ([rs next]) {
        ExamTypeSubjectModel *SubModel = [[ExamTypeSubjectModel alloc] init];
        [tempPatten  addObject:[myDBParser parseDBResult:rs :SubModel]];
    }
    marrexamType = [NSMutableArray arrayWithArray:tempPatten];
    
}
-(NSString *)convertToCommaSeparatedFromArray:(NSArray*)array{
    return [array componentsJoinedByString:@","];
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
        return 4;
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    
        if ([[arrayForBool objectAtIndex:section] boolValue]) {
            
            return 1;
        }
        else{
            return 0;
        }
   
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    ExamTypeSubjectModel *examPatten=[marrexamType objectAtIndex:section];
    
    ReadyPaperCell * cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    if (!cell)
    {
        [tableView registerNib:[UINib nibWithNibName:@"ReadyPaperCell" bundle:nil] forCellReuseIdentifier:@"Cell"];
        cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    }
    
    cell.lbl_title.text=[NSString stringWithFormat:@"%@",examPatten.ExamTypeName];
    cell.lbl_marks.text=[NSString stringWithFormat:@"(%@ Marks)",examPatten.TotalMarks];
    
    
    
    BOOL manyCells                  = [[arrayForBool objectAtIndex:section] boolValue];
    if (!manyCells) {
  
    }
    cell.contentView.tag                  = section;

    UITapGestureRecognizer  *headerTapped   = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(sectionHeaderTapped:)];
    [cell.contentView addGestureRecognizer:headerTapped];
    if (!manyCells) {
        cell.img_redio.selected=NO;
    }else{
    //    cell.imageview.image  = [UIImage imageNamed:@"uparrow"];
        cell.img_redio.selected=YES;
    }
   
  //  cell.delegate =self;
    
    // [cell reloadInputViews];
    return cell;
    
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *footer  = [[UIView alloc] initWithFrame:CGRectZero];
    return footer;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{

    return 60;
   
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 10;
}
- (void)tableView:(UITableView *)tableView willDisplayFooterView:(UIView *)view forSection:(NSInteger)section
{
    //Set the background color of the View
    view.backgroundColor = [UIColor colorWithHex:0xF0F0F0];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
   
    ExamTypeSubjectModel *examPatten=[marrexamType objectAtIndex:indexPath.section];
    
    readypapercellrow * cell = [tableView dequeueReusableCellWithIdentifier:@"MYCell"];
    
    cell.lbl_textvw.text=[NSString stringWithFormat:@"%@",examPatten.MainInstruction];
    
        if ([[arrayForBool objectAtIndex:indexPath.section] boolValue]) {
            
            CGRect frame = cell.lbl_textvw.frame;
            frame.size.height = cell.lbl_textvw.contentSize.height+15;
            cell.lbl_textvw.frame = frame;
    
                return cell.lbl_textvw.frame.size.height+55;
            
        }
        else{
            return 0;
        }
    
}
// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ExamTypeSubjectModel *examPatten=[marrexamType objectAtIndex:indexPath.section];
    
    
    readypapercellrow * cell = [tableView dequeueReusableCellWithIdentifier:@"MYCell"];
    
    cell.lbl_textvw.text=[NSString stringWithFormat:@"%@",examPatten.MainInstruction];
    
    CGRect frame = cell.lbl_textvw.frame;
    frame.size.height = cell.lbl_textvw.contentSize.height+15;
    cell.lbl_textvw.frame = frame;
    examTypeID=examPatten.ExamTypeID;
    NSLog(@"Exam pattenID == %@",examPatten.ExamTypeID);
     NSLog(@"Exam ExamTypeName == %@",examPatten.ExamTypeName);
    
    if (cell ==nil)
    {
        [tableView registerNib:[UINib nibWithNibName:@"readypapercellrow" bundle:nil] forCellReuseIdentifier:@"MYCell"];
        cell = [tableView dequeueReusableCellWithIdentifier:@"MYCell"];
    }
    
    BOOL manyCells  = [[arrayForBool objectAtIndex:indexPath.section] boolValue];
    if (!manyCells) {
        // cell.textLabel.text = @"click to enlarge";
    }
    else{
       
            
        
    }
    return cell;
}
#pragma mark - gesture tapped
- (void)sectionHeaderTapped:(UITapGestureRecognizer *)gestureRecognizer{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:gestureRecognizer.view.tag];
    if (indexPath.row == 0) {
        
        for (int i = 0; i<4; i++) {
//            NSString * rate = [NSString stringWithFormat:@"0"];
//            [arrayForBool addObject:rate];
            if (i == indexPath.section) {
                BOOL collapsed  = [[arrayForBool objectAtIndex:indexPath.section] boolValue];
                
                if (collapsed==NO) {
                    collapsed       = !collapsed;
                }
                [arrayForBool replaceObjectAtIndex:i withObject:[NSNumber numberWithBool:collapsed]];
            }
            else{
                 NSString * rate = [NSString stringWithFormat:@"0"];
                [arrayForBool replaceObjectAtIndex:i withObject:rate];
            }
        }

        
    }
     [self.tbl_table reloadData];
    
    double delayInSeconds = 0.05;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        NSLog(@"Do some work");
        [self.tbl_table reloadData];
    });
    
}

- (IBAction)btn_BACK:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)btn_NEXT:(id)sender {
    
    ChaptersVC * next = [self.storyboard instantiateViewControllerWithIdentifier:@"ChaptersVC"];
    next.isFrom=_isFrom;
    next.examTypeID=examTypeID;
    next.BoardID=_BoardID;
    next.MediumID=_MediumID;
    next.StandardID=_StandardID;
    next.SubjectID=_SubjectID;
    next.subjectName=_subjectName;
    next.totalTest=_totalTest;
    next.subjectIcon=_subjectIcon;
    
    [self.navigationController pushViewController:next animated:YES];
    
}
@end
