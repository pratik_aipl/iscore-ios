//
//  ReadyPaperVC.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 5/30/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyDBQueries.h"
#import "MyDBResultParser.h"
#import "QuestionTypeTVC.h"
#import "TVCDispChapters.h"

@interface ReadyPaperVC : UIViewController<UITableViewDelegate,UITableViewDataSource>{
    BOOL isMultipleExpansionAllowed;
    BOOL isFilterd;
    
    MyDBQueries *myDbQueris;
    MyDBResultParser *myDBParser;
    NSMutableArray *PaperType ;
    NSString *filePath,*examTypeID;
    
    
    NSMutableArray *examType;
    NSMutableArray *marrexamType;
    NSMutableArray *marrexamPatten;
}

@property (retain , nonatomic)NSString *BoardID;
@property (retain , nonatomic)NSString *MediumID;
@property (retain , nonatomic)NSString *StandardID;
@property (retain , nonatomic)NSString *SubjectID;
@property (retain , nonatomic)NSString *subjectName;
@property (retain , nonatomic)NSString *totalTest;
@property (retain , nonatomic)NSString *subjectIcon;
@property ( retain , nonatomic )NSString* isFrom;

@property (assign, nonatomic) BOOL checkselect;
@property(nonatomic,retain)NSMutableArray *List;

@property (strong, nonatomic) IBOutlet UITableView *tbl_table;


- (IBAction)btn_BACK:(id)sender;
- (IBAction)btn_NEXT:(id)sender;


@end
