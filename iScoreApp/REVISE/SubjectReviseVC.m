//
//  SubjectReviseVC.m
//  iScoreApp
//
//  Created by ADMIN-Khushal on 8/13/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "SubjectReviseVC.h"
#import "MyModel.h"
#import "MyDBQueries.h"
#import "MCQSubjectModel.h"
#import "UIColor+CL.h"
#import "Chapters.h"
#import "MyModel.h"
#import "MyDBQueries.h"
#import "ChapterListModel.h"
#import "UIColor+CL.h"
#import "SubjectCell.h"
#import "ApplicationConst.h"
#import "HomeVC.h"
#import "SubjectForP.h"
#import "SubjectForPCell.h"
#import "SelectPaperType.h"
#import "ChaptersVC.h"
#import "SelectQueTypeVC.h"




@interface SubjectReviseVC ()

@end

@implementation SubjectReviseVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    myDbQueris =[[MyDBQueries alloc]init];
    myDBParser=[[MyDBResultParser alloc]init];
    filePath = [MyModel getFilePath];
    
    FMResultSet *rs = [MyModel selectQuery:[myDbQueris getPRECSubjectsQuery:_StandardID]];
    subjectArray = [[NSMutableArray alloc] init];
    while ([rs next]) {
        
        MCQSubjectModel *SubModel = [[MCQSubjectModel alloc] init];
        [subjectArray  addObject:[myDBParser parseDBResult:rs :SubModel]];
        NSLog(@"--%@",subjectArray);
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return subjectArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    MCQSubjectModel *mySubject = [subjectArray objectAtIndex:indexPath.row];
    
    static NSString *CellIdentifier = @"Cell";
    
    
    SubjectForPCell *cell = (SubjectForPCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:self options:nil];
        cell = (SubjectForPCell *)[nib objectAtIndex:0];
    }
    
    cell.lbl_tot_test.text=[NSString stringWithFormat:@"%@",mySubject.TotalSubjectTest];
    
    cell.lbl_subjectnm.text=[NSString stringWithFormat:@"%@",mySubject.SubjectName];
    //cell.lbl_dt.text=[NSString stringWithFormat:@"%@",mySubject.Last_Date];
    
    
    if (mySubject.Last_Date==[NSNull null])
    {
        if (![cell.lbl_dt.text isEqualToString:@"N/A"]) {
            // cell.lbl_dt1.frame = CGRectMake(cell.lbl_dt1.frame.origin.x+40, cell.lbl_dt1.frame.origin.y, cell.lbl_dt1.frame.size.width, cell.lbl_dt1.frame.size.height);
            
            CGRect border = cell.lbl_dt1.frame;
            border.origin.x =210;
            cell.lbl_dt1.frame = border;
            
            
        }
        
        cell.lbl_dt.text=@"N/A";
    }
    else
    {
        cell.lbl_dt.text=[NSString stringWithFormat:@"%@",mySubject.Last_Date];
        CGRect border = cell.lbl_dt1.frame;
        border.origin.x =179;
        cell.lbl_dt1.frame = border;
        
        
    /*   NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
         [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
         NSDate *date  = [dateFormatter dateFromString:cell.lbl_dt.text];
         // Convert to new Date Format
         [dateFormatter setDateFormat:@"dd-MM-yyyy"];
         cell.lbl_dt.text = [dateFormatter stringFromDate:date];
         */
        
        cell.lbl_dt.text=[APP_DELEGATE getDateInFormat:cell.lbl_dt.text];
    }
    
    
    NSString* imagepath = [NSString stringWithFormat:@"%@/subject_icon/%@",filePath,[mySubject.SubjectIcon stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
    NSLog(@"Subject Icon Path :: %@",[mySubject.SubjectIcon stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]);
    NSFileManager *fileManager = [[NSFileManager alloc] init];
    
    UIImage *theImage=[UIImage imageWithContentsOfFile:imagepath];
    cell.img_subject.image=theImage;
    
    
    
    //etc.
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MCQSubjectModel *mySubject = [subjectArray objectAtIndex:indexPath.row];

    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    if (cell.selected) {
        // ... Uncheck
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
    
    NSString* imagepath = [NSString stringWithFormat:@"%@/subject_icon/%@",filePath,[mySubject.SubjectIcon stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
    NSLog(@"imagePath  %@",imagepath);
   
    /* SelectPaperType * next = [self.storyboard instantiateViewControllerWithIdentifier:@"SelectPaperType"];
    next.SubjectID=mySubject.SubjectID;
    next.subjectName=mySubject.SubjectName;
    next.totalTest=mySubject.TotalSubjectTest;
    next.subjectIcon=imagepath;
    next.BoardID=_BoardID;
    next.StandardID=_StandardID;
    next.MediumID=_MediumID;
    [self.navigationController pushViewController:next animated:YES];
    */
    
    
    
    ChaptersVC * next = [self.storyboard instantiateViewControllerWithIdentifier:@"ChaptersVC"];
    
    next.isFrom=@"Revise";
    
    next.BoardID=_BoardID;
    next.MediumID=_MediumID;
    next.StandardID=_StandardID;
    next.SubjectID=mySubject.SubjectID;;
    next.subjectName=mySubject.SubjectName;
    next.subjectIcon=imagepath;
    [APP_DELEGATE hideLoadingView];
    [self.navigationController pushViewController:next animated:YES];
    
    
}


- (IBAction)btn_BACK:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end

