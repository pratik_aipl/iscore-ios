//
//  SelectQueTypeCell.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 8/13/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectQueTypeCell : UITableViewCell





@property (weak, nonatomic) IBOutlet UIView *vw_cell;
@property (weak, nonatomic) IBOutlet UIImageView *img_check;
@property (weak, nonatomic) IBOutlet UILabel *lbl_title;
@property (weak, nonatomic) IBOutlet UILabel *lbl_marks;



@end
