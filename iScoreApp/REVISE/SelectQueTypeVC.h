//
//  SelectQueTypeVC.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 8/13/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QuestionTypeTVC1.h"
#import "TVCDispChapters1.h"
#import "MyDBQueries.h"
#import "MyDBResultParser.h"

@interface SelectQueTypeVC : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    NSMutableArray *arrCheck;
    
    MyDBQueries *myDbQueris;
    MyDBResultParser *myDBParser;
    NSMutableArray *arrQuestype;
    
    
}

@property (retain , nonatomic)NSString *subjectName;
@property (retain , nonatomic)NSString *strChapterlist;
@property (retain , nonatomic)NSString *subjectIcon;
@property (retain , nonatomic)NSString *isFrom;
@property (retain , nonatomic)NSString *examTypeID;
@property (retain , nonatomic)NSString *BoardID;
@property (retain , nonatomic)NSString *MediumID;
@property (retain , nonatomic)NSString *StandardID;
@property (retain , nonatomic)NSString *SubjectID;


@property (weak, nonatomic) IBOutlet UIButton *btn_select_all;

@property (weak, nonatomic) IBOutlet UIImageView *img_selectall;
@property (weak, nonatomic) IBOutlet UITableView *tbl_main;




- (IBAction)btn_BACK:(id)sender;
- (IBAction)btn_SELECTALL:(id)sender;
- (IBAction)btn_NEXT:(id)sender;

@end
