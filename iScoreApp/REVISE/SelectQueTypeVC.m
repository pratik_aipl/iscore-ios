//
//  SelectQueTypeVC.m
//  iScoreApp
//
//  Created by ADMIN-Khushal on 8/13/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "SelectQueTypeVC.h"
#import "SelectQueTypeCell.h"
#import "MyModel.h"
#import "MyDBQueries.h"
#import "UIColor+CL.h"
#import "SubjectVC.h"
#import "SelectQueTypeVC.h"
#import "SetPaperModel.h"
#import "PDFView.h"




@interface SelectQueTypeVC ()

@end

@implementation SelectQueTypeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    arrCheck=[[NSMutableArray alloc]init];
    
    _btn_select_all.selected=NO;
    
    [self CallQuestionTypes];
    
    
    for (int i=0; i<arrQuestype.count; i++) {
        [arrCheck addObject:@"0"];
    }
    
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _tbl_main.frame.size.width, 80)];
    [_tbl_main setTableFooterView:view];
    
/*
    if (@available(iOS 11.0, *)) {
        [_tbl_main setInsetsContentViewsToSafeArea:YES];
    } else {
        // Fallback on earlier versions
    }*/
    
    
    
}

-(void)CallQuestionTypes
{
    myDbQueris =[[MyDBQueries alloc]init];
    myDBParser=[[MyDBResultParser alloc]init];
    
    FMResultSet *rs = [MyModel selectQuery:[myDbQueris getQuestionTypes:_SubjectID :_strChapterlist]];
    arrQuestype = [[NSMutableArray alloc] init];
    
    while ([rs next]) {
        SetPaperModel *SubModel = [[SetPaperModel alloc] init];
        [arrQuestype  addObject:[myDBParser parseDBResult:rs :SubModel]];
    }
   
    NSLog(@"arrQuestype %@",arrQuestype);
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrQuestype.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    SetPaperModel *QueType=[arrQuestype objectAtIndex:indexPath.row];
    
    static NSString *CellIdentifier = @"Cell";
    
     SelectQueTypeCell *cell = (SelectQueTypeCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:self options:nil];
        cell = (SelectQueTypeCell *)[nib objectAtIndex:0];
    }
    
    
    if ([[arrCheck objectAtIndex:indexPath.row] boolValue]){
        [cell.img_check setImage:[UIImage imageNamed:@"checkbox-selectd"]];
    }else{
        [cell.img_check setImage:[UIImage imageNamed:@"checkbox-unselectd"]];
    }
    
    
    if (_btn_select_all.selected==TRUE) {
        [_img_selectall setImage:[UIImage imageNamed:@"checkbox-selectd"]];
    }
    else
    {
        [_img_selectall setImage:[UIImage imageNamed:@"checkbox-unselectd"]];
    }
    
    cell.lbl_title.lineBreakMode = NSLineBreakByWordWrapping;
    cell.lbl_title.numberOfLines = 0;
    
    cell.lbl_title.text=[NSString stringWithFormat:@"%@",QueType.QuestionType];
    cell.lbl_marks.text=[NSString stringWithFormat:@"[%@]",QueType.Marks];
    
    
    [self setShadow:cell.vw_cell];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    if (cell.selected) {
        // ... Uncheck
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
    
    if([[arrCheck objectAtIndex:indexPath.row] isEqualToString:@"0"]){
        [arrCheck replaceObjectAtIndex:indexPath.row withObject:@"1"];
    }else{
        [arrCheck replaceObjectAtIndex:indexPath.row withObject:@"0"];
    }
    
    
    [self checkSelectAll];
    
    
    
    
    [_tbl_main reloadData];
    
}

-(void)setShadow :(UIView*)vview
{
    vview.layer.shadowRadius  = 2.5f;
    vview.layer.shadowColor   = [UIColor colorWithRed:176.f/255.f green:199.f/255.f blue:226.f/255.f alpha:1.f].CGColor;
    vview.layer.shadowOffset  = CGSizeMake(0.0f, 0.0f);
    vview.layer.shadowOpacity = 0.7f;
    vview.layer.masksToBounds = NO;
    
    UIEdgeInsets shadowInsets     = UIEdgeInsetsMake(0, 0, -1.5f, 0);
    UIBezierPath *shadowPath      = [UIBezierPath bezierPathWithRect:UIEdgeInsetsInsetRect(vview.bounds, shadowInsets)];
    vview.layer.shadowPath    = shadowPath.CGPath;
    
    vview.layer.cornerRadius=4;
    
}


- (IBAction)btn_BACK:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (IBAction)btn_SELECTALL:(id)sender {
    
    for (int i=0; i<arrQuestype.count; i++) {
        
        if (_btn_select_all.selected==NO){
            [arrCheck replaceObjectAtIndex:i withObject:@"1"];
        }else{
            [arrCheck replaceObjectAtIndex:i withObject:@"0"];
        }
        
    }
    
    if (_btn_select_all.selected==NO){
        [_img_selectall setImage:[UIImage imageNamed:@"checkbox-selectd"]];
        _btn_select_all.selected=YES;
    }else{
        [_img_selectall setImage:[UIImage imageNamed:@"checkbox-unselectd"]];
        _btn_select_all.selected=NO;
    }
    
    [_tbl_main reloadData];
    
}



-(void)checkSelectAll
{
    int index=0;
    
    for (int i=0; i<arrQuestype.count; i++) {
        if ([[arrCheck objectAtIndex:i] intValue]==1) {
            index++;
        }
    }
    
    
    if ([arrCheck count]==index) {
        [_img_selectall setImage:[UIImage imageNamed:@"checkbox-selectd"]];
        _btn_select_all.selected=true;
    }
    else
    {
        [_img_selectall setImage:[UIImage imageNamed:@"checkbox-unselectd"]];
        _btn_select_all.selected=false;
    }
    
}



- (IBAction)btn_NEXT:(id)sender {
    
    NSMutableArray *arrSelectedQue=[[NSMutableArray alloc]init];
    
    for (int i=0; i<arrQuestype.count; i++) {
        SetPaperModel *QueType=[arrQuestype objectAtIndex:i];
        
        if ([[arrCheck objectAtIndex:i] intValue]==1) {
            [arrSelectedQue addObject:[arrQuestype objectAtIndex:i]];
        }
        NSLog(@"arrSelectedQue --%@ ",arrSelectedQue);
    }
    
    
    
    
    PDFView *VC=[[PDFView alloc]initWithNibName:@"PDFView" bundle:nil];
    VC.isFrom=@"Revise";
    VC.subjectID=_SubjectID;
    VC.arrSelectedQue=arrSelectedQue;
    VC.chapterList=_strChapterlist;
    
    [self.navigationController pushViewController:VC animated:YES];
    
    
    
}





@end
