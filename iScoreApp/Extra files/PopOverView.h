//
//  PopOverView.h
//  f2f
//
//  Created by Parth Patoliya on 11/05/16.
//  Copyright (c) 2016 rishi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ConfirmQueType.h"


@class PopOverView;

@interface PopOverView : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    NSMutableArray* arrHostel;
    NSMutableArray* arrYear;
    NSMutableArray *arrMonth;
    NSMutableArray *arrCurrent;
    NSMutableArray *arrtype;
    NSMutableArray *arrhours;
    int ischecked;
    NSMutableArray *arrCity;
    NSMutableArray *arrCategory;
    NSMutableArray *arrsortby;
    NSMutableArray *arrlearn;
    NSMutableArray *arrprofe;
    NSMutableArray *arrcountry;
    NSMutableArray *arrstate;
    
    NSMutableArray *arrDuration;

}
@property(nonatomic,assign) NSMutableArray *marrgender;
@property(nonatomic,assign) NSMutableArray *marrdays;
@property(nonatomic,assign) NSMutableArray *marrcategor;
@property(nonatomic,assign) NSMutableArray *marrcountry;
@property(nonatomic,assign) NSMutableArray *marrstate;
@property(nonatomic,assign) NSMutableArray *marrcity;

@property(nonatomic,assign) NSMutableArray *marrBranch;




@property (weak, nonatomic) IBOutlet UITableView *tblPopOver;
@property(nonatomic,assign)PopOverView *delegates;

@property (nonatomic) NSInteger index;

//USING AFN


-(void)Popvalueselected:(NSString*)strValue;
-(void)dismissPopUpViewController;

@end
