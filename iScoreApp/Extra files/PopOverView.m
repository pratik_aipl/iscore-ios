//
//  PopOverView.m
//  f2f
//
//  Created by Parth Patoliya on 11/05/16.
//  Copyright (c) 2016 rishi. All rights reserved.
//

#import "PopOverView.h"

@interface PopOverView ()

@end

@implementation PopOverView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _tblPopOver.delegate = self;
    _tblPopOver.dataSource = self;
    
  //  arrHostel = [[NSMutableArray alloc]initWithObjects:@"HOSTELS",@"MUMBAI - ANDHERI BOYS",@"MUMBAI-SANDHRUST ROAD BOYS",@"VADODARA BOYS",@"VALLABH VIDYANAGAR BOYS",@"VALLABH VIDYANAGAR GIRLS",@"AMEHADABAD BOYS",@"AMEHADABAD GIRLS",@"PUNE BOYS",@"PUNE GIRLS",@"BHAVNAGAR BOYS",@"UDAIPUR BOYS",nil];
    arrYear = [[NSMutableArray alloc]initWithObjects:@"AL",@"ST", nil];
    arrMonth = [[NSMutableArray alloc]initWithObjects:@"JANUARY",@"FEBRUARY",@"MARCH",@"APRIL",@"MAY",@"JUNE",@"JULY",@"AUGUST",@"SEPTEMBER",@"OCTOBER",@"NOVEMBER",@"DECEMBER", nil];
    arrstate = [[NSMutableArray alloc]initWithObjects:@"MALE",@"FEMALE",nil];
      arrtype = [[NSMutableArray alloc]initWithObjects:@"Rajkot",@"Surat",@"Jamnagar",nil];
     arrhours = [[NSMutableArray alloc]initWithObjects:@"Delay by 30 min",@"Delay by 1 hour",@"Delay by 2 hour",nil];
    
    ///INDIE-------
    
    arrCity = [[NSMutableArray alloc]initWithObjects:@"Los Angeles",@"Washington",@"San Francisco",nil];
    arrcountry = [[NSMutableArray alloc]initWithObjects:@"USA",@"Canada",nil];
    arrstate = [[NSMutableArray alloc]initWithObjects:@"California",@"Florida",@"New York",nil];
    arrCategory = [[NSMutableArray alloc]initWithObjects:@"Manager",@"Director",@"Artiest",nil];
    arrsortby = [[NSMutableArray alloc]initWithObjects:@"Date",@"Name",@"Location",nil];
    arrlearn = [[NSMutableArray alloc]initWithObjects:@"Diu",@"Shomnath",@"Veraval",nil];
    arrprofe = [[NSMutableArray alloc]initWithObjects:@"JOB",@"BUSINESS",nil];
    
    
    arrDuration = [[NSMutableArray alloc]initWithObjects: @"20 Minute", @"30 Minute", @"40 Minute", @"45 Minute", @"1.00 hour", @"1.15 hour", @"1.30 hour", @"1.45 hour", @"2 hour", @"2.15 hour", @"2.30 hour", @"2.45 hour", @"3 hour",nil];

    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated
{
    switch (self.index) {
        case 2:
        {
            arrCurrent = arrDuration;
        }
            break;
    }

}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrCurrent.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    cell.textLabel.textColor = [UIColor blackColor];
    cell.backgroundColor = [UIColor clearColor];
    cell.tag = indexPath.row;
    //cell.textLabel.text = [arrCurrent objectAtIndex:indexPath.row];
    
    
    cell.textLabel.text = [arrCurrent objectAtIndex:indexPath.row];
        
   
    //self.tblPopOver.frame = CGRectMake(0,0,cell.frame.size.width,(cell.frame.size.height)*(arrCurrent.count));
    return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
     [self.delegates Popvalueselected:[arrCurrent objectAtIndex:indexPath.row]];
    
    [self.delegates dismissPopUpViewController];
}

@end
