//
//  IntroView.h
//  ABCIntroView
//
//  Created by Adam Cooper on 2/4/15.
//  Copyright (c) 2015 Adam Cooper. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import <FBSDKLoginKit/FBSDKLoginKit.h>
@protocol ABCIntroViewDelegate <NSObject>

-(void)onDoneButtonPressed;
-(void)onDoneButtonPressed1;

@end

@interface ABCIntroView : UIView{
  // FBSDKLoginButton *fbloginButton;

}
@property id<ABCIntroViewDelegate> delegate;

@end
