//
//  IntroView.m
//  DrawPad
//
//  Created by Adam Cooper on 2/4/15.
//  Copyright (c) 2015 Adam Cooper. All rights reserved.
//

#import "ABCIntroView.h"
#import "UIColor+CL.h"

//#import "Constants.h"

@interface ABCIntroView () <UIScrollViewDelegate>
@property (strong, nonatomic) UIScrollView *scrollView;
@property (strong, nonatomic) UIPageControl *pageControl;
@property (strong, nonatomic) UIButton *doneButton;

@property (strong, nonatomic) UIView *viewOne;
@property (strong, nonatomic) UIView *viewTwo;
@property (strong, nonatomic) UIView *viewThree;
@property (strong, nonatomic) UIView *viewFour;
@property (strong, nonatomic) UIView *viewFive;
@property (strong, nonatomic) UIView *viewsix;

@end

@implementation ABCIntroView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        [self addSubview:self.scrollView];
        [self.scrollView addSubview:self.viewOne];
        [self.scrollView addSubview:self.viewTwo];
        [self.scrollView addSubview:self.viewThree];
        [self.scrollView addSubview:self.viewFour];
        [self.scrollView addSubview:self.viewFive];
        [self addSubview:self.doneButton];
    }
    return self;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    CGFloat pageWidth = CGRectGetWidth(self.bounds);
    CGFloat pageFraction = self.scrollView.contentOffset.x / pageWidth;
    self.pageControl.currentPage = roundf(pageFraction);
    
}

-(UIView *)viewOne {
    if (!_viewOne) {

        
        _viewOne = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height*0.5)];
        _viewOne.backgroundColor = [UIColor greenColor];
        
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, self.frame.size.height*.05, self.frame.size.width*.8, 60)];
        titleLabel.center = CGPointMake(self.center.x, self.frame.size.height*.1);
        titleLabel.text = [NSString stringWithFormat:@"fghfghfgdfgh"];
        titleLabel.font = [UIFont systemFontOfSize:40.0];
        
        titleLabel.textColor = [UIColor blackColor];
        titleLabel.textAlignment =  NSTextAlignmentCenter;
        titleLabel.numberOfLines = 0;
          [_viewOne addSubview:titleLabel];
       // [imageview addSubview:titleLabel];
        UIImageView *imageview = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0,self.frame.size.width, self.frame.size.height)];
        imageview.contentMode = UIViewContentModeScaleToFill;
        imageview.image = [UIImage imageNamed:@"tour_mcq.jpg"];
   
      
     [_viewOne addSubview:imageview];
        
       /* UIImageView *imageview1 = [[UIImageView alloc] initWithFrame:CGRectMake(5, 5,100, 100)];
        imageview1.contentMode = UIViewContentModeScaleToFill;
        imageview1.image = [UIImage imageNamed:@"one.jpg"];
        [_viewOne addSubview:imageview1];*/
        
        UILabel *descriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, self.frame.size.height*.5, 220, 60)];
        descriptionLabel.text = [NSString stringWithFormat:@"Hello iScore"];
        descriptionLabel.font = [UIFont systemFontOfSize:35.0];
        // descriptionLabel.font = [UIFont fontWithName:@"Arial-BoldItalic" size:40.0];r
        descriptionLabel.textColor = [UIColor blackColor];
        descriptionLabel.textAlignment =  NSTextAlignmentLeft;
        descriptionLabel.numberOfLines = 3;
        [descriptionLabel sizeToFit];
        [_viewOne addSubview:descriptionLabel];
        UIButton *doneButton1= [[UIButton alloc] initWithFrame:CGRectMake(self.frame.size.width*0.2,self.frame.size.height-105, 200, 40)];
        //    doneButton1 = [[UIButton alloc] initWithFrame:CGRectMake(self.frame.size.width-60, 20, 50, 60)];
        [doneButton1 setTintColor:[UIColor whiteColor]];
        [doneButton1 setTitle:@"yy" forState:UIControlStateNormal];
        [doneButton1.titleLabel setFont:[UIFont systemFontOfSize:18.0]];
        [doneButton1 setBackgroundColor:[UIColor clearColor]];
        [doneButton1 addTarget:self.delegate action:@selector(onDoneButtonPressed) forControlEvents:UIControlEventTouchUpInside];
        [_viewOne addSubview:doneButton1];
        
        UIButton *doneButton11= [[UIButton alloc] initWithFrame:CGRectMake(self.frame.size.width*0.6,self.frame.size.height-60, 50, 15)];
        [doneButton11 setTintColor:[UIColor whiteColor]];
        [doneButton11 setTitle:@"done" forState:UIControlStateNormal];
        [doneButton11.titleLabel setFont:[UIFont systemFontOfSize:18.0]];
        [doneButton11 setBackgroundColor:[UIColor clearColor]];
        [doneButton11 addTarget:self.delegate action:@selector(onDoneButtonPressed1) forControlEvents:UIControlEventTouchUpInside];
        [_viewOne addSubview:doneButton11];
    }
    _viewOne.layer.backgroundColor=[UIColor colorWithHex:0xAAAAAA].CGColor;
    return _viewOne;
    
}

-(UIView *)viewTwo {
    if (!_viewTwo) {
        CGFloat originWidth = self.frame.size.width;
        CGFloat originHeight = self.frame.size.height;
        
        _viewTwo = [[UIView alloc] initWithFrame:CGRectMake(originWidth, 0, originWidth, originHeight)];
        
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, self.frame.size.height*.05, self.frame.size.width*.8, 60)];
        titleLabel.center = CGPointMake(self.center.x, self.frame.size.height*.1);
        titleLabel.text = [NSString stringWithFormat:@""];
        titleLabel.font = [UIFont systemFontOfSize:40.0];
        titleLabel.textColor = [UIColor whiteColor];
        titleLabel.textAlignment =  NSTextAlignmentCenter;
        titleLabel.numberOfLines = 0;
        [_viewTwo addSubview:titleLabel];
        
        UIImageView *imageview = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0-20,self.frame.size.width, self.frame.size.height)];
        imageview.contentMode = UIViewContentModeScaleToFill;
        imageview.image = [UIImage imageNamed:@"tour_board.jpg"];
        [_viewTwo addSubview:imageview];
        
        UIImageView *imageview1 = [[UIImageView alloc] initWithFrame:CGRectMake(5, 5,100, 100)];
        imageview1.contentMode = UIViewContentModeScaleToFill;
        imageview1.image = [UIImage imageNamed:@""];
        [_viewTwo addSubview:imageview1];
        UILabel *descriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, self.frame.size.height*.5, 220, 50)];
        descriptionLabel.text = [NSString stringWithFormat:@""];
        descriptionLabel.font = [UIFont systemFontOfSize:35.0];
        descriptionLabel.textColor = [UIColor whiteColor];
        descriptionLabel.textAlignment =  NSTextAlignmentLeft;
        descriptionLabel.numberOfLines = 3;
        [descriptionLabel sizeToFit];
        
        [_viewTwo addSubview:descriptionLabel];
        UIButton *doneButton1= [[UIButton alloc] initWithFrame:CGRectMake(self.frame.size.width*0.2,self.frame.size.height-106, 200, 40)];
        [doneButton1 setTintColor:[UIColor whiteColor]];
        [doneButton1 setTitle:@"" forState:UIControlStateNormal];
        [doneButton1.titleLabel setFont:[UIFont systemFontOfSize:18.0]];
        [doneButton1 setBackgroundColor:[UIColor clearColor]];
        [doneButton1 addTarget:self.delegate action:@selector(onDoneButtonPressed) forControlEvents:UIControlEventTouchUpInside];
        [_viewTwo addSubview:doneButton1];
        UIButton *doneButton11= [[UIButton alloc] initWithFrame:CGRectMake(self.frame.size.width*0.6,self.frame.size.height-60, 50, 15)];
        [doneButton11 setTintColor:[UIColor whiteColor]];
        [doneButton11 setTitle:@"" forState:UIControlStateNormal];
        [doneButton11.titleLabel setFont:[UIFont systemFontOfSize:18.0]];
        [doneButton11 setBackgroundColor:[UIColor clearColor]];
        [doneButton11 addTarget:self.delegate action:@selector(onDoneButtonPressed1) forControlEvents:UIControlEventTouchUpInside];
        [_viewTwo addSubview:doneButton11];
    }
    _viewTwo.layer.backgroundColor=[UIColor colorWithHex:0x888888].CGColor;
    return _viewTwo;
    
}

-(UIView *)viewThree{
    
    if (!_viewThree) {
        CGFloat originWidth = self.frame.size.width;
        CGFloat originHeight = self.frame.size.height;
        _viewThree = [[UIView alloc] initWithFrame:CGRectMake(originWidth*2, 0, originWidth, originHeight)];
        
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, self.frame.size.height*.5, 220, 60)];
        titleLabel.center = CGPointMake(self.center.x, self.frame.size.height*.1);
        titleLabel.text = [NSString stringWithFormat:@""];
        titleLabel.font = [UIFont systemFontOfSize:40.0];
        titleLabel.textColor = [UIColor whiteColor];
        titleLabel.textAlignment =  NSTextAlignmentCenter;
        titleLabel.numberOfLines = 0;
        [_viewThree addSubview:titleLabel];
        
        UIImageView *imageview = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0-20,self.frame.size.width, self.frame.size.height)];
        imageview.contentMode = UIViewContentModeScaleToFill;
        imageview.image = [UIImage imageNamed:@"tour_genrate.jpg"];
        [_viewThree addSubview:imageview];
        
        UIImageView *imageview1 = [[UIImageView alloc] initWithFrame:CGRectMake(5, 5,100, 100)];
        imageview1.contentMode = UIViewContentModeScaleToFill;
        imageview1.image = [UIImage imageNamed:@""];
        [_viewThree addSubview:imageview1];
        
        UILabel *descriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, self.frame.size.height*.5, 220, 60)];
        descriptionLabel.text = [NSString stringWithFormat:@""];
        descriptionLabel.font = [UIFont systemFontOfSize:35.0];
        descriptionLabel.textColor = [UIColor whiteColor];
        descriptionLabel.textAlignment =  NSTextAlignmentLeft;
        descriptionLabel.numberOfLines = 3;
        [descriptionLabel sizeToFit];
        
        [descriptionLabel sizeToFit];
        [_viewThree addSubview:descriptionLabel];
        UIButton *doneButton1= [[UIButton alloc] initWithFrame:CGRectMake(self.frame.size.width*0.2,self.frame.size.height-106, 200, 40)];
        [doneButton1 setTintColor:[UIColor whiteColor]];
        [doneButton1 setTitle:@"" forState:UIControlStateNormal];
        [doneButton1.titleLabel setFont:[UIFont systemFontOfSize:18.0]];
        [doneButton1 setBackgroundColor:[UIColor clearColor]];
        [doneButton1 addTarget:self.delegate action:@selector(onDoneButtonPressed) forControlEvents:UIControlEventTouchUpInside];
        
        [_viewThree addSubview:doneButton1];
        
        UIButton *doneButton11= [[UIButton alloc] initWithFrame:CGRectMake(self.frame.size.width*0.6,self.frame.size.height-60, 50, 15)];
        
        [doneButton11 setTintColor:[UIColor whiteColor]];
        [doneButton11 setTitle:@"" forState:UIControlStateNormal];
        [doneButton11.titleLabel setFont:[UIFont systemFontOfSize:18.0]];
        [doneButton11 setBackgroundColor:[UIColor clearColor]];
        [doneButton11 addTarget:self.delegate action:@selector(onDoneButtonPressed1) forControlEvents:UIControlEventTouchUpInside];
        
        [_viewThree addSubview:doneButton11];
    }
     _viewThree.layer.backgroundColor=[UIColor colorWithHex:0x666666].CGColor;
    return _viewThree;
    
}
-(UIView *)viewFour{
    
    if (!_viewFour) {
        CGFloat originWidth = self.frame.size.width;
        CGFloat originHeight = self.frame.size.height;
        _viewFour = [[UIView alloc] initWithFrame:CGRectMake(originWidth*2, 0, originWidth, originHeight)];
        
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, self.frame.size.height*.5, 220, 60)];
        titleLabel.center = CGPointMake(self.center.x, self.frame.size.height*.1);
        titleLabel.text = [NSString stringWithFormat:@""];
        titleLabel.font = [UIFont systemFontOfSize:40.0];
        titleLabel.textColor = [UIColor whiteColor];
        titleLabel.textAlignment =  NSTextAlignmentCenter;
        titleLabel.numberOfLines = 0;
        [_viewFour addSubview:titleLabel];
        
        UIImageView *imageview = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0-20,self.frame.size.width, self.frame.size.height)];
        imageview.contentMode = UIViewContentModeScaleToFill;
        imageview.image = [UIImage imageNamed:@"tour_report.jpg"];
        [_viewFour addSubview:imageview];
        
        UIImageView *imageview1 = [[UIImageView alloc] initWithFrame:CGRectMake(5, 5,100, 100)];
        imageview1.contentMode = UIViewContentModeScaleToFill;
        imageview1.image = [UIImage imageNamed:@""];
        [_viewFour addSubview:imageview1];
        
        UILabel *descriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, self.frame.size.height*.5, 220, 60)];
        descriptionLabel.text = [NSString stringWithFormat:@""];
        descriptionLabel.font = [UIFont systemFontOfSize:35.0];
        descriptionLabel.textColor = [UIColor whiteColor];
        descriptionLabel.textAlignment =  NSTextAlignmentLeft;
        descriptionLabel.numberOfLines = 3;
        [descriptionLabel sizeToFit];
        
        [descriptionLabel sizeToFit];
        [_viewFour addSubview:descriptionLabel];
        UIButton *doneButton1= [[UIButton alloc] initWithFrame:CGRectMake(self.frame.size.width*0.2,self.frame.size.height-106, 200, 40)];
        [doneButton1 setTintColor:[UIColor whiteColor]];
        [doneButton1 setTitle:@"" forState:UIControlStateNormal];
        [doneButton1.titleLabel setFont:[UIFont systemFontOfSize:18.0]];
        [doneButton1 setBackgroundColor:[UIColor clearColor]];
        [doneButton1 addTarget:self.delegate action:@selector(onDoneButtonPressed) forControlEvents:UIControlEventTouchUpInside];
        
        [_viewFour addSubview:doneButton1];
        
        UIButton *doneButton11= [[UIButton alloc] initWithFrame:CGRectMake(self.frame.size.width*0.6,self.frame.size.height-60, 50, 15)];
        
        [doneButton11 setTintColor:[UIColor whiteColor]];
        [doneButton11 setTitle:@"" forState:UIControlStateNormal];
        [doneButton11.titleLabel setFont:[UIFont systemFontOfSize:18.0]];
        [doneButton11 setBackgroundColor:[UIColor clearColor]];
        [doneButton11 addTarget:self.delegate action:@selector(onDoneButtonPressed1) forControlEvents:UIControlEventTouchUpInside];
        
        [_viewFour addSubview:doneButton11];
    }
     _viewFour.layer.backgroundColor=[UIColor colorWithHex:0x444444].CGColor;
    return _viewFour;
    
}
-(UIView *)viewFive{
    
    if (!_viewFive) {
        CGFloat originWidth = self.frame.size.width;
        CGFloat originHeight = self.frame.size.height;
        _viewFive = [[UIView alloc] initWithFrame:CGRectMake(originWidth*2, 0, originWidth, originHeight)];
        
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, self.frame.size.height*.5, 220, 60)];
        titleLabel.center = CGPointMake(self.center.x, self.frame.size.height*.1);
        titleLabel.text = [NSString stringWithFormat:@""];
        titleLabel.font = [UIFont systemFontOfSize:40.0];
        titleLabel.textColor = [UIColor whiteColor];
        titleLabel.textAlignment =  NSTextAlignmentCenter;
        titleLabel.numberOfLines = 0;
        [_viewFive addSubview:titleLabel];
        
        UIImageView *imageview = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0-20,self.frame.size.width, self.frame.size.height)];
        imageview.contentMode = UIViewContentModeScaleToFill;
        imageview.image = [UIImage imageNamed:@"tour_report.jpg"];
        [_viewFive addSubview:imageview];
        
        UIImageView *imageview1 = [[UIImageView alloc] initWithFrame:CGRectMake(5, 5,100, 100)];
        imageview1.contentMode = UIViewContentModeScaleToFill;
        imageview1.image = [UIImage imageNamed:@""];
        [_viewFive addSubview:imageview1];
        
        UILabel *descriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, self.frame.size.height*.5, 220, 60)];
        descriptionLabel.text = [NSString stringWithFormat:@""];
        descriptionLabel.font = [UIFont systemFontOfSize:35.0];
        descriptionLabel.textColor = [UIColor whiteColor];
        descriptionLabel.textAlignment =  NSTextAlignmentLeft;
        descriptionLabel.numberOfLines = 3;
        [descriptionLabel sizeToFit];
        
        [descriptionLabel sizeToFit];
        [_viewFive addSubview:descriptionLabel];
        UIButton *doneButton1= [[UIButton alloc] initWithFrame:CGRectMake(self.frame.size.width*0.2,self.frame.size.height-106, 200, 40)];
        [doneButton1 setTintColor:[UIColor whiteColor]];
        [doneButton1 setTitle:@"" forState:UIControlStateNormal];
        [doneButton1.titleLabel setFont:[UIFont systemFontOfSize:18.0]];
        [doneButton1 setBackgroundColor:[UIColor clearColor]];
        [doneButton1 addTarget:self.delegate action:@selector(onDoneButtonPressed) forControlEvents:UIControlEventTouchUpInside];
        
        [_viewFive addSubview:doneButton1];
        
        UIButton *doneButton11= [[UIButton alloc] initWithFrame:CGRectMake(self.frame.size.width*0.6,self.frame.size.height-60, 50, 15)];
        
        [doneButton11 setTintColor:[UIColor whiteColor]];
        [doneButton11 setTitle:@"" forState:UIControlStateNormal];
        [doneButton11.titleLabel setFont:[UIFont systemFontOfSize:18.0]];
        [doneButton11 setBackgroundColor:[UIColor clearColor]];
        [doneButton11 addTarget:self.delegate action:@selector(onDoneButtonPressed1) forControlEvents:UIControlEventTouchUpInside];
        
        [_viewFive addSubview:doneButton11];
    }
    _viewFive.layer.backgroundColor=[UIColor colorWithHex:0x444444].CGColor;
    return _viewFive;
    
}
-(UIScrollView *)scrollView {
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc] initWithFrame:self.frame];
        [_scrollView setDelegate:self];
        [_scrollView setPagingEnabled:YES];
        _scrollView.alwaysBounceHorizontal = false;
        _scrollView.alwaysBounceVertical = false;
        [_scrollView setContentSize:CGSizeMake(self.frame.size.width*5, 0)];
        [self.scrollView setContentOffset:CGPointMake(0, 0) animated:NO];
    }
    return _scrollView;
}

-(UIPageControl *)pageControl {
    if (!_pageControl) {
        _pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0, self.frame.size.height-80, self.frame.size.width, 10)];
        [_pageControl setCurrentPageIndicatorTintColor:[UIColor colorWithRed:0.129 green:0.588 blue:0.953 alpha:1.000]];
        [_pageControl setNumberOfPages:5];
    }
    return _pageControl;
}
@end
