//
//  SharedData.h
//  McqApp
//
//  Created by My Mac on 3/22/17.
//  Copyright © 2017 My Mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SharedData : NSObject


@property (nonatomic ,strong) NSMutableArray * marrBoarddata;

@property (nonatomic ,strong) NSMutableArray * marrboards_paperdata;

//@property (nonatomic ,strong) NSMutableArray * marrboards_paper_data;

@property (nonatomic ,strong) NSMutableArray * marrboard_paper_files;

@property (nonatomic ,strong) NSMutableArray * marrchapters;

@property (nonatomic ,strong) NSMutableArray * marrexamtypes;

@property (nonatomic ,strong) NSMutableArray * marrexamtype_subject;

@property (nonatomic ,strong) NSMutableArray * marrmasterquestion;

@property (nonatomic ,strong) NSMutableArray * marrmcqoption;

@property (nonatomic ,strong) NSMutableArray * marrmcqquestion;

@property (nonatomic ,strong) NSMutableArray * marrmediums;

@property (nonatomic ,strong) NSMutableArray * marrquestion_types;

@property (nonatomic ,strong) NSMutableArray * marrstandards;

@property (nonatomic ,strong) NSMutableArray * marrsubjects;

@property (nonatomic ,strong) NSMutableArray * marrpaper_type;

@property (nonatomic ,strong) NSMutableArray * marrexam_type_pattern;

@property (nonatomic ,strong) NSMutableArray * marrexam_type_pattern_detail;

@property (nonatomic ,strong) NSMutableDictionary * mdictImages;

@property (nonatomic ,strong) NSMutableArray * marrtemp_mcq_test_hdr;

@property (nonatomic ,strong) NSMutableArray * marrtemp_mcq_test_dtl;

@property (nonatomic ,strong) NSMutableArray * marrtemp_mcq_test_chapter;

@property (nonatomic ,strong) NSMutableArray * marrnotification;

@property (nonatomic ,strong) NSMutableArray * marrstudent_mcq_test_hdr;

@property (nonatomic ,strong) NSMutableArray * marrstudent_mcq_test_dtl;

@property (nonatomic ,strong) NSMutableArray * marrstudent_mcq_test_chapter;

@property (nonatomic ,strong) NSMutableArray * marrstudent_question_paper;

@property (nonatomic ,strong) NSMutableArray * marrstudent_set_paper_question_type;

@property (nonatomic ,strong) NSMutableArray * marrstudent_set_paper_detail;


////NEW ISCORE ARRAY
@property (nonatomic ,strong) NSString * rootImages;
@property (nonatomic ,strong) NSMutableArray * marrImages;
@property (nonatomic ,strong) NSMutableArray * marrQueImages;






+(SharedData *) sharedInstance;

@end
