//
//  MyDBResultParser.m
//  iScoreApp
//
//  Created by ADMIN-Khushal on 4/20/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "MyDBResultParser.h"
#import "LevelModel.h"

@implementation MyDBResultParser


-(NSObject *) parseDBResult :(FMResultSet*)result  :(NSObject *) object {
    NSString *className = NSStringFromClass([object class]);
    return [[NSClassFromString(className) alloc] parseObjectWithFMResultSet:[result resultDictionary]];
}



@end
