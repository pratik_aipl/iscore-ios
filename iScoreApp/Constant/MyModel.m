//
//  MyModel.m
//  QuizApp
//
//  Created by My Mac on 2/17/17.
//  Copyright © 2017 My Mac. All rights reserved.
//

#import "MyModel.h"
#import "API.h"
#import "IQKeyboardReturnKeyHandler.h"
#import "FMDatabase.h"
#import "AppDelegate.h"
#import "Constant.h"
#import "FMDatabaseAdditions.h"
#import "MCQSubjectModel.h"

@interface MyModel ()

@end

@implementation MyModel

FMDatabase * db;
//NIDropDown * dropDown;

+(void)MyModelInit{
    
    
    //APP_DELEGATE.sharedData;
    [self openDB];
    
    
    
}

#pragma mark - *****************  Local DB  **************

// Open database
+(void)openDB
{
    
    NSArray *paths=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docspath=[paths objectAtIndex:0];
    NSString *path=[docspath stringByAppendingPathComponent:@"iScoreApp.sqlite"];
    NSLog(@"path....%@",path);
    db =[FMDatabase databaseWithPath:path];
    
    [db open];
    
    
    [self UpdateQuery:[NSString stringWithFormat:@"create table if not exists student(id INTEGER PRIMARY KEY AUTOINCREMENT , name text )"]];
    
    [self UpdateQuery:[NSString stringWithFormat:@"create table if not exists boards( BoardID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL , BoardName text , BoardFullName text , CreatedBy varchar(255) , CreatedOn datetime , ModifiedBy varchar(255) , ModifiedOn timestamp ) "]];
    
    [self UpdateQuery:[NSString stringWithFormat:@"create table if not exists boards_paper( BoardPaperID integer (11) , BoardPaperName text , CreatedBy varchar(255) , CreatedOn datetime , ModifiedBy varchar(255) , ModifiedOn timestamp )"]];
    
    [self UpdateQuery:[NSString stringWithFormat:@"create table if not exists boards_paper_data( BPDataID integer (11) , QuestionID integer (11) , BoardPaperID integer (11) )"]];
    
    [self UpdateQuery:[NSString stringWithFormat:@"create table if not exists board_paper_files(BoardPaperFileID integer (11) , BoardID integer (11) , MediumID integer (11) , StandardID integer (11) , SubjectID integer (11) , BoardPaperID integer (11) , QFile varchar(255) , QAFile varchar(255) , CreatedBy varchar(255) , CreatedOn datetime , ModifiedBy varchar(255) , ModifiedOn timestamp , Rootpath varchar(255) )"]];
    
    [self UpdateQuery:[NSString stringWithFormat:@"create table if not exists chapters( ChapterID integer(11) , ChapterNumber integer (11) , ParentChapterID integer (11) , BoardID integer (11) , MediumID integer (11) , StandardID integer (11) , SubjectID integer (11) , ChapterName text ,isMCQ integer (11),DisplayOrder integer (11), CreatedBy varchar(255) , CreatedOn datetime , ModifiedBy varchar(255) , ModifiedOn timestamp ,isGeneratePaper integer (11),WeightageMarks integer (11),isMandatory integer (11),isLibrary integer (11),isDemo integer (11))"]];
    
    [self UpdateQuery:[NSString stringWithFormat:@"create table if not exists ci_sessions(sessionID varchar(40) , ip_address varchar(16) , user_agent varchar(150) , last_activity integer (10), user_data text )"]];
    
    [self UpdateQuery:[NSString stringWithFormat:@"create table if not exists classes(ClassID integer (11) , ClassName varchar(250) , Address1 text , Area varchar(200), Zipcode varchar(10) , City varchar(50) , State varchar(50) , Country varchar(50) , CreatedBy varchar(255) , CreatedOn datetime , ModifiedBy varchar(255) , ModifiedOn timestamp )"] ];
    
    [self UpdateQuery:[NSString stringWithFormat:@"create table if not exists examtypes( ExamTypeID integer (11) , ExamTypeName text , PaperType varchar (255) NOT NULL , BoardID varchar (16) , MediumID varchar (16) , StandardID varchar (16) , TotalMarks decimal (5,2) , Duration text , Instruction text , CreatedBy varchar(255) , CreatedOn datetime , ModifiedBy varchar(255) , ModifiedOn timestamp, MarkLabel varchar (100) )"]];
    
    [self UpdateQuery:[NSString stringWithFormat:@"create table if not exists examtype_subject( ExamTypeSubjectID integer (11) , ExamTypeID text , SubjectID varchar(255) )"]];
    
    
    [self UpdateQuery:[NSString stringWithFormat:@"create table if not exists login_attempts( id integer (11) , ip_address varchar(40) , login varchar(50) , time timestamp )"]];
    
    
    [self UpdateQuery:[NSString stringWithFormat:@"create table if not exists masterquestion( MQuestionID integer (11) , Question longtext , ImageURL varchar(100) , Answer longtext , BoardID  integer (11) , MediumID integer (11) , ClassID integer (11) , StandardID integer (11) , SubjectID integer (11) , ChapterID integer (11) , QuestionTypeID integer (11) , isBoard  tinyint(1) , ExerciseNo varchar (255) , QuestionNo varchar (255) , Question_Year longtext (255) , is_demo varchar (11) , CreatedBy varchar(255) , CreatedOn datetime , ModifiedBy varchar(255) , ModifiedOn timestamp , Solution longtext , is_hide tinyint(1) , old_question_id integer(11) , isTextual tinyint(1))"]];
    
    
    [self UpdateQuery:[NSString stringWithFormat:@"create table if not exists mcqoption( MCQOPtionID integer (11) , MCQQuestionID integer (11) , Options text , ImageURL varchar(100) , OptionNo integer (11) , isCorrect  tinyint(1) , CreatedBy varchar(255) , CreatedOn datetime , ModifiedBy varchar(255) , ModifiedOn timestamp )"]];
    
    [self UpdateQuery:[NSString stringWithFormat:@"create table if not exists mcqquestion( MCQQuestionID integer (11) , Solution text , Question text, ImageURL varchar(100) , BoardID  integer (11), PageNumberID  integer (11), is_demo  integer (11) , MediumID integer (11) , ClassID integer (11) , StandardID integer (11) , SubjectID integer (11) , ChapterID integer (11) , QuestionTypeID integer (11) , QuestionLevelID  integer (11) , Duration integer (11) , CreatedBy varchar(255) , CreatedOn datetime , ModifiedBy varchar(255) , ModifiedOn timestamp )"]];
    
    [self UpdateQuery:[NSString stringWithFormat:@"create table if not exists mediums( MediumID integer (11) , BoardID integer (11) , MediumName text , CreatedBy varchar(255) , CreatedOn datetime , ModifiedBy varchar(255) , ModifiedOn timestamp )"]];
    
    [self UpdateQuery:[NSString stringWithFormat:@"create table if not exists question_types( QuestionTypeID integer (11) , QuestionType varchar(255) , BoardID  integer (11) , MediumID integer (11) , ClassID integer (11) , StandardID integer (11) , SubjectID integer (11) , Marks decimal(5,2) , isMTC integer(4),Type varchar(100) , RevisionNo  varchar(5) , CreatedBy varchar(255) , CreatedOn datetime , ModifiedBy varchar(255) , ModifiedOn timestamp , isMarker integer(4) , isPassage integer(4) )"]];
    
    
    
    [self UpdateQuery:[NSString stringWithFormat:@"create table if not exists standards( StandardID integer (11) , StandardName text , BoardID integer (11) , MediumID integer (11) , CreatedBy varchar(255) , CreatedOn datetime , ModifiedBy varchar(255) , ModifiedOn timestamp )"]];
    //
        [self UpdateQuery:[NSString stringWithFormat:@"create table if not exists subjects( SubjectID integer (11) , SubjectName text , BoardID integer (11) , MediumID integer (11) , StandardID  integer (11) , CreatedBy varchar(255) , CreatedOn datetime , ModifiedBy varchar(255) , SubjectIcon  varchar(128) , ModifiedOn timestamp , Price integer (11) , SubjectOrder integer (11), isGeneratePaper integer (11), isMCQ tinyint(1) , isWEIGHTAGE integer (11), Instruction varchar(255), isTextual integer (11),isSimilar integer (11),IsMapped integer (11),MappedSubjectID integer (11))"]];
    //
    
    
      //[self UpdateQuery:[NSString stringWithFormat:@"create table if not exists subjects( SubjectID integer (11) , SubjectName text , BoardID integer (11) , MediumID integer (11) , StandardID  integer (11) , CreatedBy varchar(255) , CreatedOn datetime , ModifiedBy varchar(255) , SubjectIcon  varchar(128) , ModifiedOn timestamp , Price integer (11) , SubjectOrder integer (11), isGeneratePaper integer (11), isMCQ tinyint(1) , isWEIGHTAGE integer (11), Instruction varchar(255), isTextual integer (11))"]];
    
    [self UpdateQuery:[NSString stringWithFormat:@"create table if not exists users( id integer (11) , username varchar(50) , password varchar(255) , email varchar(100) , activated  tinyint(1) , banned tinyint(1) , ben_reason varchar(255) , new_password_key varchar(50) , new_password_requested  datetime , new_email varchar(100) , new_email_key varchar(50) , last_ip varchar(40) , last_login datetime , crested datetime , modified timestamp )"]];
    
    [self UpdateQuery:[NSString stringWithFormat:@"create table if not exists user_autologin( key_id char(32) , user_id integer(11) , user_agent varchar(150) , last_ip varchar(40) , last_login  timestamp )"]];
    
    [self UpdateQuery:[NSString stringWithFormat:@"create table if not exists user_profiles( id integer(11) , user_id integer(11) , country varchar(20) , website varchar(255) )"]];
    
    [self UpdateQuery:[NSString stringWithFormat:@"create table if not exists temp_mcq_test_hdr( TempMCQTestHDRID integer PRIMARY KEY AUTOINCREMENT , StudentID integer(11) , BoardID  integer (11) , MediumID integer (11) ,  SubjectID integer (11) , LevelID integer(11) , Timeleft varchar(255),ClassID varchar(255), CreatedBy varchar(255) , CreatedOn datetime , ModifiedBy varchar(255) , ModifiedOn timestamp )"]];
    
    [self UpdateQuery:[NSString stringWithFormat:@"create table if not exists temp_mcq_test_dtl( TempMCQTestDTLID integer PRIMARY KEY AUTOINCREMENT , TempMCQTestHDRID varchar(16) , QuestionID  varchar (16) ,QuestionTime  varchar( 255 ), AnswerID varchar (16) ,  IsAttempt varchar (16) , srNo varchar(16) , CreatedBy varchar(255) , CreatedOn datetime , ModifiedBy varchar(255) , ModifiedOn timestamp )"]];
    
    [self UpdateQuery:[NSString stringWithFormat:@"create table if not exists temp_mcq_test_chapter( TempMCQTestChapterID integer PRIMARY KEY AUTOINCREMENT , TempMCQTestHDRID integer(11) , ChapterID  integer (11) )"]];
    
    [self UpdateQuery:[NSString stringWithFormat:@"create table if not exists student_mcq_test_hdr( StudentMCQTestHDRID integer PRIMARY KEY AUTOINCREMENT , StudentID integer(11) , BoardID  integer (11) ,TestTime varchar (255), MediumID integer (11) ,  SubjectID integer (11) , LevelID integer(11) , TimerStatus varchar (255),ClassID varchar (255),Timeleft varchar (255), AddType CHAR(1) NOT NULL DEFAULT ('A'), CreatedBy varchar(255) , CreatedOn datetime , ModifiedBy varchar(255) , ModifiedOn timestamp )"]];
    
    [self UpdateQuery:[NSString stringWithFormat:@"create table if not exists student_mcq_test_dtl( StudentMCQTestDTLID integer PRIMARY KEY AUTOINCREMENT , StudentMCQTestHDRID integer(11) , QuestionID  integer (11) , AnswerID integer (11) ,QuestionTime integer (11),  IsAttempt integer (11) , srNo integer(11) , CreatedBy varchar(255) , CreatedOn datetime , ModifiedBy varchar(255) , ModifiedOn timestamp )"]];
    
    [self UpdateQuery:[NSString stringWithFormat:@"create table if not exists student_mcq_test_chapter( StudentMCQTestChapterID integer PRIMARY KEY AUTOINCREMENT , StudentMCQTestHDRID integer(11) , ChapterID  integer (11) )"]];
    
    [self UpdateQuery:[NSString stringWithFormat:@"create table if not exists paper_type( PaperTypeID varchar (15) , PaperTypeName varchar(255) )"]];
    
    [self UpdateQuery:[NSString stringWithFormat:@"create table if not exists exam_type_pattern( ExamTypePatternID varchar (15)  , BoardID  varchar (15) , MediumID varchar (15) , StandardID varchar(15),  SubjectID varchar (15) , ExamTypeID varchar(15) , TotalMarks decimal (5,2) , Duration text , Instruction text , PatternNo varchar (15) , CreatedBy varchar(255) , CreatedOn datetime , ModifiedBy varchar(255) , ModifiedOn timestamp )"]];
    
    [self UpdateQuery:[NSString stringWithFormat:@"create table if not exists exam_type_pattern_detail( ExamTypePatternDetailID varchar(15) , ExamTypePatternID integer (11)  , ExamTypeID  integer (11) , QuestionTypeID integer (11) , QuestionNO text,  SubQuestionNO text , QuestionTypeText text , QuestionMarks varchar (255) , NoOfQuestion integer(11) , DisplayOrder integer(11) , isQuestion integer(11) , isQuestionVisible integer(11) , ChapterID integer(15) , CreatedBy varchar(255) , CreatedOn datetime , ModifiedBy varchar(255) , ModifiedOn timestamp, PageNo integer(15) )"]];
    
    
    [self UpdateQuery:[NSString stringWithFormat:@"create table if not exists student_question_paper( StudentQuestionPaperID integer PRIMARY KEY AUTOINCREMENT , ExamTypePatternID integer (11)  , StudentID  integer (11) ,SubjectID varchar (15),PaperTypeID varchar (15),TotalMarks varchar (15), CreatedBy varchar(255) ,Duration varchar (15) , CreatedOn datetime , ModifiedBy varchar(255) , ModifiedOn timestamp )"]];
    
    [self UpdateQuery:[NSString stringWithFormat:@"create table if not exists student_question_paper_chapter( StudentQuestionPaperChapterID integer PRIMARY KEY AUTOINCREMENT , StudentQuestionPaperID integer (11)  , ChapterID  integer (11) )"]];
    
    [self UpdateQuery:[NSString stringWithFormat:@"create table if not exists student_question_paper_detail( StudentQuestionPaperDetailID integer PRIMARY KEY AUTOINCREMENT , StudentQuestionPaperID integer(11), ExamTypePatternDetailID integer (11)  , MasterorCustomized  integer (11) , MQuestionID integer(11) , CQuestionID integer(11) )"]];
    
    [self UpdateQuery:[NSString stringWithFormat:@"create table if not exists notification( ID integer PRIMARY KEY AUTOINCREMENT , StudentKey varchar(255), Title varchar(255)  , Message  varchar(255) , CreatedOn datetime)"]];
    
    [self UpdateQuery:[NSString stringWithFormat:@"create table if not exists student_set_paper_question_type( StudentSetPaperQuestionTypeID integer PRIMARY KEY AUTOINCREMENT , StudentQuestionPaperID integer (11)  , QuestionTypeID  integer (11) , TotalAsk varchar(11) , ToAnswer  varchar(11) , TotalMark varchar(11))"]];
    
    [self UpdateQuery:[NSString stringWithFormat:@"create table if not exists student_set_paper_detail( StudentSetPaperDetailID integer PRIMARY KEY AUTOINCREMENT , StudentSetPaperQuestionTypeID integer (11)  , MQuestionID  integer (11))"]];
    
    
    //[db close];
    
    //NSLog(@"Table Created");
    
    //More Table=======================================
    
    //====board_paper_download
    [self UpdateQuery:[NSString stringWithFormat:@"create table if not exists board_paper_download( SubjectID    integer ( 11 ), CreatedOn     DATETIME ,BoardPaperDownloadID integer PRIMARY KEY AUTOINCREMENT, BoardPaperAnswers integer ( 11 ),BoardPaperID integer ( 11 ),StudentID integer ( 11 ) )"]];
    
    //====class_evaluater_cct_count
    [self UpdateQuery:[NSString stringWithFormat:@"create table if not exists class_evaluater_cct_count( SubjectID    varchar ( 32 ), SubjectName     varchar ( 32 ) ,ClassCctID integer PRIMARY KEY AUTOINCREMENT, Total varchar ( 32 ),Acuuracy varchar ( 32 ) )"]];
    
    //====class_evaluater_mcq_test_temp
    [self UpdateQuery:[NSString stringWithFormat:@"create table if not exists class_evaluater_mcq_test_temp( IsAttempt    varchar ( 32 ), AnswerID     varchar ( 32 ) ,QuestionID varchar ( 32 ), ClassMCQTestHDRID varchar ( 32 ),MasterorCustomized varchar ( 32 ),Question text, ClassMCQTestDTLID integer PRIMARY KEY AUTOINCREMENT )"]];
    
    //====student_mcq_test_level
    [self UpdateQuery:[NSString stringWithFormat:@"create table if not exists student_mcq_test_level( StudentMCQTestHDRID    varchar ( 32 ), StudentMCQTestLevelID  integer PRIMARY KEY AUTOINCREMENT,LevelID varchar ( 255 ))"]];
    
    //====temp_mcq_test_level
    [self UpdateQuery:[NSString stringWithFormat:@"create table if not exists temp_mcq_test_level( StudentMCQTestHDRID    varchar ( 32 ), StudentMCQTestLevelID  integer PRIMARY KEY AUTOINCREMENT,LevelID varchar ( 255 ) )"]];
    
    //====updated_masterquetion
    [self UpdateQuery:[NSString stringWithFormat:@"create table if not exists updated_masterquetion( old_question_id    integer ( 11 ), MQuestionID  integer, PRIMARY KEY(MQuestionID))"]];
    
    //====zooki
    [self UpdateQuery:[NSString stringWithFormat:@"create table if not exists zooki( image    varchar ( 255 ), Desc  longtext,ZookiID integer PRIMARY KEY AUTOINCREMENT, CreatedOn varchar ( 255 ),Title varchar ( 255 )) "]];
    
    //student_not_appeared_question
    [self UpdateQuery:[NSString stringWithFormat:@"create table if not exists student_not_appeared_question( CreatedOn datetime , QuestionID integer(11) , StudentID  integer (11))"]];
    
    //student_incorrect_question
    [self UpdateQuery:[NSString stringWithFormat:@"create table if not exists student_incorrect_question( CreatedOn datetime , QuestionID integer(11) , StudentID  integer (11))"]];
    
    
    //[self UpdateQuery:[NSString stringWithFormat:@"delete from student_not_appeared_question"]];
    //[self UpdateQuery:[NSString stringWithFormat:@"delete from student_incorrect_question"]];
    
    
}

+(FMResultSet*)selectQuery:(NSString*)strQuery{
    
    NSLog(@"Query%@",strQuery);
    
    [db beginTransaction];
    
    //[db open];
    
    FMResultSet * res = [db executeQuery:strQuery];
    
    //[db close];
    [db commit];
    
    return res;
    
}

+(void)UpdateQuery:(NSString*)strQuery{
    
    //NSLog(@"Query%@",strQuery);
    
    [db beginTransaction];
    
    //[db open];
    
    BOOL result = [db executeUpdate:strQuery];
    
    if(result){
        NSLog(@"InsertQuery Success  ");
    } else {
        NSLog(@"InsertQuery Fail :  %@",strQuery);
    }
    
    //[db close];
    
    [db commit];
    
}



+(NSUInteger)getCount:(NSString*)strQuery{
    
    [db beginTransaction];
    NSUInteger  count = [db intForQuery:strQuery];
    [db commit];
    return count;
}


+(NSMutableArray*) getAllColumnNames : (NSString*) tblName {
    
    FMResultSet *columnNamesSet = [db executeQuery:[NSString stringWithFormat:@"PRAGMA table_info(%@)", tblName]];
    NSMutableArray* columnNames = [[NSMutableArray alloc] init];
    while ([columnNamesSet next]) {
        [columnNames addObject:[columnNamesSet stringForColumn:@"name"]];
    }
    
    return columnNames;
}


#pragma mark ***** Table boards ******

+(void)insertIntoBoardTable:(NSMutableDictionary *)mdict{
    
    [self UpdateQuery:[NSString stringWithFormat:@"DELETE FROM boards"]];
    
    NSMutableArray * marrRes = [mdict objectForKey:@"data"];
    
    for(int i = 0 ; i< marrRes.count ; i++){
        
        [self UpdateQuery:[NSString stringWithFormat:@"Insert into boards(BoardName,BoardFullName,CreatedBy,CreatedOn,ModifiedBy,ModifiedOn) values ('%@','%@','','','','')",[[marrRes objectAtIndex:i] valueForKey:@"BoardName"],[[marrRes   objectAtIndex:i] valueForKey:@"BoardFullName"]]];
        
        
        
    }
    
}

/*
 +(void)getBoardTableData{
 
 FMResultSet *results = [self selectQuery:[NSString stringWithFormat:@"select * FROM boards"]];
 
 
 NSMutableArray * marr = [[NSMutableArray alloc]init];
 
 
 while ([results next])
 {
 NSMutableDictionary* mdict = [[NSMutableDictionary alloc] init];
 
 [mdict setValue:[results stringForColumn:@"BoardID"] forKey:@"BoardID"];
 [mdict setValue:[results stringForColumn:@"BoardName"] forKey:@"BoardName"];
 [mdict setValue:[results stringForColumn:@"BoardFullName"] forKey:@"BoardFullName"];
 
 [marr addObject:mdict];
 
 }
 
 ShareData.marrBoarddata = [NSMutableArray arrayWithArray:marr];
 
 }*/

#pragma mark ***** Table boards_paper ******

+(void)insertIntoboards_paperTable:(NSMutableDictionary *)mdict{
    
    [self UpdateQuery:[NSString stringWithFormat:@"DELETE FROM boards_paper"]];
    
    NSMutableArray * marrRes = [NSMutableArray arrayWithObject:mdict];
    
    for(int i = 0 ; i< [[marrRes objectAtIndex:0] count] ; i++){
        
        [self UpdateQuery:[NSString stringWithFormat:@"Insert into boards_paper(BoardPaperID,BoardPaperName,CreatedBy,CreatedOn,ModifiedBy,ModifiedOn) values ('%@','%@','%@','%@','%@','%@')",[[[marrRes objectAtIndex:0]objectAtIndex:i] valueForKey:@"BoardPaperID"],[[[marrRes objectAtIndex:0] objectAtIndex:i] valueForKey:@"BoardPaperName"],[[[marrRes objectAtIndex:0]  objectAtIndex:i] valueForKey:@"CreatedBy"] , [[[marrRes objectAtIndex:0] objectAtIndex:i] valueForKey:@"CreatedOn"] ,[[[marrRes objectAtIndex:0] objectAtIndex:i] valueForKey:@"ModifiedBy"] ,[[[marrRes objectAtIndex:0]objectAtIndex:i] valueForKey:@"ModifiedOn"]]];
        
        
    }
}

+(void)getboards_paperTableData{
    
    FMResultSet *results = [self selectQuery:[NSString stringWithFormat:@"select * FROM boards_paper"]];
    
    NSMutableArray * marr = [[NSMutableArray alloc]init];
    
    while ([results next])
    {
        NSMutableDictionary* mdict = [[NSMutableDictionary alloc] init];
        
        [mdict setValue:[results stringForColumn:@"BoardPaperID"] forKey:@"BoardPaperID"];
        [mdict setValue:[results stringForColumn:@"BoardPaperName"] forKey:@"BoardPaperName"];
        [mdict setValue:[results stringForColumn:@"CreatedBy"] forKey:@"CreatedBy"];
        [mdict setValue:[results stringForColumn:@"CreatedOn"] forKey:@"CreatedOn"];
        [mdict setValue:[results stringForColumn:@"ModifiedBy"] forKey:@"ModifiedBy"];
        [mdict setValue:[results stringForColumn:@"ModifiedOn"] forKey:@"ModifiedOn"];
        
        [marr addObject:mdict];
        
    }
    
    
    ShareData.marrboards_paperdata = [NSMutableArray arrayWithArray:marr];
    
    
}


#pragma mark ***** Table boards_paper_data ******
/*
 +(void)insertIntoboards_paper_dataTable:(NSMutableDictionary *)mdict{
 
 
 
 
 NSMutableArray * marrRes = [mdict objectForKey:@"data"];
 
 for(int i = 0 ; i< marrRes.count ; i++){
 
 [self UpdateQuery:[NSString stringWithFormat:@"Insert into boards_paper_data(BPDataID,QuestionID,BoardPaperID) values ('%@','%@','%@')",[[marrRes objectAtIndex:i] valueForKey:@"BoardName"],[[marrRes   objectAtIndex:i] valueForKey:@"BoardFullName"] ,[[marrRes   objectAtIndex:i] valueForKey:@"BoardFullName"]]];
 
 }
 
 }
 
 +(void)getboards_paper_dataTableData{
 
 FMResultSet *results = [self selectQuery:[NSString stringWithFormat:@"select * FROM boards_paper_data"]];
 
 
 NSMutableArray * marr = [[NSMutableArray alloc]init];
 
 
 while ([results next])
 {
 NSMutableDictionary* mdict = [[NSMutableDictionary alloc] init];
 
 [mdict setValue:[results stringForColumn:@"BoardID"] forKey:@"BoardID"];
 [mdict setValue:[results stringForColumn:@"BoardName"] forKey:@"BoardName"];
 [mdict setValue:[results stringForColumn:@"BoardFullName"] forKey:@"BoardFullName"];
 
 [marr addObject:mdict];
 
 }
 
 APP_DELEGATE.sharedData.marrboards_paper_data = [NSMutableArray arrayWithArray:marr];
 ////NSLog(@"marrBoardData : %@",APP_DELEGATE.sharedData.marrBoarddata);
 
 }
 */

#pragma mark ***** Table board_paper_files ******

+(void)insertIntoboard_paper_filesTable:(NSMutableDictionary *)mdict{
    
    [self UpdateQuery:[NSString stringWithFormat:@"DELETE FROM board_paper_files"]];
    
    NSMutableArray * marrRes = [mdict objectForKey:@"data"];
    
    for(int i = 0 ; i< marrRes.count ; i++){
        
        [self UpdateQuery:[NSString stringWithFormat:@"Insert into board_paper_files(BoardPaperFileID,BoardID,MediumID,StandardID,SubjectID,BoardPaperID,QFile,QAFile,CreatedBy,CreatedOn,ModifiedBy,ModifiedOn,Rootpath) values ('%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@')",[[marrRes objectAtIndex:i] valueForKey:@"BoardPaperFileID"],[[marrRes   objectAtIndex:i] valueForKey:@"BoardID"],[[marrRes   objectAtIndex:i] valueForKey:@"MediumID"],[[marrRes   objectAtIndex:i] valueForKey:@"StandardID"],[[marrRes   objectAtIndex:i] valueForKey:@"SubjectID"],[[marrRes   objectAtIndex:i] valueForKey:@"BoardPaperID"],[[marrRes   objectAtIndex:i] valueForKey:@"QFile"],[[marrRes   objectAtIndex:i] valueForKey:@"QAFile"],[[marrRes   objectAtIndex:i] valueForKey:@"CreatedBy"],[[marrRes   objectAtIndex:i] valueForKey:@"CreatedOn"],[[marrRes   objectAtIndex:i] valueForKey:@"ModifiedBy"],[[marrRes   objectAtIndex:i] valueForKey:@"ModifiedOn"],[mdict valueForKey:@"root_path"]]];
        
    }
    
}

/*
 +(void)getboard_paper_filesTableData{
 
 FMResultSet *results = [self selectQuery:[NSString stringWithFormat:@"select * FROM board_paper_files"]];
 
 
 NSMutableArray * marr = [[NSMutableArray alloc]init];
 
 
 while ([results next])
 {
 NSMutableDictionary* mdict = [[NSMutableDictionary alloc] init];
 
 [mdict setValue:[results stringForColumn:@"BoardPaperFileID"] forKey:@"BoardPaperFileID"];
 [mdict setValue:[results stringForColumn:@"BoardID"] forKey:@"BoardID"];
 [mdict setValue:[results stringForColumn:@"MediumID"] forKey:@"MediumID"];
 [mdict setValue:[results stringForColumn:@"StandardID"] forKey:@"StandardID"];
 [mdict setValue:[results stringForColumn:@"SubjectID"] forKey:@"SubjectID"];
 [mdict setValue:[results stringForColumn:@"BoardPaperID"] forKey:@"BoardPaperID"];
 [mdict setValue:[results stringForColumn:@"QFile"] forKey:@"QFile"];
 [mdict setValue:[results stringForColumn:@"QAFile"] forKey:@"QAFile"];
 [mdict setValue:[results stringForColumn:@"CreatedBy"] forKey:@"CreatedBy"];
 [mdict setValue:[results stringForColumn:@"CreatedOn"] forKey:@"CreatedOn"];
 [mdict setValue:[results stringForColumn:@"ModifiedBy"] forKey:@"ModifiedBy"];
 [mdict setValue:[results stringForColumn:@"ModifiedOn"] forKey:@"ModifiedOn"];
 [mdict setValue:[results stringForColumn:@"Rootpath"] forKey:@"Rootpath"];
 
 [marr addObject:mdict];
 
 }
 
 ShareData.marrboard_paper_files = [NSMutableArray arrayWithArray:marr];
 
 }
 */


#pragma mark ***** Table chapters ******

+(void)insertintochapter:(NSMutableDictionary *)mdict{
    
    [self UpdateQuery:[NSString stringWithFormat:@"DELETE FROM chapters"]];
    
    NSMutableArray * marrRes = [NSMutableArray arrayWithObject:mdict];
    /*
     NSMutableArray * marrRes1 = [[NSMutableArray alloc ]initWithArray:(NSMutableArray*)mdict];
     //NSLog(@"marrRes : %@",marrRes);
     //NSLog(@"marrRes1  : %@",marrRes1);*/
    
    for(int i = 0 ; i< [[marrRes objectAtIndex:0] count] ; i++){
        
        NSString* ChapterName = [NSString stringWithFormat:@"%@",[[[marrRes objectAtIndex:0] objectAtIndex:i] valueForKey:@"ChapterName"]];
        ChapterName = [ChapterName stringByReplacingOccurrencesOfString:@"'" withString:@""];
        
        [self UpdateQuery:[NSString stringWithFormat:@"Insert into chapters(ChapterID,ChapterNumber,ParentChapterID,BoardID,MediumID,StandardID,SubjectID,ChapterName,isMCQ,DisplayOrder,CreatedBy,CreatedOn,ModifiedBy,ModifiedOn,isGeneratePaper,WeightageMarks,isMandatory,isLibrary,isDemo) values ('%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@')",
                           [[[marrRes objectAtIndex:0] objectAtIndex:i] valueForKey:@"ChapterID"],
                           [[[marrRes objectAtIndex:0] objectAtIndex:i] valueForKey:@"ChapterNumber"],
                           [[[marrRes objectAtIndex:0] objectAtIndex:i] valueForKey:@"ParentChapterID"],
                           [[[marrRes objectAtIndex:0] objectAtIndex:i] valueForKey:@"BoardID"],
                           [[[marrRes objectAtIndex:0] objectAtIndex:i] valueForKey:@"MediumID"],
                           [[[marrRes objectAtIndex:0] objectAtIndex:i] valueForKey:@"StandardID"],
                           [[[marrRes objectAtIndex:0] objectAtIndex:i] valueForKey:@"SubjectID"],
                           ChapterName,
                           [[[marrRes objectAtIndex:0] objectAtIndex:i] valueForKey:@"isMCQ"],
                           [[[marrRes objectAtIndex:0] objectAtIndex:i] valueForKey:@"DisplayOrder"],
                           [[[marrRes objectAtIndex:0] objectAtIndex:i] valueForKey:@"CreatedBy"],
                           [[[marrRes objectAtIndex:0] objectAtIndex:i] valueForKey:@"CreatedOn"],
                           [[[marrRes objectAtIndex:0] objectAtIndex:i] valueForKey:@"ModifiedBy"],
                           [[[marrRes objectAtIndex:0] objectAtIndex:i] valueForKey:@"ModifiedOn"],
                           [[[marrRes objectAtIndex:0] objectAtIndex:i] valueForKey:@"isGeneratePaper"],
                           [[[marrRes objectAtIndex:0] objectAtIndex:i] valueForKey:@"WeightageMarks"],
                           [[[marrRes objectAtIndex:0] objectAtIndex:i] valueForKey:@"isMandatory"],
                           [[[marrRes objectAtIndex:0] objectAtIndex:i] valueForKey:@"isLibrary"],
                           [[[marrRes objectAtIndex:0] objectAtIndex:i] valueForKey:@"isDemo"]]];
        
        
    }
}

/*
 +(void)getchaptersTableData{
 
 FMResultSet *results = [self selectQuery:[NSString stringWithFormat:@"select * FROM chapters"]];
 
 NSMutableArray * marr = [[NSMutableArray alloc]init];
 
 while ([results next])
 {
 NSMutableDictionary* mdict = [[NSMutableDictionary alloc] init];
 
 [mdict setValue:[results stringForColumn:@"ChapterID"] forKey:@"ChapterID"];
 [mdict setValue:[results stringForColumn:@"ParentChapterID"] forKey:@"ParentChapterID"];
 [mdict setValue:[results stringForColumn:@"ChapterNumber"] forKey:@"ChapterNumber"];
 [mdict setValue:[results stringForColumn:@"BoardID"] forKey:@"BoardID"];
 [mdict setValue:[results stringForColumn:@"MediumID"] forKey:@"MediumID"];
 [mdict setValue:[results stringForColumn:@"StandardID"] forKey:@"StandardID"];
 [mdict setValue:[results stringForColumn:@"SubjectID"] forKey:@"SubjectID"];
 [mdict setValue:[results stringForColumn:@"ChapterName"] forKey:@"ChapterName"];
 [mdict setValue:[results stringForColumn:@"CreatedBy"] forKey:@"CreatedBy"];
 [mdict setValue:[results stringForColumn:@"CreatedOn"] forKey:@"CreatedOn"];
 [mdict setValue:[results stringForColumn:@"ModifiedBy"] forKey:@"ModifiedBy"];
 [mdict setValue:[results stringForColumn:@"ModifiedOn"] forKey:@"ModifiedOn"];
 
 [marr addObject:mdict];
 
 }
 
 
 ShareData.marrchapters = [NSMutableArray arrayWithArray:marr];
 
 }*/



#pragma mark ***** Table examtypes ******

+(void)insertIntoexamtypesTable:(NSMutableDictionary *)mdict{
    
    [self UpdateQuery:[NSString stringWithFormat:@"DELETE FROM examtypes"]];
    
    NSMutableArray * marrRes = [NSMutableArray arrayWithObject:mdict];
    
    for(int i = 0 ; i< [[marrRes objectAtIndex:0] count] ; i++){
        
        
        [self UpdateQuery:[NSString stringWithFormat:@"Insert into examtypes(ExamTypeID,ExamTypeName,PaperType,BoardID,MediumID,StandardID,TotalMarks,Duration,Instruction,CreatedBy,CreatedOn,ModifiedBy,ModifiedOn) values ('%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@')",
                           [[[marrRes objectAtIndex:0] objectAtIndex:i] valueForKey:@"ExamTypeID"],
                           [[[marrRes objectAtIndex:0] objectAtIndex:i] valueForKey:@"ExamTypeName"],
                           [[[marrRes objectAtIndex:0] objectAtIndex:i] valueForKey:@"PaperType"],
                           [[[marrRes objectAtIndex:0] objectAtIndex:i] valueForKey:@"BoardID"],
                           [[[marrRes objectAtIndex:0] objectAtIndex:i] valueForKey:@"MediumID"],
                           [[[marrRes objectAtIndex:0] objectAtIndex:i] valueForKey:@"StandardID"],
                           [[[marrRes objectAtIndex:0] objectAtIndex:i] valueForKey:@"TotalMarks"],
                           [[[marrRes objectAtIndex:0] objectAtIndex:i] valueForKey:@"Duration"],
                           [[[marrRes objectAtIndex:0] objectAtIndex:i] valueForKey:@"Instruction"],
                           [[[marrRes objectAtIndex:0] objectAtIndex:i] valueForKey:@"CreatedBy"],
                           [[[marrRes objectAtIndex:0] objectAtIndex:i] valueForKey:@"CreatedOn"],
                           [[[marrRes objectAtIndex:0] objectAtIndex:i] valueForKey:@"ModifiedBy"],
                           [[[marrRes objectAtIndex:0] objectAtIndex:i] valueForKey:@"ModifiedOn"]]];
        
        
    }
}

/*
 +(void)getexamtypesTableData{
 
 FMResultSet *results = [self selectQuery:[NSString stringWithFormat:@"select * FROM examtypes"]];
 
 NSMutableArray * marr = [[NSMutableArray alloc]init];
 
 while ([results next])
 {
 NSMutableDictionary* mdict = [[NSMutableDictionary alloc] init];
 
 [mdict setValue:[results stringForColumn:@"ExamTypeID"] forKey:@"ExamTypeID"];
 [mdict setValue:[results stringForColumn:@"ExamTypeName"] forKey:@"ExamTypeName"];
 [mdict setValue:[results stringForColumn:@"PaperType"] forKey:@"PaperType"];
 [mdict setValue:[results stringForColumn:@"BoardID"] forKey:@"BoardID"];
 [mdict setValue:[results stringForColumn:@"MediumID"] forKey:@"MediumID"];
 [mdict setValue:[results stringForColumn:@"StandardID"] forKey:@"StandardID"];
 [mdict setValue:[results stringForColumn:@"TotalMarks"] forKey:@"TotalMarks"];
 [mdict setValue:[results stringForColumn:@"Duration"] forKey:@"Duration"];
 [mdict setValue:[results stringForColumn:@"Instruction"] forKey:@"Instruction"];
 [mdict setValue:[results stringForColumn:@"CreatedBy"] forKey:@"CreatedBy"];
 [mdict setValue:[results stringForColumn:@"CreatedOn"] forKey:@"CreatedOn"];
 [mdict setValue:[results stringForColumn:@"ModifiedBy"] forKey:@"ModifiedBy"];
 [mdict setValue:[results stringForColumn:@"ModifiedOn"] forKey:@"ModifiedOn"];
 
 [marr addObject:mdict];
 
 }
 
 
 ShareData.marrexamtypes = [NSMutableArray arrayWithArray:marr];
 
 }*/


#pragma mark ***** Table examtype_subject ******

+(void)insertIntoexamtype_subjectTable:(NSMutableDictionary *)mdict{
    
    [self UpdateQuery:[NSString stringWithFormat:@"DELETE FROM examtype_subject"]];
    
    NSMutableArray * marrRes = [NSMutableArray arrayWithObject:mdict];
    
    for(int i = 0 ; i< [[marrRes objectAtIndex:0] count] ; i++){
        
        
        [self UpdateQuery:[NSString stringWithFormat:@"Insert into examtype_subject(ExamTypeSubjectID,ExamTypeID,SubjectID) values ('%@','%@','%@')",
                           [[[marrRes objectAtIndex:0] objectAtIndex:i] valueForKey:@"ExamTypeSubjectID"],
                           [[[marrRes objectAtIndex:0] objectAtIndex:i] valueForKey:@"ExamTypeID"],
                           [[[marrRes objectAtIndex:0] objectAtIndex:i] valueForKey:@"SubjectID"]]];
        
        
    }
    
}

/*
 +(void)getexamtype_subjectTableData{
 
 FMResultSet *results = [self selectQuery:[NSString stringWithFormat:@"select * FROM examtype_subject"]];
 
 
 NSMutableArray * marr = [[NSMutableArray alloc]init];
 
 
 while ([results next])
 {
 NSMutableDictionary* mdict = [[NSMutableDictionary alloc] init];
 
 [mdict setValue:[results stringForColumn:@"ExamTypeSubjectID"] forKey:@"ExamTypeSubjectID"];
 [mdict setValue:[results stringForColumn:@"ExamTypeID"] forKey:@"ExamTypeID"];
 [mdict setValue:[results stringForColumn:@"SubjectID"] forKey:@"SubjectID"];
 
 [marr addObject:mdict];
 
 }
 
 ShareData.marrexamtype_subject = [NSMutableArray arrayWithArray:marr];
 
 }
 */

#pragma mark ***** Table masterquestion ******

+(void)insertIntomasterquestionTable:(NSMutableDictionary *)mdict{
    
    //[self UpdateQuery:[NSString stringWithFormat:@"DELETE FROM masterquestion"]];
    
    NSMutableArray * marrRes = [[NSMutableArray alloc ]initWithArray:(NSMutableArray*)mdict];
    
    for(int i = 0; i< [marrRes count]; i++){
        
        
        [db executeUpdate:@"Insert into masterquestion(MQuestionID,Question,ImageURL,Answer,BoardID,MediumID,ClassID,StandardID,SubjectID,ChapterID,QuestionTypeID,isBoard,ExerciseNo,QuestionNo,Question_Year,is_demo,CreatedBy,CreatedOn,ModifiedBy,ModifiedOn,Solution,is_hide,old_question_id) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
         [[marrRes objectAtIndex:i] valueForKey:@"MQuestionID"],
         [[marrRes objectAtIndex:i] valueForKey:@"Question"],
         [[marrRes objectAtIndex:i] valueForKey:@"ImageURL"],
         [[marrRes objectAtIndex:i] valueForKey:@"Answer"],
         [[marrRes objectAtIndex:i] valueForKey:@"BoardID"],
         [[marrRes objectAtIndex:i] valueForKey:@"MediumID"],
         [[marrRes objectAtIndex:i] valueForKey:@"ClassID"],
         [[marrRes objectAtIndex:i] valueForKey:@"StandardID"],
         [[marrRes objectAtIndex:i] valueForKey:@"SubjectID"],
         [[marrRes objectAtIndex:i] valueForKey:@"ChapterID"],
         [[marrRes objectAtIndex:i] valueForKey:@"QuestionTypeID"],
         [[marrRes objectAtIndex:i] valueForKey:@"isBoard"],
         [[marrRes objectAtIndex:i] valueForKey:@"ExerciseNo"],
         [[marrRes objectAtIndex:i] valueForKey:@"QuestionNo"],
         [[marrRes objectAtIndex:i] valueForKey:@"Question_Year"],
         [[marrRes objectAtIndex:i] valueForKey:@"is_demo"],
         [[marrRes objectAtIndex:i] valueForKey:@"CreatedBy"],
         [[marrRes objectAtIndex:i] valueForKey:@"CreatedOn"],
         [[marrRes objectAtIndex:i] valueForKey:@"ModifiedBy"],
         [[marrRes objectAtIndex:i] valueForKey:@"ModifiedOn"],
         [[marrRes objectAtIndex:i] valueForKey:@"Solution"],
         [[marrRes objectAtIndex:i] valueForKey:@"is_hide"],
         [[marrRes objectAtIndex:i] valueForKey:@"old_question_id"]];
        
        
        
    }
    
}




#pragma mark ***** Table mcqoption ******

+(void)insertIntomcqoptionTable:(NSMutableDictionary *)mdict{
    
    //[self UpdateQuery:[NSString stringWithFormat:@"DELETE FROM mcqoption"]];
    
    // NSMutableArray * marrRes = [NSMutableArray arrayWithObject:mdict];
    
    
    NSMutableArray * marrRes = [[NSMutableArray alloc ]initWithArray:(NSMutableArray*)mdict];
    
    
    
    for(int i = 0 ; i< [marrRes count] ; i++){
        
        NSString* option = [NSString stringWithFormat:@"%@",[[marrRes objectAtIndex:i] valueForKey:@"Options"]];
        option = [option stringByReplacingOccurrencesOfString:@"'" withString:@""];
        
        
        [self UpdateQuery:[NSString stringWithFormat:@"Insert into mcqoption(MCQOPtionID,MCQQuestionID,Options,ImageURL,OptionNo,isCorrect,CreatedBy,CreatedOn,ModifiedBy,ModifiedOn) values ('%@','%@','%@','%@','%@','%@','%@','%@','%@','%@')",
                           [[marrRes  objectAtIndex:i] valueForKey:@"MCQOPtionID"],
                           [[marrRes  objectAtIndex:i] valueForKey:@"MCQQuestionID"],
                           option,
                           [[marrRes  objectAtIndex:i] valueForKey:@"ImageURL"],
                           [[marrRes  objectAtIndex:i] valueForKey:@"OptionNo"],
                           [[marrRes  objectAtIndex:i] valueForKey:@"isCorrect"],
                           [[marrRes  objectAtIndex:i] valueForKey:@"CreatedBy"],
                           [[marrRes objectAtIndex:i] valueForKey:@"CreatedOn"],
                           [[marrRes  objectAtIndex:i] valueForKey:@"ModifiedBy"],
                           [[marrRes objectAtIndex:i] valueForKey:@"ModifiedOn"]]];
        
    }
}



#pragma mark ***** Table mcqquestion ******

+(void)insertIntomcqquestionTable:(NSMutableDictionary *)mdict{
    
    //[self UpdateQuery:[NSString stringWithFormat:@"DELETE FROM mcqquestion"]];
    
    
    NSMutableArray * marrRes = [[NSMutableArray alloc ]initWithArray:(NSMutableArray*)mdict];
    
    for(int i = 0 ; i< [marrRes count] ; i++){
        
        
        [db executeUpdate:@"Insert into mcqquestion(MCQQuestionID,Question,ImageURL,BoardID,MediumID,ClassID,StandardID,SubjectID,ChapterID,QuestionTypeID,QuestionLevelID,Duration,CreatedBy,CreatedOn,ModifiedBy,ModifiedOn,PageNumberID,Solution,is_demo) values  (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",[[marrRes  objectAtIndex:i] valueForKey:@"MCQQuestionID"],
         [[marrRes objectAtIndex:i] valueForKey:@"Question"],
         [[marrRes objectAtIndex:i] valueForKey:@"ImageURL"],
         [[marrRes  objectAtIndex:i] valueForKey:@"BoardID"],
         [[marrRes  objectAtIndex:i] valueForKey:@"MediumID"],
         [[marrRes  objectAtIndex:i] valueForKey:@"ClassID"],
         [[marrRes  objectAtIndex:i] valueForKey:@"StandardID"],
         [[marrRes objectAtIndex:i] valueForKey:@"SubjectID"],
         [[marrRes  objectAtIndex:i] valueForKey:@"ChapterID"],
         [[marrRes objectAtIndex:i] valueForKey:@"QuestionTypeID"],
         [[marrRes  objectAtIndex:i] valueForKey:@"QuestionLevelID"],
         [[marrRes objectAtIndex:i] valueForKey:@"Duration"],
         [[marrRes  objectAtIndex:i] valueForKey:@"CreatedBy"],
         [[marrRes objectAtIndex:i] valueForKey:@"CreatedOn"],
         [[marrRes  objectAtIndex:i] valueForKey:@"ModifiedBy"],
         [[marrRes objectAtIndex:i] valueForKey:@"ModifiedOn"],
         [[marrRes objectAtIndex:i] valueForKey:@"PageNumberID"],
         [[marrRes objectAtIndex:i] valueForKey:@"Solution"],
         [[marrRes objectAtIndex:i] valueForKey:@"is_demo"]];
        
        
        
    }
}





#pragma mark ***** Table mediums ******


+(void)insertIntomediumsTable:(NSMutableDictionary *)mdict{
    
    [self UpdateQuery:[NSString stringWithFormat:@"DELETE FROM mediums"]];
    
    NSMutableArray * marrRes = [mdict objectForKey:@"data"];
    
    for(int i = 0 ; i< marrRes.count ; i++){
        
        
        [self UpdateQuery:[NSString stringWithFormat:@"Insert into mediums(MediumID,BoardID,MediumName,CreatedBy,CreatedOn,ModifiedBy,ModifiedOn) values ('%@','','%@','','','','')",[[marrRes objectAtIndex:i] valueForKey:@"MediumID"],[[marrRes objectAtIndex:i] valueForKey:@"MediumName"]]];
        
        
        
    }
    
}

/*
 +(void)getmediumsTableData{
 
 FMResultSet *results = [self selectQuery:[NSString stringWithFormat:@"select * FROM mediums"]];
 
 
 NSMutableArray * marr = [[NSMutableArray alloc]init];
 
 
 while ([results next])
 {
 NSMutableDictionary* mdict = [[NSMutableDictionary alloc] init];
 
 [mdict setValue:[results stringForColumn:@"MediumID"] forKey:@"MediumID"];
 [mdict setValue:[results stringForColumn:@"MediumName"] forKey:@"MediumName"];
 
 
 [marr addObject:mdict];
 
 }
 
 
 ShareData.marrmediums = [NSMutableArray arrayWithArray:marr];
 
 }*/

#pragma mark ***** Table question_types ******

+(void)insertIntoquestion_typesTable:(NSMutableDictionary *)mdict{
    
    [self UpdateQuery:[NSString stringWithFormat:@"DELETE FROM question_types"]];
    
    NSMutableArray * marrRes = [[NSMutableArray alloc ]initWithArray:(NSMutableArray*)mdict];
    
    for(int i = 0 ; i< [marrRes count] ; i++){
        
        
        [self UpdateQuery:[NSString stringWithFormat:@"Insert into question_types(QuestionTypeID,QuestionType,BoardID,MediumID,ClassID,StandardID,SubjectID,Marks,Type,RevisionNo,CreatedBy,CreatedOn,ModifiedBy,ModifiedOn,isMTC) values ('%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@')",
                           [[marrRes  objectAtIndex:i] valueForKey:@"QuestionTypeID"],
                           [[marrRes  objectAtIndex:i] valueForKey:@"QuestionType"],
                           [[marrRes objectAtIndex:i] valueForKey:@"BoardID"],
                           [[marrRes  objectAtIndex:i] valueForKey:@"MediumID"],
                           [[marrRes  objectAtIndex:i] valueForKey:@"ClassID"],
                           [[marrRes  objectAtIndex:i] valueForKey:@"StandardID"],
                           [[marrRes  objectAtIndex:i] valueForKey:@"SubjectID"],
                           [[marrRes objectAtIndex:i] valueForKey:@"Marks"],
                           [[marrRes  objectAtIndex:i] valueForKey:@"Type"],
                           [[marrRes  objectAtIndex:i] valueForKey:@"RevisionNo"],
                           [[marrRes  objectAtIndex:i] valueForKey:@"CreatedBy"],
                           [[marrRes  objectAtIndex:i] valueForKey:@"CreatedOn"],
                           [[marrRes objectAtIndex:i] valueForKey:@"ModifiedBy"],
                           [[marrRes  objectAtIndex:i] valueForKey:@"ModifiedOn"],
                           [[marrRes objectAtIndex:i] valueForKey:@"isMTC"]]];
    }
    
}

/*
 +(void)getquestion_typesTableData{
 
 FMResultSet *results = [self selectQuery:[NSString stringWithFormat:@"select * FROM question_types"]];
 
 NSMutableArray * marr = [[NSMutableArray alloc]init];
 
 while ([results next])
 {
 NSMutableDictionary* mdict = [[NSMutableDictionary alloc] init];
 
 [mdict setValue:[results stringForColumn:@"QuestionTypeID"] forKey:@"QuestionTypeID"];
 [mdict setValue:[results stringForColumn:@"QuestionType"] forKey:@"QuestionType"];
 [mdict setValue:[results stringForColumn:@"BoardID"] forKey:@"BoardID"];
 [mdict setValue:[results stringForColumn:@"MediumID"] forKey:@"MediumID"];
 [mdict setValue:[results stringForColumn:@"ClassID"] forKey:@"ClassID"];
 [mdict setValue:[results stringForColumn:@"StandardID"] forKey:@"StandardID"];
 [mdict setValue:[results stringForColumn:@"SubjectID"] forKey:@"SubjectID"];
 [mdict setValue:[results stringForColumn:@"Marks"] forKey:@"Marks"];
 [mdict setValue:[results stringForColumn:@"Type"] forKey:@"Type"];
 [mdict setValue:[results stringForColumn:@"RevisionNo"] forKey:@"RevisionNo"];
 [mdict setValue:[results stringForColumn:@"CreatedBy"] forKey:@"CreatedBy"];
 [mdict setValue:[results stringForColumn:@"CreatedOn"] forKey:@"CreatedOn"];
 [mdict setValue:[results stringForColumn:@"ModifiedBy"] forKey:@"ModifiedBy"];
 [mdict setValue:[results stringForColumn:@"ModifiedOn"] forKey:@"ModifiedOn"];
 
 [marr addObject:mdict];
 
 }
 
 ShareData.marrquestion_types = [NSMutableArray arrayWithArray:marr];
 
 }*/

#pragma mark ***** Table standards ******


+(void)insertIntostandardsTable:(NSMutableDictionary *)mdict{
    
    [self UpdateQuery:[NSString stringWithFormat:@"DELETE FROM standards"]];
    
    NSMutableArray * marrRes = [mdict objectForKey:@"data"];
    
    for(int i = 0 ; i< marrRes.count ; i++){
        
        
        [self UpdateQuery:[NSString stringWithFormat:@"Insert into standards(StandardID,StandardName,BoardID,MediumID,CreatedBy,CreatedOn,ModifiedBy,ModifiedOn) values ('%@','%@','','','','','','')",[[marrRes objectAtIndex:i] valueForKey:@"StandardID"],[[marrRes objectAtIndex:i] valueForKey:@"StandardName"]]];
        
        
        
    }
    
}

/*
 +(void)getstandardsTableData{
 
 FMResultSet *results = [self selectQuery:[NSString stringWithFormat:@"select * FROM standards"]];
 
 
 NSMutableArray * marr = [[NSMutableArray alloc]init];
 
 
 while ([results next])
 {
 NSMutableDictionary* mdict = [[NSMutableDictionary alloc] init];
 
 [mdict setValue:[results stringForColumn:@"StandardID"] forKey:@"StandardID"];
 [mdict setValue:[results stringForColumn:@"StandardName"] forKey:@"StandardName"];
 
 
 [marr addObject:mdict];
 
 }
 
 
 ShareData.marrstandards = [NSMutableArray arrayWithArray:marr];
 
 }*/

#pragma mark ***** Table subjects ******

+(void)insertIntosubjectsTable:(NSMutableDictionary *)mdict{
    
    [self UpdateQuery:[NSString stringWithFormat:@"DELETE FROM subjects"]];
    
    NSMutableArray * marrRes = [[NSMutableArray alloc ]initWithArray:(NSMutableArray*)mdict];
    
    for(int i = 0 ; i< [marrRes count] ; i++){
        
        
        [self UpdateQuery:[NSString stringWithFormat:@"Insert into subjects(SubjectID,SubjectName,BoardID,MediumID,StandardID,CreatedBy,CreatedOn,ModifiedBy,SubjectIcon,ModifiedOn,Price,SubjectOrder,isGeneratePaper,isMCQ,isWEIGHTAGE,Instruction) values ('%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@')",
                           [[marrRes objectAtIndex:i] valueForKey:@"SubjectID"],
                           [[marrRes objectAtIndex:i] valueForKey:@"SubjectName"],
                           [[marrRes objectAtIndex:i] valueForKey:@"BoardID"],
                           [[marrRes objectAtIndex:i] valueForKey:@"MediumID"],
                           [[marrRes objectAtIndex:i] valueForKey:@"StandardID"],
                           [[marrRes objectAtIndex:i] valueForKey:@"CreatedBy"],
                           [[marrRes objectAtIndex:i] valueForKey:@"CreatedOn"],
                           [[marrRes objectAtIndex:i] valueForKey:@"ModifiedBy"],
                           [[marrRes objectAtIndex:i] valueForKey:@"SubjectIcon"],
                           [[marrRes objectAtIndex:i] valueForKey:@"ModifiedOn"],
                           [[marrRes objectAtIndex:i] valueForKey:@"Price"],
                           [[marrRes objectAtIndex:i] valueForKey:@"SubjectOrder"],
                           [[marrRes objectAtIndex:i] valueForKey:@"isGeneratePaper"],
                           [[marrRes objectAtIndex:i] valueForKey:@"isMCQ"],
                           [[marrRes objectAtIndex:i] valueForKey:@"isWEIGHTAGE"],
                           [[marrRes objectAtIndex:i] valueForKey:@"Instruction"]]];
    }
    
}



+(void)getsubjects{
    
    
    NSString* subjects = [NSString stringWithFormat:@"%@",[UserDefault valueForKey:@"subjects"]];
    subjects = [subjects stringByReplacingOccurrencesOfString:@"," withString:@"','"];
    
    FMResultSet *results = [self selectQuery:[NSString stringWithFormat:@"select * FROM subjects where SubjectID IN ('%@') AND BoardID = '%@' AND MediumID = '%@' AND StandardID = '%@' AND isGeneratePaper='1' ORDER BY SubjectOrder ASC",subjects,[UserDefault valueForKey:@"BoardID"],[UserDefault valueForKey:@"MediumID"],[UserDefault valueForKey:@"StandardID"]]];
    
    
    
    NSMutableArray * marr = [[NSMutableArray alloc]init];
    
    
    while ([results next])
    {
        NSMutableDictionary* mdict = [[NSMutableDictionary alloc] init];
        
        [mdict setValue:[results stringForColumn:@"SubjectID"] forKey:@"SubjectID"];
        [mdict setValue:[results stringForColumn:@"SubjectName"] forKey:@"SubjectName"];
        [mdict setValue:[results stringForColumn:@"BoardID"] forKey:@"BoardID"];
        [mdict setValue:[results stringForColumn:@"MediumID"] forKey:@"MediumID"];
        [mdict setValue:[results stringForColumn:@"StandardID"] forKey:@"StandardID"];
        [mdict setValue:[results stringForColumn:@"CreatedBy"] forKey:@"CreatedBy"];
        [mdict setValue:[results stringForColumn:@"CreatedOn"] forKey:@"CreatedOn"];
        [mdict setValue:[results stringForColumn:@"SubjectIcon"] forKey:@"SubjectIcon"];
        [mdict setValue:[results stringForColumn:@"ModifiedOn"] forKey:@"ModifiedOn"];
        [mdict setValue:[results stringForColumn:@"Price"] forKey:@"Price"];
        [mdict setValue:[results stringForColumn:@"SubjectOrder"] forKey:@"SubjectOrder"];
        [mdict setValue:[results stringForColumn:@"ModifiedBy"] forKey:@"ModifiedBy"];
        
        
        
        [marr addObject:mdict];
        
    }
    
    
    ShareData.marrsubjects = [NSMutableArray arrayWithArray:marr];
    
    NSLog(@"marrsubjects :%@",ShareData.marrsubjects);
    
}

#pragma mark ***** Table paper_type ******

+(void)insertIntopaper_typeTable:(NSMutableDictionary *)mdict{
    
    [self UpdateQuery:[NSString stringWithFormat:@"DELETE FROM paper_type"]];
    
    NSMutableArray * marrRes = [[NSMutableArray alloc ]initWithArray:(NSMutableArray*)mdict];
    
    for(int i = 0 ; i< [marrRes count] ; i++){
        
        [self UpdateQuery:[NSString stringWithFormat:@"Insert into paper_type(PaperTypeID,PaperTypeName) values ('%@','%@')",
                           [[marrRes  objectAtIndex:i] valueForKey:@"PaperTypeID"],
                           [[marrRes  objectAtIndex:i] valueForKey:@"PaperTypeName"]]];
    }
    
}

///////////////INSERT DATA METHOD/////////////////////////
+(void)insertInto_papertype:(NSMutableDictionary *)mdict :(NSString *)tblnm
{
    
    
    NSLog(@"--%@",mdict);
    NSString *insertQuery=@"", *keyQuery = @"", *valueQuery=@"";
    NSString * numberReg = @"[0-9]";
    
    NSPredicate * numberCheck = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", numberReg];
    
    if([mdict count] > 0) {
        insertQuery = [NSString stringWithFormat:@"Insert into %@ (",tblnm];
        
        NSMutableArray* columnNames = [self getAllColumnNames:tblnm];
        for(id key in mdict) {
            if(  [columnNames containsObject:key] != NSNotFound){
          //  if([columnNames containsObject:key]){
                NSString *repalcedString =  [mdict valueForKey:key];
                if (![numberCheck evaluateWithObject:repalcedString]){
                    @try {
                        if ([repalcedString rangeOfString:@"\'"].location != NSNotFound) {
                            repalcedString =  [repalcedString stringByReplacingOccurrencesOfString:@"\'" withString:@"\''"];
                        }
                    }
                    @catch (NSException *exception) {
                        NSLog(@"%@", exception.reason);
                    }
                    @finally {
                        
                    }
                }
                if([keyQuery isEqualToString:@""]){
                    keyQuery = [NSString stringWithFormat:@"%@,",key];
                    valueQuery = [NSString stringWithFormat:@" \'%@\' ,",repalcedString];
                    
                } else {
                    keyQuery = [NSString stringWithFormat:@"%@ %@,",keyQuery,key];
                    valueQuery = [NSString stringWithFormat:@"%@ \'%@\' ,",valueQuery,repalcedString];
                }
            }
        }
        
        
        //// removing extra , at end;
        keyQuery = [keyQuery substringToIndex:keyQuery.length-(keyQuery.length>0)];
        valueQuery = [valueQuery substringToIndex:valueQuery.length-(valueQuery.length>0)];
        
        //// adding remaining ) at end
        keyQuery = [NSString stringWithFormat:@"%@ )",keyQuery];
        valueQuery = [NSString stringWithFormat:@"%@ )",valueQuery];
        
        //// joinng the key and values for insert
        insertQuery = [NSString stringWithFormat:@"%@ %@ values (%@",insertQuery,keyQuery,valueQuery];
        NSLog(@"URL --%@",insertQuery);
        [self UpdateQuery:insertQuery];
    }
    
    
}





+(void)getpaper_typeTableData{
    
    FMResultSet *results = [self selectQuery:[NSString stringWithFormat:@"select * FROM paper_type"]];
    
    NSMutableArray * marr = [[NSMutableArray alloc]init];
    
    while ([results next])
    {
        NSMutableDictionary* mdict = [[NSMutableDictionary alloc] init];
        
        [mdict setValue:[results stringForColumn:@"PaperTypeID"] forKey:@"PaperTypeID"];
        [mdict setValue:[results stringForColumn:@"PaperTypeName"] forKey:@"PaperTypeName"];
        
        
        [marr addObject:mdict];
        
    }
    
    ShareData.marrpaper_type = [NSMutableArray arrayWithArray:marr];
    
    NSLog(@"ShareData.marrpaper_type : %@",ShareData.marrpaper_type);
    
}


#pragma mark ***** Table exam_type_pattern ******

+(void)insertIntoexam_type_patternTable:(NSMutableDictionary *)mdict{
    
    [self UpdateQuery:[NSString stringWithFormat:@"DELETE FROM exam_type_pattern"]];
    
    NSMutableArray * marrRes = [[NSMutableArray alloc ]initWithArray:(NSMutableArray*)mdict];
    
    for(int i = 0 ; i< [marrRes count] ; i++){
        
        
        [self UpdateQuery:[NSString stringWithFormat:@"Insert into exam_type_pattern(ExamTypePatternID,BoardID,MediumID,StandardID,SubjectID,ExamTypeID,TotalMarks,Duration,Instruction,PatternNo,CreatedBy,CreatedOn,ModifiedBy,ModifiedOn) values ('%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@')",
                           [[marrRes  objectAtIndex:i] valueForKey:@"ExamTypePatternID"],
                           [[marrRes  objectAtIndex:i] valueForKey:@"BoardID"],
                           [[marrRes  objectAtIndex:i] valueForKey:@"MediumID"],
                           [[marrRes  objectAtIndex:i] valueForKey:@"StandardID"],
                           [[marrRes  objectAtIndex:i] valueForKey:@"SubjectID"],
                           [[marrRes  objectAtIndex:i] valueForKey:@"ExamTypeID"],
                           [[marrRes  objectAtIndex:i] valueForKey:@"TotalMarks"],
                           [[marrRes  objectAtIndex:i] valueForKey:@"Duration"],
                           [[marrRes  objectAtIndex:i] valueForKey:@"Instruction"],
                           [[marrRes  objectAtIndex:i] valueForKey:@"PatternNo"],
                           [[marrRes  objectAtIndex:i] valueForKey:@"CreatedBy"],
                           [[marrRes  objectAtIndex:i] valueForKey:@"CreatedOn"],
                           [[marrRes  objectAtIndex:i] valueForKey:@"ModifiedBy"],
                           [[marrRes  objectAtIndex:i] valueForKey:@"ModifiedOn"]]];
    }
    
}






#pragma mark ***** Table exam_type_pattern_detail ******

+(void)insertIntoexam_type_pattern_detailTable:(NSMutableDictionary *)mdict{
    
    [self UpdateQuery:[NSString stringWithFormat:@"DELETE FROM exam_type_pattern_detail"]];
    
    NSMutableArray * marrRes = [[NSMutableArray alloc ]initWithArray:(NSMutableArray*)mdict];
    
    for(int i = 0 ; i< [marrRes count] ; i++){
        
        NSString* QuestionTypeText = [NSString stringWithFormat:@"%@",[[marrRes  objectAtIndex:i] valueForKey:@"QuestionTypeText"]];
        QuestionTypeText = [QuestionTypeText stringByReplacingOccurrencesOfString:@"'" withString:@""];
        
        [self UpdateQuery:[NSString stringWithFormat:@"Insert into exam_type_pattern_detail(ExamTypePatternDetailID,ExamTypePatternID,ExamTypeID,QuestionTypeID,QuestionNO,SubQuestionNO,QuestionTypeText,QuestionMarks,NoOfQuestion,DisplayOrder,isQuestion,isQuestionVisible,ChapterID,CreatedBy,CreatedOn,ModifiedBy,ModifiedOn) values ('%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@')",
                           [[marrRes  objectAtIndex:i] valueForKey:@"ExamTypePatternDetailID"],
                           [[marrRes  objectAtIndex:i] valueForKey:@"ExamTypePatternID"],
                           [[marrRes  objectAtIndex:i] valueForKey:@"ExamTypeID"],
                           [[marrRes  objectAtIndex:i] valueForKey:@"QuestionTypeID"],
                           [[marrRes  objectAtIndex:i] valueForKey:@"QuestionNO"],
                           [[marrRes  objectAtIndex:i] valueForKey:@"SubQuestionNO"],
                           QuestionTypeText,
                           [[marrRes  objectAtIndex:i] valueForKey:@"QuestionMarks"],
                           [[marrRes  objectAtIndex:i] valueForKey:@"NoOfQuestion"],
                           [[marrRes  objectAtIndex:i] valueForKey:@"DisplayOrder"],
                           [[marrRes  objectAtIndex:i] valueForKey:@"isQuestion"],
                           [[marrRes  objectAtIndex:i] valueForKey:@"isQuestionVisible"],
                           [[marrRes  objectAtIndex:i] valueForKey:@"ChapterID"],
                           [[marrRes  objectAtIndex:i] valueForKey:@"CreatedBy"],
                           [[marrRes  objectAtIndex:i] valueForKey:@"CreatedOn"],
                           [[marrRes  objectAtIndex:i] valueForKey:@"ModifiedBy"],
                           [[marrRes  objectAtIndex:i] valueForKey:@"ModifiedOn"]]];
    }
    
}



#pragma mark ***** ADD New Collum Into student_question_paper******


+(void)addCollumIntostudent_question_paper{
    
    if (![db columnExists:@"SubjectID" inTableWithName:@"student_question_paper"])
    {
        [db executeUpdate:@"ALTER TABLE student_question_paper ADD COLUMN SubjectID varchar (15)"];
    }
    
    if (![db columnExists:@"PaperTypeID" inTableWithName:@"student_question_paper"])
    {
        [db executeUpdate:@"ALTER TABLE student_question_paper ADD COLUMN PaperTypeID varchar (15)"];
    }
    
    if (![db columnExists:@"TotalMarks" inTableWithName:@"student_question_paper"])
    {
        [db executeUpdate:@"ALTER TABLE student_question_paper ADD COLUMN TotalMarks varchar (15)"];
    }
    
    if (![db columnExists:@"Duration" inTableWithName:@"student_question_paper"])
    {
        [db executeUpdate:@"ALTER TABLE student_question_paper ADD COLUMN Duration varchar (15)"];
    }
    
    
    
}

#pragma mark ***** ADD New Table Into Database ******

+(void)addNewTable{
    
    if(![db tableExists:@"student_set_paper_question_type"])
    {
        [self UpdateQuery:[NSString stringWithFormat:@"create table if not exists student_set_paper_question_type( StudentSetPaperQuestionTypeID integer PRIMARY KEY AUTOINCREMENT , StudentQuestionPaperID integer (11)  , QuestionTypeID  integer (11) , TotalAsk varchar(11) , ToAnswer  varchar(11) , TotalMark varchar(11))"]];
        
    }
    
    if(![db tableExists:@"student_set_paper_detail"])
    {
        [self UpdateQuery:[NSString stringWithFormat:@"create table if not exists student_set_paper_detail( StudentSetPaperDetailID integer PRIMARY KEY AUTOINCREMENT , StudentSetPaperQuestionTypeID integer (11)  , MQuestionID  integer (11))"]];
        
    }
    
}
#pragma mark ***** Get Table Data******



+(void)getstudent_mcq_test_hdrData:(NSString *)strQuery{
    
    FMResultSet *results = [self selectQuery:strQuery];
    
    NSMutableArray* marr = [[NSMutableArray alloc]init];
    
    
    while ([results next])
    {
        NSMutableDictionary* mdict = [[NSMutableDictionary alloc] init];
        
        [mdict setValue:[results stringForColumn:@"StudentMCQTestHDRID"] forKey:@"StudentMCQTestHDRID"];
        [mdict setValue:[results stringForColumn:@"CreatedOn"] forKey:@"CreatedOn"];
        [mdict setValue:[results stringForColumn:@"SubjectName"] forKey:@"SubjectName"];
        
        [marr addObject:mdict];
        
    }
    
    ShareData.marrstudent_mcq_test_hdr = [NSMutableArray arrayWithArray:marr];
    
    NSLog(@"marrstudent_mcq_test_hdr : %@" ,ShareData.marrstudent_mcq_test_hdr);
    
}

+(void)getstudent_mcq_test_chapterData:(NSString *)strQuery{
    
    
    FMResultSet *results = [self selectQuery:strQuery];
    
    NSMutableArray * marr = [[NSMutableArray alloc]init];
    
    
    while ([results next])
    {
        NSMutableDictionary* mdict = [[NSMutableDictionary alloc] init];
        
        [mdict setValue:[results stringForColumn:@"StudentMCQTestChapterID"] forKey:@"StudentMCQTestChapterID"];
        [mdict setValue:[results stringForColumn:@"StudentMCQTestHDRID"] forKey:@"StudentMCQTestHDRID"];
        [mdict setValue:[results stringForColumn:@"ChapterID"] forKey:@"ChapterID"];
        
        [marr addObject:mdict];
        
    }
    
    ShareData.marrstudent_mcq_test_chapter = [NSMutableArray arrayWithArray:marr];
    NSLog(@"marrstudent_mcq_test_chapter :%@",ShareData.marrstudent_mcq_test_chapter);
    
    
}

+(void)getstudent_mcq_test_dtlData:(NSString *)strQuery{
    
    
    FMResultSet *results = [self selectQuery:strQuery];
    
    NSMutableArray * marr = [[NSMutableArray alloc]init];
    
    
    
    while ([results next])
    {
        NSMutableDictionary* mdict = [[NSMutableDictionary alloc] init];
        
        [mdict setValue:[results stringForColumn:@"StudentMCQTestDTLID"] forKey:@"StudentMCQTestDTLID"];
        [mdict setValue:[results stringForColumn:@"StudentMCQTestHDRID"] forKey:@"StudentMCQTestHDRID"];
        [mdict setValue:[results stringForColumn:@"QuestionID"] forKey:@"QuestionID"];
        [mdict setValue:[results stringForColumn:@"AnswerID"] forKey:@"AnswerID"];
        [mdict setValue:[results stringForColumn:@"IsAttempt"] forKey:@"IsAttempt"];
        [mdict setValue:[results stringForColumn:@"srNo"] forKey:@"srNo"];
        [mdict setValue:[results stringForColumn:@"CreatedBy"] forKey:@"CreatedBy"];
        [mdict setValue:[results stringForColumn:@"CreatedOn"] forKey:@"CreatedOn"];
        [mdict setValue:[results stringForColumn:@"ModifiedBy"] forKey:@"ModifiedBy"];
        [mdict setValue:[results stringForColumn:@"ModifiedOn"] forKey:@"ModifiedOn"];
        [mdict setValue:[NSString stringWithFormat:@"%d",false] forKey:@"IsRight"];
        
        [marr addObject:mdict];
        
    }
    
    ShareData.marrstudent_mcq_test_dtl = [NSMutableArray arrayWithArray:marr];
    NSLog(@"student_mcq_test_dtl :%@",ShareData.marrstudent_mcq_test_dtl);
    
    
}

+(void)getexam_type_patternTableData:(NSString *)strQuery{
    
    FMResultSet *results = [self selectQuery:strQuery];
    
    NSMutableArray * marr = [[NSMutableArray alloc]init];
    
    while ([results next])
    {
        NSMutableDictionary* mdict = [[NSMutableDictionary alloc] init];
        
        [mdict setValue:[results stringForColumn:@"ExamTypePatternID"] forKey:@"ExamTypePatternID"];
        [mdict setValue:[results stringForColumn:@"BoardID"] forKey:@"BoardID"];
        [mdict setValue:[results stringForColumn:@"MediumID"] forKey:@"MediumID"];
        [mdict setValue:[results stringForColumn:@"StandardID"] forKey:@"StandardID"];
        [mdict setValue:[results stringForColumn:@"SubjectID"] forKey:@"SubjectID"];
        [mdict setValue:[results stringForColumn:@"ExamTypeID"] forKey:@"ExamTypeID"];
        [mdict setValue:[results stringForColumn:@"TotalMarks"] forKey:@"TotalMarks"];
        [mdict setValue:[results stringForColumn:@"Duration"] forKey:@"Duration"];
        [mdict setValue:[results stringForColumn:@"Instruction"] forKey:@"Instruction"];
        [mdict setValue:[results stringForColumn:@"PatternNo"] forKey:@"PatternNo"];
        [mdict setValue:[results stringForColumn:@"CreatedBy"] forKey:@"CreatedBy"];
        [mdict setValue:[results stringForColumn:@"CreatedOn"] forKey:@"CreatedOn"];
        [mdict setValue:[results stringForColumn:@"ModifiedBy"] forKey:@"ModifiedBy"];
        [mdict setValue:[results stringForColumn:@"ModifiedOn"] forKey:@"ModifiedOn"];
        
        
        
        [marr addObject:mdict];
        
    }
    
    ShareData.marrexam_type_pattern = [NSMutableArray arrayWithArray:marr];
    NSLog(@"marrexam_type_pattern : - %@",ShareData.marrexam_type_pattern);
    
}


+(void)getexam_type_pattern_detailTableData:(NSString *)strQuery{
    
    FMResultSet *results = [self selectQuery:strQuery];
    
    NSMutableArray * marr = [[NSMutableArray alloc]init];
    
    while ([results next])
    {
        NSMutableDictionary* mdict = [[NSMutableDictionary alloc] init];
        
        [mdict setValue:[results stringForColumn:@"ExamTypePatternDetailID"] forKey:@"ExamTypePatternDetailID"];
        [mdict setValue:[results stringForColumn:@"ExamTypePatternID"] forKey:@"ExamTypePatternID"];
        [mdict setValue:[results stringForColumn:@"ExamTypeID"] forKey:@"ExamTypeID"];
        [mdict setValue:[results stringForColumn:@"QuestionTypeID"] forKey:@"QuestionTypeID"];
        [mdict setValue:[results stringForColumn:@"QuestionNO"] forKey:@"QuestionNO"];
        [mdict setValue:[results stringForColumn:@"SubQuestionNO"] forKey:@"SubQuestionNO"];
        [mdict setValue:[results stringForColumn:@"QuestionTypeText"] forKey:@"QuestionTypeText"];
        [mdict setValue:[results stringForColumn:@"QuestionMarks"] forKey:@"QuestionMarks"];
        [mdict setValue:[results stringForColumn:@"NoOfQuestion"] forKey:@"NoOfQuestion"];
        [mdict setValue:[results stringForColumn:@"DisplayOrder"] forKey:@"DisplayOrder"];
        [mdict setValue:[results stringForColumn:@"isQuestion"] forKey:@"isQuestion"];
        [mdict setValue:[results stringForColumn:@"isQuestionVisible"] forKey:@"isQuestionVisible"];
        [mdict setValue:[results stringForColumn:@"ChapterID"] forKey:@"ChapterID"];
        [mdict setValue:[results stringForColumn:@"CreatedBy"] forKey:@"CreatedBy"];
        [mdict setValue:[results stringForColumn:@"CreatedOn"] forKey:@"CreatedOn"];
        [mdict setValue:[results stringForColumn:@"ModifiedBy"] forKey:@"ModifiedBy"];
        [mdict setValue:[results stringForColumn:@"ModifiedOn"] forKey:@"ModifiedOn"];
        
        
        
        [marr addObject:mdict];
        
    }
    
    ShareData.marrexam_type_pattern_detail = [NSMutableArray arrayWithArray:marr];
    NSLog(@"marrexam_type_pattern_detail : - %@",ShareData.marrexam_type_pattern_detail);
    
}

+(void)getstudent_question_paperData:(NSString *)strQuery{
    
    
    FMResultSet *results = [self selectQuery:strQuery];
    
    NSMutableArray * marr = [[NSMutableArray alloc]init];
    
    
    while ([results next])
    {
        NSMutableDictionary* mdict = [[NSMutableDictionary alloc] init];
        
        [mdict setValue:[results stringForColumn:@"StudentQuestionPaperID"] forKey:@"StudentQuestionPaperID"];
        [mdict setValue:[results stringForColumn:@"ExamTypePatternID"] forKey:@"ExamTypePatternID"];
        [mdict setValue:[results stringForColumn:@"StudentID"] forKey:@"StudentID"];
        [mdict setValue:[results stringForColumn:@"CreatedBy"] forKey:@"CreatedBy"];
        [mdict setValue:[results stringForColumn:@"CreatedOn"] forKey:@"CreatedOn"];
        [mdict setValue:[results stringForColumn:@"StudentID"] forKey:@"StudentID"];
        [mdict setValue:[results stringForColumn:@"ModifiedBy"] forKey:@"ModifiedBy"];
        [mdict setValue:[results stringForColumn:@"ModifiedOn"] forKey:@"ModifiedOn"];
        [mdict setValue:[results stringForColumn:@"SubjectID"] forKey:@"SubjectID"];
        [mdict setValue:[results stringForColumn:@"PaperTypeID"] forKey:@"PaperTypeID"];
        [mdict setValue:[results stringForColumn:@"TotalMarks"] forKey:@"TotalMarks"];
        [mdict setValue:[results stringForColumn:@"Duration"] forKey:@"Duration"];
        
        
        [marr addObject:mdict];
        
    }
    
    ShareData.marrstudent_question_paper = [NSMutableArray arrayWithArray:marr];
    NSLog(@"marrstudent_question_paper :%@",ShareData.marrstudent_question_paper);
    
    
}
/*
 +(void)getstudent_set_paper_question_typeData:(NSString *)strQuery{
 
 FMResultSet *results = [self selectQuery:strQuery];
 
 NSMutableArray * marr = [[NSMutableArray alloc]init];
 
 
 while ([results next])
 {
 NSMutableDictionary* mdict = [[NSMutableDictionary alloc] init];
 
 [mdict setValue:[results stringForColumn:@"StudentQuestionPaperID"] forKey:@"StudentQuestionPaperID"];
 [mdict setValue:[results stringForColumn:@"ExamTypePatternID"] forKey:@"ExamTypePatternID"];
 [mdict setValue:[results stringForColumn:@"StudentID"] forKey:@"StudentID"];
 [mdict setValue:[results stringForColumn:@"CreatedBy"] forKey:@"CreatedBy"];
 [mdict setValue:[results stringForColumn:@"CreatedOn"] forKey:@"CreatedOn"];
 [mdict setValue:[results stringForColumn:@"ModifiedBy"] forKey:@"ModifiedBy"];
 [mdict setValue:[results stringForColumn:@"ModifiedOn"] forKey:@"ModifiedOn"];
 
 
 [marr addObject:mdict];
 
 }
 
 ShareData.marrstudent_set_paper_question_type = [NSMutableArray arrayWithArray:marr];
 NSLog(@"marrstudent_set_paper_question_type :%@",ShareData.marrstudent_set_paper_question_type);
 
 
 }*/
+(void)getstudent_set_paper_question_typeData:(NSString *)strQuery{
    
    FMResultSet *results = [self selectQuery:strQuery];
    
    NSMutableArray * marr = [[NSMutableArray alloc]init];
    
    
    while ([results next])
    {
        NSMutableDictionary* mdict = [[NSMutableDictionary alloc] init];
        
        
        [mdict setValue:[results stringForColumn:@"StudentQuestionPaperID"] forKey:@"StudentQuestionPaperID"];
        [mdict setValue:[results stringForColumn:@"QuestionTypeID"] forKey:@"QuestionTypeID"];
        [mdict setValue:[results stringForColumn:@"TotalAsk"] forKey:@"TotalAsk"];
        [mdict setValue:[results stringForColumn:@"ToAnswer"] forKey:@"ToAnswer"];
        [mdict setValue:[results stringForColumn:@"TotalMark"] forKey:@"TotalMark"];
        [mdict setValue:[results stringForColumn:@"StudentSetPaperQuestionTypeID"] forKey:@"StudentSetPaperQuestionTypeID"];
        
        
        
        [marr addObject:mdict];
        
    }
    
    ShareData.marrstudent_set_paper_question_type = [NSMutableArray arrayWithArray:marr];
    NSLog(@"marrstudent_set_paper_question_type :%@",ShareData.marrstudent_set_paper_question_type);
    
    
}


+(void)getstudent_set_paper_detailData:(NSString *)strQuery{
    
    FMResultSet *results = [self selectQuery:strQuery];
    
    NSMutableArray * marr = [[NSMutableArray alloc]init];
    
    
    while ([results next])
    {
        NSMutableDictionary* mdict = [[NSMutableDictionary alloc] init];
        
        [mdict setValue:[results stringForColumn:@"StudentSetPaperDetailID"] forKey:@"StudentSetPaperDetailID"];
        [mdict setValue:[results stringForColumn:@"StudentSetPaperQuestionTypeID"] forKey:@"StudentSetPaperQuestionTypeID"];
        [mdict setValue:[results stringForColumn:@"MQuestionID"] forKey:@"MQuestionID"];
        
        [marr addObject:mdict];
        
    }
    
    ShareData.marrstudent_set_paper_detail = [NSMutableArray arrayWithArray:marr];
    NSLog(@"marrstudent_set_paper_detail :%@",ShareData.marrstudent_set_paper_detail);
    
    
}



/*
 +(void)getmcqquestionTableData:(NSString *)strQuery{
 
 FMResultSet *results = [self selectQuery:strQuery];
 
 NSMutableArray * marr = [[NSMutableArray alloc]init];
 
 while ([results next])
 {
 NSMutableDictionary* mdict = [[NSMutableDictionary alloc] init];
 
 [mdict setValue:[results stringForColumn:@"MCQQuestionID"] forKey:@"MCQQuestionID"];
 [mdict setValue:[results stringForColumn:@"Question"] forKey:@"Question"];
 [mdict setValue:[results stringForColumn:@"ImageURL"] forKey:@"ImageURL"];
 [mdict setValue:[results stringForColumn:@"BoardID"] forKey:@"BoardID"];
 [mdict setValue:[results stringForColumn:@"MediumID"] forKey:@"MediumID"];
 [mdict setValue:[results stringForColumn:@"ClassID"] forKey:@"ClassID"];
 [mdict setValue:[results stringForColumn:@"StandardID"] forKey:@"StandardID"];
 [mdict setValue:[results stringForColumn:@"SubjectID"] forKey:@"SubjectID"];
 [mdict setValue:[results stringForColumn:@"ChapterID"] forKey:@"ChapterID"];
 [mdict setValue:[results stringForColumn:@"QuestionTypeID"] forKey:@"QuestionTypeID"];
 [mdict setValue:[results stringForColumn:@"QuestionLevelID"] forKey:@"QuestionLevelID"];
 [mdict setValue:[results stringForColumn:@"Duration"] forKey:@"Duration"];
 [mdict setValue:[results stringForColumn:@"CreatedBy"] forKey:@"CreatedBy"];
 [mdict setValue:[results stringForColumn:@"CreatedOn"] forKey:@"CreatedOn"];
 [mdict setValue:[results stringForColumn:@"ModifiedBy"] forKey:@"ModifiedBy"];
 [mdict setValue:[results stringForColumn:@"ModifiedOn"] forKey:@"ModifiedOn"];
 
 [marr addObject:mdict];
 
 }
 
 ShareData.marrmcqquestion = [NSMutableArray arrayWithArray:marr];
 NSLog(@"marrmcqquestion :%@", ShareData.marrmcqquestion);
 
 }*/


/*
 +(void)getmcqoptionTableData:(NSString *)strQuery{
 
 FMResultSet *results = [self selectQuery:strQuery];
 
 NSMutableArray * marr = [[NSMutableArray alloc]init];
 
 while ([results next])
 {
 NSMutableDictionary* mdict = [[NSMutableDictionary alloc] init];
 
 [mdict setValue:[results stringForColumn:@"MCQOPtionID"] forKey:@"MCQOPtionID"];
 [mdict setValue:[results stringForColumn:@"MCQQuestionID"] forKey:@"MCQQuestionID"];
 [mdict setValue:[results stringForColumn:@"Options"] forKey:@"Options"];
 [mdict setValue:[results stringForColumn:@"ImageURL"] forKey:@"ImageURL"];
 [mdict setValue:[results stringForColumn:@"OptionNo"] forKey:@"OptionNo"];
 [mdict setValue:[results stringForColumn:@"isCorrect"] forKey:@"isCorrect"];
 [mdict setValue:[results stringForColumn:@"CreatedBy"] forKey:@"CreatedBy"];
 [mdict setValue:[results stringForColumn:@"CreatedOn"] forKey:@"CreatedOn"];
 [mdict setValue:[results stringForColumn:@"ModifiedBy"] forKey:@"ModifiedBy"];
 [mdict setValue:[results stringForColumn:@"ModifiedOn"] forKey:@"ModifiedOn"];
 
 [marr addObject:mdict];
 }
 
 ShareData.marrmcqoption = [NSMutableArray arrayWithArray:marr];
 NSLog(@"marrmcqoption :%@", ShareData.marrmcqoption);
 
 }*/

#pragma mark **************** Delete Table Method **********

+(void)deletetemporaryTabledata{
    
    [self UpdateQuery:[NSString stringWithFormat:@"DELETE FROM temp_mcq_test_hdr"]];
    [self UpdateQuery:[NSString stringWithFormat:@"DELETE FROM sqlite_sequence where name = 'temp_mcq_test_hdr'"]];
    
    [self UpdateQuery:[NSString stringWithFormat:@"DELETE FROM temp_mcq_test_dtl"]];
    [self UpdateQuery:[NSString stringWithFormat:@"DELETE FROM sqlite_sequence where name = 'temp_mcq_test_dtl'"]];
    
    [self UpdateQuery:[NSString stringWithFormat:@"DELETE FROM temp_mcq_test_chapter"]];
    [self UpdateQuery:[NSString stringWithFormat:@"DELETE FROM sqlite_sequence where name = 'temp_mcq_test_chapter'"]];
    
    [self UpdateQuery:[NSString stringWithFormat:@"DELETE FROM temp_mcq_test_level"]];
    [self UpdateQuery:[NSString stringWithFormat:@"DELETE FROM sqlite_sequence where name = 'temp_mcq_test_level'"]];
    
}

+(void)deleteIncorrectAndNotappered :(NSString*)tblnm :(NSString*)queId
{
    [self UpdateQuery:[NSString stringWithFormat:@"DELETE from %@ where QuestionID=%@",tblnm,queId]];
}


+(void)deleteStudentTabledata{
    
    [self UpdateQuery:[NSString stringWithFormat:@"DELETE FROM student_mcq_test_hdr"]];
    [self UpdateQuery:[NSString stringWithFormat:@"DELETE FROM sqlite_sequence where name = 'student_mcq_test_hdr'"]];
    
    [self UpdateQuery:[NSString stringWithFormat:@"DELETE FROM student_mcq_test_dtl"]];
    [self UpdateQuery:[NSString stringWithFormat:@"DELETE FROM sqlite_sequence where name = 'student_mcq_test_dtl'"]];
    
    [self UpdateQuery:[NSString stringWithFormat:@"DELETE FROM student_mcq_test_chapter"]];
    [self UpdateQuery:[NSString stringWithFormat:@"DELETE FROM sqlite_sequence where name = 'student_mcq_test_chapter'"]];
    
    [self UpdateQuery:[NSString stringWithFormat:@"DELETE FROM notification"]];
    [self UpdateQuery:[NSString stringWithFormat:@"DELETE FROM sqlite_sequence where name = 'notification'"]];
}


+(int)getLastInsertedRowID{
    
    int lastId =(int) [db lastInsertRowId];
    NSLog(@"lastID %d",lastId);
    return (int) [db lastInsertRowId];
    
}

#pragma mark **************** Validation Method **********

+(NSString*)getCuruntDateTime{
    
    NSDate * now = [NSDate date];
    
    NSDateFormatter *DateFormator = [[NSDateFormatter alloc] init];
    [DateFormator setDateFormat:@"dd-MM-yyyy HH:mm:ss"];
    NSString *strdate = [DateFormator stringFromDate:now];
    
    return strdate;
    
}
+(BOOL)isPasswordMatch:(NSString *)pwd withConfirmPwd:(NSString *)cnfPwd {
    //asume pwd and cnfPwd has not whitespace
    
    if([pwd isEqualToString:cnfPwd]){
        //NSLog(@"Hurray! Password matches ");
        return TRUE;
    }
    
    return FALSE;
}

+(BOOL)validateEmailWithString:(NSString*)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
    
}

+(BOOL)validatePhone:(NSString *)phoneNumber
{
    //NSString *phoneRegex = @"^((\\+)|(00))[0-9]{6,14}$";
    NSString *phoneRegex = @"[0-9]{10}";
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    
    return [phoneTest evaluateWithObject:phoneNumber];
}

#pragma mark **************** Alert Method **********

+(void)alert:(NSString*)message view:(UIViewController * )strView{
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Alert" message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:ok];
    
    [strView presentViewController:alertController animated:YES completion:nil];
    
    
}

+ (NSString *) getFilePath {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    return [paths objectAtIndex:0];;
    
}

+ (id)cleanJsonToObject:(id)data {
    NSError* error;
    if (data == (id)[NSNull null]){
        return [[NSObject alloc] init];
    }
    id jsonObject;
    if ([data isKindOfClass:[NSData class]]){
        jsonObject = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    } else {
        jsonObject = data;
    }
    if ([jsonObject isKindOfClass:[NSArray class]]) {
        NSMutableArray *array = [jsonObject mutableCopy];
        for (int i = array.count-1; i >= 0; i--) {
            id a = array[i];
            if (a == (id)[NSNull null]){
                [array removeObjectAtIndex:i];
            } else {
                array[i] = [self cleanJsonToObject:a];
            }
        }
        return array;
    } else if ([jsonObject isKindOfClass:[NSDictionary class]]) {
        NSMutableDictionary *dictionary = [jsonObject mutableCopy];
        for(NSString *key in [dictionary allKeys]) {
            id d = dictionary[key];
            if (d == (id)[NSNull null]){
                dictionary[key] = @"";
            } else {
                dictionary[key] = [self cleanJsonToObject:d];
            }
        }
        return dictionary;
    } else {
        return jsonObject;
    }
}


@end
