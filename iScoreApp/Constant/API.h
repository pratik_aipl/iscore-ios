//
//  API.h
//  Pixavr
//
//  Created by My Mac on 1/12/17.
//  Copyright © 2017 My Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <AFNetworking/AFNetworking.h>
#import <AFNetworking/AFURLRequestSerialization.h>
#import <AFNetworking/AFURLSessionManager.h>


@interface API : UIViewController

+(void)GetUrl:(NSString*)strUrl Parameters:(NSMutableDictionary *)params andCallback:(void (^)(NSMutableDictionary *result, NSError *error))Callback;

+(void)PostUrl:(NSString*)strUrl Parameters:(NSMutableDictionary *)params andCallback:(void (^)(NSMutableDictionary *result, NSError *error))Callback;

+(void)PostUrlWithImage:(NSString*)strUrl Parameters:(NSMutableDictionary *)params ImageData:(NSData*)ImageData  andCallback:(void (^)(NSMutableDictionary *result, NSError *error))Callback;

+(void)PostImageAddFrameUrl:(NSString*)strUrl Parameters:(NSMutableDictionary *)params ImageData:(NSData*)ImageData andCallback:(void (^)(NSMutableDictionary *result, NSError *error))Callback;

+(NSMutableDictionary*)getDictResponse:(NSString *)strURL RequestData:(NSString *)strRequestData;
+(NSMutableDictionary*)Registration:(NSMutableDictionary*)marrDetail ImageNSData:(NSData*)imgNSData;

@end
