//
//  API.m
//  Pixavr
//
//  Created by My Mac on 1/12/17.
//  Copyright © 2017 My Mac. All rights reserved.
//

#import "API.h"


@interface API ()

@end

@implementation API


+(void)GetUrl:(NSString*)strUrl Parameters:(NSMutableDictionary *)params andCallback:(void (^)(NSMutableDictionary *result, NSError *error))Callback{
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    
    [manager GET:strUrl parameters:params success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        
        NSError * error=nil;
        NSMutableDictionary * parsedData = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
        
    completionHandler:Callback(parsedData,nil);
        
        // NSLog(@"Decoded: %@", parsedData);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        // NSLog(@"error = %@", error);
        
    completionHandler:Callback(nil,error);
        
    }];
}


+(void)PostUrl:(NSString*)strUrl Parameters:(NSMutableDictionary *)params andCallback:(void (^)(NSMutableDictionary *result, NSError *error))Callback{
    
        //NSLog(@"parameter : %@",params);
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    
    [manager POST:strUrl parameters:params success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        NSError * error=nil;
        
        NSMutableDictionary * parsedData = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
        
        NSLog(@"url :%@",strUrl);

        //NSLog(@"Decoded: %@", parsedData);
        
        completionHandler:Callback(parsedData,nil);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
    
    completionHandler:Callback(nil,error);
          NSLog(@"error: %@", error);
    }];
    
}

//With Image

+(void)PostUrlWithImage:(NSString*)strUrl Parameters:(NSMutableDictionary *)params ImageData:(NSData*)ImageData andCallback:(void (^)(NSMutableDictionary *result, NSError *error))Callback{
    
    
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:strUrl]];
    
    AFHTTPRequestOperation *op = [manager POST:strUrl parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        
        [formData appendPartWithFileData:ImageData name:@"ProfileImage" fileName:@"photo.jpg" mimeType:@"image/jpeg"];
        
       
        //NSLog(@"url:%@",strUrl);
        //NSLog(@"parameters:%@",params);
       
        
        
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
       
    completionHandler:Callback(responseObject,nil);
        
        NSLog(@"Success: %@ ***** %@", operation.responseString, responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    completionHandler:Callback(nil, error);
        
        NSLog(@"Error: %@ ***** %@", operation.responseString, error);
        
    }];
    
    [op start];
    
}

+(void)PostImageAddFrameUrl:(NSString*)strUrl Parameters:(NSMutableDictionary *)params ImageData:(NSData*)ImageData andCallback:(void (^)(NSMutableDictionary *result, NSError *error))Callback{
    
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:@"http://server.url"]];
    
    
    
    AFHTTPRequestOperation *op = [manager POST:strUrl parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        
       
        
        
        
        [formData appendPartWithFileData:ImageData name:@"logo_image" fileName:@"photo.jpg" mimeType:@"image/jpeg"];
      
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
    completionHandler:Callback(responseObject,nil);
        
        NSLog(@"Success: %@ ***** %@", operation.responseString, responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    completionHandler:Callback(nil, error);
        
        NSLog(@"Error: %@ ***** %@", operation.responseString, error);
        
    }];
    

    
    [op start];
    
}




+(void)LoginURL:(NSString*)Url param:(NSMutableDictionary*)mdicParam success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
        failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure{
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    
    [manager GET:Url parameters:mdicParam success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        
        NSError * error=nil;
        NSDictionary * parsedData = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
        
        NSLog(@"Decoded: %@", parsedData);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"error = %@", error);
        
    }];
    
}

/*
+(NSMutableDictionary*)getDictResponse:(NSString *)strURL RequestData:(NSString *)strRequestData
{
    NSString *myRequestString = strRequestData;
    
    NSLog(@"myRequestString:%@",myRequestString);
    // Create Data from request
    NSData *myRequestData = [NSData dataWithBytes: [myRequestString UTF8String] length: [myRequestString length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString:[NSString stringWithFormat:@"%@",strURL]]];
    //NSLog(@"myRequestString:%@",request);
    
    [request setTimeoutInterval:60];
    // set Request Type
    [request setHTTPMethod: @"POST"];
    // Set content-type
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    // Set Request Body
    [request setHTTPBody: myRequestData];
    // Now send a request and get Response
    
    NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse:nil error: nil];
    // NSLog(@"ReturnData:%@",returnData);
    
    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    
    NSLog(@"returnString:%@", returnString);
    
    NSError* error;
    NSMutableDictionary* marr = [[NSMutableDictionary alloc]init];
    if(returnData != nil)
    {
        NSMutableDictionary* json = [NSJSONSerialization JSONObjectWithData:returnData
                                                                    options:kNilOptions
                                                                      error:&error];
        NSLog(@"Json :%@",json);
        
        NSString *strSuccess = [NSString stringWithFormat:@"%@",[json valueForKey:@"result"]];
        
        if ([strSuccess isEqualToString:@"true"]) {
            
            //showAlert(AlertTitle, [json valueForKey:@"msg"]);
            // NSLog(@"Array :%@",[json valueForKey:@"invitation_list"]);
            return json;
        }
        else
        {
            //showAlert(AlertTitle, [json valueForKey:@"msg"]);
            return json;
        }
        return json;
    }else {
        [self checkIfInternetConection];
        return marr;
    }
}

// Registration
+(NSMutableDictionary*)Registration:(NSMutableDictionary*)marrDetail ImageNSData:(NSData*)imgNSData{
    
    NSURL *url = [[NSURL alloc] initWithString:@"http://api.pixavr.wcg-work.com/register"];
    NSMutableURLRequest *urequest = [NSMutableURLRequest requestWithURL:url];
    // Dictionary that holds post parameters. You can set your post parameters that your server accepts or programmed to accept.
    
    NSMutableDictionary* _params = [[NSMutableDictionary alloc] initWithDictionary:marrDetail];
    
    [urequest setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [urequest setHTTPShouldHandleCookies:NO];
    [urequest setTimeoutInterval:60];
    [urequest setHTTPMethod:@"POST"];
    
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [urequest setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    NSMutableData *body = [NSMutableData data];
    
    // add params (all params are strings)
    for (NSString *param in _params) {
        NSLog(@"Param:%@",param);
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    // Add __VIEWSTATE
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Disposition: form-data; name=\"__VIEWSTATE\"\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"/wEPDwUKLTQwMjY2MDA0M2RkXtxyHItfb0ALigfUBOEHb/mYssynfUoTDJNZt/K8pDs=" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSLog(@"Body:%@",body);
    
    // add image data profileImage
    NSData *imageData = imgNSData;
    if (imageData) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Disposition: form-data; name=\"avatar\"; filename=\"image.jpg\"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:imageData];
        [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    ///....................remove
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [urequest setHTTPBody:body];
    //return and test
    NSHTTPURLResponse *response = nil;
    NSData *returnData = [NSURLConnection sendSynchronousRequest:urequest returningResponse:&response error:nil];
    //remove..
    NSError* error;
    //   NSLog(@"return data..%@",returnData);
    if(returnData != nil)
    {
        NSMutableDictionary* json = [NSJSONSerialization JSONObjectWithData:returnData
                                                                    options:kNilOptions
                                                                      error:&error];
        
        NSLog(@"json  :%@",json);
        
        if([json valueForKey:@"success"])
        {
            return  json;
        }else{
            
            return json;
        }
        
    }else
    {
        // [self checkIfInternetConection];
        
        //  NSMutableDictionary* mdict=  [[NSMutableDictionary alloc]init];
        // [mdict setValue:@"Server Error" forKey:@"ServerError"];
        NSMutableDictionary *marr=[[NSMutableDictionary alloc] init];
        BOOL check=[self checkIfInternetConection];
        if (check) {
            [marr setValue:@"connection Error" forKey:@"ServerError"];
            [marr setValue:@"connection Error" forKey:@"msg"];
        }
        else
        {
            [marr setValue:@"check Internet Connection" forKey:@"ServerError"];
            [marr setValue:@"check Internet Connection" forKey:@"msg"];    }
        
        return marr;
        
    }
    
}

#pragma mark Internet Connection

+(BOOL)checkIfInternetConection
{
    Reachability* wifiReach = [Reachability reachabilityWithHostName: @"www.google.com"];
    NetworkStatus netStatus = [wifiReach currentReachabilityStatus];
    
    switch (netStatus)
    {
        case NotReachable:
        {
            NSLog(@"Access Not Available");
            showAlert(AlertTitle, AlertInternet);
            return NO;
            break;
        }
        case ReachableViaWWAN:
        {
            NSLog(@"Reachable WWAN");
            //showAlert(AlertTitle, AlertInternet);
            return YES;
            break;
        }
        case ReachableViaWiFi:
        {
            NSLog(@"Reachable WiFi");
            //showAlert(AlertTitle, AlertInternet);
            return YES;
            break;
        }
    }
}
*/



@end
