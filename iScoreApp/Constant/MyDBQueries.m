//
//  MyDBQueries.m
//  iScoreApp
//
//  Created by ADMIN-Khushal on 4/20/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import "MyDBQueries.h"

@implementation MyDBQueries


-(NSString*)getMCQSubjectsQuery :(NSString *)stdId
{
    return [NSString stringWithFormat:@"select s.*,(select CreatedOn from student_mcq_test_hdr as sthd where sthd.SubjectID=s.SubjectID                       order by CreatedOn desc limit 1) as Last_Date, count(smth.StudentMCQTestHDRID) as TotalSubjectTest,                       (( (select count(StudentMCQTestDTLID) from student_mcq_test_dtl as dtl left join mcqoption as                           opt on opt.MCQOPtionID=dtl.AnswerID where dtl.IsAttempt=1 and opt.isCorrect=1 and EXISTS                           (select StudentMCQTestHDRID from student_mcq_test_hdr hdr where hdr.StudentMCQTestHDRID=dtl.StudentMCQTestHDRID                            and hdr.SubjectID=s.SubjectID ) ) *100) / (select count (StudentMCQTestDTLID)                                                                       from student_mcq_test_dtl dtl where EXISTS (select StudentMCQTestHDRID from student_mcq_test_hdr hdr where                                                                                                                   hdr.StudentMCQTestHDRID=dtl.StudentMCQTestHDRID and hdr.SubjectID=s.SubjectID )) ) as accuracy1                       from subjects as s left join student_mcq_test_hdr as smth on s.SubjectID=smth.SubjectID where s.StandardID=%@ AND s.isMCQ=1 group by s.SubjectID order by s.SubjectOrder",stdId];
    
    
    
}

-(NSString*)getChapterList :(NSString *)subjectID
{
    return [NSString stringWithFormat:@"select main_c.*, (((select count(smtd.StudentMCQTestDTLID) from student_mcq_test_dtl smtd join mcqoption as mo on mo.MCQOPtionID=smtd.AnswerID  join mcqquestion as mq on mq.MCQQuestionID=mo.MCQQuestionID where ((mq.ChapterID=main_c.ChapterID) or (mq.ChapterID in (select c.ChapterID from chapters as c where c.ParentChapterID=main_c.ChapterID))) AND mo.isCorrect=1)*100)/(select count(smtd.StudentMCQTestDTLID) from student_mcq_test_dtl smtd join mcqoption as mo on mo.MCQOPtionID=smtd.AnswerID                                                         join mcqquestion as mq on mq.MCQQuestionID=mo.MCQQuestionID where ((mq.ChapterID=main_c.ChapterID) or (mq.ChapterID in (select c.ChapterID from chapters as c where c.ParentChapterID=main_c.ChapterID))))) as acuracy from chapters as main_c where main_c.ParentChapterID=0 AND main_c.SubjectID=%@ AND main_c.isMCQ=1",subjectID];
}



-(NSString*)getSubChapterCount:(NSString *) chapterID{
    return  [NSString stringWithFormat:@"SELECT COUNT(*) FROM chapters where ParentChapterID = %@ AND isMCQ = %@ ORDER BY DisplayOrder ASC",chapterID,@"1"];
}


-(NSString*)getSubChapter:(NSString *) chapterID{
    return  [NSString stringWithFormat:@"select * FROM chapters where ParentChapterID = %@ AND isMCQ = %@ ORDER BY DisplayOrder ASC",chapterID,@"1"];
}
////-----------SELECT LEVEL---------------
-(NSString*)getLevels:(NSString*)chapterlist level:(NSString*)levelID subjectid:(NSString*)subID
{
    NSString *query  = [NSString stringWithFormat:@"select (select count (StudentMCQTestDTLID) from student_mcq_test_dtl as dtl left join mcqquestion as mq on mq.MCQQuestionID=dtl.QuestionID where mq.QuestionLevelID=%@ AND mq.ChapterID IN(%@) and EXISTS (select StudentMCQTestHDRID from student_mcq_test_hdr hdr where hdr.StudentMCQTestHDRID=dtl.StudentMCQTestHDRID and hdr.SubjectID=%@)) as Total_attempt_level, (select count(StudentMCQTestDTLID) from student_mcq_test_dtl as dtl left join mcqoption as opt on opt.MCQOPtionID=dtl.AnswerID  left join mcqquestion as mq on mq.MCQQuestionID=dtl.QuestionID where dtl.IsAttempt=1 and opt.isCorrect=1 and mq.QuestionLevelID=%@ AND mq.ChapterID IN(%@) and EXISTS (select StudentMCQTestHDRID from student_mcq_test_hdr hdr where hdr.StudentMCQTestHDRID=dtl.StudentMCQTestHDRID and hdr.SubjectID=%@)) as Right_Answer_level , (((select count(StudentMCQTestDTLID)                                                                                                                                                                         from student_mcq_test_dtl as dtl left join mcqoption as opt on opt.MCQOPtionID=dtl.AnswerID left join mcqquestion as mq on mq.MCQQuestionID=dtl.QuestionID where dtl.IsAttempt=1 and opt.isCorrect=1 and mq.QuestionLevelID=%@ AND mq.ChapterID IN(%@) and EXISTS (select StudentMCQTestHDRID from student_mcq_test_hdr hdr where hdr.StudentMCQTestHDRID=dtl.StudentMCQTestHDRID and hdr.SubjectID=%@))    *100) /(select count (StudentMCQTestDTLID) from student_mcq_test_dtl as dtl left join mcqquestion as mq on mq.MCQQuestionID=dtl.QuestionID  where mq.QuestionLevelID=%@ AND mq.ChapterID IN(%@) and EXISTS (select StudentMCQTestHDRID from student_mcq_test_hdr hdr where hdr.StudentMCQTestHDRID=dtl.StudentMCQTestHDRID and hdr.SubjectID=%@))) as accuracy from student_mcq_test_hdr as smth where smth.SubjectID=%@ Limit 1",levelID,chapterlist,subID,levelID,chapterlist,subID,levelID,chapterlist,subID,levelID,chapterlist,subID,subID];
    
    
    //select (select count (StudentMCQTestDTLID) from student_mcq_test_dtl as dtl left join mcqquestion as mq on mq.MCQQuestionID=dtl.QuestionID  where mq.QuestionLevelID=3 AND mq.ChapterID IN(450704281,450704279,450704277,450704275,450704274,450704273,450704271,450704270,450704269,450704267,450704265,450704264) and EXISTS  (select StudentMCQTestHDRID from student_mcq_test_hdr hdr where hdr.StudentMCQTestHDRID=dtl.StudentMCQTestHDRID and hdr.SubjectID=110)) as Total_attempt_level, (select count(StudentMCQTestDTLID) from student_mcq_test_dtl as dtl left join mcqoption as opt on opt.MCQOPtionID=dtl.AnswerID  left join mcqquestion as mq on mq.MCQQuestionID=dtl.QuestionID where dtl.IsAttempt=1 and opt.isCorrect=1  and mq.QuestionLevelID=3 AND mq.ChapterID IN(450704281,450704279,450704277,450704275,450704274,450704273,450704271,450704270,450704269,450704267,450704265,450704264) and EXISTS (select StudentMCQTestHDRID from student_mcq_test_hdr hdr where hdr.StudentMCQTestHDRID=dtl.StudentMCQTestHDRID and hdr.SubjectID=110)) as Right_Answer_level , (((select count(StudentMCQTestDTLID) from student_mcq_test_dtl as dtl left join mcqoption as opt on opt.MCQOPtionID=dtl.AnswerID  left join mcqquestion as mq on mq.MCQQuestionID=dtl.QuestionID where dtl.IsAttempt=1 and opt.isCorrect=1 and mq.QuestionLevelID=3 AND mq.ChapterID IN(450704281,450704279,450704277,450704275,450704274,450704273,450704271,450704270,450704269,450704267,450704265,450704264) and EXISTS (select StudentMCQTestHDRID from student_mcq_test_hdr hdr where hdr.StudentMCQTestHDRID=dtl.StudentMCQTestHDRID and hdr.SubjectID=110))    *100) / (select count (StudentMCQTestDTLID) from student_mcq_test_dtl as dtl left join mcqquestion as mq on mq.MCQQuestionID=dtl.QuestionID  where mq.QuestionLevelID=3 AND mq.ChapterID IN(450704281,450704279,450704277,450704275,450704274,450704273,450704271,450704270,450704269,450704267,450704265,450704264) and EXISTS  (select StudentMCQTestHDRID from student_mcq_test_hdr hdr where hdr.StudentMCQTestHDRID=dtl.StudentMCQTestHDRID and hdr.SubjectID=110))) as accuracy from student_mcq_test_hdr as smth where smth.SubjectID=110 limit 1
    
    
    
    return query;
}

-(NSString*)getAccuracy:(NSString*)chapterlist subjectId:(NSString*)subjectID
{
    NSString *str=[NSString stringWithFormat:@"select (((select count(StudentMCQTestDTLID) from student_mcq_test_dtl as dtl left join mcqoption as opt on opt.MCQOPtionID=dtl.AnswerID  left join mcqquestion as mq on mq.MCQQuestionID=dtl.QuestionID where dtl.IsAttempt=1 and opt.isCorrect=1  AND mq.ChapterID IN(%@) and EXISTS (select StudentMCQTestHDRID from student_mcq_test_hdr hdr where hdr.StudentMCQTestHDRID=dtl.StudentMCQTestHDRID and hdr.SubjectID=%@))    *100) / (select count (StudentMCQTestDTLID) from student_mcq_test_dtl as dtl left join mcqquestion as mq on mq.MCQQuestionID=dtl.QuestionID  where  mq.ChapterID IN(%@) and EXISTS (select StudentMCQTestHDRID from student_mcq_test_hdr hdr where hdr.StudentMCQTestHDRID=dtl.StudentMCQTestHDRID and hdr.SubjectID=%@))) as accuracy from student_mcq_test_hdr as smth where smth.SubjectID=%@ limit 1",chapterlist,subjectID,chapterlist,subjectID,subjectID];
    
    
    
    return str;
}

//NOT APPERED
-(NSString*)inCurrect :(NSString*)chapterlist  subjectId:(NSString*)SubjectID boardId:(NSString*)BoardID mediumId:(NSString*)MediumId standerdId:(NSString*)StanderdId 
{
    NSString *str=[NSString stringWithFormat:@"select  * from  mcqquestion  where BoardID=%@  AND MediumID=%@ AND StandardID=%@  AND SubjectID=%@ AND ChapterID IN (%@) and MCQQuestionID in (select QuestionID from student_not_appeared_question)  ORDER BY RANDOM() limit 20",BoardID,MediumId,StanderdId,SubjectID,chapterlist];
    
    return str;
}

//INCURRECT
-(NSString*)inCurrect1 :(NSString*)chapterlist  subjectId:(NSString*)SubjectID boardId:(NSString*)BoardID mediumId:(NSString*)MediumId standerdId:(NSString*)StanderdId
{
    NSString *str=[NSString stringWithFormat:@"select  * from  mcqquestion  where BoardID=%@  AND MediumID=%@  AND StandardID=%@  AND SubjectID=%@ AND ChapterID IN (%@)and MCQQuestionID in (select QuestionID from student_incorrect_question)  ORDER BY RANDOM() limit 20",BoardID,MediumId,StanderdId,SubjectID,chapterlist];
    return str;
}

//PRACTICE PAPER
-(NSString*)getPRECSubjectsQuery :(NSString *)stdId
{
    return [NSString stringWithFormat:@"SELECT s.*,(select count(StudentQuestionPaperID) from student_question_paper as sqp where sqp.SubjectID = s.SubjectID) as TotalSubjectTest, (select CreatedOn from student_question_paper as lsqp where lsqp.SubjectID = s.SubjectID order by lsqp.CreatedOn desc limit 1) as Last_Date  FROM subjects as s where s.StandardID=%@ AND s.isGeneratePaper=1 ORDER BY s.SubjectOrder ASC",stdId];
    
}

//PAPER TYPE NAME
-(NSString*)getPaperType :(NSString *)subId
{
    return [NSString stringWithFormat:@"select pt.*, (select count(StudentQuestionPaperID) from student_question_paper as sqp left  join Subjects as s on sqp.SubjectID=s.SubjectID where sqp.PaperTypeID = pt.PaperTypeID AND s.SubjectID= %@) as TotalTest,(select lsqp.CreatedOn from student_question_paper as lsqp left join Subjects as s on lsqp.SubjectID=s.SubjectID where lsqp.PaperTypeID = pt.PaperTypeID AND s.SubjectID=%@ order by lsqp.CreatedOn desc limit 1) as last_attempt from paper_type as pt",subId,subId];
    
}

//---------ChaperList For Practive Paper---------
-(NSString*)getChapterListForP :(NSString *)subjectID
{
    return [NSString stringWithFormat:@"select  * from chapters where SubjectID = %@ AND ParentChapterID = 0 AND isGeneratePaper = 1 ORDER BY DisplayOrder ASC ",subjectID];
    
}

-(NSString*)getSubChapterCountForP:(NSString *) chapterID{
    return  [NSString stringWithFormat:@"SELECT COUNT(*) FROM chapters where ParentChapterID = %@ AND isGeneratePaper = 1 ORDER BY DisplayOrder ASC",chapterID];
}

-(NSString*)getSubChapterForP:(NSString*)chapterID
{
    return [NSString stringWithFormat:@"select  * from chapters where ParentChapterID = %@ AND isGeneratePaper = 1 ORDER BY DisplayOrder ASC",chapterID];
}
//-----------------SetPaper--------------------
-(NSString*)getSetPaperQueTypes:(NSString*)chapterID :(NSString*)subjectID
{
    return [NSString stringWithFormat:@"SELECT COUNT(qm.MQuestionID) as TotalQuestion, qt.QuestionTypeID, qt.QuestionType, qt.Marks FROM masterquestion qm JOIN question_types qt  ON qt.QuestionTypeID = qm.QuestionTypeID WHERE qm.SubjectID = %@ AND qm.ChapterID IN (%@)GROUP BY qt.QuestionTypeID  ORDER BY RevisionNo ASC",subjectID,chapterID];
}

//--------------------ExamType_Subject------------------------PREACTICE PAPER
-(NSString*)getExamtype_subject : (NSString*)subjectID
{
    return [NSString stringWithFormat:@"select  ExamTypeID from  examtype_subject  where subjectID=%@",subjectID];
}
-(NSString*)getExamPatten :(NSString*)subjectID :(NSString*)examPattenID :(NSString*)paperType
{
    return [NSString stringWithFormat:@"select  et.*,(SELECT `Instruction` FROM `exam_type_pattern` WHERE `SubjectID` = %@ AND `ExamTypeID` = et.ExamTypeID LIMIT 1) as MainInstruction from  examtypes as et where ExamTypeID IN (%@) AND (PaperType LIKE  '%@,%%' OR PaperType LIKE '%%, %@ ,%%' OR PaperType LIKE '%%,%@' OR PaperType = '%@')",subjectID,examPattenID,paperType,paperType,paperType,paperType];
}

-(NSString*)getExamTypePatten : (NSString*)subjectID :(NSString*)examTypeID
{
    return [NSString stringWithFormat:@"select ExamTypePatternID from exam_type_pattern where ExamTypeID='%@' AND SubjectID='%@' ORDER BY Random()",examTypeID,subjectID];
}
-(NSString*)getExamTypePattenDetails :(NSString*)examTypePattenID
{
    return [NSString stringWithFormat:@"select  * from  exam_type_pattern_detail where ExamTypePatternID = '%@' order by DisplayOrder",examTypePattenID];
}
-(NSString*)getMasterQuestion :(NSString*)subjectID :(NSString*)questionTypeID :(NSString*)mQueID
{
    return [NSString stringWithFormat:@"select * from masterquestion where SubjectID = '%@' AND QuestionTypeID = '%@' AND MQuestionID NOT IN (%@) ORDER BY Random() LIMIT 1",subjectID,questionTypeID,mQueID];
}
-(NSString*)getMasterQuestionwithChapter :(NSString*)questionTypeID :(NSString*)chapterIDs :(NSString*)mQueID
{
    return [NSString stringWithFormat:@"select distinct * from masterquestion where QuestionTypeID =%@ AND ChapterID IN (%@) AND MQuestionID NOT IN (%@) ORDER BY Random() LIMIT 1",questionTypeID,chapterIDs,mQueID];
}
-(NSString*)getPrilimPaper :(NSString*)stdQuePaperID
{
    return [NSString stringWithFormat:@"select etpd.ExamTypePatternDetailID, etpd.QuestionNo, etpd.SubQuestionNo, etpd.QuestionTypeText,etpd.isQuestion, etpd.PageNo, etpd.QuestionMarks,etpd.QuestionTypeID,sqpd.StudentQuestionPaperDetailID,mq.MQuestionID,mq.Question, mq.Answer, qt.isPassage from exam_type_pattern_detail as etpd left join student_question_paper_detail as sqpd on (sqpd.StudentQuestionPaperID = %@ AND sqpd. ExamTypePatternDetailID = etpd.ExamTypePatternDetailID) Left join masterquestion as mq on mq.MQuestionID = sqpd.MQuestionID Left join question_types as qt on qt.QuestionTypeID = mq.QuestionTypeID where etpd.ExamTypePatternID = (select ExamTypePatternID from student_question_paper where StudentQuestionPaperID = %@) Order by etpd.DisplayOrder",stdQuePaperID,stdQuePaperID];
}


//---------------------Set Paper Master Question-----------
-(NSString*)getSetPaperMasterQue :(NSString*)chaptersID :(NSString*)questionTypeID :(NSString*)limit
{
    return [NSString stringWithFormat:@"select distinct * from masterquestion where ChapterID IN (%@)          AND QuestionTypeID = '%@'  ORDER BY Random() LIMIT %@",chaptersID,questionTypeID,limit];
}


////////////////////////////////////////////////////////////////////

//////////////////////  SEARCH PAPER    ////////////////////////////

////////////////////////////////////////////////////////////////////
//--------------------GET ALL SUBJECT LIST--------------------------
-(NSString*)getAllsubjects
{
    return [NSString stringWithFormat:@"select * from subjects where isMCQ = 1 order by SubjectOrder"];
}
//select * from subjects where isGeneratePaper = 1 order by SubjectOrder
-(NSString*)getPractisePaperSubject
{
    return [NSString stringWithFormat:@"select * from subjects where isGeneratePaper = 1 order by SubjectOrder"];
}



-(NSString*)getAllTestList
{
    return [NSString stringWithFormat:@"select s.SubjectID,s.SubjectName, ((  (select count(StudentMCQTestDTLID) from student_mcq_test_dtl as dtl left join mcqoption as opt on opt.MCQOPtionID=dtl.AnswerID  where dtl.IsAttempt=1 and opt.isCorrect=1 and dtl.StudentMCQTestHDRID =smth.StudentMCQTestHDRID)*  100) / (select count (StudentMCQTestDTLID) from student_mcq_test_dtl dtl where dtl.StudentMCQTestHDRID =smth.StudentMCQTestHDRID) ) as accuracy1,(select count(StudentMCQTestDTLID) from student_mcq_test_dtl as dtl left join mcqoption as opt on opt.MCQOPtionID=dtl.AnswerID  where dtl.IsAttempt=1 and opt.isCorrect=1 and dtl.StudentMCQTestHDRID =smth.StudentMCQTestHDRID) as Total_Right,(select count (StudentMCQTestDTLID) from student_mcq_test_dtl dtl where dtl.StudentMCQTestHDRID =smth.StudentMCQTestHDRID) as Total_que, smth.StudentMCQTestHDRID, smth.CreatedOn from student_mcq_test_hdr as smth left join subjects as s on s.SubjectID=smth.SubjectID group by smth.StudentMCQTestHDRID order by smth.CreatedOn DESC"];
}

-(NSString*)getAllTestListWithFilter :(NSString *)substr :(NSString*)levelstr
{
    return [NSString stringWithFormat:@"select s.SubjectID,s.SubjectName, ((( select count(StudentMCQTestDTLID)  from student_mcq_test_dtl as dtl left join mcqoption as opt on opt.MCQOPtionID=dtl.AnswerID  where dtl.IsAttempt=1  and opt.isCorrect=1 and dtl.StudentMCQTestHDRID =smth.StudentMCQTestHDRID) *  100) / (select count (StudentMCQTestDTLID) from student_mcq_test_dtl dtl where dtl.StudentMCQTestHDRID =smth.StudentMCQTestHDRID) )  as accuracy1, (select count(StudentMCQTestDTLID) from student_mcq_test_dtl as dtl left join mcqoption as opt on opt.MCQOPtionID=dtl.AnswerID  where dtl.IsAttempt=1 and opt.isCorrect=1 and dtl.StudentMCQTestHDRID =smth.StudentMCQTestHDRID) as Total_Right,(select count (StudentMCQTestDTLID) from student_mcq_test_dtl dtl where dtl.StudentMCQTestHDRID =smth.StudentMCQTestHDRID) as  Total_que, smth.StudentMCQTestHDRID, smth.CreatedOn from student_mcq_test_hdr as smth left join subjects as s on s.SubjectID=smth.SubjectID where 1 %@ %@ order by smth.CreatedOn DESC",substr,levelstr];
    
    //and s.SubjectID in(16)
    //and smth.StudentMCQTestHDRID IN (select StudentMCQTestHDRID from student_mcq_test_level where LevelID IN (2))
    
}
//---------------------------PRECTISE PAPER-------
-(NSString*)getAllTestListPractisePaper
{
    return [NSString stringWithFormat:@"SELECT sqp.StudentQuestionPaperID, sqp.CreatedOn, s.SubjectName, et.ExamTypeName, et.Duration, et.TotalMarks,sqp.ExamTypePatternID,pt.PaperTypeName,sqp.PaperTypeID,sqp.TotalMarks AS SetPaperMarks,sqp.Duration AS SetPaperDuration FROM student_question_paper sqp LEFT JOIN exam_type_pattern etp ON sqp.ExamTypePatternID = etp.ExamTypePatternID LEFT JOIN examtypes et ON etp.ExamTypeID = et.ExamTypeID LEFT JOIN subjects s ON sqp.SubjectID = s.SubjectID  LEFT JOIN paper_type pt ON sqp.PaperTypeID=pt.PaperTypeID ORDER BY sqp.StudentQuestionPaperID DESC"];
}

-(NSString*)getPractisePaperFilter :(NSString*)substr :(NSString*)ptype
{
    NSUserDefaults *stander=[[NSUserDefaults alloc]init];
    return [NSString stringWithFormat:@"SELECT sqp.StudentQuestionPaperID, sqp.CreatedOn as CreatedOn, s.SubjectName,et.Duration, et.ExamTypeName, et.TotalMarks, sqp.PaperTypeID, pt.PaperTypeName, pt.PaperTypeID,sqp.TotalMarks AS SetPaperMarks,sqp.Duration AS SetPaperDuration,sqp.ExamTypePatternID   FROM `student_question_paper` `sqp`  LEFT JOIN `exam_type_pattern` `etp` ON `sqp`.`ExamTypePatternID` = `etp`.`ExamTypePatternID`  LEFT JOIN `examtypes` `et` ON `etp`.`ExamTypeID` = `et`.`ExamTypeID`  LEFT JOIN `paper_type` `pt` ON `sqp`.`PaperTypeID` = `pt`.`PaperTypeID` LEFT JOIN `subjects` `s` ON `sqp`.`SubjectID` = `s`.`SubjectID`  WHERE `sqp`.`StudentID` = %@ %@ %@",[stander valueForKeyPath:@"ParseData.StudentID"],substr,ptype];
}


-(NSString*)getSPPracticePaper:(NSString*)paperID
{
    return [NSString stringWithFormat:@"select etpd.ExamTypePatternDetailID, etpd.QuestionNo, etpd.SubQuestionNo, etpd.QuestionTypeText, etpd.isQuestion, etpd.PageNo, etpd.QuestionMarks,etpd.QuestionTypeID,sqpd.StudentQuestionPaperDetailID,mq.MQuestionID, mq.Question, mq.Answer, qt.isPassage from exam_type_pattern_detail as etpd  left join student_question_paper_detail as sqpd on (sqpd.StudentQuestionPaperID =%@ AND sqpd.    ExamTypePatternDetailID = etpd.ExamTypePatternDetailID) Left join masterquestion as mq on mq.MQuestionID = sqpd.MQuestionID Left join question_types as qt on qt.QuestionTypeID = mq.QuestionTypeID where etpd.ExamTypePatternID = (select ExamTypePatternID from student_question_paper where   StudentQuestionPaperID = %@) Order by etpd.DisplayOrder",paperID,paperID];
}

-(NSString*)getMCQTestDTL :(NSString*)SMTHDRID
{
    return [NSString stringWithFormat:@"select  * from student_mcq_test_dtl where StudentMCQTestHDRID = %@ ORDER BY StudentMCQTestDTLID ASC",SMTHDRID];
}

-(NSString*)getMCQQuestion :(NSString*)SMIDs
{
    return [NSString stringWithFormat:@"select * from mcqquestion where MCQQuestionID IN(%@)",SMIDs];
}





////////////////////////////////////////////////////////
-(NSString*)getStudentSetPaperQueType :(NSString*)lastID
{
    return [NSString stringWithFormat:@"SELECT sspd.StudentSetPaperDetailID, qt.isPassage, mq.Question, mq.Answer, sspd.MQuestionID, qt.QuestionType, sspq.QuestionTypeID, sspq.TotalAsk, sspq.ToAnswer, sspq.TotalMark FROM student_set_paper_question_type sspq LEFT JOIN question_types qt ON qt.QuestionTypeID = sspq.QuestionTypeID LEFT JOIN student_set_paper_detail sspd ON sspd.StudentSetPaperQuestionTypeID = sspq.StudentSetPaperQuestionTypeID LEFT JOIN masterquestion mq on mq.MQuestionID=sspd.MQuestionID WHERE sspq.StudentSetPaperQuestionTypeID IN (%@)  ORDER BY sspd.StudentSetPaperQuestionTypeID",lastID];
}


////////////////////////////////////////////////////////////////////
//////////////////////  REVISE    ////////////////////////////
////////////////////////////////////////////////////////////////////


-(NSString*)getQuestionTypes :(NSString*)subjectID :(NSString*)chapterList
{
    return [NSString stringWithFormat:@"SELECT qt.QuestionTypeID, qt.QuestionType, qt.Marks FROM masterquestion qm JOIN question_types qt ON qt.QuestionTypeID = qm.QuestionTypeID  WHERE qm.SubjectID = %@ AND qm.ChapterID IN (%@)  GROUP BY qt.QuestionTypeID ORDER BY qt.RevisionNo ASC",subjectID,chapterList];
}

-(NSString*)getReviseQuetion :(NSString*)queType :(NSString*)chapterList
{
    return [NSString stringWithFormat:@"select distinct * from masterquestion where ChapterID IN (%@)  AND QuestionTypeID = '%@'  ORDER BY Random()",chapterList,queType];
}

////////////////////////////////////////////////////////////////////
//////////////////////  ZOOKI    ////////////////////////////
////////////////////////////////////////////////////////////////////

-(NSString*)getZooki
{
    return [NSString stringWithFormat:@"select * from zooki"];
}


@end
