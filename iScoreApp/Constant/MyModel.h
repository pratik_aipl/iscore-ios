//
//  MyModel.h
//  QuizApp
//
//  Created by My Mac on 2/17/17.
//  Copyright © 2017 My Mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FMDatabase.h"
//#import "NIDropDown.h"

@interface MyModel : UIViewController



+(void)MyModelInit;
+(FMResultSet*)selectQuery:(NSString*)strQuery;
+(void)UpdateQuery:(NSString*)strQuery;
+(NSUInteger)getCount:(NSString*)strQuery;
+(NSMutableArray*)getAllColumnNames: (NSString*) tblName;
+(void)insertIntoBoardTable:(NSMutableDictionary *)mdict;
+(void)insertIntoboards_paperTable:(NSMutableDictionary *)mdict;
+(void)getboards_paperTableData;
+(void)insertIntoboard_paper_filesTable:(NSMutableDictionary *)mdict;
+(void)insertIntochaptersTable:(NSMutableDictionary *)mdict;
+(void)insertintochapter:(NSMutableDictionary *)mdict;
+(void)insertIntoexamtypesTable:(NSMutableDictionary *)mdict;
+(void)insertIntoexamtype_subjectTable:(NSMutableDictionary *)mdict;
+(void)insertIntomasterquestionTable:(NSMutableDictionary *)mdict;
+(void)insertIntomcqoptionTable:(NSMutableDictionary *)mdict;
+(void)insertIntomcqquestionTable:(NSMutableDictionary *)mdict;
+(void)insertIntomediumsTable:(NSMutableDictionary *)mdict;
+(void)insertIntoquestion_typesTable:(NSMutableDictionary *)mdict;
+(void)insertIntostandardsTable:(NSMutableDictionary *)mdict;
+(void)insertIntosubjectsTable:(NSMutableDictionary *)mdict;
+(void)insertIntopaper_typeTable:(NSMutableDictionary *)mdict;
+(void)getpaper_typeTableData;
+(void)insertIntoexam_type_patternTable:(NSMutableDictionary *)mdict;
+(void)insertIntoexam_type_pattern_detailTable:(NSMutableDictionary *)mdict;

+(void)addCollumIntostudent_question_paper;
+(void)addNewTable;

+(void)getsubjects;
+(void)getstudent_mcq_test_hdrData:(NSString *)strQuery;
+(void)getstudent_mcq_test_chapterData:(NSString *)strQuery;
+(void)getstudent_mcq_test_dtlData:(NSString *)strQuery;
+(void)getmcqquestionTableData:(NSString *)strQuery;
+(void)getmcqoptionTableData:(NSString *)strQuery;
+(void)getexam_type_patternTableData:(NSString *)strQuery;
+(void)getexam_type_pattern_detailTableData:(NSString *)strQuery;
+(void)getstudent_question_paperData:(NSString *)strQuery;
+(void)getstudent_set_paper_question_typeData:(NSString *)strQuery;
+(void)getstudent_set_paper_detailData:(NSString *)strQuery;

/////////// WEB VIEW ====================



///////////TEMP///////////////////////
+(void)insertInto_papertype:(NSMutableDictionary *)mdict :(NSString *)tblnm;
+(void)deleteIncorrectAndNotappered :(NSString*)tblnm :(NSString*)queId;


//+(NSMutableArray*)getmasterquestionTableData:(NSString *)strQuery;


+(int)getLastInsertedRowID;

+(void)deletetemporaryTabledata;
+(void)deleteStudentTabledata;

+(NSString*)getCuruntDateTime;
+(void)alert:(NSString*)message view:(UIViewController * )strView;
+(BOOL)isPasswordMatch:(NSString *)pwd withConfirmPwd:(NSString *)cnfPwd;
+(BOOL)validateEmailWithString:(NSString*)email;
+(BOOL)validatePhone:(NSString *)phoneNumber;
+(NSString*) getFilePath;
+ (id)cleanJsonToObject:(id)data;
/*
+(void)setTitle:(NSString *)title :(UIButton *)sender;
+(void)rel;
*/
@end
