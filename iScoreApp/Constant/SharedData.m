
//
//  SharedData.m
//  McqApp
//
//  Created by My Mac on 3/22/17.
//  Copyright © 2017 My Mac. All rights reserved.
//

#import "SharedData.h"

@implementation SharedData

static SharedData *sharedInstance = nil;

// Get the shared instance and create it if necessary.

+ (SharedData *)sharedInstance
{
    if (sharedInstance == nil)
    {
        sharedInstance = [[super allocWithZone:NULL] init];
    }
    return sharedInstance;
}

- (id)init
{
    self = [super init];
    if (self)
    {
        
    }
    return self;
}

@end
