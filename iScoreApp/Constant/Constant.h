//
//  Constant.h
//  QuizApp
//
//  Created by My Mac on 2/21/17.
//  Copyright © 2017 My Mac. All rights reserved.
//

#ifndef Constant_h
#define Constant_h

#import "AppDelegate.h"


#define Internet_connectivity @"Please check internet connection or try again later."

// Live Url
//#define Main_URL @"http://staff.parshvaa.com/"

// Test Server Url
#define Main_URL @"https://test.escore.parshvaa.com/"

#define BASEURL @"https://test.escore.parshvaa.com/web_services/version_39/web_services/"

#define APP_DELEGATE ((AppDelegate*)[[UIApplication sharedApplication] delegate])

#define UserDefault  [NSUserDefaults standardUserDefaults]

#define ShareData  APP_DELEGATE.sharedData

#define ImgRoot_Path [ShareData.mdictImages valueForKey:@"root_path"]


//#define NSLog(FORMAT, ...) printf("%s\n", [[NSString stringWithFormat:FORMAT, ##__VA_ARGS__] UTF8String]);

#endif /* Constant_h */
