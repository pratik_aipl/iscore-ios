//
//  MyDBResultParser.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 4/20/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FMResultSet.h"
#import "ChapterListModel.h"

@interface MyDBResultParser : NSObject
-(NSObject *) parseDBResult :(FMResultSet*)result :(NSObject *) object;

@end
