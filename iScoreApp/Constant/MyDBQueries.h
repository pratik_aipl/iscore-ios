//
//  MyDBQueries.h
//  iScoreApp
//
//  Created by ADMIN-Khushal on 4/20/18.
//  Copyright © 2018 ADMIN-Khushal. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MyDBQueries : NSObject

//-------SUBJECT LIST-----------
-(NSString*)getMCQSubjectsQuery :(NSString*)stdId;
//-------CHAPTER LIST-----------
-(NSString*)getChapterList :(NSString *)subjectID;
-(NSString*)getSubChapterCount:(NSString*)chapterID;
-(NSString*)getSubChapter:(NSString*)chapterID;
//-------SELECT LEVEL-----------
-(NSString*)getLevels:(NSString*)chapterlist level:(NSString*)levelID subjectid:(NSString*)subID;
-(NSString*)getAccuracy:(NSString*)chapterlist subjectId:(NSString*)subjectID;
-(NSString*)inCurrect :(NSString*)chapterlist  subjectId:(NSString*)SubjectID boardId:(NSString*)BoardID mediumId:(NSString*)MediumId standerdId:(NSString*)StanderdId;
-(NSString*)inCurrect1 :(NSString*)chapterlist  subjectId:(NSString*)SubjectID boardId:(NSString*)BoardID mediumId:(NSString*)MediumId standerdId:(NSString*)StanderdId;

//----------PRECTICE PAPER----------
-(NSString*)getPRECSubjectsQuery :(NSString *)stdId;
//---------PAPER TYPE---------------
-(NSString*)getPaperType :(NSString *)subId;
//---------ChaperList For Practive Paper---------
-(NSString*)getChapterListForP :(NSString *)subjectID;
-(NSString*)getSubChapterCountForP:(NSString *) chapterID;
-(NSString*)getSubChapterForP:(NSString*)chapterID;
//-----------------SetPaper--------------------
-(NSString*)getSetPaperQueTypes:(NSString*)chapterID :(NSString*)subjectID;

//--------------------ExamType_Subject------------------------
-(NSString*)getExamtype_subject : (NSString*)subjectID;
-(NSString*)getExamPatten :(NSString*)subjectID :(NSString*)examPattenID :(NSString*)paperType;
-(NSString*)getExamTypePatten : (NSString*)subjectID :(NSString*)examTypeID;
-(NSString*)getExamTypePattenDetails :(NSString*)examTypePattenID;
-(NSString*)getMasterQuestion :(NSString*)subjectID :(NSString*)questionTypeID :(NSString*)mQueID;
-(NSString*)getMasterQuestionwithChapter :(NSString*)questionTypeID :(NSString*)chapterIDs :(NSString*)mQueID;
-(NSString*)getPrilimPaper :(NSString*)stdQuePaperID;

//---------------------Set Paper Master Question-----------
-(NSString*)getSetPaperMasterQue :(NSString*)chaptersID :(NSString*)questionTypeID :(NSString*)limit;
-(NSString*)getStudentSetPaperQueType :(NSString*)lastID;

//--------------------GET ALL SUBJECT LIST--------------------------
-(NSString*)getAllsubjects;
-(NSString*)getAllTestList;
-(NSString*)getAllTestListWithFilter :(NSString *)substr :(NSString*)levelstr;

-(NSString*)getPractisePaperSubject;
-(NSString*)getAllTestListPractisePaper;
-(NSString*)getPractisePaperFilter :(NSString*)substr :(NSString*)ptype;
-(NSString*)getSPPracticePaper:(NSString*)paperID;
-(NSString*)getMCQTestDTL :(NSString*)SMTHDRID;
-(NSString*)getMCQQuestion :(NSString*)SMIDs;

//////////////////////  REVISE    ////////////////////////////
-(NSString*)getQuestionTypes :(NSString*)subjectID :(NSString*)chapterList;
-(NSString*)getReviseQuetion :(NSString*)queType :(NSString*)chapterList;


//////////////////////  ZOOKI    ////////////////////////////
-(NSString*)getZooki;






@end
